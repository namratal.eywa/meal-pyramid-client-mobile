"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_main_progress_progress_module_ts"],{

/***/ 41822:
/*!****************************************************************************************!*\
  !*** ./src/app/main/progress/progress-feedback-log/progress-feedback-log.component.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressFeedbackLogComponent": () => (/* binding */ ProgressFeedbackLogComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_feedback_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-feedback-log.component.html?ngResource */ 75638);
/* harmony import */ var _progress_feedback_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-feedback-log.component.scss?ngResource */ 98328);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);












let ProgressFeedbackLogComponent = class ProgressFeedbackLogComponent {
  constructor(_uts, _gs, _sb, _shs, _dialog) {
    this._uts = _uts;
    this._gs = _gs;
    this._sb = _sb;
    this._shs = _shs;
    this._dialog = _dialog;
    this.deleting = false;
    this.dispatchView = false;
    this.hasChanged = false;
    this.isNew = false;
    this.saving = false;
    this.deleteEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_7__.EventEmitter();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();
    this.dispatchView = true;
    this.logFG.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(() => {
      this.hasChanged = !this._uts.shallowEqual(this.logFGInitial, this.logFG.getRawValue());
    });
    this.observeRadioButtons();
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  get dateFC() {
    return this.logFG.get('date');
  }

  get keyFC() {
    return this.logFG.get('key');
  }

  get uidFC() {
    return this.logFG.get('uid');
  }

  setInitialFG() {
    this.setInputFields();
    this.logFGInitial = this.logFG.getRawValue();
  }

  get isOutsideMealFC() {
    return this.logFG.get('isOutsideMeal');
  }

  get outsideMealsCountFC() {
    return this.logFG.get('outsideMealsCount');
  }

  get isMealSkippedFC() {
    return this.logFG.get('isMealSkipped');
  }

  get skippedMealsCountFC() {
    return this.logFG.get('skippedMealsCount');
  }

  get isRoutineChangedFC() {
    return this.logFG.get('isRoutineChanged');
  }

  get routineChangeDetailsFC() {
    return this.logFG.get('routineChangeDetails');
  }

  get isHungryBetweenMealsFC() {
    return this.logFG.get('isHungryBetweenMeals');
  }

  get isAlcoholConsumedFC() {
    return this.logFG.get('isAlcoholConsumed');
  }

  get alcoholConsumedDetailsFC() {
    return this.logFG.get('alcoholConsumedDetails');
  }

  get isHydrationFollowedFC() {
    return this.logFG.get('isHydrationFollowed');
  }

  get hydrationIntakeFC() {
    return this.logFG.get('hydrationIntake');
  }

  get hadCompetitionFC() {
    return this.logFG.get('hadCompetition');
  }

  get competitionPerformanceFC() {
    return this.logFG.get('competitionPerformance');
  }

  get competitionRecoveryFC() {
    return this.logFG.get('competitionRecovery');
  }

  get upcomingCompetitionFC() {
    return this.logFG.get('upcomingCompetition');
  }

  get upcomingCompetitionDetailsFC() {
    return this.logFG.get('upcomingCompetitionDetails');
  }

  setInputFields() {
    if (!this.isOutsideMealFC.value) {
      this.outsideMealsCountFC.disable();
    }

    if (!this.isMealSkippedFC.value) {
      this.skippedMealsCountFC.disable();
    }

    if (!this.isRoutineChangedFC.value) {
      this.routineChangeDetailsFC.disable();
    }

    if (!this.isAlcoholConsumedFC.value) {
      this.alcoholConsumedDetailsFC.disable();
    }

    if (!this.isHydrationFollowedFC.value) {
      this.hydrationIntakeFC.disable();
    }

    if (!this.hadCompetitionFC.value) {
      this.competitionPerformanceFC.disable();
      this.competitionRecoveryFC.disable();
    }

    if (!this.upcomingCompetitionFC.value) {
      this.upcomingCompetitionDetailsFC.disable();
    }
  }

  observeRadioButtons() {
    this.isOutsideMealFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.outsideMealsCountFC.enable();
      } else {
        this.outsideMealsCountFC.reset('');
        this.outsideMealsCountFC.disable();
      }
    });
    this.isMealSkippedFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.skippedMealsCountFC.enable();
      } else {
        this.skippedMealsCountFC.reset('');
        this.skippedMealsCountFC.disable();
      }
    });
    this.isRoutineChangedFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.routineChangeDetailsFC.enable();
      } else {
        this.routineChangeDetailsFC.reset('');
        this.routineChangeDetailsFC.disable();
      }
    });
    this.isAlcoholConsumedFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.alcoholConsumedDetailsFC.enable();
      } else {
        this.alcoholConsumedDetailsFC.reset('');
        this.alcoholConsumedDetailsFC.disable();
      }
    });
    this.isHydrationFollowedFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.hydrationIntakeFC.enable();
      } else {
        this.hydrationIntakeFC.reset('');
        this.hydrationIntakeFC.disable();
      }
    });
    this.hadCompetitionFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.competitionPerformanceFC.enable();
        this.competitionRecoveryFC.enable();
      } else {
        this.competitionPerformanceFC.reset('');
        this.competitionPerformanceFC.disable();
        this.competitionRecoveryFC.reset('');
        this.competitionRecoveryFC.disable();
      }
    });
    this.upcomingCompetitionFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.upcomingCompetitionDetailsFC.enable();
      } else {
        this.upcomingCompetitionDetailsFC.reset('');
        this.upcomingCompetitionDetailsFC.disable();
      }
    });
  }

  reset() {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  save() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.hasChanged || !_this.logFG.valid || _this.deleting) {
        return;
      }

      try {
        _this.saving = true;

        if (_this.isNew) {
          const key = _this._gs.getNewKey('userFeedbackLogs', true, _this.uidFC.value);

          _this.keyFC.setValue(key);
        }

        yield _this._gs.setData(_this.uidFC.value, 'userFeedbackLogs', _this.logFG.getRawValue(), _this.isNew, ['date'], true, _this.keyFC.value);
        _this.saving = false;
        _this.isNew = false;

        _this.setInitialFG();

        _this.hasChanged = false;

        _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessMessages.SAVE_OK, 2000);
      } catch (err) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SAVE_FAILED);

        _this.saving = false;
      }
    })();
  }

  delete() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this2.saving) {
        return;
      }

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to delete the log?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this2._shs.setDialogBox(dialogBox);

      const dialogRef = _this2._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            _this2.deleting = true;

            if (!_this2.isNew) {
              yield _this2._gs.setData(_this2.uidFC.value, 'userFeedbackLogs', {
                recStatus: false
              }, _this2.isNew, ['date'], true, _this2.keyFC.value);
            }

            _this2.deleteEvent.emit({
              date: _this2.dateFC.value
            });
          } catch (err) {
            _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);

            _this2.deleting = false;
          }
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  }

};

ProgressFeedbackLogComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.GeneralService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog
}];

ProgressFeedbackLogComponent.propDecorators = {
  logFG: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  startWeight: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  deleteEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Output
  }]
};
ProgressFeedbackLogComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-progress-feedback-log',
  template: _progress_feedback_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_progress_feedback_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProgressFeedbackLogComponent);


/***/ }),

/***/ 54284:
/*!********************************************************************************!*\
  !*** ./src/app/main/progress/progress-feedback/progress-feedback.component.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressFeedbackComponent": () => (/* binding */ ProgressFeedbackComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_feedback_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-feedback.component.html?ngResource */ 29505);
/* harmony import */ var _progress_feedback_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-feedback.component.scss?ngResource */ 45547);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 83910);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);











let ProgressFeedbackComponent = class ProgressFeedbackComponent {
    constructor(_fb, _us, _sb, _as) {
        this._fb = _fb;
        this._us = _us;
        this._sb = _sb;
        this._as = _as;
        this.dispatchView = false;
        this.loggedDates = [];
        this.logs = [];
        this.logsFA = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        this._me$ = this._as.getMe();
        // Needs to be an arrow function as per Angular Material since it's a property
        this.dateFilter = (date) => {
            const res = this.loggedDates.filter((i) => i.valueOf() === date.valueOf());
            return res.length === 0;
        };
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)((me) => {
            this.me = me;
            return this._us.getUserFeedbackLogs(this.me.uid, this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.catchError)((err) => {
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_9__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1))
            .subscribe((logs) => {
            this.setInitialLogsFA(logs);
            this.dispatchView = true;
        });
        this.maxDate = moment__WEBPACK_IMPORTED_MODULE_2__().endOf('day').toDate();
    }
    // *
    // * Feedback Log - Date
    // *
    onDailyDateSelection(event) {
        const date = event.value;
        this.addNewLog(date);
    }
    // *
    // * Feedback Log - FormArray
    // *
    setInitialLogsFA(logs) {
        this.logsFA.clear();
        logs.map((log) => {
            this.addExistingLog(log);
        });
    }
    addExistingLog(log) {
        const fg = this._fb.group({ ...log });
        this.logsFA.push(fg);
        this.loggedDates.push(log.date);
    }
    addNewLog(date) {
        const log = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserFeedbackLog();
        log.date = date;
        log.uid = this.me.uid;
        log.membershipKey = this.me.membershipKey;
        log.isActive = true;
        log.recStatus = true;
        const fg = this._fb.group({ ...log });
        this.logsFA.insert(0, fg);
        this.loggedDates.push(date);
    }
    removeLog(e, i) {
        this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
        this.logsFA.removeAt(i);
    }
};
ProgressFeedbackComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService }
];
ProgressFeedbackComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-progress-feedback',
        template: _progress_feedback_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_progress_feedback_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProgressFeedbackComponent);



/***/ }),

/***/ 67571:
/*!********************************************************************************!*\
  !*** ./src/app/main/progress/progress-food-log/progress-food-log.component.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressFoodLogComponent": () => (/* binding */ ProgressFoodLogComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_food_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-food-log.component.html?ngResource */ 24280);
/* harmony import */ var _progress_food_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-food-log.component.scss?ngResource */ 61758);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);












let ProgressFoodLogComponent = class ProgressFoodLogComponent {
  constructor(_uts, _gs, _sb, _shs, _dialog) {
    this._uts = _uts;
    this._gs = _gs;
    this._sb = _sb;
    this._shs = _shs;
    this._dialog = _dialog;
    this.deleting = false;
    this.dispatchView = false;
    this.hasChanged = false;
    this.isNew = false;
    this.saving = false;
    this.deleteEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_7__.EventEmitter();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();
    this.dispatchView = true;
    this.logFG.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      this.hasChanged = !this._uts.shallowEqual(this.logFGInitial, val);
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  get dateFC() {
    return this.logFG.get('date');
  }

  get keyFC() {
    return this.logFG.get('key');
  }

  get uidFC() {
    return this.logFG.get('uid');
  }

  setInitialFG() {
    this.logFGInitial = this.logFG.value;
  }

  reset() {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  save() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.hasChanged || !_this.logFG.valid || _this.deleting) {
        return;
      }

      try {
        _this.saving = true;

        if (_this.isNew) {
          const key = _this._gs.getNewKey('userFoodLogs', true, _this.uidFC.value);

          _this.keyFC.setValue(key);
        }

        yield _this._gs.setData(_this.uidFC.value, 'userFoodLogs', _this.logFG.value, _this.isNew, ['date'], true, _this.keyFC.value);
        _this.saving = false;
        _this.isNew = false;

        _this.setInitialFG();

        _this.hasChanged = false;

        _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessMessages.SAVE_OK, 2000);
      } catch (err) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SAVE_FAILED);

        _this.saving = false;
      }
    })();
  }

  delete() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this2.saving) {
        return;
      }

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to delete the log?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this2._shs.setDialogBox(dialogBox);

      const dialogRef = _this2._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            _this2.deleting = true;

            if (!_this2.isNew) {
              yield _this2._gs.setData(_this2.uidFC.value, 'userFoodLogs', {
                recStatus: false
              }, _this2.isNew, ['date'], true, _this2.keyFC.value);
            }

            _this2.deleteEvent.emit({
              date: _this2.dateFC.value
            });
          } catch (err) {
            _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.DELETE_FAILED);

            _this2.deleting = false;
          }
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  }

};

ProgressFoodLogComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.GeneralService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog
}];

ProgressFoodLogComponent.propDecorators = {
  logFG: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  startWeight: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  deleteEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Output
  }]
};
ProgressFoodLogComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-progress-food-log',
  template: _progress_food_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_progress_food_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProgressFoodLogComponent);


/***/ }),

/***/ 23574:
/*!************************************************************************!*\
  !*** ./src/app/main/progress/progress-food/progress-food.component.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressFoodComponent": () => (/* binding */ ProgressFoodComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_food_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-food.component.html?ngResource */ 91007);
/* harmony import */ var _progress_food_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-food.component.scss?ngResource */ 24143);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 83910);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);











let ProgressFoodComponent = class ProgressFoodComponent {
    constructor(_fb, _us, _sb, _as) {
        this._fb = _fb;
        this._us = _us;
        this._sb = _sb;
        this._as = _as;
        this.dispatchView = false;
        this.loggedDates = [];
        this.logs = [];
        this.logsFA = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        this._me$ = this._as.getMe();
        // Needs to be an arrow function as per Angular Material since it's a property
        this.dateFilter = (date) => {
            const res = this.loggedDates.filter((i) => i.valueOf() === date.valueOf());
            return res.length === 0;
        };
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)((me) => {
            this.me = me;
            return this._us.getUserFoodLogs(this.me.uid, this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.catchError)((err) => {
            console.log(err);
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_9__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.take)(1))
            .subscribe((logs) => {
            console.log(logs);
            this.setInitialLogsFA(logs);
            this.dispatchView = true;
        });
        this.maxDate = moment__WEBPACK_IMPORTED_MODULE_2__().endOf('day').toDate();
    }
    // *
    // * Food Log - Date
    // *
    onDailyDateSelection(event) {
        const date = event.value;
        this.addNewLog(date);
    }
    // *
    // * Food Log - FormArray
    // *
    setInitialLogsFA(logs) {
        this.logsFA.clear();
        logs.map((log) => {
            this.addExistingLog(log);
        });
    }
    addExistingLog(log) {
        const fg = this._fb.group({ ...log });
        this.logsFA.push(fg);
        this.loggedDates.push(log.date);
    }
    addNewLog(date) {
        const log = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserFoodLog();
        log.date = date;
        log.uid = this.me.uid;
        log.membershipKey = this.me.membershipKey;
        log.isActive = true;
        log.recStatus = true;
        const fg = this._fb.group({ ...log });
        this.logsFA.insert(0, fg);
        this.loggedDates.push(date);
    }
    removeLog(e, i) {
        this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
        this.logsFA.removeAt(i);
    }
};
ProgressFoodComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService }
];
ProgressFoodComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-progress-food',
        template: _progress_food_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_progress_food_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProgressFoodComponent);



/***/ }),

/***/ 61062:
/*!**********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-measurement-log/progress-measurement-log.component.ts ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressMeasurementLogComponent": () => (/* binding */ ProgressMeasurementLogComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_measurement_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-measurement-log.component.html?ngResource */ 67770);
/* harmony import */ var _progress_measurement_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-measurement-log.component.scss?ngResource */ 44482);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);












let ProgressMeasurementLogComponent = class ProgressMeasurementLogComponent {
  constructor(_uts, _gs, _sb, _shs, _dialog) {
    this._uts = _uts;
    this._gs = _gs;
    this._sb = _sb;
    this._shs = _shs;
    this._dialog = _dialog;
    this.deleting = false;
    this.dispatchView = false;
    this.hasChanged = false;
    this.isNew = false;
    this.isUpdated = false;
    this.saving = false;
    this.startMeasurement = null;
    this.deleteEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_7__.EventEmitter();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();
    this.dispatchView = true;
    this.logFG.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      this.hasChanged = !this._uts.shallowEqual(this.logFGInitial, val);
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  get dateFC() {
    return this.logFG.get('date');
  }

  get absFC() {
    return this.logFG.get('abs');
  }

  get chestFC() {
    return this.logFG.get('chest');
  }

  get hipsFC() {
    return this.logFG.get('hips');
  }

  get midArmFC() {
    return this.logFG.get('midArm');
  }

  get thighsFC() {
    return this.logFG.get('thighs');
  }

  get keyFC() {
    return this.logFG.get('key');
  }

  get uidFC() {
    return this.logFG.get('uid');
  }

  setInitialFG() {
    this.logFGInitial = this.logFG.value;
  }

  reset() {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  save() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.hasChanged || !_this.logFG.valid || _this.deleting) {
        return;
      }

      try {
        _this.saving = true;

        if (_this.isNew) {
          const key = _this._gs.getNewKey('userMeasurementLogs', true, _this.uidFC.value);

          _this.keyFC.setValue(key);
        }

        yield _this._gs.setData(_this.uidFC.value, 'userMeasurementLogs', _this.logFG.value, _this.isNew, ['date'], true, _this.keyFC.value);
        _this.saving = false;
        _this.isUpdated = true;

        _this.setInitialFG();

        _this.hasChanged = false;

        _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessMessages.SAVE_OK, 2000);
      } catch (err) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SAVE_FAILED);

        _this.saving = false;
      }
    })();
  }

  delete() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this2.saving) {
        return;
      }

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to delete the log?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this2._shs.setDialogBox(dialogBox);

      const dialogRef = _this2._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            _this2.deleting = true;

            if (!_this2.isNew || _this2.isUpdated) {
              yield _this2._gs.setData(_this2.uidFC.value, 'userMeasurementLogs', {
                recStatus: false
              }, _this2.isNew, ['date'], true, _this2.keyFC.value);
            }

            _this2.deleteEvent.emit({
              date: _this2.dateFC.value
            });
          } catch (err) {
            _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);

            _this2.deleting = false;
          }
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  }

};

ProgressMeasurementLogComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.GeneralService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog
}];

ProgressMeasurementLogComponent.propDecorators = {
  logFG: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  startWeight: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  deleteEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Output
  }]
};
ProgressMeasurementLogComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-progress-measurement-log',
  template: _progress_measurement_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_progress_measurement_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProgressMeasurementLogComponent);


/***/ }),

/***/ 11534:
/*!**************************************************************************************!*\
  !*** ./src/app/main/progress/progress-measurement/progress-measurement.component.ts ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressMeasurementComponent": () => (/* binding */ ProgressMeasurementComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_measurement_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-measurement.component.html?ngResource */ 82278);
/* harmony import */ var _progress_measurement_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-measurement.component.scss?ngResource */ 33658);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ 92340);
// ToDo:
// Firestore issue with userMeasurements collection due to bi-directional indices.
// This has caused some glitch in the collection and giving erratic behavior.
// Thus, Summary section is incomplete.












let ProgressMeasurementComponent = class ProgressMeasurementComponent {
    constructor(_fb, _uts, _us, _sb, _as) {
        this._fb = _fb;
        this._uts = _uts;
        this._us = _us;
        this._sb = _sb;
        this._as = _as;
        this.delta = '';
        this.dispatchView = false;
        this.hasChanged = false;
        this.imgURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__.environment.bodyMeasurementGuide.imgURL;
        this.latestMeasurement = null;
        this.loggedDates = [];
        this.logs = [];
        this.logsFA = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormArray([]);
        this.startMeasurement = null;
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
        // Needs to be an arrow function as per Angular Material since it's a property
        this.dateFilter = (date) => {
            const res = this.loggedDates.filter((i) => i.valueOf() === date.valueOf());
            return res.length === 0;
        };
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)((me) => {
            this.me = me;
            return this._us.getUserMeasurementLogs(this.me.uid, this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.catchError)((err) => {
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_11__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_12__.takeUntil)(this._notifier$))
            .subscribe((logs) => {
            this.logs = logs;
            this.setInitialLogsFA(logs);
            if (logs.length > 0) {
                this.setStartMeasurement();
            }
            this.trackSummaryLogs();
            this.dispatchView = true;
        });
        this.maxDate = moment__WEBPACK_IMPORTED_MODULE_2__().endOf('day').toDate();
    }
    // *
    // * Summary
    // *
    trackSummaryLogs() {
        this._us
            .getUserMeasurementLogLatest(this.me.uid, this.me.membershipKey)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.catchError)((err) => {
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_11__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_12__.takeUntil)(this._notifier$))
            .subscribe((arr) => {
            if (arr.length > 0) {
                this.setLatestMeasurement(arr[0]);
            }
            this.computeDelta();
        });
    }
    setStartMeasurement() {
        this.startMeasurement =
            this.logs[this.logs.length - 1].abs +
                this.logs[this.logs.length - 1].chest +
                this.logs[this.logs.length - 1].hips +
                this.logs[this.logs.length - 1].midArm +
                this.logs[this.logs.length - 1].thighs;
    }
    setLatestMeasurement(log) {
        this.latestMeasurement =
            log.abs + log.chest + log.hips + log.midArm + log.thighs;
    }
    computeDelta() {
        this.delta = this._uts.computeDifference(this.latestMeasurement, this.startMeasurement);
    }
    // *
    // * Measurement Log - Date
    // *
    onDailyDateSelection(event) {
        const date = event.value;
        this.addNewLog(date);
    }
    // *
    // * Measurement Log - FormArray
    // *
    setInitialLogsFA(logs) {
        this.logsFA.clear();
        logs.map((log) => {
            this.addExistingLog(log);
        });
    }
    addExistingLog(log) {
        const fg = this._fb.group({ ...log });
        fg.get('abs').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('chest').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('hips').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('midArm').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('thighs').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        this.logsFA.push(fg);
        this.loggedDates.push(log.date);
    }
    addNewLog(date) {
        const log = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserMeasurementLog();
        log.date = date;
        log.uid = this.me.uid;
        log.membershipKey = this.me.membershipKey;
        log.isActive = true;
        log.recStatus = true;
        const fg = this._fb.group({ ...log });
        fg.get('abs').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('chest').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('hips').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('midArm').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        fg.get('thighs').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/)
        ]);
        this.logsFA.insert(0, fg);
        this.loggedDates.push(date);
    }
    removeLog(e, i) {
        this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
        this.logsFA.removeAt(i);
        this.logs.splice(i, 1);
        if (this.logs.length > 0) {
            this.setStartMeasurement();
        }
    }
};
ProgressMeasurementComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService }
];
ProgressMeasurementComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_14__.Component)({
        selector: 'app-progress-measurement',
        template: _progress_measurement_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_progress_measurement_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProgressMeasurementComponent);



/***/ }),

/***/ 8266:
/*!**********************************************************************************!*\
  !*** ./src/app/main/progress/progress-photo-log/progress-photo-log.component.ts ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressPhotoLogComponent": () => (/* binding */ ProgressPhotoLogComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_photo_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-photo-log.component.html?ngResource */ 25513);
/* harmony import */ var _progress_photo_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-photo-log.component.scss?ngResource */ 66800);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-lightbox */ 25015);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);














let ProgressPhotoLogComponent = class ProgressPhotoLogComponent {
  constructor(_sb, _lightbox, _lightboxConfig, _sts, _gs, _as, _uts, _fb, _shs, _dialog) {
    this._sb = _sb;
    this._lightbox = _lightbox;
    this._lightboxConfig = _lightboxConfig;
    this._sts = _sts;
    this._gs = _gs;
    this._as = _as;
    this._uts = _uts;
    this._fb = _fb;
    this._shs = _shs;
    this._dialog = _dialog;
    this.acceptedFileTypes = '.png, .jpeg, .jpg';
    this.backImagesArr = [];
    this.deleting = false;
    this.deletingFile = false;
    this.dispatchView = false;
    this.frontalImagesArr = [];
    this.isNew = false;
    this.lateralImagesArr = [];
    this.loading = false;
    this.lightboxAlbum = [];
    this.uploadCollectionName = 'userPhotoLogFiles';
    this.uploadDirPath = 'photoLogs';
    this.uploadFileNamePartial = 'MEALpyramid_PL';
    this.uploadFileNamePrefix = '';
    this.uploadingBack = false;
    this.uploadingFrontal = false;
    this.uploadingLateral = false;
    this.deleteEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
  }

  ngOnInit() {
    this.isNew = !this.keyFC.value;

    this._as.getMe().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.catchError)(() => {
      this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_11__.EMPTY;
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_12__.takeUntil)(this._notifier$)).subscribe(user => {
      this.me = user;
      this.uploadFileNamePrefix = `${this.uploadFileNamePartial}`;
      this.dispatchView = true;
    });

    if (this.frontalImagesFA.controls.length > 0) {
      this.frontalImagesArr = this.frontalImagesFA.value;
    }

    if (this.lateralImagesFA.controls.length > 0) {
      this.lateralImagesArr = this.lateralImagesFA.value;
    }

    if (this.backImagesFA.controls.length > 0) {
      this.backImagesArr = this.backImagesFA.value;
    }

    this.lightboxInit();
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  get frontalImagesFA() {
    return this.logFG.get('frontalImages');
  }

  get lateralImagesFA() {
    return this.logFG.get('lateralImages');
  }

  get backImagesFA() {
    return this.logFG.get('backImages');
  }

  get dateFC() {
    return this.logFG.get('date');
  }

  get keyFC() {
    return this.logFG.get('key');
  }

  get uidFC() {
    return this.logFG.get('uid');
  }

  get membershipKeyFC() {
    return this.logFG.get('membershipKey');
  }

  delete() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to delete?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this._shs.setDialogBox(dialogBox);

      const dialogRef = _this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_7__.DialogBoxComponent);

      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            _this.deleting = true;

            if (!_this.isNew) {
              yield _this._gs.setData(_this.uidFC.value, 'userPhotoLogs', {
                recStatus: false
              }, _this.isNew, ['date'], true, _this.keyFC.value);
            } // ToDo: Delete all images


            _this.deleteEvent.emit({
              date: _this.dateFC.value
            });
          } catch (err) {
            _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.ErrorMessages.DELETE_FAILED);

            _this.deleting = false;
          }
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  } // *
  // * Images
  // *


  isAnyLoaderActive() {
    return this.deleting || this.uploadingFrontal || this.uploadingLateral || this.uploadingBack || this.deletingFile;
  }

  onAvailable(type) {
    switch (type) {
      case 'Frontal':
        this.uploadingFrontal = true;
        break;

      case 'Lateral':
        this.uploadingLateral = true;
        break;

      case 'Back':
        this.uploadingBack = true;
        break;

      default:
        break;
    }
  }

  onUploadComplete(obj, type) {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const mFileObj = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.MFilee2(obj, _this2._uts);

      switch (type) {
        case 'Frontal':
          _this2.frontalImagesArr.push(mFileObj);

          _this2.frontalImagesFA.push(_this2._fb.control(mFileObj));

          break;

        case 'Lateral':
          _this2.lateralImagesArr.push(mFileObj);

          _this2.lateralImagesFA.push(_this2._fb.control(mFileObj));

          break;

        case 'Back':
          _this2.backImagesArr.push(mFileObj);

          _this2.backImagesFA.push(_this2._fb.control(mFileObj));

          break;

        default:
          break;
      }

      try {
        if (_this2.isNew) {
          const key = _this2._gs.getNewKey('userPhotoLogs', true, _this2.uidFC.value);

          _this2.keyFC.setValue(key);
        }

        yield _this2._gs.setData(_this2.uidFC.value, 'userPhotoLogs', _this2.logFG.value, _this2.isNew, ['date'], true, _this2.keyFC.value);
        _this2.isNew = false;
      } catch (err) {
        _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.ErrorMessages.FILE_NOT_UPLOADED);
      }

      switch (type) {
        case 'Frontal':
          _this2.uploadingFrontal = false;
          break;

        case 'Lateral':
          _this2.uploadingLateral = false;
          break;

        case 'Back':
          _this2.uploadingBack = false;
          break;

        default:
          break;
      }
    })();
  }

  deleteFile(type, i) {
    var _this3 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this3.deletingFile = true;
      let fileObj;

      try {
        switch (type) {
          case 'Frontal':
            fileObj = _this3.frontalImagesFA.at(i).value;

            _this3.frontalImagesFA.removeAt(i);

            break;

          case 'Lateral':
            fileObj = _this3.lateralImagesFA.at(i).value;

            _this3.lateralImagesFA.removeAt(i);

            break;

          case 'Back':
            fileObj = _this3.backImagesFA.at(i).value;

            _this3.backImagesFA.removeAt(i);

            break;

          default:
            break;
        }

        yield _this3._gs.setData(_this3.uidFC.value, 'userPhotoLogs', _this3.logFG.value, false, ['date'], true, _this3.keyFC.value); // ToDo: Mark recStatus = false in 'photoLogFiles' collection

        _this3._sts.delete(fileObj.filePath).then().catch();

        _this3.deletingFile = false;
      } catch (error) {
        _this3._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.ErrorMessages.CANNOT_DELETE_FILE);

        _this3.deletingFile = false;
      }
    })();
  } // *
  // * Lightbox
  // *


  lightboxInit() {
    Object.assign(this._lightboxConfig, src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.LightboxConfigs);
  }

  openLightbox(src) {
    this.lightboxAlbum.length = 0;
    this.lightboxAlbum.push({
      src
    });

    this._lightbox.open(this.lightboxAlbum, 0);
  }

};

ProgressPhotoLogComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SnackBarService
}, {
  type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_3__.Lightbox
}, {
  type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_3__.LightboxConfig
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.StorageService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.GeneralService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.UtilitiesService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SharedService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__.MatDialog
}];

ProgressPhotoLogComponent.propDecorators = {
  logFG: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input
  }],
  deleteEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output
  }]
};
ProgressPhotoLogComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_15__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-progress-photo-log',
  template: _progress_photo_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_progress_photo_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProgressPhotoLogComponent);


/***/ }),

/***/ 12204:
/*!**************************************************************************!*\
  !*** ./src/app/main/progress/progress-photo/progress-photo.component.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressPhotoComponent": () => (/* binding */ ProgressPhotoComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_photo_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-photo.component.html?ngResource */ 45366);
/* harmony import */ var _progress_photo_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-photo.component.scss?ngResource */ 57501);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 83910);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ 92340);












let ProgressPhotoComponent = class ProgressPhotoComponent {
    constructor(_fb, _us, _sb, _as) {
        this._fb = _fb;
        this._us = _us;
        this._sb = _sb;
        this._as = _as;
        this.dispatchView = false;
        this.imgURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__.environment.photoLogGuide.imgURL;
        this.loggedDates = [];
        this.logs = [];
        this.logsFA = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormArray([]);
        this._me$ = this._as.getMe();
        // Needs to be an arrow function as per Angular Material since it's a property
        this.dateFilter = (date) => {
            const res = this.loggedDates.filter((i) => i.valueOf() === date.valueOf());
            return res.length === 0;
        };
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.switchMap)((user) => {
            this.me = user;
            return this._us.getUserPhotoLogs(this.me.uid, this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.catchError)((err) => {
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_10__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.take)(1))
            .subscribe((logs) => {
            this.setInitialLogsFA(logs);
            this.dispatchView = true;
        });
        this.maxDate = moment__WEBPACK_IMPORTED_MODULE_2__().endOf('day').toDate();
    }
    // *
    // * Photo Log - Date
    // *
    onDailyDateSelection(event) {
        const date = event.value;
        this.addNewLog(date);
    }
    // *
    // * Photo Log - FormArray
    // *
    setInitialLogsFA(logs) {
        this.logsFA.clear();
        logs.map((log) => {
            this.addExistingLog(log);
        });
    }
    addExistingLog(log) {
        const fg = this._fb.group({});
        for (const key in log) {
            if (Array.isArray(log[key])) {
                if (log[key].length > 0) {
                    const fa = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormArray([]);
                    log[key].map((i) => {
                        const fileFG = this._fb.group(i);
                        fa.push(fileFG);
                    });
                    fg.addControl(key, fa);
                }
                else {
                    fg.addControl(key, this._fb.array([]));
                }
            }
            else {
                fg.addControl(key, this._fb.control(log[key]));
            }
        }
        this.logsFA.push(fg);
        this.loggedDates.push(log.date);
    }
    addNewLog(date) {
        const log = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserPhotoLog();
        log.date = date;
        log.uid = this.me.uid;
        log.membershipKey = this.me.membershipKey;
        log.isActive = true;
        log.recStatus = true;
        const fg = this._fb.group({});
        for (const key in log) {
            if (Array.isArray(log[key])) {
                fg.addControl(key, this._fb.array([]));
            }
            else {
                fg.addControl(key, this._fb.control(log[key]));
            }
        }
        this.logsFA.insert(0, fg);
        this.loggedDates.push(date);
    }
    removeLog(e, i) {
        this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
        this.logsFA.removeAt(i);
    }
};
ProgressPhotoComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService }
];
ProgressPhotoComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
        selector: 'app-progress-photo',
        template: _progress_photo_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_progress_photo_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProgressPhotoComponent);



/***/ }),

/***/ 49040:
/*!**********************************************************!*\
  !*** ./src/app/main/progress/progress-routing.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressRoutingModule": () => (/* binding */ ProgressRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _progress_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress.component */ 59015);




const routes = [
    {
        path: '**',
        component: _progress_component__WEBPACK_IMPORTED_MODULE_0__.ProgressComponent
    }
];
let ProgressRoutingModule = class ProgressRoutingModule {
};
ProgressRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], ProgressRoutingModule);



/***/ }),

/***/ 11158:
/*!************************************************************************************!*\
  !*** ./src/app/main/progress/progress-weight-log/progress-weight-log.component.ts ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressWeightLogComponent": () => (/* binding */ ProgressWeightLogComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_weight_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-weight-log.component.html?ngResource */ 82709);
/* harmony import */ var _progress_weight_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-weight-log.component.scss?ngResource */ 22309);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);












let ProgressWeightLogComponent = class ProgressWeightLogComponent {
  constructor(_uts, _gs, _shs, _sb, _dialog) {
    this._uts = _uts;
    this._gs = _gs;
    this._shs = _shs;
    this._sb = _sb;
    this._dialog = _dialog;
    this.deleting = false;
    this.delta = '';
    this.dispatchView = false;
    this.hasChanged = false;
    this.isNew = false;
    this.isUpdated = false;
    this.saving = false;
    this.deleteEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_7__.EventEmitter();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();
    this.dispatchView = true;
    this.logFG.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(val => {
      this.hasChanged = !this._uts.shallowEqual(this.logFGInitial, val);
      this.delta = this._uts.computeDifference(this.weightFC.value, this.startWeight);
    });
  }

  ngOnChanges(changes) {
    const {
      startWeight
    } = changes;

    if (startWeight) {
      this.delta = this._uts.computeDifference(this.weightFC.value, this.startWeight);
    }
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  get dateFC() {
    return this.logFG.get('date');
  }

  get weightFC() {
    return this.logFG.get('weight');
  }

  get keyFC() {
    return this.logFG.get('key');
  }

  get uidFC() {
    return this.logFG.get('uid');
  }

  setInitialFG() {
    this.logFGInitial = this.logFG.value;
  }

  reset() {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  save() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.hasChanged || !_this.logFG.valid || _this.deleting) {
        return;
      }

      try {
        _this.saving = true;

        if (_this.isNew) {
          const key = _this._gs.getNewKey('userWeightLogs', true, _this.uidFC.value);

          _this.keyFC.setValue(key);
        }

        yield _this._gs.setData(_this.uidFC.value, 'userWeightLogs', _this.logFG.value, _this.isNew, ['date'], true, _this.keyFC.value);
        _this.saving = false;
        _this.isUpdated = true;

        _this.setInitialFG();

        _this.hasChanged = false;

        _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessMessages.SAVE_OK, 2000);
      } catch (err) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SAVE_FAILED);

        _this.saving = false;
      }
    })();
  }

  delete() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this2.saving) {
        return;
      }

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to delete the log?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this2._shs.setDialogBox(dialogBox);

      const dialogRef = _this2._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            _this2.deleting = true;

            if (!_this2.isNew || _this2.isUpdated) {
              yield _this2._gs.setData(_this2.uidFC.value, 'userWeightLogs', {
                recStatus: false
              }, _this2.isNew, ['date'], true, _this2.keyFC.value);
            }

            _this2.deleteEvent.emit({
              date: _this2.dateFC.value
            });
          } catch (err) {
            _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.DELETE_FAILED);

            _this2.deleting = false;
          }
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  }

};

ProgressWeightLogComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.GeneralService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog
}];

ProgressWeightLogComponent.propDecorators = {
  logFG: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  startWeight: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input
  }],
  deleteEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Output
  }]
};
ProgressWeightLogComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-progress-weight-log',
  template: _progress_weight_log_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_progress_weight_log_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProgressWeightLogComponent);


/***/ }),

/***/ 89751:
/*!****************************************************************************!*\
  !*** ./src/app/main/progress/progress-weight/progress-weight.component.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressWeightComponent": () => (/* binding */ ProgressWeightComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_weight_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-weight.component.html?ngResource */ 82730);
/* harmony import */ var _progress_weight_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress-weight.component.scss?ngResource */ 80755);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);











let ProgressWeightComponent = class ProgressWeightComponent {
    constructor(_fb, _uts, _us, _sb, _as) {
        this._fb = _fb;
        this._uts = _uts;
        this._us = _us;
        this._sb = _sb;
        this._as = _as;
        this.delta = '';
        this.dispatchView = false;
        this.hasChanged = false;
        this.latestWeight = null;
        this.loggedDates = [];
        this.logsFA = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        this.startWeight = null;
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();
        // Needs to be an arrow function as per Angular Material since it's a property
        this.dateFilter = (date) => {
            const res = this.loggedDates.filter((i) => i.valueOf() === Date.valueOf());
            return res.length === 0;
        };
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.switchMap)((me) => {
            this.me = me;
            this.initProgressFG();
            this.trackProgressFG();
            return this._us.getUserWeightLogs(this.me.uid, this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.catchError)((err) => {
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_10__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.takeUntil)(this._notifier$))
            .subscribe((logs) => {
            this.setInitialLogsFA(logs);
            this.trackLatestLog();
            this.dispatchView = true;
        });
        this.maxDate = moment__WEBPACK_IMPORTED_MODULE_2__().endOf('day').toDate();
    }
    // *
    // * Summary
    // *
    trackLatestLog() {
        this._us
            .getUserWeightLogLatest(this.me.uid, this.me.membershipKey)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.catchError)((err) => {
            this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_10__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.takeUntil)(this._notifier$))
            .subscribe((logs) => {
            this.latestWeight = logs.length > 0 ? logs[0].weight : null;
            this.computeDelta();
        });
    }
    computeDelta() {
        this.delta = this._uts.computeDifference(this.latestWeight, this.startWeight);
    }
    // *
    // * Progress
    // *
    initProgressFG() {
        this.progressFG = this._fb.group({
            startDate: [this.me.weightProgress.startDate],
            startWeight: [
                this.me.weightProgress.startWeight,
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/)
                ]
            ],
            targetDate: [this.me.weightProgress.targetDate],
            targetWeight: [
                this.me.weightProgress.targetWeight,
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/)
                ]
            ]
        });
        this.progressFGInitial = this.progressFG.value;
        this.startWeight = this.startWeightFC.value;
    }
    get startWeightFC() {
        return this.progressFG.get('startWeight');
    }
    get startDateFC() {
        return this.progressFG.get('startDate');
    }
    get targetWeightFC() {
        return this.progressFG.get('targetWeight');
    }
    get targetDateFC() {
        return this.progressFG.get('targetDate');
    }
    trackProgressFG() {
        this.progressFG.valueChanges
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.takeUntil)(this._notifier$))
            .subscribe((val) => {
            this.hasChanged = !this._uts.shallowEqual(this.progressFGInitial, val);
        });
    }
    onReset() {
        if (!this.hasChanged) {
            return;
        }
        this.progressFG.reset(this.progressFGInitial);
        this.progressFG.markAsPristine();
    }
    onSave() {
        if (!this.hasChanged ||
            !this.progressFG.valid ||
            (this.startDateFC.value &&
                !this._uts.isValidDate(this.startDateFC.value)) ||
            (this.targetDateFC.value &&
                !this._uts.isValidDate(this.targetDateFC.value))) {
            return;
        }
        this._us.setUser(this.me.uid, {
            weightProgress: this.progressFG.value
        });
        this.hasChanged = false;
        this.progressFGInitial = this.progressFG.value;
        this.startWeight = this.startWeightFC.value;
        this.computeDelta();
    }
    // *
    // * Weight Log - Date
    // *
    onDailyDateSelection(event) {
        const date = event.value;
        this.addNewLog(date);
    }
    // *
    // * Weight Log - FormArray
    // *
    setInitialLogsFA(logs) {
        this.logsFA.clear();
        logs.map((log) => {
            this.addExistingLog(log);
        });
    }
    addExistingLog(log) {
        const fg = this._fb.group({ ...log });
        fg.get('weight').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/)
        ]);
        this.logsFA.push(fg);
        this.loggedDates.push(log.date);
    }
    addNewLog(date) {
        const log = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserWeightLog();
        log.date = date;
        log.uid = this.me.uid;
        log.membershipKey = this.me.membershipKey;
        log.isActive = true;
        log.recStatus = true;
        const fg = this._fb.group({ ...log });
        fg.get('weight').setValidators([
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/)
        ]);
        this.logsFA.insert(0, fg);
        this.loggedDates.push(date);
    }
    removeLog(e, i) {
        this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
        this.logsFA.removeAt(i);
    }
};
ProgressWeightComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService }
];
ProgressWeightComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
        selector: 'app-progress-weight',
        template: _progress_weight_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_progress_weight_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProgressWeightComponent);



/***/ }),

/***/ 59015:
/*!*****************************************************!*\
  !*** ./src/app/main/progress/progress.component.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressComponent": () => (/* binding */ ProgressComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _progress_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress.component.html?ngResource */ 78115);
/* harmony import */ var _progress_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress.component.scss?ngResource */ 7744);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);










let ProgressComponent = class ProgressComponent {
  constructor(_as, _dialog, // private _us: UserService,
  _sb, // private _sts: StaticDataService,
  _shs) {
    this._as = _as;
    this._dialog = _dialog;
    this._sb = _sb;
    this._shs = _shs;
    this.dispatchView = false; // foodLogs: any[];
    // imgURL = environment.photoLogGuide.imgURL;

    this.isPackageSubscribed = false;
    this.loading = false; // photoLogs: any[];
    // recallLogs: any[];

    this.tabSelected = 0;
    this.segment = 'food';
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.switchMap)(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      return this._shs.getProgressTabSelected();
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.catchError)(() => {
      this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_8__.EMPTY;
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(tab => {
      this.tabSelected = tab;
      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  } // isFoodLogAvailable(d: Date): boolean {
  //   const arr = this.foodLogs.filter((item) =>
  //     item.date.seconds
  //       ? item.date.toDate().valueOf() === d.valueOf()
  //       : item.date.valueOf() === d.valueOf()
  //   );
  //   return arr.length > 0;
  // }
  // isRecallLogAvailable(d: Date): boolean {
  //   const arr = this.recallLogs.filter((item) =>
  //     item.date.seconds
  //       ? item.date.toDate().valueOf() === d.valueOf()
  //       : item.date.valueOf() === d.valueOf()
  //   );
  //   return arr.length > 0;
  // }
  // isPhotoLogAvailable(d: Date): boolean {
  //   const arr = this.photoLogs.filter((item) =>
  //     item.date.seconds
  //       ? item.date.toDate().valueOf() === d.valueOf()
  //       : item.date.valueOf() === d.valueOf()
  //   );
  //   return arr.length > 0;
  // }
  // async onDailyDateSelection(
  //   event: MatDatepickerInputEvent<Date>
  // ): Promise<void> {
  //   if (!this.isFoodLogAvailable(event.value)) {
  //     this.loading = true;
  //     const ref = this._sts.getNewKey('userProgressDaily', true, this.me.uid);
  //     await this._sts.setData(
  //       this.me.uid,
  //       'userProgressDaily',
  //       {
  //         date: event.value,
  //         uid: this.me.uid,
  //         membershipKey: this.me.membershipKey,
  //         key: ref
  //       },
  //       true,
  //       ['date'],
  //       true,
  //       ref
  //     );
  //     this.foodLogs.unshift({ date: event.value, key: ref, isNew: true });
  //     this.loading = false;
  //   }
  // }
  // async onRecallDateSelection(
  //   event: MatDatepickerInputEvent<Date>
  // ): Promise<void> {
  //   if (!this.isRecallLogAvailable(event.value)) {
  //     this.loading = true;
  //     const ref = this._sts.getNewKey('userProgressRecall', true, this.me.uid);
  //     await this._sts.setData(
  //       this.me.uid,
  //       'userProgressRecall',
  //       {
  //         date: event.value,
  //         uid: this.me.uid,
  //         membershipKey: this.me.membershipKey,
  //         key: ref
  //       },
  //       true,
  //       ['date'],
  //       true,
  //       ref
  //     );
  //     this.recallLogs.unshift({ date: event.value, key: ref, isNew: true });
  //     this.loading = false;
  //   }
  // }
  // async onPhotoDateSelection(
  //   event: MatDatepickerInputEvent<Date>
  // ): Promise<void> {
  //   if (!this.isPhotoLogAvailable(event.value)) {
  //     this.loading = true;
  //     const ref = this._sts.getNewKey('userPhotoLogs', true, this.me.uid);
  //     await this._sts.setData(
  //       this.me.uid,
  //       'userPhotoLogs',
  //       {
  //         frontalImages: [],
  //         lateralImages: [],
  //         backImages: [],
  //         date: event.value,
  //         uid: this.me.uid,
  //         membershipKey: this.me.membershipKey,
  //         key: ref
  //       },
  //       true,
  //       ['date'],
  //       true,
  //       ref
  //     );
  //     this.photoLogs.unshift({ date: event.value, key: ref, isNew: true });
  //     this.loading = false;
  //   }
  // }


  showDialog() {
    this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_4__.GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: {
        step: this.me.onboardingStep || 0
      }
    });
  }

};

ProgressComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.SharedService
}];

ProgressComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-progress',
  template: _progress_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
  styles: [_progress_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
})], ProgressComponent);


/***/ }),

/***/ 91080:
/*!**************************************************!*\
  !*** ./src/app/main/progress/progress.module.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProgressModule": () => (/* binding */ ProgressModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _progress_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./progress-routing.module */ 49040);
/* harmony import */ var _progress_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./progress.component */ 59015);
/* harmony import */ var _progress_food_progress_food_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./progress-food/progress-food.component */ 23574);
/* harmony import */ var _progress_food_log_progress_food_log_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./progress-food-log/progress-food-log.component */ 67571);
/* harmony import */ var _progress_weight_progress_weight_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./progress-weight/progress-weight.component */ 89751);
/* harmony import */ var _progress_weight_log_progress_weight_log_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./progress-weight-log/progress-weight-log.component */ 11158);
/* harmony import */ var _progress_measurement_progress_measurement_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./progress-measurement/progress-measurement.component */ 11534);
/* harmony import */ var _progress_measurement_log_progress_measurement_log_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./progress-measurement-log/progress-measurement-log.component */ 61062);
/* harmony import */ var _progress_feedback_progress_feedback_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./progress-feedback/progress-feedback.component */ 54284);
/* harmony import */ var _progress_feedback_log_progress_feedback_log_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./progress-feedback-log/progress-feedback-log.component */ 41822);
/* harmony import */ var _progress_photo_progress_photo_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./progress-photo/progress-photo.component */ 12204);
/* harmony import */ var _progress_photo_log_progress_photo_log_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./progress-photo-log/progress-photo-log.component */ 8266);
/* harmony import */ var src_app_shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/app/shared/layout/layout.module */ 68634);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-lightbox */ 25015);
/* harmony import */ var src_app_shared_button_file_upload__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! src/app/shared/button-file-upload */ 28458);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);
/* harmony import */ var src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! src/app/shared/material-design */ 12497);
/* harmony import */ var src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! src/app/shared/shared-components */ 35886);
/* harmony import */ var src_app_shared_upload_file__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! src/app/shared/upload-file */ 74355);
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/tabs */ 15892);


























let ProgressModule = class ProgressModule {
};
ProgressModule = (0,tslib__WEBPACK_IMPORTED_MODULE_19__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_20__.NgModule)({
        declarations: [
            _progress_component__WEBPACK_IMPORTED_MODULE_1__.ProgressComponent,
            _progress_food_progress_food_component__WEBPACK_IMPORTED_MODULE_2__.ProgressFoodComponent,
            _progress_food_log_progress_food_log_component__WEBPACK_IMPORTED_MODULE_3__.ProgressFoodLogComponent,
            _progress_weight_progress_weight_component__WEBPACK_IMPORTED_MODULE_4__.ProgressWeightComponent,
            _progress_weight_log_progress_weight_log_component__WEBPACK_IMPORTED_MODULE_5__.ProgressWeightLogComponent,
            _progress_measurement_progress_measurement_component__WEBPACK_IMPORTED_MODULE_6__.ProgressMeasurementComponent,
            _progress_measurement_log_progress_measurement_log_component__WEBPACK_IMPORTED_MODULE_7__.ProgressMeasurementLogComponent,
            _progress_feedback_progress_feedback_component__WEBPACK_IMPORTED_MODULE_8__.ProgressFeedbackComponent,
            _progress_feedback_log_progress_feedback_log_component__WEBPACK_IMPORTED_MODULE_9__.ProgressFeedbackLogComponent,
            _progress_photo_progress_photo_component__WEBPACK_IMPORTED_MODULE_10__.ProgressPhotoComponent,
            _progress_photo_log_progress_photo_log_component__WEBPACK_IMPORTED_MODULE_11__.ProgressPhotoLogComponent,
            // FoodLogComponent,
            // WeightLogComponent,
            // MeasumementLogComponent,
            // PhotoLogComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_21__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_22__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_23__.IonicModule,
            _progress_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProgressRoutingModule,
            src_app_shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_12__.LayoutModule,
            src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_15__.GetStartedModule,
            src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_16__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_24__.FlexLayoutModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_22__.ReactiveFormsModule,
            ngx_lightbox__WEBPACK_IMPORTED_MODULE_13__.LightboxModule,
            src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_17__.SharedComponentsModule,
            src_app_shared_upload_file__WEBPACK_IMPORTED_MODULE_18__.UploadFileModule,
            src_app_shared_button_file_upload__WEBPACK_IMPORTED_MODULE_14__.ButtonFileUploadModule,
            _angular_material_tabs__WEBPACK_IMPORTED_MODULE_25__.MatTabsModule
        ]
    })
], ProgressModule);



/***/ }),

/***/ 98328:
/*!*****************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-feedback-log/progress-feedback-log.component.scss?ngResource ***!
  \*****************************************************************************************************/
/***/ ((module) => {

module.exports = "::ng-deep .parametersPanel .mat-expansion-panel-body {\n  padding: 0 6px;\n}\n\n.mat-expansion-panel-header-title,\n.mat-expansion-panel-header-description {\n  display: flex;\n  flex-grow: 1;\n  flex-basis: none;\n  margin-right: 16px;\n  align-items: center;\n}\n\n.mat-expansion-panel-header {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 0 14px;\n  border-radius: inherit;\n  transition: height 225ms cubic-bezier(0.4, 0, 0.2, 1);\n}\n\n::ng-deep .mat-form-field-infix {\n  width: auto !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2dyZXNzLWZlZWRiYWNrLWxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQUE7QUFDRjs7QUFFQTs7RUFFRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxREFBQTtBQUNGOztBQUVBO0VBQ0Usc0JBQUE7QUFDRiIsImZpbGUiOiJwcm9ncmVzcy1mZWVkYmFjay1sb2cuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLnBhcmFtZXRlcnNQYW5lbCAubWF0LWV4cGFuc2lvbi1wYW5lbC1ib2R5IHtcclxuICBwYWRkaW5nOiAwIDZweDtcclxufVxyXG5cclxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlLFxyXG4ubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItZGVzY3JpcHRpb24ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1ncm93OiAxO1xyXG4gIGZsZXgtYmFzaXM6IG5vbmU7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMCAxNHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IGluaGVyaXQ7XHJcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDIyNW1zIGN1YmljLWJlemllcigwLjQsIDAsIDAuMiwgMSk7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LWZvcm0tZmllbGQtaW5maXgge1xyXG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XHJcbn0iXX0= */";

/***/ }),

/***/ 45547:
/*!*********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-feedback/progress-feedback.component.scss?ngResource ***!
  \*********************************************************************************************/
/***/ ((module) => {

module.exports = "::ng-deep .parametersPanel .mat-expansion-panel-body {\n  padding: 0 6px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2dyZXNzLWZlZWRiYWNrLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtBQUNGIiwiZmlsZSI6InByb2dyZXNzLWZlZWRiYWNrLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOjpuZy1kZWVwIC5wYXJhbWV0ZXJzUGFuZWwgLm1hdC1leHBhbnNpb24tcGFuZWwtYm9keSB7XHJcbiAgcGFkZGluZzogMCA2cHg7XHJcbn0iXX0= */";

/***/ }),

/***/ 61758:
/*!*********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-food-log/progress-food-log.component.scss?ngResource ***!
  \*********************************************************************************************/
/***/ ((module) => {

module.exports = "::ng-deep .parametersPanel .mat-expansion-panel-body {\n  padding: 0 6px;\n}\n\n.mat-expansion-panel-header-title,\n.mat-expansion-panel-header-description {\n  display: flex;\n  flex-grow: 1;\n  flex-basis: none;\n  margin-right: 16px;\n  align-items: center;\n}\n\n.mat-expansion-panel-header {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 0 14px;\n  border-radius: inherit;\n  transition: height 225ms cubic-bezier(0.4, 0, 0.2, 1);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2dyZXNzLWZvb2QtbG9nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtBQUNGOztBQUVBOztFQUVFLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHFEQUFBO0FBQ0YiLCJmaWxlIjoicHJvZ3Jlc3MtZm9vZC1sb2cuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6Om5nLWRlZXAgLnBhcmFtZXRlcnNQYW5lbCAubWF0LWV4cGFuc2lvbi1wYW5lbC1ib2R5IHtcclxuICBwYWRkaW5nOiAwIDZweDtcclxufVxyXG5cclxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlLFxyXG4ubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItZGVzY3JpcHRpb24ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1ncm93OiAxO1xyXG4gIGZsZXgtYmFzaXM6IG5vbmU7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMCAxNHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IGluaGVyaXQ7XHJcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDIyNW1zIGN1YmljLWJlemllcigwLjQsIDAsIDAuMiwgMSk7XHJcbn0iXX0= */";

/***/ }),

/***/ 24143:
/*!*************************************************************************************!*\
  !*** ./src/app/main/progress/progress-food/progress-food.component.scss?ngResource ***!
  \*************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy1mb29kLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 44482:
/*!***********************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-measurement-log/progress-measurement-log.component.scss?ngResource ***!
  \***********************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy1tZWFzdXJlbWVudC1sb2cuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 33658:
/*!***************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-measurement/progress-measurement.component.scss?ngResource ***!
  \***************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy1tZWFzdXJlbWVudC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 66800:
/*!***********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-photo-log/progress-photo-log.component.scss?ngResource ***!
  \***********************************************************************************************/
/***/ ((module) => {

module.exports = "::ng-deep .mat-expansion-panel-header-description {\n  flex-grow: none;\n}\n\n::ng-deep .parametersPanel .mat-expansion-panel-body {\n  padding: 0 6px;\n}\n\n.mat-expansion-panel-header-title,\n.mat-expansion-panel-header-description {\n  display: flex;\n  flex-grow: 1;\n  flex-basis: none;\n  margin-right: 16px;\n  align-items: center;\n}\n\n.mat-expansion-panel-header {\n  display: flex;\n  flex-direction: row;\n  align-items: center;\n  padding: 0 14px;\n  border-radius: inherit;\n  transition: height 225ms cubic-bezier(0.4, 0, 0.2, 1);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2dyZXNzLXBob3RvLWxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7QUFDRjs7QUFDQTtFQUNFLGNBQUE7QUFFRjs7QUFDQTs7RUFFRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQUVGOztBQUNBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7RUFDQSxxREFBQTtBQUVGIiwiZmlsZSI6InByb2dyZXNzLXBob3RvLWxvZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjo6bmctZGVlcCAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItZGVzY3JpcHRpb24ge1xyXG4gIGZsZXgtZ3Jvdzogbm9uZTtcclxufVxyXG46Om5nLWRlZXAgLnBhcmFtZXRlcnNQYW5lbCAubWF0LWV4cGFuc2lvbi1wYW5lbC1ib2R5IHtcclxuICBwYWRkaW5nOiAwIDZweDtcclxufVxyXG5cclxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlLFxyXG4ubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItZGVzY3JpcHRpb24ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1ncm93OiAxO1xyXG4gIGZsZXgtYmFzaXM6IG5vbmU7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgcGFkZGluZzogMCAxNHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IGluaGVyaXQ7XHJcbiAgdHJhbnNpdGlvbjogaGVpZ2h0IDIyNW1zIGN1YmljLWJlemllcigwLjQsIDAsIDAuMiwgMSk7XHJcbn0iXX0= */";

/***/ }),

/***/ 57501:
/*!***************************************************************************************!*\
  !*** ./src/app/main/progress/progress-photo/progress-photo.component.scss?ngResource ***!
  \***************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy1waG90by5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 22309:
/*!*************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-weight-log/progress-weight-log.component.scss?ngResource ***!
  \*************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy13ZWlnaHQtbG9nLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 80755:
/*!*****************************************************************************************!*\
  !*** ./src/app/main/progress/progress-weight/progress-weight.component.scss?ngResource ***!
  \*****************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy13ZWlnaHQuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 7744:
/*!******************************************************************!*\
  !*** ./src/app/main/progress/progress.component.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9ncmVzcy5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 75638:
/*!*****************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-feedback-log/progress-feedback-log.component.html?ngResource ***!
  \*****************************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <mat-expansion-panel class=\"parametersPanel\">\r\n    <mat-expansion-panel-header>\r\n      <mat-panel-title>\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--2\">date_range</mat-icon>\r\n        <span class=\"eh-w-140 eh-ml-2\">{{dateFC.value | date:'E | d-MMM-y':'+0530'}}</span>\r\n        <span *ngIf=\"isNew\" class=\"e-txt e-txt--matred300 eh-txt-12 eh-txt-bold eh-ml-8\">\r\n          New!\r\n        </span>\r\n      </mat-panel-title>\r\n\r\n      <mat-panel-description fxLayoutAlign=\"end\">\r\n\r\n\r\n      </mat-panel-description>\r\n    </mat-expansion-panel-header>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\" fxLayoutAlign=\"end\">\r\n      <div *ngIf=\"hasChanged\" class=\"e-txt e-txt--matred600 eh-mr-6\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3\">warning</mat-icon>\r\n      </div>\r\n      <app-button-icon-title class=\"eh-w-20 eh-px-2 eh-mr-2\" buttonStyle=\"e-txt e-txt--matred600\"\r\n        icon=\"settings_backup_restore\" [disabled]=\"saving || deleting\" (onButtonClick)=\"reset()\">\r\n      </app-button-icon-title>\r\n\r\n      <app-button-icon-title class=\"eh-w-20 eh-px-2 eh-mr-2\" buttonStyle=\"e-txt e-txt--matgreen500\" icon=\"save\"\r\n        [loading]=\"deleting\" (onButtonClick)=\"save()\"></app-button-icon-title>\r\n\r\n      <app-button-icon icon=\"delete\" class=\"primary\" [loading]=\"deleting\" [disabled]=\"saving\"\r\n        (onButtonClick)=\"delete()\">\r\n      </app-button-icon>\r\n    </div>\r\n\r\n    <form [formGroup]=\"logFG\" autocomplete=\"off\">\r\n      <div class=\"e-title eh-mt-16\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">feedback</mat-icon>\r\n        <span>General</span>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8 eh-py-12\" fxLayout=\"column\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            Did you eat any meal out?\r\n          </div>\r\n\r\n          <div class=\"eh-ml-12 eh-py-8\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"isOutsideMeal\"\r\n              aria-label=\"Select an option\">\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                Yes\r\n              </mat-radio-button>\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                No\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-py-8\" fxLayout=\"row\" fxLayoutAlign=\"start\">\r\n          <div class=\"eh-txt eh-py-2\">\r\n            If yes, how many meals?\r\n          </div>\r\n\r\n          <div class=\"eh-py-8\">\r\n            <mat-form-field>\r\n              <input class=\"eh-w-100p\" matInput formControlName=\"outsideMealsCount\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8\" fxLayout=\"column\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            Any change in routine?\r\n          </div>\r\n          <div class=\"eh-ml-12\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"isRoutineChanged\"\r\n              aria-label=\"Select an option\">\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                Yes\r\n              </mat-radio-button>\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                No\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-py-6 eh-mb-4 eh-mt-8 \" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"eh-txt eh-py-2\">\r\n            If yes, please share details.\r\n          </div>\r\n          <div class=\"e-bg e-bg--eblue250 eh-mt-4 \">\r\n            <mat-form-field>\r\n              <textarea matInput formControlName=\"routineChangeDetails\" class=\"eh-hide-scrollbars\" matInput rows=\"5\"\r\n                cols=\"40\"></textarea>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8 eh-py-6\" fxLayout=\"column\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            Hungry between meals?\r\n          </div>\r\n          <div class=\"eh-ml-12\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"isHungryBetweenMeals\"\r\n              aria-label=\"Select an option\">\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                Yes\r\n              </mat-radio-button>\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                No\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8 eh-py-6\" fxLayout=\"column\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            Did you consume alcohol?\r\n          </div>\r\n          <div class=\"eh-ml-2\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"isAlcoholConsumed\"\r\n              aria-label=\"Select an option\">\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                Yes\r\n              </mat-radio-button>\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                No\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-py-6\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            If yes, how much?\r\n          </div>\r\n          <div class=\"eh-ml-12\">\r\n            <mat-form-field>\r\n              <input matInput formControlName=\"alcoholConsumedDetails\" type=\"text\" matInput rows=\"5\" cols=\"40\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            Water intake as recommended?\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"isHydrationFollowed\"\r\n              aria-label=\"Select an option\">\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                Yes\r\n              </mat-radio-button>\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                No\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\" class=\"eh-mt-8\">\r\n          <div class=\"eh-txt\">\r\n            Avg intake/day\r\n          </div>\r\n          <div class=\"eh-ml-4\">\r\n            <mat-form-field>\r\n              <input matInput formControlName=\"hydrationIntake\" type=\"text\" rows=\"5\" cols=\"40\">\r\n            </mat-form-field>\r\n          </div>\r\n          <div class=\"eh-ml-4\">\r\n            litre\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"e-title eh-mt-16\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">content_paste</mat-icon>\r\n        <span>Overall Feedback</span>\r\n      </div>\r\n\r\n      <div class=\"e-bg e-bg--eblue250 eh-mt-16 eh-ml-8 eh-px-4\">\r\n        <mat-form-field>\r\n          <textarea matInput formControlName=\"overallFeedback\" class=\"eh-hide-scrollbars\" rows=\"5\" cols=\"40\"></textarea>\r\n        </mat-form-field>\r\n      </div>\r\n\r\n      <div class=\"e-title eh-mt-16\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">feedback</mat-icon>\r\n        <span>Athlete Feedback</span>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-16\" fxLayout=\"column\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt\">\r\n            Did you have any competition?\r\n          </div>\r\n          <div class=\"\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"hadCompetition\"\r\n              aria-label=\"Select an option\">\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                Yes\r\n              </mat-radio-button>\r\n              <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                No\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"column\">\r\n          <div fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n            <div class=\"eh-txt eh-py-6\">\r\n              How was your performance?\r\n            </div>\r\n            <div class=\"e-bg e-bg--eblue250 eh-py-6\">\r\n              <mat-form-field>\r\n                <textarea matInput formControlName=\"competitionPerformance\" class=\"eh-hide-scrollbars\" matInput rows=\"5\"\r\n                  cols=\"40\"></textarea>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n            <div class=\"eh-txt eh-py-6\">\r\n              How was your recovery?\r\n            </div>\r\n\r\n            <div class=\"e-bg e-bg--eblue250 \">\r\n              <mat-form-field>\r\n                <textarea matInput formControlName=\"competitionRecovery\" class=\"eh-hide-scrollbars\" matInput rows=\"5\"\r\n                  cols=\"40\"></textarea>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8\" fxLayout=\"column\">\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n            <div class=\"eh-txt eh-py-6\">\r\n              Any upcoming competitions?\r\n            </div>\r\n            <div class=\"eh-ml-12\">\r\n              <mat-radio-group class=\"e-mat-radio-group\" matInput formControlName=\"upcomingCompetition\"\r\n                aria-label=\"Select an option\">\r\n                <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"true\">\r\n                  Yes\r\n                </mat-radio-button>\r\n                <mat-radio-button class=\"eh-ml-8 eh-txt-12\" [value]=\"false\">\r\n                  No\r\n                </mat-radio-button>\r\n              </mat-radio-group>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"eh-py-6\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n            <div class=\"eh-txt eh-py-6\">\r\n              If yes, please specify what & when.\r\n            </div>\r\n            <div class=\"e-bg e-bg--eblue250 eh-py-6\">\r\n              <mat-form-field>\r\n                <textarea matInput formControlName=\"upcomingCompetitionDetails\" class=\"eh-hide-scrollbars\" matInput\r\n                  rows=\"5\" cols=\"40\"></textarea>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </mat-expansion-panel>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 29505:
/*!*********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-feedback/progress-feedback.component.html?ngResource ***!
  \*********************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <app-button-icon-title class=\"eh-w-84\" title=\"Add Log\" buttonStyle=\"e-txt e-txt--warn\" icon=\"playlist_add\"\r\n      iconStyle=\"e-mat-icon--2\" (onButtonClick)=\"picker.open()\"></app-button-icon-title>\r\n\r\n    <mat-form-field appearance=\"none\">\r\n      <input matInput [matDatepicker]=\"picker\" [max]=\"maxDate\" [hidden]=\"true\" [matDatepickerFilter]=\"dateFilter\"\r\n        (dateInput)=\"onDailyDateSelection($event)\">\r\n      <mat-datepicker #picker></mat-datepicker>\r\n    </mat-form-field>\r\n  </div>\r\n\r\n  <mat-accordion class=\"e-mat-accordion\" multi>\r\n    <div *ngFor=\"let logFG of logsFA.controls; let i = index;\" class=\"eh-mt-8\">\r\n      <app-progress-feedback-log [logFG]=\"logFG\" (deleteEvent)=\"removeLog($event, i)\"></app-progress-feedback-log>\r\n    </div>\r\n  </mat-accordion>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 24280:
/*!*********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-food-log/progress-food-log.component.html?ngResource ***!
  \*********************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <mat-expansion-panel class=\"parametersPanel\">\r\n    <mat-expansion-panel-header>\r\n      <mat-panel-title>\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--2\">date_range</mat-icon>\r\n        <span class=\"eh-w-140 eh-ml-2\">{{dateFC.value | date:'E | d-MMM-y':'+0530'}}</span>\r\n        <span *ngIf=\"isNew\" class=\"e-txt e-txt--matred300 eh-txt-12 eh-txt-bold eh-ml-8\">\r\n          New!\r\n        </span>\r\n      </mat-panel-title>\r\n    </mat-expansion-panel-header>\r\n    <div class=\"eh-mt-8 eh-w-100p eh-h-100p\" fxLayout=\"row\" fxLayoutAlign=\"end center\">\r\n      <div *ngIf=\"hasChanged\" class=\"e-txt e-txt--matred600 eh-mr-6\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3\">warning</mat-icon>\r\n      </div>\r\n      <app-button-icon-title class=\"eh-w-20 eh-px-2\" buttonStyle=\"e-txt e-txt--matred600\" icon=\"settings_backup_restore\"\r\n        [disabled]=\"saving || deleting\" (onButtonClick)=\"reset()\">\r\n      </app-button-icon-title>\r\n      <app-button-icon-title class=\"eh-w-20 eh-px-2 eh-ml-4\" buttonStyle=\"e-txt e-txt--matgreen500\" icon=\"save\"\r\n        [loading]=\"deleting\" (onButtonClick)=\"save()\">\r\n      </app-button-icon-title>\r\n      <app-button-icon icon=\"delete\" class=\"eh-ml-4\" [loading]=\"deleting\" [disabled]=\"saving\"\r\n        (onButtonClick)=\"delete()\">\r\n      </app-button-icon>\r\n    </div>\r\n    <form [formGroup]=\"logFG\" autocomplete=\"off\" class=\"eh-mt-16\">\r\n      <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n        <div class=\"e-title eh-txt-bold\">\r\n          Meal : Breakfast\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n            <span>Time</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n              <input matInput formControlName=\"breakfastTime\" placeholder=\"hh:mm\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n            <span>Food</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"breakfastAte\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n            <span>Fluids</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"breakfastDrank\" type=\"number\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n        <div class=\"e-title eh-txt-bold\">\r\n          Meal : Mid-Morning\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n            <span>Time</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"midMorningTime\" placeholder=\"hh:mm\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n            <span>Food</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"midMorningAte\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n            <span>Fluids</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"midMorningDrank\" type=\"number\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n        <div class=\"e-title eh-txt-bold\">\r\n          Meal : Lunch\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n            <span>Time</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n              <input matInput formControlName=\"lunchTime\" placeholder=\"hh:mm\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n            <span>Food</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"lunchAte\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n            <span>Fluids</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"lunchDrank\" type=\"number\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n        <div class=\"e-title eh-txt-bold\">\r\n          Meal : Mid-Afternoon\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n            <span>Time</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n              <input matInput formControlName=\"midAfternoonTime\" placeholder=\"hh:mm\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n            <span>Food</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"midAfternoonAte\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n            <span>Fluids</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"midAfternoonDrank\" type=\"number\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n        <div class=\"e-title eh-txt-bold\">\r\n          Meal : Evening\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n            <span>Time</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n              <input matInput formControlName=\"eveningTime\" placeholder=\"hh:mm\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n            <span>Food</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"eveningAte\" type=\"text\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n          <div fxFlex=\"0 0 30%\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n            <span>Fluids</span>\r\n          </div>\r\n          <div fxFlex=\"0 0 70%\">\r\n            <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n              <input matInput formControlName=\"eveningDrank\" type=\"number\">\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"e-title eh-txt-bold\">\r\n            Meal : Dinner\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n              <span>Time</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n                <input matInput formControlName=\"dinnerTime\" placeholder=\"hh:mm\" type=\"text\">\r\n              </mat-form-field>\r\n\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n              <span>Food</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"dinnerAte\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n              <span>Fluids</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"dinnerDrank\" type=\"number\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"e-title eh-txt-bold\">\r\n            Meal : Bed-Time\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n              <span>Time</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n                <input matInput formControlName=\"bedTimeTime\" placeholder=\"hh:mm\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n              <span>Food</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n                <input matInput formControlName=\"bedTimeAte\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n              <span>Fluids</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"bedTimeDrank\" type=\"number\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"e-title eh-txt-bold\">\r\n            Meal : Pre-Workout\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n              <span>Time</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n                <input matInput formControlName=\"preWorkoutTime\" placeholder=\"hh:mm\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n              <span>Food</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"preWorkoutAte\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n              <span>Fluids</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"preWorkoutDrank\" type=\"number\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"e-title eh-txt-bold\">\r\n            Meal : Post-Workout\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n              <span>Time</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n                <input matInput formControlName=\"postWorkoutTime\" placeholder=\"hh:mm\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n              <span>Food</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"postWorkoutAte\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n              <span>Fluids</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"postWorkoutDrank\" type=\"number\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p  eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"e-title eh-txt-bold\">\r\n            Meal : Other\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">av_timer</mat-icon>\r\n              <span>Time</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\" floatLabel=\"never\">\r\n                <input matInput formControlName=\"otherTime\" placeholder=\"hh:mm\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">restaurant</mat-icon>\r\n              <span>Food</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"otherAte\" type=\"text\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">local_drink</mat-icon>\r\n              <span>Fluids</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"otherDrank\" type=\"number\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <!-- <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n            <div class=\"e-title eh-txt-bold\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">fitness_center</mat-icon>\r\n              <span>Exercise</span>\r\n            </div>\r\n            <div class=\"e-title\" fxFlex=\"0 0 35%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">list</mat-icon>\r\n              <span>Type</span>\r\n            </div>\r\n            <div class=\"e-title\" class=\"e-title eh-txt-bold\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">timer</mat-icon>\r\n              <span>Duration</span>\r\n            </div>\r\n            <div class=\"e-title\" fxFlex=\"0 0 25%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">network_check</mat-icon>\r\n              <span>Intensity</span>\r\n            </div>\r\n          </div> -->\r\n\r\n        <div class=\"eh-mt-8 eh-maxw-320 eh-h-100p eh-p-8\" fxLayout=\"column\" fxLayoutAlign=\"start\">\r\n          <div class=\"e-title eh-txt-bold\">\r\n            Exercise : Activity\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div class=\"eh-txt-bold\" fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">list</mat-icon>\r\n              <span>Type</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"exerciseType\" type=\"text\" placeholder=\"Walking, Running, ...\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div class=\"eh-txt-bold\" fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">timer</mat-icon>\r\n              <span>Duration</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"exerciseDuration\" type=\"text\" placeholder=\"15m, 1h, ...\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n            <div class=\"eh-txt-bold\" fxFlex=\"0 0 30%\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5 eh-mr-8\">network_check</mat-icon>\r\n              <span>Intensity</span>\r\n            </div>\r\n            <div fxFlex=\"0 0 70%\">\r\n              <mat-form-field class=\"eh-w-100p eh-ml-8\">\r\n                <input matInput formControlName=\"exerciseIntensity\" type=\"text\" placeholder=\"High, Medium, or Low\">\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </mat-expansion-panel>\r\n</ng-container>\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 91007:
/*!*************************************************************************************!*\
  !*** ./src/app/main/progress/progress-food/progress-food.component.html?ngResource ***!
  \*************************************************************************************/
/***/ ((module) => {

module.exports = "<!-- <ion-app>\r\n  <ion-content class=\"ion-padding\"> -->\r\n<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <app-button-icon-title class=\"eh-w-84\" title=\"Add Log\" buttonStyle=\"e-txt e-txt--warn\" icon=\"playlist_add\"\r\n      iconStyle=\"e-mat-icon--2\" (onButtonClick)=\"picker.open()\"></app-button-icon-title>\r\n\r\n\r\n    <mat-form-field appearance=\"none\">\r\n      <input matInput [matDatepicker]=\"picker\" [max]=\"maxDate\" [hidden]=\"true\" [matDatepickerFilter]=\"dateFilter\"\r\n        (dateInput)=\"onDailyDateSelection($event)\">\r\n      <mat-datepicker #picker></mat-datepicker>\r\n    </mat-form-field>\r\n  </div>\r\n\r\n  <mat-accordion class=\"e-mat-accordion\" multi>\r\n    <div *ngFor=\"let logFG of logsFA.controls; let i = index;\" class=\"eh-mt-8\">\r\n      <app-progress-food-log [logFG]=\"logFG\" (deleteEvent)=\"removeLog($event, i)\"></app-progress-food-log>\r\n    </div>\r\n  </mat-accordion>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>\r\n<!-- </ion-content>\r\n\r\n</ion-app> -->";

/***/ }),

/***/ 67770:
/*!***********************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-measurement-log/progress-measurement-log.component.html?ngResource ***!
  \***********************************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView\">\r\n  <div class=\"e-card2  eh-p-6 eh-my-8\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"flex-end center\">\r\n      <div class=\"eh-w-20\" fxLayout=\"row\" fxlayoutAlign=\"center center\">\r\n        <mat-icon *ngIf=\"hasChanged\" class=\"e-mat-icon e-mat-icon--3 e-txt e-txt--matred600\">warning</mat-icon>\r\n      </div>\r\n      <div class=\"eh-ml-8\">\r\n        <app-button-icon icon=\"settings_backup_restore\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matred600\"\r\n          [disabled]=\"saving || deleting\" (onButtonClick)=\"reset()\">\r\n        </app-button-icon>\r\n      </div>\r\n\r\n      <app-button-icon class=\"eh-ml-4\" icon=\"save\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matgreen500\" [loading]=\"saving\"\r\n        [disabled]=\"deleting\" (onButtonClick)=\"save()\">\r\n      </app-button-icon>\r\n\r\n      <app-button-icon class=\"eh-ml-4\" icon=\"delete\" [loading]=\"deleting\" [disabled]=\"saving\"\r\n        (onButtonClick)=\"delete()\">\r\n      </app-button-icon>\r\n    </div>\r\n    <div class=\"eh-py-6 eh-px-4\" fxLayout=\"column\" fxLayoutAlign=\"start\" [formGroup]=\"logFG\">\r\n      <div class=\"eh-txt-12\">\r\n        Date : {{dateFC.value | date:'mediumDate'}}\r\n      </div>\r\n\r\n      <div class=\"eh-py-4\" fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n        <div fxFlex=\"0 0 30%\" class=\"eh-txt-12 eh-mt-10\">\r\n          Chest (in)\r\n        </div>\r\n        <div fxFlex=\"0 0 70%\">\r\n          <input class=\"eh-w-140p eh-txt-12 e-brd e-brd--bottom e-brd--egrey400 \" type=\"number\" formControlName=\"chest\"\r\n            [ngClass]=\"{'e-brd--error': !chestFC.pristine && !chestFC.valid}\">\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-py-4\" fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n        <div fxFlex=\"0 0 30%\" class=\"eh-txt-12 eh-mt-10\">\r\n          Mid Arm (in)\r\n        </div>\r\n        <div fxFlex=\"0 0 70%\">\r\n          <input class=\"eh-w-140p eh-txt-12 e-brd e-brd--bottom e-brd--egrey400 \" type=\"number\" formControlName=\"midArm\"\r\n            [ngClass]=\"{'e-brd--error': !midArmFC.pristine && !midArmFC.valid}\">\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-py-4\" fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n        <div fxFlex=\"0 0 30%\" class=\"eh-txt-12 eh-mt-10\">\r\n          Abs (in\r\n        </div>\r\n        <div fxFlex=\"0 0 70%\">\r\n          <input class=\" eh-txt-12 e-brd e-brd--bottom e-brd--egrey400 \" type=\"number\" formControlName=\"abs\"\r\n            [ngClass]=\"{'e-brd--error': !absFC.pristine && !absFC.valid}\">\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-py-4\" fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n        <div fxFlex=\"0 0 30%\" class=\"eh-txt-12 eh-mt-10\">\r\n          Hips (in)\r\n        </div>\r\n        <div fxFlex=\"0 0 70%\">\r\n          <input class=\" eh-txt-12 e-brd e-brd--bottom e-brd--egrey400 \" type=\"number\" formControlName=\"hips\"\r\n            [ngClass]=\"{'e-brd--error': !hipsFC.pristine && !hipsFC.valid}\">\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-py-4\" fxLayout=\"row\" fxLayoutAlign=\"start baseline\">\r\n        <div fxFlex=\"0 0 30%\" class=\"eh-txt-12 eh-mt-10\">\r\n          Thighs (in)\r\n        </div>\r\n        <div fxFlex=\"0 0 70%\">\r\n          <input class=\" eh-txt-12 e-brd e-brd--bottom e-brd--egrey400 \" type=\"number\" formControlName=\"thighs\"\r\n            [ngClass]=\"{'e-brd--error': !thighsFC.pristine && !thighsFC.valid}\">\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ng-container>";

/***/ }),

/***/ 82278:
/*!***************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-measurement/progress-measurement.component.html?ngResource ***!
  \***************************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <div class=\"e-card2 e-card2--bg-eblue100 eh-p-8 eh-txt-12\">\r\n    <div class=\"eh-txt-bold\">Summary</div>\r\n    <div class=\"eh-mt-4\" fxLayout=\"row\">\r\n      <div>Start:</div>\r\n      <div class=\"eh-w-64 eh-ml-8 eh-txt-bold\">\r\n        {{startMeasurement ? startMeasurement + ' in' : '-'}}\r\n      </div>\r\n      <div class=\"eh-ml-16\">Latest:</div>\r\n      <div class=\"eh-w-64 eh-ml-8 eh-txt-bold\">\r\n        {{latestMeasurement ? latestMeasurement + ' in' : '-'}}\r\n      </div>\r\n      <div class=\"eh-ml-16\">Difference:</div>\r\n      <div class=\"eh-w-64 eh-ml-8 eh-txt-bold\">\r\n        {{delta !== null ? delta + ' in' : '-'}}\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"e-title eh-mt-16\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n    <span>Body Measurement Logs</span>\r\n  </div>\r\n\r\n  <div class=\"eh-mt-8\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <img class=\"eh-w-500\" [src]=\"imgURL\" alt=\"Body Measurement Guide\">\r\n  </div>\r\n\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <app-button-icon-title class=\"eh-w-84\" title=\"Add Log\" buttonStyle=\"e-txt e-txt--warn\" icon=\"playlist_add\"\r\n      iconStyle=\"e-mat-icon--2\" (onButtonClick)=\"picker3.open()\"></app-button-icon-title>\r\n\r\n    <mat-form-field appearance=\"none\">\r\n      <input matInput [matDatepicker]=\"picker3\" [max]=\"maxDate\" [hidden]=\"true\" [matDatepickerFilter]=\"dateFilter\"\r\n        (dateInput)=\"onDailyDateSelection($event)\">\r\n      <mat-datepicker #picker3></mat-datepicker>\r\n    </mat-form-field>\r\n  </div>\r\n  <!-- \r\n  <div class=\"e-brd e-brd--bottom eh-p-12\" fxLayout=\"row\">\r\n    <div class=\"eh-txt-12\" fxFlex=\"0 0 16%\">Date</div>\r\n    <div class=\"eh-txt-12\" fxFlex=\"0 0 14%\">Chest (in)</div>\r\n    <div class=\"eh-txt-12\" fxFlex=\"0 0 14%\">Mid Arm (in)</div>\r\n    <div class=\"eh-txt-12\" fxFlex=\"0 0 14%\">Abs (in)</div>\r\n    <div class=\"eh-txt-12\" fxFlex=\"0 0 14%\">Hips (in)</div>\r\n    <div class=\"eh-txt-12\" fxFlex=\"0 0 14%\">Thighs (in)</div>\r\n    <div fxFlex=\"0 0 14%\"></div>\r\n  </div> -->\r\n\r\n  <ng-container *ngFor=\"let logFG of logsFA.controls; let i = index;\">\r\n    <app-progress-measurement-log [logFG]=\"logFG\" [startMeasurement]=\"startMeasurement\"\r\n      (deleteEvent)=\"removeLog($event, i)\">\r\n    </app-progress-measurement-log>\r\n  </ng-container>\r\n\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 25513:
/*!***********************************************************************************************!*\
  !*** ./src/app/main/progress/progress-photo-log/progress-photo-log.component.html?ngResource ***!
  \***********************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <mat-expansion-panel #panel class=\"parametersPanel\">\r\n    <mat-expansion-panel-header>\r\n      <mat-panel-title>\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--2\">date_range</mat-icon>\r\n        <span class=\"eh-w-140 eh-ml-2\">{{dateFC.value | date:'E | d-MMM-y':'+0530'}}</span>\r\n        <span *ngIf=\"isNew\" class=\"e-txt e-txt--matred300 eh-txt-12 eh-txt-bold eh-ml-8\">\r\n          New!\r\n        </span>\r\n      </mat-panel-title>\r\n\r\n      <mat-panel-description fxLayoutAlign=\"end\">\r\n        <app-button-icon icon=\"delete\" [loading]=\"deleting\" [disabled]=\"isAnyLoaderActive()\" (onButtonClick)=\"delete()\">\r\n        </app-button-icon>\r\n      </mat-panel-description>\r\n    </mat-expansion-panel-header>\r\n\r\n    <div class=\"e-title eh-mt-16\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n      <span>Frontal Images</span>\r\n    </div>\r\n\r\n    <div class=\"eh-w-68 eh-mt-16 eh-ml-12\">\r\n      <app-button-file-upload [disabled]=\"isAnyLoaderActive()\" [uploadByName]=\"me.name\" [uploadByUID]=\"me.uid\"\r\n        [uploadCollectionName]=\"uploadCollectionName\" [uploadDirPath]=\"uploadDirPath\"\r\n        [uploadFileNamePrefix]=\"uploadFileNamePrefix\" [userMembershipKey]=\"membershipKeyFC.value\"\r\n        [userUID]=\"uidFC.value\" (availableEvent)=\"onAvailable('Frontal')\"\r\n        (uploadEvent)=\"onUploadComplete($event, 'Frontal')\">\r\n      </app-button-file-upload>\r\n    </div>\r\n\r\n    <div *ngIf=\"panel.expanded\" class=\"eh-mt-16\" fxLayout=\"row wrap\">\r\n      <div *ngFor=\"let image of frontalImagesFA.controls; let i = index;\"\r\n        class=\"eh-w-76 eh-ml-12 eh-mt-12 eh-pos-relative\">\r\n        <img [src]=\"image.value.fileURL\" class=\"eh-fit eh-csr-ptr\" alt=\"\" (click)=\"openLightbox(image.value.fileURL)\">\r\n        <div class=\"eh-pos-absolute\" style=\"top: -16px; right: -16px;\">\r\n          <app-button-icon icon=\"cancel\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matgrey600\"\r\n            [disabled]=\"isAnyLoaderActive()\" toolTip=\"Delete\" (onButtonClick)=\"deleteFile('Frontal', i)\">\r\n          </app-button-icon>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"e-title eh-mt-16\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n      <span>Lateral Images</span>\r\n    </div>\r\n\r\n    <div class=\"eh-w-68 eh-mt-16 eh-ml-12\">\r\n      <app-button-file-upload [disabled]=\"isAnyLoaderActive()\" [uploadByName]=\"me.name\" [uploadByUID]=\"me.uid\"\r\n        [uploadCollectionName]=\"uploadCollectionName\" [uploadDirPath]=\"uploadDirPath\"\r\n        [uploadFileNamePrefix]=\"uploadFileNamePrefix\" [userMembershipKey]=\"membershipKeyFC.value\"\r\n        [userUID]=\"uidFC.value\" (availableEvent)=\"onAvailable('Lateral')\"\r\n        (uploadEvent)=\"onUploadComplete($event, 'Lateral')\">\r\n      </app-button-file-upload>\r\n    </div>\r\n\r\n    <div *ngIf=\"panel.expanded\" class=\"eh-mt-16\" fxLayout=\"row wrap\">\r\n      <div *ngFor=\"let image of lateralImagesFA.controls; let i = index;\"\r\n        class=\"eh-w-76 eh-ml-12 eh-mt-12 eh-pos-relative\">\r\n        <img [src]=\"image.value.fileURL\" class=\"eh-fit eh-csr-ptr\" alt=\"\" (click)=\"openLightbox(image.value.fileURL)\">\r\n        <div class=\"eh-pos-absolute\" style=\"top: -16px; right: -16px;\">\r\n          <app-button-icon icon=\"cancel\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matgrey600\"\r\n            [disabled]=\"isAnyLoaderActive()\" toolTip=\"Delete\" (onButtonClick)=\"deleteFile('Lateral', i)\">\r\n          </app-button-icon>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"e-title eh-mt-16\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n      <span>Back Images</span>\r\n    </div>\r\n\r\n    <div class=\"eh-w-68 eh-mt-16 eh-ml-12\">\r\n      <app-button-file-upload [disabled]=\"isAnyLoaderActive()\" [uploadByName]=\"me.name\" [uploadByUID]=\"me.uid\"\r\n        [uploadCollectionName]=\"uploadCollectionName\" [uploadDirPath]=\"uploadDirPath\"\r\n        [uploadFileNamePrefix]=\"uploadFileNamePrefix\" [userMembershipKey]=\"membershipKeyFC.value\"\r\n        [userUID]=\"uidFC.value\" (availableEvent)=\"onAvailable('Back')\" (uploadEvent)=\"onUploadComplete($event, 'Back')\">\r\n      </app-button-file-upload>\r\n    </div>\r\n\r\n    <div *ngIf=\"panel.expanded\" class=\"eh-mt-16\" fxLayout=\"row wrap\">\r\n      <div *ngFor=\"let image of backImagesFA.controls; let i = index;\"\r\n        class=\"eh-w-76 eh-ml-12 eh-mt-12 eh-pos-relative\">\r\n        <img [src]=\"image.value.fileURL\" class=\"eh-fit eh-csr-ptr\" alt=\"\" (click)=\"openLightbox(image.value.fileURL)\">\r\n        <div class=\"eh-pos-absolute\" style=\"top: -16px; right: -16px;\">\r\n          <app-button-icon icon=\"cancel\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matgrey600\"\r\n            [disabled]=\"isAnyLoaderActive()\" toolTip=\"Delete\" (onButtonClick)=\"deleteFile('Back', i)\">\r\n          </app-button-icon>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </mat-expansion-panel>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 45366:
/*!***************************************************************************************!*\
  !*** ./src/app/main/progress/progress-photo/progress-photo.component.html?ngResource ***!
  \***************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <img class=\"eh-w-400\" [src]=\"imgURL\" alt=\"Photo Log Guide\">\r\n  </div>\r\n\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <app-button-icon-title class=\"eh-w-84\" title=\"Add Log\" buttonStyle=\"e-txt e-txt--warn\" icon=\"playlist_add\"\r\n      iconStyle=\"e-mat-icon--2\" (onButtonClick)=\"picker.open()\"></app-button-icon-title>\r\n\r\n    <mat-form-field appearance=\"none\">\r\n      <input matInput [matDatepicker]=\"picker\" [max]=\"maxDate\" [hidden]=\"true\" [matDatepickerFilter]=\"dateFilter\"\r\n        (dateInput)=\"onDailyDateSelection($event)\">\r\n      <mat-datepicker #picker></mat-datepicker>\r\n    </mat-form-field>\r\n  </div>\r\n\r\n  <mat-accordion class=\"e-mat-accordion\" multi>\r\n    <div *ngFor=\"let logFG of logsFA.controls; let i = index;\" class=\"eh-mt-8\">\r\n      <app-progress-photo-log [logFG]=\"logFG\" (deleteEvent)=\"removeLog($event, i)\"></app-progress-photo-log>\r\n    </div>\r\n  </mat-accordion>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 82709:
/*!*************************************************************************************************!*\
  !*** ./src/app/main/progress/progress-weight-log/progress-weight-log.component.html?ngResource ***!
  \*************************************************************************************************/
/***/ ((module) => {

module.exports = "\r\n<ng-container *ngIf=\"dispatchView\">\r\n  <div class=\"e-brd--left e-brd--right\">\r\n    <div class=\" eh-py-6 eh-px-12 eh-txt-center\" fxLayout=\"row\" fxLayoutAlign=\"start center\" [formGroup]=\"logFG\">\r\n      <div class=\"eh-txt-12\" fxFlex=\"0 0 33%\">\r\n        {{dateFC.value | date:'mediumDate'}}\r\n      </div>\r\n\r\n      <div fxFlex=\"0 0 33%\">\r\n        <input class=\"eh-w-100 eh-txt-12 e-brd e-brd--bottom e-brd--egrey400 eh-p-4 eh-txt-center\" type=\"number\"\r\n          formControlName=\"weight\" [ngClass]=\"{'e-brd--error': !weightFC.pristine && !weightFC.valid}\">\r\n      </div>\r\n\r\n      <div class=\"eh-txt-12 eh-txt-center\" fxFlex=\"0 0 33%\">{{delta !== null ? delta : '-'}}</div>\r\n    </div>\r\n\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"flex-end center\" class=\"e-brd e-brd--bottom\">\r\n      <div class=\"eh-w-20\" fxLayout=\"row\" fxlayoutAlign=\"center center\">\r\n        <mat-icon *ngIf=\"hasChanged\" class=\"e-mat-icon e-mat-icon--3 e-txt e-txt--matred600\">warning</mat-icon>\r\n      </div>\r\n\r\n      <div class=\"eh-pl-2\">\r\n        <app-button-icon icon=\"settings_backup_restore\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matred600\"\r\n          [disabled]=\"saving || deleting\" (onButtonClick)=\"reset()\">\r\n        </app-button-icon>\r\n      </div>\r\n\r\n      <app-button-icon class=\"eh-ml-4\" icon=\"save\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matgreen500\" [loading]=\"saving\"\r\n        [disabled]=\"deleting\" (onButtonClick)=\"save()\">\r\n      </app-button-icon>\r\n\r\n      <app-button-icon class=\"eh-ml-4\" icon=\"delete\" [loading]=\"deleting\" [disabled]=\"saving\"\r\n        (onButtonClick)=\"delete()\">\r\n      </app-button-icon>\r\n    </div>\r\n  </div>\r\n</ng-container>";

/***/ }),

/***/ 82730:
/*!*****************************************************************************************!*\
  !*** ./src/app/main/progress/progress-weight/progress-weight.component.html?ngResource ***!
  \*****************************************************************************************/
/***/ ((module) => {

module.exports = "<!-- <ion-app>\r\n  <ion-content class=\"ion-padding\"> -->\r\n<ng-container *ngIf=\"dispatchView else dataAwaitedBlk\">\r\n  <div class=\"e-card2 e-card2--bg-eblue100  eh-p-8 eh-txt-12\">\r\n    <div class=\"eh-txt-bold\">Summary</div>\r\n    <div class=\"eh-mt-4\" fxLayout=\"row\">\r\n      <div>Start:</div>\r\n      <div class=\"eh-w-64 eh-ml-8 eh-txt-bold\">\r\n        {{startWeight ? startWeight + ' kg' : '-'}}\r\n      </div>\r\n      <div class=\"eh-ml-16\">Latest:</div>\r\n      <div class=\"eh-w-64 eh-ml-8 eh-txt-bold\">\r\n        {{latestWeight ? latestWeight + ' kg' : '-'}}\r\n      </div>\r\n      <div class=\"eh-ml-16\">Difference:</div>\r\n      <div class=\"eh-w-64 eh-ml-8 eh-txt-bold\">\r\n        {{delta !== null ? delta + ' kg' : '-'}}\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"e-title eh-mt-16\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n    <span>Weight Progress Parameters</span>\r\n  </div>\r\n\r\n  <ng-container [formGroup]=\"progressFG\">\r\n    <div class=\"e-card2 e-card2--bg-eblue100 eh-mt-16 eh-pt-8 eh-px-8 eh-txt-12\">\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start\">\r\n        <div *ngIf=\"hasChanged\" class=\"e-txt e-txt--matred600 eh-mr-4\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--3\">warning</mat-icon>\r\n          <!-- <span class=\"eh-txt-12 eh-ml-4 eh-txt-bold\">Unsaved changes</span> -->\r\n        </div>\r\n\r\n        <app-button-icon-title class=\"eh-w-20 eh-px-2 eh-mr-2\" buttonStyle=\"e-txt e-txt--matred600\"\r\n          icon=\"settings_backup_restore\" [disabled]=\"loading\" (onButtonClick)=\"onReset()\"></app-button-icon-title>\r\n\r\n\r\n        <app-button-icon-title class=\"eh-w-20 eh-px-2 eh-mr-2\" buttonStyle=\"e-txt e-txt--matgreen500\" icon=\"save\"\r\n          [loading]=\"loading\" (onButtonClick)=\"onSave()\"></app-button-icon-title>\r\n      </div>\r\n\r\n      <div fxLayout=\"column\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt-12 eh-w-100\">\r\n            Start Date\r\n          </div>\r\n\r\n          <mat-form-field appearance=\"standard\">\r\n            <input matInput [matDatepicker]=\"picker1\" formControlName=\"startDate\" readonly>\r\n            <mat-datepicker-toggle class=\"e-mat-datepicker\" matSuffix [for]=\"picker1\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker1></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt-12 eh-w-100 eh-mr-4\">\r\n            Start Weight\r\n          </div>\r\n          <div fxLayout=\"row\">\r\n            <input class=\"eh-txt-12 eh-w-180 e-brd e-brd--bottom \" type=\"number\" formControlName=\"startWeight\"\r\n              [ngClass]=\"{'e-brd--egrey400': startWeightFC.valid, 'e-brd--error': !startWeightFC.valid}\">\r\n            <div class=\"eh-txt-14\">kg</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div fxLayout=\"column\">\r\n        <div fxFlex=\"0 0 50%\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <div class=\"eh-txt-12 eh-w-100\">\r\n            Target Date\r\n          </div>\r\n\r\n          <mat-form-field appearance=\"standard\">\r\n            <input matInput [matDatepicker]=\"picker2\" formControlName=\"targetDate\" readonly>\r\n            <mat-datepicker-toggle class=\"e-mat-datepicker\" matSuffix [for]=\"picker2\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker2></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\" class=\"eh-pb-4\">\r\n          <div class=\"eh-txt-12 eh-w-100\">\r\n            Target Weight\r\n          </div>\r\n\r\n          <input class=\"eh-w-180 eh-txt-12 e-brd e-brd--bottom eh-p-4\" type=\"number\" formControlName=\"targetWeight\"\r\n            [ngClass]=\"{'e-brd--egrey400': targetWeightFC.valid, 'e-brd--error': !targetWeightFC.valid}\">\r\n\r\n          <div class=\"eh-txt-14 eh-ml-4\">kg</div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <br>\r\n  </ng-container>\r\n\r\n  <div class=\"e-title eh-mt-16\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n    <span>Weight Logs</span>\r\n  </div>\r\n\r\n  <div class=\"eh-mt-16\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <app-button-icon-title class=\"eh-w-84\" title=\"Add Log\" buttonStyle=\"e-txt e-txt--warn\" icon=\"playlist_add\"\r\n      iconStyle=\"e-mat-icon--2\" (onButtonClick)=\"picker3.open()\"></app-button-icon-title>\r\n\r\n    <mat-form-field appearance=\"none\">\r\n      <input matInput [matDatepicker]=\"picker3\" [max]=\"maxDate\" [hidden]=\"true\" [matDatepickerFilter]=\"dateFilter\"\r\n        (dateInput)=\"onDailyDateSelection($event)\">\r\n      <mat-datepicker #picker3></mat-datepicker>\r\n    </mat-form-field>\r\n  </div>\r\n\r\n  <div class=\"eh-txt-12 eh-txt-italic\">\r\n    Please enter your weight in the table (in the morning, after toilet, before breakfast - without clothes)\r\n  </div>\r\n\r\n  <div class=\"e-brd e-brd--bottom eh-p-8 eh-mt-12\" fxLayout=\"row\">\r\n    <div class=\"eh-txt-12 eh-txt-center\" fxFlex=\"0 0 32%\">Date</div>\r\n    <div class=\"eh-txt-12 eh-txt-center\" fxFlex=\"0 0 33%\">Weight(kg)</div>\r\n    <div class=\"eh-txt-12 eh-txt-center\" fxFlex=\"0 0 35%\">Total Loss/Gain(kg)</div>\r\n  </div>\r\n\r\n  <ng-container *ngFor=\"let logFG of logsFA.controls; let i = index;\">\r\n    <app-progress-weight-log [logFG]=\"logFG\" [startWeight]=\"startWeight\" (deleteEvent)=\"removeLog($event, i)\">\r\n    </app-progress-weight-log>\r\n  </ng-container>\r\n\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v eh-txt-center eh-h-100v\" fxLayout=\"row\"\r\n    fxLayoutAlign=\"center center\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>\r\n<!-- </ion-content>\r\n</ion-app> -->";

/***/ }),

/***/ 78115:
/*!******************************************************************!*\
  !*** ./src/app/main/progress/progress.component.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

module.exports = "<ion-app>\r\n  <app-header title=\"My Progress\"></app-header>\r\n  <ion-content>\r\n    <ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n      <ng-container *ngIf=\"isPackageSubscribed; else isPackageNotSubscribedBlk\">\r\n        <div class=\"e-card2 e-card2--shadow eh-maxw-1200 eh-h-100p eh-mx-auto\">\r\n          <div class=\"eh-ofy-auto eh-hide-scrollbars\">\r\n            <!-- <ion-grid>\r\n          <ion-row>\r\n            <ion-segment [(ngModel)]=\"segment\" value=\"food\" scrollable=\"true\" class=\"eh-p-0\">\r\n              <ion-col col-2>\r\n                <ion-segment-button value=\"food\" class=\"segment-button segment-activated\">\r\n                  <ion-label class=\"eh-txt-16 eh-txt-capital\">food</ion-label>\r\n                </ion-segment-button>\r\n              </ion-col>\r\n              <ion-col col-2>\r\n                <ion-segment-button value=\"weight\">\r\n                  <ion-label class=\"eh-txt-16 eh-txt-capital\">Weight\r\n                  </ion-label>\r\n                </ion-segment-button>\r\n              </ion-col>\r\n              <ion-col col-2>\r\n                <ion-segment-button value=\"measurement\">\r\n                  <ion-label class=\"eh-txt-16 eh-txt-capital\">Measurement\r\n                  </ion-label>\r\n                </ion-segment-button>\r\n              </ion-col>\r\n              <ion-col col-2>\r\n                <ion-segment-button value=\"feedback\">\r\n                  <ion-label class=\"eh-txt-16 eh-txt-capital\">Feedback\r\n                  </ion-label>\r\n                </ion-segment-button>\r\n              </ion-col>\r\n              <ion-col col-2>\r\n                <ion-segment-button value=\"photo\">\r\n                  <ion-label class=\"eh-txt-16 eh-txt-capital\">Photo\r\n                  </ion-label>\r\n                </ion-segment-button>\r\n              </ion-col>\r\n            </ion-segment>\r\n          </ion-row>\r\n        </ion-grid> -->\r\n            <!-- </div>\r\n        </div> -->\r\n\r\n\r\n            <mat-tab-group class=\"e-mat-tab-group e-mat-tab-group--1 e-mat-tab-group--bottom-border-none eh-h-100p\"\r\n              animationDuration=\"0ms\" [selectedIndex]=\"1\" [disableRipple]=\"true\">\r\n              <mat-tab label=\"Daily Food\">\r\n                <ng-template matTabContent>\r\n                  <div class=\"eh-p-16\">\r\n                    <app-progress-food></app-progress-food>\r\n                  </div>\r\n                </ng-template>\r\n              </mat-tab>\r\n              <mat-tab label=\"Daily Weight Log\">\r\n                <ng-template matTabContent>\r\n                  <div class=\"eh-p-16\">\r\n                    <app-progress-weight></app-progress-weight>\r\n                  </div>\r\n                </ng-template>\r\n              </mat-tab>\r\n              <mat-tab label=\"Weekly Measurement Log\">\r\n                <ng-template matTabContent>\r\n                  <div class=\"eh-p-16\">\r\n                    <app-progress-measurement></app-progress-measurement>\r\n                  </div>\r\n                </ng-template>\r\n              </mat-tab>\r\n              <mat-tab label=\"General Feedback\">\r\n                <ng-template matTabContent>\r\n                  <div class=\"eh-p-16\">\r\n                    <app-progress-feedback></app-progress-feedback>\r\n                  </div>\r\n                </ng-template>\r\n              </mat-tab>\r\n              <mat-tab label=\"Photo Log\">\r\n                <ng-template matTabContent>\r\n                  <div class=\"eh-p-16\">\r\n                    <app-progress-photo></app-progress-photo>\r\n                  </div>\r\n                </ng-template>\r\n              </mat-tab>\r\n            </mat-tab-group>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n      <!-- <mat-tab label=\"Daily Food & Activity Diary\">\r\n                <div class=\"eh-p-16\">\r\n                  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                    <app-button-icon-title [title]=\"'Add'\" [buttonStyle]=\"'e-txt e-txt--eblue700'\" [icon]=\"'note_add'\"\r\n                      [iconStyle]=\"'e-mat-icon--2'\" (onButtonClick)=\"picker.open()\"></app-button-icon-title>\r\n\r\n                    <mat-form-field class=\"e-mat-form-field\" appearance=\"none\">\r\n                      <input matInput [matDatepicker]=\"picker\" [max]=\"maxDate\" [hidden]=\"true\"\r\n                        (dateInput)=\"onDailyDateSelection($event)\">\r\n                      <mat-datepicker #picker></mat-datepicker>\r\n                    </mat-form-field>\r\n                  </div>\r\n\r\n                  <mat-accordion class=\"e-mat-accordion\" multi>\r\n                    <div *ngFor=\"let log of foodLogs; first as isFirst\" [ngClass]=\"{'eh-mt-8': !isFirst}\">\r\n                      <app-food-log [foodLog]=\"log\"></app-food-log>\r\n                    </div>\r\n                  </mat-accordion>\r\n                </div>\r\n              </mat-tab>\r\n\r\n              <mat-tab label=\"Daily Weight Log\">\r\n                <div class=\"eh-p-16\">\r\n                  <app-weight-log></app-weight-log>\r\n                </div>\r\n              </mat-tab>\r\n\r\n              <mat-tab label=\"Weekly Measurement Log\">\r\n                <div class=\"eh-p-16\">\r\n                  <app-measumement-log></app-measumement-log>\r\n                </div>\r\n              </mat-tab>\r\n\r\n              <mat-tab label=\"General Feedback\">\r\n                <div class=\"eh-p-16\">\r\n                  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                    <app-button-icon-title [title]=\"'Add'\" [buttonStyle]=\"'e-txt e-txt--eblue700'\" [icon]=\"'note_add'\"\r\n                      [iconStyle]=\"'e-mat-icon--2'\" (onButtonClick)=\"picker2.open()\"></app-button-icon-title>\r\n\r\n                    <mat-form-field class=\"e-mat-form-field\" appearance=\"none\">\r\n                      <input matInput [matDatepicker]=\"picker2\" [max]=\"maxDate\" [hidden]=\"true\"\r\n                        (dateInput)=\"onRecallDateSelection($event)\">\r\n                      <mat-datepicker #picker2></mat-datepicker>\r\n                    </mat-form-field>\r\n                  </div>\r\n\r\n                  <mat-accordion class=\"e-mat-accordion\" multi>\r\n                    <div *ngFor=\"let log of recallLogs; first as isFirst\" [ngClass]=\"{'eh-mt-8': !isFirst}\">\r\n                      <app-recall-log [recallLog]=\"log\"></app-recall-log>\r\n                    </div>\r\n                  </mat-accordion>\r\n                </div>\r\n              </mat-tab>\r\n\r\n              <mat-tab label=\"Photo Log\">\r\n                <div class=\"eh-p-16\">\r\n                  <div class=\"eh-xy-center\">\r\n                    <img class=\"eh-w-400\" [src]=\"imgURL\" alt=\"Photo Log Guide\">\r\n                  </div>\r\n\r\n                  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                    <app-button-icon-title [title]=\"'Add'\" [buttonStyle]=\"'e-txt e-txt--eblue700'\" [icon]=\"'note_add'\"\r\n                      [iconStyle]=\"'e-mat-icon--2'\" (onButtonClick)=\"picker3.open()\"></app-button-icon-title>\r\n\r\n                    <mat-form-field class=\"e-mat-form-field\" appearance=\"none\">\r\n                      <input matInput [matDatepicker]=\"picker3\" [max]=\"maxDate\" [hidden]=\"true\"\r\n                        (dateInput)=\"onPhotoDateSelection($event)\">\r\n                      <mat-datepicker #picker3></mat-datepicker>\r\n                    </mat-form-field>\r\n                  </div>\r\n\r\n                  <mat-accordion class=\"e-mat-accordion\" multi>\r\n                    <div *ngFor=\"let log of photoLogs; first as isFirst\" [ngClass]=\"{'eh-mt-8': !isFirst}\">\r\n                      <app-photo-log [photoLog]=\"log\"></app-photo-log>\r\n                    </div>\r\n                  </mat-accordion>\r\n                </div>\r\n              </mat-tab> -->\r\n      <!-- </mat-tab-group> -->\r\n\r\n\r\n      <!-- <ng-container [ngSwitch]=\"segment\">\r\n          <div *ngSwitchCase=\"'food'\">\r\n            <div class=\"eh-p-16\">\r\n              <app-progress-food></app-progress-food>\r\n            </div>\r\n          </div>\r\n          <div *ngSwitchCase=\"'weight'\">\r\n            <div class=\"eh-p-16\">\r\n              <app-progress-weight></app-progress-weight>\r\n            </div>\r\n          </div>\r\n          <div *ngSwitchCase=\"'measurement'\">\r\n            <div class=\"eh-p-16\">\r\n              <app-progress-measurement></app-progress-measurement>\r\n            </div>\r\n          </div>\r\n          <div *ngSwitchCase=\"'feedback'\">\r\n            <div class=\"eh-p-16\">\r\n              <app-progress-feedback></app-progress-feedback>\r\n            </div>\r\n          </div>\r\n          <div *ngSwitchCase=\"'photo'\">\r\n            <div class=\"eh-p-16\">\r\n              <app-progress-photo></app-progress-photo>\r\n            </div>\r\n          </div>\r\n        </ng-container>\r\n      </ng-container> -->\r\n\r\n      <ng-template #isPackageNotSubscribedBlk>\r\n        <div class=\"eh-h-100p\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n          <span class=\"e-txt e-txt--placeholder\">This is a PRO-member area.</span>\r\n          <span class=\"e-txt e-txt--placeholder\">Please subscribe to a package to access this section.</span>\r\n          <button *ngIf=\"!me.flags.isOrientationScheduled\" mat-stroked-button\r\n            class=\"e-mat-button e-mat-button--eblue400 eh-mt-8\" (click)=\"showDialog()\">\r\n            Resume On-boarding\r\n          </button>\r\n        </div>\r\n      </ng-template>\r\n    </ng-container>\r\n\r\n    <ng-template #dataAwaitedBlk>\r\n      <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n        <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n      </div>\r\n    </ng-template>\r\n\r\n  </ion-content>\r\n  <!-- <app-tabs></app-tabs> -->\r\n</ion-app>";

/***/ })

}]);
//# sourceMappingURL=src_app_main_progress_progress_module_ts.js.map