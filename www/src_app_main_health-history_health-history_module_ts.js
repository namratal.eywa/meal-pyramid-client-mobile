"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_main_health-history_health-history_module_ts"],{

/***/ 20819:
/*!****************************************************************************************!*\
  !*** ./src/app/main/health-history/advanced-test-file/advanced-test-file.component.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdvancedTestFileComponent": () => (/* binding */ AdvancedTestFileComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _advanced_test_file_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./advanced-test-file.component.html?ngResource */ 52111);
/* harmony import */ var _advanced_test_file_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./advanced-test-file.component.scss?ngResource */ 13916);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services */ 98138);








let AdvancedTestFileComponent = class AdvancedTestFileComponent {
    constructor(_as, _us, _sb, _sts) {
        this._as = _as;
        this._us = _us;
        this._sb = _sb;
        this._sts = _sts;
        this.acceptedFileTypes = '.jpg, .jpeg, .png, .pdf';
        this.advancedTestFiles = [];
        this.areAdvancedTestFilesAvailable = false;
        this.dispatchView = false;
        this.downloadFileName = '';
        this.fileURL = '';
        this.isFileImg = false;
        this.placeholder = 'Select a file to load details';
        this.showFile = false;
        this.title = '';
        this.downloading = false;
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.switchMap)((me) => {
            this.me = me;
            return this._us.getUserAdvancedTestFiles(this.me.uid, 
            // this.me.membershipKey
            this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.catchError)(() => {
            this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_8__.EMPTY;
        }))
            .subscribe((files) => {
            this.advancedTestFiles = files;
            this.areAdvancedTestFilesAvailable = Boolean(this.advancedTestFiles.length > 0);
            this.dispatchView = true;
        });
    }
    onDownloadFile(file) {
        this.downloading = true;
        console.log(file);
        this._sts.download(file.fileURL, file.fileName);
        setTimeout(() => {
            this.downloading = false;
            this._sb.openSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.SuccessTypes.FILE_DOWNLAOD);
        }, 2000);
    }
};
AdvancedTestFileComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.AuthService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.StorageService }
];
AdvancedTestFileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-advanced-test-file',
        template: _advanced_test_file_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_advanced_test_file_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AdvancedTestFileComponent);



/***/ }),

/***/ 48399:
/*!****************************************************************************************************!*\
  !*** ./src/app/main/health-history/clinical-assessment-file/clinical-assessment-file.component.ts ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ClinicalAssessmentFileComponent": () => (/* binding */ ClinicalAssessmentFileComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _clinical_assessment_file_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./clinical-assessment-file.component.html?ngResource */ 96414);
/* harmony import */ var _clinical_assessment_file_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./clinical-assessment-file.component.scss?ngResource */ 70435);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services */ 98138);








let ClinicalAssessmentFileComponent = class ClinicalAssessmentFileComponent {
    constructor(_as, _us, _sb, _sts, _sds) {
        this._as = _as;
        this._us = _us;
        this._sb = _sb;
        this._sts = _sts;
        this._sds = _sds;
        this.acceptedFileTypes = '.jpg, .jpeg, .png, .pdf';
        this.areClinicalAssessmentFilesAvailable = false;
        this.assessments = [];
        this.clinicalAssessmentFiles = [];
        this.diabetesAssessments = [];
        this.dispatchView = false;
        this.downloadFileName = '';
        this.fileKey = '';
        this.fileName = '';
        this.filePath = '';
        this.fileType = '';
        this.fileURL = '';
        this.generalAssessments = [];
        this.hasAssessments = false;
        this.hormonesAssessments = [];
        this.isFileImg = false;
        this.kidneyAssessments = [];
        this.placeholder = 'Select a file to load details';
        this.otherAssessments = [];
        this.otherBloodAssessments = [];
        // showFile = false;
        this.sportsAssessments = [];
        this.title = '';
        this.uploadCollectionName = 'userAssessmentFiles';
        this.uploadDir = 'assessments';
        this.uploadDirPath = 'assessments';
        this.uploadFileNamePartial = 'MEALpyramid_CA';
        this.uploadFileNamePrefix = '';
        this.uploading = false;
        this.vitaminsAssessments = [];
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
        this.downloading = false;
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.switchMap)((me) => {
            this.me = me;
            this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;
            return this._us.getActiveUserAssessments(this.me.uid, this.me.membershipKey);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.switchMap)((arr) => {
            if (arr.length) {
                if (arr.length !== 1) {
                    throw new Error();
                }
                this.hasAssessments = Boolean(arr[0].tests && arr[0].tests.length);
                this.assessments = arr[0].tests;
                this.generalAssessments = this.assessments.filter((item) => item.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AssessmentCategories.GENERAL);
                // this.kidneyAssessments = this.assessments.filter(
                //   (item) => item.category === AssessmentCategories.KIDNEY_PROFILE
                // );
                this.diabetesAssessments = this.assessments.filter((item) => item.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AssessmentCategories.DIABETIC_PROFILE);
                this.vitaminsAssessments = this.assessments.filter((item) => item.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AssessmentCategories.VITAMIN_PROFILE);
                this.hormonesAssessments = this.assessments.filter((item) => item.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AssessmentCategories.HORMONES);
                this.sportsAssessments = this.assessments.filter((item) => item.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AssessmentCategories.SPORTS_SPECIFIC_TESTS);
                // this.otherBloodAssessments = this.assessments.filter(
                //   (item) => item.category === AssessmentCategories.OTHER_BLOOD_TESTS
                // );
                this.otherAssessments = this.assessments.filter((item) => item.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AssessmentCategories.OTHER_TESTS);
            }
            else {
                this.hasAssessments = false;
            }
            return this._us.getUserAssessmentFiles(this.me.uid, 
            // this.me.membershipKey
            this.me.allMembershipKeys);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.catchError)(() => {
            this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_8__.EMPTY;
        }))
            .subscribe((files) => {
            this.clinicalAssessmentFiles = files.sort((a, b) => b.uploadedAt.valueOf() - a.uploadedAt.valueOf());
            this.areClinicalAssessmentFilesAvailable = Boolean(this.clinicalAssessmentFiles.length > 0);
            this.dispatchView = true;
        });
    }
    onDownloadFile(file) {
        this.downloading = true;
        console.log(file.fileURL);
        console.log(file.fileName);
        this._sts.download(file.fileURL, file.fileName);
        // this._demo.downloadFile(file.fileURL, file.fileName);
        setTimeout(() => {
            this.downloading = false;
            this._sb.openSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.SuccessTypes.FILE_DOWNLAOD);
        }, 2000);
    }
    onShowClick(file) {
        // this.showFile = false;
        this.title = file.fileName;
        this.fileKey = file.key;
        this.fileType = file.fileType;
        this.downloadFileName = file.fileName;
        this.filePath = file.filePath;
        this.fileURL = file.fileURL;
        this.isFileImg = file.fileType.includes('image');
        // setTimeout(() => {
        //   this.showFile = true;
        // }, 250);
    }
    // onUploadFile(e: any): Promise<void> {
    //   if (!e || !e.target || !e.target.files) {
    //     return;
    //   }
    //   const fileList = e.target.files;
    //   if (fileList.length < 1) {
    //     return;
    //   }
    //   this.uploading = true;
    //   this.areClinicalAssessmentFilesAvailable = false;
    //   this.fileName = `MEALpyramid_CA_${this._uts.sanitize(
    //     this.me.name
    //   )}_${Math.floor(Date.now() / 100)}`;
    //   this.file = fileList.item(0);
    // }
    // onUploadComplete(): void {
    //   this.uploading = false;
    // }
    onUploadComplete() {
        this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.SuccessMessages.FILE_UPLOADED);
    }
};
ClinicalAssessmentFileComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.AuthService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.StaticDataService }
];
ClinicalAssessmentFileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-clinical-assessment-file',
        template: _clinical_assessment_file_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_clinical_assessment_file_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ClinicalAssessmentFileComponent);



/***/ }),

/***/ 83010:
/*!**********************************************************************!*\
  !*** ./src/app/main/health-history/health-history-routing.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HealthHistoryRoutingModule": () => (/* binding */ HealthHistoryRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _health_history_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./health-history.component */ 41676);




const routes = [
    {
        path: '',
        component: _health_history_component__WEBPACK_IMPORTED_MODULE_0__.HealthHistoryComponent
    }
];
let HealthHistoryRoutingModule = class HealthHistoryRoutingModule {
};
HealthHistoryRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], HealthHistoryRoutingModule);



/***/ }),

/***/ 41676:
/*!*****************************************************************!*\
  !*** ./src/app/main/health-history/health-history.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HealthHistoryComponent": () => (/* binding */ HealthHistoryComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _health_history_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./health-history.component.html?ngResource */ 38871);
/* harmony import */ var _health_history_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./health-history.component.scss?ngResource */ 72);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);









let HealthHistoryComponent = class HealthHistoryComponent {
    constructor(_as, _dialog, _ss) {
        this._as = _as;
        this._dialog = _dialog;
        this._ss = _ss;
        this.dispatchView = false;
        this.isPackageSubscribed = false;
        this.tabSelected = 0;
        this.segment = 'address';
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.switchMap)((me) => {
            this.me = me;
            this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
            return this._ss.getHealthHistoryTabSelected();
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.takeUntil)(this._notifier$))
            .subscribe((res) => {
            this.tabSelected = res;
            this.dispatchView = true;
        });
    }
    showDialog() {
        this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_3__.GetStartedComponent, {
            width: '60vw',
            height: '90vh',
            data: { step: this.me.onboardingStep || 0 }
        });
    }
};
HealthHistoryComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_2__.AuthService },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_7__.MatDialog },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_2__.SharedService }
];
HealthHistoryComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-health-history',
        template: _health_history_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_health_history_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], HealthHistoryComponent);



/***/ }),

/***/ 98213:
/*!**************************************************************!*\
  !*** ./src/app/main/health-history/health-history.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HealthHistoryModule": () => (/* binding */ HealthHistoryModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _health_history_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./health-history.component */ 41676);
/* harmony import */ var _health_history_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./health-history-routing.module */ 83010);
/* harmony import */ var _advanced_test_file_advanced_test_file_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./advanced-test-file/advanced-test-file.component */ 20819);
/* harmony import */ var _clinical_assessment_file_clinical_assessment_file_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./clinical-assessment-file/clinical-assessment-file.component */ 48399);
/* harmony import */ var _health_questionnaire_health_questionnaire_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./health-questionnaire/health-questionnaire.component */ 40624);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var src_app_shared_button_file_upload__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/button-file-upload */ 28458);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);
/* harmony import */ var src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/material-design */ 12497);
/* harmony import */ var src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/shared-components */ 35886);
/* harmony import */ var src_app_shared_upload_file__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/shared/upload-file */ 74355);
/* harmony import */ var src_app_shared_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/shared/layout */ 47633);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ 2508);

















let HealthHistoryModule = class HealthHistoryModule {
};
HealthHistoryModule = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.NgModule)({
        declarations: [
            _health_history_component__WEBPACK_IMPORTED_MODULE_0__.HealthHistoryComponent,
            _advanced_test_file_advanced_test_file_component__WEBPACK_IMPORTED_MODULE_2__.AdvancedTestFileComponent,
            _clinical_assessment_file_clinical_assessment_file_component__WEBPACK_IMPORTED_MODULE_3__.ClinicalAssessmentFileComponent,
            _health_questionnaire_health_questionnaire_component__WEBPACK_IMPORTED_MODULE_4__.HealthQuestionnaireComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_13__.CommonModule,
            src_app_shared_layout__WEBPACK_IMPORTED_MODULE_10__.LayoutModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicModule,
            src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_6__.GetStartedModule,
            src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_7__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_15__.FlexLayoutModule,
            _health_history_routing_module__WEBPACK_IMPORTED_MODULE_1__.HealthHistoryRoutingModule,
            src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_8__.SharedComponentsModule,
            src_app_shared_upload_file__WEBPACK_IMPORTED_MODULE_9__.UploadFileModule,
            src_app_shared_button_file_upload__WEBPACK_IMPORTED_MODULE_5__.ButtonFileUploadModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__.FormsModule
        ]
    })
], HealthHistoryModule);



/***/ }),

/***/ 40624:
/*!********************************************************************************************!*\
  !*** ./src/app/main/health-history/health-questionnaire/health-questionnaire.component.ts ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HealthQuestionnaireComponent": () => (/* binding */ HealthQuestionnaireComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _health_questionnaire_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./health-questionnaire.component.html?ngResource */ 11049);
/* harmony import */ var _health_questionnaire_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./health-questionnaire.component.scss?ngResource */ 19863);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);









let HealthQuestionnaireComponent = class HealthQuestionnaireComponent {
    constructor(_as, _us, _sb) {
        this._as = _as;
        this._us = _us;
        this._sb = _sb;
        this.dispatchView = false;
        this.userCategories = src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.UserCategories;
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__.Subject();
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.switchMap)((user) => {
            this.me = user;
            return this._us.getUserHealthHistory(this.me.uid, this.me.membershipKey);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.catchError)(() => {
            this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_8__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$))
            .subscribe((res) => {
            if (res.length === 0) {
                this.healthHistory = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.UserHealthHistory();
            }
            else if (res.length === 1) {
                [this.healthHistory] = res;
            }
            else {
                this.healthHistory = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.UserHealthHistory();
            }
            this.dispatchView = true;
        });
    }
};
HealthQuestionnaireComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService }
];
HealthQuestionnaireComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-health-questionnaire',
        template: _health_questionnaire_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_health_questionnaire_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], HealthQuestionnaireComponent);



/***/ }),

/***/ 13916:
/*!*****************************************************************************************************!*\
  !*** ./src/app/main/health-history/advanced-test-file/advanced-test-file.component.scss?ngResource ***!
  \*****************************************************************************************************/
/***/ ((module) => {

module.exports = "ion-icon {\n  zoom: 0.75;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkdmFuY2VkLXRlc3QtZmlsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQUE7QUFDRiIsImZpbGUiOiJhZHZhbmNlZC10ZXN0LWZpbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taWNvbiB7XHJcbiAgem9vbTogMC43NVxyXG59Il19 */";

/***/ }),

/***/ 70435:
/*!*****************************************************************************************************************!*\
  !*** ./src/app/main/health-history/clinical-assessment-file/clinical-assessment-file.component.scss?ngResource ***!
  \*****************************************************************************************************************/
/***/ ((module) => {

module.exports = "ion-icon {\n  zoom: 0.75;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNsaW5pY2FsLWFzc2Vzc21lbnQtZmlsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQUE7QUFDRiIsImZpbGUiOiJjbGluaWNhbC1hc3Nlc3NtZW50LWZpbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taWNvbiB7XHJcbiAgem9vbTogMC43NVxyXG59Il19 */";

/***/ }),

/***/ 72:
/*!******************************************************************************!*\
  !*** ./src/app/main/health-history/health-history.component.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "ion-segment-button {\n  --color-checked: #eb445a;\n}\n\n.segment-button-indicator {\n  display: none;\n}\n\nion-segment-button::part(indicator-background) {\n  background: #eb445a;\n}\n\n/* Material Design styles */\n\nion-segment-button.md::part(native) {\n  color: #000;\n}\n\n.segment-button-checked.md::part(native) {\n  color: #000;\n}\n\n/* iOS styles */\n\nion-segment-button.ios::part(native) {\n  color: #000;\n}\n\n.segment-button-checked.ios::part(native) {\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlYWx0aC1oaXN0b3J5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usd0JBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7QUFDRjs7QUFFQTtFQUNFLG1CQUFBO0FBQ0Y7O0FBRUEsMkJBQUE7O0FBQ0E7RUFDRSxXQUFBO0FBQ0Y7O0FBRUE7RUFDRSxXQUFBO0FBQ0Y7O0FBRUEsZUFBQTs7QUFDQTtFQUNFLFdBQUE7QUFDRjs7QUFFQTtFQUNFLFdBQUE7QUFDRiIsImZpbGUiOiJoZWFsdGgtaGlzdG9yeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1zZWdtZW50LWJ1dHRvbiB7XHJcbiAgLS1jb2xvci1jaGVja2VkOiAjZWI0NDVhO1xyXG59XHJcblxyXG4uc2VnbWVudC1idXR0b24taW5kaWNhdG9yIHtcclxuICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG5pb24tc2VnbWVudC1idXR0b246OnBhcnQoaW5kaWNhdG9yLWJhY2tncm91bmQpIHtcclxuICBiYWNrZ3JvdW5kOiAjZWI0NDVhO1xyXG59XHJcblxyXG4vKiBNYXRlcmlhbCBEZXNpZ24gc3R5bGVzICovXHJcbmlvbi1zZWdtZW50LWJ1dHRvbi5tZDo6cGFydChuYXRpdmUpIHtcclxuICBjb2xvcjogIzAwMDtcclxufVxyXG5cclxuLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQubWQ6OnBhcnQobmF0aXZlKSB7XHJcbiAgY29sb3I6ICMwMDA7XHJcbn1cclxuXHJcbi8qIGlPUyBzdHlsZXMgKi9cclxuaW9uLXNlZ21lbnQtYnV0dG9uLmlvczo6cGFydChuYXRpdmUpIHtcclxuICBjb2xvcjogIzAwMDtcclxufVxyXG5cclxuLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQuaW9zOjpwYXJ0KG5hdGl2ZSkge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG59XHJcbiJdfQ== */";

/***/ }),

/***/ 19863:
/*!*********************************************************************************************************!*\
  !*** ./src/app/main/health-history/health-questionnaire/health-questionnaire.component.scss?ngResource ***!
  \*********************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFsdGgtcXVlc3Rpb25uYWlyZS5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 52111:
/*!*****************************************************************************************************!*\
  !*** ./src/app/main/health-history/advanced-test-file/advanced-test-file.component.html?ngResource ***!
  \*****************************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-h-100p\" fxLayout=\"column\">\r\n    <div>\r\n      <ng-container *ngIf=\"areAdvancedTestFilesAvailable; else noFilesBlk\">\r\n        <div class=\"eh-px-2 eh-pb-16 eh-ofy-scroll eh-hide-scrollbars\">\r\n          <div class=\"eh-txt-12 eh-mt-10 e-txt eh-txt-center e-txt--matgrey500\">\r\n            All Advanced Testing Reports\r\n          </div>\r\n          <!-- <div class=\"e-title\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">calendar_view_month</mat-icon>\r\n            <span>All Advanced Testing Reports</span>\r\n          </div> -->\r\n          <div *ngFor=\"let file of advancedTestFiles\" class=\"eh-mt-16\">\r\n            <div class=\"e-card2 e-card2--round1 e-card2--hover2\">\r\n              <div class=\"eh-p-12\">\r\n                <div fxLayout=\"row nowrap\">\r\n                  <mat-icon class=\"e-mat-icon e-mat-icon--3\">file_present</mat-icon>\r\n                  <span class=\"eh-ml-8 eh-txt-12 eh-txt-breakword\">{{file.fileName}}</span>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n                  <div class=\"eh-txt-10 eh-mt-8 e-txt e-txt--matgrey500 eh-txt-italic\">\r\n                    Uploaded: {{file.uploadedAt | date:'E, d-MMM-y h:mm a':'+0530'}}\r\n                  </div>\r\n                  <div (click)=\"onDownloadFile(file)\">\r\n                    <ion-icon name=\"download-outline\" size=\"large\"></ion-icon>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n\r\n      <ng-template #noFilesBlk>\r\n        <div class=\"e-txt e-txt--placeholder eh-txt-center eh-p-16\">\r\n          No Advanced Testing Reports available\r\n        </div>\r\n      </ng-template>\r\n    </div>\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 96414:
/*!*****************************************************************************************************************!*\
  !*** ./src/app/main/health-history/clinical-assessment-file/clinical-assessment-file.component.html?ngResource ***!
  \*****************************************************************************************************************/
/***/ ((module) => {

module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-h-100p eh-mt-10\">\r\n    <div fxLayout=\"column\">\r\n      <div *ngIf=\"hasAssessments;\" class=\"e-card2 e-card2--round1 e-card2--hover2 eh-mt-6\" fxLayout=\"column\">\r\n        <!-- <div>\r\n          <mat-icon role=\"img\" class=\"mat-icon\" aria-hidden=\"true\" data-mat-icon-type=\"font\">\r\n            feedback</mat-icon>\r\n          <span class=\"e-title eh-mb-4\">\r\n            Required Clinical Assessments\r\n          </span>\r\n        </div> -->\r\n\r\n        <div class=\"e-title\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">feedback</mat-icon>\r\n          <span> Required Clinical Assessments</span>\r\n        </div>\r\n\r\n        <div *ngIf=\"generalAssessments.length > 0\" class=\"e-brd e-brd--bottom eh-mt-8 eh-px-4 eh-mx-4\" fxLayout=\"row\">\r\n          <span class=\"eh-txt-12\">General:&nbsp;</span>\r\n          <div fxLayout=\"row wrap\">\r\n            <div class=\"eh-ml-8 eh-txt-12\" *ngFor=\"let item of generalAssessments; last as isLast\" fxLayout=\"row wrap\">\r\n              <span *ngIf=\"!isLast\">{{item.name}},</span>\r\n              <span *ngIf=\"isLast\">{{item.name}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n\r\n        <div *ngIf=\"diabetesAssessments.length > 0\" class=\"e-brd e-brd--bottom eh-mt-4 eh-px-4 eh-mx-4\" fxLayout=\"row\">\r\n          <span class=\"eh-txt-12\">Diabetes:&nbsp;</span>\r\n          <div fxLayout=\"row wrap\">\r\n            <div class=\"eh-ml-8 eh-txt-12\" *ngFor=\"let item of diabetesAssessments; last as isLast\">\r\n              <span *ngIf=\"!isLast\">{{item.name}},</span>\r\n              <span *ngIf=\"isLast\">{{item.name}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"vitaminsAssessments.length > 0\" class=\"e-brd e-brd--bottom eh-mt-4 eh-px-4 eh-mx-4\" fxLayout=\"row\">\r\n          <span class=\"eh-txt-12\">Vitamins:&nbsp;</span>\r\n          <div fxLayout=\"row wrap\">\r\n            <div class=\"eh-ml-8 eh-txt-12\" *ngFor=\"let item of vitaminsAssessments; last as isLast\">\r\n              <span *ngIf=\"!isLast\">{{item.name}},</span>\r\n              <span *ngIf=\"isLast\">{{item.name}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"hormonesAssessments.length > 0\" class=\"e-brd e-brd--bottom eh-mt-4 eh-px-4 eh-mx-4\" fxLayout=\"row\">\r\n          <span class=\"eh-txt-12\">Hormones:&nbsp;</span>\r\n          <div fxLayout=\"row wrap\">\r\n            <div class=\"eh-ml-8 eh-txt-12\" *ngFor=\"let item of hormonesAssessments; last as isLast\">\r\n              <span *ngIf=\"!isLast\">{{item.name}},</span>\r\n              <span *ngIf=\"isLast\">{{item.name}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"sportsAssessments.length > 0\" class=\"e-brd e-brd--bottom eh-mt-4 eh-px-4 eh-mx-4\" fxLayout=\"row\">\r\n          <span class=\"eh-txt-12\">Sports:&nbsp;</span>\r\n          <div fxLayout=\"row wrap\">\r\n            <div class=\"eh-ml-8 eh-txt-12\" *ngFor=\"let item of sportsAssessments; last as isLast\">\r\n              <span *ngIf=\"!isLast\">{{item.name}},</span>\r\n              <span *ngIf=\"isLast\">{{item.name}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n\r\n        <div *ngIf=\"otherAssessments.length > 0\" class=\"e-brd e-brd--bottom eh-mt-4 eh-px-4 eh-mx-4\" fxLayout=\"row\">\r\n          <span class=\"eh-txt-12\">Other:&nbsp;</span>\r\n          <div fxLayout=\"row wrap\">\r\n            <div class=\"eh-ml-8 eh-txt-12\" *ngFor=\"let item of otherAssessments; last as isLast\">\r\n              <span *ngIf=\"!isLast\">{{item.name}},</span>\r\n              <span *ngIf=\"isLast\">{{item.name}}</span>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n        <!-- <div class=\"eh-h-68\" fxLayout=\"row\" fxLayoutAlign=\"end center\">\r\n\r\n\r\n          <div class=\"eh-w-128 eh-mt-8\">\r\n            <app-button-file-upload [uploadByName]=\"me.name\" [uploadByUID]=\"me.uid\"\r\n              [uploadCollectionName]=\"uploadCollectionName\" [uploadDirPath]=\"uploadDirPath\"\r\n              [uploadFileNamePrefix]=\"uploadFileNamePrefix\" [userMembershipKey]=\"me.membershipKey\" [userUID]=\"me.uid\"\r\n              (uploadEvent)=\"onUploadComplete()\">\r\n            </app-button-file-upload>\r\n          </div>\r\n        </div> -->\r\n      </div>\r\n\r\n      <ng-container *ngIf=\"areClinicalAssessmentFilesAvailable; else noFilesBlk\">\r\n        <div class=\"eh-px-2 eh-pb-16 eh-ofy-scroll eh-hide-scrollbars\">\r\n          <div class=\"eh-mt-16 eh-txt-12 e-txt eh-txt-center e-txt--matgrey500\">\r\n            All Uploaded Reports\r\n          </div>\r\n\r\n          <div *ngFor=\"let file of clinicalAssessmentFiles;\" class=\"eh-mt-16\">\r\n            <div class=\"e-card2 e-card2--round1 e-card2--hover2\">\r\n              <div class=\"eh-p-12\">\r\n                <div fxLayout=\"row nowrap\" color=\"eh-txt-dark\">\r\n                  <mat-icon class=\"e-mat-icon e-mat-icon--3\">file_present</mat-icon>\r\n                  <span class=\"eh-ml-8 eh-txt-12 eh-txt-breakword\">{{file.fileName}}</span>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n                  <div class=\"eh-txt-10 eh-mt-8 e-txt e-txt--matgrey500 eh-txt-italic\">\r\n                    Uploaded: {{file.uploadedAt | date:'E, d-MMM-y h:mm a':'+0530'}}\r\n                  </div>\r\n                  <div (click)=\"onDownloadFile(file)\">\r\n                    <ion-icon name=\"download-outline\" size=\"large\"></ion-icon>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n              <!-- <app-preview-download [allowDelete]=\"true\" [downloadFileName]=\"downloadFileName\" [fileKey]=\"fileKey\"\r\n                [filePath]=\"filePath\" [fileURL]=\"fileURL\" [isImg]=\"isFileImg\" [placeholder]=\"placeholder\"\r\n                [uploadCollectionName]=\"uploadCollectionName\" [userUID]=\"me.uid\" [title]=\"title\">\r\n              </app-preview-download> -->\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <!-- <div class=\"e-brd e-brd--left\">\r\n          <app-preview-download [allowDelete]=\"true\" [downloadFileName]=\"downloadFileName\" [fileKey]=\"fileKey\"\r\n            [filePath]=\"filePath\" [fileURL]=\"fileURL\" [isImg]=\"isFileImg\" [placeholder]=\"placeholder\"\r\n            [uploadCollectionName]=\"uploadCollectionName\" [userUID]=\"me.uid\" [title]=\"title\">\r\n          </app-preview-download>\r\n        </div> -->\r\n      </ng-container>\r\n\r\n      <ng-template #noFilesBlk>\r\n        <div class=\"e-txt e-txt--placeholder eh-txt-center eh-p-16\">\r\n          No Clinical Assessment Reports available\r\n        </div>\r\n      </ng-template>\r\n    </div>\r\n\r\n    <!-- <div class=\"e-brd e-brd--left\">\r\n      <app-preview-download [allowDelete]=\"true\" [downloadFileName]=\"downloadFileName\" [fileKey]=\"fileKey\"\r\n        [filePath]=\"filePath\" [fileURL]=\"fileURL\" [isImg]=\"isFileImg\" [placeholder]=\"placeholder\"\r\n        [uploadCollectionName]=\"uploadCollectionName\" [userUID]=\"me.uid\" [title]=\"title\">\r\n      </app-preview-download>\r\n    </div> -->\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 38871:
/*!******************************************************************************!*\
  !*** ./src/app/main/health-history/health-history.component.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "<ion-app>\r\n  <app-header *ngIf=\"dispatchView; else dataAwaitedBlk\" title=\"Health History\"></app-header>\r\n  <ion-content class=\"eh-px-10\">\r\n    <ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n      <ng-container *ngIf=\"isPackageSubscribed; else isPackageNotSubscribedBlk\">\r\n        <div class=\"eh-hide-scrollbars eh-py-0 eh-px-6\">\r\n\r\n\r\n          <mat-tab-group class=\"e-mat-tab-group e-mat-tab-group--1 e-mat-tab-group--bottom-border-none eh-h-100p\"\r\n            animationDuration=\"0ms\" [selectedIndex]=\"0\" [disableRipple]=\"true\">\r\n            <mat-tab label=\"Health Questionnaire\">\r\n              <ng-template matTabContent>\r\n                <div class=\"eh-mx-10 eh-px-4 eh-ofy-scroll\">\r\n                  <app-health-questionnaire></app-health-questionnaire>\r\n                </div>\r\n              </ng-template>\r\n            </mat-tab>\r\n\r\n            <mat-tab label=\"Clinical Assessment\">\r\n              <ng-template matTabContent>\r\n                <div class=\"eh-mx-10 eh-px-4\">\r\n                  <app-clinical-assessment-file></app-clinical-assessment-file>\r\n                </div>\r\n              </ng-template>\r\n            </mat-tab>\r\n\r\n            <mat-tab label=\"Advanced Testing Reports\">\r\n              <ng-template matTabContent>\r\n                <div class=\"eh-mx-10 eh-px-4\">\r\n                  <app-advanced-test-file></app-advanced-test-file>\r\n                </div>\r\n              </ng-template>\r\n            </mat-tab>\r\n\r\n          </mat-tab-group>\r\n          <!-- <ng-container [ngSwitch]=\"segment\">\r\n            <div *ngSwitchCase=\"'address'\">\r\n              <app-health-questionnaire></app-health-questionnaire>\r\n            </div>\r\n            <div *ngSwitchCase=\"'kyc'\">\r\n              <app-clinical-assessment-file></app-clinical-assessment-file>\r\n            </div>\r\n            <div *ngSwitchCase=\"'digi-kyc'\">\r\n              <app-advanced-test-file></app-advanced-test-file>\r\n            </div>\r\n          </ng-container> -->\r\n        </div>\r\n      </ng-container>\r\n\r\n      <ng-template #isPackageNotSubscribedBlk>\r\n        <div class=\"eh-h-100p\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n          <span class=\"e-txt e-txt--placeholder\">This is a PRO-member area.</span>\r\n          <span class=\"e-txt e-txt--placeholder\">Please subscribe to a package to access this section.</span>\r\n          <button *ngIf=\"!me.flags.isOrientationScheduled\" mat-stroked-button\r\n            class=\"e-mat-button e-mat-button--eblue400 eh-mt-8\" (click)=\"showDialog()\">\r\n            Resume On-boarding\r\n          </button>\r\n        </div>\r\n      </ng-template>\r\n    </ng-container>\r\n  </ion-content>\r\n  <!-- <app-tabs></app-tabs> -->\r\n  <ng-template #dataAwaitedBlk>\r\n    <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n      <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n    </div>\r\n  </ng-template>\r\n\r\n</ion-app>";

/***/ }),

/***/ 11049:
/*!*********************************************************************************************************!*\
  !*** ./src/app/main/health-history/health-questionnaire/health-questionnaire.component.html?ngResource ***!
  \*********************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"eh-mt-16 eh-w-100p eh-ofy-scroll eh-hide-scrollbars\">\r\n  <ng-container *ngIf=\"dispatchView; else dataAwaitedBlock\">\r\n    <div class=\"e-title\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">medical_services</mat-icon>\r\n      <span>Medical History & Lifestyle</span>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Medical Conditions</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">\r\n        <span *ngIf=\"healthHistory.medicalConditions.length === 0\">-</span>\r\n        <div *ngFor=\"let item of healthHistory.medicalConditions; last as isLast\">\r\n          <span *ngIf=\"!isLast\">{{item}},&nbsp;</span>\r\n          <span *ngIf=\"isLast\">{{item}}</span>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Other Medical Conditions</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">\r\n        {{healthHistory.otherMedicalConditions || '-'}}\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Are you taking any medication?</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\" fxFlex=\"0 0 150px\">\r\n        {{healthHistory.onMedication ? 'Yes' : 'No'}}\r\n      </div>\r\n      <ng-container *ngIf=\"healthHistory.onMedication\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_6\" fxFlex=\"0 0 80px\">Details</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\">\r\n          {{healthHistory.medicationDesc || '-'}}\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n\r\n    <div *ngIf=\"me.personalDetails.gender === 'Female'\" class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\">\r\n        Do you consider your menstrual cycle regular?\r\n      </div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\">\r\n        <ng-container *ngIf=\"healthHistory.isMCycleRegular === true\">Yes</ng-container>\r\n        <ng-container *ngIf=\"healthHistory.isMCycleRegular === false\">No</ng-container>\r\n        <ng-container *ngIf=\"healthHistory.isMCycleRegular === null\">-</ng-container>\r\n      </div>\r\n\r\n    </div>\r\n\r\n    <div *ngIf=\"me.personalDetails.gender === 'Female'\" class=\"eh-mt-16\" fxLayout=\"row\">\r\n\r\n      <div class=\"eh-txt-12 eh-ml-4  eh-txt-lh1_6 \" fxFlex=\"0 0 150px\">Details</div>\r\n      <div class=\"e-txt eh-ml-8 e-txt--matamber600 eh-txt-breakword\">\r\n        <ng-container *ngIf=\"healthHistory.isMCycleRegular === false\">\r\n          {{healthHistory.mCycleDesc}}\r\n        </ng-container>\r\n        <ng-container *ngIf=\"healthHistory.isMCycleRegular === true || healthHistory.isMCycleRegular === null\">\r\n          -\r\n        </ng-container>\r\n      </div>\r\n    </div>\r\n\r\n    <mat-divider class=\"e-mat-divider eh-mt-16\"></mat-divider>\r\n\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Do you drink alcohol?</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\" fxFlex=\"0 0 150px\">\r\n        {{healthHistory.doesAlcohol ? 'Yes' : 'No'}}\r\n      </div>\r\n      <ng-container *ngIf=\"healthHistory.doesAlcohol\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_6\" fxFlex=\"0 0 80px\">Details</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\">{{ healthHistory.alcoholDesc }}</div>\r\n      </ng-container>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Do you smoke?</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\" fxFlex=\"0 0 150px\">\r\n        {{healthHistory.doesSmoking ? 'Yes' : 'No'}}\r\n      </div>\r\n      <ng-container *ngIf=\"healthHistory.doesSmoking\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_6\" fxFlex=\"0 0 80px\">Details</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\">{{ healthHistory.smokingDesc }}</div>\r\n      </ng-container>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Do you chew tobacco?</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\" fxFlex=\"0 0 150px\">\r\n        {{healthHistory.doesTobacco ? 'Yes' : 'No'}}\r\n      </div>\r\n      <ng-container *ngIf=\"healthHistory.doesTobacco\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_6\" fxFlex=\"0 0 80px\">Details</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\">{{ healthHistory.tobaccoDesc }}</div>\r\n      </ng-container>\r\n    </div>\r\n\r\n    <div class=\"e-title eh-mt-24\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">directions_run</mat-icon>\r\n      <span>Exercise</span>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Do you exercise regularly?</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\">\r\n        {{healthHistory.doesExercise ? 'Yes' : 'No'}}\r\n      </div>\r\n    </div>\r\n\r\n    <ng-container *ngIf=\"healthHistory.doesExercise\">\r\n      <div class=\"eh-mt-16 eh-ml-4\" fxLayout=\"row\">\r\n        <div class=\"eh-txt-12 e-brd e-brd--bottom eh-txt-italic\" fxFlex=\"0 0 25%\">Type of Exercise/Activity</div>\r\n        <div class=\"eh-txt-12 e-brd e-brd--bottom eh-txt-italic\" fxFlex=\"0 0 25%\">Days per Week</div>\r\n        <div class=\"eh-txt-12 e-brd e-brd--bottom eh-txt-italic\" fxFlex=\"0 0 25%\">Duration (minutes)</div>\r\n        <div class=\"eh-txt-12 e-brd e-brd--bottom eh-txt-italic\" fxFlex=\"0 0 25%\">Intensity</div>\r\n      </div>\r\n\r\n      <div *ngFor=\"let item of healthHistory.exerciseDesc\" class=\"eh-mt-16 eh-ml-4\" fxLayout=\"row\">\r\n        <div class=\"eh-txt-breakword\" fxFlex=\"0 0 25%\">{{ item.name || '-' }}</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\" fxFlex=\"0 0 25%\">{{ item.daysPerWeek || '-' }}</div>\r\n        <div class=\"e-txt e-txt--matamber600\" fxFlex=\"0 0 25%\">{{ item.duration || '-' }}</div>\r\n        <div class=\"e-txt e-txt--matamber600\" fxFlex=\"0 0 25%\">{{ item.intensity || '-' }}</div>\r\n      </div>\r\n    </ng-container>\r\n\r\n    <div class=\"e-title eh-mt-24\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">monitor_weight</mat-icon>\r\n      <span>Weight History</span>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Have you tried to lose weight before?</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600\" fxFlex=\"0 0 150px\">\r\n        {{ healthHistory.triedLosingWeight ? 'Yes' : 'No' }}\r\n      </div>\r\n      <ng-container *ngIf=\"healthHistory.triedLosingWeight\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_6\" fxFlex=\"0 0 80px\">Details</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\">\r\n          <span *ngIf=\"healthHistory.weightLossTypes.length === 0\">-</span>\r\n          <div *ngFor=\"let item of healthHistory.weightLossTypes; last as isLast\">\r\n            <span *ngIf=\"!isLast\">{{item}},&nbsp;</span>\r\n            <span *ngIf=\"isLast\">{{item}}</span>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n    <ng-container *ngIf=\"me.personalDetails.category === userCategories.SPORTS_NUTRITION\">\r\n      <div class=\"e-title eh-mt-24\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">sports_cricket</mat-icon>\r\n        <span>Sports</span>\r\n      </div>\r\n\r\n      <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n        <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Which sport do you play?</div>\r\n        <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">\r\n          {{ healthHistory.sportName || '-' }}\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n        <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Any sports injury?</div>\r\n        <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">\r\n          {{ healthHistory.sportInjury || '-' }}\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n        <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Any other specific concerns?</div>\r\n        <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">\r\n          {{ healthHistory.sportConcerns || '-' }}\r\n        </div>\r\n      </div>\r\n    </ng-container>\r\n    <div class=\"e-title eh-mt-24\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">restaurant</mat-icon>\r\n      <span>Nutrition</span>\r\n    </div>\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Choice of diet</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">{{ healthHistory.mealCategory || '-' }}</div>\r\n    </div>\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">Eating Habits</div>\r\n      <div class=\"eh-ml-8 e-txt e-txt--matamber600 eh-txt-breakword\">{{ healthHistory.eatingHabits || '-' }}</div>\r\n    </div>\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div class=\"eh-txt-12 eh-ml-4 eh-txt-lh1_6\" fxFlex=\"0 0 150px\">\r\n        Do you take any vitamins or dietary supplements?\r\n      </div>\r\n      <div fxFlex=\"0 0 150px\" class=\"eh-ml-8 e-txt e-txt--matamber600\">\r\n        {{ healthHistory.onSupplements ? 'Yes' : 'No' }}\r\n      </div>\r\n      <ng-container *ngIf=\"healthHistory.onSupplements\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_6\" fxFlex=\"0 0 80px\">Details</div>\r\n        <div class=\"e-txt e-txt--matamber600 eh-txt-breakword\">{{ healthHistory.supplementsDesc }}</div>\r\n      </ng-container>\r\n    </div>\r\n    <div class=\"e-title eh-mt-24\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">reorder</mat-icon>\r\n      <span>General</span>\r\n    </div>\r\n    <div class=\"eh-txt-12 eh-ml-4 eh-mt-16\">\r\n      Do you have any other concerns or issues related to your health and diet which are not been covered in this\r\n      questionnaire?\r\n    </div>\r\n    <div class=\"eh-mt-8 eh-ml-4 e-txt e-txt--matamber600 eh-txt-prewrap\"\r\n      [innerHTML]=\"healthHistory.hasOtherConcerns ? '[Yes]' : '-'\">\r\n    </div>\r\n    <div *ngIf=\"healthHistory.hasOtherConcerns\"\r\n      class=\"eh-ml-4 eh-mt-4 eh-pb-4 e-brd e-brd--bottom e-txt e-txt--matamber600 eh-txt-prewrap\"\r\n      [innerHTML]=\"healthHistory.otherConcernsDesc\">\r\n    </div>\r\n\r\n    <div class=\"eh-txt-12 eh-ml-4 eh-mt-16\">\r\n      Typical Day's Routine specific to Food Intake & Exercise Schedule\r\n    </div>\r\n    <div\r\n      class=\"eh-mt-8 eh-ml-4 eh-pb-4 e-brd e-brd--bottom  e-brd e-brd--bottom e-txt e-txt--matamber600 eh-txt-prewrap\"\r\n      [innerHTML]=\"healthHistory.typicalDayRoutine || '-'\">\r\n    </div>\r\n    <div class=\"eh-pt-16\"></div>\r\n  </ng-container>\r\n</div>\r\n<ng-template #dataAwaitedBlock>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ })

}]);
//# sourceMappingURL=src_app_main_health-history_health-history_module_ts.js.map