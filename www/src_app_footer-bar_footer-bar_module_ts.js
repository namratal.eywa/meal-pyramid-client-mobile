"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_footer-bar_footer-bar_module_ts"],{

/***/ 92858:
/*!*********************************************************!*\
  !*** ./src/app/footer-bar/footer-bar-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FooterBarPageRoutingModule": () => (/* binding */ FooterBarPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _common_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/constants */ 31896);
/* harmony import */ var _core_guards__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../core/guards */ 61423);
/* harmony import */ var _footer_bar_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer-bar.page */ 79605);






const routes = [
    {
        path: '',
        component: _footer_bar_page__WEBPACK_IMPORTED_MODULE_2__.FooterBarPage,
        children: [
            {
                path: '',
                redirectTo: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.HOME,
                pathMatch: 'full'
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.HOME,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ../home/home.module */ 3467)).then(m => m.HomeModule)
            },
            {
                path: 'p',
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ../pages/pages.module */ 18950)).then(m => m.PagesModule)
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.APPOINTMENTS,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_main_appointment_appointment_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../main/appointment/appointment.module */ 26109)).then((m) => m.AppointmentModule)
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.DIETS,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_main_diet_diet_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../main/diet/diet.module */ 54041)).then((m) => m.DietModule)
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.PROFILE,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_profile_profile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../profile/profile.module */ 84523)).then((m) => m.ProfileModule)
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.HEALTH_HISTORY,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_shared_button-file-upload_index_ts"), __webpack_require__.e("src_app_main_health-history_health-history_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ../main/health-history/health-history.module */ 98213)).then((m) => m.HealthHistoryModule)
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.PROGRESS,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_shared_button-file-upload_index_ts"), __webpack_require__.e("src_app_main_progress_progress_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ../main/progress/progress.module */ 91080)).then((m) => m.ProgressModule)
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.RECOMMENDATIONS,
                canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_1__.AuthGuard],
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_main_recommendation_recommendation_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../main/recommendation/recommendation.module */ 48592)).then((m) => m.RecommendationModule)
            },
        ],
    }
];
let FooterBarPageRoutingModule = class FooterBarPageRoutingModule {
};
FooterBarPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    })
], FooterBarPageRoutingModule);



/***/ }),

/***/ 59709:
/*!*************************************************!*\
  !*** ./src/app/footer-bar/footer-bar.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FooterBarPageModule": () => (/* binding */ FooterBarPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _pages_pages_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../pages/pages.module */ 18950);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _footer_bar_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer-bar-routing.module */ 92858);
/* harmony import */ var _footer_bar_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./footer-bar.page */ 79605);








let FooterBarPageModule = class FooterBarPageModule {
};
FooterBarPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _footer_bar_routing_module__WEBPACK_IMPORTED_MODULE_1__.FooterBarPageRoutingModule,
            _pages_pages_module__WEBPACK_IMPORTED_MODULE_0__.PagesModule
        ],
        declarations: [_footer_bar_page__WEBPACK_IMPORTED_MODULE_2__.FooterBarPage],
        exports: [_footer_bar_page__WEBPACK_IMPORTED_MODULE_2__.FooterBarPage]
    })
], FooterBarPageModule);



/***/ }),

/***/ 79605:
/*!***********************************************!*\
  !*** ./src/app/footer-bar/footer-bar.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FooterBarPage": () => (/* binding */ FooterBarPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _footer_bar_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer-bar.page.html?ngResource */ 44084);
/* harmony import */ var _footer_bar_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer-bar.page.scss?ngResource */ 87816);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var _common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/constants */ 31896);
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/services */ 98138);










let FooterBarPage = class FooterBarPage {
    // private _me$: Observable<User> = this._as.getMe();
    constructor(_router, navCtrl, _as) {
        this._router = _router;
        this.navCtrl = navCtrl;
        this._as = _as;
        this.chatURL = '';
        this.dispatchView = false;
        this.isPackageSubscribed = false;
        this.loading = false;
        this.routeIDs = _common_constants__WEBPACK_IMPORTED_MODULE_2__.RouteIDs;
        this.closed$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
        this.showTabs = true; // <-- show tabs by default
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
    }
    ngOnInit() {
        this._as.getMe().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.catchError)((error) => {
            return rxjs__WEBPACK_IMPORTED_MODULE_6__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$)).subscribe((user) => {
            this.me = user;
            console.log(this.me);
            this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
            this.subscribedPackage = this.me.subscribedPackage;
            this.dispatchView = true;
        });
    }
    goto(path) {
        this.path = path;
        this._router.navigate(['/tab/' + path]);
        console.log(this.path);
    }
    ngOnDestroy() {
        this.closed$.next();
        this._notifier$.next();
        this._notifier$.complete(); // <-- close subscription when component is destroyed
    }
};
FooterBarPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.NavController },
    { type: _core_services__WEBPACK_IMPORTED_MODULE_3__.AuthService }
];
FooterBarPage = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-footer-bar',
        template: _footer_bar_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_footer_bar_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FooterBarPage);



/***/ }),

/***/ 87816:
/*!************************************************************!*\
  !*** ./src/app/footer-bar/footer-bar.page.scss?ngResource ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "ion-tab-bar,\nion-tab-button {\n  background-color: #1f212c;\n}\n\nion-icon {\n  color: #ffffff;\n}\n\nion-tab-bar {\n  border-top-right-radius: 20px;\n  border-top-left-radius: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvb3Rlci1iYXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLHlCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0FBQ0YiLCJmaWxlIjoiZm9vdGVyLWJhci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdGFiLWJhcixcclxuaW9uLXRhYi1idXR0b24ge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxZjIxMmM7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuaW9uLXRhYi1iYXIge1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAyMHB4O1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDIwcHg7XHJcbn0iXX0= */";

/***/ }),

/***/ 44084:
/*!************************************************************!*\
  !*** ./src/app/footer-bar/footer-bar.page.html?ngResource ***!
  \************************************************************/
/***/ ((module) => {

module.exports = "<ion-app>\r\n    <ion-tabs >\r\n      <ion-tab-bar slot=\"bottom\">\r\n        <ion-tab-button (click)=\"goto(routeIDs.HOME)\" [disabled]=\"!isPackageSubscribed\">\r\n          <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n          <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Home</ion-label>\r\n          <ion-icon name=\"home\"></ion-icon>\r\n        </ion-tab-button>\r\n        <ion-tab-button (click)=\"goto(routeIDs.DIETS)\" [disabled]=\"!isPackageSubscribed\">\r\n          <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n          <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Diet</ion-label>\r\n          <ion-icon name=\"fast-food-outline\"></ion-icon>\r\n        </ion-tab-button>\r\n        <ion-tab-button (click)=\"goto(routeIDs.PROGRESS)\" [disabled]=\"!isPackageSubscribed\">\r\n          <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n          <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Progress</ion-label>\r\n          <ion-icon name=\"pulse-outline\"></ion-icon>\r\n        </ion-tab-button>\r\n        <ion-tab-button (click)=\"goto(routeIDs.APPOINTMENTS)\" [disabled]=\"!isPackageSubscribed\">\r\n          <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n          <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Appointment</ion-label>\r\n          <ion-icon name=\"calendar\"></ion-icon>\r\n        </ion-tab-button>\r\n        <ion-tab-button (click)=\"goto(routeIDs.HEALTH_HISTORY)\" [disabled]=\"!isPackageSubscribed\">\r\n          <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n          <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Health History</ion-label>\r\n          <ion-icon name=\"reader-outline\"></ion-icon>\r\n        </ion-tab-button>\r\n      </ion-tab-bar>\r\n    </ion-tabs>\r\n</ion-app>";

/***/ })

}]);
//# sourceMappingURL=src_app_footer-bar_footer-bar_module_ts.js.map