"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_main_recommendation_recommendation_module_ts"],{

/***/ 25562:
/*!**********************************************************!*\
  !*** ./src/app/main/recommendation/download.services.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DownloadFileService": () => (/* binding */ DownloadFileService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ 41059);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/services/auth.service */ 90263);
/* harmony import */ var _core_services_sessionn_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/services/sessionn.service */ 35213);
/* harmony import */ var _core_services_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/services/shared.service */ 37536);
/* harmony import */ var _core_services_snack_bar_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../core/services/snack-bar.service */ 28492);
/* harmony import */ var _core_services_utilities_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../core/services/utilities.service */ 53623);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ 61832);














let DownloadFileService = class DownloadFileService {
  constructor(_afStore, _uts, _as, _router, _dialog, menuCtrl, loadingCtrl, _ss, _shs, _sb, transfer, androidPermissions) {
    this._afStore = _afStore;
    this._uts = _uts;
    this._as = _as;
    this._router = _router;
    this._dialog = _dialog;
    this.menuCtrl = menuCtrl;
    this.loadingCtrl = loadingCtrl;
    this._ss = _ss;
    this._shs = _shs;
    this._sb = _sb;
    this.transfer = transfer;
    this.androidPermissions = androidPermissions;
    this.file = {};
    this.fileName = "demo.jpeg";
    this.FileTransfer = this.transfer.create();
  } // case1()
  // { }


  photoviewer() {}

  downloadFile() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // const fileTransfer: FileTransferObject = this.transfer.create();
      yield _this.FileTransfer.download("https://cdn.pixabay.com/photo/2017/01/06/23/21/soap-bubble-1959327_960_720.jpg", _this.file.externalRootDirectory + '/Download/' + "soap-bubble-1959327_960_720.jpg");
    })();
  }

  getPermission() {
    this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(status => {
      if (status.hasPermission) {
        this.downloadFile();
      } else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(status => {
          if (status.hasPermission) {
            this.downloadFile();
          }
        });
      }
    });
  }

};

DownloadFileService.ctorParameters = () => [{
  type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_8__.AngularFirestore
}, {
  type: _core_services_utilities_service__WEBPACK_IMPORTED_MODULE_6__.UtilitiesService
}, {
  type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.MenuController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.LoadingController
}, {
  type: _core_services_sessionn_service__WEBPACK_IMPORTED_MODULE_3__.SessionnService
}, {
  type: _core_services_shared_service__WEBPACK_IMPORTED_MODULE_4__.SharedService
}, {
  type: _core_services_snack_bar_service__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_1__.FileTransfer
}, {
  type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_7__.AndroidPermissions
}];

DownloadFileService = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Injectable)({
  providedIn: 'root'
})], DownloadFileService);


/***/ }),

/***/ 71297:
/*!**********************************************************************!*\
  !*** ./src/app/main/recommendation/recommendation-routing.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RecommendationRoutingModule": () => (/* binding */ RecommendationRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _recommendation_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./recommendation.component */ 37930);




const routes = [
    {
        path: '**',
        component: _recommendation_component__WEBPACK_IMPORTED_MODULE_0__.RecommendationComponent
    }
];
let RecommendationRoutingModule = class RecommendationRoutingModule {
};
RecommendationRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], RecommendationRoutingModule);



/***/ }),

/***/ 37930:
/*!*****************************************************************!*\
  !*** ./src/app/main/recommendation/recommendation.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RecommendationComponent": () => (/* binding */ RecommendationComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _recommendation_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./recommendation.component.html?ngResource */ 50695);
/* harmony import */ var _recommendation_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recommendation.component.scss?ngResource */ 11009);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);
/* harmony import */ var _download_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./download.services */ 25562);












let RecommendationComponent = class RecommendationComponent {
    constructor(_as, _dialog, _us, _download) {
        this._as = _as;
        this._dialog = _dialog;
        this._us = _us;
        this._download = _download;
        this.dispatchView = false;
        this.isPackageSubscribed = false;
        this.userRecommendations = src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.UserRecommendations;
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.switchMap)((me) => {
            this.me = me;
            return this._us.getUserGeneralRecommendations(this.me.uid, this.me.membershipKey);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$))
            .subscribe((recom) => {
            if (recom.length === 0) {
                this.userGeneralRecommendation = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.UserGeneralRecommendation();
            }
            else if (recom.length === 1) {
                [this.userGeneralRecommendation] = recom;
            }
            else {
                [this.userGeneralRecommendation] = recom;
            }
            this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
            this.dispatchView = true;
        });
    }
    showDialog() {
        this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_5__.GetStartedComponent, {
            width: '60vw',
            height: '90vh',
            data: { step: this.me.onboardingStep || 0 }
        });
    }
    Demofile() {
        this._download.downloadFile();
    }
};
RecommendationComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_10__.MatDialog },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.UserService },
    { type: _download_services__WEBPACK_IMPORTED_MODULE_6__.DownloadFileService }
];
RecommendationComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-recommendation',
        template: _recommendation_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_recommendation_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RecommendationComponent);



/***/ }),

/***/ 48592:
/*!**************************************************************!*\
  !*** ./src/app/main/recommendation/recommendation.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RecommendationModule": () => (/* binding */ RecommendationModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _recommendation_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./recommendation.component */ 37930);
/* harmony import */ var _recommendation_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recommendation-routing.module */ 71297);
/* harmony import */ var src_app_shared_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/layout */ 47633);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);
/* harmony import */ var src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/material-design */ 12497);
/* harmony import */ var src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/shared-components */ 35886);











let RecommendationModule = class RecommendationModule {
};
RecommendationModule = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.NgModule)({
        declarations: [_recommendation_component__WEBPACK_IMPORTED_MODULE_0__.RecommendationComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.IonicModule,
            src_app_shared_layout__WEBPACK_IMPORTED_MODULE_2__.LayoutModule,
            src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_3__.GetStartedModule,
            src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_4__.MaterialDesignModule,
            src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_5__.SharedComponentsModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_10__.FlexLayoutModule,
            _recommendation_routing_module__WEBPACK_IMPORTED_MODULE_1__.RecommendationRoutingModule
        ],
        // providers:[
        //   DownloadFileService
        // ]
    })
], RecommendationModule);



/***/ }),

/***/ 11009:
/*!******************************************************************************!*\
  !*** ./src/app/main/recommendation/recommendation.component.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWNvbW1lbmRhdGlvbi5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 50695:
/*!******************************************************************************!*\
  !*** ./src/app/main/recommendation/recommendation.component.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

module.exports = "<app-header title=\"Recommendations\" backButtonURL=\"/tab/profile\"></app-header>\r\n<ion-content>\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <ion-button (click)=\"Demofile()\">\r\n      download File Demo\r\n    </ion-button>\r\n  </div>\r\n  <ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n    <ng-container *ngIf=\"isPackageSubscribed; else isPackageNotSubscribedBlk\">\r\n      <div class=\"eh-maxw-1200 eh-h-100p eh-mx-auto\">\r\n        <div class=\"eh-p-10 eh-ofy-scroll eh-hide-scrollbars\">\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">local_drink</mat-icon>\r\n              <span>{{userRecommendations.WATER}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.water || '-'\"></div>\r\n          </ion-card>\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">directions_run</mat-icon>\r\n              <span>{{userRecommendations.ACTIVITY}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.activity || '-'\"></div>\r\n\r\n          </ion-card>\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">local_pharmacy</mat-icon>\r\n              <span>{{userRecommendations.SUPPLEMENTATION}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.supplementation || '-'\">\r\n            </div>\r\n          </ion-card>\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">bookmark_added</mat-icon>\r\n              <span>{{userRecommendations.OTHER}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.other || '-'\"></div>\r\n\r\n          </ion-card>\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">not_interested</mat-icon>\r\n              <span>{{userRecommendations.AVOID}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.avoid || '-'\"></div>\r\n\r\n          </ion-card>\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">done</mat-icon>\r\n              <span>{{userRecommendations.DOS}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.dos || '-'\"></div>\r\n\r\n          </ion-card>\r\n          <ion-card class=\"e-card2 eh-p-4\">\r\n            <div class=\"e-title\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">grading</mat-icon>\r\n              <span>{{userRecommendations.GENERAL}}</span>\r\n            </div>\r\n            <div class=\"eh-mt-8 eh-ml-12 eh-minh-100 e-txt--matamber600\"\r\n              [innerHTML]=\"userGeneralRecommendation.general || '-'\"></div>\r\n          </ion-card>\r\n        </div>\r\n      </div>\r\n    </ng-container>\r\n\r\n    <ng-template #isPackageNotSubscribedBlk>\r\n      <div class=\"eh-h-100p\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n        <span class=\"e-txt e-txt--placeholder\">This is a PRO-member area.</span>\r\n        <span class=\"e-txt e-txt--placeholder\">Please subscribe to a package to access this section.</span>\r\n        <button *ngIf=\"!me.flags.isOrientationScheduled\" mat-stroked-button\r\n          class=\"e-mat-button e-mat-button--eblue400 eh-mt-8\" (click)=\"showDialog()\">\r\n          Resume On-boarding\r\n        </button>\r\n      </div>\r\n    </ng-template>\r\n  </ng-container>\r\n\r\n  <ng-template #dataAwaitedBlk>\r\n    <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n      <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n    </div>\r\n  </ng-template>\r\n\r\n</ion-content>\r\n<!-- <app-tabs></app-tabs> -->";

/***/ })

}]);
//# sourceMappingURL=src_app_main_recommendation_recommendation_module_ts.js.map