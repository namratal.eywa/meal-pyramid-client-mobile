"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_shared_button-file-upload_index_ts"],{

/***/ 51937:
/*!***************************************************************************!*\
  !*** ./src/app/shared/button-file-upload/button-file-upload.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonFileUploadComponent": () => (/* binding */ ButtonFileUploadComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _button_file_upload_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./button-file-upload.component.html?ngResource */ 31203);
/* harmony import */ var _button_file_upload_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./button-file-upload.component.scss?ngResource */ 82729);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/operators */ 44661);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @capacitor/camera */ 4241);
/* harmony import */ var _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @capacitor/filesystem */ 91662);
/* harmony import */ var _capacitor_preferences__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/preferences */ 85191);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @capacitor/core */ 26549);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-lightbox */ 25015);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/crop/ngx */ 82475);

















const IMAGE_DIR = 'stored-images';
let ButtonFileUploadComponent = class ButtonFileUploadComponent {
  constructor(_sts, _gs, platform, actionSheetCtrl, _lightbox, _lightboxConfig, sanitizer, crop) {
    this._sts = _sts;
    this._gs = _gs;
    this.platform = platform;
    this.actionSheetCtrl = actionSheetCtrl;
    this._lightbox = _lightbox;
    this._lightboxConfig = _lightboxConfig;
    this.sanitizer = sanitizer;
    this.crop = crop;
    this.acceptedFileTypes = '.jpg, .jpeg, .png, .pdf';
    this.disabled = false;
    this.uploadByName = '';
    this.uploadByUID = '';
    this.uploadCollectionName = '';
    this.uploadDirPath = '';
    this.uploadFileNamePrefix = '';
    this.userMembershipKey = '';
    this.userUID = '';
    this.title = 'Upload';
    this.availableEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_12__.EventEmitter();
    this.uploadEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_12__.EventEmitter();
    this.uploading = false;
    this.PHOTO_STORAGE = 'photos';
    this.photos = [];
    this.images = [];
    this.srcImage = "";
    this.lightboxAlbum = [];

    this.convertBlobToBase64 = blob => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;

      reader.onload = () => {
        resolve(reader.result);
      };

      reader.readAsDataURL(blob);
    });
  }

  ngOnInit() {
    // console.log("photo", this.photos.)
    this.loadFiles();
  }

  onFileUpload(e) {
    var _this = this;

    if (!e || !e.target || !e.target.files) {
      return;
    }

    console.log("", e);
    const fileList = e.target.files;

    if (fileList.length !== 1) {
      return;
    }

    const [file] = fileList;
    this.availableEvent.emit({
      fileSize: file.size,
      initialFileName: file.name
    });
    this.uploading = true;

    const {
      fName,
      fPath,
      uploadRef,
      snapshotChanges$
    } = this._sts.upload(file, this.uploadDirPath, this.uploadFileNamePrefix, this.userUID);

    snapshotChanges$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_13__.finalize)( /*#__PURE__*/(0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const fKey = _this.userUID ? _this._gs.getNewKey(_this.uploadCollectionName, true, _this.userUID) : _this._gs.getNewKey(_this.uploadCollectionName);
      const fURL = yield uploadRef.getDownloadURL().toPromise();
      const fileObj = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.Filee2();
      fileObj.key = fKey;
      fileObj.fileName = fName;
      fileObj.filePath = fPath;
      fileObj.fileURL = fURL;
      fileObj.fileType = file.type;
      fileObj.fileSize = file.size;
      fileObj.uploadedBy.uid = _this.uploadByUID;
      fileObj.uploadedBy.name = _this.uploadByName;
      fileObj.uploadedAt = new Date();
      fileObj.isActive = true;
      fileObj.recStatus = true;

      if (_this.userUID) {
        fileObj.uid = _this.userUID;
        fileObj.membershipKey = _this.userMembershipKey;
        yield _this._gs.setData(_this.userUID, _this.uploadCollectionName, fileObj, true, ['uploadedAt'], true, fKey);
      } else {
        yield _this._gs.setData(fKey, _this.uploadCollectionName, fileObj, true, ['uploadedAt']);
      }

      _this.uploadEvent.emit({
        fileKey: fKey,
        fileName: fName,
        fileType: file.type,
        filePath: fPath,
        fileURL: fURL,
        fileSize: file.size,
        initialFileName: file.name
      });

      _this.uploading = false;
    }))).subscribe();
  }

  getSantizeUrl(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  CameraPhotos() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Take a photo
      const capturedPhoto = yield _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__.Camera.getPhoto({
        resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__.CameraResultType.Uri,
        source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__.CameraSource.Camera,
        quality: 50,
        allowEditing: true
      });
      _this2.guestPicture = capturedPhoto.base64String;
      console.log(_this2.guestPicture);
      const savedImageFile = yield _this2.savePicture(capturedPhoto);

      _this2.photos.unshift({
        filepath: "soon...",
        webviewPath: savedImageFile.webviewPath
      });

      console.log(savedImageFile.webviewPath);
      _capacitor_preferences__WEBPACK_IMPORTED_MODULE_7__.Preferences.set({
        key: _this2.PHOTO_STORAGE,
        value: JSON.stringify(_this2.photos)
      });
    })();
  }

  addPhotoToGallery() {
    var _this3 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Take a photo
      const capturedPhoto = yield _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__.Camera.getPhoto({
        resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__.CameraResultType.Uri,
        source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_5__.CameraSource.Photos,
        quality: 50,
        allowEditing: true
      }); // Save the picture and add it to photo collection

      let savedImageFile = yield _this3.savePicture(capturedPhoto);
      _this3.photo = _this3.sanitizer.bypassSecurityTrustResourceUrl(capturedPhoto && capturedPhoto.webPath);

      _this3.photos.unshift(savedImageFile);
    })();
  }

  loadSaved() {
    var _this4 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Retrieve cached photo array data
      const photoList = yield _capacitor_preferences__WEBPACK_IMPORTED_MODULE_7__.Preferences.get({
        key: _this4.PHOTO_STORAGE
      });
      _this4.photos = JSON.parse(photoList.value) || []; // If running on the web...

      if (!_this4.platform.is('hybrid')) {
        // Display the photo by reading into base64 format
        for (let photo of _this4.photos) {
          // Read each saved photo's data from the Filesystem
          const readFile = yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Filesystem.readFile({
            path: photo.filepath,
            directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Directory.Data
          }); // Web platform only: Load the photo as base64 data

          photo.webviewPath = `data:image/jpeg;base64,${readFile.data}`;

          _this4.uploadEvent.emit(photo.webviewPath);
        }
      }
    })();
  }

  loadFiles() {
    var _this5 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this5.images = [];
      _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Filesystem.readdir({
        path: IMAGE_DIR,
        directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Directory.Data
      }).then(result => {
        _this5.loadFileData(result.files);

        console.log(result.files);
      }, /*#__PURE__*/function () {
        var _ref2 = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (err) {
          // Folder does not yet exists!
          yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Filesystem.mkdir({
            path: IMAGE_DIR,
            directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Directory.Data
          });
        });

        return function (_x) {
          return _ref2.apply(this, arguments);
        };
      }());
    })();
  } // Save picture to file on device


  savePicture(photo) {
    var _this6 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Convert photo to base64 format, required by Filesystem API to save
      const base64Data = yield _this6.readAsBase64(photo);
      const fileName = new Date().getTime() + '.jpeg';
      const savedFile = yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Filesystem.writeFile({
        path: fileName,
        data: base64Data,
        directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Directory.Data
      });
      console.log(base64Data);
      _this6.srcImage = base64Data;

      _this6.photos.forEach(childObj => {
        console.log(childObj.webviewPath, childObj.filepath);
      });

      if (_this6.platform.is('hybrid')) {
        // Display the new image by rewriting the 'file://' path to HTTP
        // Details: https://ionicframework.com/docs/building/webview#file-protocol
        return {
          filepath: savedFile.uri,
          webviewPath: _capacitor_core__WEBPACK_IMPORTED_MODULE_8__.Capacitor.convertFileSrc(savedFile.uri)
        };
      } else {
        // Use webPath to display the new image instead of base64 since it's
        // already loaded into memory
        return {
          filepath: fileName,
          webviewPath: photo.webPath
        };
      }
    })();
  } // base on the name of the file


  loadFileData(fileNames) {
    var _this7 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      for (let f of fileNames) {
        const filePath = `${IMAGE_DIR}/${f}`;
        const readFile = yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Filesystem.readFile({
          path: filePath,
          directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Directory.Data
        });

        _this7.images.push({
          name: f,
          path: filePath,
          data: `data:image/jpeg;base64,${readFile.data}`
        });
      }
    })();
  }

  readAsBase64(photo) {
    var _this8 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // "hybrid" will detect Cordova or Capacitor
      if (_this8.platform.is('hybrid')) {
        // Read the file into base64 format
        const file = yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_6__.Filesystem.readFile({
          path: photo.path
        });
        return file.data;
      } else {
        // Fetch the photo, read as a blob, then convert to base64 format
        const response = yield fetch(photo.webPath);
        const blob = yield response.blob();
        console.log(_this8.convertBlobToBase64(blob));
        return yield _this8.convertBlobToBase64(blob);
      }
    })();
  }

  deleteFile(i) {
    this.photos.pop();
  } // cropImage(fileUrl) {
  //   this.crop.crop(fileUrl, { quality: 50 })
  //     .then(
  //       newPath => {
  //         this.showCroppedImage(newPath.split('?')[0])
  //       },
  //       error => {
  //         alert('Error cropping image' + error);
  //       }
  //     );
  // }


  alertPhoto() {
    var _this9 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      let actionSheet = _this9.actionSheetCtrl.create({
        header: 'Please select option',
        cssClass: 'action-sheets-basic-page',
        buttons: [{
          text: 'Take photo',
          icon: 'camera',
          handler: () => {
            _this9.CameraPhotos();
          }
        }, {
          text: 'Choose photo from Gallery',
          icon: 'folder-open',
          handler: () => {
            _this9.addPhotoToGallery();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          data: {
            action: 'cancel'
          }
        }]
      });

      (yield actionSheet).present();
    })();
  } // *
  // * Lightbox
  // *


  lightboxInit() {
    Object.assign(this._lightboxConfig, src_app_common_constants__WEBPACK_IMPORTED_MODULE_10__.LightboxConfigs);
  }

};

ButtonFileUploadComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.StorageService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.GeneralService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.Platform
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.ActionSheetController
}, {
  type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_9__.Lightbox
}, {
  type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_9__.LightboxConfig
}, {
  type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_15__.DomSanitizer
}, {
  type: _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_11__.Crop
}];

ButtonFileUploadComponent.propDecorators = {
  acceptedFileTypes: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  disabled: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  uploadByName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  uploadByUID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  uploadCollectionName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  uploadDirPath: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  uploadFileNamePrefix: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  userMembershipKey: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  userUID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  title: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Input
  }],
  availableEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Output
  }],
  uploadEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.Output
  }]
};
ButtonFileUploadComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_16__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-button-file-upload',
  template: _button_file_upload_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_button_file_upload_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ButtonFileUploadComponent);


/***/ }),

/***/ 97597:
/*!************************************************************************!*\
  !*** ./src/app/shared/button-file-upload/button-file-upload.module.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonFileUploadModule": () => (/* binding */ ButtonFileUploadModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/layout */ 83278);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _button_file_upload_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./button-file-upload.component */ 51937);
/* harmony import */ var _shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared-components/shared-components.module */ 89366);
/* harmony import */ var _material_design__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../material-design */ 12497);










let ButtonFileUploadModule = class ButtonFileUploadModule {
};
ButtonFileUploadModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        declarations: [_button_file_upload_component__WEBPACK_IMPORTED_MODULE_0__.ButtonFileUploadComponent],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule, _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_8__.LayoutModule, _material_design__WEBPACK_IMPORTED_MODULE_2__.MaterialDesignModule, _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__.FlexLayoutModule, _shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_1__.SharedComponentsModule],
        exports: [_button_file_upload_component__WEBPACK_IMPORTED_MODULE_0__.ButtonFileUploadComponent]
    })
], ButtonFileUploadModule);



/***/ }),

/***/ 28458:
/*!****************************************************!*\
  !*** ./src/app/shared/button-file-upload/index.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonFileUploadModule": () => (/* reexport safe */ _button_file_upload_module__WEBPACK_IMPORTED_MODULE_0__.ButtonFileUploadModule)
/* harmony export */ });
/* harmony import */ var _button_file_upload_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./button-file-upload.module */ 97597);



/***/ }),

/***/ 34830:
/*!****************************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/definitions.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraDirection": () => (/* binding */ CameraDirection),
/* harmony export */   "CameraResultType": () => (/* binding */ CameraResultType),
/* harmony export */   "CameraSource": () => (/* binding */ CameraSource)
/* harmony export */ });
var CameraSource;

(function (CameraSource) {
  /**
   * Prompts the user to select either the photo album or take a photo.
   */
  CameraSource["Prompt"] = "PROMPT";
  /**
   * Take a new photo using the camera.
   */

  CameraSource["Camera"] = "CAMERA";
  /**
   * Pick an existing photo from the gallery or photo album.
   */

  CameraSource["Photos"] = "PHOTOS";
})(CameraSource || (CameraSource = {}));

var CameraDirection;

(function (CameraDirection) {
  CameraDirection["Rear"] = "REAR";
  CameraDirection["Front"] = "FRONT";
})(CameraDirection || (CameraDirection = {}));

var CameraResultType;

(function (CameraResultType) {
  CameraResultType["Uri"] = "uri";
  CameraResultType["Base64"] = "base64";
  CameraResultType["DataUrl"] = "dataUrl";
})(CameraResultType || (CameraResultType = {}));

/***/ }),

/***/ 4241:
/*!**********************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/index.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Camera": () => (/* binding */ Camera),
/* harmony export */   "CameraDirection": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraDirection),
/* harmony export */   "CameraResultType": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraResultType),
/* harmony export */   "CameraSource": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraSource)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 26549);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 34830);

const Camera = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Camera', {
  web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor_camera_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 71327)).then(m => new m.CameraWeb())
});



/***/ }),

/***/ 84970:
/*!*********************************************************************!*\
  !*** ./node_modules/@capacitor/preferences/dist/esm/definitions.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);


/***/ }),

/***/ 85191:
/*!***************************************************************!*\
  !*** ./node_modules/@capacitor/preferences/dist/esm/index.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Preferences": () => (/* binding */ Preferences)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 26549);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 84970);

const Preferences = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Preferences', {
  web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor_preferences_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 97333)).then(m => new m.PreferencesWeb())
});



/***/ }),

/***/ 82729:
/*!****************************************************************************************!*\
  !*** ./src/app/shared/button-file-upload/button-file-upload.component.scss?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJidXR0b24tZmlsZS11cGxvYWQuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 31203:
/*!****************************************************************************************!*\
  !*** ./src/app/shared/button-file-upload/button-file-upload.component.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "<input type=\"file\" #fileUpload class=\"eh-d-none\" [accept]=\"acceptedFileTypes\" [disabled]=\"disabled || uploading\"\r\n  (change)=\"onFileUpload($event)\">\r\n<!-- <app-button buttonStyle=\"e-mat-button--eblue400\" title=\"Upload\" [loading]=\"uploading\" [disabled]=\"disabled\"\r\n  (onButtonClick)=\"fileUpload.click()\">\r\n</app-button> -->\r\n\r\n\r\n\r\n\r\n<app-button buttonStyle=\"e-mat-button--eblue400\" title=\"Upload\" [loading]=\"uploading\" [disabled]=\"disabled\"\r\n  (onButtonClick)=\"alertPhoto()\">\r\n</app-button>\r\n<div class=\"eh-mt-16\">\r\n  <div class=\"eh-w-76 eh-mt-12 eh-pos-relative\" *ngFor=\"let photo of photos;  let i = index;\">\r\n    <img [src]=getSantizeUrl(photo.webviewPath) class=\"eh-fit eh-csr-ptr\" alt=\"\"\r\n      (click)=\"openLightbox()\">\r\n    <div class=\" eh-pos-absolute\" style=\"top: -16px; right: -16px;\">\r\n      <app-button-icon icon=\"cancel\" iconStyle=\"e-mat-icon--3 e-txt e-txt--matgrey600\" toolTip=\"Delete\"\r\n        (onButtonClick)=\"deleteFile(i)\">\r\n      </app-button-icon>\r\n    </div>\r\n  </div>";

/***/ })

}]);
//# sourceMappingURL=default-src_app_shared_button-file-upload_index_ts.js.map