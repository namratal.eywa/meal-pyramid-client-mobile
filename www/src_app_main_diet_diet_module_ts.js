"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_main_diet_diet_module_ts"],{

/***/ 39585:
/*!**************************************************!*\
  !*** ./src/app/main/diet/diet-routing.module.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DietRoutingModule": () => (/* binding */ DietRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _diet_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./diet.component */ 31078);




const routes = [
    {
        path: '**',
        component: _diet_component__WEBPACK_IMPORTED_MODULE_0__.DietComponent
    }
];
let DietRoutingModule = class DietRoutingModule {
};
DietRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], DietRoutingModule);



/***/ }),

/***/ 31078:
/*!*********************************************!*\
  !*** ./src/app/main/diet/diet.component.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DietComponent": () => (/* binding */ DietComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _diet_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./diet.component.html?ngResource */ 43200);
/* harmony import */ var _diet_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./diet.component.scss?ngResource */ 23542);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_shared_get_started_get_started_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/get-started/get-started.component */ 3729);











let DietComponent = class DietComponent {
    constructor(_as, _dialog, _sss, _sb, _us, _uts, _sts, _sds) {
        this._as = _as;
        this._dialog = _dialog;
        this._sss = _sss;
        this._sb = _sb;
        this._us = _us;
        this._uts = _uts;
        this._sts = _sts;
        this._sds = _sds;
        this.dispatchDietFileView = true;
        this.dispatchView = false;
        this.downloadFileName = '';
        this.fileURL = '';
        this.hasClinicalAssessments = false;
        this.isBehindSchedule = false;
        this.isDietFileAvailable = false;
        this.isImg = false;
        this.isPackageSubscribed = false;
        this.isWaitingForFirstDiet = false;
        this.previewDownloadPlaceholder = 'Select a diet to load details';
        this.previewDownloadTitle = '';
        this.showDiet = false;
        this.weeksLeft = 0;
        this.downloading = false;
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__.Subject();
    }
    ngOnInit() {
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)((me) => {
            this.me = me;
            this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
            return this._us.getActiveUserAssessments(this.me.uid, this.me.membershipKey);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)((res) => {
            this.hasClinicalAssessments = Boolean(res.length > 0 && res[0].tests.length > 0);
            return this._sss.getAllSessionsByUser(this.me.uid);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)((allSessions) => {
            this.setPreviousSessions(allSessions);
            return this._sss.getSessionsByUser(this.me.uid, this.me.membershipKey);
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.catchError)(() => {
            this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);
            return rxjs__WEBPACK_IMPORTED_MODULE_10__.EMPTY;
        }))
            .subscribe((sessions) => {
            this.setCurrentSession(sessions);
            this.setFutureSessions(sessions);
            const now = moment__WEBPACK_IMPORTED_MODULE_2__();
            const then = moment__WEBPACK_IMPORTED_MODULE_2__(this.me.subscriptionDuration.end);
            const weeks = moment__WEBPACK_IMPORTED_MODULE_2__.duration(then.diff(now)).asWeeks();
            this.weeksLeft = Math.floor(weeks);
            this.isWaitingForFirstDiet = !this.currentSession;
            if (this.hasClinicalAssessments) {
                this._sb.openInfoSnackBar('Your Clinical Assessments reports are due. Kindly upload all your Clinical Assessments reports at the earliest.');
            }
            this.dispatchView = true;
        });
    }
    ngOnDestroy() {
        this._notifier$.next();
        this._notifier$.complete();
    }
    onTabPanelClick(event, tab) {
        this.isFirstTabVisible = (event.index === 0) ? true : true;
        this.isSecondTabVisible = (event.index === 1) ? true : false;
        this.isThirdTabVisible = (event.index === 2) ? true : false;
    }
    DownloadFile(URL, Name) {
        this.downloading = true;
        console.log(URL, Name);
        this._sts.download(URL, Name);
        setTimeout(() => {
            this.downloading = false;
            this._sb.openSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessTypes.FILE_DOWNLAOD);
        }, 2000);
    }
    // onDownloadFile(file: Filee2): void {
    //   console.log(file)
    //   this.downloading = true;
    //   this._sts.download(file.fileURL, file.fileName);
    //   // this._demo.downloadFile(file.fileURL, file.fileName);
    //   setTimeout(() => {
    //     this.downloading = false;
    //     this._sb.openSnackBar(SuccessTypes.FILE_DOWNLAOD);
    //   }, 2000);
    // }
    setCurrentSession(sessionsArr) {
        const a = sessionsArr.filter((item) => item.isCurrent && item.sessionNumber);
        if (a.length === 0) {
            return;
        }
        if (a.length !== 1) {
            this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);
            return;
        }
        [this.currentSession] = a;
        this.isBehindSchedule = false;
        if (this.currentSession.sessionNumber > 0 &&
            this.currentSession.startDate) {
            const number = this.currentSession.sessionNumber + 1;
            if (number < this.me.subscribedPackage.numberOfSessions) {
                const [session] = sessionsArr.filter((item) => item.sessionNumber === number);
                this.isBehindSchedule =
                    !session.fileURL &&
                        session.idealDueDate &&
                        session.idealDueDate.valueOf() < moment__WEBPACK_IMPORTED_MODULE_2__.now();
            }
        }
    }
    setPreviousSessions(sessionsArr) {
        const sessions = sessionsArr.filter((item) => item.isCompleted && item.sessionNumber > 0);
        this.previousSessions = sessions;
    }
    setFutureSessions(sessionsArr) {
        const sessions = sessionsArr
            .filter((item) => !item.isCurrent && !item.isCompleted && item.sessionNumber > 0)
            .sort((a, b) => {
            return a.sessionNumber > b.sessionNumber ? 1 : -1;
        });
        this.futureSessions = sessions;
    }
    showDialog() {
        this._dialog.open(src_app_shared_get_started_get_started_component__WEBPACK_IMPORTED_MODULE_5__.GetStartedComponent, {
            width: '60vw',
            height: '90vh',
            data: { step: this.me.onboardingStep || 0 }
        });
    }
    onShowDiet(session) {
        this.dispatchDietFileView = false;
        this.previewDownloadTitle = `Diet # ${session.sessionNumber}`;
        this.fileURL = session.fileURL;
        this.isImg = session.fileType.includes('image');
        // this.downloadFileName = session.fileURL
        //   ? `MEALpyramid__Diet-${session.sessionNumber}__${this._uts.sanitize(
        //       this.me.name
        //     )}`
        //   : '';
        this.downloadFileName = session.fileName;
        if (!session.fileURL) {
            this.previewDownloadPlaceholder = 'The Diet file is not yet uploaded';
        }
        console.log("file");
        setTimeout(() => {
            this.dispatchDietFileView = true;
        }, 250);
    }
};
DietComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.AuthService },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__.MatDialog },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.SessionnService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.UserService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.UtilitiesService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.StaticDataService }
];
DietComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
        selector: 'app-diet',
        template: _diet_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_13__.ViewEncapsulation.None,
        styles: [_diet_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], DietComponent);



/***/ }),

/***/ 54041:
/*!******************************************!*\
  !*** ./src/app/main/diet/diet.module.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DietModule": () => (/* binding */ DietModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _diet_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./diet.component */ 31078);
/* harmony import */ var _diet_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./diet-routing.module */ 39585);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);
/* harmony import */ var src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/material-design */ 12497);
/* harmony import */ var src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/shared-components */ 35886);
/* harmony import */ var src_app_shared_upload_file__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/upload-file */ 74355);
/* harmony import */ var src_app_shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/layout/layout.module */ 68634);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-pdf-viewer */ 63940);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 93819);













let DietModule = class DietModule {
};
DietModule = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.NgModule)({
        declarations: [_diet_component__WEBPACK_IMPORTED_MODULE_0__.DietComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_9__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonicModule,
            _diet_routing_module__WEBPACK_IMPORTED_MODULE_1__.DietRoutingModule,
            src_app_shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_6__.LayoutModule,
            src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_2__.GetStartedModule,
            src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_3__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_11__.FlexLayoutModule,
            src_app_shared_upload_file__WEBPACK_IMPORTED_MODULE_5__.UploadFileModule,
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_12__.PdfViewerModule,
            src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_4__.SharedComponentsModule
        ],
    })
], DietModule);



/***/ }),

/***/ 23542:
/*!**********************************************************!*\
  !*** ./src/app/main/diet/diet.component.scss?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "ion-icon {\n  zoom: 0.75;\n}\n\n.mat-ripple.mat-tab-label.mat-focus-indicator.mat-tab-label.ng-star-inserted {\n  opacity: 1;\n}\n\n::ng-deep .mat-tab-label-active {\n  border-bottom: 2px solid #ff4081 !important;\n}\n\n::ng-deep .mat-tab-group.mat-primary .mat-ink-bar,\n.mat-tab-nav-bar.mat-primary .mat-ink-bar {\n  background-color: #ff4081;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRpZXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxVQUFBO0FBQ0Y7O0FBRUE7RUFDRSxVQUFBO0FBQ0Y7O0FBRUE7RUFDRSwyQ0FBQTtBQUNGOztBQUVBOztFQUVFLHlCQUFBO0FBQ0YiLCJmaWxlIjoiZGlldC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pY29uIHtcclxuICB6b29tOiAwLjc1XHJcbn1cclxuXHJcbi5tYXQtcmlwcGxlLm1hdC10YWItbGFiZWwubWF0LWZvY3VzLWluZGljYXRvci5tYXQtdGFiLWxhYmVsLm5nLXN0YXItaW5zZXJ0ZWQge1xyXG4gIG9wYWNpdHk6IDE7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1sYWJlbC1hY3RpdmUge1xyXG4gIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZmY0MDgxICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXRhYi1ncm91cC5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIsXHJcbi5tYXQtdGFiLW5hdi1iYXIubWF0LXByaW1hcnkgLm1hdC1pbmstYmFyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmY0MDgxO1xyXG59Il19 */";

/***/ }),

/***/ 43200:
/*!**********************************************************!*\
  !*** ./src/app/main/diet/diet.component.html?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "<app-header title=\"My Diet\"></app-header>\r\n<ion-content *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-px-12\">\r\n    <ng-container *ngIf=\"isPackageSubscribed; else isPackageNotSubscribedBlk\">\r\n      <ng-container *ngIf=\"isWaitingForFirstDiet\">\r\n        <div class=\"eh-h-100p\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--2 e-mat-icon--matgrey500\">hourglass_bottom</mat-icon>\r\n          <span class=\"e-txt e-txt--placeholder eh-txt-center\">Hang tight - while Kinita & team work out the best\r\n            diets for\r\n            you.</span>\r\n          <span class=\"e-txt e-txt--placeholder\">Please check back soon.</span>\r\n        </div>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"!isWaitingForFirstDiet\">\r\n        <div class=\"eh-maxw-1200 eh-h-100p eh-mx-auto eh-mb-6\">\r\n          <div class=\"eh-ofy-auto eh-hide-scrollbars\" fxLayout=\"column\">\r\n            <div class=\"eh-pos-relative\" fxLayout=\"column\">\r\n              <div class=\"eh-px-6 e-title\" fxFlex=\"0 0 auto\">Current</div>\r\n              <div class=\"e-card2 eh-mt-8 eh-p-12\">\r\n                <div fxLayout=\"row\">\r\n                  <div class=\"e-avatar e-avatar--square\" fxLayout=\"row\" fxLayoutAlign=\"center center\" fxFlex=\"none\">\r\n                    <mat-icon class=\"e-mat-icon e-mat-icon--1\">restaurant</mat-icon>\r\n                  </div>\r\n                  <div class=\"eh-ml-16\" fxFlex=\"grow\">\r\n                    <div class=\"eh-txt-bold\">#{{currentSession.sessionNumber}}</div>\r\n                    <div class=\"eh-txt-12\">\r\n                      Start: {{currentSession.startDate ? (currentSession.startDate | date:'mediumDate':'+0530') :\r\n                      '-'}}\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"eh-mt-12 eh-txt-underline\">{{currentSession.package.name}}</div>\r\n                <div class=\"eh-txt-12\">{{currentSession.package.description}}</div>\r\n                <div class=\"eh-mt-12\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n                  <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                    <app-avatar [userUID]=\"currentSession.staff.uid\" [size]=\"'medium'\" [border]=\"true\"\r\n                      matTooltip=\"{{currentSession.staff.name}}\"></app-avatar>\r\n                    <span class=\"eh-txt-12 eh-ml-4\">{{currentSession.staff.name}}</span>\r\n                  </div>\r\n                  <div *ngIf=\"currentSession.fileURL\" class=\"eh-px-2\" (click)=\"onDownloadFile(currentSession)\">\r\n                    <ion-icon name=\"download-outline\" size=\"large\">Dowload</ion-icon>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"e-card2 eh-mt-16 eh-ofy-auto eh-hide-scrollbars \">\r\n                <mat-tab-group class=\"e-mat-tab-group e-mat-tab-group--1 e-mat-tab-group--1 eh-h-100p\"\r\n                  animationDuration=\"0ms\" mat-align-tabs=\"center\" [selectedIndex]=\"0\" [disableRipple]=\"true\">\r\n                  <mat-tab label=\"Upcoming\">\r\n                    <ng-template matTabContent>\r\n                      <ng-container *ngIf=\"futureSessions.length > 0; else noFutureSessionsBlock\">\r\n                        <div class=\"eh-pb-16 eh-ofy-scroll eh-hide-scrollbars\">\r\n                          <div *ngFor=\"let session of futureSessions\" class=\"e-card2 eh-mt-16 eh-mx-16 eh-p-12\">\r\n                            <div fxLayout=\"row\">\r\n                              <div class=\"e-avatar e-avatar--square\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n                                fxFlex=\"none\">\r\n                                <mat-icon class=\"e-mat-icon e-mat-icon--1\">restaurant</mat-icon>\r\n                              </div>\r\n                              <div class=\"eh-ml-16\" fxFlex=\"grow\">\r\n                                <div class=\"eh-txt-bold\">#{{session.sessionNumber}}</div>\r\n                                <div class=\"eh-txt-12\">\r\n                                  Due: {{session.dueDate ? (session.dueDate | date:'mediumDate':'+0530') :\r\n                                  '-'}}\r\n                                </div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"eh-mt-12 eh-txt-underline\">{{session.package.name}}</div>\r\n                            <div class=\"eh-txt-12\">{{session.package.description}}</div>\r\n                            <div class=\"eh-mt-12\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                              <app-avatar [userUID]=\"session.staff.uid\" [size]=\"'medium'\" [border]=\"true\"\r\n                                matTooltip=\"{{session.staff.name}}\"></app-avatar>\r\n                              <span class=\"eh-txt-12 eh-ml-4\">{{session.staff.name}}</span>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </ng-container>\r\n                    </ng-template>\r\n\r\n                    <ng-template #noFutureSessionsBlock>\r\n                      <div class=\"e-txt e-txt--placeholder eh-txt-center eh-p-16\">\r\n                        No upcoming diets available\r\n                      </div>\r\n                    </ng-template>\r\n                  </mat-tab>\r\n                  <mat-tab label=\"Previous\">\r\n                    <ng-template matTabContent>\r\n                      <ng-container *ngIf=\"previousSessions.length > 0; else noPrevSessionsBlock\">\r\n                        <div class=\"eh-pb-16 eh-ofy-scroll eh-hide-scrollbars\">\r\n                          <div *ngFor=\"let session of previousSessions\" class=\"e-card2 eh-mt-16 eh-mx-16 eh-p-12\">\r\n                            <div class=\"eh-pos-relative\" fxLayout=\"row\">\r\n                              <div class=\"e-avatar e-avatar--square\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n                                fxFlex=\"none\">\r\n                                <mat-icon class=\"e-mat-icon e-mat-icon--1\">restaurant</mat-icon>\r\n                              </div>\r\n                              <div class=\"eh-ml-16\" fxFlex=\"grow\">\r\n                                <div class=\"eh-txt-bold\">#{{session.sessionNumber}}</div>\r\n                                <div class=\"eh-txt-12\">\r\n                                  Start: {{session.startDate ? (session.startDate | date:'mediumDate':'+0530') :\r\n                                  '-'}}\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"eh-pos-absolute e-tag e-tag--matgrey500\" style=\"right: 0; top: 0;\">\r\n                                Completed\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"eh-mt-12 eh-txt-underline\">{{session.package.name}}</div>\r\n                            <div class=\"eh-txt-12\">{{session.package.description}}</div>\r\n                            <div class=\"eh-mt-12\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n                              <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n                                <app-avatar [userUID]=\"session.staff.uid\" [size]=\"'medium'\" [border]=\"true\"\r\n                                  matTooltip=\"{{session.staff.name}}\"></app-avatar>\r\n                                <span class=\"eh-txt-12 eh-ml-4\">{{session.staff.name}}</span>\r\n                              </div>\r\n                              <a *ngIf=\"session.fileURL\" class=\"eh-txt-12\">\r\n                                <div *ngIf=\"session.fileURL\" class=\"eh-px-2\"\r\n                                  (click)=\"DownloadFile(session.fileURL, session.fileName)\">\r\n                                  <ion-icon name=\"download-outline\" size=\"large\"></ion-icon>\r\n                                </div>\r\n                              </a>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                      </ng-container>\r\n                    </ng-template>\r\n                  </mat-tab>\r\n                </mat-tab-group>\r\n              </div>\r\n\r\n              <div *ngIf=\"isBehindSchedule\" class=\"eh-pos-absolute eh-w-100p e-bg e-bg--ewhite\"\r\n                style=\"bottom: 0; z-index: 10;\">\r\n                <mat-divider class=\"e-mat-divider\"></mat-divider>\r\n                <div class=\"eh-h-56 eh-txt-12 e-txt e-txt--error\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                  <span class=\"\">\r\n                    You have {{weeksLeft}} week(s) left in your package\r\n                  </span>\r\n                  <span>\r\n                    You are behind schedule\r\n                  </span>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n    </ng-container>\r\n  </div>\r\n  <ng-template #isPackageNotSubscribedBlk>\r\n    <div class=\"eh-h-100v\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n      <span class=\"e-txt e-txt--placeholder\">This is a PRO-member area.</span>\r\n      <span class=\"e-txt e-txt--placeholder\">Please subscribe to a package to access this section.</span>\r\n      <button *ngIf=\"!me.flags.isOrientationScheduled\" mat-stroked-button\r\n        class=\"e-mat-button e-mat-button--eblue400 eh-mt-8\" (click)=\"showDialog()\">\r\n        Resume On-boarding\r\n      </button>\r\n    </div>\r\n  </ng-template>\r\n</ion-content>\r\n<!-- <app-tabs></app-tabs> -->\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ })

}]);
//# sourceMappingURL=src_app_main_diet_diet_module_ts.js.map