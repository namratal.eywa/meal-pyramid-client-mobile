(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["main"],{

/***/ 90158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _core_guards__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/guards */ 61423);




const routes = [
    {
        path: '',
        redirectTo: "tab",
        pathMatch: 'full'
    },
    // {
    //   path: '',
    //   loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule)
    // },
    // {
    //   path: 'p',
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () => import('./pages/pages.module').then( m => m.PagesModule)
    // },
    // {
    //   path: RouteIDs.APPOINTMENTS,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () =>
    //     import('../app/main/appointment/appointment.module').then((m) => m.AppointmentModule)
    // },
    // {
    //   path: RouteIDs.DIETS,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () => import('../app/main/diet/diet.module').then((m) => m.DietModule)
    // },
    // {
    //   path: RouteIDs.PROFILE,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () => import('./profile/profile.module').then((m) => m.ProfileModule)
    // },
    // {
    //   path: RouteIDs.HEALTH_HISTORY,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () =>
    //     import('../app/main/health-history/health-history.module').then((m) => m.HealthHistoryModule)
    // },
    // {
    //   path: RouteIDs.PROGRESS,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () => import('../app/main/progress/progress.module').then((m) => m.ProgressModule)
    // },
    // {
    //   path: RouteIDs.RECOMMENDATIONS,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () =>
    //     import('../app/main/recommendation/recommendation.module').then((m) => m.RecommendationModule)
    // },
    // {
    //   path: RouteIDs.HOME,
    //   canActivate: [AuthGuard],
    //   canActivateChild: [AuthGuard],
    //   loadChildren: () => import('./home/home.module').then( m => m.HomeModule)
    // },
    {
        path: "tab",
        canActivate: [_core_guards__WEBPACK_IMPORTED_MODULE_0__.AuthGuard],
        canActivateChild: [_core_guards__WEBPACK_IMPORTED_MODULE_0__.AuthGuard],
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_footer-bar_footer-bar_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./footer-bar/footer-bar.module */ 59709)).then(m => m.FooterBarPageModule)
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_3__.PreloadAllModules })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 55041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _app_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component.html?ngResource */ 33383);
/* harmony import */ var _app_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss?ngResource */ 79259);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ 91714);
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ 37954);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./common/constants */ 31896);
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ 92340);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 47418);














let AppComponent = class AppComponent {
  constructor(platform, splashScreen, statusBar, _router, _location, alertController, _as, _general, _sb) {
    this.platform = platform;
    this.splashScreen = splashScreen;
    this.statusBar = statusBar;
    this._router = _router;
    this._location = _location;
    this.alertController = alertController;
    this._as = _as;
    this._general = _general;
    this._sb = _sb;
    this.chatURL = '';
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.loading = false;
    this.routeIDs = _common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs;
    this.closed$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();
    this.showTabs = true; // 

    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();
    this.initializeApp();
    this.chatURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__.environment.support.chatURL; // App.addListener('backButton', () => {
    //   if (this._location.isCurrentPathEqualTo('/tab/home')) {
    //     this._general.closeApp();
    //   }
    //   else {
    //     this._location.back();
    //   }
    // });
  }

  ngOnInit() {
    this.chatURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_6__.environment.support.chatURL;

    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.catchError)(() => {
      this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_10__.EMPTY;
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$)).subscribe(me => {
      this.me = me;
      console.log(this.me);
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.dispatchView = true;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this._router.events.subscribe(event => {
      if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_11__.NavigationEnd) {
        if (this._router.url === "/" + _common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.LOGIN) {
          this.show_footer = false;
        }
      }
    });

    this.backButtonEvent();
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(10, processNextHandler => {
      console.log('Back press handler!');

      if (this._location.isCurrentPathEqualTo("/tab/home")) {
        // Show Exit Alert!
        navigator['app'].exitApp(); // this._general.closeApp();

        processNextHandler();
      } else {
        // Navigate to back page
        console.log('Navigate to back page');

        this._location.back();
      }
    });
  } // async onLogoutClick(): Promise<void> {
  //   const confirm = this.alertController.create({
  //     header: "exit",
  //     message: "Do you really want to exit from the application?",
  //     animated: true,
  //     buttons: [
  //       {
  //         text: "Yes",
  //         handler: () => {
  //             setTimeout(async () => {
  //               navigator['app'].exitApp();
  //             }, 750);
  //         }
  //       },
  //       {
  //         text: "No",
  //         handler: async () => {
  //         },
  //       },
  //     ],
  //   });
  //   (await confirm).present();
  // }


  ngOnDestroy() {
    this.closed$.next();

    this._notifier$.next();

    this._notifier$.complete(); // <-- close subscription when component is destroyed

  }

};

AppComponent.ctorParameters = () => [{
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.Platform
}, {
  type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__.SplashScreen
}, {
  type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_2__.StatusBar
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router
}, {
  type: _angular_common__WEBPACK_IMPORTED_MODULE_13__.Location
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.AlertController
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.GeneralService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}];

AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-root',
  template: _app_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
  styles: [_app_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
})], AppComponent);


/***/ }),

/***/ 36747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _home_home_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home/home.module */ 3467);
/* harmony import */ var _pages_pages_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/pages.module */ 18950);
/* harmony import */ var _auth_auth_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth/auth.module */ 71674);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/platform-browser/animations */ 37146);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ 90158);
/* harmony import */ var _shared_shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/shared-components/shared-components.module */ 89366);
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core/core.module */ 40294);
/* harmony import */ var _shared_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared/layout */ 47633);
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ 37954);
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ 91714);
/* harmony import */ var _shared_material_design__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/material-design */ 12497);
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file/ngx */ 12358);
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ 23081);
/* harmony import */ var _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/network/ngx */ 99118);
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/camera/ngx */ 6018);
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ 41059);
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/crop/ngx */ 82475);
/* harmony import */ var _ionic_native_base64_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/base64/ngx */ 89604);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/http/ngx */ 44719);
/* harmony import */ var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ionic-native/downloader/ngx */ 397);
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ 61832);



















 //ngx








let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_21__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_22__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_23__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_24__.IonicModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__.AppRoutingModule,
            _core_core_module__WEBPACK_IMPORTED_MODULE_6__.CoreModule,
            _shared_layout__WEBPACK_IMPORTED_MODULE_7__.LayoutModule,
            _auth_auth_module__WEBPACK_IMPORTED_MODULE_2__.AuthModule,
            _pages_pages_module__WEBPACK_IMPORTED_MODULE_1__.PagesModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_25__.HttpClientModule,
            _shared_material_design__WEBPACK_IMPORTED_MODULE_10__.MaterialDesignModule,
            _shared_shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_5__.SharedComponentsModule,
            _home_home_module__WEBPACK_IMPORTED_MODULE_0__.HomeModule,
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_26__.BrowserAnimationsModule],
        providers: [_ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__.SplashScreen,
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_9__.StatusBar,
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_11__.File,
            _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_15__.FileTransfer,
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_12__.FileOpener,
            _ionic_native_network_ngx__WEBPACK_IMPORTED_MODULE_13__.Network,
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_14__.Camera,
            _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_16__.Crop,
            _ionic_native_base64_ngx__WEBPACK_IMPORTED_MODULE_17__.Base64,
            _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_18__.HTTP,
            _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_19__.Downloader,
            _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_20__.AndroidPermissions,
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_27__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_24__.IonicRouteStrategy },],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 40431:
/*!*********************************************!*\
  !*** ./src/app/auth/auth-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthPageRoutingModule": () => (/* binding */ AuthPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _common_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../common/constants */ 31896);
/* harmony import */ var _auth_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.page */ 13561);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ 78146);
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./register/register.component */ 67225);







const routes = [
    {
        path: '',
        component: _auth_page__WEBPACK_IMPORTED_MODULE_1__.AuthPage,
        children: [
            {
                path: 'register',
                component: _register_register_component__WEBPACK_IMPORTED_MODULE_3__.RegisterComponent
            },
            {
                path: _common_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs.LOGIN,
                component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__.LoginComponent
            }
        ]
    }
];
let AuthPageRoutingModule = class AuthPageRoutingModule {
};
AuthPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_6__.RouterModule],
    })
], AuthPageRoutingModule);



/***/ }),

/***/ 71674:
/*!*************************************!*\
  !*** ./src/app/auth/auth.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthModule": () => (/* binding */ AuthModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../shared/layout/layout.module */ 68634);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _auth_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth-routing.module */ 40431);
/* harmony import */ var _auth_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.page */ 13561);
/* harmony import */ var _shared_material_design__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/material-design */ 12497);
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ 78146);
/* harmony import */ var _shared_shared_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/shared-components */ 35886);
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./register/register.component */ 67225);













let AuthModule = class AuthModule {
};
AuthModule = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_9__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.IonicModule,
            _shared_material_design__WEBPACK_IMPORTED_MODULE_3__.MaterialDesignModule,
            _shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_0__.LayoutModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_12__.FlexLayoutModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.ReactiveFormsModule,
            _auth_routing_module__WEBPACK_IMPORTED_MODULE_1__.AuthPageRoutingModule,
            _shared_shared_components__WEBPACK_IMPORTED_MODULE_5__.SharedComponentsModule
        ],
        declarations: [_auth_page__WEBPACK_IMPORTED_MODULE_2__.AuthPage, _login_login_component__WEBPACK_IMPORTED_MODULE_4__.LoginComponent, _register_register_component__WEBPACK_IMPORTED_MODULE_6__.RegisterComponent]
    })
], AuthModule);



/***/ }),

/***/ 13561:
/*!***********************************!*\
  !*** ./src/app/auth/auth.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthPage": () => (/* binding */ AuthPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _auth_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.page.html?ngResource */ 2708);
/* harmony import */ var _auth_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.page.scss?ngResource */ 57783);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);




let AuthPage = class AuthPage {
    constructor() { }
    ngOnInit() {
    }
};
AuthPage.ctorParameters = () => [];
AuthPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-auth',
        template: _auth_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_auth_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AuthPage);



/***/ }),

/***/ 78146:
/*!***********************************************!*\
  !*** ./src/app/auth/login/login.component.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginComponent": () => (/* binding */ LoginComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _login_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.component.html?ngResource */ 85639);
/* harmony import */ var _login_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.component.scss?ngResource */ 83488);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_shared_dialog_box_dialog_box_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box/dialog-box.component */ 85980);
/* harmony import */ var _capacitor_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/browser */ 18313);

















let LoginComponent = class LoginComponent {
  constructor(_formBuilder, _as, _sbs, _router, _snackBarService, _location, menuCtrl, _dialog, _shs) {
    this._formBuilder = _formBuilder;
    this._as = _as;
    this._sbs = _sbs;
    this._router = _router;
    this._snackBarService = _snackBarService;
    this._location = _location;
    this.menuCtrl = menuCtrl;
    this._dialog = _dialog;
    this._shs = _shs;
    this.isPasswordVisible = false;
    this.loading = false;
    this.passwordFormControlIcon = 'visibility_off';
    this.passwordFormControlType = 'password';
    this.sendingForgotPassEmail = false;
    this.showLogin = true;
    this.toggleView = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this.menuCtrl.swipeGesture(false);
  }

  ngOnInit() {
    this.loginFormGroup = this._formBuilder.group({
      email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.email]],
      password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required]]
    });
    this.forgotPassFG = this._formBuilder.group({
      email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.email]]
    });
  }

  onLogin() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this.loginFormGroup.valid) {
        _this.loading = true;

        try {
          yield _this._as.signInWithEmail(_this.loginFormGroup.value);

          _this.loginFormGroup.reset();

          _this.checkUserFlag(); // this._router.navigate([RouteIDs.HOME]);


          _this.loading = false;
        } catch (err) {
          _this.loginFormGroup.reset();

          _this.loading = false;

          _this._sbs.openErrorSnackBar("The password is invalid or the user does not have a password.");
        }
      }
    })();
  }

  subcribeDialogue() {
    const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_5__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'Close';
    dialogBox.actionTrueBtnTxt = 'Continue';
    dialogBox.icon = 'grading';
    dialogBox.title = 'Subcribe Package';
    dialogBox.message = ' Please Subcribe Package to continue using the application. ';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(src_app_shared_dialog_box_dialog_box_component__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

    dialogRef.beforeClosed().subscribe( /*#__PURE__*/function () {
      var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
        if (!res) {
          return;
        }

        try {
          yield _capacitor_browser__WEBPACK_IMPORTED_MODULE_7__.Browser.open({
            url: "https://dev-meal-pyramid-users.web.app"
          }); //  await window.open("https://dev-meal-pyramid-users.web.app");
          //  navigator['app'].exitApp();      
        } catch (err) {}
      });

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  checkUserFlag() {
    var _this2 = this;

    this.subscription = this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.takeUntil)(this._notifier$)).subscribe( /*#__PURE__*/function () {
      var _ref2 = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (user) {
        _this2.me = yield user;
        console.log(_this2.me);

        if (_this2.me.flags.isPackageSubscribed) {
          yield _this2._router.navigate(['/']);
          console.log("home");
        } else if (_this2.me.membershipKey.length === 0 && !_this2.me.flags.isPackageSubscribed) {
          yield _this2._router.navigate(['/']);
          console.log("2", _this2.me.membershipKey.length);
        } else if (!_this2.me.flags.isPackageSubscribed) {
          console.log("3");
          yield _this2.subcribeDialogue();
        }

        console.log(_this2.me.membershipKey.length, _this2.me.membershipKey.length);
      });

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }());
  }

  onForgotPass() {
    var _this3 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this3.forgotPassFG.valid) {
        return;
      }

      try {
        _this3.sendingForgotPassEmail = true;
        yield _this3._as.sendPasswordResetEmail(_this3.forgotPassFG.get('email').value);

        _this3.forgotPassFG.reset();

        _this3.sendingForgotPassEmail = false;

        _this3._snackBarService.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessMessages.FORGOT_PASSWORD_EMAIL_SENT, 5000);
      } catch (err) {
        _this3.forgotPassFG.reset();

        _this3.sendingForgotPassEmail = false;

        _this3._snackBarService.openErrorSnackBar(err.message);
      }
    })();
  }

  toggleViewPassword() {
    this.showLogin = !this.showLogin;
    const path = this._location.isCurrentPathEqualTo(`/${src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.LOGIN}`) ? src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.FORGOT_PASSWORD : src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.LOGIN;

    this._location.replaceState(path);
  }

  onShowRegister() {
    this.toggleView.emit(true);

    this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.REGISTER]);

    this.loginFormGroup.reset();
  }

  onTogglePasswordView() {
    this.isPasswordVisible = !this.isPasswordVisible;
    this.passwordFormControlType = this.passwordFormControlType === 'password' ? 'text' : 'password';
    this.passwordFormControlIcon = this.passwordFormControlIcon === 'visibility_off' ? 'visibility' : 'visibility_off';
  }

};

LoginComponent.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: _angular_common__WEBPACK_IMPORTED_MODULE_13__.Location
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.MenuController
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SharedService
}];

LoginComponent.propDecorators = {
  toggleView: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output
  }]
};
LoginComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_16__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-login',
  template: _login_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_login_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], LoginComponent);


/***/ }),

/***/ 67225:
/*!*****************************************************!*\
  !*** ./src/app/auth/register/register.component.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterComponent": () => (/* binding */ RegisterComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _register_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.component.html?ngResource */ 64966);
/* harmony import */ var _register_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./register.component.scss?ngResource */ 98041);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);










let RegisterComponent = class RegisterComponent {
  constructor(_formBuilder, _as, _sbs, _us, _router, _hs, menuCtrl) {
    this._formBuilder = _formBuilder;
    this._as = _as;
    this._sbs = _sbs;
    this._us = _us;
    this._router = _router;
    this._hs = _hs;
    this.menuCtrl = menuCtrl;
    this.isPasswordVisible = false;
    this.loading = false;
    this.passwordFormControlIcon = 'visibility_off';
    this.passwordFormControlType = 'password';
    this.toggleView = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.menuCtrl.swipeGesture(false);
  }

  ngOnInit() {
    this.registerFormGroup = this._formBuilder.group({
      firstName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^[A-Za-z0-9]{2,51}$/)]],
      lastName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^[A-Za-z0-9]{1,51}$/)]],
      email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.email]],
      password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required]],
      mobile: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/) // Validators.minLength(10),
      // Validators.maxLength(10),
      // Validators.pattern('^[0-9]*$')
      ]]
    });
  }

  get email() {
    return this.registerFormGroup.get('email').value;
  }

  get password() {
    return this.registerFormGroup.get('password').value;
  }

  get firstName() {
    return this.registerFormGroup.get('firstName').value;
  }

  get lastName() {
    return this.registerFormGroup.get('lastName').value;
  }

  get mobile() {
    return this.registerFormGroup.get('mobile').value;
  }

  onRegister() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this.registerFormGroup.valid) {
        _this.loading = true;

        try {
          const cred = yield _this._as.setFirebaseMe({
            email: _this.email,
            password: _this.password
          });
          const data = { ..._this.registerFormGroup.value,
            uid: cred.user.uid
          };
          yield _this._us.setUserFromAuth(data);

          _this._hs.notifyAllAdmins({
            message: `A new user, ${_this.firstName} ${_this.lastName} just registered!`,
            isSystemGenerated: true
          }).then().catch();

          _this._hs.sendWelcomeEmail({
            name: _this.firstName,
            email: _this.email
          }).then().catch();

          _this._hs.sendHelloEmail({
            firstName: _this.firstName,
            lastName: _this.lastName,
            email: _this.email,
            mobile: _this.mobile
          }).then().catch();

          _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.HOME]);
        } catch (err) {
          _this.registerFormGroup.reset();

          _this.loading = false;

          _this._sbs.openErrorSnackBar(err.message);
        }
      }
    })();
  }

  onShowLogin() {
    this.toggleView.emit(true);

    this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.LOGIN]);

    this.registerFormGroup.reset();
    console.log("login");
  }

  onTogglePasswordView() {
    this.isPasswordVisible = !this.isPasswordVisible;
    this.passwordFormControlType = this.passwordFormControlType === 'password' ? 'text' : 'password';
    this.passwordFormControlIcon = this.passwordFormControlIcon === 'visibility_off' ? 'visibility' : 'visibility_off';
  }

};

RegisterComponent.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.UserService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.HttpService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.MenuController
}];

RegisterComponent.propDecorators = {
  toggleView: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
  }]
};
RegisterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
  selector: 'app-register',
  template: _register_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_register_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], RegisterComponent);


/***/ }),

/***/ 63886:
/*!***************************************************!*\
  !*** ./src/app/common/constants/api.constants.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "APIFunctions": () => (/* binding */ APIFunctions)
/* harmony export */ });
// Verified -
var APIFunctions;
(function (APIFunctions) {
    APIFunctions["CREATE_PAYMENT"] = "/payment/auto/create";
    APIFunctions["SET_PAYMENT"] = "/payment/auto";
    APIFunctions["GET_AVAILABLE_ORIENTATION_SLOTS"] = "/slots/orientation/available/user";
    APIFunctions["SET_ORIENTATION_APPOINTMENT"] = "/appointment/orientation/book";
    APIFunctions["CANCEL_ORIENTATION_APPOINTMENT"] = "/appointment/orientation/cancel";
    APIFunctions["GET_AVAILABLE_RECALL_SLOTS"] = "/slots/recall/available/user";
    APIFunctions["SET_RECALL_APPOINTMENT"] = "/appointment/recall/book";
    APIFunctions["CANCEL_RECALL_APPOINTMENT"] = "/appointment/recall/cancel";
    APIFunctions["GET_AVAILABLE_ADVANCED_TESTING_SLOTS"] = "/slots/advanced-testing/available/user";
    APIFunctions["SET_ADVANCED_TESTING_APPOINTMENT"] = "/appointment/advanced-testing/book";
    APIFunctions["CANCEL_ADVANCED_TESTING_APPOINTMENT"] = "/appointment/advanced-testing/cancel";
    // SUBSCRIBE_PACKAGE = '/user/subscribe',
    APIFunctions["EMAIL_WELCOME"] = "/email/welcome";
    APIFunctions["EMAIL_REGISTERED"] = "/email/hello";
    APIFunctions["NOTIFY_ADMINS"] = "/notify/admins";
})(APIFunctions || (APIFunctions = {}));


/***/ }),

/***/ 4136:
/*!*******************************************************!*\
  !*** ./src/app/common/constants/general.constants.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdvancedTestPackages": () => (/* binding */ AdvancedTestPackages),
/* harmony export */   "AppointmentStatuses": () => (/* binding */ AppointmentStatuses),
/* harmony export */   "AppointmentTypes": () => (/* binding */ AppointmentTypes),
/* harmony export */   "AssessmentCategories": () => (/* binding */ AssessmentCategories),
/* harmony export */   "ErrorMessages": () => (/* binding */ ErrorMessages),
/* harmony export */   "ErrorTypes": () => (/* binding */ ErrorTypes),
/* harmony export */   "ExerciseTypes": () => (/* binding */ ExerciseTypes),
/* harmony export */   "GenderTypes": () => (/* binding */ GenderTypes),
/* harmony export */   "GeneralNutritionMedicalConditions": () => (/* binding */ GeneralNutritionMedicalConditions),
/* harmony export */   "GeneralNutritionSubCategories": () => (/* binding */ GeneralNutritionSubCategories),
/* harmony export */   "LightboxConfigs": () => (/* binding */ LightboxConfigs),
/* harmony export */   "Locations": () => (/* binding */ Locations),
/* harmony export */   "MealCategories": () => (/* binding */ MealCategories),
/* harmony export */   "PackageCategories": () => (/* binding */ PackageCategories),
/* harmony export */   "PackageTypes": () => (/* binding */ PackageTypes),
/* harmony export */   "PerformanceNutritionSubCategories": () => (/* binding */ PerformanceNutritionSubCategories),
/* harmony export */   "ProductTypes": () => (/* binding */ ProductTypes),
/* harmony export */   "RouteIDs": () => (/* binding */ RouteIDs),
/* harmony export */   "UserCategories": () => (/* binding */ UserCategories),
/* harmony export */   "UserRecommendations": () => (/* binding */ UserRecommendations),
/* harmony export */   "UserRoles": () => (/* binding */ UserRoles),
/* harmony export */   "WeightLossTypes": () => (/* binding */ WeightLossTypes)
/* harmony export */ });
var RouteIDs;
(function (RouteIDs) {
    RouteIDs["APPOINTMENTS"] = "appointments";
    RouteIDs["DIETS"] = "diets";
    RouteIDs["HEALTH_HISTORY"] = "health-history";
    RouteIDs["HOME"] = "home";
    RouteIDs["LANDING"] = "";
    RouteIDs["LOGIN"] = "login";
    RouteIDs["PROGRESS"] = "progress";
    RouteIDs["RECOMMENDATIONS"] = "recommendations";
    RouteIDs["REGISTER"] = "register";
    RouteIDs["ERROR"] = "error";
    RouteIDs["PROFILE"] = "profile";
    RouteIDs["FORGOT_PASSWORD"] = "forgot-password";
    RouteIDs["NOTIFICATION"] = "notification";
    RouteIDs["BILLING"] = "billings";
})(RouteIDs || (RouteIDs = {}));
var ErrorMessages;
(function (ErrorMessages) {
    ErrorMessages["SYSTEM"] = "Aw snap! Something went wrong.";
    ErrorMessages["SYSTEM_TRY_AGAIN"] = "Sorry, the system encountered an error. Please try again.";
    ErrorMessages["ACTION_NOT_ALLOWED"] = "This action is not allowed.";
    ErrorMessages["SAVE_FAILED"] = "Sorry, the changes could not be saved.";
    ErrorMessages["DELETE_FAILED"] = "Sorry, the item could not be deleted.";
    ErrorMessages["CANNOT_RECALL"] = "This user cannot have a Recall appointment.";
    ErrorMessages["CANNOT_ORIENTATION"] = "This user cannot have an Orientation appointment.";
    ErrorMessages["INVALID_DATE"] = "The selected date is invalid.";
    ErrorMessages["USER_NOT_LOGGED_IN"] = "Please login to continue using the application.";
    ErrorMessages["USER_NOT_AUTHORIZED"] = "You're not authorized to access this application.";
    ErrorMessages["INVALID_DATA"] = "The provided data does not pass validation.";
    ErrorMessages["DISPLAY_PICTURE_NOT_SAVED"] = "Your profile picture could not be set.";
    ErrorMessages["DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED"] = "You have chosen a display picture, but not uploaded yet. Please upload or reset prior to saving the data.";
    ErrorMessages["CANNOT_DELETE_FILE"] = "The file could not be deleted.";
    ErrorMessages["FILE_NOT_UPLOADED"] = "The file could not be uploaded.";
})(ErrorMessages || (ErrorMessages = {}));
var ErrorTypes;
(function (ErrorTypes) {
    ErrorTypes["SYSTEM"] = "Aw snap! Something went wrong.";
    ErrorTypes["SYSTEM_TRY_AGAIN"] = "Sorry, the system encountered an error. Please try again.";
    ErrorTypes["ACTION_NOT_ALLOWED"] = "This action is not allowed.";
    ErrorTypes["CANNOT_RECALL"] = "This user cannot have a Recall appointment.";
    ErrorTypes["CANNOT_ORIENTATION"] = "This user cannot have an Orientation appointment.";
    ErrorTypes["INVALID_DATE"] = "The selected date is invalid.";
    ErrorTypes["USER_NOT_LOGGED_IN"] = "Please login to continue using the application.";
    ErrorTypes["USER_NOT_AUTHORIZED"] = "You're not authorized to access this application.";
    ErrorTypes["INVALID_DATA"] = "The provided data does not pass validation.";
    ErrorTypes["DISPLAY_PICTURE_NOT_SAVED"] = "Your profile picture could not be set.";
    ErrorTypes["CANNOT_DELETE_FILE"] = "The file could not be deleted.";
})(ErrorTypes || (ErrorTypes = {}));
// Verified -
var UserRoles;
(function (UserRoles) {
    UserRoles["USER"] = "user";
    UserRoles["NUTRITIONIST"] = "nutritionist";
    UserRoles["ADVANCED_TESTING_STAFF"] = "advancedTestingStaff";
    UserRoles["LEAD_NUTRITIONIST"] = "leadNutritionist";
    UserRoles["ADMIN"] = "admin";
    UserRoles["SUPER_ADMIN"] = "superAdmin";
})(UserRoles || (UserRoles = {}));
// export enum UserTypes {
//   PRE_ORIENTATION_USER = 'preOrientationUser',
//   ORIENTATION_USER = 'orientationUser',
//   SUBSCRIBER = 'subscriber'
// }
// Verified -
var GenderTypes;
(function (GenderTypes) {
    GenderTypes["MALE"] = "Male";
    GenderTypes["FEMALE"] = "Female";
    GenderTypes["UNDISCLOSED"] = "Undisclosed";
})(GenderTypes || (GenderTypes = {}));
// Verified -
var Locations;
(function (Locations) {
    Locations["JUHU"] = "Juhu";
    Locations["KEMPS_CORNER"] = "Kemps Corner";
    Locations["ONLINE"] = "Online";
})(Locations || (Locations = {}));
// Verified -
var ProductTypes;
(function (ProductTypes) {
    ProductTypes["ORIENTATION"] = "Orientation";
    ProductTypes["SUBSCRIPTION"] = "Subscription";
    ProductTypes["ADVANCED_TESTING"] = "Advanced Testing";
})(ProductTypes || (ProductTypes = {}));
// Verified -
var PackageTypes;
(function (PackageTypes) {
    PackageTypes["STANDARD"] = "Standard";
    PackageTypes["PREMIUM"] = "Premium";
    PackageTypes["PRESTIGE"] = "Prestige";
})(PackageTypes || (PackageTypes = {}));
// Verified -
var PackageCategories;
(function (PackageCategories) {
    PackageCategories["GENERAL"] = "General Health Nutrition";
    PackageCategories["PERFORMANCE"] = "Sports & Performance Nutrition";
})(PackageCategories || (PackageCategories = {}));
// Verified -
var GeneralNutritionSubCategories;
(function (GeneralNutritionSubCategories) {
    GeneralNutritionSubCategories["FAT_LOSS"] = "Fat Loss";
    GeneralNutritionSubCategories["THERAPEUTIC"] = "Therapeutic";
    GeneralNutritionSubCategories["GROWING_KIDS"] = "Growing Kids";
    GeneralNutritionSubCategories["PREGNANCY"] = "Pregnancy";
    GeneralNutritionSubCategories["HORMONAL_BALANCE"] = "Hormonal Balance";
    GeneralNutritionSubCategories["MAINTENANCE"] = "Maintenance";
    GeneralNutritionSubCategories["BALANCED_DIETS"] = "Balanced Diets";
})(GeneralNutritionSubCategories || (GeneralNutritionSubCategories = {}));
// Verified -
var GeneralNutritionMedicalConditions;
(function (GeneralNutritionMedicalConditions) {
    GeneralNutritionMedicalConditions["DIABETES"] = "Diabetes";
    GeneralNutritionMedicalConditions["PRE_DIABETES"] = "Pre-diabetes";
    GeneralNutritionMedicalConditions["PCOD"] = "PCOD";
    GeneralNutritionMedicalConditions["HIGH_CHOLESTROL"] = "High Cholestrol";
    GeneralNutritionMedicalConditions["HIGH_TRYGLYCERIDES"] = "High Triglycerides";
    GeneralNutritionMedicalConditions["LOW_IRON"] = "Low Iron OR Anemia";
    GeneralNutritionMedicalConditions["B12_DEFICIENCY"] = "B12 Deficiency";
    GeneralNutritionMedicalConditions["D3_DEFICIENCY"] = "D3 Deficiency";
    GeneralNutritionMedicalConditions["HEART_DISEASE"] = "Heart Disease";
    GeneralNutritionMedicalConditions["HIGH_URIC_ACID"] = "High Uric Acid";
    GeneralNutritionMedicalConditions["THYROID"] = "Thyroid";
    GeneralNutritionMedicalConditions["HIGH_BLOOD_PRESSURE"] = "High Blood Pressure";
    GeneralNutritionMedicalConditions["GERD"] = "GERD";
    GeneralNutritionMedicalConditions["CHRONIC_ACIDITY"] = "Chronic Acidity";
})(GeneralNutritionMedicalConditions || (GeneralNutritionMedicalConditions = {}));
// Verified -
var PerformanceNutritionSubCategories;
(function (PerformanceNutritionSubCategories) {
    PerformanceNutritionSubCategories["ELITE_ATHLETE"] = "Elite Athlete";
    PerformanceNutritionSubCategories["JUNIOR_ATHLETE"] = "Junior Athlete";
    PerformanceNutritionSubCategories["ADVENTURE_SPORTS"] = "Adventure Sports";
    PerformanceNutritionSubCategories["FITNESS_ENTHUSIAST"] = "Fitness Enthusiast";
    PerformanceNutritionSubCategories["RECREATIONAL_ATHLETE"] = "Recreational Athlete";
    PerformanceNutritionSubCategories["MUSCLE_GAIN"] = "Muscle Gain & Fat Loss";
})(PerformanceNutritionSubCategories || (PerformanceNutritionSubCategories = {}));
// Verified -
var ExerciseTypes;
(function (ExerciseTypes) {
    ExerciseTypes["WALKING"] = "Walking";
    ExerciseTypes["JOGGING"] = "Jogging";
    ExerciseTypes["SWIMMING"] = "Swimming";
    ExerciseTypes["PILATES"] = "Pilates";
    ExerciseTypes["ZUMBA"] = "Zumba/Dance";
    ExerciseTypes["YOGA"] = "Yoga";
    ExerciseTypes["WEIGHTS"] = "Weight Training";
    ExerciseTypes["HIIT"] = "HIIT";
    ExerciseTypes["MISC"] = "Any Sports";
})(ExerciseTypes || (ExerciseTypes = {}));
// Verified -
var WeightLossTypes;
(function (WeightLossTypes) {
    WeightLossTypes["COMMERCIAL"] = "Commercial weight loss centers";
    WeightLossTypes["GYM"] = "Joined the gym";
    WeightLossTypes["SELF"] = "Self-dieting/training";
    WeightLossTypes["EXPERT"] = "Visited a nutritionist/fitness-expert";
    WeightLossTypes["OTHER"] = "Other";
})(WeightLossTypes || (WeightLossTypes = {}));
// Verified -
var MealCategories;
(function (MealCategories) {
    MealCategories["VEG"] = "Vegetarian";
    MealCategories["JAIN"] = "Jain";
    MealCategories["NON_VEG"] = "Non-vegetarian";
    MealCategories["VEGAN"] = "Vegan";
    MealCategories["EGG"] = "Eggetarian";
    MealCategories["OTHER"] = "Other";
})(MealCategories || (MealCategories = {}));
// export enum Frequency {
//   WEEKLY = 7,
//   FORTNIGHTLY = 14,
//   MONTHLY = 30
// }
// Verified -
var AppointmentTypes;
(function (AppointmentTypes) {
    AppointmentTypes["ORIENTATION"] = "Orientation";
    AppointmentTypes["RECALL"] = "Recall";
    AppointmentTypes["ADVANCED_TESTING"] = "Advanced Testing";
})(AppointmentTypes || (AppointmentTypes = {}));
// Verified -
var AppointmentStatuses;
(function (AppointmentStatuses) {
    AppointmentStatuses["ATTENDED"] = "Attended";
    AppointmentStatuses["NO_SHOW"] = "No-show";
})(AppointmentStatuses || (AppointmentStatuses = {}));
// Verified -
// // ToDo: Delete KIDNEY_PROFILE, OTHER_BLOOD_TESTS
var AssessmentCategories;
(function (AssessmentCategories) {
    AssessmentCategories["GENERAL"] = "General";
    // KIDNEY_PROFILE = 'Kidney Profile',
    AssessmentCategories["DIABETIC_PROFILE"] = "Diabetic Profile";
    AssessmentCategories["VITAMIN_PROFILE"] = "Vitamin Profile";
    AssessmentCategories["HORMONES"] = "Hormones";
    AssessmentCategories["SPORTS_SPECIFIC_TESTS"] = "Sports Specific Tests";
    // OTHER_BLOOD_TESTS = 'Other Blood Tests',
    AssessmentCategories["OTHER_TESTS"] = "Other Tests";
})(AssessmentCategories || (AssessmentCategories = {}));
// Verified -
// export const AdvancedTestPackages = [
//   {
//     key: 'wellnessTestBasic',
//     name: 'Wellness Test - Basic',
//     price: {
//       amount: 2300,
//       amountWithTax: 2714
//     },
//     testKeys: ['weight', 'bloodSugar', 'bodyCompositionAnalysis'],
//     isActive: true
//   },
//   {
//     key: 'wellnessTestBasicPlus',
//     name: 'Wellness Test - Basic+',
//     price: {
//       amount: 7800,
//       amountWithTax: 9204
//     },
//     testKeys: [
//       'weight',
//       'bloodSugar',
//       'bodyCompositionAnalysis',
//       'metabolicRateTesting'
//     ],
//     isActive: true
//   },
//   {
//     key: 'wellnessTestPro',
//     name: 'Wellness Test - Pro',
//     price: {
//       amount: 17300,
//       amountWithTax: 20414
//     },
//     isActive: false
//   },
//   {
//     key: 'performanceTestBasic',
//     name: 'Performance Test - Basic',
//     price: {
//       amount: 3500,
//       amountWithTax: 4130
//     },
//     testKeys: [
//       'weight',
//       'bloodSugar',
//       'bodyCompositionAnalysis',
//       'lungCapacityTesting'
//     ],
//     isActive: true
//   },
//   {
//     key: 'performanceTestBasicPlus',
//     name: 'Performance Test - Basic+',
//     price: {
//       amount: 9000,
//       amountWithTax: 10620
//     },
//     testKeys: [
//       'weight',
//       'bloodSugar',
//       'bodyCompositionAnalysis',
//       'lungCapacityTesting',
//       'metabolicRateTesting'
//     ],
//     isActive: true
//   },
//   {
//     key: 'performanceTestPro',
//     name: 'Performance Test - Pro',
//     price: {
//       amount: 22000,
//       amountWithTax: 25690
//     },
//     isActive: false
//   }
// ];
const AdvancedTestPackages = [
    {
        key: 'wellnessTestBasic',
        name: 'Wellness Test - Basic',
        price: {
            amount: 2650,
            amountWithTax: 3127
        },
        testKeys: [
            'heightWeight',
            'bloodSugar',
            'handGripStrength',
            'bodyCompositionAnalysis'
        ],
        isActive: true
    },
    {
        key: 'wellnessTestBasicPlus',
        name: 'Wellness Test - Basic+',
        price: {
            amount: 8150,
            amountWithTax: 9617
        },
        testKeys: [
            'heightWeight',
            'bloodSugar',
            'handGripStrength',
            'bodyCompositionAnalysis',
            'restingMetabolicRate'
        ],
        isActive: true
    },
    {
        key: 'wellnessTestPro',
        name: 'Wellness Test - Pro',
        price: {
            amount: 12650,
            amountWithTax: 14927
        },
        testKeys: [
            'heightWeight',
            'bloodSugar',
            'handGripStrength',
            'bodyCompositionAnalysis',
            'restingMetabolicRate',
            'vitaminScan'
        ],
        isActive: true
    },
    {
        key: 'performanceTestBasic',
        name: 'Performance Test - Basic',
        price: {
            amount: 8650,
            amountWithTax: 10207
        },
        testKeys: [
            'heightWeight',
            'bloodSugar',
            'handGripStrength',
            'bodyCompositionAnalysis',
            'pulmonaryFunction',
            'vitaminScan'
        ],
        isActive: true
    },
    {
        key: 'performanceTestBasicPlus',
        name: 'Performance Test - Basic+',
        price: {
            amount: 14150,
            amountWithTax: 16697
        },
        testKeys: [
            'heightWeight',
            'bloodSugar',
            'handGripStrength',
            'bodyCompositionAnalysis',
            'pulmonaryFunction',
            'vitaminScan',
            'restingMetabolicRate'
        ],
        isActive: true
    },
    {
        key: 'performanceTestPro',
        name: 'Performance Test - Pro',
        price: {
            amount: 24150,
            amountWithTax: 28497
        },
        testKeys: [
            'heightWeight',
            'bloodSugar',
            'handGripStrength',
            'bodyCompositionAnalysis',
            'pulmonaryFunction',
            'vitaminScan',
            'restingMetabolicRate',
            'lactateThreshold',
            'sweatAnalysis'
        ],
        isActive: false
    }
];
// Verified -
var UserRecommendations;
(function (UserRecommendations) {
    UserRecommendations["WATER"] = "Daily Water Intake";
    UserRecommendations["ACTIVITY"] = "Physical Activity or Exercise";
    UserRecommendations["SUPPLEMENTATION"] = "Supplementation";
    UserRecommendations["OTHER"] = "Other Recommendations";
    UserRecommendations["AVOID"] = "Things to Avoid";
    UserRecommendations["DOS"] = "Do's & Don'ts";
    UserRecommendations["GENERAL"] = "General Remarks";
})(UserRecommendations || (UserRecommendations = {}));
// Verified -
var UserCategories;
(function (UserCategories) {
    UserCategories["GENERAL_NUTRITION"] = "General Health Nutrition";
    UserCategories["SPORTS_NUTRITION"] = "Sports & Performance Nutrition";
})(UserCategories || (UserCategories = {}));
// Verified -
const LightboxConfigs = {
    fadeDuration: 0.5,
    disableScrolling: true,
    centerVertically: true,
    albumLabel: '',
    enableTransition: false
};
// export const Orientation = {
//   price: {
//     amount: 2000,
//     amountWithTax: 2360
//   }
// };


/***/ }),

/***/ 31896:
/*!*******************************************!*\
  !*** ./src/app/common/constants/index.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "APIFunctions": () => (/* reexport safe */ _api_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions),
/* harmony export */   "AdvancedTestPackages": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.AdvancedTestPackages),
/* harmony export */   "AppointmentStatuses": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.AppointmentStatuses),
/* harmony export */   "AppointmentTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.AppointmentTypes),
/* harmony export */   "AssessmentCategories": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.AssessmentCategories),
/* harmony export */   "ErrorMessages": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.ErrorMessages),
/* harmony export */   "ErrorTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.ErrorTypes),
/* harmony export */   "ExerciseTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.ExerciseTypes),
/* harmony export */   "GenderTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.GenderTypes),
/* harmony export */   "GeneralNutritionMedicalConditions": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.GeneralNutritionMedicalConditions),
/* harmony export */   "GeneralNutritionSubCategories": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.GeneralNutritionSubCategories),
/* harmony export */   "LightboxConfigs": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.LightboxConfigs),
/* harmony export */   "Locations": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.Locations),
/* harmony export */   "MealCategories": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.MealCategories),
/* harmony export */   "PackageCategories": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.PackageCategories),
/* harmony export */   "PackageTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.PackageTypes),
/* harmony export */   "PerformanceNutritionSubCategories": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.PerformanceNutritionSubCategories),
/* harmony export */   "ProductTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.ProductTypes),
/* harmony export */   "RouteIDs": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.RouteIDs),
/* harmony export */   "SuccessMessages": () => (/* reexport safe */ _message_constants__WEBPACK_IMPORTED_MODULE_2__.SuccessMessages),
/* harmony export */   "SuccessTypes": () => (/* reexport safe */ _message_constants__WEBPACK_IMPORTED_MODULE_2__.SuccessTypes),
/* harmony export */   "UserCategories": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.UserCategories),
/* harmony export */   "UserRecommendations": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.UserRecommendations),
/* harmony export */   "UserRoles": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.UserRoles),
/* harmony export */   "WeightLossTypes": () => (/* reexport safe */ _general_constants__WEBPACK_IMPORTED_MODULE_0__.WeightLossTypes)
/* harmony export */ });
/* harmony import */ var _general_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./general.constants */ 4136);
/* harmony import */ var _api_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./api.constants */ 63886);
/* harmony import */ var _message_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./message.constants */ 77735);





/***/ }),

/***/ 77735:
/*!*******************************************************!*\
  !*** ./src/app/common/constants/message.constants.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SuccessMessages": () => (/* binding */ SuccessMessages),
/* harmony export */   "SuccessTypes": () => (/* binding */ SuccessTypes)
/* harmony export */ });
var SuccessMessages;
(function (SuccessMessages) {
    SuccessMessages["APPT_BOOKED"] = "Your appointment has been scheduled!";
    SuccessMessages["APPT_CANCELED"] = "Your appointment has been canceled!";
    SuccessMessages["FILE_UPLOADED"] = "Thank you! Your file has been uploaded.";
    SuccessMessages["SAVE_OK"] = "The changes have been saved successfully.";
    SuccessMessages["FORGOT_PASSWORD_EMAIL_SENT"] = "If you have an account with us, you should receive an email to reset your password shortly.";
    SuccessMessages["FILE_DELETED"] = "The file has been deleted.";
})(SuccessMessages || (SuccessMessages = {}));
var SuccessTypes;
(function (SuccessTypes) {
    SuccessTypes["APPT_BOOKED"] = "Your appointment has been scheduled!";
    SuccessTypes["APPT_CANCELED"] = "Your appointment has been canceled!";
    SuccessTypes["FILE_UPLOADED"] = "Thank you! Your file has been uploaded.";
    SuccessTypes["SAVE_OK"] = "The changes have been saved successfully.";
    SuccessTypes["FILE_DOWNLAOD"] = "The file has been downloaded!";
    SuccessTypes["FORGOT_PASSWORD_EMAIL_SENT"] = "We just sent you an email to reset your password.";
})(SuccessTypes || (SuccessTypes = {}));


/***/ }),

/***/ 21055:
/*!******************************************************!*\
  !*** ./src/app/common/models/advanced-test.model.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdvancedTest": () => (/* binding */ AdvancedTest),
/* harmony export */   "MATPackage": () => (/* binding */ MATPackage),
/* harmony export */   "UserAdvancedTest": () => (/* binding */ UserAdvancedTest)
/* harmony export */ });
/* harmony import */ var _price_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./price.model */ 95814);

// Used & Verified -
class UserAdvancedTest {
    constructor(data, uts) {
        this.key = '';
        this.isCompleted = false;
        this.isLocked = false;
        this.chosenTests = [];
        this.recommendedTests = [];
        this.purchasedTests = [];
        this.recommendedPackage = new MATPackage();
        this.chosenPackage = new MATPackage();
        this.purchasedPackage = new MATPackage();
        this.uid = '';
        this.membershipKey = '';
        this.paymentKey = '';
        this.userAppointmentKey = '';
        this.isActive = false;
        this.recStatus = false;
        this.isForcedReset = false;
        this.systemLog = [];
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
            // if (data.recommendedTests) {
            //   this.recommendedTests = AdvancedTest.FromData(
            //     data.recommendedTests,
            //     uts
            //   );
            // }
            // if (data.purchasedTests) {
            //   this.purchasedTests = AdvancedTest.FromData(data.purchasedTests, uts);
            // }
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new UserAdvancedTest(item, uts));
        }
        return arr;
    }
}
class AdvancedTest {
    constructor(data, uts) {
        this.key = '';
        this.name = '';
        this.price = new _price_model__WEBPACK_IMPORTED_MODULE_0__.Price();
        // sort = 0;
        this.isActive = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let tests = [];
        if (data && data.length) {
            tests = data.map((item) => new AdvancedTest(item, uts));
        }
        return tests;
    }
}
class MATPackage {
    constructor(data, uts) {
        this.key = '';
        this.name = '';
        this.price = new _price_model__WEBPACK_IMPORTED_MODULE_0__.Price();
        this.testKeys = [];
        this.isActive = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new MATPackage(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 16955:
/*!*****************************************************!*\
  !*** ./src/app/common/models/api-response.model.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "APIResponse": () => (/* binding */ APIResponse),
/* harmony export */   "Meta": () => (/* binding */ Meta)
/* harmony export */ });
// Used & Verified
class APIResponse {
    constructor(resData) {
        this.status = false;
        this.success = new Meta();
        this.data = {};
        this.error = new Meta();
        if (resData) {
            this.status = resData.status || false;
            if (resData.success) {
                this.success.code = resData.success.code || '';
                this.success.message = resData.success.message || '';
                this.success.description = resData.success.description || '';
            }
            if (resData.error) {
                this.error.code = resData.error.code || '';
                this.error.message = resData.error.message || '';
                this.error.description = resData.error.description || '';
            }
            this.data = resData.data || {};
        }
    }
}
class Meta {
    constructor() {
        this.code = '';
        this.message = '';
        this.description = '';
    }
}


/***/ }),

/***/ 35660:
/*!****************************************************!*\
  !*** ./src/app/common/models/appointment.model.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Appointment": () => (/* binding */ Appointment),
/* harmony export */   "UserAppointment": () => (/* binding */ UserAppointment)
/* harmony export */ });
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mini-user.model */ 47241);
/* harmony import */ var _mini_location_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mini-location.model */ 31347);


// Used & Verified -
class Appointment {
    constructor(data, uts) {
        this.key = '';
        this.mergeSlotKey = '';
        this.isOOO = false;
        this.isBlocked = false;
        this.isPerCalendar = false;
        this.isHoliday = false;
        this.isAvailable = false;
        this.isCompleted = false;
        this.status = '';
        this.description = '';
        this.type = '';
        this.location = new _mini_location_model__WEBPACK_IMPORTED_MODULE_1__.MLocationn();
        this.sessionStart = null;
        this.sessionEnd = null;
        this.user = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.staff = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        // isActive = false;
        this.clicked = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Appointment(item, uts));
        }
        return arr;
    }
    static FromBackend(res, uts) {
        let arr = [];
        if (res && !res.status) {
            throw new Error(res.error.description);
        }
        if (res && res.data && res.data.length) {
            arr = res.data.map((item) => new Appointment(item, uts));
        }
        return arr;
    }
}
class UserAppointment {
    constructor(data, uts) {
        this.key = '';
        this.bookedAt = null;
        this.bookedBy = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.isCanceled = false;
        this.canceledAt = null;
        this.canceledBy = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.appointment = new Appointment();
        this.uid = '';
        this.membershipKey = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new UserAppointment(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 47435:
/*!***************************************************!*\
  !*** ./src/app/common/models/assessment.model.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Assessment": () => (/* binding */ Assessment),
/* harmony export */   "UserAssessment": () => (/* binding */ UserAssessment)
/* harmony export */ });
// Used & Verified -
class UserAssessment {
    constructor(data, uts) {
        this.key = '';
        this.isCompleted = false;
        this.tests = [];
        this.uid = '';
        this.membershipKey = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
            // if (data.tests && data.tests.length) {
            //   this.tests = data.tests;
            // }
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new UserAssessment(item, uts));
        }
        return arr;
    }
}
class Assessment {
    constructor(data, uts) {
        this.category = '';
        this.key = '';
        this.name = '';
        this.description = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Assessment(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 74027:
/*!*******************************************************!*\
  !*** ./src/app/common/models/community-feed.model.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommunityFeed": () => (/* binding */ CommunityFeed)
/* harmony export */ });
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mini-user.model */ 47241);

// Used & Verified -
class CommunityFeed {
    constructor(data, uts) {
        this.key = '';
        this.description = '';
        // title = '';
        this.mediaKey = '';
        this.mediaName = '';
        this.mediaType = '';
        this.mediaPath = '';
        this.mediaURL = '';
        this.publishedAt = null;
        this.staff = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let feeds = [];
        if (data && data.length) {
            feeds = data.map((item) => new CommunityFeed(item, uts));
        }
        return feeds;
    }
}


/***/ }),

/***/ 44870:
/*!***************************************************!*\
  !*** ./src/app/common/models/dialog-box.model.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DialogBox": () => (/* binding */ DialogBox)
/* harmony export */ });
// Used & Verified -
// ToDo: Verify why is Object.getOwnPropertyNames being used?
class DialogBox {
    constructor() {
        this.actionFalseBtnStyle = '';
        this.actionFalseBtnTxt = '';
        this.actionTrueBtnStyle = '';
        this.actionTrueBtnTxt = '';
        this.icon = '';
        this.title = '';
        this.message = '';
    }
    reset() {
        Object.getOwnPropertyNames(this).forEach((key) => {
            this[key] = '';
        });
    }
}


/***/ }),

/***/ 61258:
/*!*****************************************************!*\
  !*** ./src/app/common/models/feedback-log.model.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserFeedbackLog": () => (/* binding */ UserFeedbackLog)
/* harmony export */ });
// Verified -
class UserFeedbackLog {
    constructor(data, uts) {
        this.key = '';
        this.date = null;
        this.isHydrationFollowed = null;
        this.hydrationIntake = '';
        this.isOutsideMeal = null;
        this.outsideMealsCount = '';
        this.isMealSkipped = null;
        this.skippedMealsCount = '';
        this.isRoutineChanged = null;
        this.routineChangeDetails = '';
        this.isHungryBetweenMeals = null;
        this.isAlcoholConsumed = null;
        this.alcoholConsumedDetails = '';
        this.overallFeedback = '';
        this.hadCompetition = null;
        this.competitionPerformance = '';
        this.competitionRecovery = '';
        this.upcomingCompetition = null;
        this.upcomingCompetitionDetails = '';
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let userFeedbackLogs = [];
        if (data && data.length) {
            userFeedbackLogs = data.map((item) => new UserFeedbackLog(item, uts));
        }
        return userFeedbackLogs;
    }
}


/***/ }),

/***/ 480:
/*!**********************************************!*\
  !*** ./src/app/common/models/filee.model.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Filee": () => (/* binding */ Filee),
/* harmony export */   "Filee2": () => (/* binding */ Filee2),
/* harmony export */   "MFilee2": () => (/* binding */ MFilee2)
/* harmony export */ });
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mini-user.model */ 47241);

// Used & Verified
class Filee {
    constructor(data, uts) {
        this.key = '';
        this.fileName = '';
        this.filePath = '';
        this.fileURL = '';
        this.fileType = '';
        this.uploadedAt = null;
        this.uploadedBy = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.user = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Filee(item, uts));
        }
        return arr;
    }
}
// Used & Verified -
class Filee2 {
    constructor(data, uts) {
        this.key = '';
        this.fileName = '';
        this.fileType = '';
        this.filePath = '';
        this.fileURL = '';
        this.fileSize = null;
        this.uploadedAt = null;
        this.uploadedBy = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Filee2(item, uts));
        }
        return arr;
    }
}
// Used & Verified -
class MFilee2 {
    constructor(data, uts) {
        this.fileKey = '';
        this.fileName = '';
        this.fileType = '';
        this.filePath = '';
        this.fileURL = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new MFilee2(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 82494:
/*!****************************************************!*\
  !*** ./src/app/common/models/firebase-me.model.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseMe": () => (/* binding */ FirebaseMe)
/* harmony export */ });
// Verified & Used
class FirebaseMe {
    constructor(data, uts) {
        this.authState = false;
        this.email = '';
        this.emailVerified = false;
        this.name = '';
        this.photoURL = '';
        this.uid = '';
        if (data && uts) {
            this.authState = true;
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
}


/***/ }),

/***/ 49810:
/*!*************************************************!*\
  !*** ./src/app/common/models/food-log.model.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserFoodLog": () => (/* binding */ UserFoodLog)
/* harmony export */ });
// Verified -
class UserFoodLog {
    constructor(data, uts) {
        this.key = '';
        this.date = null;
        this.breakfastAte = '';
        this.breakfastDrank = '';
        this.breakfastTime = '';
        this.midMorningAte = '';
        this.midMorningDrank = '';
        this.midMorningTime = '';
        this.lunchAte = '';
        this.lunchDrank = '';
        this.lunchTime = '';
        this.midAfternoonAte = '';
        this.midAfternoonDrank = '';
        this.midAfternoonTime = '';
        this.eveningAte = '';
        this.eveningDrank = '';
        this.eveningTime = '';
        this.dinnerAte = '';
        this.dinnerDrank = '';
        this.dinnerTime = '';
        this.bedTimeAte = '';
        this.bedTimeDrank = '';
        this.bedTimeTime = '';
        this.preWorkoutAte = '';
        this.preWorkoutDrank = '';
        this.preWorkoutTime = '';
        this.postWorkoutAte = '';
        this.postWorkoutDrank = '';
        this.postWorkoutTime = '';
        this.otherAte = '';
        this.otherDrank = '';
        this.otherTime = '';
        this.exerciseType = '';
        this.exerciseDuration = '';
        this.exerciseIntensity = '';
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let userFoodLogs = [];
        if (data && data.length) {
            userFoodLogs = data.map((item) => new UserFoodLog(item, uts));
        }
        return userFoodLogs;
    }
}


/***/ }),

/***/ 37815:
/*!***************************************************************!*\
  !*** ./src/app/common/models/general-recommendation.model.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserGeneralRecommendation": () => (/* binding */ UserGeneralRecommendation)
/* harmony export */ });
// Used & Verified -
class UserGeneralRecommendation {
    constructor(data, uts) {
        this.key = '';
        this.water = '';
        this.activity = '';
        this.supplementation = '';
        this.other = '';
        this.avoid = '';
        this.dos = '';
        this.general = '';
        this.uid = '';
        this.membershipKey = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new UserGeneralRecommendation(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 55670:
/*!********************************************!*\
  !*** ./src/app/common/models/gst.model.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GST": () => (/* binding */ GST)
/* harmony export */ });
// Used & Verified -
class GST {
    constructor(data, uts) {
        this.address = '';
        this.name = '';
        this.number = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
}


/***/ }),

/***/ 79898:
/*!*******************************************************!*\
  !*** ./src/app/common/models/health-history.model.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Exercise": () => (/* binding */ Exercise),
/* harmony export */   "UserHealthHistory": () => (/* binding */ UserHealthHistory)
/* harmony export */ });
// Used & Verified -
class UserHealthHistory {
    constructor(data, uts) {
        this.key = '';
        this.alcoholDesc = '';
        this.doesAlcohol = false;
        this.doesExercise = false;
        this.doesSmoking = false;
        this.doesTobacco = false;
        this.eatingHabits = '';
        this.exerciseDesc = [];
        this.hasOtherConcerns = false;
        this.isMCycleRegular = null;
        this.mCycleDesc = '';
        this.mealCategory = '';
        this.medicalConditions = [];
        this.medicationDesc = '';
        this.onMedication = false;
        this.onSupplements = false;
        this.otherConcernsDesc = '';
        this.otherMedicalConditions = '';
        this.smokingDesc = '';
        this.sportConcerns = '';
        this.sportInjury = '';
        this.sportName = '';
        this.supplementsDesc = '';
        this.tobaccoDesc = '';
        this.triedLosingWeight = false;
        this.typicalDayRoutine = '';
        this.weightLossTypes = [];
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new UserHealthHistory(item, uts));
        }
        return arr;
    }
}
class Exercise {
    constructor(data, uts) {
        this.daysPerWeek = null;
        this.duration = null;
        this.intensity = '';
        this.name = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Exercise(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 18073:
/*!****************************************!*\
  !*** ./src/app/common/models/index.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "APIResponse": () => (/* reexport safe */ _api_response_model__WEBPACK_IMPORTED_MODULE_18__.APIResponse),
/* harmony export */   "AdvancedTest": () => (/* reexport safe */ _advanced_test_model__WEBPACK_IMPORTED_MODULE_10__.AdvancedTest),
/* harmony export */   "Appointment": () => (/* reexport safe */ _appointment_model__WEBPACK_IMPORTED_MODULE_6__.Appointment),
/* harmony export */   "Assessment": () => (/* reexport safe */ _assessment_model__WEBPACK_IMPORTED_MODULE_9__.Assessment),
/* harmony export */   "BirthdayGreetings": () => (/* reexport safe */ _user_model__WEBPACK_IMPORTED_MODULE_2__.BirthdayGreetings),
/* harmony export */   "CommunityFeed": () => (/* reexport safe */ _community_feed_model__WEBPACK_IMPORTED_MODULE_17__.CommunityFeed),
/* harmony export */   "DialogBox": () => (/* reexport safe */ _dialog_box_model__WEBPACK_IMPORTED_MODULE_7__.DialogBox),
/* harmony export */   "Exercise": () => (/* reexport safe */ _health_history_model__WEBPACK_IMPORTED_MODULE_4__.Exercise),
/* harmony export */   "Filee": () => (/* reexport safe */ _filee_model__WEBPACK_IMPORTED_MODULE_16__.Filee),
/* harmony export */   "Filee2": () => (/* reexport safe */ _filee_model__WEBPACK_IMPORTED_MODULE_16__.Filee2),
/* harmony export */   "FirebaseMe": () => (/* reexport safe */ _firebase_me_model__WEBPACK_IMPORTED_MODULE_0__.FirebaseMe),
/* harmony export */   "Flag": () => (/* reexport safe */ _user_model__WEBPACK_IMPORTED_MODULE_2__.Flag),
/* harmony export */   "Locationn": () => (/* reexport safe */ _location_model__WEBPACK_IMPORTED_MODULE_3__.Locationn),
/* harmony export */   "MATPackage": () => (/* reexport safe */ _advanced_test_model__WEBPACK_IMPORTED_MODULE_10__.MATPackage),
/* harmony export */   "MAppointment": () => (/* reexport safe */ _mini_appointment_model__WEBPACK_IMPORTED_MODULE_12__.MAppointment),
/* harmony export */   "MFilee2": () => (/* reexport safe */ _filee_model__WEBPACK_IMPORTED_MODULE_16__.MFilee2),
/* harmony export */   "MLocationn": () => (/* reexport safe */ _mini_location_model__WEBPACK_IMPORTED_MODULE_19__.MLocationn),
/* harmony export */   "MPackage": () => (/* reexport safe */ _mini_package_model__WEBPACK_IMPORTED_MODULE_14__.MPackage),
/* harmony export */   "MUser": () => (/* reexport safe */ _mini_user_model__WEBPACK_IMPORTED_MODULE_11__.MUser),
/* harmony export */   "Meta": () => (/* reexport safe */ _api_response_model__WEBPACK_IMPORTED_MODULE_18__.Meta),
/* harmony export */   "Notification": () => (/* reexport safe */ _notification_model__WEBPACK_IMPORTED_MODULE_8__.Notification),
/* harmony export */   "Package": () => (/* reexport safe */ _package_model__WEBPACK_IMPORTED_MODULE_1__.Package),
/* harmony export */   "Payment": () => (/* reexport safe */ _payment_model__WEBPACK_IMPORTED_MODULE_5__.Payment),
/* harmony export */   "PersonalDetails": () => (/* reexport safe */ _user_model__WEBPACK_IMPORTED_MODULE_2__.PersonalDetails),
/* harmony export */   "Price": () => (/* reexport safe */ _price_model__WEBPACK_IMPORTED_MODULE_20__.Price),
/* harmony export */   "Session": () => (/* reexport safe */ _session_model__WEBPACK_IMPORTED_MODULE_13__.Session),
/* harmony export */   "SubscriptionDuration": () => (/* reexport safe */ _user_model__WEBPACK_IMPORTED_MODULE_2__.SubscriptionDuration),
/* harmony export */   "User": () => (/* reexport safe */ _user_model__WEBPACK_IMPORTED_MODULE_2__.User),
/* harmony export */   "UserAdvancedTest": () => (/* reexport safe */ _advanced_test_model__WEBPACK_IMPORTED_MODULE_10__.UserAdvancedTest),
/* harmony export */   "UserAppointment": () => (/* reexport safe */ _appointment_model__WEBPACK_IMPORTED_MODULE_6__.UserAppointment),
/* harmony export */   "UserAssessment": () => (/* reexport safe */ _assessment_model__WEBPACK_IMPORTED_MODULE_9__.UserAssessment),
/* harmony export */   "UserFeedbackLog": () => (/* reexport safe */ _feedback_log_model__WEBPACK_IMPORTED_MODULE_23__.UserFeedbackLog),
/* harmony export */   "UserFoodLog": () => (/* reexport safe */ _food_log_model__WEBPACK_IMPORTED_MODULE_24__.UserFoodLog),
/* harmony export */   "UserGeneralRecommendation": () => (/* reexport safe */ _general_recommendation_model__WEBPACK_IMPORTED_MODULE_15__.UserGeneralRecommendation),
/* harmony export */   "UserHealthHistory": () => (/* reexport safe */ _health_history_model__WEBPACK_IMPORTED_MODULE_4__.UserHealthHistory),
/* harmony export */   "UserMeasurementLog": () => (/* reexport safe */ _measurement_log_model__WEBPACK_IMPORTED_MODULE_22__.UserMeasurementLog),
/* harmony export */   "UserPhotoLog": () => (/* reexport safe */ _photo_log_model__WEBPACK_IMPORTED_MODULE_25__.UserPhotoLog),
/* harmony export */   "UserWeightLog": () => (/* reexport safe */ _weight_log_model__WEBPACK_IMPORTED_MODULE_21__.UserWeightLog),
/* harmony export */   "WeightLog": () => (/* reexport safe */ _weight_log_model__WEBPACK_IMPORTED_MODULE_21__.WeightLog),
/* harmony export */   "WeightProgress": () => (/* reexport safe */ _user_model__WEBPACK_IMPORTED_MODULE_2__.WeightProgress)
/* harmony export */ });
/* harmony import */ var _firebase_me_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./firebase-me.model */ 82494);
/* harmony import */ var _package_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./package.model */ 99190);
/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.model */ 98804);
/* harmony import */ var _location_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./location.model */ 826);
/* harmony import */ var _health_history_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./health-history.model */ 79898);
/* harmony import */ var _payment_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payment.model */ 62849);
/* harmony import */ var _appointment_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./appointment.model */ 35660);
/* harmony import */ var _dialog_box_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dialog-box.model */ 44870);
/* harmony import */ var _notification_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./notification.model */ 26926);
/* harmony import */ var _assessment_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./assessment.model */ 47435);
/* harmony import */ var _advanced_test_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./advanced-test.model */ 21055);
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./mini-user.model */ 47241);
/* harmony import */ var _mini_appointment_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./mini-appointment.model */ 63533);
/* harmony import */ var _session_model__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./session.model */ 84563);
/* harmony import */ var _mini_package_model__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./mini-package.model */ 50263);
/* harmony import */ var _general_recommendation_model__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./general-recommendation.model */ 37815);
/* harmony import */ var _filee_model__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./filee.model */ 480);
/* harmony import */ var _community_feed_model__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./community-feed.model */ 74027);
/* harmony import */ var _api_response_model__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./api-response.model */ 16955);
/* harmony import */ var _mini_location_model__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./mini-location.model */ 31347);
/* harmony import */ var _price_model__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./price.model */ 95814);
/* harmony import */ var _weight_log_model__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./weight-log.model */ 83717);
/* harmony import */ var _measurement_log_model__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./measurement-log.model */ 50765);
/* harmony import */ var _feedback_log_model__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./feedback-log.model */ 61258);
/* harmony import */ var _food_log_model__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./food-log.model */ 49810);
/* harmony import */ var _photo_log_model__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./photo-log.model */ 9735);




























/***/ }),

/***/ 826:
/*!*************************************************!*\
  !*** ./src/app/common/models/location.model.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Locationn": () => (/* binding */ Locationn)
/* harmony export */ });
// Verified -
// ToDo: Verify & Delete, use getMLocations instead
class Locationn {
    constructor(data, uts) {
        this.city = '';
        this.country = '';
        this.key = '';
        this.name = '';
        this.sort = 0;
        this.isActive = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Locationn(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 50765:
/*!********************************************************!*\
  !*** ./src/app/common/models/measurement-log.model.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserMeasurementLog": () => (/* binding */ UserMeasurementLog)
/* harmony export */ });
// Verified -
class UserMeasurementLog {
    constructor(data, uts) {
        this.key = '';
        this.date = null;
        this.abs = null;
        this.chest = null;
        this.hips = null;
        this.midArm = null;
        this.thighs = null;
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let userMeasurementLogs = [];
        if (data && data.length > 0) {
            userMeasurementLogs = data.map((item) => new UserMeasurementLog(item, uts));
        }
        return userMeasurementLogs;
    }
}
// export class UserMeasurementLog {
//   uid = '';
//   membershipKey = '';
//   logs: MeasurementLog[] = [];
//   constructor(data?: any, uts?: UtilitiesService) {
//     if (data && uts) {
//       uts.deepAssignTruthyFirestore(this, data);
//       if (data.logs) {
//         this.logs = MeasurementLog.FromData(data.logs, uts);
//       }
//     }
//   }
//   static FromData(data: any, uts: UtilitiesService): UserMeasurementLog[] {
//     let userWeightLogs: UserMeasurementLog[] = [];
//     if (data && data.length > 0) {
//       userWeightLogs = data.map((item) => new UserMeasurementLog(item, uts));
//     }
//     return userWeightLogs;
//   }
// }
// export class MeasurementLog {
//   abs: number = null;
//   chest: number = null;
//   date: Date = null;
//   hips: number = null;
//   midArm: number = null;
//   thighs: number = null;
//   constructor(data?: any, uts?: UtilitiesService) {
//     if (data && uts) {
//       uts.deepAssignTruthyFirestore(this, data);
//     }
//   }
//   static FromData(data: any, uts: UtilitiesService): MeasurementLog[] {
//     let logs: MeasurementLog[] = [];
//     if (data && data.length) {
//       logs = data.map((item) => new MeasurementLog(item, uts));
//     }
//     return logs;
//   }
// }


/***/ }),

/***/ 63533:
/*!*********************************************************!*\
  !*** ./src/app/common/models/mini-appointment.model.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAppointment": () => (/* binding */ MAppointment)
/* harmony export */ });
// Used & Verified -
class MAppointment {
    constructor() {
        this.key = '';
        this.sessionStart = null;
        this.sessionEnd = null;
    }
}


/***/ }),

/***/ 31347:
/*!******************************************************!*\
  !*** ./src/app/common/models/mini-location.model.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MLocationn": () => (/* binding */ MLocationn)
/* harmony export */ });
// Used & Verified -
class MLocationn {
    // sort = 0;
    constructor(data, uts) {
        this.key = '';
        this.name = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new MLocationn(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 50263:
/*!*****************************************************!*\
  !*** ./src/app/common/models/mini-package.model.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MPackage": () => (/* binding */ MPackage)
/* harmony export */ });
// Used & Verified -
class MPackage {
    constructor(data, uts) {
        this.key = '';
        this.name = '';
        this.category = '';
        this.type = '';
        this.description = '';
        this.numberOfMonths = null;
        this.numberOfWeeks = null;
        this.frequency = null;
        this.numberOfSessions = null;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new MPackage(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 47241:
/*!**************************************************!*\
  !*** ./src/app/common/models/mini-user.model.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MUser": () => (/* binding */ MUser)
/* harmony export */ });
// Used & Verified
class MUser {
    constructor() {
        this.name = '';
        this.uid = '';
    }
}


/***/ }),

/***/ 26926:
/*!*****************************************************!*\
  !*** ./src/app/common/models/notification.model.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Notification": () => (/* binding */ Notification)
/* harmony export */ });
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mini-user.model */ 47241);

// Used & Verified -
class Notification {
    // recStatus = true;
    constructor(data, uts) {
        this.key = '';
        this.isNew = false;
        this.isRead = false;
        this.isShown = false;
        this.isSystemGenerated = false;
        this.message = '';
        this.messageHTML = '';
        this.category = '';
        this.notifDate = null;
        this.staff = new _mini_user_model__WEBPACK_IMPORTED_MODULE_0__.MUser();
        this.uid = '';
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Notification(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 99190:
/*!************************************************!*\
  !*** ./src/app/common/models/package.model.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Package": () => (/* binding */ Package)
/* harmony export */ });
/* harmony import */ var _price_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./price.model */ 95814);

// Used & Verified -
class Package {
    constructor(data, uts) {
        this.key = '';
        this.name = '';
        this.category = '';
        this.type = '';
        this.description = '';
        this.numberOfMonths = null;
        this.numberOfWeeks = null;
        this.frequency = null;
        this.numberOfSessions = null;
        this.price = new _price_model__WEBPACK_IMPORTED_MODULE_0__.Price();
        this.clicked = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new Package(item, uts));
        }
        return arr;
    }
}


/***/ }),

/***/ 62849:
/*!************************************************!*\
  !*** ./src/app/common/models/payment.model.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Payment": () => (/* binding */ Payment)
/* harmony export */ });
// ToDo: Delete
class Payment {
    constructor(data, uts) {
        if (data && uts) {
            // uts.shallowAssignTruthy(data, this);
        }
        this.txnid = this.getRandomInt();
    }
    getRandomInt() {
        return Math.floor(100000 + Math.random() * 900000);
    }
}


/***/ }),

/***/ 9735:
/*!**************************************************!*\
  !*** ./src/app/common/models/photo-log.model.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserPhotoLog": () => (/* binding */ UserPhotoLog)
/* harmony export */ });
// Used & Verified -
class UserPhotoLog {
    constructor(data, uts) {
        this.key = '';
        this.date = null;
        this.frontalImages = [];
        this.lateralImages = [];
        this.backImages = [];
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let userPhotoLogs = [];
        if (data && data.length) {
            userPhotoLogs = data.map((item) => new UserPhotoLog(item, uts));
        }
        return userPhotoLogs;
    }
}


/***/ }),

/***/ 95814:
/*!**********************************************!*\
  !*** ./src/app/common/models/price.model.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Price": () => (/* binding */ Price)
/* harmony export */ });
// Used & Verified -
class Price {
    constructor() {
        this.amount = null;
        this.amountWithTax = null;
    }
}


/***/ }),

/***/ 84563:
/*!************************************************!*\
  !*** ./src/app/common/models/session.model.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Session": () => (/* binding */ Session)
/* harmony export */ });
/* harmony import */ var _mini_package_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mini-package.model */ 50263);
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mini-user.model */ 47241);


// Used & Verified
class Session {
    // isActive = false;
    constructor(data, uts) {
        this.key = '';
        this.previousSessionKey = '';
        this.nextSessionKey = '';
        this.sessionNumber = 0;
        // file = '';
        this.fileKey = '';
        this.fileType = '';
        this.fileName = '';
        this.filePath = '';
        this.fileURL = '';
        this.isCurrent = false;
        this.isCompleted = false;
        this.startDate = null;
        this.completedAt = null;
        this.dueDate = null;
        this.idealDueDate = null;
        this.notes = '';
        this.package = new _mini_package_model__WEBPACK_IMPORTED_MODULE_0__.MPackage();
        this.staff = new _mini_user_model__WEBPACK_IMPORTED_MODULE_1__.MUser();
        this.user = new _mini_user_model__WEBPACK_IMPORTED_MODULE_1__.MUser();
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let diets = [];
        if (data && data.length) {
            diets = data.map((item) => new Session(item, uts));
        }
        return diets;
    }
}


/***/ }),

/***/ 98804:
/*!*********************************************!*\
  !*** ./src/app/common/models/user.model.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BirthdayGreetings": () => (/* binding */ BirthdayGreetings),
/* harmony export */   "Flag": () => (/* binding */ Flag),
/* harmony export */   "PersonalDetails": () => (/* binding */ PersonalDetails),
/* harmony export */   "SubscriptionDuration": () => (/* binding */ SubscriptionDuration),
/* harmony export */   "User": () => (/* binding */ User),
/* harmony export */   "WeightProgress": () => (/* binding */ WeightProgress)
/* harmony export */ });
/* harmony import */ var _mini_package_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./mini-package.model */ 50263);
/* harmony import */ var _mini_appointment_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mini-appointment.model */ 63533);
/* harmony import */ var _mini_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mini-user.model */ 47241);
/* harmony import */ var _mini_location_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mini-location.model */ 31347);
/* harmony import */ var _gst_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./gst.model */ 55670);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../constants */ 31896);






// Used & Verified -
class User {
    constructor(data, uts) {
        this.uid = '';
        this.name = '';
        this.personalDetails = new PersonalDetails();
        this.onboardingStep = 0;
        this.orientationAppt = new _mini_appointment_model__WEBPACK_IMPORTED_MODULE_1__.MAppointment();
        this.orientationNotes = '';
        this.recommendedPackage = new _mini_package_model__WEBPACK_IMPORTED_MODULE_0__.MPackage();
        this.assignedNutritionist = new _mini_user_model__WEBPACK_IMPORTED_MODULE_2__.MUser();
        this.assignedSecondaryNutritionist = new _mini_user_model__WEBPACK_IMPORTED_MODULE_2__.MUser();
        this.subscribedPackage = new _mini_package_model__WEBPACK_IMPORTED_MODULE_0__.MPackage();
        this.subscriptionDuration = new SubscriptionDuration();
        this.buyAdvancedTestingStep = 0;
        this.weightProgress = new WeightProgress();
        this.birthdayGreetings = new BirthdayGreetings();
        this.roles = [];
        this.flags = new Flag();
        this.dOBSearchIndex = '';
        this.searchArray = [];
        this.membershipKey = '';
        this.allMembershipKeys = [];
        this.isActive = false;
        this.recStatus = false;
        this.systemLog = [];
        this.createdAt = null;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromDBUser(data, uts) {
        if (data.length && data.length === 1) {
            return new User(data[0], uts);
        }
        if (!data.length) {
            throw new Error('USER_NOT_FOUND');
        }
        throw new Error('MULTIPLE_USERS_FOUND');
    }
    static FromAuth(data, uts) {
        const user = new User();
        if (!data || !data.email || !data.uid) {
            throw new Error('INVALID_DATA');
        }
        user.personalDetails.firstName = data.firstName || '';
        user.personalDetails.lastName = data.lastName || '';
        user.personalDetails.email = data.email || '';
        user.personalDetails.mobile = data.mobile || '';
        user.roles = [_constants__WEBPACK_IMPORTED_MODULE_5__.UserRoles.USER];
        user.name = data.firstName
            ? `${uts.sanitize(data.firstName)} ${uts.sanitize(data.lastName)}`
            : '';
        user.uid = data.uid;
        user.isActive = true;
        user.recStatus = true;
        user.setSearchArray(uts);
        return user;
    }
    static FromData(data, uts) {
        let arr = [];
        if (data && data.length) {
            arr = data.map((item) => new User(item, uts));
        }
        return arr;
    }
    setSearchArray(uts) {
        const arr = uts.getSearchArray(uts.sanitize(this.personalDetails.firstName), uts.sanitize(this.personalDetails.lastName));
        this.searchArray = arr;
        // const arr = [];
        // let previousString1 = '';
        // let previousString2 = '';
        // uts
        //   .sanitize(this.personalDetails.firstName)
        //   .split('')
        //   .map((item) => {
        //     previousString1 += item.toLowerCase();
        //     arr.push(previousString1);
        //   });
        // previousString1 += ' ';
        // uts
        //   .sanitize(this.personalDetails.lastName)
        //   .split('')
        //   .map((item) => {
        //     previousString1 += item.toLowerCase();
        //     arr.push(previousString1);
        //     previousString2 += item.toLowerCase();
        //     arr.push(previousString2);
        //   });
        // this.searchArray = arr;
    }
}
class PersonalDetails {
    constructor() {
        this.addressLine1 = '';
        this.addressLine2 = '';
        this.category = 'General Health Nutrition';
        this.city = '';
        this.country = '';
        this.dateOfBirth = '';
        this.email = '';
        this.firstName = '';
        this.gender = '';
        this.gst = new _gst_model__WEBPACK_IMPORTED_MODULE_4__.GST();
        this.lastName = '';
        this.mobile = '';
        this.photoURL = '';
        this.pincode = '';
        this.preferredLocation = new _mini_location_model__WEBPACK_IMPORTED_MODULE_3__.MLocationn();
        this.referredBy = '';
        this.serviceType = '';
        this.state = '';
        this.target = '';
    }
}
class Flag {
    constructor() {
        this.isOrientationPaid = false;
        this.isOrientationScheduled = false;
        this.isOrientationCompleted = false;
        this.isPackageRecommended = false;
        this.isPackageSubscribed = false;
        this.hasAdvancedTests = false;
        this.hasClinicalAssessments = false;
        this.requiresGSTInvoice = false;
    }
}
class SubscriptionDuration {
    constructor() {
        this.start = null;
        this.end = null;
    }
}
class BirthdayGreetings {
    constructor() {
        this.sent = [];
    }
}
class WeightProgress {
    constructor() {
        this.startDate = null;
        this.startWeight = null;
        this.targetDate = null;
        this.targetWeight = null;
    }
}


/***/ }),

/***/ 83717:
/*!***************************************************!*\
  !*** ./src/app/common/models/weight-log.model.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserWeightLog": () => (/* binding */ UserWeightLog),
/* harmony export */   "WeightLog": () => (/* binding */ WeightLog)
/* harmony export */ });
// Verified -
class UserWeightLog {
    constructor(data, uts) {
        this.key = '';
        this.date = null;
        this.weight = null;
        this.uid = '';
        this.membershipKey = '';
        this.isActive = false;
        this.recStatus = false;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let userWeightLogs = [];
        if (data && data.length) {
            userWeightLogs = data.map((item) => new UserWeightLog(item, uts));
        }
        return userWeightLogs;
    }
}
// Verified
// export class UserWeightLog {
//   uid = '';
//   membershipKey = '';
//   startWeight: number = null;
//   startDate: Date = null;
//   targetWeight: number = null;
//   targetDate: Date = null;
//   logs: WeightLog[] = [];
//   constructor(data?: any, uts?: UtilitiesService) {
//     if (data && uts) {
//       uts.deepAssignTruthyFirestore(this, data);
//       if (data.logs) {
//         this.logs = WeightLog.FromData(data.logs, uts);
//       }
//     }
//   }
//   static FromData(data: any, uts: UtilitiesService): UserWeightLog[] {
//     let userWeightLogs: UserWeightLog[] = [];
//     if (data && data.length > 0) {
//       userWeightLogs = data.map((item) => new UserWeightLog(item, uts));
//     }
//     return userWeightLogs;
//   }
// }
class WeightLog {
    constructor(data, uts) {
        this.date = null;
        this.weight = null;
        if (data && uts) {
            uts.deepAssignTruthyFirestore(this, data);
        }
    }
    static FromData(data, uts) {
        let logs = [];
        if (data && data.length) {
            logs = data.map((item) => new WeightLog(item, uts));
        }
        return logs;
    }
}


/***/ }),

/***/ 40294:
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CoreModule": () => (/* binding */ CoreModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/snack-bar */ 10930);
/* harmony import */ var _angular_fire_compat__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/compat */ 11879);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment */ 92340);
/* harmony import */ var _directives__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./directives */ 44955);











let CoreModule = class CoreModule {
    constructor(core) {
        if (core) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only!');
        }
    }
    static forRoot() {
        return {
            ngModule: CoreModule,
            providers: [_services__WEBPACK_IMPORTED_MODULE_0__.AuthService, _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__.AngularFirestore]
        };
    }
};
CoreModule.ctorParameters = () => [
    { type: CoreModule, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Optional }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.SkipSelf }] }
];
CoreModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        declarations: [_directives__WEBPACK_IMPORTED_MODULE_2__.DropZoneDirective],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_7__.MatSnackBarModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpClientModule,
            _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__.AngularFirestoreModule,
            _angular_fire_compat__WEBPACK_IMPORTED_MODULE_9__.AngularFireModule.initializeApp(src_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.firebaseConfig),
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.ReactiveFormsModule
        ],
    })
], CoreModule);



/***/ }),

/***/ 45758:
/*!********************************************************!*\
  !*** ./src/app/core/directives/drop-zone.directive.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DropZoneDirective": () => (/* binding */ DropZoneDirective)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


let DropZoneDirective = class DropZoneDirective {
    constructor() {
        this.dropped = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.hovered = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
    }
    onDrop($event) {
        $event.preventDefault();
        this.dropped.emit($event.dataTransfer.files);
        this.hovered.emit(false);
    }
    onDragOver($event) {
        $event.preventDefault();
        this.hovered.emit(true);
    }
    onDragLeave($event) {
        $event.preventDefault();
        this.hovered.emit(false);
    }
};
DropZoneDirective.ctorParameters = () => [];
DropZoneDirective.propDecorators = {
    dropped: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    hovered: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onDrop: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['drop', ['$event'],] }],
    onDragOver: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['dragover', ['$event'],] }],
    onDragLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['dragleave', ['$event'],] }]
};
DropZoneDirective = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.Directive)({
        selector: '[appDropZone]'
    })
], DropZoneDirective);



/***/ }),

/***/ 44955:
/*!******************************************!*\
  !*** ./src/app/core/directives/index.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DropZoneDirective": () => (/* reexport safe */ _drop_zone_directive__WEBPACK_IMPORTED_MODULE_0__.DropZoneDirective)
/* harmony export */ });
/* harmony import */ var _drop_zone_directive__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./drop-zone.directive */ 45758);



/***/ }),

/***/ 27574:
/*!*******************************************!*\
  !*** ./src/app/core/guards/auth.guard.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthGuard": () => (/* binding */ AuthGuard)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services */ 98138);






let AuthGuard = class AuthGuard {
  constructor(_as, _router, _sb, _us) {
    this._as = _as;
    this._router = _router;
    this._sb = _sb;
    this._us = _us;
  }

  canActivate(next, state) {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const fbUser = yield _this._as.getFirebaseMeP();

        if (!fbUser) {
          // this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_LOGGED_IN);
          _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.RouteIDs.LOGIN]);

          return false;
        }

        const querySnapshot = yield _this._us.getUserByUIDP(fbUser.uid);

        if (querySnapshot.empty) {
          _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.ErrorMessages.USER_NOT_AUTHORIZED);

          _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.RouteIDs.LOGIN]);

          return false;
        }

        return true;
      } catch (err) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.ErrorMessages.USER_NOT_AUTHORIZED);

        _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.RouteIDs.LOGIN]);

        return false;
      }
    })();
  }

  canActivateChild(next, state) {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        const fbUser = yield _this2._as.getFirebaseMeP();

        if (!fbUser) {
          _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.ErrorMessages.USER_NOT_LOGGED_IN);

          _this2._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.RouteIDs.LOGIN]);

          return false;
        }

        const querySnapshot = yield _this2._us.getUserByUIDP(fbUser.uid);

        if (querySnapshot.empty) {
          _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.ErrorMessages.USER_NOT_AUTHORIZED);

          _this2._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.RouteIDs.LOGIN]);

          return false;
        }

        return true;
      } catch (err) {
        _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.ErrorMessages.USER_NOT_AUTHORIZED);

        _this2._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.RouteIDs.LOGIN]);

        return false;
      }
    })();
  }

};

AuthGuard.ctorParameters = () => [{
  type: _services__WEBPACK_IMPORTED_MODULE_2__.AuthService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router
}, {
  type: _services__WEBPACK_IMPORTED_MODULE_2__.SnackBarService
}, {
  type: _services__WEBPACK_IMPORTED_MODULE_2__.UserService
}];

AuthGuard = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
  providedIn: 'root'
})], AuthGuard);


/***/ }),

/***/ 61423:
/*!**************************************!*\
  !*** ./src/app/core/guards/index.ts ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthGuard": () => (/* reexport safe */ _auth_guard__WEBPACK_IMPORTED_MODULE_0__.AuthGuard)
/* harmony export */ });
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.guard */ 27574);



/***/ }),

/***/ 19944:
/*!******************************************************!*\
  !*** ./src/app/core/services/appointment.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentService": () => (/* binding */ AppointmentService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 86942);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utilities.service */ 53623);






let AppointmentService = class AppointmentService {
    constructor(_afStore, _uts) {
        this._afStore = _afStore;
        this._uts = _uts;
    }
    // -----------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------
    // Verified -
    // ToDo: Fetch from UserAppointments instead of Appointmemnts so that
    // * older membership appointments can be filtered
    getAppointmentsByUser(uid, membershipKey) {
        return this._afStore
            .collection('appointments', (ref) => ref
            .where('user.uid', '==', uid)
            // .where('membershipKey', '==', membershipKey)
            .where('recStatus', '==', true))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)((data) => src_app_common_models__WEBPACK_IMPORTED_MODULE_0__.Appointment.FromData(data, this._uts)));
    }
};
AppointmentService.ctorParameters = () => [
    { type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_3__.AngularFirestore },
    { type: _utilities_service__WEBPACK_IMPORTED_MODULE_1__.UtilitiesService }
];
AppointmentService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], AppointmentService);



/***/ }),

/***/ 90263:
/*!***********************************************!*\
  !*** ./src/app/core/services/auth.service.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthService": () => (/* binding */ AuthService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/compat/auth */ 5873);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ 84505);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 80823);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 25670);
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.service */ 88386);







let AuthService = class AuthService {
  constructor(_afAuth, _us) {
    var _this = this;

    this._afAuth = _afAuth;
    this._us = _us;
    this._meBS$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__.BehaviorSubject(null);
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
    this._me$ = this._meBS$.asObservable().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.debounceTime)(500), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.switchMap)(data => data));

    this._afAuth.authState.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_7__.EMPTY), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$)).subscribe( /*#__PURE__*/function () {
      var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (fbUser) {
        if (fbUser) {
          const user$ = _this._us.getUserByUID(fbUser.uid);

          _this._meBS$.next(user$);
        }
      });

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  } // Verified -


  signInWithEmail({
    email,
    password
  }) {
    return this._afAuth.signInWithEmailAndPassword(email, password);
  } // Verified -


  logout() {
    return this._afAuth.signOut();
  } // Verified -


  getFirebaseMeP() {
    return this._afAuth.authState.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.first)()).toPromise();
  } // Verified -


  getMe() {
    return this._me$;
  }

  setFirebaseMe({
    email,
    password
  }) {
    return this._afAuth.createUserWithEmailAndPassword(email, password);
  }

  sendPasswordResetEmail(email) {
    return this._afAuth.sendPasswordResetEmail(email);
  }

};

AuthService.ctorParameters = () => [{
  type: _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_10__.AngularFireAuth
}, {
  type: _user_service__WEBPACK_IMPORTED_MODULE_1__.UserService
}];

AuthService = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Injectable)({
  providedIn: 'root'
})], AuthService);


/***/ }),

/***/ 56529:
/*!***********************************************!*\
  !*** ./src/app/core/services/form.service.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FormService": () => (/* binding */ FormService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ 2508);



let FormService = class FormService {
    constructor(_fb) {
        this._fb = _fb;
    }
    // ! Verify & Delete
    formGroupBuilder(obj) {
        const keys = Object.keys(obj);
        const formControlsObj = {};
        const arr = keys.filter((i) => !Array.isArray(obj[i]));
        arr.map((i) => {
            formControlsObj[i] = new _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormControl(obj[i]);
        });
        return this._fb.group(formControlsObj);
    }
    formArrayBuilder() {
        return this._fb.array([]);
    }
};
FormService.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_0__.FormBuilder }
];
FormService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], FormService);



/***/ }),

/***/ 96300:
/*!**************************************************!*\
  !*** ./src/app/core/services/general.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeneralService": () => (/* binding */ GeneralService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/compat/app */ 51181);
/* harmony import */ var src_app_common_constants_general_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants/general.constants */ 4136);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./auth.service */ 90263);
/* harmony import */ var _sessionn_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sessionn.service */ 35213);
/* harmony import */ var _shared_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./shared.service */ 37536);
/* harmony import */ var _snack_bar_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./snack-bar.service */ 28492);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./utilities.service */ 53623);
















let GeneralService = class GeneralService {
  constructor(_afStore, _uts, _as, _router, _dialog, menuCtrl, loadingCtrl, _ss, _shs, _sb) {
    this._afStore = _afStore;
    this._uts = _uts;
    this._as = _as;
    this._router = _router;
    this._dialog = _dialog;
    this.menuCtrl = menuCtrl;
    this.loadingCtrl = loadingCtrl;
    this._ss = _ss;
    this._shs = _shs;
    this._sb = _sb;
    this.db = firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore();
  } // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------
  // Verified -


  getNewKey(collectionName, userSubColl = false, userUID = '') {
    if (userSubColl) {
      return this.db.collection('users').doc(userUID).collection(collectionName).doc().id;
    }

    return this.db.collection(collectionName).doc().id;
  } // -----------------------------------------------------------------------------------------------------
  // Setters
  // -----------------------------------------------------------------------------------------------------
  // Verified -


  setData(key, collectionName, data, isNew = false, dateFields = [], userSubColl = false, subKey = '') {
    const obj = { ...this._uts.toDB(data, dateFields),
      ...(isNew && {
        createdAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
      }),
      updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
    };

    if (userSubColl) {
      return this._afStore.collection('users').doc(key).collection(collectionName).doc(subKey).set(obj, {
        merge: true
      });
    }

    return this._afStore.collection(collectionName).doc(key).set(obj, {
      merge: true
    });
  }

  onLogout() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'logout';
      dialogBox.title = 'Logout';
      dialogBox.message = '  Are you sure you want to log out ? ';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this._shs.setDialogBox(dialogBox);

      const dialogRef = _this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_4__.DialogBoxComponent);

      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            setTimeout( /*#__PURE__*/(0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
              yield _this._as.logout();

              _this._router.navigate([src_app_common_constants_general_constants__WEBPACK_IMPORTED_MODULE_2__.RouteIDs.LOGIN]);
            }), 750);
          } catch (err) {
            _this._sb.openErrorSnackBar(src_app_common_constants_general_constants__WEBPACK_IMPORTED_MODULE_2__.ErrorMessages.DELETE_FAILED);
          }
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    })();
  }

  closeApp() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'logout';
      dialogBox.title = 'Exist';
      dialogBox.message = 'Do you want to close the app?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this2._shs.setDialogBox(dialogBox);

      const dialogRef = _this2._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_4__.DialogBoxComponent);

      dialogRef.beforeClosed().subscribe( /*#__PURE__*/function () {
        var _ref3 = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            return;
          }

          try {
            navigator['app'].exitApp();
          } catch (err) {
            _this2._sb.openErrorSnackBar(src_app_common_constants_general_constants__WEBPACK_IMPORTED_MODULE_2__.ErrorMessages.DELETE_FAILED);
          }
        });

        return function (_x2) {
          return _ref3.apply(this, arguments);
        };
      }());
    })();
  }

};

GeneralService.ctorParameters = () => [{
  type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_10__.AngularFirestore
}, {
  type: _utilities_service__WEBPACK_IMPORTED_MODULE_9__.UtilitiesService
}, {
  type: _auth_service__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__.MatDialog
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.MenuController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.LoadingController
}, {
  type: _sessionn_service__WEBPACK_IMPORTED_MODULE_6__.SessionnService
}, {
  type: _shared_service__WEBPACK_IMPORTED_MODULE_7__.SharedService
}, {
  type: _snack_bar_service__WEBPACK_IMPORTED_MODULE_8__.SnackBarService
}];

GeneralService = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Injectable)({
  providedIn: 'root'
})], GeneralService);


/***/ }),

/***/ 74922:
/*!******************************************************!*\
  !*** ./src/app/core/services/global-data.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GlobalDataService": () => (/* binding */ GlobalDataService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


let GlobalDataService = class GlobalDataService {
    constructor() {
        this.data = {};
    }
};
GlobalDataService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)()
], GlobalDataService);



/***/ }),

/***/ 72229:
/*!***********************************************!*\
  !*** ./src/app/core/services/http.service.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HttpService": () => (/* binding */ HttpService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 86942);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utilities.service */ 53623);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ 92340);








let HttpService = class HttpService {
    constructor(_http, _uts) {
        this._http = _http;
        this._uts = _uts;
        this._headers = {
            'Content-Type': 'application/json'
        };
    }
    // -----------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------
    // Verified -
    getAvailableOrientationSlots(userUID, dateString, locationKey) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpParams();
        params = params.append('userUID', userUID);
        params = params.append('dateString', dateString);
        params = params.append('locationKey', locationKey);
        return this._http
            .get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.GET_AVAILABLE_ORIENTATION_SLOTS}`, { params })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => src_app_common_models__WEBPACK_IMPORTED_MODULE_2__.Appointment.FromBackend(res, this._uts)));
    }
    // Verified -
    getAvailableAdvancedTestingSlots(userUID, dateString, locationKey) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpParams();
        params = params.append('userUID', userUID);
        params = params.append('dateString', dateString);
        params = params.append('locationKey', locationKey);
        return this._http
            .get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.GET_AVAILABLE_ADVANCED_TESTING_SLOTS}`, { params })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => src_app_common_models__WEBPACK_IMPORTED_MODULE_2__.Appointment.FromBackend(res, this._uts)));
    }
    // Verified -
    getAvailableRecallSlots(userUID, dateString, locationKey) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpParams();
        params = params.append('userUID', userUID);
        params = params.append('dateString', dateString);
        params = params.append('locationKey', locationKey);
        return this._http
            .get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.GET_AVAILABLE_RECALL_SLOTS}`, { params })
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => src_app_common_models__WEBPACK_IMPORTED_MODULE_2__.Appointment.FromBackend(res, this._uts)));
    }
    // -----------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------
    // Verified -
    createPayment(body) {
        const options = {
            headers: this._headers
        };
        return this._http
            .post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.CREATE_PAYMENT}`, body, options)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => new src_app_common_models__WEBPACK_IMPORTED_MODULE_2__.APIResponse(res)));
    }
    // Verified -
    setPayment(body) {
        const options = {
            headers: this._headers
        };
        return this._http
            .post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.SET_PAYMENT}`, body, options)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)((res) => new src_app_common_models__WEBPACK_IMPORTED_MODULE_2__.APIResponse(res)));
    }
    // Verified -
    setOrientationAppointment(body) {
        const options = {
            headers: this._headers
        };
        return this._http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.SET_ORIENTATION_APPOINTMENT}`, body, options);
    }
    // Verified -
    cancelOrientationAppointment(body) {
        const options = {
            headers: this._headers
        };
        return this._http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.CANCEL_ORIENTATION_APPOINTMENT}`, body, options);
    }
    // Verified -
    setRecallAppointment(body) {
        const options = {
            headers: this._headers
        };
        return this._http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.SET_RECALL_APPOINTMENT}`, body, options);
    }
    // Verified -
    cancelRecallAppointment(body) {
        const options = {
            headers: this._headers
        };
        return this._http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.CANCEL_RECALL_APPOINTMENT}`, body, options);
    }
    // Verified -
    setAdvancedTestingAppointment(body) {
        const options = {
            headers: this._headers
        };
        return this._http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.SET_ADVANCED_TESTING_APPOINTMENT}`, body, options);
    }
    // Verified -
    cancelAdvancedTestingAppointment(body) {
        const options = {
            headers: this._headers
        };
        return this._http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.CANCEL_ADVANCED_TESTING_APPOINTMENT}`, body, options);
    }
    // Verified
    // subscribeToPackage(body: {
    //   userUID: string;
    //   packageKey: string;
    //   packageStartDateStr: string;
    // }): Observable<any> {
    //   const options = {
    //     headers: this._headers
    //   };
    //   return this._http.post(
    //     `${environment.api.baseURL}${APIFunctions.SUBSCRIBE_PACKAGE}`,
    //     body,
    //     options
    //   );
    // }
    // Verified -
    sendWelcomeEmail(body) {
        const options = {
            headers: this._headers
        };
        return this._http
            .post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.EMAIL_WELCOME}`, body, options)
            .toPromise();
    }
    // Verified -
    sendHelloEmail(body) {
        const options = {
            headers: this._headers
        };
        return this._http
            .post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.EMAIL_REGISTERED}`, body, options)
            .toPromise();
    }
    // Verified -
    notifyAllAdmins(body) {
        const options = {
            headers: this._headers
        };
        return this._http
            .post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.api.baseURL}${src_app_common_constants__WEBPACK_IMPORTED_MODULE_1__.APIFunctions.NOTIFY_ADMINS}`, body, options)
            .toPromise();
    }
};
HttpService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient },
    { type: _utilities_service__WEBPACK_IMPORTED_MODULE_0__.UtilitiesService }
];
HttpService = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
        providedIn: 'root'
    })
], HttpService);



/***/ }),

/***/ 98138:
/*!****************************************!*\
  !*** ./src/app/core/services/index.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentService": () => (/* reexport safe */ _appointment_service__WEBPACK_IMPORTED_MODULE_8__.AppointmentService),
/* harmony export */   "AuthService": () => (/* reexport safe */ _auth_service__WEBPACK_IMPORTED_MODULE_0__.AuthService),
/* harmony export */   "FormService": () => (/* reexport safe */ _form_service__WEBPACK_IMPORTED_MODULE_4__.FormService),
/* harmony export */   "GeneralService": () => (/* reexport safe */ _general_service__WEBPACK_IMPORTED_MODULE_13__.GeneralService),
/* harmony export */   "GlobalDataService": () => (/* reexport safe */ _global_data_service__WEBPACK_IMPORTED_MODULE_14__.GlobalDataService),
/* harmony export */   "HttpService": () => (/* reexport safe */ _http_service__WEBPACK_IMPORTED_MODULE_7__.HttpService),
/* harmony export */   "PaymentService": () => (/* reexport safe */ _payment_service__WEBPACK_IMPORTED_MODULE_6__.PaymentService),
/* harmony export */   "ScriptService": () => (/* reexport safe */ _script_service__WEBPACK_IMPORTED_MODULE_12__.ScriptService),
/* harmony export */   "ScriptStore": () => (/* reexport safe */ _script_service__WEBPACK_IMPORTED_MODULE_12__.ScriptStore),
/* harmony export */   "SessionnService": () => (/* reexport safe */ _sessionn_service__WEBPACK_IMPORTED_MODULE_11__.SessionnService),
/* harmony export */   "SharedService": () => (/* reexport safe */ _shared_service__WEBPACK_IMPORTED_MODULE_9__.SharedService),
/* harmony export */   "SnackBarService": () => (/* reexport safe */ _snack_bar_service__WEBPACK_IMPORTED_MODULE_1__.SnackBarService),
/* harmony export */   "StaticDataService": () => (/* reexport safe */ _static_data_service__WEBPACK_IMPORTED_MODULE_5__.StaticDataService),
/* harmony export */   "StorageService": () => (/* reexport safe */ _storage_service__WEBPACK_IMPORTED_MODULE_10__.StorageService),
/* harmony export */   "UploadService": () => (/* reexport safe */ _upload_service__WEBPACK_IMPORTED_MODULE_15__.UploadService),
/* harmony export */   "UserService": () => (/* reexport safe */ _user_service__WEBPACK_IMPORTED_MODULE_2__.UserService),
/* harmony export */   "UtilitiesService": () => (/* reexport safe */ _utilities_service__WEBPACK_IMPORTED_MODULE_3__.UtilitiesService)
/* harmony export */ });
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./auth.service */ 90263);
/* harmony import */ var _snack_bar_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./snack-bar.service */ 28492);
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user.service */ 88386);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utilities.service */ 53623);
/* harmony import */ var _form_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form.service */ 56529);
/* harmony import */ var _static_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./static-data.service */ 36222);
/* harmony import */ var _payment_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment.service */ 86074);
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./http.service */ 72229);
/* harmony import */ var _appointment_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./appointment.service */ 19944);
/* harmony import */ var _shared_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./shared.service */ 37536);
/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./storage.service */ 22323);
/* harmony import */ var _sessionn_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./sessionn.service */ 35213);
/* harmony import */ var _script_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./script.service */ 54924);
/* harmony import */ var _general_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./general.service */ 96300);
/* harmony import */ var _global_data_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./global-data.service */ 74922);
/* harmony import */ var _upload_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./upload.service */ 90217);


















/***/ }),

/***/ 86074:
/*!**************************************************!*\
  !*** ./src/app/core/services/payment.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaymentService": () => (/* binding */ PaymentService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 58987);



let PaymentService = class PaymentService {
    constructor(_http) {
        this._http = _http;
    }
    createPayment(data) {
        // const headers = {
        //   headers: new HttpHeaders({
        //     accept: 'application/json',
        //     'Content-Type': 'application/x-www-form-urlencoded'
        //   })
        // };
        // return this._http.post(environment.payU.baseURL, data, headers).toPromise();
    }
};
PaymentService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient }
];
PaymentService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], PaymentService);



/***/ }),

/***/ 54924:
/*!*************************************************!*\
  !*** ./src/app/core/services/script.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ScriptService": () => (/* binding */ ScriptService),
/* harmony export */   "ScriptStore": () => (/* binding */ ScriptStore)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


const ScriptStore = [
    {
        name: 'payUBolt',
        src: 'https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js'
    }
];
let ScriptService = class ScriptService {
    constructor() {
        this.scripts = {};
        ScriptStore.forEach((script) => {
            this.scripts[script.name] = {
                loaded: false,
                src: script.src
            };
        });
    }
    loadScript(name) {
        return new Promise((resolve) => {
            if (this.scripts[name].loaded) {
                resolve({ script: name, loaded: true, status: 'Already Loaded' });
            }
            else {
                const script = document.createElement('script');
                script.type = 'text/javascript';
                script.src = this.scripts[name].src;
                if (script.readyState) {
                    // IE
                    script.onreadystatechange = () => {
                        if (script.readyState === 'loaded' ||
                            script.readyState === 'complete') {
                            script.onreadystatechange = null;
                            this.scripts[name].loaded = true;
                            resolve({ script: name, loaded: true, status: 'Loaded' });
                        }
                    };
                }
                else {
                    // Others
                    script.onload = () => {
                        this.scripts[name].loaded = true;
                        resolve({ script: name, loaded: true, status: 'Loaded' });
                    };
                }
                script.onerror = () => resolve({ script: name, loaded: false, status: 'Loaded' });
                document.getElementsByTagName('head')[0].appendChild(script);
            }
        });
    }
    load(...scripts) {
        const promises = [];
        scripts.forEach((script) => promises.push(this.loadScript(script)));
        return Promise.all(promises);
    }
    loadPayUBolt() {
        return new Promise((resolve) => {
            if (this.scripts.payUBolt.loaded) {
                resolve({ script: 'payUBolt', loaded: true, status: 'Already Loaded' });
            }
            else {
                const script = document.createElement('script');
                script.type = 'text/javascript';
                script.id = 'bolt';
                script.setAttribute('bolt-color', 'FF4081');
                script.setAttribute('bolt-logo', 'https://firebasestorage.googleapis.com/v0/b/stunning-prism-221707.appspot.com/o/assets%2Fmp-logo.png?alt=media&token=613e9e80-f283-4368-8394-5a578261cee0');
                script.src = this.scripts.payUBolt.src;
                script.onload = () => {
                    this.scripts.payUBolt.loaded = true;
                    resolve({ script: 'payUBolt', loaded: true, status: 'Loaded' });
                };
                script.onerror = () => resolve({ script: 'payUBolt', loaded: false, status: 'Loaded' });
                document.getElementsByTagName('head')[0].appendChild(script);
            }
        });
    }
};
ScriptService.ctorParameters = () => [];
ScriptService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root'
    })
], ScriptService);



/***/ }),

/***/ 35213:
/*!***************************************************!*\
  !*** ./src/app/core/services/sessionn.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SessionnService": () => (/* binding */ SessionnService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 80823);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 86942);
/* harmony import */ var src_app_common_models_session_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/models/session.model */ 84563);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utilities.service */ 53623);






let SessionnService = class SessionnService {
    constructor(_afStore, _uts) {
        this._afStore = _afStore;
        this._uts = _uts;
        // Verified -
        this._debounceMillis = 500;
    }
    // Verified -
    getSessionsByUser(uid, membershipKey) {
        return this._afStore
            .collection('sessions', (ref) => ref
            .where('user.uid', '==', uid)
            .where('membershipKey', '==', membershipKey)
            .where('isActive', '==', true)
            .where('recStatus', '==', true))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.debounceTime)(this._debounceMillis), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.map)((arr) => arr.sort((a, b) => a.sessionNumber < b.sessionNumber ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.map)((data) => src_app_common_models_session_model__WEBPACK_IMPORTED_MODULE_0__.Session.FromData(data, this._uts)));
    }
    getAllSessionsByUser(uid) {
        return this._afStore
            .collection('sessions', (ref) => ref
            .where('user.uid', '==', uid)
            .where('isActive', '==', true)
            .where('recStatus', '==', true))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.debounceTime)(this._debounceMillis), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.map)((arr) => arr.sort((a, b) => (a.updatedAt < b.updatedAt ? 1 : -1))), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.map)((data) => src_app_common_models_session_model__WEBPACK_IMPORTED_MODULE_0__.Session.FromData(data, this._uts)));
    }
};
SessionnService.ctorParameters = () => [
    { type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_4__.AngularFirestore },
    { type: _utilities_service__WEBPACK_IMPORTED_MODULE_1__.UtilitiesService }
];
SessionnService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], SessionnService);



/***/ }),

/***/ 37536:
/*!*************************************************!*\
  !*** ./src/app/core/services/shared.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedService": () => (/* binding */ SharedService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 84505);
/* harmony import */ var src_app_common_models_dialog_box_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/common/models/dialog-box.model */ 44870);




let SharedService = class SharedService {
    constructor() {
        this._dialogBox$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject(new src_app_common_models_dialog_box_model__WEBPACK_IMPORTED_MODULE_0__.DialogBox());
        this._progressTab$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject(1);
        this._healthHistoryTab$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject(0);
        this._homeSnackBar$ = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject(null);
        this.dialogBox$ = this._dialogBox$.asObservable();
    }
    getProgressTabSelected() {
        return this._progressTab$.asObservable();
    }
    getHealthHistoryTabSelected() {
        return this._healthHistoryTab$.asObservable();
    }
    getHomeSnackBar() {
        return this._homeSnackBar$.asObservable();
    }
    setDialogBox(data) {
        this._dialogBox$.next(data);
    }
    setProgressTabSelected(n = 0) {
        this._progressTab$.next(n);
    }
    setHealthHistoryTabSelected(n = 0) {
        this._healthHistoryTab$.next(n);
    }
    setHomeSnackBar(obj) {
        this._homeSnackBar$.next(obj);
    }
};
SharedService.ctorParameters = () => [];
SharedService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
    })
], SharedService);



/***/ }),

/***/ 28492:
/*!****************************************************!*\
  !*** ./src/app/core/services/snack-bar.service.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SnackBarService": () => (/* binding */ SnackBarService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material/snack-bar */ 10930);



let SnackBarService = class SnackBarService {
    constructor(_sb) {
        this._sb = _sb;
    }
    openSnackBar(message = '', panelClass = ['e-snackbar', 'e-snackbar--info'], action = 'X', duration = 3000, horizontalPosition = 'center', verticalPosition = 'bottom') {
        this._sb.open(message, action, {
            duration,
            panelClass,
            horizontalPosition,
            verticalPosition
        });
    }
    openSuccessSnackBar(message = '', duration = 4000) {
        this._sb.open(message, 'X', {
            duration,
            panelClass: ['e-snackbar', 'e-snackbar--success']
        });
    }
    openWarnSnackBar(message = '', duration = 4000) {
        this._sb.open(message, 'X', {
            duration,
            panelClass: ['e-snackbar', 'e-snackbar--warn']
        });
    }
    openErrorSnackBar(message = '', duration = 4000) {
        this._sb.open(message, 'X', {
            duration,
            panelClass: ['e-snackbar', 'e-snackbar--error']
        });
    }
    openInfoSnackBar(message = '', duration = 4000) {
        this._sb.open(message, 'X', {
            duration,
            panelClass: ['e-snackbar', 'e-snackbar--info']
        });
    }
};
SnackBarService.ctorParameters = () => [
    { type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_0__.MatSnackBar }
];
SnackBarService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], SnackBarService);



/***/ }),

/***/ 36222:
/*!******************************************************!*\
  !*** ./src/app/core/services/static-data.service.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StaticDataService": () => (/* binding */ StaticDataService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 86942);
/* harmony import */ var firebase_compat_app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase/compat/app */ 51181);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/firestore */ 26009);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utilities.service */ 53623);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/models */ 18073);








let StaticDataService = class StaticDataService {
    constructor(_afStore, _uts) {
        this._afStore = _afStore;
        this._uts = _uts;
    }
    // -----------------------------------------------------------------------------------------------------
    // Getters
    // -----------------------------------------------------------------------------------------------------
    // Verified -
    getPackages() {
        return this._afStore
            .collection('packages', (ref) => ref
            .where('isActive', '==', true)
            .where('recStatus', '==', true)
            .orderBy('sort'))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)((data) => src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.Package.FromData(data, this._uts)));
    }
    // // ToDo: Verify & Delete, use getMLocations instead
    // getLocations(): Observable<Locationn[]> {
    //   return this._afStore
    //     .collection('locations', (ref) =>
    //       ref
    //         .where('isActive', '==', true)
    //         .where('recStatus', '==', true)
    //         .orderBy('sort')
    //     )
    //     .valueChanges()
    //     .pipe(map((data) => Locationn.FromData(data, this._uts)));
    // }
    // Verified -
    getMLocations() {
        return this._afStore
            .collection('locations', (ref) => ref
            .where('isActive', '==', true)
            .where('recStatus', '==', true)
            .orderBy('sort'))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)((data) => src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.MLocationn.FromData(data, this._uts)));
    }
    // Verified -
    getAllAdvancedTests() {
        return this._afStore
            .collection('advancedTests', (ref) => ref.where('recStatus', '==', true).orderBy('sort'))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)((data) => src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.AdvancedTest.FromData(data, this._uts)));
    }
    // Verified -
    getCommunityFeed() {
        return this._afStore
            .collection('communityFeed', (ref) => ref.where('isActive', '==', true).where('recStatus', '==', true))
            .valueChanges()
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)((arr) => arr.sort((a, b) => (a.publishedAt < b.publishedAt ? 1 : -1))), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)((data) => src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.CommunityFeed.FromData(data, this._uts)));
    }
    // Verified
    getNewKey(collectionName, userSubColl = false, userUID = '') {
        if (userSubColl) {
            return this._afStore
                .collection('users')
                .doc(userUID)
                .collection(collectionName)
                .doc().ref.id;
        }
        return this._afStore.collection(collectionName).doc().ref.id;
    }
    // -----------------------------------------------------------------------------------------------------
    // Setters
    // -----------------------------------------------------------------------------------------------------
    // Verified
    setData(key, collectionName, data, isNew = false, dateFields = [], userSubColl = false, subKey = '') {
        const obj = {
            ...this._uts.toDB(data, dateFields),
            ...(isNew && {
                createdAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_0__["default"].firestore.FieldValue.serverTimestamp()
            }),
            updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_0__["default"].firestore.FieldValue.serverTimestamp()
        };
        if (userSubColl) {
            return this._afStore
                .collection('users')
                .doc(key)
                .collection(collectionName)
                .doc(subKey)
                .set(obj, { merge: true });
        }
        return this._afStore
            .collection(collectionName)
            .doc(key)
            .set(obj, { merge: true });
    }
};
StaticDataService.ctorParameters = () => [
    { type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_5__.AngularFirestore },
    { type: _utilities_service__WEBPACK_IMPORTED_MODULE_2__.UtilitiesService }
];
StaticDataService = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)({
        providedIn: 'root'
    })
], StaticDataService);



/***/ }),

/***/ 22323:
/*!**************************************************!*\
  !*** ./src/app/core/services/storage.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageService": () => (/* binding */ StorageService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/compat/storage */ 55574);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utilities.service */ 53623);
/* harmony import */ var capacitor_plugin_filedownload__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! capacitor-plugin-filedownload */ 83476);
/* harmony import */ var _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/filesystem */ 91662);







let StorageService = class StorageService {
  constructor(_aFStorage, _uts) {
    this._aFStorage = _aFStorage;
    this._uts = _uts;

    this.convertBlobToBase64 = blob => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;

      reader.onload = () => {
        resolve(reader.result);
      };

      reader.readAsDataURL(blob);
    });
  } // Verified -


  list(path) {
    return this._aFStorage.ref(path).listAll();
  }

  upload(file, dirPath, fileNamePrefix, userUID) {
    const fileType = file.type.split('/').slice(-1).toString();
    const fileName = fileNamePrefix ? `${fileNamePrefix}_${Math.floor(Date.now() / 100)}.${fileType}` : `MEALpyramid_${this._uts.dashifyFileName(file.name)}_${Math.floor(Date.now() / 100)}.${fileType}`;
    let filePath;

    if (userUID) {
      filePath = dirPath ? `${dirPath}/${userUID}/${fileName}` : `files/${userUID}/${fileName}`;
    } else {
      filePath = dirPath ? `${dirPath}/${fileName}` : `files/${fileName}`;
    }

    const ref = this._aFStorage.ref(filePath);

    const task = this._aFStorage.upload(filePath, file);

    return {
      fName: fileName,
      fPath: filePath,
      uploadRef: ref,
      uploadProgress$: task.percentageChanges(),
      snapshotChanges$: task.snapshotChanges()
    };
  }

  statApplicationDirectory() {
    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const info = yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Filesystem.stat({
        path: '/',
        directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Directory.External
      });
      console.log('Stat Info: ', info);
    })();
  } // Verified -


  download(url, name) {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      console.log(url, name, 'downloading.');
      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';

      xhr.onload = () => {
        const blob = xhr.response;
        const link = document.createElement('a');

        if (link.download !== undefined) {
          const url = URL.createObjectURL(blob);
          link.setAttribute('href', url);
          link.setAttribute('download', name);
          link.style.visibility = 'hidden';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      };

      xhr.open('GET', url);
      xhr.send();
      capacitor_plugin_filedownload__WEBPACK_IMPORTED_MODULE_2__.FileDownload.download({
        uri: url,
        fileName: name
      }).then(res => {
        console.log(res.path);
      }).catch(err => {
        console.log(err);
      }).then(savedFile => {
        console.log('File saved to:', savedFile);
      });

      try {
        _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Filesystem.writeFile({
          path: 'secrets/text.txt',
          data: "This is a test",
          directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Directory.Documents,
          encoding: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Encoding.UTF8
        });
      } catch (e) {
        console.error('Unable to write file', e);
      }

      const response = yield fetch(url); // convert to a Blob

      const blob = yield response.blob(); // convert to base64 data, which the Filesystem plugin requires

      const base64Data = yield _this.convertBlobToBase64(blob);

      const savedFile = /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
          yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Filesystem.writeFile({
            path: 'Download/images',
            data: base64Data,
            directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_3__.Directory.Data
          });
        });

        return function savedFile() {
          return _ref.apply(this, arguments);
        };
      }(); // helper function
      // const writeSecretFile = async () => {
      //   await Filesystem.writeFile({
      //       path: name,
      //       data: url,
      //       directory: Directory.External,
      //       encoding: Encoding.UTF8,
      //       recursive: true
      //   });
      //   };
      //   const readSecretFile = async () => {
      //   const contents = await Filesystem.readFile({
      //       path: name,
      //       directory: Directory.External,
      //       encoding: Encoding.UTF8,    
      //   });      };
      //   const readFilePath = async () => {
      //     const contents = await Filesystem.readFile({
      //       path: name
      //     });
      //     console.log('secrets:', JSON.stringify(contents));
      //   }
      //   readFilePath();
      //   writeSecretFile();
      //   readSecretFile();   
      //   let fileExtn=name.split('.').reverse()[0];
      //   let fileMIMEType=this.getMIMEtype(fileExtn);
      //            this.fileOpener.open("file:///storage/emulated/0/download/"+ name+"", fileMIMEType)
      //                   .then(() => console.log('File is opened'))
      //                   .catch(e => console.log('Error openening file', e));
      // }
      // getMIMEtype(extn){
      //   let ext=extn.toLowerCase();
      //   let MIMETypes={
      //     'txt' :'text/plain',
      //     'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      //     'doc' : 'application/msword',
      //     'pdf' : 'application/pdf',
      //     'jpg' : 'image/jpeg',
      //     'bmp' : 'image/bmp',
      //     'png' : 'image/png',
      //     'xls' : 'application/vnd.ms-excel',
      //     'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      //     'rtf' : 'application/rtf',
      //     'ppt' : 'application/vnd.ms-powerpoint',
      //     'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
      //   }
      //   return MIMETypes[ext];


      savedFile();
    })();
  } // Verified -


  delete(path) {
    return this._aFStorage.ref(path).delete().toPromise();
  }

};

StorageService.ctorParameters = () => [{
  type: _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_4__.AngularFireStorage
}, {
  type: _utilities_service__WEBPACK_IMPORTED_MODULE_1__.UtilitiesService
}];

StorageService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
  providedIn: 'root'
})], StorageService);


/***/ }),

/***/ 90217:
/*!*************************************************!*\
  !*** ./src/app/core/services/upload.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadService": () => (/* binding */ UploadService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/compat/storage */ 55574);
/* harmony import */ var _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/http/ngx */ 44719);
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/file/ngx */ 12358);







let UploadService = class UploadService {
  constructor(_aFStorage, http, file, Nativehttp) {
    this._aFStorage = _aFStorage;
    this.http = http;
    this.file = file;
    this.Nativehttp = Nativehttp;
  } // Verified -


  list(path) {
    return this._aFStorage.ref(path).listAll();
  }

  downloadFile(url, name) {
    this.bbname = name;
    this.Nativehttp.sendRequest(url, {
      method: "get",
      responseType: "arraybuffer"
    }).then(httpResponse => {
      console.log("File dowloaded successfully");
      this.downloadedFile = new Blob([httpResponse.data], {
        type: 'image/jpeg'
      });
    }).catch(err => {
      console.error(err);
    });
  }

  writeFile() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this.downloadedFile == null) return;
      var filename = _this.bbname;
      yield _this.createFile(filename);
      yield _this.writeToFile(filename);
    })();
  }

  createFile(filename) {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return _this2.file.createFile(_this2.file.externalRootDirectory, filename, false).catch(err => {
        console.error(err);
      });
    })();
  }

  writeToFile(filename) {
    return this.file.writeFile(this.file.externalRootDirectory, filename, this.downloadedFile, {
      replace: true,
      append: false
    }).then(createdFile => {
      console.log('File written successfully.');
      console.log(createdFile);
    }).catch(err => {
      console.error(err);
    });
  }

};

UploadService.ctorParameters = () => [{
  type: _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_3__.AngularFireStorage
}, {
  type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient
}, {
  type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_2__.File
}, {
  type: _ionic_native_http_ngx__WEBPACK_IMPORTED_MODULE_1__.HTTP
}];

UploadService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
  providedIn: 'root'
})], UploadService);


/***/ }),

/***/ 88386:
/*!***********************************************!*\
  !*** ./src/app/core/services/user.service.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserService": () => (/* binding */ UserService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/compat/firestore */ 92393);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 64139);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 80823);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 86942);
/* harmony import */ var firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! firebase/compat/app */ 51181);
/* harmony import */ var firebase_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! firebase/firestore */ 26009);
/* harmony import */ var _utilities_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./utilities.service */ 53623);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/constants */ 31896);











let UserService = class UserService {
  constructor(_afStore, _uts) {
    this._afStore = _afStore;
    this._uts = _uts; // Verified -

    this._debounceMillis = 500;
  } // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------
  // *
  // * Persons
  // *
  // Verified -


  getPersonByUID(uid) {
    return this._afStore.collection('users', ref => ref.where('uid', '==', uid).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.switchMap)(data => (0,rxjs__WEBPACK_IMPORTED_MODULE_7__.of)(src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.User.FromDBUser(data, this._uts))));
  } // Verified -


  getUserByUID(uid) {
    return this._afStore.collection('users', ref => ref.where('uid', '==', uid).where('isActive', '==', true).where('recStatus', '==', true).where('roles', 'array-contains-any', [src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.UserRoles.USER])).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.debounceTime)(this._debounceMillis), // switchMap((data) => of(User.FromDBUser(data, this._uts)))
    (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.User.FromDBUser(data, this._uts)));
  } // Verified -
  // ToDo: AngularFire toPromise() is not emitting, hence used firebase directly. Fix issue & use AngularFire .


  getUserByUIDP(uid) {
    return firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore().collection('users').where('uid', '==', uid).where('isActive', '==', true).where('recStatus', '==', true).where('roles', 'array-contains-any', [src_app_common_constants__WEBPACK_IMPORTED_MODULE_5__.UserRoles.USER]).get();
  } // *
  // * User Sub-collections
  // *
  // Verified -


  getActiveUserAppointments(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userAppointments', ref => ref.where('isCanceled', '==', false).where('appointment.isCompleted', '==', false).where('membershipKey', '==', membershipKey).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.debounceTime)(this._debounceMillis), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.sessionStart < b.sessionStart ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserAppointment.FromData(data, this._uts)));
  } // Verified & not used -


  getUserAppointments(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userAppointments', ref => ref.where('membershipKey', '==', membershipKey)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserAppointment.FromData(data, this._uts)));
  } // Verified -


  getActiveUserAppointmentsByType(uid, membershipKey, type) {
    return this._afStore.collection('users').doc(uid).collection('userAppointments', ref => ref.where('appointment.type', '==', type).where('isCanceled', '==', false).where('appointment.isCompleted', '==', false).where('membershipKey', '==', membershipKey).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserAppointment.FromData(data, this._uts)));
  } // Verified -


  getActiveUserAssessments(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userAssessments', ref => ref.where('isCompleted', '==', false).where('uid', '==', uid).where('membershipKey', '==', membershipKey).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserAssessment.FromData(data, this._uts)));
  } // Verified -


  getActiveUserAdvancedTests(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userAdvancedTests', ref => ref.where('isCompleted', '==', false).where('uid', '==', uid).where('membershipKey', '==', membershipKey).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserAdvancedTest.FromData(data, this._uts)));
  } // Verified -


  getUserHealthHistory(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userHealthHistory', ref => ref.where('uid', '==', uid).where('membershipKey', '==', membershipKey).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserHealthHistory.FromData(data, this._uts)));
  } // Verified -


  getUserAssessmentFiles(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userAssessmentFiles', ref => ref.where('uid', '==', uid) // .where('membershipKey', '==', membershipKey)
    .where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.uploadedAt < b.uploadedAt ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.Filee2.FromData(data, this._uts)));
  } // Verified -


  getUserAdvancedTestFiles(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userAdvancedTestFiles', ref => ref.where('uid', '==', uid) // .where('membershipKey', '==', membershipKey)
    .where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.uploadedAt < b.uploadedAt ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.Filee2.FromData(data, this._uts)));
  } // Verified -
  // getUserFoodLogs(uid: string): Observable<any> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userProgressDaily', (ref) => ref.orderBy('date', 'desc'))
  //     .valueChanges();
  // }


  getUserFoodLogs(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userFoodLogs', ref => ref.where('uid', '==', uid) // .where('membershipKey', '==', membershipKey)
    .where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.date < b.date ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserFoodLog.FromData(data, this._uts)));
  } // Verified


  getUserFoodLog(uid, key) {
    return this._afStore.collection('users').doc(uid).collection('userProgressDaily').doc(key).valueChanges();
  } // Verified


  getUserRecallLogs(uid) {
    return this._afStore.collection('users').doc(uid).collection('userProgressRecall', ref => ref.orderBy('date', 'desc')).valueChanges();
  } // Verified


  getUserRecallLog(uid, key) {
    return this._afStore.collection('users').doc(uid).collection('userProgressRecall').doc(key).valueChanges();
  } // Verified -
  // getUserWeightLogs(uid: string): Observable<UserWeightLog[]> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userWeightLogs', (ref) => ref.where('uid', '==', uid))
  //     .valueChanges()
  //     .pipe(map((data) => UserWeightLog.FromData(data, this._uts)));
  // }


  getUserWeightLogs(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userWeightLogs', ref => ref.where('uid', '==', uid).where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.date < b.date ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserWeightLog.FromData(data, this._uts)));
  } // Verified -


  getUserWeightLogLatest(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userWeightLogs', ref => ref.where('uid', '==', uid).where('membershipKey', '==', membershipKey).where('isActive', '==', true).where('recStatus', '==', true).orderBy('date', 'desc').limit(1)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserWeightLog.FromData(data, this._uts)));
  } // Verified
  // getUserMeasurementLogs(uid: string): Observable<UserMeasurementLog[]> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userMeasurementLogs', (ref) => ref.where('uid', '==', uid))
  //     .valueChanges()
  //     .pipe(map((data) => UserMeasurementLog.FromData(data, this._uts)));
  // }
  // Verified -


  getUserMeasurementLogs(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userMeasurementLogs', ref => ref.where('uid', '==', uid).where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.date < b.date ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserMeasurementLog.FromData(data, this._uts)));
  } // Verified -


  getUserMeasurementLogLatest(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userMeasurementLogs', ref => ref.where('uid', '==', uid).where('membershipKey', '==', membershipKey).where('isActive', '==', true).where('recStatus', '==', true).orderBy('date', 'desc').limit(1)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserMeasurementLog.FromData(data, this._uts)));
  } // Verified -


  getUserFeedbackLogs(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userFeedbackLogs', ref => ref.where('uid', '==', uid).where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.date < b.date ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserFeedbackLog.FromData(data, this._uts)));
  } // Verified
  // getUserPhotoLogs(uid: string): Observable<any> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userPhotoLogs', (ref) => ref.orderBy('date', 'desc'))
  //     .valueChanges();
  // }
  // Verified
  // getUserPhotoLog(uid: string, key: string): Observable<any> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userPhotoLogs')
  //     .doc(key)
  //     .valueChanges();
  // }
  // Verified -


  getUserPhotoLogs(uid, // membershipKey: string
  allMembershipKeys = []) {
    return this._afStore.collection('users').doc(uid).collection('userPhotoLogs', ref => ref.where('uid', '==', uid).where('membershipKey', 'in', allMembershipKeys).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.date < b.date ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserPhotoLog.FromData(data, this._uts)));
  } // Verified -


  getUserGeneralRecommendations(uid, membershipKey) {
    return this._afStore.collection('users').doc(uid).collection('userGeneralRecommendations', ref => ref.where('uid', '==', uid).where('membershipKey', '==', membershipKey).where('isActive', '==', true).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserGeneralRecommendation.FromData(data, this._uts)));
  } // Verified -


  getUserNotifications(uid) {
    return this._afStore.collection('users').doc(uid).collection('userNotifications', ref => ref.where('isRead', '==', false).where('uid', '==', uid).where('recStatus', '==', true)).valueChanges().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(arr => arr.sort((a, b) => a.notifDate < b.notifDate ? 1 : -1)), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.map)(data => src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.Notification.FromData(data, this._uts)));
  } // -----------------------------------------------------------------------------------------------------
  // Setters
  // -----------------------------------------------------------------------------------------------------
  // Verified


  setUserFromAuth(data) {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const user = src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.User.FromAuth(data, _this._uts);
      const obj = { ..._this._uts.toDB(user),
        createdAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp(),
        updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
      };

      try {
        yield _this._afStore.collection('users').doc(user.uid).set(obj, {
          merge: true
        });
        return user;
      } catch (error) {
        throw new Error('CouldNotSetUser');
      }
      /* ***** Reference ***** */
      // const [err] = await this._uts.promiseHandler(
      //   this._afStore.collection('users').doc(user.uid).set(obj, { merge: true }),
      //   'ERROR_SET_USER'
      // );
      // if (err) {
      //   throw err;
      // }

      /* ********** */

    })();
  } // Verified


  setUser(uid, user) {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const obj = { ..._this2._uts.toDB(user, ['orientationAppt.sessionStart', 'orientationAppt.sessionEnd', 'subscriptionDuration.start', 'subscriptionDuration.end', 'weightProgress.startDate', 'weightProgress.targetDate']),
        updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
      };

      try {
        yield _this2._afStore.collection('users').doc(uid).set(obj, {
          merge: true
        });
        return user;
      } catch (error) {
        throw new Error('CouldNotSetUser');
      }
    })();
  } // ToDo: Verify & Delete


  setNewUserNotification(uid, key, data) {
    var _this3 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const obj = { ..._this3._uts.toDB(data, ['notifDate']),
        createdAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp(),
        updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
      };
      obj.notifDate = firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp();
      return _this3._afStore.collection('users').doc(uid).collection('userNotifications').doc(key).set(obj, {
        merge: true
      });
    })();
  } // ToDo: Verify & Delete


  setUserAdvancedTest(test, isNew) {
    var _this4 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const obj = { ..._this4._uts.toDB(test),
        ...(isNew && {
          key: _this4._afStore.collection('userAdvancedTests').doc().ref.id
        }),
        ...(isNew && {
          createdAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
        }),
        updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
      };
      const [err] = yield _this4._uts.promiseHandler(_this4._afStore.collection('users').doc(test.uid).collection('userAdvancedTests').doc(obj.key).set(obj, {
        merge: true
      }), 'ERROR_SET_USER');

      if (err) {
        throw err;
      }

      return true;
    })();
  } // ToDo: Verify & Delete


  setUserAssessmentFile(data, isNew) {
    var _this5 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const obj = { ..._this5._uts.toDB(data, ['uploadedAt']),
        ...(isNew && {
          key: _this5._afStore.collection('userAssessmentFiles').doc().ref.id
        }),
        ...(isNew && {
          createdAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
        }),
        updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
      };
      const [err] = yield _this5._uts.promiseHandler(_this5._afStore.collection('users').doc(obj.user.uid).collection('userAssessmentFiles').doc(obj.key).set(obj, {
        merge: true
      }), 'ERROR_SET_USER');

      if (err) {
        throw err;
      }

      return true;
    })();
  } // Verified


  setUserWeightLog(data) {
    const obj = { ...data,
      updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
    };
    return this._afStore.collection('users').doc(data.uid).collection('userWeightLogs').doc(data.uid).set(obj, {
      merge: true
    });
  } // Verified


  setUserMeasurementLog(data) {
    const obj = { ...data,
      updatedAt: firebase_compat_app__WEBPACK_IMPORTED_MODULE_1__["default"].firestore.FieldValue.serverTimestamp()
    };
    return this._afStore.collection('users').doc(data.uid).collection('userMeasurementLogs').doc(data.uid).set(obj, {
      merge: true
    });
  }

};

UserService.ctorParameters = () => [{
  type: _angular_fire_compat_firestore__WEBPACK_IMPORTED_MODULE_10__.AngularFirestore
}, {
  type: _utilities_service__WEBPACK_IMPORTED_MODULE_3__.UtilitiesService
}];

UserService = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Injectable)({
  providedIn: 'root'
})], UserService);


/***/ }),

/***/ 53623:
/*!****************************************************!*\
  !*** ./src/app/core/services/utilities.service.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UtilitiesService": () => (/* binding */ UtilitiesService)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var lodash_isequal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash.isequal */ 57883);
/* harmony import */ var lodash_isequal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash_isequal__WEBPACK_IMPORTED_MODULE_1__);




let UtilitiesService = class UtilitiesService {
  constructor(utilitiesService) {
    if (utilitiesService) {
      throw new Error('UtilitiesService is already loaded!');
    }
  } // Verified -
  // // ToDo: Replace target[key] === null condition


  deepAssignTruthyFirestore(target, source) {
    for (const key of Object.keys(target)) {
      if (typeof source[key] !== 'object' && typeof target[key] !== 'object') {
        if (source[key]) {
          target[key] = source[key];
        }
      } else if (Array.isArray(source[key]) && source[key].length > 0) {
        target[key] = source[key];
      } else if (source[key] && Object.keys(source[key]).includes('seconds')) {
        target[key] = new Date(source[key].seconds * 1000);
      } else if ((typeof source[key] === 'boolean' || typeof source[key] === 'number') && target[key] === null) {
        target[key] = source[key];
      } else if (source[key]) {
        this.deepAssignTruthyFirestore(target[key], source[key]);
      }
    }

    return target;
  } // Verified -


  sanitize(str) {
    return str.trim().replace(/[^A-Za-z0-9]+/g, '');
  } // Verified -
  // ToDo: Delete


  promiseHandler(promise, improved) {
    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      return promise.then(data => [null, data || true]).catch(err => {
        if (improved) {
          Object.assign(err, {
            initial: err.message
          });
          Object.assign(err, {
            message: improved
          });
        }

        return [err];
      });
    })();
  } // Verified -


  dashify(str) {
    return str.trim().replace(/[^A-Za-z0-9]+/g, '-').toLowerCase();
  } // Verified -


  dashifyFileName(str) {
    return str.trim().split('.').slice(0, -1).toString().replace(/[^A-Za-z0-9]+/g, '-');
  } // Verified -


  isValidDate(value) {
    return Boolean(value instanceof Date && value.getTime());
  } // Verified -
  // * Works only for first level date-fields in a map.


  toDB(data, dateFields) {
    const obj = JSON.parse(JSON.stringify(data));

    if (dateFields && dateFields.length) {
      dateFields.map(item => {
        const a = item.split('.');

        if (Object.prototype.hasOwnProperty.call(data, a[0])) {
          obj[a[0]] = data[a[0]];
        }
      });
    }

    return obj;
  } // Verified -


  shallowEqual(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (const key of keys1) {
      if (object1[key] !== object2[key]) {
        return false;
      }
    }

    return true;
  } // Verified -


  deepEqual(object1, object2) {
    return lodash_isequal__WEBPACK_IMPORTED_MODULE_1__(object1, object2);
  } // Verified -


  getSearchArray(str1, str2) {
    const arr = [];
    let previousString1 = '';
    let previousString2 = '';
    str1.split('').map(item => {
      previousString1 += item.toLowerCase();
      arr.push(previousString1);
    });
    previousString1 += ' ';
    str2.split('').map(item => {
      previousString1 += item.toLowerCase();
      arr.push(previousString1);
      previousString2 += item.toLowerCase();
      arr.push(previousString2);
    });
    return arr;
  } // Verified -


  sleep(milliSecs) {
    return new Promise(resolve => setTimeout(resolve, milliSecs));
  } // Verified -


  computeDifference(val1, val2) {
    let res = null;

    if (val1 && val2 && !Number.isNaN(val1) && !Number.isNaN(val2)) {
      const diff = val1 - val2;
      const m = Number((Math.abs(diff) * 100).toPrecision(15));
      const roundedDiff = Math.round(m) / 100 * Math.sign(diff);
      res = roundedDiff > 0 ? `+${roundedDiff}` : roundedDiff.toString();
    }

    return res;
  }

};

UtilitiesService.ctorParameters = () => [{
  type: UtilitiesService,
  decorators: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Optional
  }, {
    type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.SkipSelf
  }]
}];

UtilitiesService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
  providedIn: 'root'
})], UtilitiesService);


/***/ }),

/***/ 36619:
/*!*********************************************!*\
  !*** ./src/app/home/feed/feed.component.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FeedComponent": () => (/* binding */ FeedComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _feed_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./feed.component.html?ngResource */ 91056);
/* harmony import */ var _feed_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./feed.component.scss?ngResource */ 54076);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-lightbox */ 25015);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);










let FeedComponent = class FeedComponent {
    constructor(_as, _sds, _lightbox, _lightboxConfig) {
        this._as = _as;
        this._sds = _sds;
        this._lightbox = _lightbox;
        this._lightboxConfig = _lightboxConfig;
        this.areFeedsAvailable = false;
        this.lightboxAlbum = [];
        this._me$ = this._as.getMe();
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__.Subject();
    }
    ngOnInit() {
        this.now = moment__WEBPACK_IMPORTED_MODULE_2__();
        this._me$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)((me) => {
            this.me = me;
            return this._sds.getCommunityFeed();
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$))
            .subscribe((data) => {
            this.feeds = data;
            if (this.feeds.length > 0) {
                this.areFeedsAvailable = true;
            }
        });
    }
    ngOnDestroy() {
        this._notifier$.next();
        this._notifier$.complete();
    }
    lightboxInit() {
        Object.assign(this._lightboxConfig, src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.LightboxConfigs);
    }
    openLightbox(src) {
        this.lightboxAlbum = [];
        this.lightboxAlbum.push({
            src
        });
        this._lightbox.open(this.lightboxAlbum, 0);
    }
};
FeedComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.StaticDataService },
    { type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_3__.Lightbox },
    { type: ngx_lightbox__WEBPACK_IMPORTED_MODULE_3__.LightboxConfig }
];
FeedComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-feed',
        template: _feed_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_feed_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FeedComponent);



/***/ }),

/***/ 52003:
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeRoutingModule": () => (/* binding */ HomeRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.component */ 45067);




const routes = [
    {
        path: '',
        component: _home_component__WEBPACK_IMPORTED_MODULE_0__.HomeComponent,
    }
];
let HomeRoutingModule = class HomeRoutingModule {
};
HomeRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], HomeRoutingModule);



/***/ }),

/***/ 45067:
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeComponent": () => (/* binding */ HomeComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _home_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.component.html?ngResource */ 64715);
/* harmony import */ var _home_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.component.scss?ngResource */ 11832);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ 19193);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! rxjs/operators */ 83910);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ 92340);
/* harmony import */ var _common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/constants */ 31896);
/* harmony import */ var _common_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/models */ 18073);
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../core/services */ 98138);
/* harmony import */ var _shared_buy_advanced_testing__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/buy-advanced-testing */ 55942);
/* harmony import */ var _shared_dialog_box__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/dialog-box */ 55599);
/* harmony import */ var _shared_get_started__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/get-started */ 14526);

















let HomeComponent = class HomeComponent {
  constructor(_as, _dialog, _shs, _sb, _hs, _router, _ss, _us, _uts, menuCtrl, alertController) {
    this._as = _as;
    this._dialog = _dialog;
    this._shs = _shs;
    this._sb = _sb;
    this._hs = _hs;
    this._router = _router;
    this._ss = _ss;
    this._us = _us;
    this._uts = _uts;
    this.menuCtrl = menuCtrl;
    this.alertController = alertController;
    this.advancedRecommendedPackage = '';
    this.dispatchView = false;
    this.hasIntroVideoEnded = false;
    this.hasWelcomeVideoEnded = false;
    this.heroImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.heroSection.kinita;
    this.introVideoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.video.intro;
    this.showAdvancedTesting = false;
    this.showClinicalAssessmentUpload = false;
    this.showDidNotAttendOrientation = false;
    this.showFirstDiet = false;
    this.showHasActiveAdvancedTestingAppt = false;
    this.showHasPurchasedAdvancedTests = false;
    this.showHasRecommendedAdvancedTests = false;
    this.showHasSubscribed = false;
    this.showLastDiet = false;
    this.showNewSubscriber = false;
    this.showNextAppointmentAvailable = false;
    this.showOnboardingIncomplete = false;
    this.showRecommendations = false;
    this.showScheduleNextAppointment = false;
    this.showVideo = true;
    this.showWaitingOnATReports = false;
    this.showWaitingOnOrientation = false;
    this.showWaitingOnRecommendations = false;
    this.userAppointments = []; // videoJSConfigObj = {
    //   preload: 'metadata',
    //   controls: false,
    //   autoplay: 'any',
    //   overrideNative: true,
    //   techOrder: ['html5', 'flash'],
    //   html5: {
    //     nativeVideoTracks: false,
    //     nativeAudioTracks: false,
    //     nativeTextTracks: false,
    //     hls: {
    //       withCredentials: false,
    //       overrideNative: true,
    //       debug: true
    //     }
    //   }
    // };

    this.welcomeVideoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.video.welcome;
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_10__.Subject();
    this.menuCtrl.swipeGesture(true);
  }

  ngOnInit() {
    var _this = this;

    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.switchMap)(me => {
      this.me = me;
      this.userHasSubcribedPackage = this.me.flags.isPackageSubscribed;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;
      this.dispatchView = true;
      console.log(this.me);
      return (0,rxjs__WEBPACK_IMPORTED_MODULE_12__.combineLatest)([this._us.getActiveUserAppointments(this.me.uid, this.me.membershipKey), this._ss.getSessionsByUser(this.me.uid, this.me.membershipKey)]);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_13__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_14__.catchError)(() => {
      this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_15__.EMPTY;
    })).subscribe( /*#__PURE__*/function () {
      var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (arr) {
        _this.setAllViewFlagsFalse();

        yield _this._uts.sleep(500);
        const [appointments, sessions] = arr;
        _this.userAppointments = appointments;

        if (_this.me.flags.isPackageSubscribed) {
          _this.showHasSubscribed = true;
          const recallAppts = appointments.filter(i => i.appointment.type === _common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.RECALL).sort((a, b) => {
            return a.appointment.sessionStart > b.appointment.sessionStart ? 1 : -1;
          });
          const [currentSess] = sessions.filter(i => i.isCurrent);
          const [nextSession] = currentSess.nextSessionKey ? sessions.filter(i => i.key === currentSess.nextSessionKey) : [];
          const [lastSession] = sessions.filter(i => i.sessionNumber === sessions.length - 1);

          if (recallAppts.length === 0) {
            _this.showNewSubscriber = currentSess.sessionNumber === 0;

            if (nextSession) {
              _this.showScheduleNextAppointment = true;
              _this.nextAppointmentDueDate = nextSession.dueDate;
            }
          } else {
            _this.showNextAppointmentAvailable = true;
            _this.nextAppointment = recallAppts[0].appointment;
          }

          if (currentSess.sessionNumber === 1 && currentSess.fileURL) {
            _this.showFirstDiet = true;
          } else if (currentSess.sessionNumber === lastSession.sessionNumber) {
            _this.showLastDiet = true;
          }

          _this.dispatchView = true;

          _this.checkForSnackBar();
        } else if (_this.me.flags.isPackageRecommended) {
          _this.showRecommendations = true;
          _this.dispatchView = true;

          _this.checkForSnackBar();
        } else if (_this.me.flags.isOrientationCompleted) {
          _this.showWaitingOnRecommendations = true;
          _this.dispatchView = true;

          _this.checkForSnackBar();
        } else if (_this.me.flags.isOrientationScheduled) {
          _this.showWaitingOnOrientation = true;
          const appts = appointments.filter(i => i.appointment.type === _common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.ORIENTATION);

          if (appts.length !== 1) {
            _this._router.navigate([_common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.ERROR]);

            return;
          }

          [_this.orientationAppointment] = appts;
          _this.showDidNotAttendOrientation = Boolean(_this.orientationAppointment.appointment.sessionStart < new Date());
          _this.dispatchView = true;

          _this.checkForSnackBar();
        } else {
          _this.showOnboardingIncomplete = true;
          _this.dispatchView = true;

          _this.checkForSnackBar();
        }

        _this._us.getActiveUserAssessments(_this.me.uid, _this.me.membershipKey).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_13__.takeUntil)(_this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_14__.catchError)(() => {
          _this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

          _this.dispatchView = true;
          return rxjs__WEBPACK_IMPORTED_MODULE_15__.EMPTY;
        })).subscribe(res => {
          if (res.length) {
            if (res.length !== 1) {
              _this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

              _this.dispatchView = true;
              return;
            }

            _this.showClinicalAssessmentUpload = Boolean(res[0].tests && res[0].tests.length);
          } else {
            _this.showClinicalAssessmentUpload = false;
          }
        });

        _this._us.getActiveUserAdvancedTests(_this.me.uid, _this.me.membershipKey).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_13__.takeUntil)(_this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_14__.catchError)(() => {
          _this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

          return rxjs__WEBPACK_IMPORTED_MODULE_15__.EMPTY;
        })).subscribe(res => {
          if (res.length === 0) {
            _this.showAdvancedTesting = false;
          } else if (res.length === 1) {
            const [userAdvancedTest] = res;
            _this.showAdvancedTesting = Boolean(userAdvancedTest.recommendedTests.length > 0) || Boolean(userAdvancedTest.purchasedTests.length > 0);

            if (!_this.showAdvancedTesting) {
              return;
            }

            if (_this.me.buyAdvancedTestingStep === 0) {
              _this.advancedRecommendedPackage = userAdvancedTest.recommendedPackage.name;
              _this.showHasRecommendedAdvancedTests = true;
            } else if (_this.me.buyAdvancedTestingStep === 1) {
              _this.showHasPurchasedAdvancedTests = true;
            } else if (_this.me.buyAdvancedTestingStep === 2) {
              [_this.advancedTestingAppt] = _this.userAppointments.filter(appt => appt.appointment.type === _common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.ADVANCED_TESTING);
              _this.showHasActiveAdvancedTestingAppt = Boolean(_this.advancedTestingAppt);
              _this.showWaitingOnATReports = !_this.advancedTestingAppt;
            }
          }
        });
      });

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());
  }

  showDialog(comp, step = 0) {
    this._dialog.open(comp, {
      width: '60vw',
      height: '90vh',
      data: {
        step
      }
    });
  }

  onIntroVideoEnd() {
    this.hasIntroVideoEnded = true;
  }

  onWelcomeVideoEnd() {
    this.hasWelcomeVideoEnded = true;
  }

  onCancelOrientationClick() {
    var _this2 = this;

    const dialogBox = new _common_models__WEBPACK_IMPORTED_MODULE_5__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel your Orientation appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(_shared_dialog_box__WEBPACK_IMPORTED_MODULE_8__.DialogBoxComponent);

    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref2 = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          _this2.dispatchView = false;

          _this2._us.getActiveUserAppointmentsByType(_this2.me.uid, _this2.me.membershipKey, _common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.ORIENTATION).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_16__.take)(1), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.switchMap)(appts => {
            if (appts.length !== 1) {
              throw new Error();
            }

            const [appt] = appts;
            const body = {
              appointmentKey: appt.appointment.key,
              userUID: _this2.me.uid
            };
            return _this2._hs.cancelOrientationAppointment(body);
          }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_14__.catchError)(() => {
            _this2._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

            _this2.dispatchView = true;
            reject();
            return rxjs__WEBPACK_IMPORTED_MODULE_15__.EMPTY;
          })).subscribe(res => {
            if (!res.status) {
              _this2._sb.openErrorSnackBar(res.error.description || _common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

              _this2.dispatchView = true;
              reject();
              return;
            }

            _this2._sb.openSuccessSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.APPT_CANCELED);

            _this2.dispatchView = true;
            resolve(true);
          });
        });

        return function (_x2) {
          return _ref2.apply(this, arguments);
        };
      }());
    });
  }

  onCancelATApptClick() {
    var _this3 = this;

    const dialogBox = new _common_models__WEBPACK_IMPORTED_MODULE_5__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel your Advanced Testing appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(_shared_dialog_box__WEBPACK_IMPORTED_MODULE_8__.DialogBoxComponent);

    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref3 = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          _this3.dispatchView = false;

          _this3._us.getActiveUserAppointmentsByType(_this3.me.uid, _this3.me.membershipKey, _common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.ADVANCED_TESTING).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_16__.take)(1), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.switchMap)(appts => {
            if (appts.length !== 1) {
              throw new Error();
            }

            const [appt] = appts;
            const body = {
              appointmentKey: appt.appointment.key,
              userUID: _this3.me.uid
            };
            return _this3._hs.cancelAdvancedTestingAppointment(body);
          }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_14__.catchError)(() => {
            _this3._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

            _this3.dispatchView = true;
            reject();
            return rxjs__WEBPACK_IMPORTED_MODULE_15__.EMPTY;
          })).subscribe(res => {
            if (!res.status) {
              _this3._sb.openErrorSnackBar(res.error.description || _common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

              _this3.dispatchView = true;
              reject();
              return;
            }

            _this3._sb.openSuccessSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.APPT_CANCELED);

            _this3.dispatchView = true;
            resolve(true);
          });
        });

        return function (_x3) {
          return _ref3.apply(this, arguments);
        };
      }());
    });
  }

  setAllViewFlagsFalse() {
    this.showDidNotAttendOrientation = false;
    this.showOnboardingIncomplete = false;
    this.showRecommendations = false;
    this.showVideo = false;
    this.showWaitingOnOrientation = false;
    this.showWaitingOnRecommendations = false;
    this.showAdvancedTesting = false;
    this.showHasSubscribed = false;
    this.showNewSubscriber = false;
    this.showFirstDiet = false;
    this.showLastDiet = false;
    this.showNextAppointmentAvailable = false;
    this.showScheduleNextAppointment = false;
    this.showHasRecommendedAdvancedTests = false;
    this.showHasPurchasedAdvancedTests = false;
    this.showHasActiveAdvancedTestingAppt = false;
    this.showWaitingOnATReports = false;
    delete this.nextAppointment;
    delete this.nextAppointmentDueDate;
    delete this.advancedTestingAppt;
    this.advancedRecommendedPackage = '';
  }

  goto(path) {
    this.path = path;

    this._router.navigate([path]);
  }

  onResumeOnboardingClick() {
    this.showDialog(_shared_get_started__WEBPACK_IMPORTED_MODULE_9__.GetStartedComponent, this.me.onboardingStep);
  }

  onBuyAdvancedTestingClick() {
    this.showDialog(_shared_buy_advanced_testing__WEBPACK_IMPORTED_MODULE_7__.BuyAdvancedTestingComponent, this.me.buyAdvancedTestingStep);
  }

  onUploadClick() {
    this._shs.setHealthHistoryTabSelected(1);

    this._router.navigate([_common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.HEALTH_HISTORY]);
  }

  onGoToProgress(n) {
    this._shs.setProgressTabSelected(n);

    switch (n) {
      case 1:
        this._router.navigate(['/tab/' + _common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.PROGRESS]);

        break;

      case 2:
        this._router.navigate(['/tab/' + _common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.PROGRESS]);

        break;

      case 3:
        this._router.navigate(['/tab/' + _common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.PROGRESS]);

        break;

      case 4:
        this._router.navigate([_common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.APPOINTMENTS]);

        break;

      default:
        break;
    }
  }

  checkForSnackBar() {
    var _this4 = this;

    this._shs.getHomeSnackBar().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_16__.take)(1)).subscribe( /*#__PURE__*/function () {
      var _ref4 = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (obj) {
        if (obj && obj.type && obj.message) {
          switch (obj.type) {
            case 'Success':
              _this4._sb.openSuccessSnackBar(obj.message);

              break;

            case 'Error':
              _this4._sb.openErrorSnackBar(obj.message);

              break;

            default:
              break;
          }

          _this4._shs.setHomeSnackBar(null);
        }
      });

      return function (_x4) {
        return _ref4.apply(this, arguments);
      };
    }());
  }

};

HomeComponent.ctorParameters = () => [{
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_17__.MatDialog
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.SharedService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.SnackBarService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.HttpService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_18__.Router
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.SessionnService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.UserService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_6__.UtilitiesService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_19__.MenuController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_19__.AlertController
}];

HomeComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_20__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_21__.Component)({
  selector: 'app-home',
  template: _home_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_home_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], HomeComponent);


/***/ }),

/***/ 3467:
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeModule": () => (/* binding */ HomeModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _shared_shared_components_footer_footer_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../shared/shared-components/footer/footer.component */ 40301);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-routing.module */ 52003);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.component */ 45067);
/* harmony import */ var _feed_feed_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./feed/feed.component */ 36619);
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/cdk/layout */ 83278);
/* harmony import */ var _shared_buy_advanced_testing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/buy-advanced-testing */ 55942);
/* harmony import */ var _shared_get_started__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/get-started */ 14526);
/* harmony import */ var _shared_material_design__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/material-design */ 12497);
/* harmony import */ var _shared_shared_components__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/shared-components */ 35886);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-lightbox */ 25015);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 93819);















let HomeModule = class HomeModule {
};
HomeModule = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.NgModule)({
        declarations: [
            _home_component__WEBPACK_IMPORTED_MODULE_2__.HomeComponent,
            _feed_feed_component__WEBPACK_IMPORTED_MODULE_3__.FeedComponent,
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.IonicModule,
            _home_routing_module__WEBPACK_IMPORTED_MODULE_1__.HomeRoutingModule,
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_13__.LayoutModule,
            _shared_get_started__WEBPACK_IMPORTED_MODULE_5__.GetStartedModule,
            _shared_material_design__WEBPACK_IMPORTED_MODULE_6__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__.FlexLayoutModule,
            _shared_buy_advanced_testing__WEBPACK_IMPORTED_MODULE_4__.BuyAdvancedTestingModule,
            _shared_shared_components__WEBPACK_IMPORTED_MODULE_7__.SharedComponentsModule,
            ngx_lightbox__WEBPACK_IMPORTED_MODULE_8__.LightboxModule
        ],
        exports: [
            _shared_shared_components_footer_footer_component__WEBPACK_IMPORTED_MODULE_0__.FooterComponent,
            _home_component__WEBPACK_IMPORTED_MODULE_2__.HomeComponent
        ]
    })
], HomeModule);



/***/ }),

/***/ 73456:
/*!*******************************!*\
  !*** ./src/app/home/index.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeModule": () => (/* reexport safe */ _home_module__WEBPACK_IMPORTED_MODULE_0__.HomeModule)
/* harmony export */ });
/* harmony import */ var _home_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.module */ 3467);



/***/ }),

/***/ 47336:
/*!**************************************************************!*\
  !*** ./src/app/pages/notification/notification.component.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NotificationComponent": () => (/* binding */ NotificationComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _notification_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./notification.component.html?ngResource */ 15046);
/* harmony import */ var _notification_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notification.component.scss?ngResource */ 89125);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 66587);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);










let NotificationComponent = class NotificationComponent {
  constructor(_as, _us, _sb, _sts, router) {
    this._as = _as;
    this._us = _us;
    this._sb = _sb;
    this._sts = _sts;
    this.router = router;
    this.areNotificationsAvailable = false;
    this.dispatchView = false;
    this.notifications = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.switchMap)(me => {
      this.me = me;
      return this._us.getUserNotifications(this.me.uid);
    })).subscribe(notifs => {
      this.notifications = notifs;

      if (this.notifications.length > 0) {
        this.areNotificationsAvailable = true;
      } else {
        this.areNotificationsAvailable = false;
      }

      this.dispatchView = true;
      return (0,rxjs__WEBPACK_IMPORTED_MODULE_8__.throwError)(Error);
    });
  }

  onClick(notif) {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      notif.isRead = true;
      notif.isShown = true;
      notif.isNew = false;

      try {
        yield _this._sts.setData(_this.me.uid, 'userNotifications', notif, false, ['notifDate'], true, notif.key);
      } catch (error) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
      }
    })();
  }

};

NotificationComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.UserService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.StaticDataService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router
}];

NotificationComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
  selector: 'app-notification',
  template: _notification_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_notification_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], NotificationComponent);


/***/ }),

/***/ 39730:
/*!***********************************************!*\
  !*** ./src/app/pages/pages-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoutingModule": () => (/* binding */ PagesRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages.component */ 37664);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./notification/notification.component */ 47336);





const routes = [{
        path: '',
        component: _pages_component__WEBPACK_IMPORTED_MODULE_0__.PagesComponent,
        children: [
            { path: 'notification', component: _notification_notification_component__WEBPACK_IMPORTED_MODULE_1__.NotificationComponent },
        ]
    }];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule]
    })
], PagesRoutingModule);



/***/ }),

/***/ 37664:
/*!******************************************!*\
  !*** ./src/app/pages/pages.component.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesComponent": () => (/* binding */ PagesComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _pages_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages.component.html?ngResource */ 80599);
/* harmony import */ var _pages_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages.component.scss?ngResource */ 37490);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);




let PagesComponent = class PagesComponent {
    constructor() { }
    ngOnInit() { }
};
PagesComponent.ctorParameters = () => [];
PagesComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-pages',
        template: _pages_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_pages_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], PagesComponent);



/***/ }),

/***/ 18950:
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesModule": () => (/* binding */ PagesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var src_app_home__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/home */ 73456);
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages.component */ 37664);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pages-routing.module */ 39730);
/* harmony import */ var _notification_notification_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notification/notification.component */ 47336);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var ngx_lightbox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-lightbox */ 25015);
/* harmony import */ var _shared_buy_advanced_testing__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/buy-advanced-testing */ 55942);
/* harmony import */ var _shared_get_started__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/get-started */ 14526);
/* harmony import */ var _shared_material_design__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/material-design */ 12497);
/* harmony import */ var _shared_shared_components__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/shared-components */ 35886);
/* harmony import */ var _shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/layout/layout.module */ 68634);















let PagesModule = class PagesModule {
};
PagesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.NgModule)({
        declarations: [_pages_component__WEBPACK_IMPORTED_MODULE_1__.PagesComponent, _notification_notification_component__WEBPACK_IMPORTED_MODULE_3__.NotificationComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule,
            _pages_routing_module__WEBPACK_IMPORTED_MODULE_2__.PagesRoutingModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.IonicModule,
            _shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_9__.LayoutModule,
            src_app_home__WEBPACK_IMPORTED_MODULE_0__.HomeModule,
            _shared_get_started__WEBPACK_IMPORTED_MODULE_6__.GetStartedModule,
            _shared_material_design__WEBPACK_IMPORTED_MODULE_7__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__.FlexLayoutModule,
            _shared_buy_advanced_testing__WEBPACK_IMPORTED_MODULE_5__.BuyAdvancedTestingModule,
            _shared_shared_components__WEBPACK_IMPORTED_MODULE_8__.SharedComponentsModule,
            ngx_lightbox__WEBPACK_IMPORTED_MODULE_4__.LightboxModule
        ]
    })
], PagesModule);



/***/ }),

/***/ 50681:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/appointment/appointment.component.ts ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentComponent": () => (/* binding */ AppointmentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appointment.component.html?ngResource */ 40703);
/* harmony import */ var _appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appointment.component.scss?ngResource */ 73887);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 19193);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var _dialog_box__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialog-box */ 55599);















let AppointmentComponent = class AppointmentComponent {
  constructor(_sds, _as, _fb, _hs, _sb, _dialog, _shs, _router) {
    this._sds = _sds;
    this._as = _as;
    this._fb = _fb;
    this._hs = _hs;
    this._sb = _sb;
    this._dialog = _dialog;
    this._shs = _shs;
    this._router = _router;
    this.areSlotsAvailable = false;
    this.bookingAppt = false;
    this.dispatchView = false;
    this.isSearchPristine = true;
    this.loadingSlots = false;
    this.locations = [];
    this.slots = [];
    this.closeDialog = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this._locations$ = this._sds.getMLocations();
    this._h$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_10__.combineLatest)([this._me$, this._locations$]);
  }

  ngOnInit() {
    this._h$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.takeUntil)(this._notifier$)).subscribe(data => {
      [this.me, this.locations] = data;
      this.locations = this.locations.filter(i => i.name === src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.Locations.JUHU);
      this.createSearchFG();
      this.startDate = moment__WEBPACK_IMPORTED_MODULE_3__().startOf('day').toDate();
      this.endDate = moment__WEBPACK_IMPORTED_MODULE_3__().add(365, 'day').startOf('day').toDate();
      this.dispatchView = true;
    });
  }

  createSearchFG() {
    this.searchFG = this._fb.group({
      date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_12__.Validators.required],
      location: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_12__.Validators.required]
    });
  }

  get locationFC() {
    return this.searchFG.get('location');
  }

  get dateFC() {
    return this.searchFG.get('date');
  }

  isSearchFGValid() {
    return this.searchFG.valid;
  }

  resetFactorySearch() {
    this.searchFG.reset();
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
    this.isSearchPristine = true;
  }

  resetLightSearch() {
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
  }

  lockSearch() {
    this.loadingSlots = true;
    this.searchFG.disable();
  }

  unlockSearch() {
    this.loadingSlots = false;
    this.searchFG.enable();
  }

  lockBook() {
    this.bookingAppt = true;
    this.searchFG.disable();
  }

  unlockBook() {
    this.bookingAppt = false;
    this.searchFG.enable();
  }

  onSearch() {
    this.lockSearch();
    this.resetLightSearch();
    this.isSearchPristine = false;

    this._hs.getAvailableAdvancedTestingSlots(this.me.uid, moment__WEBPACK_IMPORTED_MODULE_3__(this.dateFC.value).format('YYYY-MM-DD'), this.locationFC.value.key).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_13__.catchError)(err => {
      this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

      this.resetFactorySearch();
      this.unlockSearch();
      return rxjs__WEBPACK_IMPORTED_MODULE_14__.EMPTY;
    })).subscribe(data => {
      this.slots = data;

      if (this.slots.length > 0) {
        this.areSlotsAvailable = true;
      }

      this.unlockSearch();
    });
  }

  onSlotClicked(appt) {
    appt.clicked = true;
    this.slots.map(item => {
      if (item.key !== appt.key) {
        item.clicked = false;
      }
    });
    this.selectedSlot = appt;
  }

  onBookAppointment() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.lockBook();

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_5__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to book this appointment?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this._shs.setDialogBox(dialogBox);

      const dialogRef = _this._dialog.open(_dialog_box__WEBPACK_IMPORTED_MODULE_7__.DialogBoxComponent);

      return new Promise(resolve => {
        dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
          var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
            if (!res) {
              resolve(false);

              _this.unlockBook();

              return;
            }

            const body = {
              appointmentKey: _this.selectedSlot.key,
              appointmentMergeSlotKey: _this.selectedSlot.mergeSlotKey,
              userUID: _this.me.uid
            };

            _this._hs.setAdvancedTestingAppointment(body).subscribe(apiRes => {
              if (!apiRes.status) {
                _this._sb.openErrorSnackBar(apiRes.error.description);

                resolve(false);

                _this.unlockBook();

                return;
              }

              _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.APPT_BOOKED);

              _this.resetFactorySearch();

              _this.unlockBook();

              resolve(true);

              _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.HOME]);

              _this.closeSelf();
            });
          });

          return function (_x) {
            return _ref.apply(this, arguments);
          };
        }());
      });
    })();
  }

  closeSelf() {
    this.closeDialog.emit();
  }

};

AppointmentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.StaticDataService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.AuthService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SnackBarService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SharedService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_16__.Router
}];

AppointmentComponent.propDecorators = {
  closeDialog: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output
  }]
};
AppointmentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_17__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-appointment',
  template: _appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AppointmentComponent);


/***/ }),

/***/ 58506:
/*!*******************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/buy-advanced-testing.component.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BuyAdvancedTestingComponent": () => (/* binding */ BuyAdvancedTestingComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _buy_advanced_testing_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./buy-advanced-testing.component.html?ngResource */ 88682);
/* harmony import */ var _buy_advanced_testing_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./buy-advanced-testing.component.scss?ngResource */ 96659);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ 31484);





let BuyAdvancedTestingComponent = class BuyAdvancedTestingComponent {
    constructor(_dialog, data) {
        this._dialog = _dialog;
        this.data = data;
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.stepper.steps.forEach((item, i) => {
            item.completed = false;
            if (i < this.data.step) {
                item.completed = true;
            }
        });
    }
    goToNextStep() {
        this.stepper.selected.completed = true;
        this.stepper.next();
    }
    closeDialog() {
        this._dialog.close();
    }
};
BuyAdvancedTestingComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__.MatDialogRef },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Inject, args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__.MAT_DIALOG_DATA,] }] }
];
BuyAdvancedTestingComponent.propDecorators = {
    stepper: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['stepper',] }]
};
BuyAdvancedTestingComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-buy-advanced-testing',
        template: _buy_advanced_testing_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_buy_advanced_testing_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], BuyAdvancedTestingComponent);



/***/ }),

/***/ 24073:
/*!****************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/buy-advanced-testing.module.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BuyAdvancedTestingModule": () => (/* binding */ BuyAdvancedTestingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/stepper */ 44193);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _buy_advanced_testing_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./buy-advanced-testing.component */ 58506);
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payment/payment.component */ 56655);
/* harmony import */ var _appointment_appointment_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appointment/appointment.component */ 50681);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _dialog_box__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../dialog-box */ 55599);
/* harmony import */ var _material_design__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../material-design */ 12497);











let BuyAdvancedTestingModule = class BuyAdvancedTestingModule {
};
BuyAdvancedTestingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        declarations: [
            _buy_advanced_testing_component__WEBPACK_IMPORTED_MODULE_0__.BuyAdvancedTestingComponent,
            _payment_payment_component__WEBPACK_IMPORTED_MODULE_1__.PaymentComponent,
            _appointment_appointment_component__WEBPACK_IMPORTED_MODULE_2__.AppointmentComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_7__.CommonModule,
            _material_design__WEBPACK_IMPORTED_MODULE_4__.MaterialDesignModule,
            _angular_material_stepper__WEBPACK_IMPORTED_MODULE_8__.MatStepperModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_9__.FlexLayoutModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.ReactiveFormsModule,
            _dialog_box__WEBPACK_IMPORTED_MODULE_3__.DialogBoxModule
        ],
        exports: [_buy_advanced_testing_component__WEBPACK_IMPORTED_MODULE_0__.BuyAdvancedTestingComponent]
    })
], BuyAdvancedTestingModule);



/***/ }),

/***/ 55942:
/*!******************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/index.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BuyAdvancedTestingComponent": () => (/* reexport safe */ _buy_advanced_testing_component__WEBPACK_IMPORTED_MODULE_1__.BuyAdvancedTestingComponent),
/* harmony export */   "BuyAdvancedTestingModule": () => (/* reexport safe */ _buy_advanced_testing_module__WEBPACK_IMPORTED_MODULE_0__.BuyAdvancedTestingModule)
/* harmony export */ });
/* harmony import */ var _buy_advanced_testing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./buy-advanced-testing.module */ 24073);
/* harmony import */ var _buy_advanced_testing_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./buy-advanced-testing.component */ 58506);




/***/ }),

/***/ 56655:
/*!**************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/payment/payment.component.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaymentComponent": () => (/* binding */ PaymentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _payment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payment.component.html?ngResource */ 4211);
/* harmony import */ var _payment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payment.component.scss?ngResource */ 17211);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/environments/environment */ 92340);













let PaymentComponent = class PaymentComponent {
  constructor(_as, _sds, _fb, _us, _hs, _sb, _router, _shs, _uts) {
    this._as = _as;
    this._sds = _sds;
    this._fb = _fb;
    this._us = _us;
    this._hs = _hs;
    this._sb = _sb;
    this._router = _router;
    this._shs = _shs;
    this._uts = _uts;
    this.advancedTestPackagesFC = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormControl();
    this.advancedTestPackages = src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.MATPackage.FromData(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.AdvancedTestPackages, this._uts);
    this.advancedTestsFA = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormArray([]);
    this.allAdvancedTests = [];
    this.cost = 0;
    this.costWithTax = 0;
    this.dispatchView = false;
    this.initialAdvancedTests = [];
    this.isNew = false;
    this.loading = false;
    this.closeDialog = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
    this.goToNextStep = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.switchMap)(me => {
      this.me = me;
      this.requiresGSTFC = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormControl(this.me.flags.requiresGSTInvoice);
      this.gstFG = this._fb.group({
        number: [this.me.personalDetails.gst.number, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/^[A-Z0-9]{15}$/)],
        name: [this.me.personalDetails.gst.name, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.pattern(/^[a-zA-Z. ]{2,51}$/)],
        address: [this.me.personalDetails.gst.address, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.minLength(5)]
      });
      return this._sds.getAllAdvancedTests();
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.switchMap)(tests => {
      this.allAdvancedTests = tests;
      return this._us.getActiveUserAdvancedTests(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_12__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_13__.EMPTY)).subscribe(userAdvancedTests => {
      this.cost = 0;
      this.costWithTax = 0;

      if (userAdvancedTests.length === 0) {
        this.currentUserAdvancedTest = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserAdvancedTest();
        this.isNew = true;
      } else if (userAdvancedTests.length === 1) {
        [this.currentUserAdvancedTest] = userAdvancedTests;
        this.isNew = false;
      }

      this.setInitialAdvancedTests();
      this.setInitialAdvancedTestPackage();
      this.setAdvancedTestsFA(this.currentUserAdvancedTest.recommendedTests); // if (userAdvancedTests.length) {
      //   if (userAdvancedTests.length !== 1) {
      //     throw new Error();
      //   }
      //   [this.currentUserAdvancedTest] = userAdvancedTests;
      //   this.currentUserAdvancedTest.recommendedTests.map((item) => {
      //     this.addAdvancedTest(item);
      //   });
      // } else {
      //   this.currentUserAdvancedTest = new UserAdvancedTest();
      //   this.currentUserAdvancedTest.uid = this.me.uid;
      //   this.currentUserAdvancedTest.membershipKey = this.me.membershipKey;
      // }

      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  } // *
  // * GST
  // *


  get gstNumberFC() {
    return this.gstFG.get('number');
  }

  get gstNameFC() {
    return this.gstFG.get('name');
  }

  get gstAddressFC() {
    return this.gstFG.get('address');
  } // *
  // * Initial
  // *


  setInitialAdvancedTests() {
    this.initialAdvancedTests.length = 0;

    if (this.currentUserAdvancedTest.recommendedTests.length > 0) {
      this.initialAdvancedTests = this.currentUserAdvancedTest.recommendedTests;
    }
  }

  setInitialAdvancedTestPackage() {
    const [pkg] = this.advancedTestPackages.filter(pkg => pkg.key === this.currentUserAdvancedTest.recommendedPackage.key);

    if (pkg) {
      this.advancedTestPackageInitial = pkg;
      this.advancedTestPackagesFC.setValue(pkg);
    } else {
      const newPkg = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.MATPackage();
      this.advancedTestPackageInitial = newPkg;
      this.advancedTestPackagesFC.setValue(newPkg);
    }
  } // *
  // * Individual Advanced Tests Form Array
  // *


  addAdvancedTest(t) {
    this.advancedTestsFA.push(this._fb.group(t));
    this.cost += t.price.amount;
    this.costWithTax += t.price.amountWithTax;
  }

  removeAdvancedTest(t) {
    let i = 0;
    let remove;
    this.advancedTestsFA.controls.map(test => {
      if (test.value.name === t.name) {
        remove = i;
      }

      i++;
    });
    this.advancedTestsFA.removeAt(remove);
    this.cost -= t.price.amount;
    this.costWithTax -= t.price.amountWithTax;
  }

  onCheckChange(e) {
    if (e.checked) {
      this.addAdvancedTest(e.source.value);
    } else {
      this.removeAdvancedTest(e.source.value);
    }

    const testKeys = this.advancedTestsFA.value.map(test => test.key);
    this.advancedTestPackagesFC.reset();
    this.advancedTestPackages.map(pkg => {
      if (pkg.testKeys.sort().join() === testKeys.sort().join()) {
        this.advancedTestPackagesFC.setValue(pkg);
      }
    });
  }

  setAdvancedTestsFA(arr = []) {
    this.advancedTestsFA.clear();

    if (arr.length > 0) {
      arr.map(item => {
        this.addAdvancedTest(item);
      });
    }
  } // *
  // * Advanced Tests Packages
  // *


  onPackageCheck(p) {
    const tests = [];
    p.testKeys.map(k => {
      const t = this.allAdvancedTests.filter(i => i.key === k)[0];
      tests.push(t);
    });
    this.cost = 0;
    this.costWithTax = 0;
    this.setAdvancedTestsFA(tests); // this.advancedTestsFA.reset();
    // this.allAdvancedTests.map((t) => this.removeAdvancedTest(t));
    // tests.map((t) => {
    //   this.addAdvancedTest(t);
    // });
  }

  onPackageCheckChange(e) {
    this.onPackageCheck(e.value);
  } // *
  // * Misc
  // *


  isChecked(t) {
    let flag = false;
    this.advancedTestsFA.controls.map(test => {
      if (test.value.name === t.name) {
        flag = true;
      }
    });
    return flag;
  }

  resetPackages() {
    this.advancedTestPackagesFC.setValue(false);
    this.cost = 0;
    this.costWithTax = 0;
    this.setAdvancedTestsFA(this.initialAdvancedTests);
    this.advancedTestPackagesFC.reset(this.advancedTestPackageInitial); // this.advancedTestsFA.reset();
    // this.allAdvancedTests.map((t) => this.removeAdvancedTest(t));
    // this.cost = 0;
    // this.costWithTax = 0;
    // this.advancedTestPackagesFC.setValue(false);
    // this.currentUserAdvancedTest.recommendedTests.map((item) => {
    //   this.addAdvancedTest(item);
    // });
  } // *
  // * Payment
  // *


  onPayment() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.cost) {
        return;
      }

      try {
        _this.loading = true;

        if (_this.isNew) {
          _this.currentUserAdvancedTest.key = _this._sds.getNewKey('userAdvancedTests', true, _this.me.uid);
          _this.currentUserAdvancedTest.uid = _this.me.uid;
          _this.currentUserAdvancedTest.membershipKey = _this.me.membershipKey;
          _this.currentUserAdvancedTest.isActive = true;
          _this.currentUserAdvancedTest.recStatus = true;
        }

        _this.currentUserAdvancedTest.chosenTests = _this.advancedTestsFA.value;
        _this.currentUserAdvancedTest.chosenPackage = _this.advancedTestPackagesFC.value || new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.MATPackage();
        yield _this._sds.setData(_this.me.uid, 'userAdvancedTests', _this.currentUserAdvancedTest, _this.isNew, [], true, _this.currentUserAdvancedTest.key); // if (
        //   Boolean(this.currentUserAdvancedTest.recommendedTests.length > 0) !==
        //   this.me.flags.hasAdvancedTests
        // ) {
        //   await this._sds.setData(this.me.uid, 'users', {
        //     flags: {
        //       hasAdvancedTests: Boolean(
        //         this.currentUserAdvancedTest.recommendedTests.length > 0
        //       )
        //     }
        //   });
        // }
      } catch (error) {
        _this._sb.openErrorSnackBar(error.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
      } // if (!this.currentUserAdvancedTest.key) {
      //   this.currentUserAdvancedTest.key = this._sds.getNewKey(
      //     'userAdvancedTests',
      //     true
      //   );
      // }
      // this.currentUserAdvancedTest.recommendedTests =
      //   this.advancedTestsFA.value;
      // await this._sds.setData(
      //   this.me.uid,
      //   'userAdvancedTests',
      //   this.currentUserAdvancedTest,
      //   false,
      //   [],
      //   true,
      //   this.currentUserAdvancedTest.key
      // );
      // await this._us.setUser(this.me.uid, {
      //   flags: {
      //     requiresGSTInvoice: this.needGSTFC.value
      //   },
      //   personalDetails: {
      //     gst: {
      //       number: this.gstNumberFC.value
      //     }
      //   }
      // });


      const paymentObj = {
        userUID: _this.me.uid,
        product: src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ProductTypes.ADVANCED_TESTING,
        requiresGST: _this.requiresGSTFC.value,
        gstNumber: _this.gstNumberFC.value,
        gstName: _this.gstNameFC.value,
        gstAddress: _this.gstAddressFC.value
      };

      _this._hs.createPayment(paymentObj).subscribe(res => {
        if (!res.status) {
          _this._sb.openErrorSnackBar(res.error.description || 'Sorry, the payment could not be initiated. Please try again.');

          _this.loading = false;
          return;
        }

        const {
          paymentKey
        } = res.data;
        const paises = res.data.order.amount;
        const razorPayOptions = {
          key: src_environments_environment__WEBPACK_IMPORTED_MODULE_6__.environment.payment.razorPay.key,
          amount: paises,
          currency: src_environments_environment__WEBPACK_IMPORTED_MODULE_6__.environment.payment.currency,
          name: _this.me.name,
          description: src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ProductTypes.ADVANCED_TESTING,
          order_id: res.data.order.id,
          handler: response => {
            const obj = {
              userUID: _this.me.uid,
              paymentKey,
              product: src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ProductTypes.ADVANCED_TESTING,
              pgResponse: response
            };

            _this._hs.setPayment(obj).subscribe( /*#__PURE__*/function () {
              var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res1) {
                if (!res1.status) {
                  _this._shs.setHomeSnackBar({
                    type: 'Error',
                    message: res1.error.description || 'Thank you for the payment. Please contact our Support team for the next steps.'
                  });
                } else {
                  _this._shs.setHomeSnackBar({
                    type: 'Success',
                    message: 'Thank you for the payment.'
                  });
                }

                _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.HOME]);
              });

              return function (_x) {
                return _ref.apply(this, arguments);
              };
            }());
          }
        };
        const rzpInstance = new Razorpay(razorPayOptions);
        rzpInstance.open();
        rzpInstance.on('payment.failed', response => {// console.log('[error] ', response);
        });
        _this.loading = false;

        _this.closeSelf();
      }, () => {
        _this._sb.openErrorSnackBar('Sorry, the payment could not be initiated. Please try again.');

        _this.loading = false;
      });
    })();
  }

  closeSelf() {
    this.closeDialog.emit();
  }

};

PaymentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.StaticDataService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_14__.Router
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}];

PaymentComponent.propDecorators = {
  closeDialog: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output
  }],
  goToNextStep: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output
  }]
};
PaymentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_15__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-payment',
  template: _payment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_payment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], PaymentComponent);
 // *
// * PayU
// *
// const payUObj = {
//   txnid: res.data.paymentKey,
//   hash: res.data.hash,
//   surl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   furl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   productinfo: ProductTypes.ADVANCED_TESTING,
//   amount: this.costWithTax.toString(),
//   phone: this.me.personalDetails.mobile,
//   email: this.me.personalDetails.email,
//   firstname: this.me.personalDetails.firstName,
//   key: environment.payU.merchantKey
// };
// bolt.launch(payUObj, {
//   responseHandler: (obj) => {
//     if (obj.response.txnStatus !== 'CANCEL') {
//       this._hs
//         .setPayment(obj.response)
//         .subscribe(async (res1: APIResponse) => {
//           if (!res1.status) {
//             this._sb.openErrorSnackBar(
//               res1.error.description || ErrorMessages.SYSTEM,
//               4000
//             );
//           }
//           const obj = {
//             buyAdvancedTestingStep: 1,
//             flags: {
//               requiresGSTInvoice: this.needGSTFC.value
//             },
//             personalDetails: {
//               gstNumber: this.gstNumberFC.value
//             }
//           };
//           await this._us.setUser(this.me.uid, obj);
//           if (
//             this.currentUserAdvancedTest &&
//             this.currentUserAdvancedTest.uid
//           ) {
//             this.currentUserAdvancedTest.purchasedTests =
//               this.advancedTestsFA.value;
//             this.currentUserAdvancedTest.paymentKey =
//               this.paymentKey;
//             this.currentUserAdvancedTest.isLocked = true;
//             await this._us.setUserAdvancedTest(
//               this.currentUserAdvancedTest,
//               false
//             );
//           } else {
//             const test = new UserAdvancedTest();
//             test.purchasedTests = this.advancedTestsFA.value;
//             test.uid = this.me.uid;
//             test.paymentKey = this.paymentKey;
//             test.isLocked = true;
//             await this._us.setUserAdvancedTest(test, true);
//           }
//           // this.goToNextStep.emit();
//           this._router.navigate([RouteIDs.HOME]);
//         });
//     }
//   },
//   catchException: (err) => {
//     this._sb.openErrorSnackBar(
//       err.message || ErrorMessages.SYSTEM,
//       4000
//     );
//   }
// });
// this.loading = false;
// this.closeSelf();

/***/ }),

/***/ 85980:
/*!***********************************************************!*\
  !*** ./src/app/shared/dialog-box/dialog-box.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DialogBoxComponent": () => (/* binding */ DialogBoxComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _dialog_box_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dialog-box.component.html?ngResource */ 59292);
/* harmony import */ var _dialog_box_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dialog-box.component.scss?ngResource */ 31310);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/services */ 98138);







let DialogBoxComponent = class DialogBoxComponent {
    constructor(_ss) {
        this._ss = _ss;
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
    }
    ngOnInit() {
        this._ss.dialogBox$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.takeUntil)(this._notifier$))
            .subscribe((data) => {
            this.dialog = data;
        });
    }
    ngOnDestroy() {
        this.dialog.reset();
        this._ss.setDialogBox(this.dialog);
        this._notifier$.next(true);
        this._notifier$.complete();
    }
};
DialogBoxComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_2__.SharedService }
];
DialogBoxComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-dialog-box',
        template: _dialog_box_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_dialog_box_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], DialogBoxComponent);



/***/ }),

/***/ 6679:
/*!********************************************************!*\
  !*** ./src/app/shared/dialog-box/dialog-box.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DialogBoxModule": () => (/* binding */ DialogBoxModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/divider */ 71528);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _dialog_box_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dialog-box.component */ 85980);









let DialogBoxModule = class DialogBoxModule {
};
DialogBoxModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_dialog_box_component__WEBPACK_IMPORTED_MODULE_0__.DialogBoxComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__.MatDialogModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_5__.MatButtonModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__.MatIconModule,
            _angular_material_divider__WEBPACK_IMPORTED_MODULE_7__.MatDividerModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_8__.FlexLayoutModule
        ],
        exports: [_dialog_box_component__WEBPACK_IMPORTED_MODULE_0__.DialogBoxComponent]
    })
], DialogBoxModule);



/***/ }),

/***/ 55599:
/*!********************************************!*\
  !*** ./src/app/shared/dialog-box/index.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DialogBoxComponent": () => (/* reexport safe */ _dialog_box_component__WEBPACK_IMPORTED_MODULE_1__.DialogBoxComponent),
/* harmony export */   "DialogBoxModule": () => (/* reexport safe */ _dialog_box_module__WEBPACK_IMPORTED_MODULE_0__.DialogBoxModule)
/* harmony export */ });
/* harmony import */ var _dialog_box_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dialog-box.module */ 6679);
/* harmony import */ var _dialog_box_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dialog-box.component */ 85980);




/***/ }),

/***/ 18293:
/*!*************************************************************************!*\
  !*** ./src/app/shared/get-started/appointment/appointment.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentComponent": () => (/* binding */ AppointmentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appointment.component.html?ngResource */ 39368);
/* harmony import */ var _appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appointment.component.scss?ngResource */ 99562);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 19193);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var _dialog_box__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../dialog-box */ 55599);















let AppointmentComponent = class AppointmentComponent {
  constructor(_sds, _as, _fb, _hs, _sb, _dialog, _shs, _router) {
    this._sds = _sds;
    this._as = _as;
    this._fb = _fb;
    this._hs = _hs;
    this._sb = _sb;
    this._dialog = _dialog;
    this._shs = _shs;
    this._router = _router;
    this.areSlotsAvailable = false;
    this.bookingAppt = false;
    this.dispatchView = false;
    this.isSearchPristine = true;
    this.loadingSlots = false;
    this.locations = [];
    this.slots = [];
    this.closeDialog = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_9__.Subject();
    this._locations$ = this._sds.getMLocations();
    this._h$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_10__.combineLatest)([this._me$, this._locations$]);
  }

  ngOnInit() {
    this._h$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.takeUntil)(this._notifier$)).subscribe(data => {
      [this.me, this.locations] = data;
      this.createSearchFG();
      this.startDate = moment__WEBPACK_IMPORTED_MODULE_3__().add(3, 'day').startOf('day').toDate();
      this.endDate = moment__WEBPACK_IMPORTED_MODULE_3__().add(365, 'day').startOf('day').toDate();
      this.dispatchView = true;
    });
  }

  createSearchFG() {
    this.searchFG = this._fb.group({
      date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_12__.Validators.required],
      location: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_12__.Validators.required]
    });
  }

  get locationFC() {
    return this.searchFG.get('location');
  }

  get dateFC() {
    return this.searchFG.get('date');
  }

  isSearchFGValid() {
    return this.searchFG.valid;
  }

  resetFactorySearch() {
    this.searchFG.reset();
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
    this.isSearchPristine = true;
  }

  resetLightSearch() {
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
  }

  lockSearch() {
    this.loadingSlots = true;
    this.searchFG.disable();
  }

  unlockSearch() {
    this.loadingSlots = false;
    this.searchFG.enable();
  }

  lockBook() {
    this.bookingAppt = true;
    this.searchFG.disable();
  }

  unlockBook() {
    this.bookingAppt = false;
    this.searchFG.enable();
  }

  onSearch() {
    this.lockSearch();
    this.resetLightSearch();
    this.isSearchPristine = false;

    this._hs.getAvailableOrientationSlots(this.me.uid, moment__WEBPACK_IMPORTED_MODULE_3__(this.dateFC.value).format('YYYY-MM-DD'), this.locationFC.value.key).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_13__.catchError)(err => {
      this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM_TRY_AGAIN);

      this.resetFactorySearch();
      this.unlockSearch();
      return rxjs__WEBPACK_IMPORTED_MODULE_14__.EMPTY;
    })).subscribe(data => {
      this.slots = data;

      if (this.slots.length > 0) {
        this.areSlotsAvailable = true;
      }

      this.unlockSearch();
    });
  }

  onSlotClicked(appt) {
    appt.clicked = true;
    this.slots.map(item => {
      if (item.key !== appt.key) {
        item.clicked = false;
      }
    });
    this.selectedSlot = appt;
  }

  onBookAppointment() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.lockBook();

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_5__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to book this appointment?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this._shs.setDialogBox(dialogBox);

      const dialogRef = _this._dialog.open(_dialog_box__WEBPACK_IMPORTED_MODULE_7__.DialogBoxComponent);

      return new Promise(resolve => {
        dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
          var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
            if (!res) {
              resolve(false);

              _this.unlockBook();

              return;
            }

            const body = {
              appointmentKey: _this.selectedSlot.key,
              appointmentMergeSlotKey: _this.selectedSlot.mergeSlotKey,
              userUID: _this.me.uid
            };

            _this._hs.setOrientationAppointment(body).subscribe(apiRes => {
              if (!apiRes.status) {
                _this._sb.openErrorSnackBar(apiRes.error.description);

                resolve(false);

                _this.unlockBook();

                return;
              }

              _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.APPT_BOOKED);

              _this.resetFactorySearch();

              _this.unlockBook();

              resolve(true);

              _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.HOME]);

              _this.closeSelf();
            });
          });

          return function (_x) {
            return _ref.apply(this, arguments);
          };
        }());
      });
    })();
  }

  closeSelf() {
    this.closeDialog.emit();
  }

};

AppointmentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.StaticDataService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.AuthService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SnackBarService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_15__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SharedService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_16__.Router
}];

AppointmentComponent.propDecorators = {
  closeDialog: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output
  }]
};
AppointmentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_17__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
  selector: 'app-appointment',
  template: _appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AppointmentComponent);


/***/ }),

/***/ 3729:
/*!*************************************************************!*\
  !*** ./src/app/shared/get-started/get-started.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GetStartedComponent": () => (/* binding */ GetStartedComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _get_started_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./get-started.component.html?ngResource */ 46519);
/* harmony import */ var _get_started_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./get-started.component.scss?ngResource */ 16213);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ 31484);





let GetStartedComponent = class GetStartedComponent {
    constructor(_dialog, data) {
        this._dialog = _dialog;
        this.data = data;
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.stepper.steps.forEach((item, i) => {
            item.completed = false;
            if (i < this.data.step) {
                item.completed = true;
            }
        });
    }
    goToNextStep() {
        this.stepper.selected.completed = true;
        this.stepper.next();
    }
    closeDialog() {
        this._dialog.close();
    }
};
GetStartedComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__.MatDialogRef },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Inject, args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__.MAT_DIALOG_DATA,] }] }
];
GetStartedComponent.propDecorators = {
    stepper: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['stepper',] }]
};
GetStartedComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-get-started',
        template: _get_started_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_get_started_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], GetStartedComponent);



/***/ }),

/***/ 14617:
/*!**********************************************************!*\
  !*** ./src/app/shared/get-started/get-started.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GetStartedModule": () => (/* binding */ GetStartedModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/stepper */ 44193);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _get_started_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./get-started.component */ 3729);
/* harmony import */ var _personal_details_personal_details_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./personal-details/personal-details.component */ 16286);
/* harmony import */ var _health_history_health_history_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./health-history/health-history.component */ 28341);
/* harmony import */ var _payment_payment_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payment/payment.component */ 31909);
/* harmony import */ var _appointment_appointment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./appointment/appointment.component */ 18293);
/* harmony import */ var _input_file_preview__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../input-file-preview */ 60510);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../dialog-box */ 55599);
/* harmony import */ var _material_design__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../material-design */ 12497);
/* harmony import */ var _upload_file__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../upload-file */ 74355);















let GetStartedModule = class GetStartedModule {
};
GetStartedModule = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.NgModule)({
        declarations: [
            _get_started_component__WEBPACK_IMPORTED_MODULE_0__.GetStartedComponent,
            _personal_details_personal_details_component__WEBPACK_IMPORTED_MODULE_1__.PersonalDetailsComponent,
            _health_history_health_history_component__WEBPACK_IMPORTED_MODULE_2__.HealthHistoryComponent,
            _payment_payment_component__WEBPACK_IMPORTED_MODULE_3__.PaymentComponent,
            _appointment_appointment_component__WEBPACK_IMPORTED_MODULE_4__.AppointmentComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule,
            _angular_material_stepper__WEBPACK_IMPORTED_MODULE_12__.MatStepperModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_13__.FlexLayoutModule,
            _material_design__WEBPACK_IMPORTED_MODULE_7__.MaterialDesignModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_14__.ReactiveFormsModule,
            _upload_file__WEBPACK_IMPORTED_MODULE_8__.UploadFileModule,
            _dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxModule,
            _input_file_preview__WEBPACK_IMPORTED_MODULE_5__.InputFilePreviewModule
        ],
        exports: [_get_started_component__WEBPACK_IMPORTED_MODULE_0__.GetStartedComponent]
    })
], GetStartedModule);



/***/ }),

/***/ 28341:
/*!*******************************************************************************!*\
  !*** ./src/app/shared/get-started/health-history/health-history.component.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HealthHistoryComponent": () => (/* binding */ HealthHistoryComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _health_history_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./health-history.component.html?ngResource */ 24856);
/* harmony import */ var _health_history_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./health-history.component.scss?ngResource */ 93018);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);











let HealthHistoryComponent = class HealthHistoryComponent {
  constructor(_fb, _us, _as, _sb, _sts) {
    this._fb = _fb;
    this._us = _us;
    this._as = _as;
    this._sb = _sb;
    this._sts = _sts;
    this.dispatchView = false;
    this.exerciseTypes = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ExerciseTypes);
    this.generalNutritionMedConds = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.GeneralNutritionMedicalConditions);
    this.initialMedicalConditions = {};
    this.initialWeightLossAttempts = {};
    this.isFemale = false;
    this.isNew = false;
    this.isSportsPerson = false;
    this.mealCategories = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.MealCategories);
    this.otherMedCondFC = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl({
      value: '',
      disabled: true
    });
    this.weightLossTypes = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.WeightLossTypes);
    this.closeDialog = new _angular_core__WEBPACK_IMPORTED_MODULE_7__.EventEmitter();
    this.goToNextStep = new _angular_core__WEBPACK_IMPORTED_MODULE_7__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)(user => {
      this.me = user;

      switch (this.me.personalDetails.category) {
        case src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.UserCategories.GENERAL_NUTRITION:
          this.isSportsPerson = false;
          break;

        case src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.UserCategories.SPORTS_NUTRITION:
          this.isSportsPerson = true;
          break;

        default:
          this.isSportsPerson = false;
          break;
      }

      this.isFemale = this.me.personalDetails.gender === src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.GenderTypes.FEMALE;
      return this._us.getUserHealthHistory(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(data => {
      if (data.length === 0) {
        this.initialHealthHistory = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.UserHealthHistory();
        this.initialHealthHistory.key = this._sts.getNewKey('userHealthHistory', true, this.me.uid);
        this.initialHealthHistory.uid = this.me.uid;
        this.initialHealthHistory.membershipKey = this.me.membershipKey;
        this.initialHealthHistory.isActive = true;
        this.initialHealthHistory.recStatus = true;
        this.isNew = true;
      } else if (data.length === 1) {
        [this.initialHealthHistory] = data;
        this.isNew = false;
      }

      this.createFormGroup(this.initialHealthHistory);
      this.onOnMedicationChkChange();
      this.onIsMCycleRegularChkChange();
      this.onDoesAlcoholChkChange();
      this.onDoesSmokingChkChange();
      this.onDoesTobaccoChkChange();
      this.onDoesExerciseChkChange();
      this.onTriedLosingWeightChkChange();
      this.onOnSupplementsChkChange();
      this.onHasOtherConcernsChkChange();
      this.dispatchView = true;
    });
  }

  createFormGroup(obj) {
    this.step2FormGroup = this._fb.group({});

    for (const key in obj) {
      if (!Array.isArray(obj[key])) {
        this.step2FormGroup.addControl(key, this._fb.control(obj[key]));
      }
    } // * Medical Conditions


    this.step2FormGroup.addControl('medicalConditions', this._fb.array([]));
    this.initialHealthHistory.medicalConditions.map(i => this.addMedicalCondition(i));
    this.medicalConditionsFA.controls.map(item => {
      this.initialMedicalConditions[item.value] = true;
    }); // * Exercise

    this.step2FormGroup.addControl('exerciseDesc', this._fb.array([]));
    this.exerciseTypes.map(str => {
      const e = this.initialHealthHistory.exerciseDesc.filter(i => i.name === str && (Boolean(i.daysPerWeek) || Boolean(i.duration) || Boolean(i.intensity)))[0];
      const exercise = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.Exercise();
      exercise.name = str;

      if (e && e.name) {
        exercise.daysPerWeek = e.daysPerWeek;
        exercise.duration = e.duration;
        exercise.intensity = e.intensity;
      }

      this.addExercise(exercise);
    }); // * Weight Loss

    this.step2FormGroup.addControl('weightLossTypes', this._fb.array([]));
    this.initialHealthHistory.weightLossTypes.map(i => this.addWeightLossType(i));
    this.weightLossTypesFA.controls.map(item => {
      this.initialWeightLossAttempts[item.value] = true;
    }); // * Misc

    if (!this.onMedicationFC.value) {
      this.medicationDescFC.disable();
    }

    if (this.isMCycleRegularFC.value) {
      this.mCycleDescFC.disable();
    }

    if (!this.doesAlcoholFC.value) {
      this.alcoholDescFC.disable();
    }

    if (!this.doesSmokingFC.value) {
      this.smokingDescFC.disable();
    }

    if (!this.doesTobaccoFC.value) {
      this.tobaccoDescFC.disable();
    }

    if (!this.onSupplementsFC.value) {
      this.supplementsDescFC.disable();
    }

    if (!this.hasOtherConcernsFC.value) {
      this.otherConcernsDescFC.disable();
    }
  } // *
  // * Medical Conditions
  // *


  get medicalConditionsFA() {
    return this.step2FormGroup.get('medicalConditions');
  }

  addMedicalCondition(val) {
    this.medicalConditionsFA.push(this._fb.control(val));
  }

  removeMedicalCondition(val) {
    let i = 0;
    let remove;
    this.medicalConditionsFA.controls.map(c => {
      if (c.value === val) {
        remove = i;
      }

      i++;
    });
    this.medicalConditionsFA.removeAt(remove);
  }

  onMedCondChkChange(e) {
    if (e.checked) {
      this.addMedicalCondition(e.source.value);
    } else {
      this.removeMedicalCondition(e.source.value);
    }
  } // * -----


  get otherMedicalConditionsFC() {
    return this.step2FormGroup.get('otherMedicalConditions');
  }

  addOtherMedCond(val) {
    if (val) {
      this.addMedicalCondition(val);
    }
  }

  onOtherMedCondChkChange(e) {
    if (e.checked) {
      this.otherMedicalConditionsFC.enable();
    } else {
      this.otherMedicalConditionsFC.reset('');
      this.otherMedicalConditionsFC.disable();
    }
  } // *
  // * Medication
  // *


  get onMedicationFC() {
    return this.step2FormGroup.get('onMedication');
  }

  get medicationDescFC() {
    return this.step2FormGroup.get('medicationDesc');
  }

  onOnMedicationChkChange() {
    this.onMedicationFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.medicationDescFC.enable();
      } else {
        this.medicationDescFC.reset('');
        this.medicationDescFC.disable();
      }
    });
  } // *
  // * Menstrual Cycle
  // *


  get isMCycleRegularFC() {
    return this.step2FormGroup.get('isMCycleRegular');
  }

  get mCycleDescFC() {
    return this.step2FormGroup.get('mCycleDesc');
  }

  onIsMCycleRegularChkChange() {
    this.isMCycleRegularFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (!val) {
        this.mCycleDescFC.enable();
      } else {
        this.mCycleDescFC.reset('');
        this.mCycleDescFC.disable();
      }
    });
  } // *
  // * Alcohol
  // *


  get doesAlcoholFC() {
    return this.step2FormGroup.get('doesAlcohol');
  }

  get alcoholDescFC() {
    return this.step2FormGroup.get('alcoholDesc');
  }

  onDoesAlcoholChkChange() {
    this.doesAlcoholFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.alcoholDescFC.enable();
      } else {
        this.alcoholDescFC.reset('');
        this.alcoholDescFC.disable();
      }
    });
  } // *
  // * Smoking
  // *


  get doesSmokingFC() {
    return this.step2FormGroup.get('doesSmoking');
  }

  get smokingDescFC() {
    return this.step2FormGroup.get('smokingDesc');
  }

  onDoesSmokingChkChange() {
    this.doesSmokingFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.smokingDescFC.enable();
      } else {
        this.smokingDescFC.reset('');
        this.smokingDescFC.disable();
      }
    });
  } // *
  // * Tobacco
  // *


  get doesTobaccoFC() {
    return this.step2FormGroup.get('doesTobacco');
  }

  get tobaccoDescFC() {
    return this.step2FormGroup.get('tobaccoDesc');
  }

  onDoesTobaccoChkChange() {
    this.doesTobaccoFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.tobaccoDescFC.enable();
      } else {
        this.tobaccoDescFC.reset('');
        this.tobaccoDescFC.disable();
      }
    });
  } // *
  // * Exercise
  // *


  get doesExerciseFC() {
    return this.step2FormGroup.get('doesExercise');
  }

  onDoesExerciseChkChange() {
    this.doesExerciseFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (!val) {
        this.exerciseDescFA.controls.map(c => {
          c.get('daysPerWeek').reset();
          c.get('duration').reset();
          c.get('intensity').reset('');
        });
      }
    });
  } // * ---


  get exerciseDescFA() {
    return this.step2FormGroup.get('exerciseDesc');
  }

  addExercise(e) {
    const fg = this._fb.group(e);

    fg.get('daysPerWeek').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^[1-7]$/)]);
    fg.get('duration').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^([1-9][0-9][0-9]|[1-9][0-9]|[1-9])$/)]);
    this.exerciseDescFA.push(fg);
  } // *
  // * Weight
  // *


  get triedLosingWeightFC() {
    return this.step2FormGroup.get('triedLosingWeight');
  }

  onTriedLosingWeightChkChange() {
    this.triedLosingWeightFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (!val) {
        this.weightLossTypesFA.clear();
      }
    });
  }

  get weightLossTypesFA() {
    return this.step2FormGroup.get('weightLossTypes');
  }

  addWeightLossType(val) {
    this.weightLossTypesFA.push(this._fb.control(val));
  }

  removeWeightLossType(val) {
    let i = 0;
    let remove;
    this.weightLossTypesFA.controls.map(c => {
      if (c.value === val) {
        remove = i;
      }

      i++;
    });
    this.weightLossTypesFA.removeAt(remove);
  }

  onWeightLossTypeChkChange(e) {
    if (e.checked) {
      this.addWeightLossType(e.source.value);
    } else {
      this.removeWeightLossType(e.source.value);
    }
  } // *
  // * Meal
  // *


  get mealCategoryFC() {
    return this.step2FormGroup.get('mealCategory');
  } // *
  // * Supplements
  // *


  get onSupplementsFC() {
    return this.step2FormGroup.get('onSupplements');
  }

  onOnSupplementsChkChange() {
    this.onSupplementsFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.supplementsDescFC.enable();
      } else {
        this.supplementsDescFC.reset('');
        this.supplementsDescFC.disable();
      }
    });
  }

  get supplementsDescFC() {
    return this.step2FormGroup.get('supplementsDesc');
  } // *
  // * Other Concerns
  // *


  get hasOtherConcernsFC() {
    return this.step2FormGroup.get('hasOtherConcerns');
  }

  onHasOtherConcernsChkChange() {
    this.hasOtherConcernsFC.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(val => {
      if (val) {
        this.otherConcernsDescFC.enable();
      } else {
        this.otherConcernsDescFC.reset('');
        this.otherConcernsDescFC.disable();
      }
    });
  }

  get otherConcernsDescFC() {
    return this.step2FormGroup.get('otherConcernsDesc');
  } // *
  // *  General
  // *


  get uidFC() {
    return this.step2FormGroup.get('uid');
  }

  get keyFC() {
    return this.step2FormGroup.get('key');
  }

  enableAllDesc() {
    this.otherMedicalConditionsFC.enable();
    this.medicationDescFC.enable();
    this.mCycleDescFC.enable();
    this.alcoholDescFC.enable();
    this.smokingDescFC.enable();
    this.tobaccoDescFC.enable();
    this.supplementsDescFC.enable();
    this.otherConcernsDescFC.enable();
  } // *
  // *  Save
  // *


  isStep2FormValid() {
    return this.step2FormGroup.valid;
  }

  onSubscribeLater() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.isStep2FormValid()) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.INVALID_DATA);

        return;
      }

      try {
        _this.enableAllDesc();

        yield _this._sts.setData(_this.me.uid, 'userHealthHistory', _this.step2FormGroup.value, _this.isNew, [], true, _this.keyFC.value);

        _this.closeSelf();
      } catch (error) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM_TRY_AGAIN);
      }
    })();
  }

  onNext() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this2.isStep2FormValid()) {
        _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.INVALID_DATA);

        return;
      }

      try {
        _this2.enableAllDesc();

        yield _this2._sts.setData(_this2.me.uid, 'userHealthHistory', _this2.step2FormGroup.value, _this2.isNew, [], true, _this2.keyFC.value);
        _this2.me.onboardingStep = 2;
        const obj = {
          onboardingStep: 2
        };
        yield _this2._us.setUser(_this2.me.uid, obj);

        _this2.goToNextStep.emit();
      } catch (error) {
        _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM_TRY_AGAIN);
      }
    })();
  } // *
  // *  Close
  // *


  closeSelf() {
    this.closeDialog.emit();
  }

};

HealthHistoryComponent.ctorParameters = () => [{
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.StaticDataService
}];

HealthHistoryComponent.propDecorators = {
  closeDialog: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Output
  }],
  goToNextStep: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Output
  }]
};
HealthHistoryComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
  selector: 'app-health-history',
  template: _health_history_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_health_history_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], HealthHistoryComponent);


/***/ }),

/***/ 14526:
/*!*********************************************!*\
  !*** ./src/app/shared/get-started/index.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GetStartedComponent": () => (/* reexport safe */ _get_started_component__WEBPACK_IMPORTED_MODULE_1__.GetStartedComponent),
/* harmony export */   "GetStartedModule": () => (/* reexport safe */ _get_started_module__WEBPACK_IMPORTED_MODULE_0__.GetStartedModule)
/* harmony export */ });
/* harmony import */ var _get_started_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./get-started.module */ 14617);
/* harmony import */ var _get_started_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./get-started.component */ 3729);




/***/ }),

/***/ 31909:
/*!*****************************************************************!*\
  !*** ./src/app/shared/get-started/payment/payment.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaymentComponent": () => (/* binding */ PaymentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _payment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payment.component.html?ngResource */ 32241);
/* harmony import */ var _payment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payment.component.scss?ngResource */ 35113);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ 92340);












let PaymentComponent = class PaymentComponent {
  constructor(_as, _sb, _hs, _router, _fb, _shs) {
    this._as = _as;
    this._sb = _sb;
    this._hs = _hs;
    this._router = _router;
    this._fb = _fb;
    this._shs = _shs;
    this.dispatchView = false;
    this.loading = false;
    this.closeDialog = new _angular_core__WEBPACK_IMPORTED_MODULE_6__.EventEmitter();
    this.goToNextStep = new _angular_core__WEBPACK_IMPORTED_MODULE_6__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();

    this._router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$)).subscribe(data => {
      this.me = data;
      this.requiresGSTFC = new _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormControl(this.me.flags.requiresGSTInvoice);
      this.gstFG = this._fb.group({
        number: [this.me.personalDetails.gst.number, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.pattern(/^[A-Z0-9]{15}$/)],
        name: [this.me.personalDetails.gst.name, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.pattern(/^[a-zA-Z. ]{2,51}$/)],
        address: [this.me.personalDetails.gst.address, _angular_forms__WEBPACK_IMPORTED_MODULE_9__.Validators.minLength(5)]
      });
      this.dispatchView = true;
    });
  }

  get gstNumberFC() {
    return this.gstFG.get('number');
  }

  get gstNameFC() {
    return this.gstFG.get('name');
  }

  get gstAddressFC() {
    return this.gstFG.get('address');
  }

  onCreatePayment() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.loading = true;
      const paymentObj = {
        userUID: _this.me.uid,
        product: src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ProductTypes.ORIENTATION,
        requiresGST: _this.requiresGSTFC.value,
        gstNumber: _this.gstNumberFC.value,
        gstName: _this.gstNameFC.value,
        gstAddress: _this.gstAddressFC.value
      };

      _this._hs.createPayment(paymentObj).subscribe(res => {
        if (!res.status) {
          _this._sb.openErrorSnackBar(res.error.description || 'Sorry, the payment could not be initiated. Please try again.');

          _this.loading = false;
          return;
        }

        const {
          paymentKey
        } = res.data;
        const paises = res.data.order.amount;
        const razorPayOptions = {
          key: src_environments_environment__WEBPACK_IMPORTED_MODULE_5__.environment.payment.razorPay.key,
          amount: paises,
          currency: src_environments_environment__WEBPACK_IMPORTED_MODULE_5__.environment.payment.currency,
          name: _this.me.name,
          description: src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ProductTypes.ORIENTATION,
          order_id: res.data.order.id,
          handler: response => {
            const obj = {
              userUID: _this.me.uid,
              paymentKey,
              product: src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ProductTypes.ORIENTATION,
              pgResponse: response
            };

            _this._hs.setPayment(obj).subscribe( /*#__PURE__*/function () {
              var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res1) {
                if (!res1.status) {
                  _this._shs.setHomeSnackBar({
                    type: 'Error',
                    message: res1.error.description || 'Thank you for the payment. Please contact our Support team for the next steps.'
                  });
                } else {
                  _this._shs.setHomeSnackBar({
                    type: 'Success',
                    message: 'Thank you for the payment.'
                  });
                }

                _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.HOME]);
              });

              return function (_x) {
                return _ref.apply(this, arguments);
              };
            }());
          }
        };
        const rzpInstance = new Razorpay(razorPayOptions);
        rzpInstance.open();
        rzpInstance.on('payment.failed', response => {// console.log('[error] ', response);
        });
        _this.loading = false;

        _this.closeSelf();
      }, () => {
        _this._sb.openErrorSnackBar('Sorry, the payment could not be initiated. Please try again.');

        _this.loading = false;
      });
    })();
  }

  closeSelf() {
    this.closeDialog.emit();
  }

};

PaymentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.HttpService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SharedService
}];

PaymentComponent.propDecorators = {
  closeDialog: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Output
  }],
  goToNextStep: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Output
  }]
};
PaymentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-payment',
  template: _payment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_payment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], PaymentComponent);
 // *
// * PayU
// *
// const payUObj = {
//   txnid: res.data.paymentKey,
//   hash: res.data.hash,
//   surl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   furl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   productinfo: ProductTypes.ORIENTATION,
//   amount: Orientation.price.amountWithTax.toString(),
//   phone: this.me.personalDetails.mobile,
//   email: this.me.personalDetails.email,
//   firstname: this.me.personalDetails.firstName,
//   key: environment.payU.merchantKey
// };
// bolt.launch(payUObj, {
//   responseHandler: (obj) => {
//     if (obj.response.txnStatus !== 'CANCEL') {
//       this._hs
//         .setPayment(obj.response)
//         .subscribe((res1: APIResponse) => {
//           if (!res1.status) {
//             this._sb.openErrorSnackBar(
//               res1.error.description || ErrorMessages.SYSTEM,
//               4000
//             );
//           }
//           this._router.navigate([RouteIDs.HOME]);
//         });
//     }
//   },
//   catchException: (err) => {
//     this._sb.openErrorSnackBar(
//       err.message || ErrorMessages.SYSTEM,
//       4000
//     );
//   }
// });
// const tempPaymentObj = {
//   txnid: payUObj.txnid,
//   productinfo: payUObj.productinfo
// };
// this._hs.setPayment(tempPaymentObj).subscribe((res1: APIResponse) => {
//   if (!res1.status) {
//     this._sb.openErrorSnackBar(
//       res1.error.description || ErrorMessages.SYSTEM,
//       4000
//     );
//   }
//   this._router.navigate([RouteIDs.HOME]);
// });
// this.loading = false;
// this.closeSelf();

/***/ }),

/***/ 16286:
/*!***********************************************************************************!*\
  !*** ./src/app/shared/get-started/personal-details/personal-details.component.ts ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PersonalDetailsComponent": () => (/* binding */ PersonalDetailsComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _personal_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./personal-details.component.html?ngResource */ 98139);
/* harmony import */ var _personal_details_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./personal-details.component.scss?ngResource */ 65070);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 19193);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);











let PersonalDetailsComponent = class PersonalDetailsComponent {
  constructor(_as, _sds, _fb, _us, _sb, _uts) {
    this._as = _as;
    this._sds = _sds;
    this._fb = _fb;
    this._us = _us;
    this._sb = _sb;
    this._uts = _uts;
    this.categories = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.PackageCategories);
    this.dispatchView = false;
    this.dpChosenFileSize = 0;
    this.dpChosenInitialFileName = '';
    this.dpUploadedFileSize = 0;
    this.dpUploadedInitialFileName = '';
    this.genderTypes = src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.GenderTypes;
    this.locations = [];
    this.packageCategories = src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.PackageCategories;
    this.subCategories = [];
    this.uploadCollectionName = 'userProfilePictureFiles';
    this.uploadFileNamePartial = 'MEALpyramid_DP';
    this.uploadFileNamePrefix = '';
    this.uploadDirPath = 'displayPictures';
    this.closeDialog = new _angular_core__WEBPACK_IMPORTED_MODULE_6__.EventEmitter();
    this.goToNextStep = new _angular_core__WEBPACK_IMPORTED_MODULE_6__.EventEmitter();
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();
    this._locations$ = this._sds.getMLocations();
    this._h$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_8__.combineLatest)([this._me$, this._locations$]);
  }

  ngOnInit() {
    this._h$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(data => {
      [this.me, this.locations] = data;
      this.maxDate = moment__WEBPACK_IMPORTED_MODULE_3__().subtract(1, 'day').startOf('day').toDate();
      this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;
      this.createFG();
      this.setSubCategories();
      this.dispatchView = true;
    });
  } // *
  // * FormGroup
  // *


  createFG() {
    this.step1FormGroup = this._fb.group({
      name: [this.me.name],
      onboardingStep: [this.me.onboardingStep],
      dOBSearchIndex: [this.me.dOBSearchIndex],
      searchArray: [this.me.searchArray],
      personalDetails: this._fb.group(this.me.personalDetails)
    });
    this.firstNameFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9]{2,51}$/)]);
    this.lastNameFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9]{1,51}$/)]);
    this.dateOfBirthFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.genderFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.addressLine1.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.minLength(5)]);
    this.cityFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.stateFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.countryFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.pincodeFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9-]{3,11}$/)]);
    this.mobileFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/)]);
    this.categoryFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.serviceTypeFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.target.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.maxLength(50)]);
    const dob = moment__WEBPACK_IMPORTED_MODULE_3__(this.dateOfBirthFC.value).toDate();
    this.dob = this._uts.isValidDate(dob) ? dob : null;
  }

  get firstNameFC() {
    return this.step1FormGroup.get('personalDetails.firstName');
  }

  get lastNameFC() {
    return this.step1FormGroup.get('personalDetails.lastName');
  }

  get nameFC() {
    return this.step1FormGroup.get('name');
  }

  get gender() {
    return this.step1FormGroup.get('personalDetails.gender').value;
  }

  get genderFC() {
    return this.step1FormGroup.get('personalDetails.gender');
  }

  get dateOfBirthFC() {
    return this.step1FormGroup.get('personalDetails.dateOfBirth');
  }

  get dOBSearchIndex() {
    return this.step1FormGroup.get('dOBSearchIndex');
  }

  get searchArrayFC() {
    return this.step1FormGroup.get('searchArray');
  }

  get photoURLFC() {
    return this.step1FormGroup.get('personalDetails.photoURL');
  }

  get addressLine1() {
    return this.step1FormGroup.get('personalDetails.addressLine1');
  }

  get cityFC() {
    return this.step1FormGroup.get('personalDetails.city');
  }

  get stateFC() {
    return this.step1FormGroup.get('personalDetails.state');
  }

  get pincodeFC() {
    return this.step1FormGroup.get('personalDetails.pincode');
  }

  get countryFC() {
    return this.step1FormGroup.get('personalDetails.country');
  }

  get mobileFC() {
    return this.step1FormGroup.get('personalDetails.mobile');
  }

  get emailFC() {
    return this.step1FormGroup.get('personalDetails.email');
  }

  get preferredLocationFC() {
    return this.step1FormGroup.get('personalDetails.preferredLocation');
  }

  get categoryFC() {
    return this.step1FormGroup.get('personalDetails.category');
  }

  get serviceTypeFC() {
    return this.step1FormGroup.get('personalDetails.serviceType');
  }

  get target() {
    return this.step1FormGroup.get('personalDetails.target');
  }

  get onboardingStepFC() {
    return this.step1FormGroup.get('onboardingStep');
  }

  isStep1FormValid() {
    return this.step1FormGroup.valid;
  } // *
  // * Date of Birth
  // *


  onDOBSet(e) {
    this.dateOfBirthFC.setValue(moment__WEBPACK_IMPORTED_MODULE_3__(e.value).format('YYYY-MM-DD'));
    this.dOBSearchIndex.setValue(moment__WEBPACK_IMPORTED_MODULE_3__(e.value).format('MM-DD'));
  } // *
  // * Sub-categories
  // *


  setSubCategories() {
    if (this.categoryFC.value === this.packageCategories.GENERAL) {
      this.subCategories = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.GeneralNutritionSubCategories);
    } else if (this.categoryFC.value === this.packageCategories.PERFORMANCE) {
      this.subCategories = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.PerformanceNutritionSubCategories);
    }
  } // *
  // * Preferred Location
  // *


  isPreferredLocationChecked(l) {
    return l.key === this.preferredLocationFC.value.key;
  } // *
  // * Display Picture
  // *


  availableEvent(e) {
    if (!e.fileSize) {
      return;
    }

    this.dpChosenInitialFileName = e.initialFileName;
    this.dpChosenFileSize = e.fileSize;
  }

  uploadEvent(e) {
    if (!e.fileURL) {
      return;
    }

    this.dpUploadedInitialFileName = e.initialFileName;
    this.dpUploadedFileSize = e.fileSize;
    this.photoURLFC.setValue(e.fileURL);
  } // *
  // * Save
  // *


  setNameSearchArray() {
    const sanitizedFirstName = this._uts.sanitize(this.firstNameFC.value);

    const sanitizedLastName = this._uts.sanitize(this.lastNameFC.value);

    const fullName = `${sanitizedFirstName} ${sanitizedLastName}`;
    this.nameFC.setValue(fullName);

    const searchArray = this._uts.getSearchArray(sanitizedFirstName, sanitizedLastName);

    this.searchArrayFC.setValue(searchArray);
  }

  onSubscribeLater() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (_this.dpChosenFileSize !== _this.dpUploadedFileSize || _this.dpChosenInitialFileName !== _this.dpUploadedInitialFileName) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED);

        return;
      }

      try {
        _this.setNameSearchArray();

        yield _this._us.setUser(_this.me.uid, _this.step1FormGroup.value);

        _this.closeSelf();
      } catch (error) {
        console.log(error);
      }
    })();
  }

  onNext() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this2.isStep1FormValid()) {
        return;
      }

      if (_this2.dpChosenFileSize !== _this2.dpUploadedFileSize || _this2.dpChosenInitialFileName !== _this2.dpUploadedInitialFileName) {
        _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED);

        return;
      }

      try {
        _this2.onboardingStepFC.setValue(1);

        _this2.setNameSearchArray();

        yield _this2._us.setUser(_this2.me.uid, _this2.step1FormGroup.value);

        _this2.goToNextStep.emit();
      } catch (error) {
        console.log(error);
      }
    })();
  } // *
  // * Close
  // *


  closeSelf() {
    this.closeDialog.emit();
  }

};

PersonalDetailsComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.StaticDataService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}];

PersonalDetailsComponent.propDecorators = {
  closeDialog: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Output
  }],
  goToNextStep: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Output
  }]
};
PersonalDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
  selector: 'app-personal-details',
  template: _personal_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_personal_details_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], PersonalDetailsComponent);


/***/ }),

/***/ 60510:
/*!****************************************************!*\
  !*** ./src/app/shared/input-file-preview/index.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputFilePreviewModule": () => (/* reexport safe */ _input_file_preview_module__WEBPACK_IMPORTED_MODULE_0__.InputFilePreviewModule)
/* harmony export */ });
/* harmony import */ var _input_file_preview_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input-file-preview.module */ 99961);



/***/ }),

/***/ 24349:
/*!***************************************************************************!*\
  !*** ./src/app/shared/input-file-preview/input-file-preview.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputFilePreviewComponent": () => (/* binding */ InputFilePreviewComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _input_file_preview_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./input-file-preview.component.html?ngResource */ 61102);
/* harmony import */ var _input_file_preview_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./input-file-preview.component.scss?ngResource */ 12379);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 44661);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);








let InputFilePreviewComponent = class InputFilePreviewComponent {
  constructor(_sts, _sds) {
    this._sts = _sts;
    this._sds = _sds;
    this.acceptedFileTypes = '.jpg, .jpeg, .png';
    this.files = [];
    this.imgURL = '';
    this.isUploaded = false;
    this.uploading = false;
    this.disabled = false;
    this.isReset = false;
    this.uploadByName = '';
    this.uploadByUID = '';
    this.uploadCollectionName = '';
    this.uploadDirPath = '';
    this.uploadFileNamePrefix = '';
    this.userMembershipKey = '';
    this.userName = '';
    this.userUID = '';
    this.availableEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.uploadEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
  }

  ngOnInit() {}

  ngOnChanges() {
    if (this.isReset) {
      this.reset();
    }
  }

  reset() {
    this.files.length = 0;
    this.imgURL = '';
    this.isUploaded = false;
    this.isReset = false;
  }

  onFileAvailable(e) {
    if (this.uploading) {
      return;
    }

    this.reset();

    if (!e || !e.target || !e.target.files) {
      return;
    }

    const fileList = e.target.files;

    if (fileList.length !== 1) {
      return;
    }

    this.files.push(fileList[0]);
    const reader = new FileReader();

    reader.onload = event => {
      this.imgURL = event.target.result;
      this.availableEvent.emit({
        fileSize: this.files[0].size,
        initialFileName: this.files[0].name
      });
    };

    reader.onerror = event => {
      console.log(`CANNOT_READ_FILE: ${event.target.error.code}`);
    };

    reader.readAsDataURL(this.files[0]);
  }

  onFileUpload() {
    var _this = this;

    if (!this.files.length || !this.imgURL) {
      return;
    }

    this.uploading = true;

    const {
      fName,
      fPath,
      uploadRef,
      snapshotChanges$
    } = this._sts.upload(this.files[0], this.uploadDirPath, this.uploadFileNamePrefix, this.userUID);

    snapshotChanges$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.finalize)( /*#__PURE__*/(0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      const fKey = _this.userUID ? _this._sds.getNewKey(_this.uploadCollectionName, true, _this.userUID) : _this._sds.getNewKey(_this.uploadCollectionName);
      const fURL = yield uploadRef.getDownloadURL().toPromise();
      const fileObj = new src_app_common_models__WEBPACK_IMPORTED_MODULE_3__.Filee2();
      fileObj.key = fKey;
      fileObj.fileName = fName;
      fileObj.filePath = fPath;
      fileObj.fileURL = fURL;
      fileObj.fileType = _this.files[0].type;
      fileObj.fileSize = _this.files[0].size;
      fileObj.uploadedBy.uid = _this.uploadByUID;
      fileObj.uploadedBy.name = _this.uploadByName;
      fileObj.uploadedAt = new Date();
      fileObj.isActive = true;
      fileObj.recStatus = true;

      if (_this.userUID) {
        fileObj.uid = _this.userUID;
        fileObj.membershipKey = _this.userMembershipKey;
        yield _this._sds.setData(_this.userUID, _this.uploadCollectionName, fileObj, true, ['uploadedAt'], true, fKey);
      } else {
        yield _this._sds.setData(fKey, _this.uploadCollectionName, fileObj, true, ['uploadedAt']);
      }

      _this.uploadEvent.emit({
        fileKey: fKey,
        fileName: fName,
        fileType: _this.files[0].type,
        filePath: fPath,
        fileURL: fURL,
        fileSize: _this.files[0].size,
        initialFileName: _this.files[0].name
      });

      _this.uploading = false;
      _this.isUploaded = true;
    }))).subscribe();
  }

};

InputFilePreviewComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.StorageService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.StaticDataService
}];

InputFilePreviewComponent.propDecorators = {
  disabled: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  isReset: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadByName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadByUID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadCollectionName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadDirPath: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadFileNamePrefix: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  userMembershipKey: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  userName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  userUID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  availableEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
  }],
  uploadEvent: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
  }]
};
InputFilePreviewComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
  selector: 'app-input-file-preview',
  template: _input_file_preview_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_input_file_preview_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], InputFilePreviewComponent);


/***/ }),

/***/ 99961:
/*!************************************************************************!*\
  !*** ./src/app/shared/input-file-preview/input-file-preview.module.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputFilePreviewModule": () => (/* binding */ InputFilePreviewModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _input_file_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input-file-preview.component */ 24349);
/* harmony import */ var _shared_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../shared-components */ 35886);






let InputFilePreviewModule = class InputFilePreviewModule {
};
InputFilePreviewModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        declarations: [_input_file_preview_component__WEBPACK_IMPORTED_MODULE_0__.InputFilePreviewComponent],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _shared_components__WEBPACK_IMPORTED_MODULE_1__.SharedComponentsModule, _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__.FlexLayoutModule],
        exports: [_input_file_preview_component__WEBPACK_IMPORTED_MODULE_0__.InputFilePreviewComponent]
    })
], InputFilePreviewModule);



/***/ }),

/***/ 47633:
/*!****************************************!*\
  !*** ./src/app/shared/layout/index.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LayoutModule": () => (/* reexport safe */ _layout_module__WEBPACK_IMPORTED_MODULE_0__.LayoutModule)
/* harmony export */ });
/* harmony import */ var _layout_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layout.module */ 68634);



/***/ }),

/***/ 61184:
/*!***************************************************!*\
  !*** ./src/app/shared/layout/layout.component.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LayoutComponent": () => (/* binding */ LayoutComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _layout_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./layout.component.html?ngResource */ 1269);
/* harmony import */ var _layout_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout.component.scss?ngResource */ 47304);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ 92340);











let LayoutComponent = class LayoutComponent {
  constructor(_router, _as, _sb) {
    this._router = _router;
    this._as = _as;
    this._sb = _sb;
    this.chatURL = '';
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.loading = false;
    this.routeIDs = src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs;
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__.Subject();
    this._me$ = this._as.getMe();
  }

  ngOnInit() {
    this.path = this.getSubPath(this._router.url);
    this.chatURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__.environment.support.chatURL;

    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$)).subscribe(user => {
      this.me = user;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;
      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  getSubPath(data) {
    return data.split('/')[1];
  }

  goto(path) {
    this.path = path;

    this._router.navigate([path]);
  }

  onLogout() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this.loading = true;
        setTimeout( /*#__PURE__*/(0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
          yield _this._as.logout();
          _this.loading = false;

          _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs.LOGIN]);
        }), 750);
      } catch (error) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);
      }
    })();
  }

};

LayoutComponent.ctorParameters = () => [{
  type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}];

LayoutComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
  selector: 'app-layout',
  template: _layout_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_layout_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], LayoutComponent);


/***/ }),

/***/ 68634:
/*!************************************************!*\
  !*** ./src/app/shared/layout/layout.module.ts ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LayoutModule": () => (/* binding */ LayoutModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./layout.component */ 61184);
/* harmony import */ var _material_design__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../material-design */ 12497);
/* harmony import */ var _shared_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared-components */ 35886);







let LayoutModule = class LayoutModule {
};
LayoutModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        declarations: [_layout_component__WEBPACK_IMPORTED_MODULE_0__.LayoutComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _material_design__WEBPACK_IMPORTED_MODULE_1__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__.FlexLayoutModule,
            _shared_components__WEBPACK_IMPORTED_MODULE_2__.SharedComponentsModule
        ],
        exports: [_layout_component__WEBPACK_IMPORTED_MODULE_0__.LayoutComponent]
    })
], LayoutModule);



/***/ }),

/***/ 82421:
/*!***************************************************************!*\
  !*** ./src/app/shared/material-design/custom-date-adapter.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CustomDateAdapter": () => (/* binding */ CustomDateAdapter)
/* harmony export */ });
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material/core */ 59121);

/** Adapts the native JS Date for use with cdk-based components that work with dates. */
class CustomDateAdapter extends _angular_material_core__WEBPACK_IMPORTED_MODULE_0__.NativeDateAdapter {
    getFirstDayOfWeek() {
        return 1;
    }
}


/***/ }),

/***/ 12497:
/*!*************************************************!*\
  !*** ./src/app/shared/material-design/index.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialDesignModule": () => (/* reexport safe */ _material_design_module__WEBPACK_IMPORTED_MODULE_0__.MaterialDesignModule)
/* harmony export */ });
/* harmony import */ var _material_design_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./material-design.module */ 77116);



/***/ }),

/***/ 77116:
/*!******************************************************************!*\
  !*** ./src/app/shared/material-design/material-design.module.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialDesignModule": () => (/* binding */ MaterialDesignModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ 75074);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/sidenav */ 16643);
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/toolbar */ 52543);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/divider */ 71528);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ 19837);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/radio */ 52922);
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/datepicker */ 42298);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/core */ 59121);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/select */ 57371);
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/menu */ 88589);
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/tabs */ 15892);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/tooltip */ 6896);
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/expansion */ 17591);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/table */ 85288);
/* harmony import */ var _custom_date_adapter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./custom-date-adapter */ 82421);
























let MaterialDesignModule = class MaterialDesignModule {
};
MaterialDesignModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_4__.MatInputModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatFormFieldModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__.MatIconModule,
            _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_7__.MatProgressSpinnerModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_8__.MatButtonModule,
            _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_9__.MatSidenavModule,
            _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_10__.MatToolbarModule,
            _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__.MatDividerModule,
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__.MatDialogModule,
            _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__.MatButtonToggleModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__.MatCheckboxModule,
            _angular_material_radio__WEBPACK_IMPORTED_MODULE_15__.MatRadioModule,
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_16__.MatDatepickerModule,
            _angular_material_core__WEBPACK_IMPORTED_MODULE_17__.MatNativeDateModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_18__.MatSelectModule,
            _angular_material_menu__WEBPACK_IMPORTED_MODULE_19__.MatMenuModule,
            _angular_material_tabs__WEBPACK_IMPORTED_MODULE_20__.MatTabsModule,
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_21__.MatTooltipModule,
            _angular_material_expansion__WEBPACK_IMPORTED_MODULE_22__.MatExpansionModule,
            _angular_material_table__WEBPACK_IMPORTED_MODULE_23__.MatTableModule
        ],
        exports: [
            _angular_material_input__WEBPACK_IMPORTED_MODULE_4__.MatInputModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatFormFieldModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__.MatIconModule,
            _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_7__.MatProgressSpinnerModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_8__.MatButtonModule,
            _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_9__.MatSidenavModule,
            _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_10__.MatToolbarModule,
            _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__.MatDividerModule,
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__.MatDialogModule,
            _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__.MatButtonToggleModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_14__.MatCheckboxModule,
            _angular_material_radio__WEBPACK_IMPORTED_MODULE_15__.MatRadioModule,
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_16__.MatDatepickerModule,
            _angular_material_core__WEBPACK_IMPORTED_MODULE_17__.MatNativeDateModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_18__.MatSelectModule,
            _angular_material_menu__WEBPACK_IMPORTED_MODULE_19__.MatMenuModule,
            _angular_material_tabs__WEBPACK_IMPORTED_MODULE_20__.MatTabsModule,
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_21__.MatTooltipModule,
            _angular_material_expansion__WEBPACK_IMPORTED_MODULE_22__.MatExpansionModule,
            _angular_material_table__WEBPACK_IMPORTED_MODULE_23__.MatTableModule
        ],
        providers: [{ provide: _angular_material_core__WEBPACK_IMPORTED_MODULE_17__.DateAdapter, useClass: _custom_date_adapter__WEBPACK_IMPORTED_MODULE_0__.CustomDateAdapter }]
    })
], MaterialDesignModule);



/***/ }),

/***/ 20909:
/*!***************************************************************************************!*\
  !*** ./src/app/shared/shared-components/appointment-doc/appointment-doc.component.ts ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentDocComponent": () => (/* binding */ AppointmentDocComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _appointment_doc_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./appointment-doc.component.html?ngResource */ 13281);
/* harmony import */ var _appointment_doc_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appointment-doc.component.scss?ngResource */ 43273);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ 92340);






let AppointmentDocComponent = class AppointmentDocComponent {
    constructor() {
        this.appointmentStatuses = src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AppointmentStatuses;
        this.appointmentTypes = src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.AppointmentTypes;
        this.nutritionistStr = '';
        this.photoURL = '';
        this.showCancel = false;
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_4__.EventEmitter();
    }
    ngOnInit() { }
    ngOnChanges() {
        if (this.appointment) {
            this.showCancel = this.appointment.sessionStart > new Date();
            this.nutritionistStr = `uid_${this.appointment.staff.uid}`;
            this.photoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.staff[this.nutritionistStr]
                ? src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.staff[this.nutritionistStr].photoURL
                : '';
        }
    }
    onCancelClick() {
        this.onCancel.emit(this.appointment);
    }
};
AppointmentDocComponent.ctorParameters = () => [];
AppointmentDocComponent.propDecorators = {
    appointment: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Output }]
};
AppointmentDocComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-appointment-doc',
        template: _appointment_doc_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_appointment_doc_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AppointmentDocComponent);



/***/ }),

/***/ 56526:
/*!*********************************************************************!*\
  !*** ./src/app/shared/shared-components/avatar/avatar.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AvatarComponent": () => (/* binding */ AvatarComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _avatar_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./avatar.component.html?ngResource */ 93301);
/* harmony import */ var _avatar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./avatar.component.scss?ngResource */ 60679);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ 92340);









let AvatarComponent = class AvatarComponent {
    constructor(_us) {
        this._us = _us;
        this.avatarSize = '';
        this.photoURL = '';
        this.border = false;
        this.userUID = '';
        this.size = '';
        this.zIndex = 0;
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__.Subject();
    }
    ngOnInit() { }
    ngOnChanges() {
        if (this.userUID) {
            this._us
                .getPersonByUID(this.userUID)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.takeUntil)(this._notifier$))
                .subscribe((user) => {
                this.user = user;
                const staffStr = `uid_${this.user.uid}`;
                if (this.user.personalDetails.photoURL) {
                    this.photoURL = this.user.personalDetails.photoURL;
                }
                else if (!this.user.roles.includes(src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.UserRoles.USER)) {
                    this.photoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__.environment.staff[staffStr]
                        ? src_environments_environment__WEBPACK_IMPORTED_MODULE_4__.environment.staff[staffStr].photoURL
                        : '';
                }
                else if (this.user.personalDetails.gender === 'Male') {
                    this.photoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__.environment.avatar.userMale;
                }
                else if (this.user.personalDetails.gender === 'Female') {
                    this.photoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_4__.environment.avatar.userFemale;
                }
            });
        }
        if (this.size) {
            this.avatarSize = `e-avatar--${this.size}`;
        }
    }
};
AvatarComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.UserService }
];
AvatarComponent.propDecorators = {
    border: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input }],
    userUID: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input }],
    size: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input }],
    zIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Input }]
};
AvatarComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-avatar',
        template: _avatar_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_avatar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AvatarComponent);



/***/ }),

/***/ 79914:
/*!*****************************************************************************************!*\
  !*** ./src/app/shared/shared-components/book-appointment/book-appointment.component.ts ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookAppointmentComponent": () => (/* binding */ BookAppointmentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _book_appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./book-appointment.component.html?ngResource */ 17418);
/* harmony import */ var _book_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./book-appointment.component.scss?ngResource */ 26834);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 19193);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);














let BookAppointmentComponent = class BookAppointmentComponent {
  constructor(_sds, _as, _fb, _hs, _sb, _dialog, _shs) {
    this._sds = _sds;
    this._as = _as;
    this._fb = _fb;
    this._hs = _hs;
    this._sb = _sb;
    this._dialog = _dialog;
    this._shs = _shs;
    this.appointmentTypes = Object.values(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes);
    this.areSlotsAvailable = false;
    this.bookingAppt = false;
    this.dispatchView = false;
    this.isSearchPristine = true;
    this.loadingSlots = false;
    this.locations = [];
    this.slots = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
    this._locations$ = this._sds.getMLocations();
    this._h$ = (0,rxjs__WEBPACK_IMPORTED_MODULE_9__.combineLatest)([this._me$, this._locations$]);
  }

  ngOnInit() {
    this._h$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$)).subscribe(data => {
      [this.me, this.locations] = data;
      this.appointmentTypes = this.appointmentTypes.filter(item => item !== src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.ORIENTATION);
      this.createSearchFG();
      this.startDate = moment__WEBPACK_IMPORTED_MODULE_3__().startOf('day').toDate();
      this.endDate = moment__WEBPACK_IMPORTED_MODULE_3__().add(365, 'day').startOf('day').toDate();
      this.dispatchView = true;
    });
  }

  createSearchFG() {
    this.searchFG = this._fb.group({
      date: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required],
      location: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required],
      type: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_11__.Validators.required]
    });
  }

  get locationFC() {
    return this.searchFG.get('location');
  }

  get dateFC() {
    return this.searchFG.get('date');
  }

  get typeFC() {
    return this.searchFG.get('type');
  }

  isSearchFGValid() {
    return this.searchFG.valid;
  }

  resetFactorySearch() {
    this.searchFG.reset();
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
    this.isSearchPristine = true;
  }

  resetLightSearch() {
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
  }

  lockSearch() {
    this.loadingSlots = true;
    this.searchFG.disable();
  }

  unlockSearch() {
    this.loadingSlots = false;
    this.searchFG.enable();
  }

  lockBook() {
    this.bookingAppt = true;
    this.searchFG.disable();
  }

  unlockBook() {
    this.bookingAppt = false;
    this.searchFG.enable();
  }

  onSearch() {
    this.lockSearch();
    this.resetLightSearch();
    this.isSearchPristine = false;
    const h$ = this.typeFC.value === src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.RECALL ? this._hs.getAvailableRecallSlots(this.me.uid, moment__WEBPACK_IMPORTED_MODULE_3__(this.dateFC.value).format('YYYY-MM-DD'), this.locationFC.value.key) : this._hs.getAvailableAdvancedTestingSlots(this.me.uid, moment__WEBPACK_IMPORTED_MODULE_3__(this.dateFC.value).format('YYYY-MM-DD'), this.locationFC.value.key);
    h$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_12__.catchError)(err => {
      this._sb.openErrorSnackBar(err.message || src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM_TRY_AGAIN);

      this.resetFactorySearch();
      this.unlockSearch();
      return rxjs__WEBPACK_IMPORTED_MODULE_13__.EMPTY;
    })).subscribe(data => {
      this.slots = data;

      if (this.slots.length > 0) {
        this.areSlotsAvailable = true;
      }

      this.unlockSearch();
    });
  }

  onSlotClicked(appt) {
    appt.clicked = true;
    this.slots.map(item => {
      if (item.key !== appt.key) {
        item.clicked = false;
      }
    });
    this.selectedSlot = appt;
  }

  onBookAppointment() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.lockBook();

      const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_5__.DialogBox();
      dialogBox.actionFalseBtnTxt = 'No';
      dialogBox.actionTrueBtnTxt = 'Yes';
      dialogBox.icon = 'help_center';
      dialogBox.title = 'Confirmation';
      dialogBox.message = 'Are you sure you want to book this appointment?';
      dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
      dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

      _this._shs.setDialogBox(dialogBox);

      const dialogRef = _this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_7__.DialogBoxComponent);

      return new Promise(resolve => {
        dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
          var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
            if (!res) {
              resolve(false);

              _this.unlockBook();

              return;
            }

            const h$ = _this.typeFC.value === src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.AppointmentTypes.RECALL ? _this._hs.setRecallAppointment({
              userUID: _this.me.uid,
              appointmentKey: _this.selectedSlot.key
            }) : _this._hs.setAdvancedTestingAppointment({
              userUID: _this.me.uid,
              appointmentKey: _this.selectedSlot.key,
              appointmentMergeSlotKey: _this.selectedSlot.mergeSlotKey
            });
            h$.subscribe(apiRes => {
              if (!apiRes.status) {
                _this._sb.openErrorSnackBar(apiRes.error.description);

                resolve(false);

                _this.unlockBook();

                return;
              }

              _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.APPT_BOOKED);

              _this.resetFactorySearch();

              _this.unlockBook();

              resolve(true);
            });
          });

          return function (_x) {
            return _ref.apply(this, arguments);
          };
        }());
      });
    })();
  }

};

BookAppointmentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.StaticDataService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.AuthService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SnackBarService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_14__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_6__.SharedService
}];

BookAppointmentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_15__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_16__.Component)({
  selector: 'app-book-appointment',
  template: _book_appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_book_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], BookAppointmentComponent);


/***/ }),

/***/ 357:
/*!*******************************************************************************************!*\
  !*** ./src/app/shared/shared-components/button-icon-title/button-icon-title.component.ts ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonIconTitleComponent": () => (/* binding */ ButtonIconTitleComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _button_icon_title_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./button-icon-title.component.html?ngResource */ 84075);
/* harmony import */ var _button_icon_title_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./button-icon-title.component.scss?ngResource */ 42023);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);




let ButtonIconTitleComponent = class ButtonIconTitleComponent {
    constructor() {
        this.showSpinner = false;
        this.buttonStyle = '';
        this.disabled = false;
        this.icon = '';
        this.iconStyle = '';
        this.loading = false;
        this.title = '';
        this.titleStyle = '';
        this.toolTip = '';
        this.onButtonClick = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
    }
    ngOnInit() { }
    ngOnChanges() {
        this.showSpinner = this.loading;
        this.buttonCSS = {
            ...(this.buttonStyle && { [this.buttonStyle]: true }),
            'eh-csr-dsb': this.disabled
        };
        this.iconCSS = {
            [this.iconStyle || 'e-mat-icon--3']: true
        };
        this.titleCSS = {
            [this.titleStyle]: true
        };
    }
    onClick($event) {
        if (this.disabled) {
            return;
        }
        this.onButtonClick.emit($event);
    }
};
ButtonIconTitleComponent.ctorParameters = () => [];
ButtonIconTitleComponent.propDecorators = {
    buttonStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    disabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    iconStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    loading: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    titleStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    toolTip: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    onButtonClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Output }]
};
ButtonIconTitleComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-button-icon-title',
        template: _button_icon_title_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_button_icon_title_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ButtonIconTitleComponent);



/***/ }),

/***/ 54416:
/*!*******************************************************************************!*\
  !*** ./src/app/shared/shared-components/button-icon/button-icon.component.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonIconComponent": () => (/* binding */ ButtonIconComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _button_icon_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./button-icon.component.html?ngResource */ 36961);
/* harmony import */ var _button_icon_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./button-icon.component.scss?ngResource */ 41127);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);




let ButtonIconComponent = class ButtonIconComponent {
    constructor() {
        this.showSpinner = false;
        this.disabled = false;
        this.icon = '';
        this.iconStyle = '';
        this.loading = false;
        this.toolTip = '';
        this.onButtonClick = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
    }
    ngOnInit() { }
    ngOnChanges() {
        this.showSpinner = this.loading;
        this.iconCSS = {
            [this.iconStyle || 'e-mat-icon--3']: true
        };
    }
    onClick($event) {
        if (this.disabled) {
            return;
        }
        this.onButtonClick.emit($event);
    }
};
ButtonIconComponent.ctorParameters = () => [];
ButtonIconComponent.propDecorators = {
    disabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    iconStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    loading: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    toolTip: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    onButtonClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Output }]
};
ButtonIconComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-button-icon',
        template: _button_icon_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_button_icon_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ButtonIconComponent);



/***/ }),

/***/ 42156:
/*!*********************************************************************!*\
  !*** ./src/app/shared/shared-components/button/button.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonComponent": () => (/* binding */ ButtonComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _button_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./button.component.html?ngResource */ 74341);
/* harmony import */ var _button_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./button.component.scss?ngResource */ 54075);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
// Verified -
/**
 * * Width is needed for the spinner to center-align while loading.
 * * Height is needed in Users App, although not needed for the same component in Staff App.
 */




let ButtonComponent = class ButtonComponent {
    constructor() {
        this.showSpinner = false;
        this.buttonStyle = '';
        this.disabled = false;
        this.loading = false;
        this.title = '';
        this.onButtonClick = new _angular_core__WEBPACK_IMPORTED_MODULE_2__.EventEmitter();
    }
    ngOnInit() { }
    ngOnChanges() {
        this.showSpinner = this.loading;
        this.buttonCSS = {
            'e-mat-button eh-px-12': true,
            [this.buttonStyle]: true,
            'eh-csr-dsb': this.disabled
        };
    }
    onClick($event) {
        if (this.disabled) {
            return;
        }
        this.onButtonClick.emit($event);
    }
};
ButtonComponent.ctorParameters = () => [];
ButtonComponent.propDecorators = {
    buttonStyle: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    disabled: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    loading: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    onButtonClick: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Output }]
};
ButtonComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-button',
        template: _button_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_button_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ButtonComponent);



/***/ }),

/***/ 40301:
/*!*********************************************************************!*\
  !*** ./src/app/shared/shared-components/footer/footer.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FooterComponent": () => (/* binding */ FooterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _footer_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./footer.component.html?ngResource */ 31703);
/* harmony import */ var _footer_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./footer.component.scss?ngResource */ 7555);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services */ 98138);










let FooterComponent = class FooterComponent {
    // private _me$: Observable<User> = this._as.getMe();
    constructor(_router, navCtrl, _as) {
        this._router = _router;
        this.navCtrl = navCtrl;
        this._as = _as;
        this.chatURL = '';
        this.dispatchView = false;
        this.isPackageSubscribed = false;
        this.loading = false;
        this.routeIDs = src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.RouteIDs;
        this.closed$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
        this.showTabs = true; // <-- show tabs by default
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
    }
    ngOnInit() {
        this._as.getMe().pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.catchError)((error) => {
            return rxjs__WEBPACK_IMPORTED_MODULE_6__.EMPTY;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$)).subscribe((user) => {
            this.me = user;
            console.log(this.me);
            this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
            this.subscribedPackage = this.me.subscribedPackage;
            this.dispatchView = true;
        });
    }
    goto(path) {
        this.path = path;
        this._router.navigate([path]);
        console.log(this.path);
    }
    ngOnDestroy() {
        this.closed$.next();
        this._notifier$.next();
        this._notifier$.complete(); // <-- close subscription when component is destroyed
    }
};
FooterComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.NavController },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_3__.AuthService }
];
FooterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-footer',
        template: _footer_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_footer_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FooterComponent);



/***/ }),

/***/ 1334:
/*!*********************************************************************!*\
  !*** ./src/app/shared/shared-components/header/header.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderComponent": () => (/* binding */ HeaderComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _header_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.component.html?ngResource */ 85309);
/* harmony import */ var _header_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.component.scss?ngResource */ 89537);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 60124);





let HeaderComponent = class HeaderComponent {
    constructor(_router) {
        this._router = _router;
        this.title = "";
        this.backButtonURL = "/tab/home";
        this.show_footer = true;
    }
    ngOnInit() {
        this.subcribption = this._router.events.subscribe((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__.NavigationEnd) {
                if (this._router.url === "/tab/home") {
                    this.show_footer = false;
                }
                else {
                    this.show_footer = true;
                }
            }
        });
    }
    ngOnDestroy() {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subcribption.unsubscribe();
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.Router }
];
HeaderComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    backButtonURL: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }]
};
HeaderComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-header',
        template: _header_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_header_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], HeaderComponent);



/***/ }),

/***/ 35886:
/*!***************************************************!*\
  !*** ./src/app/shared/shared-components/index.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedComponentsModule": () => (/* reexport safe */ _shared_components_module__WEBPACK_IMPORTED_MODULE_0__.SharedComponentsModule)
/* harmony export */ });
/* harmony import */ var _shared_components_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shared-components.module */ 89366);



/***/ }),

/***/ 71722:
/*!********************************************************************!*\
  !*** ./src/app/shared/shared-components/preview-download/index.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreviewDownloadComponent": () => (/* reexport safe */ _preview_download_component__WEBPACK_IMPORTED_MODULE_0__.PreviewDownloadComponent)
/* harmony export */ });
/* harmony import */ var _preview_download_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preview-download.component */ 71281);



/***/ }),

/***/ 71281:
/*!*****************************************************************************************!*\
  !*** ./src/app/shared/shared-components/preview-download/preview-download.component.ts ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreviewDownloadComponent": () => (/* binding */ PreviewDownloadComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _preview_download_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preview-download.component.html?ngResource */ 63552);
/* harmony import */ var _preview_download_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./preview-download.component.scss?ngResource */ 49749);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);







let PreviewDownloadComponent = class PreviewDownloadComponent {
  constructor(_sts, _sds, _sb) {
    this._sts = _sts;
    this._sds = _sds;
    this._sb = _sb;
    this.acceptedFileTypes = '';
    this.dispatchFileView = false;
    this.deleting = false;
    this.downloading = false;
    this.isFileAvailable = false;
    this.uploading = false;
    this.allowDelete = false;
    this.allowReplace = false;
    this.allowUpload = false;
    this.downloadFileName = '';
    this.fileKey = '';
    this.fileName = '';
    this.filePath = '';
    this.fileURL = '';
    this.isImg = false;
    this.isImgAllowed = false;
    this.placeholder = '';
    this.uploadByName = '';
    this.uploadByUID = '';
    this.uploadCollectionName = '';
    this.uploadDir = '';
    this.userMembershipKey = '';
    this.userName = '';
    this.userUID = '';
    this.title = '';
    this.onUploadComplete = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
    this.onDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
  }

  ngOnInit() {}

  ngOnChanges() {
    this.isFileAvailable = Boolean(this.fileURL);
    this.acceptedFileTypes = this.isImgAllowed ? '.jpg, .jpeg, .png, .pdf' : '.pdf';
    setTimeout(() => {
      this.dispatchFileView = true;
    }, 250);
  }

  onDownloadFile() {
    this.downloading = true;

    this._sts.download(this.fileURL, this.downloadFileName);

    setTimeout(() => {
      this.downloading = false;
    }, 2000);
  }

  onUploadFile(e, replaceFlag = false) {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!e || !e.target || !e.target.files) {
        return;
      }

      const fileList = e.target.files;

      if (fileList.length < 1) {
        return;
      }

      if (replaceFlag) {
        yield _this.onDeleteFile(true);
      }

      const file = fileList.item(0);
      _this.isImg = !file.type.includes('pdf'); // this.startUpload(file);
    })();
  } // startUpload(file: File): void {
  //   if (!file) {
  //     return;
  //   }
  //   this.uploading = true;
  //   const { fName, fPath, uploadRef, snapshotChanges$ } = this._sts.upload(
  //     file,
  //     this.uploadDir,
  //     this.fileName,
  //     this.userUID
  //   );
  //   snapshotChanges$
  //     .pipe(
  //       finalize(async () => {
  //         const fKey = this.userUID
  //           ? this._sds.getNewKey(this.uploadCollectionName, true, this.userUID)
  //           : this._sds.getNewKey(this.uploadCollectionName);
  //         const fURL = await uploadRef.getDownloadURL().toPromise();
  //         const fileObj = new Filee();
  //         fileObj.key = fKey;
  //         fileObj.fileName = fName;
  //         fileObj.filePath = fPath;
  //         fileObj.fileURL = fURL;
  //         fileObj.fileType = file.type;
  //         fileObj.isActive = true;
  //         fileObj.membershipKey = this.userMembershipKey;
  //         fileObj.recStatus = true;
  //         fileObj.uploadedAt = new Date();
  //         fileObj.uploadedBy.uid = this.uploadByUID;
  //         fileObj.uploadedBy.name = this.uploadByName;
  //         fileObj.user.uid = this.userUID;
  //         fileObj.user.name = this.userName;
  //         if (this.userUID) {
  //           await this._sds.setData(
  //             this.userUID,
  //             this.uploadCollectionName,
  //             fileObj,
  //             true,
  //             ['uploadedAt'],
  //             true,
  //             fKey
  //           );
  //         } else {
  //           await this._sds.setData(
  //             fKey,
  //             this.uploadCollectionName,
  //             fileObj,
  //             true,
  //             ['uploadedAt']
  //           );
  //         }
  //         this.fileKey = fKey;
  //         this.fileName = this.fileName ? this.fileName : fName;
  //         this.filePath = fPath;
  //         this.fileURL = fURL;
  //         this.isFileAvailable = true;
  //         this.onUploadComplete.emit({
  //           fileKey: fKey,
  //           fileName: fName,
  //           filePath: fPath,
  //           fileURL: fURL
  //         });
  //         this.uploading = false;
  //       })
  //     )
  //     .subscribe();
  // }


  onDeleteFile(replaceFlag = false) {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      try {
        _this2.deleting = true;

        if (_this2.userUID) {
          yield _this2._sds.setData(_this2.userUID, _this2.uploadCollectionName, {
            recStatus: false
          }, false, ['uploadedAt'], true, _this2.fileKey);
        } else {
          yield _this2._sds.setData(_this2.fileKey, _this2.uploadCollectionName, {
            recStatus: false
          }, false, ['uploadedAt']);
        }

        _this2._sts.delete(_this2.filePath).then().catch();

        if (!replaceFlag) {
          _this2.reset();
        }

        _this2.onDelete.emit();

        _this2.deleting = false;
      } catch (err) {
        _this2._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);

        _this2.deleting = false;
      }
    })();
  }

  reset() {
    this.fileKey = '';
    this.fileName = '';
    this.filePath = '';
    this.fileURL = '';
    this.isFileAvailable = false;
  }

};

PreviewDownloadComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.StorageService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.StaticDataService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}];

PreviewDownloadComponent.propDecorators = {
  allowDelete: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  allowReplace: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  allowUpload: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  downloadFileName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  fileKey: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  fileName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  filePath: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  fileURL: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  isImg: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  isImgAllowed: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  placeholder: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadByName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadByUID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadCollectionName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  uploadDir: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  userMembershipKey: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  userName: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  userUID: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  title: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input
  }],
  onUploadComplete: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
  }],
  onDelete: [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output
  }]
};
PreviewDownloadComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
  selector: 'app-preview-download',
  template: _preview_download_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_preview_download_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], PreviewDownloadComponent);


/***/ }),

/***/ 21893:
/*!*************************************************************************************!*\
  !*** ./src/app/shared/shared-components/profile-avatar/profile-avatar.component.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfileAvatarComponent": () => (/* binding */ ProfileAvatarComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _profile_avatar_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile-avatar.component.html?ngResource */ 42788);
/* harmony import */ var _profile_avatar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile-avatar.component.scss?ngResource */ 63445);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 19193);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);












let ProfileAvatarComponent = class ProfileAvatarComponent {
  constructor(_as, _dialog, _shs, _sb, _hs, _router, _ss, _us, _uts, menuCtrl, alertController, _activatedRoute) {
    this._as = _as;
    this._dialog = _dialog;
    this._shs = _shs;
    this._sb = _sb;
    this._hs = _hs;
    this._router = _router;
    this._ss = _ss;
    this._us = _us;
    this._uts = _uts;
    this.menuCtrl = menuCtrl;
    this.alertController = alertController;
    this._activatedRoute = _activatedRoute;
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_5__.Subject();
    this.routeIDs = src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs;
    this.hideNotificationIcon = false;
    this.dispatchView = false;
    this.menuCtrl.swipeGesture(true);
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.switchMap)(me => {
      this.me = me;
      this.dispatchView = true;
      console.log(this.me);
      this.userHasSubcribedPackage = this.me.flags.isPackageSubscribed;
      return (0,rxjs__WEBPACK_IMPORTED_MODULE_7__.combineLatest)([this._us.getActiveUserAppointments(this.me.uid, this.me.membershipKey), this._ss.getSessionsByUser(this.me.uid, this.me.membershipKey)]);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.catchError)(() => {
      this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_10__.EMPTY;
    })).subscribe( /*#__PURE__*/function () {
      var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (arr) {});

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }()); //   this.subscription= this._router.events.subscribe(
    //   (event: any) => {
    //     if (event instanceof NavigationEnd) {
    //       console.log('this.router.url', this._router.url);
    //       if(this._router.url==="/p/notification")
    //       this.hideNotificationIcon=true;
    //       console.log("sbjsbjb")
    //     }
    //     else
    //     {
    //       this.hideNotificationIcon=false;
    //     }
    //   }
    // );  
    // console.log(this.hideNotificationIcon)

  }

  goto(path) {
    this.path = path;

    this._router.navigate([path]);
  }

  onLogout() {// const dialogBox = new DialogBox();
    // dialogBox.actionFalseBtnTxt = 'No';
    // dialogBox.actionTrueBtnTxt = 'Yes';
    // dialogBox.icon = 'logout';
    // dialogBox.title = 'Logout';
    // dialogBox.message = '  Are you sure you want to log out ? ';
    // dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    // dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    // this._shs.setDialogBox(dialogBox);
    // const dialogRef = this._dialog.open(DialogBoxComponent);
    // dialogRef.afterClosed().subscribe(async (res: any) => {
    //   if (!res) {
    //     return;
    //   }
    //   try {
    //     setTimeout(async () => {
    //                     await this._as.logout();
    //                     this._router.navigate([RouteIDs.LOGIN]);
    //                   }, 750);
    //   } catch (err) {
    //     this._sb.openErrorSnackBar(ErrorMessages.DELETE_FAILED);
    //   }
    // });
  }

};

ProfileAvatarComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.HttpService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.Router
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SessionnService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.UserService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.UtilitiesService
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.MenuController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.AlertController
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_12__.ActivatedRoute
}];

ProfileAvatarComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-profile-avatar',
  template: _profile_avatar_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_profile_avatar_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProfileAvatarComponent);


/***/ }),

/***/ 9043:
/*!***********************************************************************************************!*\
  !*** ./src/app/shared/shared-components/recommended-package/recommended-package.component.ts ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RecommendedPackageComponent": () => (/* binding */ RecommendedPackageComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _recommended_package_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recommended-package.component.html?ngResource */ 18431);
/* harmony import */ var _recommended_package_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recommended-package.component.scss?ngResource */ 55943);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ 92340);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);













let RecommendedPackageComponent = class RecommendedPackageComponent {
  constructor(_sts, _as, _sb, _hs, _dialog, _router, _fb, _shs) {
    this._sts = _sts;
    this._as = _as;
    this._sb = _sb;
    this._hs = _hs;
    this._dialog = _dialog;
    this._router = _router;
    this._fb = _fb;
    this._shs = _shs;
    this.allPackages = [];
    this.dispatchView = false;
    this.loading = false;
    this.generalNumberOfSessionsArr = [6, 10, 20, 40];
    this.gstMultiplier = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.gst.multiplier;
    this.numberOfSessionsArr = [];
    this.numberOfSessionsFC = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('');
    this.relevantPackages = [];
    this.shownPackages = [];
    this.sportsNumberOfSessionsArr = [10, 20];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_7__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.switchMap)(me => {
      this.me = me;
      this.recommendedPackage = this.me.recommendedPackage;
      this.requiresGSTFC = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(this.me.flags.requiresGSTInvoice);
      this.gstFG = this._fb.group({
        number: [this.me.personalDetails.gst.number, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^[A-Z0-9]{15}$/)],
        name: [this.me.personalDetails.gst.name, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.pattern(/^[a-zA-Z. ]{2,51}$/)],
        address: [this.me.personalDetails.gst.address, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.minLength(5)]
      });
      this.numberOfSessionsArr = this.me.personalDetails.category === src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.UserCategories.GENERAL_NUTRITION ? this.generalNumberOfSessionsArr : this.sportsNumberOfSessionsArr;
      this.defaultSelection = this.numberOfSessionsArr.indexOf(this.recommendedPackage.numberOfSessions);
      this.numberOfSessionsFC.setValue(this.recommendedPackage.numberOfSessions);
      return this._sts.getPackages();
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.catchError)(() => {
      this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_11__.EMPTY;
    })).subscribe(data => {
      this.allPackages = data;

      if (this.me.personalDetails.category) {
        this.relevantPackages = this.allPackages.filter(item => item.category === this.me.personalDetails.category);
      } else {
        this.relevantPackages = this.allPackages;
      }

      this.onNumberOfSessionsSelect(this.me.recommendedPackage.numberOfSessions);
      this.numberOfSessionsArr = this.allPackages.map(i => {
        if (i.category === this.me.personalDetails.category) {
          return {
            name: i.name,
            sessions: i.numberOfSessions
          };
        }

        return '';
      });
      this.numberOfSessionsArr = this.numberOfSessionsArr.filter(i => i.name).sort((a, b) => a.sessions - b.sessions);
      this.numberOfSessionsArr = [...new Map(this.numberOfSessionsArr.map(item => [item.name, item])).values()];
      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  get gstNumberFC() {
    return this.gstFG.get('number');
  }

  get gstNameFC() {
    return this.gstFG.get('name');
  }

  get gstAddressFC() {
    return this.gstFG.get('address');
  }

  onNumberOfSessionsSelect(n) {
    this.shownPackages.length = 0;
    this.shownPackages = this.relevantPackages.filter(i => i.numberOfSessions === n);
  }

  onCreatePayment(pkg) {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      _this.loading = true;
      pkg.clicked = true;
      const paymentObj = {
        userUID: _this.me.uid,
        name: _this.me.name,
        product: src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ProductTypes.SUBSCRIPTION,
        packageKey: pkg.key,
        requiresGST: _this.requiresGSTFC.value,
        gstNumber: _this.gstNumberFC.value,
        gstName: _this.gstNameFC.value,
        gstAddress: _this.gstAddressFC.value
      };

      _this._hs.createPayment(paymentObj).subscribe(res => {
        if (!res.status) {
          _this._sb.openErrorSnackBar(res.error.description || 'Sorry, the payment could not be initiated. Please try again.');

          _this.loading = false;
          pkg.clicked = false;
          return;
        }

        const {
          paymentKey
        } = res.data;
        const paises = res.data.order.amount;
        const razorPayOptions = {
          key: src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.payment.razorPay.key,
          amount: paises,
          currency: src_environments_environment__WEBPACK_IMPORTED_MODULE_3__.environment.payment.currency,
          name: _this.me.name,
          description: src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ProductTypes.SUBSCRIPTION,
          order_id: res.data.order.id,
          handler: response => {
            const obj = {
              userUID: _this.me.uid,
              paymentKey,
              product: src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ProductTypes.SUBSCRIPTION,
              packageKey: pkg.key,
              pgResponse: response
            };

            _this._hs.setPayment(obj).subscribe( /*#__PURE__*/function () {
              var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res1) {
                if (!res1.status) {
                  _this._shs.setHomeSnackBar({
                    type: 'Error',
                    message: res1.error.description || 'Thank you for the payment. Please contact our Support team for the next steps.'
                  });
                } else {
                  _this._shs.setHomeSnackBar({
                    type: 'Success',
                    message: 'Thank you for the payment.'
                  });
                }

                _this._router.navigate([src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.RouteIDs.HOME]);
              });

              return function (_x) {
                return _ref.apply(this, arguments);
              };
            }());
          }
        };
        const rzpInstance = new Razorpay(razorPayOptions);
        rzpInstance.open();
        rzpInstance.on('payment.failed', response => {// console.log('[error] ', response);
        });
        _this.loading = false;
        pkg.clicked = false;

        _this.closeSelf();
      }, () => {
        _this._sb.openErrorSnackBar('Sorry, the payment could not be initiated. Please try again.');

        _this.loading = false;
        pkg.clicked = false;
      });
    })();
  }

  closeSelf() {
    this._dialog.close();
  }

};

RecommendedPackageComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.StaticDataService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_12__.MatDialogRef
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_13__.Router
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}];

RecommendedPackageComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-recommended-package',
  template: _recommended_package_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_recommended_package_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], RecommendedPackageComponent);
 // *
// * PayU
// *
// const payUObj = {
//   txnid: res.data.paymentKey,
//   hash: res.data.hash,
//   surl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   furl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   productinfo: ProductTypes.SUBSCRIPTION,
//   amount: pkg.price.amountWithTax.toString(),
//   phone: this.me.personalDetails.mobile,
//   email: this.me.personalDetails.email,
//   firstname: this.me.personalDetails.firstName,
//   key: environment.payU.merchantKey
// };
// bolt.launch(payUObj, {
//   responseHandler: (obj) => {
//     if (obj.response.txnStatus !== 'CANCEL') {
//       this._hs
//         .setPayment(obj.response)
//         .subscribe(async (res1: APIResponse) => {
//           if (!res1.status) {
//             this._sb.openErrorSnackBar(
//               res1.error.description || ErrorMessages.SYSTEM
//             );
//           }
//           await this._us.setUser(this.me.uid, {
//             flags: {
//               requiresGSTInvoice: this.needGSTFC.value
//             },
//             personalDetails: {
//               gstNumber: this.gstNumberFC.value
//             }
//           });
//           this.closeSelf();
//           this._router.navigate([RouteIDs.HOME]);
//         });
//     }
//   },
//   catchException: (err) => {
//     this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
//   }
// });
// this.loading = false;
// this.closeSelf();

/***/ }),

/***/ 89366:
/*!**********************************************************************!*\
  !*** ./src/app/shared/shared-components/shared-components.module.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SharedComponentsModule": () => (/* binding */ SharedComponentsModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header/header.component */ 1334);
/* harmony import */ var _profile_avatar_profile_avatar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile-avatar/profile-avatar.component */ 21893);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/tooltip */ 6896);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/progress-spinner */ 61708);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ng2-pdf-viewer */ 63940);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/form-field */ 75074);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/select */ 57371);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);
/* harmony import */ var _preview_download__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./preview-download */ 71722);
/* harmony import */ var _appointment_doc_appointment_doc_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./appointment-doc/appointment-doc.component */ 20909);
/* harmony import */ var _avatar_avatar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./avatar/avatar.component */ 56526);
/* harmony import */ var _recommended_package_recommended_package_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recommended-package/recommended-package.component */ 9043);
/* harmony import */ var _button_button_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./button/button.component */ 42156);
/* harmony import */ var _button_icon_button_icon_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./button-icon/button-icon.component */ 54416);
/* harmony import */ var _button_icon_title_button_icon_title_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./button-icon-title/button-icon-title.component */ 357);
/* harmony import */ var _material_design__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../material-design */ 12497);
/* harmony import */ var _tabs_tabs_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tabs/tabs.component */ 7119);
/* harmony import */ var _book_appointment_book_appointment_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./book-appointment/book-appointment.component */ 79914);
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./footer/footer.component */ 40301);




























let SharedComponentsModule = class SharedComponentsModule {
};
SharedComponentsModule = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_14__.NgModule)({
        declarations: [
            _appointment_doc_appointment_doc_component__WEBPACK_IMPORTED_MODULE_3__.AppointmentDocComponent,
            _preview_download__WEBPACK_IMPORTED_MODULE_2__.PreviewDownloadComponent,
            _avatar_avatar_component__WEBPACK_IMPORTED_MODULE_4__.AvatarComponent,
            _recommended_package_recommended_package_component__WEBPACK_IMPORTED_MODULE_5__.RecommendedPackageComponent,
            _button_button_component__WEBPACK_IMPORTED_MODULE_6__.ButtonComponent,
            _button_icon_button_icon_component__WEBPACK_IMPORTED_MODULE_7__.ButtonIconComponent,
            _button_icon_title_button_icon_title_component__WEBPACK_IMPORTED_MODULE_8__.ButtonIconTitleComponent,
            _tabs_tabs_component__WEBPACK_IMPORTED_MODULE_10__.TabsComponent,
            _book_appointment_book_appointment_component__WEBPACK_IMPORTED_MODULE_11__.BookAppointmentComponent,
            _profile_avatar_profile_avatar_component__WEBPACK_IMPORTED_MODULE_1__.ProfileAvatarComponent,
            _header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent,
            _footer_footer_component__WEBPACK_IMPORTED_MODULE_12__.FooterComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_15__.CommonModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_16__.FlexLayoutModule,
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_17__.MatTooltipModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_18__.MatIconModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_19__.MatButtonModule,
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_20__.PdfViewerModule,
            _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_21__.MatProgressSpinnerModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_22__.ReactiveFormsModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_23__.MatFormFieldModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_24__.MatSelectModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_25__.MatInputModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_26__.MatCheckboxModule,
            _material_design__WEBPACK_IMPORTED_MODULE_9__.MaterialDesignModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_27__.IonicModule
        ],
        exports: [
            _appointment_doc_appointment_doc_component__WEBPACK_IMPORTED_MODULE_3__.AppointmentDocComponent,
            _preview_download__WEBPACK_IMPORTED_MODULE_2__.PreviewDownloadComponent,
            _avatar_avatar_component__WEBPACK_IMPORTED_MODULE_4__.AvatarComponent,
            _button_button_component__WEBPACK_IMPORTED_MODULE_6__.ButtonComponent,
            _button_icon_button_icon_component__WEBPACK_IMPORTED_MODULE_7__.ButtonIconComponent,
            _button_icon_title_button_icon_title_component__WEBPACK_IMPORTED_MODULE_8__.ButtonIconTitleComponent,
            _tabs_tabs_component__WEBPACK_IMPORTED_MODULE_10__.TabsComponent,
            _book_appointment_book_appointment_component__WEBPACK_IMPORTED_MODULE_11__.BookAppointmentComponent,
            _profile_avatar_profile_avatar_component__WEBPACK_IMPORTED_MODULE_1__.ProfileAvatarComponent,
            _header_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent,
            _footer_footer_component__WEBPACK_IMPORTED_MODULE_12__.FooterComponent
        ]
    })
], SharedComponentsModule);



/***/ }),

/***/ 7119:
/*!*****************************************************************!*\
  !*** ./src/app/shared/shared-components/tabs/tabs.component.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TabsComponent": () => (/* binding */ TabsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _tabs_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tabs.component.html?ngResource */ 83858);
/* harmony import */ var _tabs_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tabs.component.scss?ngResource */ 58716);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services/auth.service */ 90263);










let TabsComponent = class TabsComponent {
    constructor(_router, navCtrl, _as) {
        this._router = _router;
        this.navCtrl = navCtrl;
        this._as = _as;
        this.chatURL = '';
        this.dispatchView = false;
        this.isPackageSubscribed = false;
        this.loading = false;
        this.routeIDs = src_app_common_constants__WEBPACK_IMPORTED_MODULE_2__.RouteIDs;
        this.closed$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
        this.showTabs = true; // <-- show tabs by default
        this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_4__.Subject();
        this._me$ = this._as.getMe();
    }
    ngOnInit() {
        this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.takeUntil)(this._notifier$)).subscribe((user) => {
            this.me = user;
            this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
            this.subscribedPackage = this.me.subscribedPackage;
            this.dispatchView = true;
        });
    }
    goto(path) {
        this.path = path;
        this._router.navigate([path]);
        console.log(this.path);
    }
    ngOnDestroy() {
        this.closed$.next(); // <-- close subscription when component is destroyed
    }
};
TabsComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.NavController },
    { type: src_app_core_services_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthService }
];
TabsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-tabs',
        template: _tabs_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_tabs_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], TabsComponent);



/***/ }),

/***/ 74355:
/*!*********************************************!*\
  !*** ./src/app/shared/upload-file/index.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadFileModule": () => (/* reexport safe */ _upload_file_module__WEBPACK_IMPORTED_MODULE_0__.UploadFileModule)
/* harmony export */ });
/* harmony import */ var _upload_file_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./upload-file.module */ 32981);



/***/ }),

/***/ 9817:
/*!*************************************************************!*\
  !*** ./src/app/shared/upload-file/upload-file.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadFileComponent": () => (/* binding */ UploadFileComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _upload_file_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./upload-file.component.html?ngResource */ 52804);
/* harmony import */ var _upload_file_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./upload-file.component.scss?ngResource */ 14679);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/services */ 98138);





let UploadFileComponent = class UploadFileComponent {
    constructor(_sds, _sb, _sts) {
        this._sds = _sds;
        this._sb = _sb;
        this._sts = _sts;
        this.deleting = false;
        this.fileKey = '';
        this.filePath = '';
        this.fileURL = '';
        this.uploading = false;
        this.fileName = '';
        this.isVisible = false;
        this.uploadByName = '';
        this.uploadByUID = '';
        this.uploadCollectionName = '';
        this.uploadDir = '';
        this.userMembershipKey = '';
        this.userName = '';
        this.userUID = '';
        this.onUploadComplete = new _angular_core__WEBPACK_IMPORTED_MODULE_3__.EventEmitter();
        this.onDelete = new _angular_core__WEBPACK_IMPORTED_MODULE_3__.EventEmitter();
    }
    ngOnInit() { }
    ngOnChanges() {
        this.reset();
    }
    reset() {
        this.fileKey = '';
        this.filePath = '';
        this.fileURL = '';
        delete this.uploadPercentage$;
        delete this.uploadState$;
    }
};
UploadFileComponent.ctorParameters = () => [
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_2__.StaticDataService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_2__.SnackBarService },
    { type: src_app_core_services__WEBPACK_IMPORTED_MODULE_2__.StorageService }
];
UploadFileComponent.propDecorators = {
    file: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    fileName: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    isVisible: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    uploadByName: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    uploadByUID: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    uploadCollectionName: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    uploadDir: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    userMembershipKey: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    userName: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    userUID: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    onUploadComplete: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Output }],
    onDelete: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Output }]
};
UploadFileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-upload-file',
        template: _upload_file_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_upload_file_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], UploadFileComponent);



/***/ }),

/***/ 32981:
/*!**********************************************************!*\
  !*** ./src/app/shared/upload-file/upload-file.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UploadFileModule": () => (/* binding */ UploadFileModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _layout_layout_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../layout/layout.module */ 68634);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-bar */ 51294);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ng2-pdf-viewer */ 63940);
/* harmony import */ var _upload_file_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./upload-file.component */ 9817);
/* harmony import */ var _shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared-components/shared-components.module */ 89366);










let UploadFileModule = class UploadFileModule {
};
UploadFileModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        declarations: [_upload_file_component__WEBPACK_IMPORTED_MODULE_1__.UploadFileComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_6__.MatProgressBarModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_7__.MatButtonModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__.MatIconModule,
            ng2_pdf_viewer__WEBPACK_IMPORTED_MODULE_9__.PdfViewerModule,
            _layout_layout_module__WEBPACK_IMPORTED_MODULE_0__.LayoutModule,
            _shared_components_shared_components_module__WEBPACK_IMPORTED_MODULE_2__.SharedComponentsModule
        ],
        exports: [_upload_file_component__WEBPACK_IMPORTED_MODULE_1__.UploadFileComponent]
    })
], UploadFileModule);



/***/ }),

/***/ 92340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'AIzaSyAxZm9cK9I3M0P31Iy7llWZE0dJkIg0PlU',
        authDomain: 'meal-pyramid-dev.firebaseapp.com',
        projectId: 'meal-pyramid-dev',
        storageBucket: 'meal-pyramid-dev.appspot.com',
        messagingSenderId: '294748298017',
        appId: '1:294748298017:web:0819fb1bdbbd87ee1ebc60'
    },
    api: {
        baseURL: 'https://us-central1-meal-pyramid-dev.cloudfunctions.net/app'
    },
    staff: {
        uid_nm2RJCW6lnPLJuNjFJWdn3B5cXx1: {
            name: 'Kinita Kadakia Patel',
            photoURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/displayPictures%2Fnm2RJCW6lnPLJuNjFJWdn3B5cXx1%2Fdp-kinita.png?alt=media&token=8f0ccbf9-ffbe-4ec0-a899-ec20051fa7fd'
        },
        uid_TVUvv16dBZhP4tSS4bPLCwVGnci2: {
            name: 'Nutritionist1',
            photoURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/displayPictures%2FTVUvv16dBZhP4tSS4bPLCwVGnci2%2Fdp-nutritionist1.png?alt=media&token=1e37fc42-1c8e-4889-9321-5a511892ee4d'
        },
        uid_mocIhriAWSOoo8gmwmsXwi6CJpT2: {
            name: 'Lincia Creado',
            photoURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/displayPictures%2FmocIhriAWSOoo8gmwmsXwi6CJpT2%2Fdp-lincia.png?alt=media&token=e9706a57-ab32-4072-b2a6-fd76b3f5a009'
        }
    },
    avatar: {
        directory: 'displayPictures',
        userMale: 'assets/images/avatar-user-male.svg',
        userFemale: 'assets/images/avatar-user-female.svg'
    },
    // payU: {
    //   baseURL: 'https://sandboxsecure.payu.in/_payment',
    //   merchantKey: 'rWYGsF4x',
    //   successURL:
    //     'https://us-central1-stunning-prism-221707.cloudfunctions.net/app/payment/success',
    //   failureURL:
    //     'https://us-central1-stunning-prism-221707.cloudfunctions.net/app/payment/failure'
    // },
    payment: {
        currency: 'INR',
        razorPay: {
            key: 'rzp_test_UEpkoReBkIhjZr'
        }
    },
    bodyMeasurementGuide: {
        imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fbody-measurement-guide.jpg?alt=media&token=b3ef3785-6e70-4f59-9193-87a10656fd25'
    },
    photoLogGuide: {
        imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fbody-photo.jpg?alt=media&token=06811871-9209-497a-a571-39409a41a4f6'
    },
    heroSection: {
        kinita: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fkinita-hero-section.jpeg?alt=media&token=8edd946f-4b00-439d-92bd-a2e6f4e8a6ee'
    },
    support: {
        chatURL: 'https://wa.me/919892686118'
    },
    login: {
        bg1: {
            imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-1.jpeg?alt=media&token=e0a52c2f-d679-470b-b860-384cbb87cedd'
        },
        bg2: {
            imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-2.jpeg?alt=media&token=f8fceb39-12af-4ea3-9d08-54b05a907038'
        },
        bg3: {
            imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-3.jpeg?alt=media&token=b51c339d-2cad-4fd9-9d68-1332540ff2fc'
        },
        bg4: {
            imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-4.jpeg?alt=media&token=3b7e3ea2-cc61-4113-b5df-42dffaca3ec6'
        },
        bg5: {
            imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-5.jpeg?alt=media&token=f5b680ef-d737-442f-824c-36b61c759f0c'
        },
        bg6: {
            imgURL: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-6.jpeg?alt=media&token=40a3b13c-6a85-406f-99b9-743d925669b9'
        }
    },
    video: {
        intro: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fintro-video.mp4?alt=media&token=4171c35d-d049-41a5-a3c3-f1e542c68481',
        welcome: 'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fwelcome-video.mp4?alt=media&token=42e2431b-8249-4773-b1dc-fc9098f2e71a'
    },
    gst: {
        percentage: 18,
        multiplier: 1.18
    }
};
// baseURL: 'https://us-central1-stunning-prism-221707.cloudfunctions.net/app'
// baseURL: 'http://localhost:5000/stunning-prism-221707/us-central1/app'
// baseURL: 'https://us-central1-meal-pyramid-dev.cloudfunctions.net/app'
// baseURL: 'http://localhost:5000/meal-pyramid-dev/us-central1/app'


/***/ }),

/***/ 14431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 76057);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 36747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 92340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-accordion_2.entry.js": [
		70079,
		"common",
		"node_modules_ionic_core_dist_esm_ion-accordion_2_entry_js"
	],
	"./ion-action-sheet.entry.js": [
		25593,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		13225,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		4812,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		86655,
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		44856,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		13059,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-breadcrumb_2.entry.js": [
		58648,
		"common",
		"node_modules_ionic_core_dist_esm_ion-breadcrumb_2_entry_js"
	],
	"./ion-button_2.entry.js": [
		98308,
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		44690,
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		64090,
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		36214,
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		69447,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime-button.entry.js": [
		17950,
		"default-node_modules_ionic_core_dist_esm_data-cb72448c_js-node_modules_ionic_core_dist_esm_th-29e28e",
		"node_modules_ionic_core_dist_esm_ion-datetime-button_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		79689,
		"default-node_modules_ionic_core_dist_esm_data-cb72448c_js-node_modules_ionic_core_dist_esm_th-29e28e",
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		18840,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		40749,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		69667,
		"common",
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		83288,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		35473,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		53634,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		22855,
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		495,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		58737,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		99632,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-picker-column-internal.entry.js": [
		54446,
		"common",
		"node_modules_ionic_core_dist_esm_ion-picker-column-internal_entry_js"
	],
	"./ion-picker-internal.entry.js": [
		32275,
		"node_modules_ionic_core_dist_esm_ion-picker-internal_entry_js"
	],
	"./ion-popover.entry.js": [
		48050,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		18994,
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		23592,
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		35454,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		290,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		92666,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		64816,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		45534,
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		94902,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		91938,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		78179,
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		90668,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		61624,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		19989,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		28902,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		70199,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		48395,
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		96357,
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		38268,
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		15269,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		32875,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 46700:
/*!***************************************************!*\
  !*** ./node_modules/moment/locale/ sync ^\.\/.*$ ***!
  \***************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./af": 58685,
	"./af.js": 58685,
	"./ar": 254,
	"./ar-dz": 4312,
	"./ar-dz.js": 4312,
	"./ar-kw": 32614,
	"./ar-kw.js": 32614,
	"./ar-ly": 18630,
	"./ar-ly.js": 18630,
	"./ar-ma": 28674,
	"./ar-ma.js": 28674,
	"./ar-sa": 49032,
	"./ar-sa.js": 49032,
	"./ar-tn": 24730,
	"./ar-tn.js": 24730,
	"./ar.js": 254,
	"./az": 53052,
	"./az.js": 53052,
	"./be": 60150,
	"./be.js": 60150,
	"./bg": 63069,
	"./bg.js": 63069,
	"./bm": 13466,
	"./bm.js": 13466,
	"./bn": 18516,
	"./bn-bd": 90557,
	"./bn-bd.js": 90557,
	"./bn.js": 18516,
	"./bo": 26273,
	"./bo.js": 26273,
	"./br": 9588,
	"./br.js": 9588,
	"./bs": 19815,
	"./bs.js": 19815,
	"./ca": 83331,
	"./ca.js": 83331,
	"./cs": 21320,
	"./cs.js": 21320,
	"./cv": 72219,
	"./cv.js": 72219,
	"./cy": 68266,
	"./cy.js": 68266,
	"./da": 66427,
	"./da.js": 66427,
	"./de": 67435,
	"./de-at": 52871,
	"./de-at.js": 52871,
	"./de-ch": 12994,
	"./de-ch.js": 12994,
	"./de.js": 67435,
	"./dv": 82357,
	"./dv.js": 82357,
	"./el": 95649,
	"./el.js": 95649,
	"./en-au": 59961,
	"./en-au.js": 59961,
	"./en-ca": 19878,
	"./en-ca.js": 19878,
	"./en-gb": 3924,
	"./en-gb.js": 3924,
	"./en-ie": 70864,
	"./en-ie.js": 70864,
	"./en-il": 91579,
	"./en-il.js": 91579,
	"./en-in": 30940,
	"./en-in.js": 30940,
	"./en-nz": 16181,
	"./en-nz.js": 16181,
	"./en-sg": 44301,
	"./en-sg.js": 44301,
	"./eo": 85291,
	"./eo.js": 85291,
	"./es": 54529,
	"./es-do": 53764,
	"./es-do.js": 53764,
	"./es-mx": 12584,
	"./es-mx.js": 12584,
	"./es-us": 63425,
	"./es-us.js": 63425,
	"./es.js": 54529,
	"./et": 35203,
	"./et.js": 35203,
	"./eu": 70678,
	"./eu.js": 70678,
	"./fa": 83483,
	"./fa.js": 83483,
	"./fi": 96262,
	"./fi.js": 96262,
	"./fil": 52521,
	"./fil.js": 52521,
	"./fo": 34555,
	"./fo.js": 34555,
	"./fr": 63131,
	"./fr-ca": 88239,
	"./fr-ca.js": 88239,
	"./fr-ch": 21702,
	"./fr-ch.js": 21702,
	"./fr.js": 63131,
	"./fy": 267,
	"./fy.js": 267,
	"./ga": 23821,
	"./ga.js": 23821,
	"./gd": 71753,
	"./gd.js": 71753,
	"./gl": 4074,
	"./gl.js": 4074,
	"./gom-deva": 92762,
	"./gom-deva.js": 92762,
	"./gom-latn": 5969,
	"./gom-latn.js": 5969,
	"./gu": 82809,
	"./gu.js": 82809,
	"./he": 45402,
	"./he.js": 45402,
	"./hi": 315,
	"./hi.js": 315,
	"./hr": 10410,
	"./hr.js": 10410,
	"./hu": 38288,
	"./hu.js": 38288,
	"./hy-am": 67928,
	"./hy-am.js": 67928,
	"./id": 71334,
	"./id.js": 71334,
	"./is": 86959,
	"./is.js": 86959,
	"./it": 34864,
	"./it-ch": 51124,
	"./it-ch.js": 51124,
	"./it.js": 34864,
	"./ja": 36141,
	"./ja.js": 36141,
	"./jv": 29187,
	"./jv.js": 29187,
	"./ka": 42136,
	"./ka.js": 42136,
	"./kk": 94332,
	"./kk.js": 94332,
	"./km": 18607,
	"./km.js": 18607,
	"./kn": 84305,
	"./kn.js": 84305,
	"./ko": 70234,
	"./ko.js": 70234,
	"./ku": 16003,
	"./ku.js": 16003,
	"./ky": 75061,
	"./ky.js": 75061,
	"./lb": 32786,
	"./lb.js": 32786,
	"./lo": 66183,
	"./lo.js": 66183,
	"./lt": 50029,
	"./lt.js": 50029,
	"./lv": 24169,
	"./lv.js": 24169,
	"./me": 68577,
	"./me.js": 68577,
	"./mi": 68177,
	"./mi.js": 68177,
	"./mk": 50337,
	"./mk.js": 50337,
	"./ml": 65260,
	"./ml.js": 65260,
	"./mn": 52325,
	"./mn.js": 52325,
	"./mr": 14695,
	"./mr.js": 14695,
	"./ms": 75334,
	"./ms-my": 37151,
	"./ms-my.js": 37151,
	"./ms.js": 75334,
	"./mt": 63570,
	"./mt.js": 63570,
	"./my": 97963,
	"./my.js": 97963,
	"./nb": 88028,
	"./nb.js": 88028,
	"./ne": 86638,
	"./ne.js": 86638,
	"./nl": 50302,
	"./nl-be": 66782,
	"./nl-be.js": 66782,
	"./nl.js": 50302,
	"./nn": 33501,
	"./nn.js": 33501,
	"./oc-lnc": 50563,
	"./oc-lnc.js": 50563,
	"./pa-in": 50869,
	"./pa-in.js": 50869,
	"./pl": 65302,
	"./pl.js": 65302,
	"./pt": 49687,
	"./pt-br": 74884,
	"./pt-br.js": 74884,
	"./pt.js": 49687,
	"./ro": 79107,
	"./ro.js": 79107,
	"./ru": 33627,
	"./ru.js": 33627,
	"./sd": 30355,
	"./sd.js": 30355,
	"./se": 83427,
	"./se.js": 83427,
	"./si": 11848,
	"./si.js": 11848,
	"./sk": 54590,
	"./sk.js": 54590,
	"./sl": 20184,
	"./sl.js": 20184,
	"./sq": 56361,
	"./sq.js": 56361,
	"./sr": 78965,
	"./sr-cyrl": 81287,
	"./sr-cyrl.js": 81287,
	"./sr.js": 78965,
	"./ss": 25456,
	"./ss.js": 25456,
	"./sv": 70451,
	"./sv.js": 70451,
	"./sw": 77558,
	"./sw.js": 77558,
	"./ta": 51356,
	"./ta.js": 51356,
	"./te": 73693,
	"./te.js": 73693,
	"./tet": 21243,
	"./tet.js": 21243,
	"./tg": 42500,
	"./tg.js": 42500,
	"./th": 55768,
	"./th.js": 55768,
	"./tk": 77761,
	"./tk.js": 77761,
	"./tl-ph": 35780,
	"./tl-ph.js": 35780,
	"./tlh": 29590,
	"./tlh.js": 29590,
	"./tr": 33807,
	"./tr.js": 33807,
	"./tzl": 93857,
	"./tzl.js": 93857,
	"./tzm": 60654,
	"./tzm-latn": 8806,
	"./tzm-latn.js": 8806,
	"./tzm.js": 60654,
	"./ug-cn": 30845,
	"./ug-cn.js": 30845,
	"./uk": 19232,
	"./uk.js": 19232,
	"./ur": 47052,
	"./ur.js": 47052,
	"./uz": 77967,
	"./uz-latn": 32233,
	"./uz-latn.js": 32233,
	"./uz.js": 77967,
	"./vi": 98615,
	"./vi.js": 98615,
	"./x-pseudo": 12320,
	"./x-pseudo.js": 12320,
	"./yo": 31313,
	"./yo.js": 31313,
	"./zh-cn": 64490,
	"./zh-cn.js": 64490,
	"./zh-hk": 55910,
	"./zh-hk.js": 55910,
	"./zh-mo": 98262,
	"./zh-mo.js": 98262,
	"./zh-tw": 44223,
	"./zh-tw.js": 44223
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 46700;

/***/ }),

/***/ 79259:
/*!***********************************************!*\
  !*** ./src/app/app.component.scss?ngResource ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-tab-bar,\nion-tab-button {\n  background-color: #1f212c;\n}\n\nion-icon {\n  color: #ffffff;\n}\n\nion-tab-bar {\n  border-top-right-radius: 20px;\n  border-top-left-radius: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUF3SEE7O0VBRUUseUJBQUE7QUF2SEY7O0FBMEhBO0VBQ0UsY0FBQTtBQXZIRjs7QUEwSEE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0FBdkhGIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGlvbi1tZW51IHtcclxuLy8gICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMjcyOTM4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5tZCBpb24tY29udGVudCB7XHJcbi8vICAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XHJcbi8vICAgLS1wYWRkaW5nLWVuZDogOHB4O1xyXG4vLyAgIC0tcGFkZGluZy10b3A6IDIwcHg7XHJcbi8vICAgLS1wYWRkaW5nLWJvdHRvbTogMjBweDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUubWQgaW9uLWxpc3Qge1xyXG4vLyAgIHBhZGRpbmc6IDIwcHggMDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUubWQgaW9uLW5vdGUge1xyXG4vLyAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbi8vIH1cclxuXHJcbi8vIGlvbi1tZW51Lm1kIGlvbi1saXN0LWhlYWRlcixcclxuLy8gaW9uLW1lbnUubWQgaW9uLW5vdGUge1xyXG4vLyAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUubWQgaW9uLWxpc3QjaW5ib3gtbGlzdCB7XHJcbi8vICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1zdGVwLTE1MCwgI2Q3ZDhkYSk7XHJcbi8vIH1cclxuXHJcbi8vIGlvbi1tZW51Lm1kIGlvbi1saXN0I2luYm94LWxpc3QgaW9uLWxpc3QtaGVhZGVyIHtcclxuLy8gICBmb250LXNpemU6IDIycHg7XHJcbi8vICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuXHJcbi8vICAgbWluLWhlaWdodDogMjBweDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUubWQgaW9uLWxpc3QjbGFiZWxzLWxpc3QgaW9uLWxpc3QtaGVhZGVyIHtcclxuLy8gICBmb250LXNpemU6IDE2cHg7XHJcblxyXG4vLyAgIG1hcmdpbi1ib3R0b206IDE4cHg7XHJcblxyXG4vLyAgIGNvbG9yOiAjNzU3NTc1O1xyXG5cclxuLy8gICBtaW4taGVpZ2h0OiAyNnB4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5tZCBpb24taXRlbSB7XHJcbi8vICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xyXG4vLyAgIC0tcGFkZGluZy1lbmQ6IDEwcHg7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCB7XHJcbi8vICAgLS1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXJnYiksIDAuMTQpO1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCBpb24taWNvbiB7XHJcbi8vICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUubWQgaW9uLWl0ZW0gaW9uLWljb24ge1xyXG4vLyAgIGNvbG9yOiAjZWVlZTtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUubWQgaW9uLWl0ZW0gaW9uLWxhYmVsIHtcclxuLy8gICBmb250LXdlaWdodDogNTAwO1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5pb3MgaW9uLWNvbnRlbnQge1xyXG4vLyAgIC0tcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbi8vIH1cclxuXHJcbi8vIGlvbi1tZW51LmlvcyBpb24tbGlzdCB7XHJcbi8vICAgcGFkZGluZzogMjBweCAwIDAgMDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUuaW9zIGlvbi1ub3RlIHtcclxuLy8gICBsaW5lLWhlaWdodDogMjRweDtcclxuLy8gICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5pb3MgaW9uLWl0ZW0ge1xyXG4vLyAgIC0tcGFkZGluZy1zdGFydDogMTZweDtcclxuLy8gICAtLXBhZGRpbmctZW5kOiAxNnB4O1xyXG4vLyAgIC0tbWluLWhlaWdodDogNTBweDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUuaW9zIGlvbi1pdGVtLnNlbGVjdGVkIGlvbi1pY29uIHtcclxuLy8gICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5pb3MgaW9uLWl0ZW0gaW9uLWljb24ge1xyXG4vLyAgIGZvbnQtc2l6ZTogMjRweDtcclxuLy8gICBjb2xvcjogIzczODQ5YTtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW1lbnUuaW9zIGlvbi1saXN0I2xhYmVscy1saXN0IGlvbi1saXN0LWhlYWRlciB7XHJcbi8vICAgbWFyZ2luLWJvdHRvbTogOHB4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5pb3MgaW9uLWxpc3QtaGVhZGVyLFxyXG4vLyBpb24tbWVudS5pb3MgaW9uLW5vdGUge1xyXG4vLyAgIHBhZGRpbmctbGVmdDogMTZweDtcclxuLy8gICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tbWVudS5pb3MgaW9uLW5vdGUge1xyXG4vLyAgIG1hcmdpbi1ib3R0b206IDhweDtcclxuLy8gfVxyXG5cclxuLy8gaW9uLW5vdGUge1xyXG4vLyAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gICBmb250LXNpemU6IDE2cHg7XHJcblxyXG4vLyAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcclxuLy8gfVxyXG5cclxuLy8gaW9uLWl0ZW0uc2VsZWN0ZWQge1xyXG4vLyAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuLy8gfVxyXG5cclxuaW9uLXRhYi1iYXIsXHJcbmlvbi10YWItYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWYyMTJjO1xyXG59XHJcblxyXG5pb24taWNvbiB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuXHJcbmlvbi10YWItYmFyIHtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMjBweDtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyMHB4O1xyXG59Il19 */";

/***/ }),

/***/ 57783:
/*!************************************************!*\
  !*** ./src/app/auth/auth.page.scss?ngResource ***!
  \************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhdXRoLnBhZ2Uuc2NzcyJ9 */";

/***/ }),

/***/ 83488:
/*!************************************************************!*\
  !*** ./src/app/auth/login/login.component.scss?ngResource ***!
  \************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-content {\n  top: 10%;\n  left: 0%;\n  right: 50%;\n  margin-top: 0;\n  margin-left: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUUsUUFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUFBRiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAxMCU7XHJcbiAgbGVmdDogMCU7XHJcbiAgcmlnaHQ6IDUwJTtcclxuICBtYXJnaW4tdG9wOiAwO1xyXG4gIG1hcmdpbi1sZWZ0OiAwO1xyXG59Il19 */";

/***/ }),

/***/ 98041:
/*!******************************************************************!*\
  !*** ./src/app/auth/register/register.component.scss?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-content {\n  position: absolute;\n  top: 2%;\n  left: 0%;\n  right: 50%;\n  margin: 0 auto;\n  margin-top: 0;\n  margin-left: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUFDRiIsImZpbGUiOiJyZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAyJTtcclxuICBsZWZ0OiAwJTtcclxuICByaWdodDogNTAlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDA7XHJcbiAgbWFyZ2luLWxlZnQ6IDA7XHJcbn0iXX0= */";

/***/ }),

/***/ 54076:
/*!**********************************************************!*\
  !*** ./src/app/home/feed/feed.component.scss?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmZWVkLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 11832:
/*!*****************************************************!*\
  !*** ./src/app/home/home.component.scss?ngResource ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
module.exports = ".cards {\n  background-color: #498ac6;\n}\n\n.wh-txt {\n  color: white;\n}\n\n.iframe_pramod {\n  top: -65px;\n  position: absolute;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBQTtBQUNGOztBQUVBO0VBQ0UsWUFBQTtBQUNGOztBQUVBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0FBQ0YiLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkcyB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQ5OGFjNjtcclxufVxyXG5cclxuLndoLXR4dCB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4uaWZyYW1lX3ByYW1vZCB7XHJcbiAgdG9wOiAtNjVweDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuXHJcbiJdfQ== */";

/***/ }),

/***/ 89125:
/*!***************************************************************************!*\
  !*** ./src/app/pages/notification/notification.component.scss?ngResource ***!
  \***************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJub3RpZmljYXRpb24uY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 37490:
/*!*******************************************************!*\
  !*** ./src/app/pages/pages.component.scss?ngResource ***!
  \*******************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYWdlcy5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 73887:
/*!***********************************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/appointment/appointment.component.scss?ngResource ***!
  \***********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBvaW50bWVudC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 96659:
/*!********************************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/buy-advanced-testing.component.scss?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJidXktYWR2YW5jZWQtdGVzdGluZy5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 17211:
/*!***************************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/payment/payment.component.scss?ngResource ***!
  \***************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYXltZW50LmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 31310:
/*!************************************************************************!*\
  !*** ./src/app/shared/dialog-box/dialog-box.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkaWFsb2ctYm94LmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 99562:
/*!**************************************************************************************!*\
  !*** ./src/app/shared/get-started/appointment/appointment.component.scss?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBvaW50bWVudC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 16213:
/*!**************************************************************************!*\
  !*** ./src/app/shared/get-started/get-started.component.scss?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJnZXQtc3RhcnRlZC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 93018:
/*!********************************************************************************************!*\
  !*** ./src/app/shared/get-started/health-history/health-history.component.scss?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFsdGgtaGlzdG9yeS5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 35113:
/*!******************************************************************************!*\
  !*** ./src/app/shared/get-started/payment/payment.component.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYXltZW50LmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 65070:
/*!************************************************************************************************!*\
  !*** ./src/app/shared/get-started/personal-details/personal-details.component.scss?ngResource ***!
  \************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwZXJzb25hbC1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 12379:
/*!****************************************************************************************!*\
  !*** ./src/app/shared/input-file-preview/input-file-preview.component.scss?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJpbnB1dC1maWxlLXByZXZpZXcuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 47304:
/*!****************************************************************!*\
  !*** ./src/app/shared/layout/layout.component.scss?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 43273:
/*!****************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/appointment-doc/appointment-doc.component.scss?ngResource ***!
  \****************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBvaW50bWVudC1kb2MuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 60679:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/avatar/avatar.component.scss?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhdmF0YXIuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 26834:
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/book-appointment/book-appointment.component.scss?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJib29rLWFwcG9pbnRtZW50LmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 42023:
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/button-icon-title/button-icon-title.component.scss?ngResource ***!
  \********************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJidXR0b24taWNvbi10aXRsZS5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 41127:
/*!********************************************************************************************!*\
  !*** ./src/app/shared/shared-components/button-icon/button-icon.component.scss?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJidXR0b24taWNvbi5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 54075:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/button/button.component.scss?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJidXR0b24uY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 7555:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/footer/footer.component.scss?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-tab-bar,\nion-tab-button {\n  background-color: #1f212c;\n}\n\nion-icon {\n  color: #ffffff;\n}\n\nion-tab-bar {\n  border-top-right-radius: 20px;\n  border-top-left-radius: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvb3Rlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFFRSx5QkFBQTtBQUNGOztBQUVBO0VBQ0UsY0FBQTtBQUNGOztBQUVBO0VBQ0UsNkJBQUE7RUFDQSw0QkFBQTtBQUNGIiwiZmlsZSI6ImZvb3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10YWItYmFyLFxyXG5pb24tdGFiLWJ1dHRvbiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzFmMjEyYztcclxufVxyXG5cclxuaW9uLWljb24ge1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcblxyXG5pb24tdGFiLWJhciB7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDIwcHg7XHJcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjBweDtcclxufSJdfQ== */";

/***/ }),

/***/ 89537:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/header/header.component.scss?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 49749:
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/preview-download/preview-download.component.scss?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcmV2aWV3LWRvd25sb2FkLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 63445:
/*!**************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/profile-avatar/profile-avatar.component.scss?ngResource ***!
  \**************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9maWxlLWF2YXRhci5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 55943:
/*!************************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/recommended-package/recommended-package.component.scss?ngResource ***!
  \************************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWNvbW1lbmRlZC1wYWNrYWdlLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 58716:
/*!******************************************************************************!*\
  !*** ./src/app/shared/shared-components/tabs/tabs.component.scss?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-tab-bar,\nion-tab-button {\n  background-color: #1f212c;\n}\n\nion-icon {\n  color: #ffffff;\n}\n\nion-tab-bar {\n  border-top-right-radius: 20px;\n  border-top-left-radius: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUseUJBQUE7QUFDRjs7QUFFQTtFQUNFLGNBQUE7QUFDRjs7QUFFQTtFQUNFLDZCQUFBO0VBQ0EsNEJBQUE7QUFDRiIsImZpbGUiOiJ0YWJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRhYi1iYXIsXHJcbmlvbi10YWItYnV0dG9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWYyMTJjO1xyXG59XHJcblxyXG5pb24taWNvbiB7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbn1cclxuXHJcbmlvbi10YWItYmFyIHtcclxuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMjBweDtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyMHB4O1xyXG59Il19 */";

/***/ }),

/***/ 14679:
/*!**************************************************************************!*\
  !*** ./src/app/shared/upload-file/upload-file.component.scss?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1cGxvYWQtZmlsZS5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 33383:
/*!***********************************************!*\
  !*** ./src/app/app.component.html?ngResource ***!
  \***********************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-app>\r\n  <ion-content>\r\n    <ion-router-outlet></ion-router-outlet>\r\n  </ion-content>\r\n\r\n  <!-- <ion-footer>\r\n    <h1 *ngIf=\"show_footer\">sssss</h1>\r\n  </ion-footer> -->\r\n</ion-app>";

/***/ }),

/***/ 2708:
/*!************************************************!*\
  !*** ./src/app/auth/auth.page.html?ngResource ***!
  \************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\r\n <ion-app>\r\n  <ion-router-outlet></ion-router-outlet>\r\n </ion-app>\r\n \r\n";

/***/ }),

/***/ 85639:
/*!************************************************************!*\
  !*** ./src/app/auth/login/login.component.html?ngResource ***!
  \************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-app>\r\n  <ion-content class=\"ion-padding eh-py-0\">\r\n    <div class=\"eh-w-100p eh-mt-8 eh-pt-10\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n      <div class=\"eh-w-120\">\r\n        <img class=\"eh-fit\" src=\"../../../../../assets/images/mp-logo.jpg\" alt=\"mp-logo\">\r\n      </div>\r\n    </div>\r\n    <div fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n      <ng-container *ngIf=\"showLogin \" [formGroup]=\"loginFormGroup\">\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-mt-24\">\r\n          <mat-label class=\"eh-txt-16\">Email</mat-label>\r\n          <input matInput formControlName=\"email\" placeholder=\"my@email.com\">\r\n          <mat-icon matSuffix>email</mat-icon>\r\n        </mat-form-field>\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n          <mat-label class=\"eh-txt-16\">Password</mat-label>\r\n          <!-- <input matInput formControlName=\"password\" type=\"password\">\r\n        <mat-icon matSuffix>vpn_key</mat-icon> -->\r\n          <input matInput formControlName=\"password\" [type]=\"passwordFormControlType\">\r\n          <mat-icon matSuffix class=\"eh-csr-ptr e-txt-primary\" (click)=\"onTogglePasswordView()\">\r\n            {{passwordFormControlIcon}}\r\n          </mat-icon>\r\n        </mat-form-field>\r\n\r\n        <div class=\"eh-w-100p eh-mt-8 eh-txt-right\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n          <a class=\"eh-txt-underline e-txt e-txt--eblue700 eh-txt-14\" href=\"javascript: void(0);\"\r\n            (click)=\"toggleViewPassword()\">\r\n            Forgot Password\r\n          </a>\r\n        </div>\r\n        <div class=\"eh-mt-16 eh-w-50p\">\r\n          <app-button buttonStyle=\"e-mat-button--eblue400 eh-txt-16\" title=\"Login\" [loading]=\"loading\"\r\n            (onButtonClick)=\"onLogin()\">\r\n          </app-button>\r\n        </div>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"!showLogin\" [formGroup]=\"forgotPassFG\">\r\n        <div class=\"eh-txt-center e-txt e-txt--eblue700 eh-mt-12\">Forgot Password</div>\r\n\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-mt-4\">\r\n          <input matInput formControlName=\"email\" type=\"email\" placeholder=\"Email\">\r\n          <mat-icon matSuffix>email</mat-icon>\r\n        </mat-form-field>\r\n\r\n        <div class=\"eh-mt-10 eh-w-60p\">\r\n          <app-button buttonStyle=\"e-mat-button--eblue400\" title=\"Get Password Reset Email\"\r\n            [loading]=\"sendingForgotPassEmail\" (onButtonClick)=\"onForgotPass()\">\r\n          </app-button>\r\n        </div>\r\n\r\n        <div class=\"eh-w-60p eh-mt-8 eh-txt-center\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n          <a class=\"eh-txt-underline e-txt eh-txt-14 e-txt--eblue700\" href=\"javascript: void(0);\"\r\n            (click)=\"toggleViewPassword()\">\r\n            Login\r\n          </a>\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n  </ion-content>\r\n</ion-app>";

/***/ }),

/***/ 64966:
/*!******************************************************************!*\
  !*** ./src/app/auth/register/register.component.html?ngResource ***!
  \******************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-content class=\"ion-padding\">\r\n\r\n  <div class=\"eh-w-100p eh-mt-8\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n    <div class=\"eh-w-120\">\r\n      <img class=\"eh-fit\" src=\"../../../../../assets/images/mp-logo.jpg\" alt=\"\">\r\n    </div>\r\n  </div>\r\n  <form [formGroup]=\"registerFormGroup\" autocomplete=\"off\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n    <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-mt-24\">\r\n      <mat-label>First Name</mat-label>\r\n      <input matInput formControlName=\"firstName\" placeholder=\"My first name\">\r\n      <mat-icon matSuffix>person</mat-icon>\r\n    </mat-form-field>\r\n    <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n      <mat-label>Last Name</mat-label>\r\n      <input matInput formControlName=\"lastName\" placeholder=\"My last name\">\r\n      <mat-icon matSuffix>person</mat-icon>\r\n    </mat-form-field>\r\n    <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n      <mat-label>Email<sup>**</sup>\r\n      </mat-label>\r\n      <input matInput formControlName=\"email\" placeholder=\"my@email.com\" type=\"email\">\r\n      <mat-icon matSuffix>email</mat-icon>\r\n    </mat-form-field>\r\n    <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n      <mat-label>Password</mat-label>\r\n      <input matInput formControlName=\"password\" [type]=\"passwordFormControlType\">\r\n      <mat-icon matSuffix class=\"eh-csr-ptr e-txt-primary\" (click)=\"onTogglePasswordView()\">\r\n        {{passwordFormControlIcon}}</mat-icon>\r\n    </mat-form-field>\r\n    <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n      <mat-label>Mobile</mat-label>\r\n      <input matInput formControlName=\"mobile\">\r\n      <mat-icon matSuffix>phone_iphone</mat-icon>\r\n      <mat-hint>+91-XXXXXXXXXX</mat-hint>\r\n    </mat-form-field>\r\n    <button *ngIf=\"!loading\" class=\"e-mat-button e-mat-button--eblue400 eh-w-60p eh-mt-16 eh-txt-16\" mat-raised-button\r\n      [disabled]=\"!registerFormGroup.valid\" (click)=\"onRegister()\">\r\n      Sign Up\r\n    </button>\r\n    <div *ngIf=\"loading\" class=\"e-spinner e-spinner--2 eh-w-100p eh-mt-16 eh-h-36\">\r\n      <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n    </div>\r\n  </form>\r\n  <div class=\"eh-mt-24 weh--100p eh-txt-center\">\r\n    Already have an account?<br>\r\n    <a class=\"eh-csr-ptr eh-txt-underline eh-grey eh-txt-14\" (click)=\"onShowLogin()\">Login now!</a>\r\n  </div>\r\n  <div class=\"eh-mt-24 eh-w-100p eh-txt-10 eh-txt-center\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n    style=\"line-height: 14px !important;\">\r\n    <i>\r\n      <sup>**</sup> Your email address is unique to the system. Once registered, this cannot be changed.\r\n    </i>\r\n  </div>\r\n\r\n</ion-content>";

/***/ }),

/***/ 91056:
/*!**********************************************************!*\
  !*** ./src/app/home/feed/feed.component.html?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-content overflow-scroll=\"true\">\r\n  <div class=\"e-card2 eh-h-100p\">\r\n    <div class=\"e-card2__header\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">rss_feed</mat-icon>\r\n      <span>Community Feed</span>\r\n    </div>\r\n\r\n    <div class=\"eh-p-16 eh-ofy-scroll eh-hide-scrollbars eh-pos-relative\">\r\n      <ng-container *ngIf=\"areFeedsAvailable; else feedsNotAvailableBlk\">\r\n        <div *ngFor=\"let item of feeds\" class=\"e-card2 eh-p-12 eh-mt-16 eh-t-spacer\">\r\n          <div fxLayout=\"row\">\r\n            <app-avatar [userUID]=\"item.staff.uid\" size=\"medium\" matTooltip=\"{{item.staff.name}}\"></app-avatar>\r\n\r\n            <div class=\"eh-ml-8\">\r\n              <div class=\"eh-txt-12\">{{item.staff.name}}</div>\r\n              <div class=\"eh-txt-12 e-txt e-txt--matgrey500\">{{now.to(item.publishedAt)}}</div>\r\n            </div>\r\n          </div>\r\n\r\n          <mat-divider class=\"e-mat-divider eh-mt-12\"></mat-divider>\r\n\r\n          <div *ngIf=\"item.mediaURL\" class=\"eh-mt-12 eh-w-252 eh-h-142\">\r\n            <img class=\"eh-fit\" [src]=\"item.mediaURL\" (click)=\"openLightbox(item.mediaURL)\">\r\n          </div>\r\n\r\n          <!-- <div class=\"eh-mt-16\" [innerHTML]=\"item.title\"></div> -->\r\n\r\n          <div class=\"eh-txt-12 eh-mt-8\" [innerHTML]=\"item.description\"></div>\r\n        </div>\r\n      </ng-container>\r\n\r\n      <ng-template #feedsNotAvailableBlk>\r\n        <div class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Check back soon for updates\r\n        </div>\r\n      </ng-template>\r\n    </div>\r\n  </div>";

/***/ }),

/***/ 64715:
/*!*****************************************************!*\
  !*** ./src/app/home/home.component.html?ngResource ***!
  \*****************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<app-header title=\"Home\"></app-header>\r\n<ion-content class=\"ion-padding\" *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div *ngIf=\"!showHasSubscribed && me && dispatchView \">\r\n    <div class=\"cards e-card2--round1 eh-p-10 eh-mb-6\">\r\n      <div class=\"eh-px-2\" *ngIf=\"me\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"space-between\">\r\n          <div>\r\n            <img class=\"eh-fit e-logo eh-mb-10 eh-mt-4 eh-p-18\" src=\"../../../../../assets/images/mp-logo.jpg\" alt=\"\">\r\n          </div>\r\n          <div class=\"eh-pl-6\">\r\n            <div class=\"wh-txt eh-txt-14 e-px-0\">Hello {{me.personalDetails.firstName}}, </div>\r\n            <div class=\"wh-txt eh-txt-12\">Please subcribe to our Packages to unlock the pro-features.</div>\r\n            <ion-chip style=\"background-color: white;\" color=\"secondary\">\r\n              <ion-icon name=\"globe-outline\" class=\"ion-no-padding\" color=\"secondary\"></ion-icon>\r\n              <ion-label class=\"ion-no-padding eh-txt-10 eh-txt-500\" color=\"secondary\"> <a\r\n                  href=\"https://dev-meal-pyramid-users.web.app\" color=\"secondary\" class=\"e-txt e-txt--eblue700 \">\r\n                  Upgrade Now\r\n                </a></ion-label>\r\n              <ion-icon name=\"arrow-forward-outline\" class=\"ion-no-padding\" color=\"secondary\"></ion-icon>\r\n            </ion-chip>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"isPackageSubscribed; else notSubscribedBlk\" class=\"e-card2 eh-mb-10\" fxFlex=\"1 0 0%\">\r\n    <div class=\"e-card2__header\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">assignment_turned_in</mat-icon>\r\n      <span>Package is Active</span>\r\n    </div>\r\n    <div class=\"eh-p-14\">\r\n      <div class=\"e-title e-title--2 eh-txt-bold\">{{subscribedPackage.name}} -\r\n        {{subscribedPackage.type}}\r\n      </div>\r\n      <div class=\"eh-mt-2 eh-txt-12\">\r\n        {{me.subscriptionDuration.start | date:'d-MMM-yyyy':'+0530'}} -\r\n        {{me.subscriptionDuration.end | date:'d-MMM-yyyy':'+0530'}}\r\n      </div>\r\n      <div class=\"eh-txt-12\" fxLayout=\"row\" fxLayoutAlign=\"start\">{{subscribedPackage.description}},</div>\r\n      <div class=\"eh-txt-12 eh-mb-2\" fxLayout=\"row\" fxLayoutAlign=\"start\">whichever finishes first</div>\r\n    </div>\r\n  </div>\r\n  <ng-template #notSubscribedBlk>\r\n    <div class=\"e-card2 eh-mb-10\" fxFlex=\"1 0 0%\">\r\n      <!-- <div>Package is Inactive</div> -->\r\n      <div class=\"e-card2__header\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">assignment_late</mat-icon>\r\n        <span>Package is Inactive</span>\r\n      </div>\r\n      <div class=\"eh-p-14\">\r\n        <div class=\"e-title e-title--2 eh-txt-bold\">\r\n          Purchase a package to activate PRO features\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n  <div *ngIf=\"me && dispatchView\">\r\n\r\n    <ng-container>\r\n      <div class=\"eh-my-2\">\r\n        <div class=\"e-card2 eh-p-4\">\r\n          <div class=\"e-card2__header eh-mb-8\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">keyboard_tab</mat-icon>\r\n            <span>Next step</span>\r\n          </div>\r\n          <ng-container *ngIf=\"showOnboardingIncomplete\">\r\n            <div class=\"eh-txt-12 eh-p-4\">You must complete the registration process to proceed forward.</div>\r\n            <button mat-stroked-button fxLayoutAlign=\"center\"\r\n              class=\"e-mat-button e-mat-button--eblue400 eh-mt-8 eh-w-50p\" (click)=\"onResumeOnboardingClick()\">\r\n              {{ me.flags.isOrientationPaid ? 'Schedule Orientation with Kinita' : 'Complete Registration' }}\r\n            </button>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"showWaitingOnOrientation\">\r\n            <ng-container *ngIf=\"!showDidNotAttendOrientation\">\r\n              <div class=\"eh-txt-12 eh-p-4\">\r\n                Your <span class=\"eh-txt-bold\">Orientation</span> has been scheduled with Kinita on:<br>\r\n                <b>{{orientationAppointment.appointment.sessionStart | date:'EEEE | h:mm a, d-MMM-y':'+0530'}}</b>\r\n                at the <b>{{orientationAppointment.appointment.location.name}}</b> location<br><br>\r\n                If you wish to reschedule, please <a class=\"e-txt e-txt--matamber600 eh-txt-bold\"\r\n                  href=\"javascript: void(0);\" (click)=\"onCancelOrientationClick()\">Cancel</a> your current\r\n                appointment & schedule a new one.\r\n              </div>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"showDidNotAttendOrientation\">\r\n              <span class=\"eh-txt-12 eh-p-4\">\r\n                Your Orientation session was scheduled in the past. Please contact our Support team for further\r\n                assistance.\r\n              </span>\r\n            </ng-container>\r\n          </ng-container>\r\n          <!-- <div class=\"eh-p-4\">\r\n              <ion-icon color=\"secondary\" class=\"eh-txt-14\" size=\"medium\" name=\"cloud-upload-outline\"></ion-icon>\r\n            </div> -->\r\n          <!-- <div class=\"eh-txt-14 eh-p-2\" (click)=\"onUploadClick()\">upload you\r\n                  <br> Pre-testing reports\r\n                </div> -->\r\n          <ng-container>\r\n            <ng-container *ngIf=\"showNewSubscriber\">\r\n              <div class=\"eh-txt-12 eh-p-4\">\r\n                Your <span class=\"eh-txt-bold\">Package</span> is <span class=\"eh-txt-bold\">Activated</span>.<br>\r\n                Please click <a class=\"e-txt e-txt--matamber600 eh-txt-bold\" href=\"javascript: void(0);\"\r\n                  routerLink=\"/appointments\">here</a> to schedule your first\r\n                appointment.\r\n              </div>\r\n            </ng-container>\r\n            <ng-container *ngIf=\"showRecommendations\">\r\n              <span class=\"eh-txt-12 eh-p-4\">\r\n                Post your orientation with Kinita, she has recommended a suitable Package for you.<br><br>\r\n              </span>\r\n              <span class=\"eh-txt-12 eh-p-4\">\r\n                Kindly <span class=\"eh-txt-bold\">purchase a Package</span> of your choice to activate your account.\r\n              </span>\r\n            </ng-container>\r\n            <ng-container *ngIf=\"showNextAppointmentAvailable && !showLastDiet\">\r\n              <div class=\"eh-txt-12 eh-p-4\">\r\n                Your next <span class=\"eh-txt-bold\">Recall Appointment</span> has been scheduled as follows:<br>\r\n                With: <u>{{nextAppointment.staff.name}}</u><br>\r\n                On: <u>{{nextAppointment.sessionStart | date:'EEEE | h:mm a, d-MMM-y':'+0530'}} at the\r\n                  {{nextAppointment.location.name}} location</u><br><br>\r\n                If you wish to reschedule, please click <a class=\"e-txt e-txt--matamber600 eh-txt-bold\"\r\n                  href=\"javascript: void(0);\" routerLink=\"/appointments\">here</a> to cancel your current\r\n                appointment & schedule a new one.\r\n              </div>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"!showNewSubscriber && showScheduleNextAppointment\">\r\n              <div class=\"eh-txt-12 eh-p-4\">\r\n                Your next <span class=\"eh-txt-bold\">Diet</span> is due on:<br>\r\n                <u>{{nextAppointmentDueDate | date:'EEEE | d-MMM-y':'+0530'}}</u><br>\r\n                Please click <a class=\"e-txt e-txt--matamber600 eh-txt-bold\" href=\"javascript: void(0);\"\r\n                  routerLink=\"/appointments\">here</a> to schedule an appointment for this day.\r\n              </div>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"showFirstDiet\">\r\n              <div class=\"eh-mt-16 eh-txt-12 eh-p-4\">\r\n                Your first <span class=\"eh-txt-bold\">Diet</span> has been uploaded.<br>To view, please go to <a\r\n                  class=\"e-txt e-txt--matamber600 eh-txt-bold\" href=\"javascript: void(0);\" routerLink=\"/diets\">My\r\n                  Diets</a> section.\r\n              </div>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"showLastDiet\">\r\n              <div class=\"eh-txt-12 eh-p-4\">\r\n                Your last <span class=\"eh-txt-bold\">Diet</span> has been uploaded.<br>To view, please go to <a\r\n                  class=\"e-txt e-txt--matamber600 eh-txt-bold\" href=\"javascript: void(0);\" routerLink=\"/diets\">My\r\n                  Diets</a> section.\r\n              </div>\r\n            </ng-container>\r\n          </ng-container>\r\n          <!-- <ion-chip color=\"secondary\">\r\n                <ion-icon name=\"globe-outline\" class=\"ion-no-padding\"></ion-icon>\r\n                <ion-label class=\"ion-no-padding eh-txt-10 eh-txt-500\">upload now</ion-label>\r\n                <ion-icon name=\"arrow-forward-outline\" class=\"ion-no-padding\"></ion-icon>\r\n              </ion-chip> -->\r\n        </div>\r\n      </div>\r\n    </ng-container>\r\n  </div>\r\n  <div class=\"e-card2 eh-my-10\">\r\n    <div class=\"e-card2__header\">\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">message</mat-icon>\r\n      <span>Other Messages</span>\r\n    </div>\r\n\r\n    <div class=\"eh-p-16\" fxFlex=\"1 0 0%\">\r\n      <ng-container *ngIf=\"showClinicalAssessmentUpload\">\r\n        <div class=\"e-title e-title--2 eh-txt-bold\">Clinical Assessments</div>\r\n\r\n        <div class=\"eh-txt-12 eh-ml-4 eh-mt-4\">Your Clinical Assessments reports are due.</div>\r\n        <div class=\"eh-mb-20 eh-ml-4 eh-txt-12\">Kindly <a class=\"e-txt e-txt--matamber600 eh-txt-bold\"\r\n            href=\"javascript: void(0);\" (click)=\"onUploadClick()\">upload</a> all your Clinical Assessments\r\n          reports at the earliest.</div>\r\n      </ng-container>\r\n\r\n      <ng-container *ngIf=\"showAdvancedTesting\">\r\n        <div class=\"e-title e-title--2 eh-txt-bold\">Advanced Tests</div>\r\n\r\n        <ng-container *ngIf=\"showHasRecommendedAdvancedTests\">\r\n          <div class=\"eh-txt-12 eh-ml-4 eh-mt-4\">\r\n            Kinita has recommended Advanced Testing for you. Please click <a\r\n              class=\"e-txt e-txt--matamber600 eh-txt-bold\" href=\"javascript: void(0);\"\r\n              (click)=\"onBuyAdvancedTestingClick()\">here</a> to\r\n            view & buy Advanced Testing.\r\n          </div>\r\n          <div *ngIf=\"advancedRecommendedPackage\" class=\"eh-mt-4 eh-pl-4 eh-txt-12\">\r\n            <span>Recommended Package:&nbsp;</span>\r\n            <span class=\"e-txt e-txt--eblue500 eh-txt-bold\">{{advancedRecommendedPackage}}</span>\r\n          </div>\r\n        </ng-container>\r\n\r\n        <ng-container *ngIf=\"showHasPurchasedAdvancedTests\">\r\n          <div class=\"eh-txt-12 eh-ml-4 eh-mt-4\">\r\n            You have purchased an Advanced Testing bundle. Please click <a class=\"e-txt e-txt--matamber600 eh-txt-bold\"\r\n              href=\"javascript: void(0);\" (click)=\"onBuyAdvancedTestingClick()\">here</a> to book an appointment.\r\n          </div>\r\n        </ng-container>\r\n\r\n        <ng-container *ngIf=\"showHasActiveAdvancedTestingAppt\">\r\n          <div class=\"eh-txt-12 eh-ml-4 eh-mt-4\">\r\n            Your Advanced Testing appointment has been scheduled on:<br>\r\n            <b>{{advancedTestingAppt.appointment.sessionStart | date:'EEEE | h:mm a, d-MMM-y':'+0530'}}</b>\r\n            at the <b>{{advancedTestingAppt.appointment.location.name}}</b> location<br><br>\r\n            If you wish to reschedule, please <a class=\"e-txt e-txt--matamber600 eh-txt-bold\"\r\n              href=\"javascript: void(0);\" (click)=\"onCancelATApptClick()\">Cancel</a> your current\r\n            appointment & schedule a new one.\r\n          </div>\r\n        </ng-container>\r\n\r\n        <ng-container *ngIf=\"showWaitingOnATReports\">\r\n          <div class=\"eh-txt-12 eh-ml-4 eh-mt-4\">\r\n            Hang tight while your reports are being prepared. Typically reports are uploaded in 2 working\r\n            days. We are excited to SEE what you’re MADE OF, aren’t you?\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n    </div>\r\n  </div>\r\n\r\n  <div fxLayout=\"column\">\r\n    <div class=\"eh-txt ion-padding eh-px-4 eh-txt-18 eh-txt-500\" *ngIf=\"showOnboardingIncomplete\">\r\n      Welcome Back!\r\n    </div>\r\n    <div *ngIf=\"!hasIntroVideoEnded && showOnboardingIncomplete\"\r\n      class=\"eh-my-6 eh-minh-100p eh-ofy-auto eh-hide-scrollbars\" fxLayout=\"row\" fxLayoutAlign=\"space-between\">\r\n      <div class=\"eh-maxh-100p\">\r\n        <video controls class=\"video-js eh-fit e-card--round1\" preload=\"auto\" width=\"100%\" controlsList=\"nodownload\"\r\n          data-setup=\"{}\" (ended)=\"onIntroVideoEnd()\">\r\n          <source [src]=\"introVideoURL\" type='video/mp4'>\r\n          <p class=\"vjs-no-js e-txt e-txt--placeholder\">\r\n            To view this video please enable JavaScript, and consider upgrading to a web browser that\r\n            <a href=\"https://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a>\r\n          </p>\r\n        </video>\r\n      </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"!hasWelcomeVideoEnded && showNewSubscriber\" class=\"eh-my-6 eh-minh-100p eh-ofy-auto eh-hide-scrollbars \"\r\n      fxLayout=\"row\" fxLayoutAlign=\"space-between\">\r\n      <div class=\"eh-maxh-100p\">\r\n        <video autoplay controls class=\"video-js eh-fit e-card--round1\" preload=\"auto\" width=\"100%\"\r\n          controlsList=\"nodownload\" data-setup=\"{}\" (ended)=\"onWelcomeVideoEnd()\">\r\n          <source [src]=\"welcomeVideoURL\" type='video/mp4'>\r\n          <p class=\"vjs-no-js e-txt e-txt--placeholder\">\r\n            To view this video please enable JavaScript, and consider upgrading to a web browser that\r\n            <a href=\"https://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a>\r\n          </p>\r\n        </video>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- <div class=\"eh-txt ion-padding eh-px-4 eh-txt-18 eh-txt-500 eh-mt-4\">\r\n      Community\r\n    </div>\r\n    <app-feed></app-feed>\r\n\r\n    <div class=\"eh-mb-16\">\r\n\r\n    </div>\r\n    <div class=\"e-card2\">\r\n      <div class=\"e-card2__header\">\r\n        <img class=\"eh-w-20\" src=\"../../../assets/images/instagram-logo-icon.svg\" alt=\"\">\r\n        <span class=\"eh-ml-8\">Kinita Kadakia Patel</span>\r\n        <span class=\"eh-ml-8\">|</span>\r\n        <span class=\"eh-ml-8\">\r\n          <a href=\"https://www.instagram.com/kskadakia/?hl=en\" target=\"_blank\">\r\n            @kskadakia\r\n          </a>\r\n        </span>\r\n      </div>\r\n      <div class=\"eh-pos-relative eh-pb-16 eh-ofx-scroll eh-hide-scrollbars\" fxLayout=\"row\">\r\n        <ng-container *ngIf=\"false\"> \r\n        <iframe data-controls=\"false\"\r\n          src=\"https://embedsocial.com/facebook_album/pro_hashtag/f54fb0207fd45ad5cdf325f11adabcfc911c7958\" width=\"100%\"\r\n          height=\"800px\" frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\" style=\"top: -64px;margin-top: -70px;  \r\n          overflow-y: hidden;\"></iframe>\r\n      </ng-container> \r\n      </div>\r\n    </div> -->\r\n\r\n\r\n</ion-content>\r\n\r\n<div *ngIf=\"userHasSubcribedPackage\">\r\n  <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\" style=\"margin-bottom:1rem ;\">\r\n    <ion-fab-button color=\"secondary\">\r\n      <ion-icon name=\"add\"></ion-icon>\r\n    </ion-fab-button>\r\n    <ion-fab-list side=\"top\">\r\n      <ion-fab-button data-desc=\"Log Measurements\" (click)=\"onGoToProgress(1)\">\r\n        <mat-icon>accessibility</mat-icon>\r\n      </ion-fab-button>\r\n      <ion-fab-button data-desc=\"Log Weight\" (click)=\"onGoToProgress(2)\">\r\n        <mat-icon>monitor_weight</mat-icon>\r\n      </ion-fab-button>\r\n      <ion-fab-button data-desc=\"Log Food\" (click)=\"onGoToProgress(3)\">\r\n        <mat-icon>lunch_dining</mat-icon>\r\n      </ion-fab-button>\r\n      <ion-fab-button data-desc=\"Appointments\" (click)=\"onGoToProgress(4)\">\r\n        <mat-icon>edit_calendar</mat-icon>\r\n      </ion-fab-button>\r\n    </ion-fab-list>\r\n  </ion-fab>\r\n</div>\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 15046:
/*!***************************************************************************!*\
  !*** ./src/app/pages/notification/notification.component.html?ngResource ***!
  \***************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<app-header title=\"Notifications\"></app-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <div class=\"eh-p-8 eh-ofy-scroll eh-hide-scrollbars\" fxFlex=\"1 0 0%\">\r\n    <ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n      <div *ngIf=\"!areNotificationsAvailable\" class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n        No unread notifications\r\n      </div>\r\n\r\n      <ng-container *ngIf=\"areNotificationsAvailable\">\r\n        <div *ngFor=\"let item of notifications\" fxLayout=\"row\" fxLayoutAlign=\"start stretch\"\r\n          class=\"e-card e-card--notif eh-mt-8 eh-t-spacer eh-pos-relative\">\r\n          <div fxFlex=\"0 auto\" class=\"e-card__content\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start stretch\" [innerHTML]=\"item.messageHTML\"></div>\r\n            <div class=\"e-txt--placeholder eh-txt-12 eh-mt-12\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n              <span *ngIf=\"item.isSystemGenerated; else userGenerBlk\">Auto</span>\r\n              <ng-template #userGenerBlk>\r\n                <span>By: {{item.staff.name}}</span>\r\n              </ng-template>\r\n              <span>{{item.notifDate | date:'EEE, d-MMM, H:mm':'+0530'}}</span>\r\n            </div>\r\n          </div>\r\n          <div class=\"eh-csr-ptr eh-xy-center\" fxFlex=\"0 0 36px\" (click)=\"onClick(item)\">\r\n            <mat-icon *ngIf=\"item.isNew\" class=\"e-mat-icon e-mat-icon--3 e-txt--matamber600\">mark_chat_read</mat-icon>\r\n            <mat-icon *ngIf=\"!item.isNew\" class=\"e-mat-icon e-mat-icon--3 e-txt--eblue700\">mark_chat_read</mat-icon>\r\n          </div>\r\n          <div *ngIf=\"!item.isSystemGenerated\" class=\"eh-pos-absolute\" style=\"top: -12px; left: -6px;\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--1 e-txt--matamber600\">campaign</mat-icon>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n    </ng-container>\r\n  </div>\r\n</ion-content>\r\n<!-- <app-tabs></app-tabs> -->\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 80599:
/*!*******************************************************!*\
  !*** ./src/app/pages/pages.component.html?ngResource ***!
  \*******************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\r\n  <ion-router-outlet id=\"main-content\"></ion-router-outlet>\r\n\r\n";

/***/ }),

/***/ 40703:
/*!***********************************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/appointment/appointment.component.html?ngResource ***!
  \***********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div fxLayout=\"column\">\r\n    <div class=\"eh-txt-bold eh-mt-24\">Schedule your appointment for Advanced Testing</div>\r\n\r\n    <mat-divider class=\"eh-w-100p\"></mat-divider>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div fxLayout=\"column\" fxFlex=\"0 0 30%\" [formGroup]=\"searchFG\">\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-180\">\r\n          <mat-label>Select a location</mat-label>\r\n          <mat-select formControlName=\"location\" placeholder=\"Select\">\r\n            <mat-option *ngFor=\"let location of locations\" class=\"eh-txt-capital\" [value]=\"location\">\r\n              {{location.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-180\">\r\n          <mat-label>Choose a date</mat-label>\r\n          <input matInput formControlName=\"date\" [min]=\"startDate\" [max]=\"endDate\" [matDatepicker]=\"datepicker\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"datepicker\"></mat-datepicker-toggle>\r\n          <mat-datepicker #datepicker></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <div class=\"eh-w-180\">\r\n          <button *ngIf=\"!loadingSlots\" mat-raised-button (click)=\"onSearch()\" [disabled]=\"!isSearchFGValid()\"\r\n            [ngClass]=\"{'e-mat-button e-mat-button--eblue400 eh-w-100p eh-mt-16': true, 'eh-csr-dsb': !isSearchFGValid()}\">\r\n            Search Availability\r\n          </button>\r\n          <div *ngIf=\"loadingSlots\" class=\"e-spinner e-spinner--2 eh-mt-16 eh-h-36\">\r\n            <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div fxLayout=\"column\" fxFlex=\"0 0 70%\"\r\n        class=\"e-card2 e-card2--bg-eblue100 eh-py-16 eh-h-400 eh-ofy-scroll eh-hide-scrollbars\">\r\n        <div *ngIf=\"!loadingSlots && isSearchPristine\" class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Search availability to load details\r\n        </div>\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && !areSlotsAvailable\"\r\n          class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Sorry, no slots are available on this day\r\n        </div>\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && areSlotsAvailable\" class=\"eh-txt-12 eh-txt-center\">\r\n          Your Advanced Testing appointment is with Lincia Creado. This is her calendar.\r\n        </div>\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && areSlotsAvailable\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\"\r\n          class=\"eh-mt-8\">\r\n          <div *ngFor=\"let item of slots\" (click)=\"onSlotClicked(item)\"\r\n            class=\"e-card2 e-card2--appt2 e-card2--hover1 eh-pos-relative\">\r\n            <span class=\"eh-txt-center\">\r\n              {{item.sessionStart | date:'h:mm a':'+0530'}} - {{item.sessionEnd | date:'h:mm a':'+0530'}}\r\n            </span>\r\n            <div *ngIf=\"item.clicked\" class=\"eh-pos-absolute\" style=\"right: 1px; top: 1px\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5\">check_circle</mat-icon>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <mat-divider class=\"eh-mt-24 eh-w-100p\"></mat-divider>\r\n\r\n    <div class=\"eh-mt-20 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"space-between start\">\r\n      <button mat-stroked-button (click)=\"closeSelf()\">Later</button>\r\n      <div class=\"eh-w-180\">\r\n        <button *ngIf=\"!bookingAppt\" mat-flat-button (click)=\"onBookAppointment()\" [disabled]=\"!selectedSlot\"\r\n          [ngClass]=\"{'e-mat-button e-mat-button--eblue400': true, 'eh-csr-dsb': !selectedSlot}\">\r\n          Schedule Appointment\r\n        </button>\r\n        <div *ngIf=\"bookingAppt\" class=\"e-spinner e-spinner--2 eh-h-36\">\r\n          <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 88682:
/*!********************************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/buy-advanced-testing.component.html?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div class=\"eh-mb-8 eh-txt-center eh-txt-16 eh-txt-bold\">\r\n  Advanced Testing\r\n</div>\r\n\r\n<mat-horizontal-stepper [linear]=\"true\" #stepper class=\"e-mat-stepper e-mat-stepper--w40p\" [selectedIndex]=\"data.step\">\r\n  <mat-step [editable]=\"false\">\r\n    <ng-template matStepperIcon=\"edit\">\r\n      <mat-icon>check</mat-icon>\r\n    </ng-template>\r\n\r\n    <ng-template matStepLabel>Select & Pay</ng-template>\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n      <app-payment (goToNextStep)=\" goToNextStep()\" (closeDialog)=\"closeDialog()\">\r\n      </app-payment>\r\n    </div>\r\n  </mat-step>\r\n\r\n  <mat-step [editable]=\"false\">\r\n    <ng-template matStepperIcon=\"edit\">\r\n      <mat-icon>check</mat-icon>\r\n    </ng-template>\r\n\r\n    <ng-template matStepLabel>Appointment</ng-template>\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n      <app-appointment (closeDialog)=\"closeDialog()\"></app-appointment>\r\n    </div>\r\n  </mat-step>\r\n</mat-horizontal-stepper>";

/***/ }),

/***/ 4211:
/*!***************************************************************************************!*\
  !*** ./src/app/shared/buy-advanced-testing/payment/payment.component.html?ngResource ***!
  \***************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-mt-20 eh-mx-auto eh-w-100p\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n    <span class=\"eh-txt-center eh-txt-12\">\r\n      Advanced tests recommended by Kinita have been selected for your reference.\r\n    </span>\r\n    <span class=\"eh-txt-center eh-txt-12\">\r\n      You can select any one testing bundle or any number of individual tests from the following:\r\n    </span>\r\n  </div>\r\n\r\n  <div class=\"e-bg e-bg--eblue200 eh-mt-20 eh-p-12 eh-w-80p eh-mx-auto e-brd e-brd--all e-brd--round1\">\r\n    <div class=\"eh-txt-center eh-mx-auto\">\r\n      <div class=\"eh-txt-12 eh-txt-bold\">\r\n        Testing Bundles\r\n      </div>\r\n\r\n      <mat-radio-group [formControl]=\"advancedTestPackagesFC\">\r\n        <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\">\r\n          <div *ngFor=\"let t of advancedTestPackages;\" class=\"eh-pos-relative eh-ml-16 eh-mt-36\" fxLayout=\"column\"\r\n            fxLayoutAlign=\"start start\" fxFlex=\"30\">\r\n\r\n            <mat-radio-button class=\"eh-txt-12\" [value]=\"t\" (change)=\"onPackageCheckChange($event)\"\r\n              [disabled]=\"!t.isActive\">\r\n              &#8377;{{t.price.amount | number:'1.0'}}\r\n            </mat-radio-button>\r\n\r\n            <span class=\"eh-txt-12 eh-mt-4\">\r\n              {{t.name}}\r\n            </span>\r\n            <span *ngIf=\"!t.isActive\" class=\"eh-txt-12 eh-txt-italic\">\r\n              (Coming soon)\r\n            </span>\r\n\r\n            <div *ngIf=\"t.key === advancedTestPackageInitial.key\" class=\"eh-pos-absolute\" style=\"top: -20px; left: 0;\">\r\n              <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                <mat-icon class=\"e-mat-icon e-mat-icon--4 e-txt-primary\">verified</mat-icon>\r\n                <span class=\"eh-ml-2 eh-txt-10 e-txt-primary\">Recommended by Kinita</span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </mat-radio-group>\r\n\r\n      <a class=\"eh-mt-16 eh-txt-underline eh-csr-ptr\" (click)=\"resetPackages()\">Reset</a>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"eh-txt-12 eh-mt-20 eh-txt-bold eh-w-80p eh-txt-center eh-mx-auto\">\r\n    Individual Tests\r\n  </div>\r\n\r\n  <div class=\"eh-w-80p eh-px-12 eh-mx-auto\" fxLayout=\"row wrap\" fxLayoutAlign=\"center start\">\r\n    <div *ngFor=\"let t of allAdvancedTests;\" class=\"eh-ml-16 eh-mt-24\" fxLayout=\"column\" fxFlex=\"30\">\r\n      <mat-checkbox class=\"e-mat-checkbox\" [checked]=\"isChecked(t)\" (change)=\"onCheckChange($event)\"\r\n        [disabled]=\"!t.isActive\">\r\n        &#8377;{{t.price.amount | number:'1.0'}}\r\n      </mat-checkbox>\r\n\r\n      <span class=\"eh-txt-12 eh-mt-4\">\r\n        {{t.name}}\r\n      </span>\r\n      <span *ngIf=\"!t.isActive\" class=\"eh-txt-12 eh-txt-italic\">\r\n        (Coming soon)\r\n      </span>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"e-txt-primary eh-txt-center eh-txt-bold eh-mt-24\">\r\n    &#8377;{{cost | number}}\r\n  </div>\r\n  <div class=\"eh-txt-center eh-txt-12 eh-txt-italic\">\r\n    (Price with GST &#8377;{{costWithTax | number}})\r\n  </div>\r\n\r\n  <div class=\"eh-txt-center eh-txt-12 eh-txt-italic\">\r\n    Please scroll down to read the full terms and conditions.\r\n  </div>\r\n\r\n  <div class=\"e-bg e-bg--eblue200 eh-mt-16 eh-p-16 e-brd e-brd--all e-brd--round1\">\r\n    <div class=\"eh-txt-center eh-txt-12\">\r\n      <mat-checkbox [formControl]=\"requiresGSTFC\">\r\n        I require a GST invoice for this payment\r\n      </mat-checkbox>\r\n    </div>\r\n    <div *ngIf=\"requiresGSTFC.value\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\" fxLayoutGap=\"16px\" [formGroup]=\"gstFG\">\r\n      <mat-form-field class=\"e-mat-form-field eh-w-180 eh-mt-12\">\r\n        <mat-label>GST Number</mat-label>\r\n        <input matInput formControlName=\"number\">\r\n      </mat-form-field>\r\n\r\n      <mat-form-field class=\"e-mat-form-field eh-w-180 eh-mt-12\">\r\n        <mat-label>GST Billing Name</mat-label>\r\n        <input matInput formControlName=\"name\">\r\n      </mat-form-field>\r\n\r\n      <mat-form-field class=\"e-mat-form-field eh-w-280 eh-mt-12\">\r\n        <mat-label>GST Billing Address</mat-label>\r\n        <input matInput formControlName=\"address\">\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"eh-mt-16 eh-w-92 eh-mx-auto\">\r\n    <button *ngIf=\"!loading\" mat-raised-button\r\n      [ngClass]=\"{'e-mat-button e-mat-button--eblue400': true, 'eh-csr-dsb': !cost}\" (click)=\"onPayment()\"\r\n      [disabled]=\"!cost\">\r\n      Pay Now\r\n    </button>\r\n    <div *ngIf=\"loading\" class=\"e-spinner e-spinner--2 eh-h-36\">\r\n      <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n    </div>\r\n  </div>\r\n\r\n  <!-- <div class=\"eh-mx-auto eh-txt-center eh-mt-16\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n    <mat-checkbox class=\"eh-txt-12\" [formControl]=\"needGSTFC\">I require a GST invoice for this payment</mat-checkbox>\r\n    <mat-form-field *ngIf=\"needGSTFC.value\" class=\"e-mat-form-field e-mat-form-field--1\" floatLabel=\"never\">\r\n      <mat-label>GST number</mat-label>\r\n      <input matInput [formControl]=\"gstNumberFC\">\r\n    </mat-form-field>\r\n  </div> -->\r\n\r\n  <div class=\"eh-mt-16 eh-txt-bold e-txt e-txt--matamber600 eh-txt-center\">OR</div>\r\n\r\n  <div class=\"eh-mt-12 eh-txt-bold eh-txt-center\">Other Modes of Payment</div>\r\n  <table class=\"e-table\">\r\n    <tr>\r\n      <td class=\"eh-txt-center\" style=\"width: 20%;\">NEFT / RTGS / IMPS</td>\r\n      <!-- <td class=\"eh-txt-center\" style=\"width: 20%;\">Credit / Debit Card</td> -->\r\n      <td class=\"eh-txt-center\" style=\"width: 20%;\">VPA</td>\r\n      <td class=\"eh-txt-center\" style=\"width: 20%;\">GPay / QR Code Scan</td>\r\n      <td class=\"eh-txt-center\" style=\"width: 20%;\">Cheque Payment</td>\r\n    </tr>\r\n\r\n    <tr>\r\n      <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n        <span class=\"eh-txt-bold\">MEALpyramid Nutrition and Wellness</span><br><br>\r\n        Account No: <span class=\"eh-txt-bold\">23905900140</span><br>\r\n        IFSC Code: <span class=\"eh-txt-bold\">SCBL0036058</span> (for rupee payments)<br>\r\n        SWIFT Code: <span class=\"eh-txt-bold\">SCBLINBBXXX</span> (for non-rupee payments)<br><br>\r\n        Bank: <span class=\"eh-txt-bold\">Standard Chartered Bank</span><br>\r\n        Branch: <span class=\"eh-txt-bold\">Juhu</span><br>\r\n        Type: <span class=\"eh-txt-bold\">Current / Domestic</span><br>\r\n        Entity Type: <span class=\"eh-txt-bold\">Partnership</span>\r\n      </td>\r\n\r\n      <!-- <td class=\"eh-txt-12\">\r\n        WhatsApp us on <span class=\"eh-txt-bold\">+91-892686118</span> and we will generate a link for you so you can\r\n        make the payment online using your Credit or Debit Card.<br><br>\r\n        <span class=\"e-txt e-txt-placeholder\">International customers can select the currency of their choice.</span>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <img class=\"eh-w-100 eh-mt-16\" src=\"../../../../assets/images/razorpay-logo-white.svg\" alt=\"RazorPay\">\r\n        </div>\r\n      </td> -->\r\n\r\n      <td class=\"eh-txt-12\">\r\n        Simply enter our VPA ID in any supported app.<br><br>\r\n        VPA ID:<br>\r\n        <span class=\"eh-txt-bold\">mealpyramid@sc</span><br><br>\r\n        Linked Number:<br>\r\n        <span class=\"eh-txt-bold\">+91-9819304049</span><br><br>\r\n      </td>\r\n\r\n      <td class=\"e-txt e-txt--matgrey700 eh-txt-12 eh-txt-center\" style=\"line-height: 1.4em;\">\r\n        <span>Scan the below QR code</span><br>\r\n        <img class=\"eh-w-144 eh-mt-12\" src=\"../../../../assets/images/payment-qr-code-2.png\" alt=\"QR Code\">\r\n        <div class=\"eh-mt-12\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\">\r\n          <img class=\"eh-w-64\" src=\"../../../../assets/images/google-pay-mark-logo.svg\" alt=\"GPay\">\r\n          <img class=\"eh-w-60\" src=\"../../../../assets/images/paytm-logo.svg\" alt=\"PayTM\">\r\n          <img class=\"eh-w-80\" src=\"../../../../assets/images/phone-pe-logo.svg\" alt=\"PhonePe\">\r\n        </div>\r\n        <!-- <div>\r\n            <img class=\"eh-w-100\" src=\"../../../../assets/images/bhim-upi-logo.png\" alt=\"BHIM UPI\">\r\n          </div> -->\r\n      </td>\r\n\r\n      <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n        <span>Cheques must be drawn in favour of:</span><br>\r\n        <span>“<b>MEALpyramid Nutrition and Wellness</b>” and sent to:</span><br><br>\r\n        <b>MEALpyramid Nutrition and Wellness</b><br>\r\n        <span>1st Floor, Anuradha Satyamorthy Building,</span><br>\r\n        <span>#16 Vithal Nagar Society,</span><br>\r\n        <span>11th Road, Juhu Scheme,</span><br>\r\n        <span>Mumbai, 400049, INDIA.</span><br><br>\r\n        <span>Tel: +91-9892686118</span>\r\n      </td>\r\n    </tr>\r\n  </table>\r\n\r\n  <div class=\"eh-mt-8\">\r\n    Once paid, kindly send us remittance details and we will activate your account accordingly. If your preferred\r\n    mode of payment is other than credit card, kindly share your payment details. WhatsApp or email us the details and\r\n    we\r\n    shall activate the purchase from our side.\r\n  </div>\r\n\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"start start\" class=\"eh-mt-24\">\r\n    <div class=\"eh-mt-8\" fxFlex=\"0 0 30%\">\r\n      <div class=\"eh-txt-bold eh-px-2 eh-mb-8\">\r\n        ALL PACKAGES INCLUDE\r\n      </div>\r\n      <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n        <div class=\"eh-mt-6\">\r\n          <div class=\"eh-txt-bold\">\r\n            Complete Nutrition Analysis\r\n          </div>\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>\r\n              Health history analysis\r\n            </li>\r\n            <li>\r\n              Clinical analysis\r\n            </li>\r\n            <li>\r\n              Dietary supplements\r\n            </li>\r\n            <li>\r\n              Lifestyle modifications\r\n            </li>\r\n            <li>\r\n              Travel tips </li>\r\n            <li>\r\n              Guide to eating out</li>\r\n          </ul>\r\n        </div>\r\n        <div class=\"eh-mt-6\">\r\n          <div class=\"eh-txt-bold\">\r\n            Interpersonal Consultations\r\n          </div>\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>\r\n              Individualised nutrition plan\r\n            </li>\r\n            <li>\r\n              Realistic goal setting\r\n            </li>\r\n            <li>\r\n              Detailed diet discussions\r\n            </li>\r\n          </ul>\r\n        </div>\r\n        <div class=\"eh-mt-6\">\r\n          <div class=\"eh-txt-bold\">Follow-Up Consultations\r\n          </div>\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>\r\n              Target monitoring\r\n            </li>\r\n            <li>\r\n              Dietary changes\r\n            </li>\r\n            <li>\r\n              Constant monitoring\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-txt-bold eh-txt-italic eh-mt-10 eh-txt-12 \">\r\n        <span class=\"eh-txt-underline\">NOTE</span> : Online services follow the\r\n        same protocol & charges\r\n      </div>\r\n      <div class=\" eh-txt-bold eh-mt-8 eh-txt-12\">\r\n        GSTIN: 27ABRFM9899F1Z6\r\n      </div>\r\n    </div>\r\n    <div class=\"eh-mt-8\" fxFlex=\"0 0 35%\">\r\n      <div class=\"eh-txt-bold eh-px-16 eh-mb-8\">MEALpyramid will :</div>\r\n      <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n        <ul class=\"eh-m-0 eh-px-16 \">\r\n          <li>\r\n            endeavour to provide you with the best nutritional advice that may help resolve weight and health problems\r\n            or improve your sporting performance; however, following the advice is ultimately at your discretion\r\n          </li>\r\n          <li>\r\n            not guarantee that you will meet your target, whatever that may be\r\n          </li>\r\n          <li>\r\n            retain the copyright of all diets and written material provided to the client </li>\r\n          <li>\r\n            reserve the right to terminate our services in the event of the client failing to comply with the prescribed\r\n            diet\r\n          </li>\r\n          <li>\r\n            not be liable under any circumstances for any health issues / complications arising during or after clients\r\n            start following our guidance\r\n          </li>\r\n          <li>\r\n            maintain client confidentiality at all times and details of your records will only be released to third\r\n            parties only upon your express written consent\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n    <div class=\"eh-mt-8\" fxFlex=\"0 0 35%\">\r\n      <div class=\"eh-txt-bold eh-px-16 eh-mb-8\">TERMS OF SERVICE</div>\r\n      <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n        <ul class=\"eh-m-0 eh-px-16\">\r\n          <li>Orientation fee is applicable for all clients (first visit charges / package renewal after long break).\r\n          </li>\r\n          <li>Health check is mandatory before starting the package.\r\n          </li>\r\n          <li>Entire package amount is payable upfront, prior to starting the consultation.</li>\r\n          <li>Amount once paid shall not be refundable under any circumstances.</li>\r\n          <li>All packages are non-transferable</li>\r\n          <li>No additional session / time extension shall be provided in case they lapse ; <span\r\n              class=\"eh-txt-underline\"> please make note of your package details carefully</span></li>\r\n          <li>All packages must be completed within the specified time period.</li>\r\n          <li>Upgrading of package midway is not permitted.</li>\r\n          <li>Children aged under 18 years must be accompanied by a parent or guardian.</li>\r\n          <li>Rates subject to change without prior notice. Rates at the time of joining shall only be applicable.</li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 59292:
/*!************************************************************************!*\
  !*** ./src/app/shared/dialog-box/dialog-box.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n  <mat-icon *ngIf=\"dialog.icon\" class=\"e-mat-icon e-mat-icon--2 e-txt-eblue700 eh-mr-4\">{{dialog.icon}}</mat-icon>\r\n  <span class=\"eh-txt-bold e-txt-eblue700\">{{dialog.title}}</span>\r\n</div>\r\n<mat-divider class=\"e-mat-divider eh-mt-8\"></mat-divider>\r\n\r\n<div class=\"eh-w-200 eh-h-100\" fxLayout=\"column\" fxLayoutAlign=\"space-between\">\r\n  <div class=\"eh-pt-16 eh-pb-20\" fxFlex=\"1 1 auto\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <div class=\"eh-txt-breakword eh-txt-center\" [innerHTML]=\"dialog.message\"></div>\r\n  </div>\r\n\r\n  <div fxFlex=\"0 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"center\">\r\n    <button *ngIf=\"dialog.actionFalseBtnTxt\" mat-flat-button\r\n      [ngClass]=\"['e-mat-button', 'eh-mr-16', dialog.actionFalseBtnStyle]\"\r\n      [mat-dialog-close]=\"false\">{{dialog.actionFalseBtnTxt}}</button>\r\n    <button mat-flat-button [ngClass]=\"['e-mat-button', dialog.actionTrueBtnStyle]\"\r\n      [mat-dialog-close]=\"true\">{{dialog.actionTrueBtnTxt}}</button>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 39368:
/*!**************************************************************************************!*\
  !*** ./src/app/shared/get-started/appointment/appointment.component.html?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div fxLayout=\"column\">\r\n    <div class=\"eh-txt-bold eh-mt-24\">Schedule your Orientation with Kinita</div>\r\n\r\n    <mat-divider class=\"eh-w-100p\"></mat-divider>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n      <div fxLayout=\"column\" fxFlex=\"0 0 30%\" [formGroup]=\"searchFG\">\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-180\">\r\n          <mat-label>Select a location</mat-label>\r\n          <mat-select formControlName=\"location\" placeholder=\"Select\">\r\n            <mat-option *ngFor=\"let location of locations\" class=\"eh-txt-capital\" [value]=\"location\">\r\n              {{location.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-180\">\r\n          <mat-label>Choose a date</mat-label>\r\n          <input matInput formControlName=\"date\" [min]=\"startDate\" [max]=\"endDate\" [matDatepicker]=\"datepicker\">\r\n          <mat-datepicker-toggle matSuffix [for]=\"datepicker\"></mat-datepicker-toggle>\r\n          <mat-datepicker #datepicker></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <div class=\"eh-w-180\">\r\n          <button *ngIf=\"!loadingSlots\" mat-raised-button (click)=\"onSearch()\" [disabled]=\"!isSearchFGValid()\"\r\n            [ngClass]=\"{'e-mat-button e-mat-button--eblue400 eh-w-100p eh-mt-16': true, 'eh-csr-dsb': !isSearchFGValid()}\">\r\n            Search Availability\r\n          </button>\r\n          <div *ngIf=\"loadingSlots\" class=\"e-spinner e-spinner--2 eh-mt-16 eh-h-36\">\r\n            <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div fxLayout=\"column\" fxFlex=\"0 0 70%\"\r\n        class=\"e-card2 e-card2--bg-eblue100 eh-py-16 eh-h-400 eh-ofy-scroll eh-hide-scrollbars\">\r\n        <div *ngIf=\"!loadingSlots && isSearchPristine\" class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Search availability to load details\r\n        </div>\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && !areSlotsAvailable\"\r\n          class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Sorry, no slots are available on this day\r\n        </div>\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && areSlotsAvailable\" class=\"eh-txt-12 eh-txt-center\">\r\n          Your Orientation appointment is with Kinita. This is her calendar.\r\n        </div>\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && areSlotsAvailable\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\"\r\n          class=\"eh-mt-8\">\r\n          <div *ngFor=\"let item of slots\" (click)=\"onSlotClicked(item)\"\r\n            class=\"e-card2 e-card2--appt2 e-card2--hover1 eh-pos-relative\">\r\n            <span class=\"eh-txt-center\">\r\n              {{item.sessionStart | date:'h:mm a':'+0530'}} - {{item.sessionEnd | date:'h:mm a':'+0530'}}\r\n            </span>\r\n            <div *ngIf=\"item.clicked\" class=\"eh-pos-absolute\" style=\"right: 1px; top: 1px\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5\">check_circle</mat-icon>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <mat-divider class=\"eh-mt-24 eh-w-100p\"></mat-divider>\r\n\r\n    <div class=\"eh-mt-20 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"space-between start\">\r\n      <button mat-stroked-button (click)=\"closeSelf()\">Later</button>\r\n      <div class=\"eh-w-180\">\r\n        <button *ngIf=\"!bookingAppt\" mat-flat-button (click)=\"onBookAppointment()\" [disabled]=\"!selectedSlot\"\r\n          [ngClass]=\"{'e-mat-button e-mat-button--eblue400': true, 'eh-csr-dsb': !selectedSlot}\">\r\n          Schedule Appointment\r\n        </button>\r\n        <div *ngIf=\"bookingAppt\" class=\"e-spinner e-spinner--2 eh-h-36\">\r\n          <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 46519:
/*!**************************************************************************!*\
  !*** ./src/app/shared/get-started/get-started.component.html?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div class=\"eh-mb-8 eh-txt-center eh-txt-16 eh-txt-bold\">\r\n  Get Started\r\n</div>\r\n\r\n<mat-horizontal-stepper [linear]=\"true\" #stepper class=\"e-mat-stepper\" [selectedIndex]=\"data.step\">\r\n  <mat-step [editable]=\"false\">\r\n    <ng-template matStepperIcon=\"edit\">\r\n      <mat-icon>check</mat-icon>\r\n    </ng-template>\r\n\r\n    <ng-template matStepLabel>Personal Details</ng-template>\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n      <div class=\"eh-txt-center eh-txt-12 eh-mt-8\">\r\n        <i>\r\n          Once committed, you will not be able to change any of your details, so do take care while filling it\r\n          in.<br>\r\n          Please scroll down to save/submit.\r\n        </i>\r\n      </div>\r\n      <app-personal-details (goToNextStep)=\"goToNextStep()\" (closeDialog)=\"closeDialog()\">\r\n      </app-personal-details>\r\n    </div>\r\n  </mat-step>\r\n\r\n  <mat-step [editable]=\"false\">\r\n    <ng-template matStepperIcon=\"edit\">\r\n      <mat-icon>check</mat-icon>\r\n    </ng-template>\r\n    <ng-template matStepLabel>Health History</ng-template>\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n      <div class=\"eh-txt-center eh-txt-12 eh-mt-8\">\r\n        <i>\r\n          Once committed, you will not be able to change any of your details, so do take care while filling it\r\n          in.<br>\r\n          Please scroll down to save/submit.\r\n        </i>\r\n      </div>\r\n      <app-health-history (goToNextStep)=\"goToNextStep()\" (closeDialog)=\"closeDialog()\">\r\n      </app-health-history>\r\n    </div>\r\n  </mat-step>\r\n\r\n  <mat-step [editable]=\"false\">\r\n    <ng-template matStepperIcon=\"edit\">\r\n      <mat-icon>check</mat-icon>\r\n    </ng-template>\r\n    <ng-template matStepLabel>Payment</ng-template>\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n      <app-payment (goToNextStep)=\"goToNextStep()\" (closeDialog)=\"closeDialog()\"></app-payment>\r\n    </div>\r\n  </mat-step>\r\n\r\n  <mat-step [editable]=\"false\">\r\n    <ng-template matStepperIcon=\"edit\">\r\n      <mat-icon>check</mat-icon>\r\n    </ng-template>\r\n    <ng-template matStepLabel>Appointment</ng-template>\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n      <app-appointment (closeDialog)=\"closeDialog()\"></app-appointment>\r\n    </div>\r\n  </mat-step>\r\n</mat-horizontal-stepper>";

/***/ }),

/***/ 24856:
/*!********************************************************************************************!*\
  !*** ./src/app/shared/get-started/health-history/health-history.component.html?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\" [formGroup]=\"step2FormGroup\">\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">medical_services</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Medical History & Lifestyle</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\">\r\n    Do you have any of the following conditions? (Please TICK if applicable)\r\n  </div>\r\n  <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\">\r\n    <div *ngFor=\"let c of generalNutritionMedConds;\" class=\"eh-mt-12 eh-ml-12 eh-fit\" fxFlex=\"0 0 22%\">\r\n      <mat-checkbox class=\"e-mat-checkbox\" [value]=\"c\" (change)=\"onMedCondChkChange($event)\"\r\n        [checked]=\"initialMedicalConditions[c]\">\r\n        {{c}}\r\n      </mat-checkbox>\r\n    </div>\r\n    <div class=\"eh-mt-12 eh-ml-12 eh-fit\" fxFlex=\"0 0 22%\">\r\n      <mat-checkbox class=\"e-mat-checkbox\" (change)=\"onOtherMedCondChkChange($event)\"\r\n        [checked]=\"otherMedicalConditionsFC.value\">\r\n        Other\r\n      </mat-checkbox>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-ml-12\">\r\n    <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n      <mat-label>&nbsp;If any other conditions, please specify:</mat-label>\r\n      <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n        formControlName=\"otherMedicalConditions\" class=\"eh-hide-scrollbars\"></textarea>\r\n    </mat-form-field>\r\n  </div>\r\n  <div class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Are you taking any medication?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"onMedication\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If so, please list and state the reasons</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"medicationDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"isFemale\" class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Do you consider your menstrual cycle regular?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"isMCycleRegular\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If not, please specify</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"mCycleDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Do you drink alcohol?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"doesAlcohol\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If yes, please specify type, frequency in a week and quantity each time</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"alcoholDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Do you smoke?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"doesSmoking\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If so, please specify the number of cigarettes per day</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"smokingDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Do you chew tobacco?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"doesTobacco\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If yes, please specify how much per day</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"tobaccoDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">directions_run</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Exercise</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Do you exercise regularly?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"doesExercise\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n  </div>\r\n  <ng-container *ngIf=\"doesExerciseFC.value\" formArrayName=\"exerciseDesc\">\r\n    <div class=\"eh-mt-16 eh-ml-12\" fxLayout=\"row\" fxLayoutAlign=\"start start\">\r\n      <span class=\"eh-txt-12 e-txt-mat-grey600 e-brd-btm-matgrey400\" fxFlex=\"0 0 25%\">Exercise Type</span>\r\n      <span class=\"eh-txt-12 e-txt-mat-grey600 e-brd-btm-matgrey400\" fxFlex=\"0 0 25%\">Days per Week</span>\r\n      <span class=\"eh-txt-12 e-txt-mat-grey600 e-brd-btm-matgrey400\" fxFlex=\"0 0 25%\">Duration (mins)</span>\r\n      <span class=\"eh-txt-12 e-txt-mat-grey600 e-brd-btm-matgrey400\" fxFlex=\"0 0 25%\">Intensity</span>\r\n    </div>\r\n    <ng-container *ngFor=\"let e of exerciseDescFA.controls; let i = index;\">\r\n      <div class=\"eh-mt-4 eh-ml-12 eh-txt-12\" [formGroupName]=\"i\" fxLayout=\"row\" fxLayoutAlign=\"start start\">\r\n        <span fxFlex=\"0 0 25%\">{{e.value.name}}</span>\r\n        <div fxFlex=\"0 0 25%\">\r\n          <input class=\"e-brd-btm-matgrey400\" type=\"number\" formControlName=\"daysPerWeek\">\r\n        </div>\r\n        <div fxFlex=\"0 0 25%\">\r\n          <input class=\"e-brd-btm-matgrey400\" type=\"number\" formControlName=\"duration\">\r\n        </div>\r\n        <div fxFlex=\"0 0 25%\">\r\n          <mat-select class=\"eh-w-160\" formControlName=\"intensity\">\r\n            <mat-option value=\"Low\">Low</mat-option>\r\n            <mat-option value=\"Moderate\">Moderate</mat-option>\r\n            <mat-option value=\"High\">High</mat-option>\r\n          </mat-select>\r\n        </div>\r\n      </div>\r\n    </ng-container>\r\n  </ng-container>\r\n  <div class=\"e-bg-eblue250 eh-mt-20 eh-p-8 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">monitor_weight</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Weight</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-240\">Have you tried to lose weight before?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"triedLosingWeight\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-8\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"triedLosingWeightFC.value\" fxLayout=\"row wrap\" fxLayoutAlign=\"start start\">\r\n    <div *ngFor=\"let t of weightLossTypes;\" class=\"eh-mt-12 eh-ml-12 eh-fit\" fxFlex=\"0 0 30%\">\r\n      <mat-checkbox class=\"e-mat-checkbox\" [value]=\"t\" (change)=\"onWeightLossTypeChkChange($event)\"\r\n        [checked]=\"initialWeightLossAttempts[t]\">\r\n        {{t}}\r\n      </mat-checkbox>\r\n    </div>\r\n  </div>\r\n  <div *ngIf=\"isSportsPerson\" class=\"e-bg-eblue250 eh-mt-20 eh-p-8 eh-w-100p\" fxLayout=\"row\"\r\n    fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">sports_cricket</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Sports</span>\r\n  </div>\r\n  <div *ngIf=\"isSportsPerson\" class=\"eh-mt-16 eh-w-100p\" fxLayout=\"column\" fxLayoutAlign=\"start start\">\r\n    <span>Which sport do you play:</span>\r\n    <input class=\"eh-w-100p eh-mt-6 e-brd-btm-matgrey400\" type=\"text\" formControlName=\"sportName\">\r\n  </div>\r\n  <div *ngIf=\"isSportsPerson\" class=\"eh-mt-16 eh-w-100p\" fxLayout=\"column\" fxLayoutAlign=\"start start\">\r\n    <span>Any sports injury?</span>\r\n    <input class=\"eh-w-100p eh-mt-6 e-brd-btm-matgrey400\" type=\"text\" formControlName=\"sportInjury\">\r\n  </div>\r\n  <div *ngIf=\"isSportsPerson\" class=\"eh-mt-16 eh-w-100p\" fxLayout=\"column\" fxLayoutAlign=\"start start\">\r\n    <span>Any other specific concerns:</span>\r\n    <input class=\"eh-w-100p eh-mt-6 e-brd-btm-matgrey400\" type=\"text\" formControlName=\"sportConcerns\">\r\n  </div>\r\n  <div class=\"e-bg-eblue250 eh-mt-20 eh-p-8 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">restaurant</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Nutrition</span>\r\n  </div>\r\n  <div class=\"eh-mt-12 eh-w-240\">Choice of diet</div>\r\n  <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\">\r\n    <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"mealCategory\">\r\n      <mat-radio-button *ngFor=\"let m of mealCategories;\" class=\"eh-ml-12 eh-mt-8\" [value]=\"m\">\r\n        {{m}}\r\n      </mat-radio-button>\r\n    </mat-radio-group>\r\n  </div>\r\n  <div class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-300\">How would you describe your eating habits?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"eatingHabits\">\r\n        <mat-radio-button value=\"Good\">Good</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-12\" value=\"Fair\">Fair</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-12\" value=\"Poor\">Poor</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-16\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-300\">Do you currently take any vitamins or dietary supplements?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"onSupplements\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-22\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If so, please list and state the reasons</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"1.2\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"supplementsDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8 eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">reorder</mat-icon>\r\n    <span class=\"e-txt-eblue700\">General</span>\r\n  </div>\r\n  <div class=\"eh-mt-12\">\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-300\">Do you have any other concerns or issues related to your health and diet which have not\r\n        been covered in this questionnaire?</span>\r\n      <mat-radio-group class=\"e-mat-radio-group eh-ml-12\" formControlName=\"hasOtherConcerns\">\r\n        <mat-radio-button [value]=\"true\">Yes</mat-radio-button>\r\n        <mat-radio-button class=\"eh-ml-22\" [value]=\"false\">No</mat-radio-button>\r\n      </mat-radio-group>\r\n    </div>\r\n    <div class=\"eh-ml-12\">\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <mat-label>&nbsp;If so, please list and state the reasons</mat-label>\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"5\" cdkAutosizeMaxRows=\"5\"\r\n          formControlName=\"otherConcernsDesc\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"eh-w-100p\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n      <span class=\"eh-w-100p\">\r\n        Describe below a typical day's routine - specific to your food intake (with timings & quantities) and exercise\r\n        schedule:\r\n      </span>\r\n      <mat-form-field class=\"eh-w-100p\" floatLabel=\"never\">\r\n        <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"10\" cdkAutosizeMaxRows=\"10\"\r\n          formControlName=\"typicalDayRoutine\" class=\"eh-hide-scrollbars\"></textarea>\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-20\">\r\n    <mat-divider></mat-divider>\r\n  </div>\r\n  <div class=\"eh-mt-20\" fxLayout=\"row\" fxLayoutAlign=\"space-between start\">\r\n    <button mat-stroked-button (click)=\"onSubscribeLater()\">Save & Subscribe Later</button>\r\n    <button mat-flat-button (click)=\"onNext()\" [disabled]=\"!isStep2FormValid()\"\r\n      [ngClass]=\"!isStep2FormValid() ? 'e-mat-button e-mat-button--eblue400 eh-csr-dsb' : 'e-mat-button e-mat-button--eblue400'\">\r\n      Save & Next\r\n    </button>\r\n  </div>\r\n</ng-container>\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 32241:
/*!******************************************************************************!*\
  !*** ./src/app/shared/get-started/payment/payment.component.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n    <div class=\"eh-txt-bold eh-mt-24\">\r\n      Orientation\r\n    </div>\r\n    <p class=\"eh-mt-8 eh-txt-justify\">\r\n      Orientation is one of the key elements, which can have a significant effect on the quality of service and support.\r\n      MEALpyramid can provide to meet your specific goals. Orientation is applicable for all clients.\r\n    </p>\r\n    <ul>\r\n      <li>\r\n        <span class=\"eh-txt-bold\">First Appointment</span>, either at our center or virtual, is considered as <span\r\n          class=\"eh-txt-bold\">Orientation</span>.\r\n      </li>\r\n      <li>\r\n        During this appointment, <span class=\"eh-txt-bold eh-txt-underline\">NO DIET</span> will be prescribed. Kinita\r\n        will understand your health history, discuss your targets and further help you with realistic goal setting.\r\n      </li>\r\n      <li>\r\n        Based on that, Kinita will recommend <span class=\"eh-txt-bold\">Pathological Tests</span>, <span\r\n          class=\"eh-txt-bold\">Advanced Testing</span> along with a <span class=\"eh-txt-bold\">Suitable Package</span>.\r\n      </li>\r\n      <li><span class=\"eh-txt-bold\">All these details will be shared with you post Orientation on email</span>.</li>\r\n    </ul>\r\n    <div class=\"e-txt-primary eh-txt-bold eh-mt-16\">\r\n      &#8377;2,360/-\r\n    </div>\r\n    <div class=\"eh-txt-12 e-txt e-txt--matgrey700\">\r\n      Clients in India: Rates are inclusive of GST.\r\n    </div>\r\n    <div class=\"eh-txt-12 e-txt e-txt--matgrey700\">\r\n      International Clients: Rates are inclusive of Bank & Conversion Fees and Other Charges.\r\n    </div>\r\n    <div class=\"eh-txt-12 eh-txt-italic\">\r\n      Please scroll down to read the full terms and conditions.\r\n    </div>\r\n\r\n    <div class=\"eh-w-100p e-bg e-bg--eblue200 eh-mt-16 eh-p-16 e-brd e-brd--all e-brd--round1\">\r\n      <div class=\"eh-txt-center eh-txt-12\">\r\n        <mat-checkbox [formControl]=\"requiresGSTFC\">\r\n          I require a GST invoice for this payment\r\n        </mat-checkbox>\r\n      </div>\r\n      <div *ngIf=\"requiresGSTFC.value\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\" fxLayoutGap=\"16px\"\r\n        [formGroup]=\"gstFG\">\r\n        <mat-form-field class=\"e-mat-form-field eh-w-180 eh-mt-12\">\r\n          <mat-label>GST Number</mat-label>\r\n          <input matInput formControlName=\"number\">\r\n        </mat-form-field>\r\n\r\n        <mat-form-field class=\"e-mat-form-field eh-w-180 eh-mt-12\">\r\n          <mat-label>GST Billing Name</mat-label>\r\n          <input matInput formControlName=\"name\">\r\n        </mat-form-field>\r\n\r\n        <mat-form-field class=\"e-mat-form-field eh-w-280 eh-mt-12\">\r\n          <mat-label>GST Billing Address</mat-label>\r\n          <input matInput formControlName=\"address\">\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-12 eh-w-132\">\r\n      <button *ngIf=\"!loading\" mat-raised-button (click)=\"onCreatePayment()\"\r\n        [ngClass]=\"{'e-mat-button e-mat-button--eblue400 eh-w-100p': true}\">\r\n        Pay Now\r\n      </button>\r\n      <div *ngIf=\"loading\" class=\"e-spinner e-spinner--2 eh-h-36\">\r\n        <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"eh-mt-16 eh-txt-bold e-txt e-txt--matamber600\">OR</div>\r\n\r\n    <div class=\"eh-mt-12 eh-txt-bold\">Other Modes of Payment</div>\r\n    <table class=\"e-table\">\r\n      <tr>\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">NEFT / RTGS / IMPS</td>\r\n        <!-- <td class=\"eh-txt-center\" style=\"width: 20%;\">Credit / Debit Card</td> -->\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">VPA</td>\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">GPay / QR Code Scan</td>\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">Cheque Payment</td>\r\n      </tr>\r\n\r\n      <tr>\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n          <span class=\"eh-txt-bold\">MEALpyramid Nutrition and Wellness</span><br><br>\r\n          Account No: <span class=\"eh-txt-bold\">23905900140</span><br>\r\n          IFSC Code: <span class=\"eh-txt-bold\">SCBL0036058</span> (for rupee payments)<br>\r\n          SWIFT Code: <span class=\"eh-txt-bold\">SCBLINBBXXX</span> (for non-rupee payments)<br><br>\r\n          Bank: <span class=\"eh-txt-bold\">Standard Chartered Bank</span><br>\r\n          Branch: <span class=\"eh-txt-bold\">Juhu</span><br>\r\n          Type: <span class=\"eh-txt-bold\">Current / Domestic</span><br>\r\n          Entity Type: <span class=\"eh-txt-bold\">Partnership</span>\r\n        </td>\r\n\r\n        <!-- <td class=\"eh-txt-12\">\r\n          WhatsApp us on <span class=\"eh-txt-bold\">+91-892686118</span> and we will generate a link for you so you can\r\n          make the payment online using your Credit or Debit Card.<br><br>\r\n          <span class=\"e-txt e-txt-placeholder\">International customers can select the currency of their choice.</span>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <img class=\"eh-w-100 eh-mt-16\" src=\"../../../../assets/images/razorpay-logo-white.svg\" alt=\"RazorPay\">\r\n          </div>\r\n        </td> -->\r\n\r\n        <td class=\"eh-txt-12\">\r\n          Simply enter our VPA ID in any supported app.<br><br>\r\n          VPA ID:<br>\r\n          <span class=\"eh-txt-bold\">mealpyramid@sc</span><br><br>\r\n          Linked Number:<br>\r\n          <span class=\"eh-txt-bold\">+91-9819304049</span><br><br>\r\n        </td>\r\n\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12 eh-txt-center\" style=\"line-height: 1.4em;\">\r\n          <span>Scan the below QR code</span><br>\r\n          <img class=\"eh-w-144 eh-mt-12\" src=\"../../../../assets/images/payment-qr-code-2.png\" alt=\"QR Code\">\r\n          <div class=\"eh-mt-12\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\">\r\n            <img class=\"eh-w-64\" src=\"../../../../assets/images/google-pay-mark-logo.svg\" alt=\"GPay\">\r\n            <img class=\"eh-w-60\" src=\"../../../../assets/images/paytm-logo.svg\" alt=\"PayTM\">\r\n            <img class=\"eh-w-80\" src=\"../../../../assets/images/phone-pe-logo.svg\" alt=\"PhonePe\">\r\n          </div>\r\n          <!-- <div>\r\n            <img class=\"eh-w-100\" src=\"../../../../assets/images/bhim-upi-logo.png\" alt=\"BHIM UPI\">\r\n          </div> -->\r\n        </td>\r\n\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n          <span>Cheques must be drawn in favour of:</span><br>\r\n          <span>“<b>MEALpyramid Nutrition and Wellness</b>” and sent to:</span><br><br>\r\n          <b>MEALpyramid Nutrition and Wellness</b><br>\r\n          <span>1st Floor, Anuradha Satyamorthy Building,</span><br>\r\n          <span>#16 Vithal Nagar Society,</span><br>\r\n          <span>11th Road, Juhu Scheme,</span><br>\r\n          <span>Mumbai, 400049, INDIA.</span><br><br>\r\n          <span>Tel: +91-9892686118</span>\r\n        </td>\r\n      </tr>\r\n    </table>\r\n\r\n    <div class=\"eh-mt-8\">\r\n      Once paid, kindly send us remittance details and we will activate your account accordingly. If your preferred\r\n      mode of payment is other than credit card, kindly share your payment details. WhatsApp or email us the details and\r\n      we\r\n      shall activate the purchase from our side.\r\n    </div>\r\n  </div>\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"start start\" class=\"eh-mt-24\">\r\n    <div class=\"eh-mt-8\" fxFlex=\"0 0 30%\">\r\n      <div class=\"eh-txt-bold eh-px-2 eh-mb-8\">\r\n        ALL PACKAGES INCLUDE\r\n      </div>\r\n      <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n        <div class=\"eh-mt-6\">\r\n          <div class=\"eh-txt-bold\">\r\n            Complete Nutrition Analysis\r\n          </div>\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>\r\n              Health history analysis\r\n            </li>\r\n            <li>\r\n              Clinical analysis\r\n            </li>\r\n            <li>\r\n              Dietary supplements\r\n            </li>\r\n            <li>\r\n              Lifestyle modifications\r\n            </li>\r\n            <li>\r\n              Travel tips </li>\r\n            <li>\r\n              Guide to eating out</li>\r\n          </ul>\r\n        </div>\r\n        <div class=\"eh-mt-6\">\r\n          <div class=\"eh-txt-bold\">\r\n            Interpersonal Consultations\r\n          </div>\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>\r\n              Individualised nutrition plan\r\n            </li>\r\n            <li>\r\n              Realistic goal setting\r\n            </li>\r\n            <li>\r\n              Detailed diet discussions\r\n            </li>\r\n          </ul>\r\n        </div>\r\n        <div class=\"eh-mt-6\">\r\n          <div class=\"eh-txt-bold\">Follow-Up Consultations\r\n          </div>\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>\r\n              Target monitoring\r\n            </li>\r\n            <li>\r\n              Dietary changes\r\n            </li>\r\n            <li>\r\n              Constant monitoring\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-txt-bold eh-txt-italic eh-mt-10 eh-txt-12\">\r\n        <span class=\"eh-txt-underline\">NOTE</span> : Online services follow the\r\n        same protocol & charges\r\n      </div>\r\n      <div class=\" eh-txt-bold eh-mt-8 eh-txt-12\">\r\n        GSTIN: 27ABRFM9899F1Z6\r\n      </div>\r\n    </div>\r\n    <div class=\"eh-mt-8\" fxFlex=\"0 0 35%\">\r\n      <div class=\"eh-txt-bold eh-px-16 eh-mb-8\">MEALpyramid will:</div>\r\n      <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n        <ul class=\"eh-m-0 eh-px-16 \">\r\n          <li>\r\n            endeavour to provide you with the best nutritional advice that may help resolve weight and health problems\r\n            or improve your sporting performance; however, following the advice is ultimately at your discretion\r\n          </li>\r\n          <li>\r\n            not guarantee that you will meet your target, whatever that may be\r\n          </li>\r\n          <li>\r\n            retain the copyright of all diets and written material provided to the client </li>\r\n          <li>\r\n            reserve the right to terminate our services in the event of the client failing to comply with the\r\n            prescribed diet\r\n          </li>\r\n          <li>\r\n            not be liable under any circumstances for any health issues / complications arising during or after\r\n            clients start following our guidance\r\n          </li>\r\n          <li>\r\n            maintain client confidentiality at all times and details of your records will only be released to third\r\n            parties only upon your express written consent\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n    <div class=\"eh-mt-8\" fxFlex=\"0 0 35%\">\r\n      <div class=\"eh-txt-bold eh-px-16 eh-mb-8\">TERMS OF SERVICE</div>\r\n      <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n        <ul class=\"eh-m-0 eh-px-16\">\r\n          <li>Orientation fee is applicable for all clients (first visit charges / package renewal after long break).\r\n          </li>\r\n          <li>Health check is mandatory before starting the package.\r\n          </li>\r\n          <li>Entire package amount is payable upfront, prior to starting the consultation.</li>\r\n          <li>Amount once paid shall not be refundable under any circumstances.</li>\r\n          <li>All packages are non-transferable</li>\r\n          <li>No additional session / time extension shall be provided in case they lapse ; <span\r\n              class=\"eh-txt-underline\"> please make note of your package details carefully</span></li>\r\n          <li>All packages must be completed within the specified time period.</li>\r\n          <li>Upgrading of package midway is not permitted.</li>\r\n          <li>Children aged under 18 years must be accompanied by a parent or guardian.</li>\r\n          <li>Rates subject to change without prior notice. Rates at the time of joining shall only be applicable.\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>\r\n\r\n\r\n<!-- <tr>\r\n        <td class=\"eh-txt-center\" style=\"width: 33%;\">Bank Transfer – NEFT</td>\r\n        <td class=\"eh-txt-center\" style=\"width: 33%;\">BHIM UPI</td>\r\n        <td class=\"eh-txt-center\" style=\"width: 33%;\">Cheque</td>\r\n      </tr>\r\n      <tr>\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n          Account Name: Kinita Patel<br>\r\n          Bank Name: Citi Bank<br>\r\n          Branch: Fort, Mumbai – 400001<br>\r\n          Account No.: 0065599112<br>\r\n          IFSC Code: CITI0100000<br>\r\n          Swift Code: CITIINBXXXX<br>\r\n          PAN: AOHPK3967K<br>\r\n          GSTIN.: 27AOHPK3967K1ZE\r\n        </td>\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12 eh-txt-center\" style=\"line-height: 1.4em;\">\r\n          <span>Scan & pay with any BHIM UPI app</span><br>\r\n          <img class=\"eh-w-144 eh-mt-12\" src=\"../../../../assets/images/payment-qr-code.png\" alt=\"QR Code\">\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <img class=\"eh-w-64\" src=\"../../../../assets/images/google-pay-mark-logo.svg\" alt=\"GPay\">\r\n            <img class=\"eh-w-60 eh-ml-8\" src=\"../../../../assets/images/paytm-logo.svg\" alt=\"PayTM\">\r\n            <img class=\"eh-w-80 eh-ml-8\" src=\"../../../../assets/images/phone-pe-logo.svg\" alt=\"PhonePe\">\r\n          </div>\r\n          <div>\r\n            <img class=\"eh-w-100\" src=\"../../../../assets/images/bhim-upi-logo.png\" alt=\"BHIM UPI\">\r\n          </div>\r\n          <div class=\"eh-mt-12\">UPI ID: mealpyramid@citi</div>\r\n        </td>\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n          <span>Cheques must be drawn in favour of:</span><br>\r\n          <span>“<b>Kinita Vishal Patel</b>” and sent to:</span><br><br>\r\n          <b>MEALpyramid by Kinita Kadakia Patel</b><br>\r\n          <span>1st Floor, Anuradha Satyamorthy Building,</span><br>\r\n          <span>#16 Vithal Nagar, Society,</span><br>\r\n          <span>New Indian Society Rd, JVPD Scheme,</span><br>\r\n          <span>Mumbai, 400049, INDIA.</span><br><br>\r\n          <span>Tel: +91-9892686118</span>\r\n        </td>\r\n      </tr> -->";

/***/ }),

/***/ 98139:
/*!************************************************************************************************!*\
  !*** ./src/app/shared/get-started/personal-details/personal-details.component.html?ngResource ***!
  \************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\" [formGroup]=\"step1FormGroup\">\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">contacts</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Basic</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\" fxLayout=\"row\" formGroupName=\"personalDetails\">\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>First Name<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"firstName\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-ml-20\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>Last Name<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"lastName\">\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start end\">\r\n        <div class=\"eh-ml-8\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>Date of Birth<sup>*</sup></mat-label>\r\n            <input matInput readonly [matDatepicker]=\"picker\" [max]=\"maxDate\" [value]=\"dob\"\r\n              (dateInput)=\"onDOBSet($event)\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"picker\" class=\"e-mat-datepicker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #picker></mat-datepicker>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-ml-20\" fxLayout=\"column\">\r\n          <span class=\"eh-txt-12 eh-mb-4\">Gender<sup>*</sup></span>\r\n          <div style=\"padding-bottom: 1.34375em;\">\r\n            <mat-button-toggle-group class=\"e-mat-button-toggle\" formControlName=\"gender\">\r\n              <mat-button-toggle [checked]=\"gender === genderTypes.MALE\" [value]=\"genderTypes.MALE\">\r\n                Male\r\n              </mat-button-toggle>\r\n              <mat-button-toggle [checked]=\"gender === genderTypes.FEMALE\" [value]=\"genderTypes.FEMALE\">\r\n                Female\r\n              </mat-button-toggle>\r\n            </mat-button-toggle-group>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div class=\"eh-w-300\">\r\n        <div>Profile Picture</div>\r\n        <app-input-file-preview [uploadByName]=\"me.name\" [uploadByUID]=\"me.uid\"\r\n          [uploadCollectionName]=\"uploadCollectionName\" [uploadDirPath]=\"uploadDirPath\"\r\n          [uploadFileNamePrefix]=\"uploadFileNamePrefix\" [userMembershipKey]=\"me.membershipKey\" [userUID]=\"me.uid\"\r\n          (availableEvent)=\"availableEvent($event)\" (uploadEvent)=\"uploadEvent($event)\">\r\n        </app-input-file-preview>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">contact_mail</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Contact</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\" fxLayout=\"row\" formGroupName=\"personalDetails\">\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8 eh-w-300\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--full-width\">\r\n            <mat-label>Address Line 1<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"addressLine1\">\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8 eh-w-300\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--full-width\">\r\n            <mat-label>Address Line 2</mat-label>\r\n            <input matInput formControlName=\"addressLine2\">\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>City<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"city\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-ml-20\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>Pincode<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"pincode\">\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>State<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"state\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-ml-20\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>Country<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"country\">\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--2\">\r\n            <mat-label>Mobile Phone<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"mobile\">\r\n            <mat-hint>+91-XXXXXXXXXX</mat-hint>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n        <div class=\"eh-ml-8 eh-w-300\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--full-width\">\r\n            <mat-label>Email<sup>*</sup></mat-label>\r\n            <input matInput readonly formControlName=\"email\">\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">tune</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Preferences</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\" fxLayout=\"row\" formGroupName=\"personalDetails\">\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8 eh-w-300\">\r\n          <div>Category<sup>*</sup></div>\r\n          <div class=\"eh-px-8\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\"\r\n              formControlName=\"category\" (change)=\"setSubCategories()\">\r\n              <mat-radio-button class=\"eh-mt-8\" *ngFor=\"let c of categories\" [value]=\"c\" fxFlex=\"0 0 60%\">{{c}}\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\">\r\n        <div *ngIf=\"categoryFC.value\" class=\"eh-ml-8 eh-w-100p\">\r\n          <div>Service Type<sup>*</sup></div>\r\n          <div class=\"eh-px-8\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\"\r\n              formControlName=\"serviceType\">\r\n              <mat-radio-button class=\"eh-mt-8\" *ngFor=\"let s of subCategories\" fxFlex=\"0 0 50%\" [value]=\"s\">{{s}}\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-24\" fxLayout=\"row\" formGroupName=\"personalDetails\">\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\">\r\n        <div class=\"eh-ml-8 eh-w-300\">\r\n          <div>Preferred Location<sup>*</sup></div>\r\n          <div class=\"eh-px-8\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n            <mat-radio-group class=\"e-mat-radio-group\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\"\r\n              formControlName=\"preferredLocation\">\r\n              <mat-radio-button class=\"eh-mt-8\" *ngFor=\"let l of locations\" fxFlex=\"0 0 50%\" [value]=\"l\"\r\n                [checked]=\"isPreferredLocationChecked(l)\">{{l.name}}\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div fxFlex=\"0 0 50%\">\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start start\">\r\n        <div class=\"eh-ml-8 eh-w-100p\">\r\n          <div>Target</div>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <mat-form-field class=\"eh-w-100p\">\r\n              <textarea matInput cdkTextareaAutosize cdkAutosizeMinRows=\"5\" formControlName=\"target\" maxLength=\"50\"\r\n                #message></textarea>\r\n              <mat-hint align=\"end\">{{message.value.length}} / 50</mat-hint>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"e-bg-eblue250 eh-mt-16 eh-p-8\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2 eh-mr-8 e-txt-eblue700\">contacts</mat-icon>\r\n    <span class=\"e-txt-eblue700\">Referral</span>\r\n  </div>\r\n  <div class=\"eh-mt-16\" fxLayout=\"row\" formGroupName=\"personalDetails\">\r\n    <div class=\"eh-ml-8\" fxFlex=\"0 0 50%\">\r\n      <mat-form-field appearance=\"standard\" class=\"e-mat-form-field e-mat-form-field--full-width\">\r\n        <mat-label>Referred by (name/email/mobile)</mat-label>\r\n        <input matInput formControlName=\"referredBy\">\r\n      </mat-form-field>\r\n    </div>\r\n  </div>\r\n  <div class=\"eh-mt-20\">\r\n    <mat-divider></mat-divider>\r\n  </div>\r\n  <div class=\"eh-mt-20\" fxLayout=\"row\" fxLayoutAlign=\"space-between start\">\r\n    <button mat-stroked-button (click)=\"onSubscribeLater()\">Save & Subscribe Later</button>\r\n    <button mat-flat-button (click)=\"onNext()\" [disabled]=\"!isStep1FormValid()\"\r\n      [ngClass]=\"!isStep1FormValid() ? 'e-mat-button e-mat-button--eblue400 eh-csr-dsb' : 'e-mat-button e-mat-button--eblue400'\">\r\n      Save & Next\r\n    </button>\r\n  </div>\r\n</ng-container>\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 61102:
/*!****************************************************************************************!*\
  !*** ./src/app/shared/input-file-preview/input-file-preview.component.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<input type=\"file\" #fileUpload class=\"eh-d-none\" [accept]=\"acceptedFileTypes\" [disabled]=\"disabled || uploading\"\r\n  (change)=\"onFileAvailable($event)\">\r\n\r\n<a class=\"eu-mt-4 eh-txt-underline e-txt e-txt--matamber600\" href=\"javascript: void(0);\"\r\n  (click)=\"fileUpload.click()\">Choose File</a>\r\n\r\n<div *ngIf=\"!imgURL\" class=\"eh-w-80 eh-h-80 eh-mt-4 eh-px-2 e-bg e-bg--eblue100 e-brd e-brd--all\" fxLayout=\"row\"\r\n  fxLayoutAlign=\"center center\">\r\n  <span class=\"eh-txt-10 eh-txt-bold e-txt e-txt--matgrey500\">PREVIEW</span>\r\n</div>\r\n\r\n<div *ngIf=\"imgURL\" class=\"eh-mt-4\" fxLayout=\"row\" fxLayoutAlign=\"start start\">\r\n  <div class=\"eh-w-80 eh-h-80 eh-px-2 eh-xy-center e-brd e-brd--all\">\r\n    <img class=\"eh-fit\" [src]=\"imgURL\" alt=\"Profile Picture\">\r\n  </div>\r\n\r\n  <div *ngIf=\"isUploaded\" class=\"eh-ml-8 e-txt e-txt--placeholder\">\r\n    File accepted.<br>Please save.\r\n  </div>\r\n\r\n  <div *ngIf=\"!isUploaded\" class=\"eh-w-76 eh-ml-8\">\r\n    <app-button buttonStyle=\"e-mat-button--eblue400\" title=\"Upload\" [loading]=\"uploading\"\r\n      [disabled]=\"disabled || uploading\" (onButtonClick)=\"onFileUpload()\">\r\n    </app-button>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 1269:
/*!****************************************************************!*\
  !*** ./src/app/shared/layout/layout.component.html?ngResource ***!
  \****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"!loading; else loadingBlk\">\r\n  \r\n</ng-container>\r\n\r\n<ng-template #loadingBlk>\r\n  <div class=\"e-spinner e-spinner--full-screen eh-xy-center\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 13281:
/*!****************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/appointment-doc/appointment-doc.component.html?ngResource ***!
  \****************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div class=\"eh-px-8\">\r\n  <div *ngIf=\"appointment\" class=\"e-card2 e-card2--hover2 eh-mx-auto eh-py-8 eh-mx-4\" fxLayout=\"row\"\r\n    fxLayoutAlign=\"space-evenly\">\r\n    <div class=\"eh-w-56 eh-h-60\" fxLayout=\"column\" fxLayoutAlign=\"space-between center\">\r\n      <span>\r\n        {{appointment.sessionStart | date:'EEE':'+0530'}}\r\n      </span>\r\n      <span class=\"eh-txt-16 \">\r\n        {{appointment.sessionStart | date:'d':'+0530'}}\r\n      </span>\r\n      <span>\r\n        {{appointment.sessionStart | date:'MMM':'+0530'}}\r\n      </span>\r\n    </div>\r\n    <div class=\"eh-w-120 eh-h-106 eh-pl-16 e-brd e-brd--left \" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n      <div fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--5\">schedule</mat-icon>\r\n        <span class=\"eh-ml-4 eh-txt-12\">\r\n          {{appointment.sessionStart | date:'H:mm':'+0530'}}-{{appointment.sessionEnd | date:'H:mm':'+0530'}}\r\n        </span>\r\n      </div>\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start space-between\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--5\">location_on</mat-icon>\r\n        <span class=\"eh-ml-4 eh-txt-12\">{{appointment.location.name}}</span>\r\n      </div>\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n        <mat-icon class=\"e-mat-icon e-mat-icon--5\">category</mat-icon>\r\n        <span *ngIf=\"appointment.type === appointmentTypes.ORIENTATION\"\r\n          class=\"eh-ml-4 e-tag e-tag--matblue500 eh-txt-capital eh-txt-lh1_2\">{{appointment.type}}\r\n        </span>\r\n        <span *ngIf=\"appointment.type === appointmentTypes.RECALL\"\r\n          class=\"eh-ml-4 e-tag e-tag--matcyan500 eh-txt-capital eh-txt-lh1_2\">{{appointment.type}}\r\n        </span>\r\n        <span *ngIf=\"appointment.type === appointmentTypes.ADVANCED_TESTING\"\r\n          class=\"eh-ml-4 e-tag e-tag--matpurple400 eh-txt-capital eh-txt-lh1_2\">Testing\r\n        </span>\r\n      </div>\r\n    </div>\r\n    <div class=\"eh-pr-6\" fxLayout=\"col\">\r\n      <div fxFlex=\"none\">\r\n        <app-avatar [userUID]=\"appointment.staff.uid\" [size]=\"'medium'\" [border]=\"true\"\r\n          matTooltip=\"{{appointment.staff.name}}\"></app-avatar>\r\n      </div>\r\n      <div class=\"eh-h-72\" fxLayout=\"column\" fxLayoutAlign=\"space-between\">\r\n        <div class=\"eh-ml-6\" fxLayout=\"column\" fxLayoutAlign=\"strecth\">\r\n          <div class=\"eh-txt-10 eh-txt\">{{appointment.staff.name}}</div>\r\n          <div *ngIf=\"appointment.type !== appointmentTypes.ADVANCED_TESTING\" class=\"eh-txt-10\">Nutritionist</div>\r\n          <div *ngIf=\"appointment.type === appointmentTypes.ADVANCED_TESTING\" class=\"eh-txt-10\">Test Technician</div>\r\n        </div>\r\n        <div *ngIf=\"showCancel\" class=\"eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"start\">\r\n          <span class=\"e-txt e-txt--placeholder eh-txt-italic\">Mark as</span>\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--2 e-mat-icon--error eh-csr-ptr eh-ml-8\" #tooltip=\"matTooltip\"\r\n            matTooltip=\"Cancel Appointment\" matTooltipPosition=\"below\" (click)=\"onCancelClick()\">\r\n            cancel\r\n          </mat-icon>\r\n        </div>\r\n        <div *ngIf=\"appointment.isCompleted\" class=\"eh-w-100p\" fxLayout=\"row\" fxLayoutAlign=\"flex-end center\">\r\n          <div *ngIf=\"appointment.status === appointmentStatuses.ATTENDED\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <span class=\"eh-txt-12 e-txt e-txt--matlightgreen800\">Attended</span>\r\n            <div class=\"e-tag e-tag--attended eh-ml-4\" matTooltip=\"Attended\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done_outline</mat-icon>\r\n            </div>\r\n          </div>\r\n          <div *ngIf=\"appointment.status === appointmentStatuses.NO_SHOW\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <span class=\"eh-txt-12 e-txt e-txt--warn\">No-show</span>\r\n            <div class=\"e-tag e-tag--no-show eh-ml-4\" matTooltip=\"No-show\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">person_off</mat-icon>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 93301:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/avatar/avatar.component.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"photoURL; else noPhotoURLBlk\">\r\n  <img *ngIf=\"zIndex\" src=\"{{photoURL}}\"\r\n    [ngClass]=\"{'e-avatar': true, 'e-avatar--small': size === 'small', 'e-avatar--medium': size === 'medium', 'e-avatar--border': border}\"\r\n    [ngStyle]=\"{'z-index': zIndex}\">\r\n  <img *ngIf=\"!zIndex\" src=\"{{photoURL}}\"\r\n    [ngClass]=\"{'e-avatar': true, 'e-avatar--small': size === 'small', 'e-avatar--medium': size === 'medium', 'e-avatar--border': border}\">\r\n</ng-container>\r\n<ng-template #noPhotoURLBlk>\r\n  <div [ngClass]=\"['e-avatar', avatarSize]\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--2\">person</mat-icon>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 17418:
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/book-appointment/book-appointment.component.html?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-h-100p eh-p-6 eh-ofy-auto eh-hide-scrollbars\" fxLayout=\"column\">\r\n    <div class=\"eh-ofy-scroll eh-hide-scrollbars\" fxLayout=\"column\" fxLayoutAlign=\"space-between\">\r\n      <div fxLayout=\"column\" [formGroup]=\"searchFG\">\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n          <mat-label>Select type</mat-label>\r\n          <mat-select formControlName=\"type\" placeholder=\"Select\">\r\n            <mat-option *ngFor=\"let type of appointmentTypes\" [value]=\"type\">\r\n              {{type}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n          <mat-label>Select a location</mat-label>\r\n          <mat-select formControlName=\"location\" placeholder=\"Select\">\r\n            <mat-option *ngFor=\"let location of locations\" class=\"eh-txt-capital\" [value]=\"location\">\r\n              {{location.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n        <mat-form-field appearance=\"standard\" class=\"e-mat-form-field\">\r\n          <mat-label>Choose a date</mat-label>\r\n          <input matInput formControlName=\"date\" [min]=\"startDate\" [max]=\"endDate\" [matDatepicker]=\"datepicker\"\r\n            readonly>\r\n          <mat-datepicker-toggle matSuffix [for]=\"datepicker\"></mat-datepicker-toggle>\r\n          <mat-datepicker #datepicker></mat-datepicker>\r\n        </mat-form-field>\r\n\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center\" class=\"eh-w-150 eh-pb-10\">\r\n          <button *ngIf=\"!loadingSlots\" mat-flat-button (click)=\"onSearch()\" [disabled]=\"!isSearchFGValid()\"\r\n            [ngClass]=\"{'e-mat-button e-mat-button--eblue400 eh-w-100p eh-mt-16': true, 'eh-csr-dsb': !isSearchFGValid()}\">\r\n            Get Availability\r\n          </button>\r\n          <div *ngIf=\"loadingSlots\" class=\"e-spinner e-spinner--2 eh-mt-16 eh-h-36\">\r\n            <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"e-card2 e-card2--bg-eblue100 eh-py-16 eh-ofy-scroll eh-hide-scrollbars\">\r\n        <div *ngIf=\"!loadingSlots && isSearchPristine\" class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Search availability to load details\r\n        </div>\r\n\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && !areSlotsAvailable\"\r\n          class=\"e-txt e-txt--placeholder eh-txt-center\">\r\n          Sorry, no slots are available on this day\r\n        </div>\r\n\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && areSlotsAvailable\" class=\"eh-txt-12 eh-txt-center\">\r\n          Your\r\n          <span class=\"eh-txt-capital\">{{typeFC.value}}</span>\r\n          appointment is with\r\n          <span>{{slots[0].staff.name}}</span>. This is her calendar.\r\n        </div>\r\n\r\n        <div *ngIf=\"!loadingSlots && !isSearchPristine && areSlotsAvailable\" fxLayout=\"row wrap\"\r\n          fxLayoutAlign=\"space-evenly\" class=\"eh-h-80\">\r\n          <div *ngFor=\"let item of slots\" (click)=\"onSlotClicked(item)\"\r\n            class=\"e-card2 e-card2--appt2 e-card2--hover1 eh-pos-relative eh-ofy-auto\">\r\n            <span class=\"eh-txt-center\">\r\n              {{item.sessionStart | date:'h:mm a':'+0530'}} - {{item.sessionEnd | date:'h:mm a':'+0530'}}\r\n            </span>\r\n            <div *ngIf=\"item.clicked\" class=\"eh-pos-absolute\" style=\"right: 1px; top: 1px\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--5\">check_circle</mat-icon>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <mat-divider fxFlex=\"0 0 auto\" class=\"eh-mt-16\"></mat-divider>\r\n\r\n    <div class=\"eh-mt-16\" fxLayout=\"column\" fxLayoutAlign=\"space-between\">\r\n      <div class=\"eh-p-6\">\r\n        <div class=\"eh-txt-12 eh-txt-lh1_2\">\r\n          For users of the <span class=\"eh-txt-underline\">Standard package</span>:\r\n        </div>\r\n        <div class=\"eh-txt-12 eh-txt-lh1_2 eh-txt-italic\">\r\n          Only 1 appointment can be scheduled at a time.\r\n        </div>\r\n\r\n        <div class=\"eh-mt-6 eh-txt-12 eh-txt-lh1_2\">\r\n          For users of the <span class=\"eh-txt-underline\">Premium package</span>:\r\n        </div>\r\n        <div class=\"eh-txt-12 eh-txt-lh1_2 eh-txt-italic\">\r\n          Up to 3 appointments can be scheduled at a time.\r\n        </div>\r\n\r\n        <div class=\"eh-mt-6 eh-txt-12 eh-txt-lh1_2\">\r\n          For users of the <span class=\"eh-txt-underline\">Prestige package</span>:\r\n        </div>\r\n        <div class=\"eh-txt-12 eh-txt-lh1_2 eh-txt-italic\">\r\n          Unlimited appointments can be scheduled at a time.\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-h-36 eh-mt-10\" fxLayout=\"row\" fxLayoutAlign=\"center\">\r\n        <button *ngIf=\" !bookingAppt\" mat-flat-button (click)=\"onBookAppointment()\" [disabled]=\"!selectedSlot\"\r\n          [ngClass]=\"{'e-mat-button e-mat-button--eblue400': true, 'eh-csr-dsb': !selectedSlot}\">\r\n          Schedule Appointment\r\n        </button>\r\n\r\n        <div *ngIf=\"bookingAppt\" class=\"e-spinner e-spinner--2\">\r\n          <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 84075:
/*!********************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/button-icon-title/button-icon-title.component.html?ngResource ***!
  \********************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div class=\"eh-w-100p eh-h-100p eh-xy-center eh-minw-56\">\r\n  <button *ngIf=\"!showSpinner\" class=\"e-btn eh-w-100p eh-h-100p\" [ngClass]=\"buttonCSS\" fxLayout=\"row\"\r\n    fxLayoutAlign=\"start center\" [matTooltip]=\"toolTip\" [disabled]=\"disabled\" (click)=\"onClick($event)\">\r\n    <mat-icon class=\"e-mat-icon\" [ngClass]=\"iconCSS\">{{icon}}</mat-icon>\r\n\r\n    <span class=\"eh-txt-underline eh-ml-4\" [ngClass]=\"titleCSS\">{{title}}</span>\r\n  </button>\r\n\r\n  <div *ngIf=\"showSpinner\" class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"18\" [strokeWidth]=\"2\"></mat-spinner>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 36961:
/*!********************************************************************************************!*\
  !*** ./src/app/shared/shared-components/button-icon/button-icon.component.html?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div class=\"eh-w-100p eh-h-100p eh-xy-center eh-minw-28 eh-minh-28\">\r\n  <button *ngIf=\"!showSpinner\" mat-icon-button class=\"e-mat-icon-button eh-w-100p eh-h-100p eh-minw-28 eh-minh-28\"\r\n    [matTooltip]=\"toolTip\" [disabled]=\"disabled\" (click)=\"onClick($event)\">\r\n    <mat-icon class=\"e-mat-icon\" [ngClass]=\"iconCSS\">{{icon}}</mat-icon>\r\n  </button>\r\n\r\n  <div *ngIf=\"showSpinner\" class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v\">\r\n    <mat-spinner [diameter]=\"18\" [strokeWidth]=\"2\"></mat-spinner>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 74341:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/button/button.component.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<!-- <div *ngIf=\"!showSpinner\" class=\"eh-w-100p eh-h-100p eh-xy-center\" style=\"min-width: 64px;\">\r\n  <button mat-flat-button [disabled]=\"disabled\" [ngClass]=\"buttonCSS\" (click)=\"onClick($event)\">\r\n    {{title}}\r\n  </button>\r\n</div>\r\n<div *ngIf=\"showSpinner\" class=\"eh-w-100p eh-h-100p eh-xy-center\" style=\"min-width: 64px;\">\r\n  <div class=\"e-spinner e-spinner--2\">\r\n    <mat-spinner [diameter]=\"20\" [strokeWidth]=\"2\"></mat-spinner>\r\n  </div>\r\n</div> -->\r\n\r\n<div class=\"eh-w-100p eh-h-100p eh-xy-center eh-minw-64\">\r\n  <button *ngIf=\"!showSpinner\" mat-flat-button class=\"eh-w-100p eh-h-100p\" [disabled]=\"disabled\" [ngClass]=\"buttonCSS\"\r\n    (click)=\"onClick($event)\">\r\n    {{title}}\r\n  </button>\r\n\r\n  <div *ngIf=\"showSpinner\" class=\"e-spinner e-spinner--3 e-spinner--center eh-h-100v eh-px-12 eh-py-6\">\r\n    <mat-spinner [diameter]=\"20\" [strokeWidth]=\"2\"></mat-spinner>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 31703:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/footer/footer.component.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-tabs>\r\n  <ion-tab-bar slot=\"bottom\">\r\n    <ion-tab-button (click)=\"goto(routeIDs.HOME)\" [disabled]=\"!isPackageSubscribed\">\r\n      <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n      <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Home</ion-label>\r\n      <ion-icon name=\"home\"></ion-icon>\r\n    </ion-tab-button>\r\n    <ion-tab-button (click)=\"goto(routeIDs.DIETS)\" [disabled]=\"!isPackageSubscribed\">\r\n      <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n      <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Diet</ion-label>\r\n      <ion-icon name=\"fast-food-outline\"></ion-icon>\r\n    </ion-tab-button>\r\n    <ion-tab-button (click)=\"goto(routeIDs.PROGRESS)\" [disabled]=\"!isPackageSubscribed\">\r\n      <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n      <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Progress</ion-label>\r\n      <ion-icon name=\"pulse-outline\"></ion-icon>\r\n    </ion-tab-button>\r\n    <ion-tab-button (click)=\"goto(routeIDs.APPOINTMENTS)\" [disabled]=\"!isPackageSubscribed\">\r\n      <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n      <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Appointment</ion-label>\r\n      <ion-icon name=\"calendar\"></ion-icon>\r\n    </ion-tab-button>\r\n    <ion-tab-button (click)=\"goto(routeIDs.HEALTH_HISTORY)\" [disabled]=\"!isPackageSubscribed\">\r\n      <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n      <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Health History</ion-label>\r\n      <ion-icon name=\"reader-outline\"></ion-icon>\r\n    </ion-tab-button>\r\n  </ion-tab-bar>\r\n</ion-tabs>";

/***/ }),

/***/ 85309:
/*!**********************************************************************************!*\
  !*** ./src/app/shared/shared-components/header/header.component.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-header class=\"ion-no-border\" [translucent]=\"true\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" *ngIf=\"show_footer\">\r\n      <ion-back-button defaultHref={{backButtonURL}}></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-padding\">{{title}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <app-profile-avatar></app-profile-avatar>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>";

/***/ }),

/***/ 63552:
/*!******************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/preview-download/preview-download.component.html?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchFileView; else loadingFileViewBlk\">\r\n  <div class=\"eh-h-100p\" fxLayout=\"column\">\r\n    <ng-container *ngIf=\"isFileAvailable\">\r\n      <div class=\"eh-h-68 e-brd e-brd--bottom eh-p-16\" fxFlex=\"0 0 auto\" fxLayout=\"row\"\r\n        fxLayoutAlign=\"space-between center\">\r\n        <div class=\"eh-txt-bold eh-txt-breakword eh-px-2\">{{title}}</div>\r\n        <div *ngIf=\"allowReplace\" fxLayout=\"row\">\r\n          <div class=\"eh-w-76\">\r\n            <input type=\"file\" #fileUpload class=\"eh-d-none\" accept=\"{{acceptedFileTypes}}\"\r\n              (change)=\"onUploadFile($event, true)\">\r\n            <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Replace'\" [loading]=\"uploading || deleting\"\r\n              [disabled]=\"uploading || deleting\" (onButtonClick)=\"fileUpload.click()\">\r\n            </app-button>\r\n          </div>\r\n\r\n          <div class=\"eh-w-92 eh-ml-16\">\r\n            <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Download'\" [loading]=\"downloading\"\r\n              [disabled]=\"uploading || deleting\" (click)=\"onDownloadFile()\">\r\n            </app-button>\r\n          </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"allowDelete\" fxLayout=\"row\">\r\n          <div class=\"eh-w-76 eh-ml-2\">\r\n            <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Delete'\" [loading]=\"deleting\"\r\n              [disabled]=\"uploading || deleting\" (onButtonClick)=\"onDeleteFile()\">\r\n            </app-button>\r\n          </div>\r\n\r\n          <div class=\"eh-w-92 eh-h-30 eh-ml-16\">\r\n            <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Download'\" [loading]=\"downloading\"\r\n              [disabled]=\"uploading || deleting\" (click)=\"onDownloadFile()\">\r\n            </app-button>\r\n          </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"!allowReplace && !allowDelete\">\r\n          <div class=\"eh-w-92 eh-h-30\">\r\n            <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Download'\" [loading]=\"downloading\"\r\n              [disabled]=\"uploading || deleting\" (click)=\"onDownloadFile()\">\r\n            </app-button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"eh-p-8 eh-ofy-scroll eh-hide-scrollbars\" fxFlex=\"1 0 0%\">\r\n        <pdf-viewer *ngIf=\"!isImg\" [original-size]=\"false\" [fit-to-page]=\"true\" src=\"{{fileURL}}\">\r\n        </pdf-viewer>\r\n        <img *ngIf=\"isImg\" class=\"eh-fit\" src=\"{{fileURL}}\" alt=\"Image\">\r\n      </div>\r\n    </ng-container>\r\n\r\n    <ng-container *ngIf=\"!isFileAvailable\">\r\n      <div class=\"eh-h-68 e-brd e-brd--bottom eh-p-16\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\"\r\n        fxFlex=\"0 0 auto\">\r\n        <span class=\"eh-txt-bold eh-txt-breakword\">{{title}}</span>\r\n\r\n        <div *ngIf=\"allowUpload\" class=\"eh-w-72\">\r\n          <input type=\"file\" #fileUpload class=\"eh-d-none\" accept=\"{{acceptedFileTypes}}\"\r\n            (change)=\"onUploadFile($event)\">\r\n          <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Upload'\" [loading]=\"uploading\"\r\n            [disabled]=\"uploading || deleting\" (onButtonClick)=\"fileUpload.click()\">\r\n          </app-button>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"e-txt e-txt--placeholder eh-txt-center eh-mt-16\">\r\n        {{placeholder}}\r\n      </div>\r\n    </ng-container>\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #loadingFileViewBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 42788:
/*!**************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/profile-avatar/profile-avatar.component.html?ngResource ***!
  \**************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-list no-lines lines=\"none\" *ngIf=\"dispatchView\">\r\n  <ion-item no-lines lines=\"none\">\r\n    <div>\r\n      <mat-icon class=\"e-mat-icon e-mat-icon--1 eh-mr-8\" (click)=\"goto('/tab/p/notification')\">\r\n        notifications_active\r\n      </mat-icon>\r\n    </div>\r\n    <div (click)=\"goto('/tab/profile')\">\r\n      <app-avatar class=\"eh-h-36\" [size]=\"'medium'\" [userUID]=\"me.uid\" [border]=\"true\">\r\n      </app-avatar>\r\n    </div>\r\n    <!-- <div [matMenuTriggerFor]=\"menu\">\r\n      <app-avatar *ngIf=\"me\" class=\"eh-h-36\" [size]=\"'medium'\" [userUID]=\"me.uid\" [border]=\"true\">\r\n      </app-avatar>\r\n    </div> -->\r\n  </ion-item>\r\n</ion-list>\r\n<!-- <mat-menu #menu=\"matMenu\" xPosition=\"before\">\r\n  <button *ngIf=\"userHasSubcribedPackage\" mat-menu-item (click)=\"goto(routeIDs.PROFILE)\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--3\">manage_accounts</mat-icon>\r\n    <span>Profile</span>\r\n  </button>\r\n  <button mat-menu-item (click)=\"onLogout()\">\r\n    <mat-icon class=\"e-mat-icon e-mat-icon--3\">logout</mat-icon>\r\n    <span>Log out</span>\r\n  </button>\r\n</mat-menu> -->";

/***/ }),

/***/ 18431:
/*!************************************************************************************************************!*\
  !*** ./src/app/shared/shared-components/recommended-package/recommended-package.component.html?ngResource ***!
  \************************************************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-px-16\" fxLayout=\"column\" fxLayoutAlign=\"start center\">\r\n    <div class=\"eh-txt-bold eh-mt-16\">Suggested Package by Kinita</div>\r\n\r\n    <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-200 eh-mt-24\">\r\n      <mat-label>Select</mat-label>\r\n      <mat-select [formControl]=\"numberOfSessionsFC\" placeholder=\"Select\"\r\n        (selectionChange)=\"onNumberOfSessionsSelect($event.value)\">\r\n        <mat-option *ngFor=\"let i of numberOfSessionsArr\" class=\"eh-txt-capital\" [value]=\"i.sessions\">\r\n          {{i.name}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n\r\n    <div class=\"eh-txt-italic eh-txt-center eh-txt-12\">\r\n      Please scroll down for full comparison of the packages. You can select any package from the following.\r\n    </div>\r\n\r\n    <div class=\"eh-w-100p e-bg e-bg--eblue200 eh-mt-16 eh-p-16 e-brd e-brd--all e-brd--round1\">\r\n      <div class=\"eh-txt-center eh-txt-12\">\r\n        <mat-checkbox [formControl]=\"requiresGSTFC\">\r\n          I require a GST invoice for this payment\r\n        </mat-checkbox>\r\n      </div>\r\n      <div *ngIf=\"requiresGSTFC.value\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\" fxLayoutGap=\"16px\"\r\n        [formGroup]=\"gstFG\">\r\n        <mat-form-field class=\"e-mat-form-field eh-w-180 eh-mt-12\">\r\n          <mat-label>GST Number</mat-label>\r\n          <input matInput formControlName=\"number\">\r\n        </mat-form-field>\r\n\r\n        <mat-form-field class=\"e-mat-form-field eh-w-180 eh-mt-12\">\r\n          <mat-label>GST Billing Name</mat-label>\r\n          <input matInput formControlName=\"name\">\r\n        </mat-form-field>\r\n\r\n        <mat-form-field class=\"e-mat-form-field eh-w-280 eh-mt-12\">\r\n          <mat-label>GST Billing Address</mat-label>\r\n          <input matInput formControlName=\"address\">\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"eh-w-90p eh-mt-40\" fxLayout=\"row\" fxLayoutAlign=\"center\">\r\n      <div *ngFor=\"let pkg of shownPackages\"\r\n        class=\"e-card2 e-card2--hover1 eh-pos-relative eh-w-212 eh-ml-20 eh-pt-16 eh-pb-24 eh-px-16\">\r\n        <div class=\"eh-txt-uppercase eh-txt-12 eh-txt-bold\">{{pkg.type}}</div>\r\n        <!-- <div class=\"eh-txt-20 eh-txt-bold eh-mt-8\">&#8377;{{pkg.price.amount | number}}</div>\r\n        <div class=\"eh-txt-12 eh-mt-4 eh-txt-italic\">\r\n          (Price with GST &#8377;{{pkg.price.amountWithTax | number}})\r\n        </div> -->\r\n        <div class=\"eh-txt-20 eh-txt-bold eh-mt-8\">&#8377;{{pkg.price.amountWithTax | number}}</div>\r\n        <div class=\"eh-txt-12 eh-mt-4 eh-txt-italic\">\r\n          (Price with GST)\r\n        </div>\r\n        <div class=\"eh-mt-16\">{{pkg.name}}</div>\r\n        <div class=\"eh-mt-16\">{{pkg.numberOfSessions}} sessions</div>\r\n        <div>in</div>\r\n        <div>{{pkg.numberOfMonths}} months</div>\r\n        <div class=\"eh-txt-italic eh-txt-12 e-txt e-txt--matgrey600\">(whichever finishes first)</div>\r\n        <div class=\"eh-w-180 eh-h-36 eh-mt-16\">\r\n          <app-button [buttonStyle]=\"'e-mat-button--eblue400'\" [title]=\"'Choose Package'\" [loading]=\"pkg.clicked\"\r\n            (click)=\"onCreatePayment(pkg)\">\r\n          </app-button>\r\n        </div>\r\n\r\n        <div *ngIf=\"pkg.key === recommendedPackage.key\" class=\"eh-pos-absolute\" style=\"top: -20px; right: 0;\">\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--4 e-txt-primary\">verified</mat-icon>\r\n            <span class=\"eh-ml-2 eh-txt-10 e-txt-primary\">Recommended by Kinita</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- <div class=\"eh-mt-16 eh-txt-12 eh-txt-bold\">Alternate payment methods</div> -->\r\n    <div class=\"eh-mt-16 eh-txt-bold e-txt e-txt--matamber600\">OR</div>\r\n\r\n    <div class=\"eh-mt-12 eh-txt-bold\">Other Modes of Payment</div>\r\n    <table class=\"e-table\">\r\n      <tr>\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">NEFT / RTGS / IMPS</td>\r\n        <!-- <td class=\"eh-txt-center\" style=\"width: 20%;\">Credit / Debit Card</td> -->\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">VPA</td>\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">GPay / QR Code Scan</td>\r\n        <td class=\"eh-txt-center\" style=\"width: 20%;\">Cheque Payment</td>\r\n      </tr>\r\n\r\n      <tr>\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n          <span class=\"eh-txt-bold\">MEALpyramid Nutrition and Wellness</span><br><br>\r\n          Account No: <span class=\"eh-txt-bold\">23905900140</span><br>\r\n          IFSC Code: <span class=\"eh-txt-bold\">SCBL0036058</span> (for rupee payments)<br>\r\n          SWIFT Code: <span class=\"eh-txt-bold\">SCBLINBBXXX</span> (for non-rupee payments)<br><br>\r\n          Bank: <span class=\"eh-txt-bold\">Standard Chartered Bank</span><br>\r\n          Branch: <span class=\"eh-txt-bold\">Juhu</span><br>\r\n          Type: <span class=\"eh-txt-bold\">Current / Domestic</span><br>\r\n          Entity Type: <span class=\"eh-txt-bold\">Partnership</span>\r\n        </td>\r\n\r\n        <!-- <td class=\"eh-txt-12\">\r\n          WhatsApp us on <span class=\"eh-txt-bold\">+91-892686118</span> and we will generate a link for you so you can\r\n          make the payment online using your Credit or Debit Card.<br><br>\r\n          <span class=\"e-txt e-txt-placeholder\">International customers can select the currency of their choice.</span>\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n            <img class=\"eh-w-100 eh-mt-16\" src=\"../../../../assets/images/razorpay-logo-white.svg\" alt=\"RazorPay\">\r\n          </div>\r\n        </td> -->\r\n\r\n        <td class=\"eh-txt-12\">\r\n          Simply enter our VPA ID in any supported app.<br><br>\r\n          VPA ID:<br>\r\n          <span class=\"eh-txt-bold\">mealpyramid@sc</span><br><br>\r\n          Linked Number:<br>\r\n          <span class=\"eh-txt-bold\">+91-9819304049</span><br><br>\r\n        </td>\r\n\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12 eh-txt-center\" style=\"line-height: 1.4em;\">\r\n          <span>Scan the below QR code</span><br>\r\n          <img class=\"eh-w-144 eh-mt-12\" src=\"../../../../assets/images/payment-qr-code-2.png\" alt=\"QR Code\">\r\n          <div class=\"eh-mt-12\" fxLayout=\"row wrap\" fxLayoutAlign=\"center\">\r\n            <img class=\"eh-w-64\" src=\"../../../../assets/images/google-pay-mark-logo.svg\" alt=\"GPay\">\r\n            <img class=\"eh-w-60 eh-ml-8\" src=\"../../../../assets/images/paytm-logo.svg\" alt=\"PayTM\">\r\n            <img class=\"eh-w-80 eh-ml-8\" src=\"../../../../assets/images/phone-pe-logo.svg\" alt=\"PhonePe\">\r\n          </div>\r\n          <!-- <div>\r\n            <img class=\"eh-w-100\" src=\"../../../../assets/images/bhim-upi-logo.png\" alt=\"BHIM UPI\">\r\n          </div> -->\r\n        </td>\r\n\r\n        <td class=\"e-txt e-txt--matgrey700 eh-txt-12\" style=\"line-height: 1.4em;\">\r\n          <span>Cheques must be drawn in favour of:</span><br>\r\n          <span>“<b>MEALpyramid Nutrition and Wellness</b>” and sent to:</span><br><br>\r\n          <b>MEALpyramid Nutrition and Wellness</b><br>\r\n          <span>1st Floor, Anuradha Satyamorthy Building,</span><br>\r\n          <span>#16 Vithal Nagar Society,</span><br>\r\n          <span>11th Road, Juhu Scheme,</span><br>\r\n          <span>Mumbai, 400049, INDIA.</span><br><br>\r\n          <span>Tel: +91-9892686118</span>\r\n        </td>\r\n      </tr>\r\n    </table>\r\n\r\n    <div class=\"eh-mt-8\">\r\n      Once paid, kindly send us remittance details and we will activate your account accordingly. If your preferred mode\r\n      of payment is other than credit card, kindly share your payment details. WhatsApp or email us the details and we\r\n      shall activate the purchase from our side.\r\n    </div>\r\n    <div class=\"eh-mt-24 eh-txt-bold eh-txt-12\">Full Comparison</div>\r\n    <div class=\"eh-w-90p\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n      <table class=\"e-table\">\r\n        <tr>\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-bold eh-txt-left\" style=\"width: 40%;\">Program Features</td>\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-bold eh-txt-uppercase\" style=\"width: 20%;\">Standard</td>\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-bold eh-txt-uppercase\" style=\"width: 20%;\">Premium</td>\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-bold eh-txt-uppercase\" style=\"width: 20%;\">Prestige</td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Welcome Kit (NA for online clients)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n              <span>&nbsp;&nbsp;PVC</span>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n              <span>&nbsp;&nbsp;FELT</span>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n              <span>&nbsp;&nbsp;FELT</span>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Access to MEALpyramid WebApp & MobileApp\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Eating out options for multiple cuisines (Standard Guidelines)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Travel Tips (Standard Guidelines)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Signed copy of \"The Athlete in You\" by Kinita Kadakia Patel (NA for online clients)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Recall - Body measurements, Weight and Diet Feedback Discussion\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Nutritionist\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Nutritionist\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Kinita\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Diet Change (As per package frequency)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              Alternate between Nutritionist & Kinita\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Kinita\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Kinita\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            One to one consultation with Kinita\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              Alternate session\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              Every session\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              Every session\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Progress Monitoring Calls\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Nutritionist\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Kinita\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              by Kinita\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            WhatsApp Support\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              with Nutritionist\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              with Nutritionist supervised by Kinita\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              with Kinita\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Invitation to attend Group Discustions / Seminars organised at MEALpyramid\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              10% discount\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              15% discount\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              Free\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Advanced Testing\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              Priority Appointment with Kinita\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Training your cook to ensure all meals are prepared as per diet plan (one time)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Coordination with Fitness Trainer to ensure meals are as per training calander\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Home Visits (if required - Mumbai Only / max 2 times)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Personal Food Shopping Guide (with client only)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            Travel meals coordination with Hotel Chef (if required)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n\r\n        <tr class=\"eh-txt-12\">\r\n          <td class=\"eh-txt-center eh-txt-12 eh-txt-left\" style=\"width: 40%;\">\r\n            On-field Meal Planning (sports)\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              -\r\n            </div>\r\n          </td>\r\n          <td class=\"eh-txt-center\" style=\"width: 20%;\">\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3\">done</mat-icon>\r\n            </div>\r\n          </td>\r\n        </tr>\r\n      </table>\r\n    </div>\r\n\r\n    <div fxLayout=\"row\" fxLayoutAlign=\"start start\" class=\"eh-mt-24\">\r\n      <div class=\"eh-mt-8\" fxFlex=\"0 0 30%\">\r\n        <div class=\"eh-txt-bold eh-px-2 eh-mb-8\">\r\n          ALL PACKAGES INCLUDE\r\n        </div>\r\n        <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n          <div class=\"eh-mt-6\">\r\n            <div class=\"eh-txt-bold\">\r\n              Complete Nutrition Analysis\r\n            </div>\r\n            <ul class=\"eh-m-0 eh-px-16\">\r\n              <li>\r\n                Health history analysis\r\n              </li>\r\n              <li>\r\n                Clinical analysis\r\n              </li>\r\n              <li>\r\n                Dietary supplements\r\n              </li>\r\n              <li>\r\n                Lifestyle modifications\r\n              </li>\r\n              <li>\r\n                Travel tips </li>\r\n              <li>\r\n                Guide to eating out</li>\r\n            </ul>\r\n          </div>\r\n          <div class=\"eh-mt-6\">\r\n            <div class=\"eh-txt-bold\">\r\n              Interpersonal Consultations\r\n            </div>\r\n            <ul class=\"eh-m-0 eh-px-16\">\r\n              <li>\r\n                Individualised nutrition plan\r\n              </li>\r\n              <li>\r\n                Realistic goal setting\r\n              </li>\r\n              <li>\r\n                Detailed diet discussions\r\n              </li>\r\n            </ul>\r\n          </div>\r\n          <div class=\"eh-mt-6\">\r\n            <div class=\"eh-txt-bold\">Follow-Up Consultations\r\n            </div>\r\n            <ul class=\"eh-m-0 eh-px-16\">\r\n              <li>\r\n                Target monitoring\r\n              </li>\r\n              <li>\r\n                Dietary changes\r\n              </li>\r\n              <li>\r\n                Constant monitoring\r\n              </li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n        <div class=\"eh-txt-bold eh-txt-italic eh-mt-10 eh-txt-12\">\r\n          <span class=\"eh-txt-underline\">NOTE</span> : Online services follow the\r\n          same protocol & charges\r\n        </div>\r\n        <div class=\" eh-txt-bold eh-mt-8 eh-txt-12\">\r\n          GSTIN: 27ABRFM9899F1Z6\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-8\" fxFlex=\"0 0 35%\">\r\n        <div class=\"eh-txt-bold eh-px-16 eh-mb-8\">MEALpyramid will:</div>\r\n        <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n          <ul class=\"eh-m-0 eh-px-16 \">\r\n            <li>\r\n              endeavour to provide you with the best nutritional advice that may help resolve weight and health problems\r\n              or improve your sporting performance; however, following the advice is ultimately at your discretion\r\n            </li>\r\n            <li>\r\n              not guarantee that you will meet your target, whatever that may be\r\n            </li>\r\n            <li>\r\n              retain the copyright of all diets and written material provided to the client </li>\r\n            <li>\r\n              reserve the right to terminate our services in the event of the client failing to comply with the\r\n              prescribed\r\n              diet\r\n            </li>\r\n            <li>\r\n              not be liable under any circumstances for any health issues / complications arising during or after\r\n              clients\r\n              start following our guidance\r\n            </li>\r\n            <li>\r\n              maintain client confidentiality at all times and details of your records will only be released to third\r\n              parties only upon your express written consent\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n      <div class=\"eh-mt-8\" fxFlex=\"0 0 35%\">\r\n        <div class=\"eh-txt-bold eh-px-16 eh-mb-8\">TERMS OF SERVICE</div>\r\n        <div class=\"eh-txt-12 eh-mt-8 eh-txt-justify\">\r\n          <ul class=\"eh-m-0 eh-px-16\">\r\n            <li>Orientation fee is applicable for all clients (first visit charges / package renewal after long break).\r\n            </li>\r\n            <li>Health check is mandatory before starting the package.\r\n            </li>\r\n            <li>Entire package amount is payable upfront, prior to starting the consultation.</li>\r\n            <li>Amount once paid shall not be refundable under any circumstances.</li>\r\n            <li>All packages are non-transferable</li>\r\n            <li>No additional session / time extension shall be provided in case they lapse ; <span\r\n                class=\"eh-txt-underline\"> please make note of your package details carefully</span></li>\r\n            <li>All packages must be completed within the specified time period.</li>\r\n            <li>Upgrading of package midway is not permitted.</li>\r\n            <li>Children aged under 18 years must be accompanied by a parent or guardian.</li>\r\n            <li>Rates subject to change without prior notice. Rates at the time of joining shall only be applicable.\r\n            </li>\r\n          </ul>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 83858:
/*!******************************************************************************!*\
  !*** ./src/app/shared/shared-components/tabs/tabs.component.html?ngResource ***!
  \******************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<ion-toolbar *ngIf=\"dispatchView && me\">\r\n  <ion-tabs>\r\n    <ion-tab-bar slot=\"bottom\">\r\n      <ion-tab-button (click)=\"goto(routeIDs.HOME)\" [disabled]=\"!isPackageSubscribed\">\r\n        <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n        <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Home</ion-label>\r\n        <ion-icon name=\"home\"></ion-icon>\r\n      </ion-tab-button>\r\n      <ion-tab-button (click)=\"goto(routeIDs.DIETS)\" [disabled]=\"!isPackageSubscribed\">\r\n        <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n        <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Diet</ion-label>\r\n        <ion-icon name=\"fast-food-outline\"></ion-icon>\r\n      </ion-tab-button>\r\n      <ion-tab-button (click)=\"goto(routeIDs.PROGRESS)\" [disabled]=\"!isPackageSubscribed\">\r\n        <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n        <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Progress</ion-label>\r\n        <ion-icon name=\"pulse-outline\"></ion-icon>\r\n      </ion-tab-button>\r\n      <ion-tab-button (click)=\"goto(routeIDs.APPOINTMENTS)\" [disabled]=\"!isPackageSubscribed\">\r\n        <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n        <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Appointment</ion-label>\r\n        <ion-icon name=\"calendar\"></ion-icon>\r\n      </ion-tab-button>\r\n      <ion-tab-button (click)=\"goto(routeIDs.HEALTH_HISTORY)\" [disabled]=\"!isPackageSubscribed\">\r\n        <ion-label *ngIf=\"!isPackageSubscribed\" class=\"e-txt e-txt--matgrey500\">PRO</ion-label>\r\n        <ion-label *ngIf=\"isPackageSubscribed\" class=\"e-txt e-txt--matgrey500 eh-txt-10\">Health History</ion-label>\r\n        <ion-icon name=\"reader-outline\"></ion-icon>\r\n      </ion-tab-button>\r\n    </ion-tab-bar>\r\n  </ion-tabs>\r\n</ion-toolbar>";

/***/ }),

/***/ 52804:
/*!**************************************************************************!*\
  !*** ./src/app/shared/upload-file/upload-file.component.html?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "<div [ngClass]=\"{'eh-w-100p eh-h-100p e-bg-matgrey100 e-brd e-brd--all': true, 'eh-d-none': !isVisible}\"\r\n  style=\"min-height: 52px;\">\r\n  <div *ngIf=\"!file\" class=\"eh-w-100p eh-h-100p eh-xy-center eh-px-16\">\r\n    <span class=\"eh-txt-10 eh-txt-bold e-txt e-txt--matgrey500\">PREVIEW</span>\r\n  </div>\r\n  <div *ngIf=\"uploadPercentage$ | async as pct\"\r\n    [ngClass]=\"pct !== 100 ? 'eh-w-100p eh-h-100p eh-xy-center eh-px-16' : 'eh-d-none'\">\r\n    <mat-progress-bar mode=\"determinate\" [value]=\"pct\"></mat-progress-bar>\r\n  </div>\r\n  <div *ngIf=\"uploadState$ | async as state\" class=\"eh-w-100p eh-h-100p\">\r\n    <div *ngIf=\"fileURL as url\" class=\"eh-w-100p eh-h-100p eh-xy-center eh-pos-relative\">\r\n      <img class=\"eh-fit\" [src]=\"url\"><br>\r\n      <div class=\"eh-pos-absolute\" style=\"top: -16px; right: -16px;\">\r\n        <app-button-icon [icon]=\"'cancel'\" (onButtonClick)=\"onFileDelete()\">\r\n        </app-button-icon>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>";

/***/ }),

/***/ 93414:
/*!************************!*\
  !*** canvas (ignored) ***!
  \************************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 70172:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 2001:
/*!**********************!*\
  !*** http (ignored) ***!
  \**********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 33779:
/*!***********************!*\
  !*** https (ignored) ***!
  \***********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 66558:
/*!*********************!*\
  !*** url (ignored) ***!
  \*********************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ 82258:
/*!**********************!*\
  !*** zlib (ignored) ***!
  \**********************/
/***/ (() => {

/* (ignored) */

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(14431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map