"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_profile_profile_module_ts"],{

/***/ 56018:
/*!****************************************************************!*\
  !*** ./src/app/profile/edit-profile/edit-profile.component.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EditProfileComponent": () => (/* binding */ EditProfileComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _edit_profile_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-profile.component.html?ngResource */ 33609);
/* harmony import */ var _edit_profile_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit-profile.component.scss?ngResource */ 37890);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ 92340);
/* harmony import */ var _capacitor_camera__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @capacitor/camera */ 4241);
/* harmony import */ var _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/filesystem */ 91662);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/dialog */ 31484);















let EditProfileComponent = class EditProfileComponent {
  constructor(_as, _router, _dialog, menuCtrl, loadingCtrl, _ss, _shs, _sb, _general) {
    this._as = _as;
    this._router = _router;
    this._dialog = _dialog;
    this.menuCtrl = menuCtrl;
    this.loadingCtrl = loadingCtrl;
    this._ss = _ss;
    this._shs = _shs;
    this._sb = _sb;
    this._general = _general;
    this.advancedRecommendedPackage = '';
    this.dispatchView = false;
    this.hasIntroVideoEnded = false;
    this.hasWelcomeVideoEnded = false;
    this.heroImage = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__.environment.heroSection.kinita;
    this.introVideoURL = src_environments_environment__WEBPACK_IMPORTED_MODULE_5__.environment.video.intro;
    this.routeIDs = src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.RouteIDs;
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
    this.photos = [];

    this.convertBlobToBase64 = blob => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;

      reader.onload = () => {
        resolve(reader.result);
      };

      reader.readAsDataURL(blob);
    });

    this.menuCtrl.swipeGesture(true);
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.takeUntil)(this._notifier$)).subscribe(user => {
      this.me = user;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;
      this.dispatchView = true;
      console.log(this.me);
    });
  }

  goto(path) {
    this.path = path;

    this._router.navigate(['/tab/' + path]);
  }

  onLogout() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      yield _this._general.onLogout();
    })();
  }

  addPhotoToGallery() {
    var _this2 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Take a photo
      const capturedPhoto = yield _capacitor_camera__WEBPACK_IMPORTED_MODULE_6__.Camera.getPhoto({
        resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_6__.CameraResultType.Uri,
        source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_6__.CameraSource.Camera,
        quality: 100
      }); // Save the picture and add it to photo collection

      _this2.photos.unshift({
        filepath: "soon...",
        webviewPath: capturedPhoto.webPath
      });

      const savedImageFile = yield _this2.savePicture(capturedPhoto);
    })();
  }

  savePicture(photo) {
    var _this3 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Convert photo to base64 format, required by Filesystem API to save
      const base64Data = yield _this3.readAsBase64(photo); // Write the file to the data directory

      const fileName = new Date().getTime() + '.jpeg';
      const savedFile = yield _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_7__.Filesystem.writeFile({
        path: fileName,
        data: base64Data,
        directory: _capacitor_filesystem__WEBPACK_IMPORTED_MODULE_7__.Directory.Data
      }); // Use webPath to display the new image instead of base64 since it's
      // already loaded into memory

      return {
        filepath: fileName,
        webviewPath: photo.webPath
      };
    })();
  }

  readAsBase64(photo) {
    var _this4 = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = yield fetch(photo.webPath);
      const blob = yield response.blob();
      return yield _this4.convertBlobToBase64(blob);
    })();
  }

};

EditProfileComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.AuthService
}, {
  type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__.MatDialog
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.MenuController
}, {
  type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.LoadingController
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SessionnService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_4__.GeneralService
}];

EditProfileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_14__.Component)({
  selector: 'app-edit-profile',
  template: _edit_profile_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_edit_profile_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], EditProfileComponent);


/***/ }),

/***/ 86829:
/*!***************************************************!*\
  !*** ./src/app/profile/profile-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfileRoutingModule": () => (/* binding */ ProfileRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user-profile/user-profile.component */ 89618);
/* harmony import */ var _edit_profile_edit_profile_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-profile/edit-profile.component */ 56018);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _profile_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile.component */ 96630);






const routes = [
    {
        path: '',
        component: _profile_component__WEBPACK_IMPORTED_MODULE_2__.ProfileComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                component: _edit_profile_edit_profile_component__WEBPACK_IMPORTED_MODULE_1__.EditProfileComponent
            },
            {
                path: 'edit-profile',
                component: _user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_0__.UserProfileComponent
            },
        ]
    }
];
let ProfileRoutingModule = class ProfileRoutingModule {
};
ProfileRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
    })
], ProfileRoutingModule);



/***/ }),

/***/ 96630:
/*!**********************************************!*\
  !*** ./src/app/profile/profile.component.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfileComponent": () => (/* binding */ ProfileComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _profile_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.component.html?ngResource */ 66038);
/* harmony import */ var _profile_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile.component.scss?ngResource */ 15465);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/constants */ 31896);
/* harmony import */ var _core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/services */ 98138);









 // import { User } from '../common/models';


let ProfileComponent = class ProfileComponent {
  constructor(_us, _sb, _as, _fb, _uts) {
    this._us = _us;
    this._sb = _sb;
    this._as = _as;
    this._fb = _fb;
    this._uts = _uts;
    this.dispatchView = false;
    this.dpChosenFileSize = 0;
    this.dpChosenInitialFileName = '';
    this.dpUploadedFileSize = 0;
    this.dpUploadedInitialFileName = '';
    this.genderTypes = _common_constants__WEBPACK_IMPORTED_MODULE_4__.GenderTypes;
    this.isPackageSubscribed = false;
    this.loading = false;
    this.uploadCollectionName = 'userProfilePictureFiles';
    this.uploadFileNamePartial = 'MEALpyramid_DP';
    this.uploadFileNamePrefix = '';
    this.uploadDirPath = 'displayPictures';
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.catchError)(() => {
      this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_9__.EMPTY;
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$)).subscribe(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.maxDate = moment__WEBPACK_IMPORTED_MODULE_3__().subtract(1, 'day').startOf('day').toDate();
      this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;
      this.createFG();
      this.personalDetailsFGInitial = this.personalDetailsFG.get('personalDetails').value;
      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  } // *
  // * FormGroup
  // *


  createFG() {
    this.personalDetailsFG = this._fb.group({
      name: [this.me.name],
      dOBSearchIndex: [this.me.dOBSearchIndex],
      searchArray: [this.me.searchArray],
      personalDetails: this._fb.group(this.me.personalDetails)
    });
    this.firstNameFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9]{2,51}$/)]);
    this.lastNameFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9]{1,51}$/)]);
    this.dateOfBirthFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.genderFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.addressLine1FC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.minLength(5)]);
    this.cityFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.stateFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.countryFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.pincodeFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9-]{3,11}$/)]);
    this.mobileFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/)]);
    const dob = moment__WEBPACK_IMPORTED_MODULE_3__(this.dateOfBirthFC.value).toDate();
    this.dob = this._uts.isValidDate(dob) ? dob : null;
  }

  get firstNameFC() {
    return this.personalDetailsFG.get('personalDetails.firstName');
  }

  get lastNameFC() {
    return this.personalDetailsFG.get('personalDetails.lastName');
  }

  get nameFC() {
    return this.personalDetailsFG.get('name');
  }

  get gender() {
    return this.personalDetailsFG.get('personalDetails.gender').value;
  }

  get genderFC() {
    return this.personalDetailsFG.get('personalDetails.gender');
  }

  get dateOfBirthFC() {
    return this.personalDetailsFG.get('personalDetails.dateOfBirth');
  }

  get dOBSearchIndex() {
    return this.personalDetailsFG.get('dOBSearchIndex');
  }

  get searchArrayFC() {
    return this.personalDetailsFG.get('searchArray');
  }

  get photoURLFC() {
    return this.personalDetailsFG.get('personalDetails.photoURL');
  }

  get addressLine1FC() {
    return this.personalDetailsFG.get('personalDetails.addressLine1');
  }

  get cityFC() {
    return this.personalDetailsFG.get('personalDetails.city');
  }

  get stateFC() {
    return this.personalDetailsFG.get('personalDetails.state');
  }

  get pincodeFC() {
    return this.personalDetailsFG.get('personalDetails.pincode');
  }

  get countryFC() {
    return this.personalDetailsFG.get('personalDetails.country');
  }

  get mobileFC() {
    return this.personalDetailsFG.get('personalDetails.mobile');
  }

  get emailFC() {
    return this.personalDetailsFG.get('personalDetails.email');
  }

  isPersonalDetailsFGValid() {
    return this.personalDetailsFG.valid;
  }

  hasChanged() {
    return !this._uts.shallowEqual(this.personalDetailsFG.get('personalDetails').value, this.personalDetailsFGInitial);
  }

  onReset() {
    if (this.hasChanged()) {
      this.personalDetailsFG.get('personalDetails').reset(this.personalDetailsFGInitial);
    }
  } // *
  // * Date of Birth
  // *


  onDOBSet(e) {
    this.dateOfBirthFC.setValue(moment__WEBPACK_IMPORTED_MODULE_3__(e.value).format('YYYY-MM-DD'));
    this.dOBSearchIndex.setValue(moment__WEBPACK_IMPORTED_MODULE_3__(e.value).format('MM-DD'));
  } // *
  // * Display Picture
  // *


  availableEvent(e) {
    if (!e.fileSize) {
      return;
    }

    this.dpChosenInitialFileName = e.initialFileName;
    this.dpChosenFileSize = e.fileSize;
  }

  uploadEvent(e) {
    if (!e.fileURL) {
      return;
    }

    this.dpUploadedInitialFileName = e.initialFileName;
    this.dpUploadedFileSize = e.fileSize;
    this.photoURLFC.setValue(e.fileURL);
  } // async uploadEvent(e: any): Promise<void> {
  //   try {
  //     if (!e.fileURL) {
  //       return;
  //     }
  //     this.photoURLFC.setValue(e.fileURL);
  //     await this._us.setUser(this.me.uid, {
  //       personalDetails: {
  //         photoURL: e.fileURL
  //       }
  //     });
  //   } catch (err) {
  //     this._sb.openErrorSnackBar(ErrorMessages.DISPLAY_PICTURE_NOT_SAVED);
  //   }
  // }
  // *
  // * Save
  // *


  setNameSearchArray() {
    const sanitizedFirstName = this._uts.sanitize(this.firstNameFC.value);

    const sanitizedLastName = this._uts.sanitize(this.lastNameFC.value);

    const fullName = `${sanitizedFirstName} ${sanitizedLastName}`;
    this.nameFC.setValue(fullName);

    const searchArray = this._uts.getSearchArray(sanitizedFirstName, sanitizedLastName);

    this.searchArrayFC.setValue(searchArray);
  }

  onSave() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.hasChanged() || !_this.personalDetailsFG.valid) {
        return;
      }

      if (_this.dpChosenFileSize !== _this.dpUploadedFileSize || _this.dpChosenInitialFileName !== _this.dpUploadedInitialFileName) {
        _this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED);

        return;
      }

      _this.loading = true;

      try {
        _this.setNameSearchArray(); // const searchArray = this._uts.getSearchArray(
        //   this._uts.sanitize(this.firstNameFC.value),
        //   this._uts.sanitize(this.lastNameFC.value)
        // );
        // const userObj: any = {
        //   name: `${this._uts.sanitize(
        //     this.firstNameFC.value
        //   )} ${this._uts.sanitize(this.lastNameFC.value)}`,
        //   searchArray,
        //   dOBSearchIndex: moment(this.dateOfBirthFC.value).format('MM-DD'),
        //   personalDetails: this.personalDetailsFG.value
        // };


        yield _this._us.setUser(_this.me.uid, _this.personalDetailsFG.value); // await this._us.setUser(this.me.uid, userObj);

        _this.loading = false;

        _this._sb.openSuccessSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.SAVE_OK, 2000);
      } catch (err) {
        _this._sb.openErrorSnackBar(_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

        _this.loading = false;
      }
    })();
  }

};

ProfileComponent.ctorParameters = () => [{
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.UserService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder
}, {
  type: _core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}];

ProfileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-profile',
  template: _profile_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_profile_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ProfileComponent);


/***/ }),

/***/ 84523:
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfileModule": () => (/* binding */ ProfileModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user-profile/user-profile.component */ 89618);
/* harmony import */ var _edit_profile_edit_profile_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-profile/edit-profile.component */ 56018);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./profile-routing.module */ 86829);
/* harmony import */ var _profile_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.component */ 96630);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _shared_dialog_box__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/dialog-box */ 55599);
/* harmony import */ var _shared_get_started__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/get-started */ 14526);
/* harmony import */ var _shared_input_file_preview__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/input-file-preview */ 60510);
/* harmony import */ var _shared_material_design__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/material-design */ 12497);
/* harmony import */ var _shared_shared_components__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/shared-components */ 35886);
/* harmony import */ var _shared_upload_file__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/upload-file */ 74355);
/* harmony import */ var _shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/layout/layout.module */ 68634);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 93819);

















let ProfileModule = class ProfileModule {
};
ProfileModule = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.NgModule)({
        declarations: [_profile_component__WEBPACK_IMPORTED_MODULE_3__.ProfileComponent, _edit_profile_edit_profile_component__WEBPACK_IMPORTED_MODULE_1__.EditProfileComponent, _user_profile_user_profile_component__WEBPACK_IMPORTED_MODULE_0__.UserProfileComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_13__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicModule,
            _shared_layout_layout_module__WEBPACK_IMPORTED_MODULE_10__.LayoutModule,
            _shared_get_started__WEBPACK_IMPORTED_MODULE_5__.GetStartedModule,
            _shared_material_design__WEBPACK_IMPORTED_MODULE_7__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_15__.FlexLayoutModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_16__.ReactiveFormsModule,
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_2__.ProfileRoutingModule,
            _shared_upload_file__WEBPACK_IMPORTED_MODULE_9__.UploadFileModule,
            _shared_dialog_box__WEBPACK_IMPORTED_MODULE_4__.DialogBoxModule,
            _shared_shared_components__WEBPACK_IMPORTED_MODULE_8__.SharedComponentsModule,
            _shared_input_file_preview__WEBPACK_IMPORTED_MODULE_6__.InputFilePreviewModule
        ]
    })
], ProfileModule);



/***/ }),

/***/ 89618:
/*!****************************************************************!*\
  !*** ./src/app/profile/user-profile/user-profile.component.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserProfileComponent": () => (/* binding */ UserProfileComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _user_profile_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user-profile.component.html?ngResource */ 83981);
/* harmony import */ var _user_profile_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-profile.component.scss?ngResource */ 24758);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 56908);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);











let UserProfileComponent = class UserProfileComponent {
  constructor(_us, _sb, _as, _fb, _uts) {
    this._us = _us;
    this._sb = _sb;
    this._as = _as;
    this._fb = _fb;
    this._uts = _uts;
    this.dispatchView = false;
    this.dpChosenFileSize = 0;
    this.dpChosenInitialFileName = '';
    this.dpUploadedFileSize = 0;
    this.dpUploadedInitialFileName = '';
    this.genderTypes = src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.GenderTypes;
    this.isPackageSubscribed = false;
    this.loading = false;
    this.uploadCollectionName = 'userProfilePictureFiles';
    this.uploadFileNamePartial = 'MEALpyramid_DP';
    this.uploadFileNamePrefix = '';
    this.uploadDirPath = 'displayPictures';
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_6__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_8__.catchError)(() => {
      this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

      return rxjs__WEBPACK_IMPORTED_MODULE_9__.EMPTY;
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.takeUntil)(this._notifier$)).subscribe(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.maxDate = moment__WEBPACK_IMPORTED_MODULE_3__().subtract(1, 'day').startOf('day').toDate();
      this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;
      this.createFG();
      this.personalDetailsFGInitial = this.personalDetailsFG.get('personalDetails').value;
      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  } // *
  // * FormGroup
  // *


  createFG() {
    this.personalDetailsFG = this._fb.group({
      name: [this.me.name],
      dOBSearchIndex: [this.me.dOBSearchIndex],
      searchArray: [this.me.searchArray],
      personalDetails: this._fb.group(this.me.personalDetails)
    });
    this.firstNameFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9]{2,51}$/)]);
    this.lastNameFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9]{1,51}$/)]);
    this.dateOfBirthFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.genderFC.setValidators(_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required);
    this.addressLine1FC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.minLength(5)]);
    this.cityFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.stateFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.countryFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z ]{2,101}$/)]);
    this.pincodeFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^[A-Za-z0-9-]{3,11}$/)]);
    this.mobileFC.setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_10__.Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/)]);
    const dob = moment__WEBPACK_IMPORTED_MODULE_3__(this.dateOfBirthFC.value).toDate();
    this.dob = this._uts.isValidDate(dob) ? dob : null;
  }

  get firstNameFC() {
    return this.personalDetailsFG.get('personalDetails.firstName');
  }

  get lastNameFC() {
    return this.personalDetailsFG.get('personalDetails.lastName');
  }

  get nameFC() {
    return this.personalDetailsFG.get('name');
  }

  get gender() {
    return this.personalDetailsFG.get('personalDetails.gender').value;
  }

  get genderFC() {
    return this.personalDetailsFG.get('personalDetails.gender');
  }

  get dateOfBirthFC() {
    return this.personalDetailsFG.get('personalDetails.dateOfBirth');
  }

  get dOBSearchIndex() {
    return this.personalDetailsFG.get('dOBSearchIndex');
  }

  get searchArrayFC() {
    return this.personalDetailsFG.get('searchArray');
  }

  get photoURLFC() {
    return this.personalDetailsFG.get('personalDetails.photoURL');
  }

  get addressLine1FC() {
    return this.personalDetailsFG.get('personalDetails.addressLine1');
  }

  get cityFC() {
    return this.personalDetailsFG.get('personalDetails.city');
  }

  get stateFC() {
    return this.personalDetailsFG.get('personalDetails.state');
  }

  get pincodeFC() {
    return this.personalDetailsFG.get('personalDetails.pincode');
  }

  get countryFC() {
    return this.personalDetailsFG.get('personalDetails.country');
  }

  get mobileFC() {
    return this.personalDetailsFG.get('personalDetails.mobile');
  }

  get emailFC() {
    return this.personalDetailsFG.get('personalDetails.email');
  }

  isPersonalDetailsFGValid() {
    return this.personalDetailsFG.valid;
  }

  hasChanged() {
    return !this._uts.shallowEqual(this.personalDetailsFG.get('personalDetails').value, this.personalDetailsFGInitial);
  }

  onReset() {
    if (this.hasChanged()) {
      this.personalDetailsFG.get('personalDetails').reset(this.personalDetailsFGInitial);
    }
  } // *
  // * Date of Birth
  // *


  onDOBSet(e) {
    this.dateOfBirthFC.setValue(moment__WEBPACK_IMPORTED_MODULE_3__(e.value).format('YYYY-MM-DD'));
    this.dOBSearchIndex.setValue(moment__WEBPACK_IMPORTED_MODULE_3__(e.value).format('MM-DD'));
  } // *
  // * Display Picture
  // *


  availableEvent(e) {
    if (!e.fileSize) {
      return;
    }

    this.dpChosenInitialFileName = e.initialFileName;
    this.dpChosenFileSize = e.fileSize;
  }

  uploadEvent(e) {
    if (!e.fileURL) {
      return;
    }

    this.dpUploadedInitialFileName = e.initialFileName;
    this.dpUploadedFileSize = e.fileSize;
    this.photoURLFC.setValue(e.fileURL);
  } // async uploadEvent(e: any): Promise<void> {
  //   try {
  //     if (!e.fileURL) {
  //       return;
  //     }
  //     this.photoURLFC.setValue(e.fileURL);
  //     await this._us.setUser(this.me.uid, {
  //       personalDetails: {
  //         photoURL: e.fileURL
  //       }
  //     });
  //   } catch (err) {
  //     this._sb.openErrorSnackBar(ErrorMessages.DISPLAY_PICTURE_NOT_SAVED);
  //   }
  // }
  // *
  // * Save
  // *


  setNameSearchArray() {
    const sanitizedFirstName = this._uts.sanitize(this.firstNameFC.value);

    const sanitizedLastName = this._uts.sanitize(this.lastNameFC.value);

    const fullName = `${sanitizedFirstName} ${sanitizedLastName}`;
    this.nameFC.setValue(fullName);

    const searchArray = this._uts.getSearchArray(sanitizedFirstName, sanitizedLastName);

    this.searchArrayFC.setValue(searchArray);
  }

  onSave() {
    var _this = this;

    return (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* () {
      if (!_this.hasChanged() || !_this.personalDetailsFG.valid) {
        return;
      }

      if (_this.dpChosenFileSize !== _this.dpUploadedFileSize || _this.dpChosenInitialFileName !== _this.dpUploadedInitialFileName) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED);

        return;
      }

      _this.loading = true;

      try {
        _this.setNameSearchArray(); // const searchArray = this._uts.getSearchArray(
        //   this._uts.sanitize(this.firstNameFC.value),
        //   this._uts.sanitize(this.lastNameFC.value)
        // );
        // const userObj: any = {
        //   name: `${this._uts.sanitize(
        //     this.firstNameFC.value
        //   )} ${this._uts.sanitize(this.lastNameFC.value)}`,
        //   searchArray,
        //   dOBSearchIndex: moment(this.dateOfBirthFC.value).format('MM-DD'),
        //   personalDetails: this.personalDetailsFG.value
        // };


        yield _this._us.setUser(_this.me.uid, _this.personalDetailsFG.value); // await this._us.setUser(this.me.uid, userObj);

        _this.loading = false;

        _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.SuccessMessages.SAVE_OK, 2000);
      } catch (err) {
        _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_4__.ErrorMessages.SYSTEM);

        _this.loading = false;
      }
    })();
  }

};

UserProfileComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UserService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormBuilder
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.UtilitiesService
}];

UserProfileComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
  selector: 'app-user-profile',
  template: _user_profile_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_user_profile_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], UserProfileComponent);


/***/ }),

/***/ 34830:
/*!****************************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/definitions.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraDirection": () => (/* binding */ CameraDirection),
/* harmony export */   "CameraResultType": () => (/* binding */ CameraResultType),
/* harmony export */   "CameraSource": () => (/* binding */ CameraSource)
/* harmony export */ });
var CameraSource;

(function (CameraSource) {
  /**
   * Prompts the user to select either the photo album or take a photo.
   */
  CameraSource["Prompt"] = "PROMPT";
  /**
   * Take a new photo using the camera.
   */

  CameraSource["Camera"] = "CAMERA";
  /**
   * Pick an existing photo from the gallery or photo album.
   */

  CameraSource["Photos"] = "PHOTOS";
})(CameraSource || (CameraSource = {}));

var CameraDirection;

(function (CameraDirection) {
  CameraDirection["Rear"] = "REAR";
  CameraDirection["Front"] = "FRONT";
})(CameraDirection || (CameraDirection = {}));

var CameraResultType;

(function (CameraResultType) {
  CameraResultType["Uri"] = "uri";
  CameraResultType["Base64"] = "base64";
  CameraResultType["DataUrl"] = "dataUrl";
})(CameraResultType || (CameraResultType = {}));

/***/ }),

/***/ 4241:
/*!**********************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/index.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Camera": () => (/* binding */ Camera),
/* harmony export */   "CameraDirection": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraDirection),
/* harmony export */   "CameraResultType": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraResultType),
/* harmony export */   "CameraSource": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraSource)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 26549);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 34830);

const Camera = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Camera', {
  web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor_camera_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 71327)).then(m => new m.CameraWeb())
});



/***/ }),

/***/ 37890:
/*!*****************************************************************************!*\
  !*** ./src/app/profile/edit-profile/edit-profile.component.scss?ngResource ***!
  \*****************************************************************************/
/***/ ((module) => {

module.exports = ".profile {\n  min-height: 200px;\n  background-color: #1f212c;\n}\n\n.profile2 {\n  background-color: #1f212c;\n  color: white;\n}\n\n.new-background-color {\n  --background: #1f212c;\n  --color: white;\n}\n\nion-header.header-md:after {\n  background: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXQtcHJvZmlsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7QUFDRjs7QUFFQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtBQUNGOztBQUVBO0VBQ0UscUJBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBR0U7RUFDRSxnQkFBQTtBQUFKIiwiZmlsZSI6ImVkaXQtcHJvZmlsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9maWxlIHtcclxuICBtaW4taGVpZ2h0OiAyMDBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMWYyMTJjO1xyXG59XHJcblxyXG4ucHJvZmlsZTIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxZjIxMmM7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG4ubmV3LWJhY2tncm91bmQtY29sb3Ige1xyXG4gIC0tYmFja2dyb3VuZDogIzFmMjEyYztcclxuICAtLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgJi5oZWFkZXItbWQ6YWZ0ZXIge1xyXG4gICAgYmFja2dyb3VuZDogbm9uZTtcclxuICB9XHJcbn0iXX0= */";

/***/ }),

/***/ 15465:
/*!***********************************************************!*\
  !*** ./src/app/profile/profile.component.scss?ngResource ***!
  \***********************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9maWxlLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 24758:
/*!*****************************************************************************!*\
  !*** ./src/app/profile/user-profile/user-profile.component.scss?ngResource ***!
  \*****************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2VyLXByb2ZpbGUuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 33609:
/*!*****************************************************************************!*\
  !*** ./src/app/profile/edit-profile/edit-profile.component.html?ngResource ***!
  \*****************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"profile\">\r\n  <ion-header class=\"ion-no-border\">\r\n    <ion-toolbar class=\"new-background-color ion-no-border\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button defaultHref=\"/tab\"></ion-back-button>\r\n      </ion-buttons>\r\n      <!-- <ion-buttons slot=\"end\">\r\n        <app-profile-avatar></app-profile-avatar>\r\n      </ion-buttons> -->\r\n      <ion-title class=\"ion-padding\">Profile</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n  <div class=\"eh-mb-16 eh-mx-auto eh-px-16 eh-mt-16\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n    <div>\r\n      <app-avatar *ngIf=\"me\" class=\"eh-h-86\" [size]=\"'large'\" [userUID]=\"me.uid\" [border]=\"true\"\r\n        matTooltip=\"{{me.name}}\">\r\n      </app-avatar>\r\n    </div>\r\n    <div *ngIf=\"me\" fxLayout=\"column\" fxLayoutAlign=\"center\">\r\n      <div class=\"eh-txt-16 eh-px-16 eh-my-4 e-txt e-txt--ewhite eh-txt-bold eh-txt-large eh-txt-capital\">\r\n        {{me.personalDetails.firstName}} {{me.personalDetails.lastName}}\r\n      </div>\r\n      <div class=\"eh-txt-16 eh-px-16 eh-my-2 e-txt e-txt--ewhite\">\r\n        {{me.personalDetails.email}}\r\n      </div>\r\n      <div class=\"eh-txt-16 eh-px-8 eh-my-2 e-txt e-txt--ewhite eh-txt-capital\">\r\n        <ion-icon name=\"location-outline\"></ion-icon>\r\n        {{me.personalDetails.city}}, {{me.personalDetails.country}}\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- <ion-card>\r\n  <ion-card-content>\r\n  </ion-card-content>\r\n</ion-card> -->\r\n<ion-content>\r\n  <ion-item detail=\"true\" class=\"eh-my-10\" (click)=\"goto(routeIDs.RECOMMENDATIONS)\">\r\n    <ion-icon name=\"thumbs-up\" color=\"primary\">\r\n    </ion-icon>\r\n    <ion-label class=\"eh-p-2 eh-ml-6 eh-txt-12 eh-txt-primary\">\r\n      Recommendations\r\n    </ion-label>\r\n  </ion-item>\r\n  <ion-item detail=\"true\" class=\"eh-my-10\" (click)=\"goto('/profile/edit-profile')\">\r\n    <ion-icon name=\"person\" color=\"primary\"></ion-icon>\r\n    <ion-label class=\"eh-p-2 eh-ml-6 eh-txt-12 eh-txt-primary \">\r\n      Personal Information\r\n    </ion-label>\r\n  </ion-item>\r\n  <ion-item button detail=\"true\" detail=\"true\" class=\"eh-my-10\" (click)=\"onLogout()\">\r\n    <ion-icon name=\"log-out-outline\" color=\"primary\" size=\"medium\"></ion-icon>\r\n    <ion-label class=\"eh-p-2 eh-ml-6  eh-txt-large eh-txt-12 eh-txt-primary \">\r\n      logout\r\n    </ion-label>\r\n  </ion-item>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"6\" *ngFor=\"let photo of photos; index as position\">\r\n        <ion-img [src]=\"photo.webviewPath\"></ion-img>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n";

/***/ }),

/***/ 66038:
/*!***********************************************************!*\
  !*** ./src/app/profile/profile.component.html?ngResource ***!
  \***********************************************************/
/***/ ((module) => {

module.exports = "\r\n\r\n<ion-router-outlet></ion-router-outlet>";

/***/ }),

/***/ 83981:
/*!*****************************************************************************!*\
  !*** ./src/app/profile/user-profile/user-profile.component.html?ngResource ***!
  \*****************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-padding\">My Profile</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <app-profile-avatar></app-profile-avatar>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <ng-container *ngIf=\"isPackageSubscribed; else isPackageNotSubscribedBlk\">\r\n    <div class=\"eh-p-16 eh-ofy-scroll eh-hide-scrollbars\" [formGroup]=\"personalDetailsFG\">\r\n      <!-- <div class=\"eh-mb-16\" fxLayout=\"row\" fxLayoutAlign=\"end center\">\r\n        <div *ngIf=\"hasChanged()\" class=\"e-txt e-txt--matred600 eh-mr-16\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--3\">warning</mat-icon>\r\n          <span class=\"eh-txt-12 eh-ml-4 eh-txt-bold\">Unsaved changes</span>\r\n        </div>\r\n        <app-button-icon-title title=\"Reset\" buttonStyle=\"e-txt e-txt--matred600\" icon=\"settings_backup_restore\"\r\n          [disabled]=\"loading\" (onButtonClick)=\"onReset()\"></app-button-icon-title>\r\n        <app-button-icon-title class=\"eh-ml-16 eh-mr-4\" title=\"Save\" buttonStyle=\"e-txt e-txt--matgreen500\" icon=\"save\"\r\n          [loading]=\"loading\" (onButtonClick)=\"onSave()\"></app-button-icon-title>\r\n      </div> -->\r\n      <ng-container formGroupName=\"personalDetails\">\r\n        <div class=\"e-title\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">assignment_ind</mat-icon>\r\n          <span>Basic</span>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>First Name<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"firstName\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>Last Name<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"lastName\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n\r\n        <div class=\"eh-mt-8\" fxLayout=\"row\">\r\n          <div class=\"eh-w-300\">\r\n            <div class=\"eh-txt-12 eh-mb-4\">Gender<sup>*</sup></div>\r\n            <mat-button-toggle-group class=\"e-mat-button-toggle\" formControlName=\"gender\">\r\n              <mat-button-toggle [checked]=\"gender === genderTypes.MALE\" [value]=\"genderTypes.MALE\" [disabled]=\"true\">\r\n                Male\r\n              </mat-button-toggle>\r\n              <mat-button-toggle [checked]=\"gender === genderTypes.FEMALE\" [value]=\"genderTypes.FEMALE\"\r\n                [disabled]=\"true\">\r\n                Female\r\n              </mat-button-toggle>\r\n            </mat-button-toggle-group>\r\n          </div>\r\n\r\n          <div class=\"eh-w-300 eh-mt-8\">\r\n            <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-180\">\r\n              <mat-label>Date of Birth<sup>*</sup></mat-label>\r\n              <input matInput readonly [matDatepicker]=\"picker\" [max]=\"maxDate\" [value]=\"dob\" [disabled]=\"loading\"\r\n                (dateInput)=\"onDOBSet($event)\" readOnly>\r\n              <mat-datepicker-toggle matSuffix [for]=\"picker\" class=\"e-mat-datepicker\">\r\n              </mat-datepicker-toggle>\r\n              <!-- <mat-datepicker #picker></mat-datepicker> -->\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n\r\n        <!-- <div class=\"eh-mt-12\" fxLayout=\"column\">\r\n          <div class=\"eh-w-300\">\r\n            <div class=\"eh-py-4\">Profile Picture</div>\r\n            <app-input-file-preview [uploadByName]=\"me.name\" [uploadByUID]=\"me.uid\"\r\n              [uploadCollectionName]=\"uploadCollectionName\" [uploadDirPath]=\"uploadDirPath\"\r\n              [uploadFileNamePrefix]=\"uploadFileNamePrefix\" [userMembershipKey]=\"me.membershipKey\" [userUID]=\"me.uid\"\r\n              (availableEvent)=\"availableEvent($event)\" (uploadEvent)=\"uploadEvent($event)\">\r\n            </app-input-file-preview>\r\n          </div>\r\n        </div> -->\r\n\r\n        <div class=\"e-title eh-mt-24\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">contact_mail</mat-icon>\r\n          <span>Contact</span>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>Email<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"email\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>Mobile<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"mobile\" readonly>\r\n            <mat-hint>+91-XXXXXXXXXX</mat-hint>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n          <div class=\"eh-w-480\">\r\n            <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n              <mat-label>Address Line 1<sup>*</sup></mat-label>\r\n              <input class=\"eh-txt-lh1_2\" matInput formControlName=\"addressLine1\" readonly>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n          <div class=\"eh-w-480\">\r\n            <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n              <mat-label>Address Line 2</mat-label>\r\n              <input class=\"eh-txt-lh1_2\" matInput formControlName=\"addressLine2\" readonly>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-8\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>City<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"city\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-mt-8\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>State<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"state\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-mt-8\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>Pincode<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"pincode\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"eh-mt-8\" fxLayout=\"row\">\r\n          <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n            <mat-label>Country<sup>*</sup></mat-label>\r\n            <input matInput formControlName=\"country\" readonly>\r\n          </mat-form-field>\r\n        </div>\r\n\r\n\r\n        <div class=\"e-title eh-mt-24\">\r\n          <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">co_present</mat-icon>\r\n          <span>Referral</span>\r\n        </div>\r\n\r\n        <div class=\"eh-mt-16\" fxLayout=\"row\">\r\n          <div class=\"eh-w-480\">\r\n            <mat-form-field appearance=\"standard\" class=\"e-mat-form-field eh-w-100p\">\r\n              <mat-label>Referred by</mat-label>\r\n              <input matInput formControlName=\"referredBy\" readonly>\r\n            </mat-form-field>\r\n          </div>\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <ng-template #isPackageNotSubscribedBlk>\r\n    <div class=\"eh-h-100p\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n      <span class=\"e-txt e-txt--placeholder\">This is a PRO-member area.</span>\r\n      <span class=\"e-txt e-txt--placeholder\">Please subscribe to a package to access this section.</span>\r\n      <button *ngIf=\"!me.flags.isOrientationScheduled\" mat-stroked-button\r\n        class=\"e-mat-button e-mat-button--eblue400 eh-mt-8\">\r\n        Resume On-boarding\r\n      </button>\r\n    </div>\r\n  </ng-template>\r\n\r\n</ng-container>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>\r\n\r\n<!-- <app-tabs></app-tabs> -->";

/***/ })

}]);
//# sourceMappingURL=src_app_profile_profile_module_ts.js.map