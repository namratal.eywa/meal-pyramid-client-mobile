"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_main_appointment_appointment_module_ts"],{

/***/ 88405:
/*!***************************************************************!*\
  !*** ./src/app/main/appointment/appoint/appoint.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointComponent": () => (/* binding */ AppointComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _appoint_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appoint.component.html?ngResource */ 62942);
/* harmony import */ var _appoint_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appoint.component.scss?ngResource */ 27720);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);













let AppointComponent = class AppointComponent {
  constructor(_as, _dialog, _aps, _shs, _hs, _sb) {
    this._as = _as;
    this._dialog = _dialog;
    this._aps = _aps;
    this._shs = _shs;
    this._hs = _hs;
    this._sb = _sb;
    this.allAppointments = [];
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.previousAppointments = [];
    this.upcomingAppointments = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      return this._aps.getAppointmentsByUser(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_12__.EMPTY)).subscribe(appts => {
      this.allAppointments = appts;
      this.setPreviousAppointments();
      this.setUpcomingAppointments();

      if (this.upcomingAppointments.length > 0) {
        [this.nextAppointment] = this.upcomingAppointments;
      } else {
        this.nextAppointment = null;
      }

      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  setPreviousAppointments() {
    this.previousAppointments.length = 0;
    this.previousAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) < new Date()).sort((a, b) => {
      return a.sessionStart < b.sessionStart ? 1 : -1;
    });
  }

  setUpcomingAppointments() {
    this.upcomingAppointments.length = 0;
    this.upcomingAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) > new Date()).sort((a, b) => {
      return a.sessionStart > b.sessionStart ? 1 : -1;
    });
  }

  showDialog() {
    this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__.GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: {
        step: this.me.onboardingStep || 0
      }
    });
  }

  onCancelClick(appt) {
    var _this = this;

    const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

    return new Promise(resolve => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          const body = {
            userUID: _this.me.uid,
            appointmentKey: appt.key
          };
          let h$;

          if (appt.type === src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.AppointmentTypes.RECALL) {
            h$ = _this._hs.cancelRecallAppointment(body);
          } else {
            h$ = _this._hs.cancelAdvancedTestingAppointment(body);
          }

          h$.subscribe(apiRes => {
            if (!apiRes.status) {
              _this._sb.openErrorSnackBar(res.error.description || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

              resolve(false);
              return;
            }

            _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessTypes.APPT_CANCELED);

            resolve(true);
          }, () => {
            _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

            resolve(false);
          });
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    });
  }

};

AppointComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AppointmentService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}];

AppointComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-appoint',
  template: _appoint_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_appoint_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AppointComponent);


/***/ }),

/***/ 29927:
/*!****************************************************************!*\
  !*** ./src/app/main/appointment/appointment-routing.module.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentRoutingModule": () => (/* binding */ AppointmentRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _view_history_view_history_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view-history/view-history.component */ 59454);
/* harmony import */ var _appoint_appoint_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appoint/appoint.component */ 88405);
/* harmony import */ var _previous_appointment_previous_appointment_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./previous-appointment/previous-appointment.component */ 25037);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _appointment_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./appointment.component */ 84381);
/* harmony import */ var _upcoming_appointment_upcoming_appointment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./upcoming-appointment/upcoming-appointment.component */ 4340);








const routes = [
    {
        path: '',
        component: _appointment_component__WEBPACK_IMPORTED_MODULE_3__.AppointmentComponent,
        children: [
            {
                path: 'previous',
                component: _previous_appointment_previous_appointment_component__WEBPACK_IMPORTED_MODULE_2__.PreviousAppointmentComponent
            },
            {
                path: 'upcoming',
                component: _upcoming_appointment_upcoming_appointment_component__WEBPACK_IMPORTED_MODULE_4__.UpcomingAppointmentComponent
            },
            {
                path: '',
                component: _appoint_appoint_component__WEBPACK_IMPORTED_MODULE_1__.AppointComponent
            },
            {
                path: 'history',
                component: _view_history_view_history_component__WEBPACK_IMPORTED_MODULE_0__.ViewHistoryComponent
            }
        ]
    }
];
let AppointmentRoutingModule = class AppointmentRoutingModule {
};
AppointmentRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule]
    })
], AppointmentRoutingModule);



/***/ }),

/***/ 84381:
/*!***********************************************************!*\
  !*** ./src/app/main/appointment/appointment.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentComponent": () => (/* binding */ AppointmentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appointment.component.html?ngResource */ 39281);
/* harmony import */ var _appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appointment.component.scss?ngResource */ 53017);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);

// ToDo: Disable all Formgroups & FormControls while any book/cancel operation is in progress.












let AppointmentComponent = class AppointmentComponent {
  constructor(_as, _dialog, _aps, _shs, _hs, _sb) {
    this._as = _as;
    this._dialog = _dialog;
    this._aps = _aps;
    this._shs = _shs;
    this._hs = _hs;
    this._sb = _sb;
    this.allAppointments = [];
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.previousAppointments = [];
    this.upcomingAppointments = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      return this._aps.getAppointmentsByUser(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_12__.EMPTY)).subscribe(appts => {
      this.allAppointments = appts;
      this.setPreviousAppointments();
      this.setUpcomingAppointments();

      if (this.upcomingAppointments.length > 0) {
        [this.nextAppointment] = this.upcomingAppointments;
      } else {
        this.nextAppointment = null;
      }

      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  setPreviousAppointments() {
    this.previousAppointments.length = 0;
    this.previousAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) < new Date()).sort((a, b) => {
      return a.sessionStart < b.sessionStart ? 1 : -1;
    });
  }

  setUpcomingAppointments() {
    this.upcomingAppointments.length = 0;
    this.upcomingAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) > new Date()).sort((a, b) => {
      return a.sessionStart > b.sessionStart ? 1 : -1;
    });
  }

  showDialog() {
    this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__.GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: {
        step: this.me.onboardingStep || 0
      }
    });
  }

  onCancelClick(appt) {
    var _this = this;

    const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

    return new Promise(resolve => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          const body = {
            userUID: _this.me.uid,
            appointmentKey: appt.key
          };
          let h$;

          if (appt.type === src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.AppointmentTypes.RECALL) {
            h$ = _this._hs.cancelRecallAppointment(body);
          } else {
            h$ = _this._hs.cancelAdvancedTestingAppointment(body);
          }

          h$.subscribe(apiRes => {
            if (!apiRes.status) {
              _this._sb.openErrorSnackBar(res.error.description || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

              resolve(false);
              return;
            }

            _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessTypes.APPT_CANCELED);

            resolve(true);
          }, () => {
            _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

            resolve(false);
          });
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    });
  }

};

AppointmentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AppointmentService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}];

AppointmentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-appointment',
  template: _appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], AppointmentComponent);


/***/ }),

/***/ 26109:
/*!********************************************************!*\
  !*** ./src/app/main/appointment/appointment.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppointmentModule": () => (/* binding */ AppointmentModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _view_history_view_history_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./view-history/view-history.component */ 59454);
/* harmony import */ var _appoint_appoint_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./appoint/appoint.component */ 88405);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 93819);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _appointment_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./appointment-routing.module */ 29927);
/* harmony import */ var _appointment_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./appointment.component */ 84381);
/* harmony import */ var src_app_shared_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/layout */ 47633);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/flex-layout */ 62681);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);
/* harmony import */ var src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/material-design */ 12497);
/* harmony import */ var src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared-components */ 35886);
/* harmony import */ var _previous_appointment_previous_appointment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./previous-appointment/previous-appointment.component */ 25037);
/* harmony import */ var _upcoming_appointment_upcoming_appointment_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./upcoming-appointment/upcoming-appointment.component */ 4340);
















let AppointmentModule = class AppointmentModule {
};
AppointmentModule = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.NgModule)({
        declarations: [_appointment_component__WEBPACK_IMPORTED_MODULE_3__.AppointmentComponent,
            _previous_appointment_previous_appointment_component__WEBPACK_IMPORTED_MODULE_8__.PreviousAppointmentComponent,
            _appoint_appoint_component__WEBPACK_IMPORTED_MODULE_1__.AppointComponent,
            _upcoming_appointment_upcoming_appointment_component__WEBPACK_IMPORTED_MODULE_9__.UpcomingAppointmentComponent,
            _view_history_view_history_component__WEBPACK_IMPORTED_MODULE_0__.ViewHistoryComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.IonicModule,
            _appointment_routing_module__WEBPACK_IMPORTED_MODULE_2__.AppointmentRoutingModule,
            src_app_shared_layout__WEBPACK_IMPORTED_MODULE_4__.LayoutModule,
            src_app_shared_material_design__WEBPACK_IMPORTED_MODULE_6__.MaterialDesignModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_14__.FlexLayoutModule,
            src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_5__.GetStartedModule,
            src_app_shared_shared_components__WEBPACK_IMPORTED_MODULE_7__.SharedComponentsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_15__.ReactiveFormsModule,
        ],
    })
], AppointmentModule);



/***/ }),

/***/ 25037:
/*!*****************************************************************************************!*\
  !*** ./src/app/main/appointment/previous-appointment/previous-appointment.component.ts ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreviousAppointmentComponent": () => (/* binding */ PreviousAppointmentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _previous_appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./previous-appointment.component.html?ngResource */ 64405);
/* harmony import */ var _previous_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./previous-appointment.component.scss?ngResource */ 88469);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);













let PreviousAppointmentComponent = class PreviousAppointmentComponent {
  constructor(_as, _dialog, _aps, _shs, _hs, _sb) {
    this._as = _as;
    this._dialog = _dialog;
    this._aps = _aps;
    this._shs = _shs;
    this._hs = _hs;
    this._sb = _sb;
    this.allAppointments = [];
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.previousAppointments = [];
    this.upcomingAppointments = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      return this._aps.getAppointmentsByUser(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_12__.EMPTY)).subscribe(appts => {
      this.allAppointments = appts;
      this.setPreviousAppointments();
      this.setUpcomingAppointments();

      if (this.upcomingAppointments.length > 0) {
        [this.nextAppointment] = this.upcomingAppointments;
      } else {
        this.nextAppointment = null;
      }

      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  setPreviousAppointments() {
    this.previousAppointments.length = 0;
    this.previousAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) < new Date()).sort((a, b) => {
      return a.sessionStart < b.sessionStart ? 1 : -1;
    });
  }

  setUpcomingAppointments() {
    this.upcomingAppointments.length = 0;
    this.upcomingAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) > new Date()).sort((a, b) => {
      return a.sessionStart > b.sessionStart ? 1 : -1;
    });
  }

  showDialog() {
    this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__.GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: {
        step: this.me.onboardingStep || 0
      }
    });
  }

  onCancelClick(appt) {
    var _this = this;

    const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

    return new Promise(resolve => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          const body = {
            userUID: _this.me.uid,
            appointmentKey: appt.key
          };
          let h$;

          if (appt.type === src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.AppointmentTypes.RECALL) {
            h$ = _this._hs.cancelRecallAppointment(body);
          } else {
            h$ = _this._hs.cancelAdvancedTestingAppointment(body);
          }

          h$.subscribe(apiRes => {
            if (!apiRes.status) {
              _this._sb.openErrorSnackBar(res.error.description || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

              resolve(false);
              return;
            }

            _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessTypes.APPT_CANCELED);

            resolve(true);
          }, () => {
            _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

            resolve(false);
          });
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    });
  }

};

PreviousAppointmentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AppointmentService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}];

PreviousAppointmentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-previous-appointment',
  template: _previous_appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_previous_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], PreviousAppointmentComponent);


/***/ }),

/***/ 4340:
/*!*****************************************************************************************!*\
  !*** ./src/app/main/appointment/upcoming-appointment/upcoming-appointment.component.ts ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpcomingAppointmentComponent": () => (/* binding */ UpcomingAppointmentComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _upcoming_appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./upcoming-appointment.component.html?ngResource */ 55017);
/* harmony import */ var _upcoming_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./upcoming-appointment.component.scss?ngResource */ 57289);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);













let UpcomingAppointmentComponent = class UpcomingAppointmentComponent {
  constructor(_as, _dialog, _aps, _shs, _hs, _sb) {
    this._as = _as;
    this._dialog = _dialog;
    this._aps = _aps;
    this._shs = _shs;
    this._hs = _hs;
    this._sb = _sb;
    this.allAppointments = [];
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.previousAppointments = [];
    this.upcomingAppointments = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      return this._aps.getAppointmentsByUser(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_12__.EMPTY)).subscribe(appts => {
      this.allAppointments = appts;
      this.setPreviousAppointments();
      this.setUpcomingAppointments();

      if (this.upcomingAppointments.length > 0) {
        [this.nextAppointment] = this.upcomingAppointments;
      } else {
        this.nextAppointment = null;
      }

      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  setPreviousAppointments() {
    this.previousAppointments.length = 0;
    this.previousAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) < new Date()).sort((a, b) => {
      return a.sessionStart < b.sessionStart ? 1 : -1;
    });
  }

  setUpcomingAppointments() {
    this.upcomingAppointments.length = 0;
    this.upcomingAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) > new Date()).sort((a, b) => {
      return a.sessionStart > b.sessionStart ? 1 : -1;
    });
  }

  showDialog() {
    this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__.GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: {
        step: this.me.onboardingStep || 0
      }
    });
  }

  onCancelClick(appt) {
    var _this = this;

    const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

    return new Promise(resolve => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          const body = {
            userUID: _this.me.uid,
            appointmentKey: appt.key
          };
          let h$;

          if (appt.type === src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.AppointmentTypes.RECALL) {
            h$ = _this._hs.cancelRecallAppointment(body);
          } else {
            h$ = _this._hs.cancelAdvancedTestingAppointment(body);
          }

          h$.subscribe(apiRes => {
            if (!apiRes.status) {
              _this._sb.openErrorSnackBar(res.error.description || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

              resolve(false);
              return;
            }

            _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessTypes.APPT_CANCELED);

            resolve(true);
          }, () => {
            _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

            resolve(false);
          });
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    });
  }

};

UpcomingAppointmentComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AppointmentService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}];

UpcomingAppointmentComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-upcoming-appointment',
  template: _upcoming_appointment_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_upcoming_appointment_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], UpcomingAppointmentComponent);


/***/ }),

/***/ 59454:
/*!*************************************************************************!*\
  !*** ./src/app/main/appointment/view-history/view-history.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ViewHistoryComponent": () => (/* binding */ ViewHistoryComponent)
/* harmony export */ });
/* harmony import */ var D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./node_modules/@babel/runtime/helpers/esm/asyncToGenerator.js */ 71670);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _view_history_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./view-history.component.html?ngResource */ 87504);
/* harmony import */ var _view_history_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view-history.component.scss?ngResource */ 82013);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/dialog */ 31484);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ 92218);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs */ 26439);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/operators */ 59095);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ 85921);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 47418);
/* harmony import */ var src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/common/constants */ 31896);
/* harmony import */ var src_app_common_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/models */ 18073);
/* harmony import */ var src_app_core_services__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services */ 98138);
/* harmony import */ var src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/dialog-box */ 55599);
/* harmony import */ var src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/get-started */ 14526);













let ViewHistoryComponent = class ViewHistoryComponent {
  constructor(_as, _dialog, _aps, _shs, _hs, _sb) {
    this._as = _as;
    this._dialog = _dialog;
    this._aps = _aps;
    this._shs = _shs;
    this._hs = _hs;
    this._sb = _sb;
    this.demo1TabIndex = 1;
    this.allAppointments = [];
    this.dispatchView = false;
    this.isPackageSubscribed = false;
    this.previousAppointments = [];
    this.upcomingAppointments = [];
    this._me$ = this._as.getMe();
    this._notifier$ = new rxjs__WEBPACK_IMPORTED_MODULE_8__.Subject();
  }

  ngOnInit() {
    this._me$.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_9__.switchMap)(me => {
      this.me = me;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      return this._aps.getAppointmentsByUser(this.me.uid, this.me.membershipKey);
    }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_10__.takeUntil)(this._notifier$), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.catchError)(() => rxjs__WEBPACK_IMPORTED_MODULE_12__.EMPTY)).subscribe(appts => {
      this.allAppointments = appts;
      this.setPreviousAppointments();
      this.setUpcomingAppointments();

      if (this.upcomingAppointments.length > 0) {
        [this.nextAppointment] = this.upcomingAppointments;
      } else {
        this.nextAppointment = null;
      }

      this.dispatchView = true;
    });
  }

  ngOnDestroy() {
    this._notifier$.next();

    this._notifier$.complete();
  }

  setPreviousAppointments() {
    this.previousAppointments.length = 0;
    this.previousAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) < new Date()).sort((a, b) => {
      return a.sessionStart < b.sessionStart ? 1 : -1;
    });
  }

  setUpcomingAppointments() {
    this.upcomingAppointments.length = 0;
    this.upcomingAppointments = this.allAppointments.filter(item => new Date(item.sessionStart) > new Date()).sort((a, b) => {
      return a.sessionStart > b.sessionStart ? 1 : -1;
    });
  }

  showDialog() {
    this._dialog.open(src_app_shared_get_started__WEBPACK_IMPORTED_MODULE_7__.GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: {
        step: this.me.onboardingStep || 0
      }
    });
  }

  onCancelClick(appt) {
    var _this = this;

    const dialogBox = new src_app_common_models__WEBPACK_IMPORTED_MODULE_4__.DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';

    this._shs.setDialogBox(dialogBox);

    const dialogRef = this._dialog.open(src_app_shared_dialog_box__WEBPACK_IMPORTED_MODULE_6__.DialogBoxComponent);

    return new Promise(resolve => {
      dialogRef.afterClosed().subscribe( /*#__PURE__*/function () {
        var _ref = (0,D_Code_meal_pyramid_client_mobile_node_modules_babel_runtime_helpers_esm_asyncToGenerator_js__WEBPACK_IMPORTED_MODULE_0__["default"])(function* (res) {
          if (!res) {
            resolve(false);
            return;
          }

          const body = {
            userUID: _this.me.uid,
            appointmentKey: appt.key
          };
          let h$;

          if (appt.type === src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.AppointmentTypes.RECALL) {
            h$ = _this._hs.cancelRecallAppointment(body);
          } else {
            h$ = _this._hs.cancelAdvancedTestingAppointment(body);
          }

          h$.subscribe(apiRes => {
            if (!apiRes.status) {
              _this._sb.openErrorSnackBar(res.error.description || src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

              resolve(false);
              return;
            }

            _this._sb.openSuccessSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.SuccessTypes.APPT_CANCELED);

            resolve(true);
          }, () => {
            _this._sb.openErrorSnackBar(src_app_common_constants__WEBPACK_IMPORTED_MODULE_3__.ErrorTypes.SYSTEM);

            resolve(false);
          });
        });

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    });
  }

};

ViewHistoryComponent.ctorParameters = () => [{
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AuthService
}, {
  type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_13__.MatDialog
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.AppointmentService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SharedService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.HttpService
}, {
  type: src_app_core_services__WEBPACK_IMPORTED_MODULE_5__.SnackBarService
}];

ViewHistoryComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([(0,_angular_core__WEBPACK_IMPORTED_MODULE_15__.Component)({
  selector: 'app-view-history',
  template: _view_history_component_html_ngResource__WEBPACK_IMPORTED_MODULE_1__,
  styles: [_view_history_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_2__]
})], ViewHistoryComponent);


/***/ }),

/***/ 27720:
/*!****************************************************************************!*\
  !*** ./src/app/main/appointment/appoint/appoint.component.scss?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBvaW50LmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 53017:
/*!************************************************************************!*\
  !*** ./src/app/main/appointment/appointment.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBvaW50bWVudC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 88469:
/*!******************************************************************************************************!*\
  !*** ./src/app/main/appointment/previous-appointment/previous-appointment.component.scss?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcmV2aW91cy1hcHBvaW50bWVudC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 57289:
/*!******************************************************************************************************!*\
  !*** ./src/app/main/appointment/upcoming-appointment/upcoming-appointment.component.scss?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1cGNvbWluZy1hcHBvaW50bWVudC5jb21wb25lbnQuc2NzcyJ9 */";

/***/ }),

/***/ 82013:
/*!**************************************************************************************!*\
  !*** ./src/app/main/appointment/view-history/view-history.component.scss?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = ".mat-ripple.mat-tab-label.mat-focus-indicator.mat-tab-label.ng-star-inserted {\n  opacity: 1;\n}\n\n::ng-deep .mat-tab-label-active {\n  border-bottom: 2px solid #ff4081 !important;\n}\n\n::ng-deep .mat-tab-group.mat-primary .mat-ink-bar,\n.mat-tab-nav-bar.mat-primary .mat-ink-bar {\n  background-color: #ff4081;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpZXctaGlzdG9yeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFNQTtFQUNFLFVBQUE7QUFMRjs7QUFRQTtFQUNFLDJDQUFBO0FBTEY7O0FBUUE7O0VBRUUseUJBQUE7QUFMRiIsImZpbGUiOiJ2aWV3LWhpc3RvcnkuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBAbWVkaWEgKG1heC13aWR0aDogNTk5cHgpIHtcclxuLy8gICAubWF0LXRhYi1sYWJlbCB7XHJcbi8vICAgICBtaW4td2lkdGg6IDIwMHB4O1xyXG4vLyAgIH1cclxuLy8gfVxyXG5cclxuLm1hdC1yaXBwbGUubWF0LXRhYi1sYWJlbC5tYXQtZm9jdXMtaW5kaWNhdG9yLm1hdC10YWItbGFiZWwubmctc3Rhci1pbnNlcnRlZCB7XHJcbiAgb3BhY2l0eTogMTtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWxhYmVsLWFjdGl2ZSB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkICNmZjQwODEgIWltcG9ydGFudDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtdGFiLWdyb3VwLm1hdC1wcmltYXJ5IC5tYXQtaW5rLWJhcixcclxuLm1hdC10YWItbmF2LWJhci5tYXQtcHJpbWFyeSAubWF0LWluay1iYXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjQwODE7XHJcbn0iXX0= */";

/***/ }),

/***/ 62942:
/*!****************************************************************************!*\
  !*** ./src/app/main/appointment/appoint/appoint.component.html?ngResource ***!
  \****************************************************************************/
/***/ ((module) => {

module.exports = "<ion-app>\r\n  <app-header *ngIf=\"dispatchView; else dataAwaitedBlk\" title=\"Appointment\"></app-header>\r\n  <ion-content class=\"ion-padding eh-py-0\">\r\n    <ng-container *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n      <ng-container *ngIf=\"isPackageSubscribed; else isPackageNotSubscribedBlk\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center\" class=\"eh-w-150 \">\r\n          <button *ngIf=\"!loadingSlots\" mat-flat-button [routerLink]=\"['/tab/appointments/history']\"\r\n            [ngClass]=\"{'e-mat-button e-mat-button--eblue400 eh-w-100p': true, 'eh-csr-dsb': !selectedSlot}\">\r\n            Appointment History\r\n          </button>\r\n          <div *ngIf=\"loadingSlots\" class=\"e-spinner e-spinner--2 eh-h-36\">\r\n            <mat-spinner [diameter]=\"24\" [strokeWidth]=\"2\"></mat-spinner>\r\n          </div>\r\n        </div>\r\n        <br>\r\n        <div fxLayout=\"column\">\r\n          <div class=\"e-card2 e-card2--shadow\">\r\n            <div class=\"e-card2__header\">\r\n              <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">event</mat-icon>\r\n              <span class=\"eh-txt\">\r\n                Next Appointment\r\n              </span>\r\n            </div>\r\n            <div class=\"eh-p-6\">\r\n              <app-appointment-doc *ngIf=\"nextAppointment; else noNextAppointmentBlk\" [appointment]=\"nextAppointment\"\r\n                (onCancel)=\"onCancelClick($event)\">\r\n              </app-appointment-doc>\r\n\r\n              <ng-template #noNextAppointmentBlk>\r\n                <div class=\"e-txt e-txt--placeholder\">\r\n                  No Upcoming Appointments\r\n                </div>\r\n              </ng-template>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"e-card2 e-card2--shadow eh-mt-16\" fxLayout=\"column\">\r\n          <div class=\"e-card2__header\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">edit_calendar</mat-icon>\r\n            <span class=\"eh-txt\">\r\n              Book Appointment\r\n            </span>\r\n          </div>\r\n          <app-book-appointment></app-book-appointment>\r\n        </div>\r\n\r\n\r\n\r\n        <!-- <div class=\"e-card2 e-card2--shadow eh-h-100p\" fxLayout=\"column\">\r\n          <div class=\"e-card2__header\">\r\n            <mat-icon class=\"e-mat-icon e-mat-icon--3 eh-mr-8\">calendar_view_month</mat-icon>\r\n            <span class=\"eh-txt\">\r\n              All My Appointments\r\n            </span>\r\n          </div>\r\n\r\n          <div class=\"eh-ofy-auto eh-hide-scrollbars\">\r\n            <mat-tab-group class=\"e-mat-tab-group e-mat-tab-group--1 eh-h-100p\" animationDuration=\"0ms\"\r\n              [selectedIndex]=\"0\">\r\n              <mat-tab label=\"Previous\">\r\n                <div class=\"eh-p-2 eh-ofy-scroll eh-hide-scrollbars\">\r\n                  <ng-container *ngIf=\"previousAppointments.length > 0; else noPreviousAppointmentsBlk\">\r\n                    <div *ngFor=\"let appt of previousAppointments\" class=\"eh-mt-16 eh-t-spacer\">\r\n                      <app-appointment-doc [appointment]=\"appt\" (onCancel)=\"onCancelClick($event)\">\r\n                      </app-appointment-doc>\r\n                    </div>\r\n                  </ng-container>\r\n                  <ng-template #noPreviousAppointmentsBlk>\r\n                    <div fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                      <div class=\" e-txt e-txt--placeholder\">\r\n                        No Previous Appointments\r\n                      </div>\r\n                    </div>\r\n                  </ng-template>\r\n                </div>\r\n              </mat-tab>\r\n              <mat-tab label=\"Upcoming\">\r\n                <div class=\"eh-p-2 eh-ofy-scroll eh-hide-scrollbars\">\r\n                  <ng-container *ngIf=\"upcomingAppointments.length > 0; else noUpcomingAppointmentsBlk\">\r\n                    <div *ngFor=\"let appt of upcomingAppointments\" class=\"eh-mt-16 eh-t-spacer\">\r\n                      <app-appointment-doc [appointment]=\"appt\" (onCancel)=\"onCancelClick($event)\">\r\n                      </app-appointment-doc>\r\n                    </div>\r\n                  </ng-container>\r\n\r\n                </div>\r\n\r\n                <ng-template #noUpcomingAppointmentsBlk>\r\n                  <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <div class=\"e-txt e-txt--placeholder\">\r\n                      No Upcoming Appointments\r\n                    </div>\r\n                  </div>\r\n                </ng-template>\r\n              </mat-tab>\r\n            </mat-tab-group>\r\n          </div>\r\n        </div> -->\r\n      </ng-container>\r\n\r\n      <ng-template #isPackageNotSubscribedBlk>\r\n        <div class=\"eh-h-100p\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n          <span class=\"e-txt e-txt--placeholder\">This is a PRO-member area.</span>\r\n          <span class=\"e-txt e-txt--placeholder\">Please subscribe to a package to access this section.</span>\r\n          <button *ngIf=\"!me.flags.isOrientationScheduled\" mat-stroked-button\r\n            class=\"e-mat-button e-mat-button--eblue400 eh-mt-8\" (click)=\"showDialog()\">\r\n            Resume On-boarding\r\n          </button>\r\n        </div>\r\n      </ng-template>\r\n    </ng-container>\r\n    <ng-template #dataAwaitedBlk>\r\n      <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n        <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n      </div>\r\n    </ng-template>\r\n  </ion-content>\r\n  <!-- <app-tabs></app-tabs> -->\r\n</ion-app>";

/***/ }),

/***/ 39281:
/*!************************************************************************!*\
  !*** ./src/app/main/appointment/appointment.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "<ion-app>\r\n    <ion-router-outlet ></ion-router-outlet>\r\n</ion-app>";

/***/ }),

/***/ 64405:
/*!******************************************************************************************************!*\
  !*** ./src/app/main/appointment/previous-appointment/previous-appointment.component.html?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-padding eh-txt-16\"> Previous Appointment</ion-title>\r\n    < <ion-buttons slot=\"end\">\r\n      >\r\n      <app-profile-avatar></app-profile-avatar>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content class=\"eh-px-10\" *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"eh-h-100p\" fxLayout=\"column\">\r\n    <div class=\"eh-ofy-auto eh-hide-scrollbars\">\r\n      <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n        <ng-container *ngIf=\"previousAppointments.length > 0; else noPreviousAppointmentsBlk\">\r\n          <div class=\"e-card2 e-card2--hover2\" *ngFor=\"let appt of previousAppointments\" class=\"eh-mt-16 eh-t-spacer\">\r\n            <app-appointment-doc [appointment]=\"appt\" (onCancel)=\"onCancelClick($event)\">\r\n            </app-appointment-doc>\r\n          </div>\r\n\r\n        </ng-container>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</ion-content>\r\n<ng-template #noPreviousAppointmentsBlk>\r\n  <div fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n    <div class=\" e-txt e-txt--placeholder\">\r\n      No Previous Appointments\r\n    </div>\r\n  </div>\r\n</ng-template>\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 55017:
/*!******************************************************************************************************!*\
  !*** ./src/app/main/appointment/upcoming-appointment/upcoming-appointment.component.html?ngResource ***!
  \******************************************************************************************************/
/***/ ((module) => {

module.exports = "<app-header title=\"Upcoming Appointment\"></app-header>\r\n<ion-content class=\"eh-px-10\" *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n  <div class=\"e-card2 e-card2--shadow eh-h-100p\" fxLayout=\"column\">\r\n    <div class=\"eh-ofy-auto eh-hide-scrollbars\">\r\n      <div class=\"eh-ofy-scroll eh-hide-scrollbars\">\r\n        <ng-container *ngIf=\"upcomingAppointments.length > 0; else noUpcomingAppointmentsBlk\">\r\n          <div *ngFor=\"let appt of upcomingAppointments\" class=\"e-card2 e-card2--hover2 eh-mt-16 eh-t-spacer\">\r\n            <app-appointment-doc [appointment]=\"appt\" (onCancel)=\"onCancelClick($event)\">\r\n            </app-appointment-doc>\r\n          </div>\r\n        </ng-container>\r\n      </div>\r\n    </div>\r\n    <ng-template #noUpcomingAppointmentsBlk>\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"center center\" class=\"eh-h-100v eh-xy-center e-txt e-txt--placeholder\">\r\n\r\n        No Upcoming Appointments\r\n      </div>\r\n    </ng-template>\r\n  </div>\r\n</ion-content>\r\n\r\n<ng-template #dataAwaitedBlk>\r\n  <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n    <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n  </div>\r\n</ng-template>";

/***/ }),

/***/ 87504:
/*!**************************************************************************************!*\
  !*** ./src/app/main/appointment/view-history/view-history.component.html?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "<ion-app>\r\n  <app-header title=\"All Appointment\"></app-header>\r\n  <ion-content class=\"eh-px-10\" *ngIf=\"dispatchView; else dataAwaitedBlk\">\r\n    <div class=\"eh-hide-scrollbars eh-py-0 eh-px-6\">\r\n      <mat-tab-group class=\"e-mat-tab-group e-mat-tab-group--1 eh-h-100p\" animationDuration=\"0ms\"\r\n        mat-align-tabs=\"center\" [(selectedIndex)]=\"Upcoming\" [disableRipple]=\"true\" headerPosition=\"above\">\r\n        <mat-tab label=\"Upcoming\">\r\n          <div class=\"eh-p-2 eh-ofy-scroll eh-hide-scrollbars\">\r\n            <ng-container *ngIf=\"upcomingAppointments.length > 0; else noUpcomingAppointmentsBlk\">\r\n              <div *ngFor=\"let appt of upcomingAppointments\" class=\"eh-mt-16 \">\r\n                <app-appointment-doc [appointment]=\"appt\" (onCancel)=\"onCancelClick($event)\">\r\n                </app-appointment-doc>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n        </mat-tab>\r\n        <mat-tab label=\"Previous\">\r\n          <div class=\"eh-p-2 eh-ofy-scroll eh-hide-scrollbars\">\r\n            <ng-container *ngIf=\"previousAppointments.length > 0; else noPreviousAppointmentsBlk\">\r\n              <div *ngFor=\"let appt of previousAppointments\" class=\"eh-mt-16 \">\r\n                <app-appointment-doc [appointment]=\"appt\" (onCancel)=\"onCancelClick($event)\">\r\n                </app-appointment-doc>\r\n              </div>\r\n            </ng-container>\r\n            <ng-template #noPreviousAppointmentsBlk>\r\n              <div fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                <div class=\" e-txt e-txt--placeholder\">\r\n                  No Previous Appointments\r\n                </div>\r\n              </div>\r\n            </ng-template>\r\n          </div>\r\n        </mat-tab>\r\n      </mat-tab-group>\r\n    </div>\r\n    <ng-template #noUpcomingAppointmentsBlk>\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"center center\" class=\"eh-h-100v eh-xy-center e-txt e-txt--placeholder\">\r\n\r\n        No Upcoming Appointments\r\n      </div>\r\n    </ng-template>\r\n  </ion-content>\r\n  <!-- <app-tabs></app-tabs> -->\r\n  <ng-template #dataAwaitedBlk>\r\n    <div class=\"e-spinner e-spinner--center eh-h-100v eh-h-100p\">\r\n      <mat-spinner [diameter]=\"30\" [strokeWidth]=\"3\"></mat-spinner>\r\n    </div>\r\n  </ng-template>\r\n</ion-app>";

/***/ })

}]);
//# sourceMappingURL=src_app_main_appointment_appointment_module_ts.js.map