import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Filee2 } from 'src/app/common/models';
import { StorageService, GeneralService } from 'src/app/core/services';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { ActionSheetController, Platform } from '@ionic/angular';
import { Capacitor } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Lightbox, LightboxConfig } from 'ngx-lightbox';
import { LightboxConfigs } from 'src/app/common/constants';
import { Crop } from '@ionic-native/crop/ngx';
const IMAGE_DIR = 'stored-images';
export interface UserPhoto {
  filepath: string;
  webviewPath: string;
}
interface LocalFile {
  name: string;
  path: string;
  data: string;
}
@Component({
  selector: 'app-button-file-upload',
  templateUrl: './button-file-upload.component.html',
  styleUrls: ['./button-file-upload.component.scss']
})
export class ButtonFileUploadComponent implements OnInit {



  @Input() acceptedFileTypes = '.jpg, .jpeg, .png, .pdf';
  @Input() disabled = false;
  @Input() uploadByName = '';
  @Input() uploadByUID = '';
  @Input() uploadCollectionName = '';
  @Input() uploadDirPath = '';
  @Input() uploadFileNamePrefix = '';
  @Input() userMembershipKey = '';
  @Input() userUID = '';
  @Input() title = 'Upload';
  @Output() availableEvent = new EventEmitter<any>();
  @Output() uploadEvent = new EventEmitter<any>();


  public uploading = false;
  public PHOTO_STORAGE: string = 'photos';
  public photo: SafeResourceUrl;
  public photos: UserPhoto[] = [];
  public images: LocalFile[] = [];
  public srcImage: string = "";
  lightboxAlbum = [];
  guestPicture: string;
  constructor(private _sts: StorageService,
    private _gs: GeneralService,
    private platform: Platform,
    private actionSheetCtrl: ActionSheetController,
    private _lightbox: Lightbox,
    private _lightboxConfig: LightboxConfig,
    private sanitizer: DomSanitizer,
    private crop: Crop,) { }

  ngOnInit(): void {
    // console.log("photo", this.photos.)
    this.loadFiles();
  }

  onFileUpload(e: any): void {
    if (!e || !e.target || !e.target.files) {
      return;
    }
    console.log("", e)
    const fileList: File[] = e.target.files;
    if (fileList.length !== 1) {
      return;
    }
    const [file] = fileList;

    this.availableEvent.emit({
      fileSize: file.size,
      initialFileName: file.name
    });

    this.uploading = true;

    const { fName, fPath, uploadRef, snapshotChanges$ } = this._sts.upload(
      file,
      this.uploadDirPath,
      this.uploadFileNamePrefix,
      this.userUID
    );

    snapshotChanges$
      .pipe(
        finalize(async () => {
          const fKey = this.userUID
            ? this._gs.getNewKey(this.uploadCollectionName, true, this.userUID)
            : this._gs.getNewKey(this.uploadCollectionName);
          const fURL = await uploadRef.getDownloadURL().toPromise();

          const fileObj = new Filee2();
          fileObj.key = fKey;
          fileObj.fileName = fName;
          fileObj.filePath = fPath;
          fileObj.fileURL = fURL;
          fileObj.fileType = file.type;
          fileObj.fileSize = file.size;
          fileObj.uploadedBy.uid = this.uploadByUID;
          fileObj.uploadedBy.name = this.uploadByName;
          fileObj.uploadedAt = new Date();
          fileObj.isActive = true;
          fileObj.recStatus = true;

          if (this.userUID) {
            fileObj.uid = this.userUID;
            fileObj.membershipKey = this.userMembershipKey;
            await this._gs.setData(
              this.userUID,
              this.uploadCollectionName,
              fileObj,
              true,
              ['uploadedAt'],
              true,
              fKey
            );
          } else {
            await this._gs.setData(
              fKey,
              this.uploadCollectionName,
              fileObj,
              true,
              ['uploadedAt']
            );
          }

          this.uploadEvent.emit({
            fileKey: fKey,
            fileName: fName,
            fileType: file.type,
            filePath: fPath,
            fileURL: fURL,
            fileSize: file.size,
            initialFileName: file.name
          });
          this.uploading = false;
        })
      )
      .subscribe();
  }


  public getSantizeUrl(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  public async CameraPhotos() {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 50,
      allowEditing: true
    });
    this.guestPicture = capturedPhoto.base64String;
    console.log(this.guestPicture);
    const savedImageFile = await this.savePicture(capturedPhoto);
    this.photos.unshift({
      filepath: "soon...",
      webviewPath: savedImageFile.webviewPath
    });
    console.log(savedImageFile.webviewPath)
    Preferences.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.photos),
    });
  }

  public async addPhotoToGallery() {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Photos,
      quality: 50,
      allowEditing: true
    })
    // Save the picture and add it to photo collection
    let savedImageFile = await this.savePicture(capturedPhoto);
    this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(capturedPhoto && (capturedPhoto.webPath));
    this.photos.unshift(savedImageFile);
  }
  public async loadSaved() {
    // Retrieve cached photo array data
    const photoList = await Preferences.get({ key: this.PHOTO_STORAGE });
    this.photos = JSON.parse(photoList.value) || [];

    // If running on the web...
    if (!this.platform.is('hybrid')) {
      // Display the photo by reading into base64 format
      for (let photo of this.photos) {
        // Read each saved photo's data from the Filesystem
        const readFile = await Filesystem.readFile({
          path: photo.filepath,
          directory: Directory.Data,
        });

        // Web platform only: Load the photo as base64 data
        photo.webviewPath = `data:image/jpeg;base64,${readFile.data}`;
        this.uploadEvent.emit(photo.webviewPath);
      }
    }
  }

  async loadFiles() {
    this.images = [];
    Filesystem.readdir({
      path: IMAGE_DIR,
      directory: Directory.Data
    })
      .then(
        (result) => {
          this.loadFileData(result.files);
          console.log(result.files)
        },
        async (err) => {
          // Folder does not yet exists!
          await Filesystem.mkdir({
            path: IMAGE_DIR,
            directory: Directory.Data
          });
        }
      )
  }
  // Save picture to file on device
  private async savePicture(photo: Photo) {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(photo);
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data,
    })
    console.log(base64Data)

    this.srcImage = base64Data;
    this.photos.forEach(childObj => {
      console.log(childObj.webviewPath, childObj.filepath)
    });
    if (this.platform.is('hybrid')) {
      // Display the new image by rewriting the 'file://' path to HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      return {
        filepath: savedFile.uri,
        webviewPath: Capacitor.convertFileSrc(savedFile.uri),
      };
    } else {
      // Use webPath to display the new image instead of base64 since it's
      // already loaded into memory
      return {
        filepath: fileName,
        webviewPath: photo.webPath,
      };
    }
  }
  // base on the name of the file
  async loadFileData(fileNames: any) {
    for (let f of fileNames) {
      const filePath = `${IMAGE_DIR}/${f}`;

      const readFile = await Filesystem.readFile({
        path: filePath,
        directory: Directory.Data
      });

      this.images.push({
        name: f,
        path: filePath,
        data: `data:image/jpeg;base64,${readFile.data}`
      });
    }
  }

  private async readAsBase64(photo: Photo) {
    // "hybrid" will detect Cordova or Capacitor
    if (this.platform.is('hybrid')) {
      // Read the file into base64 format
      const file = await Filesystem.readFile({
        path: photo.path
      });

      return file.data;
    }
    else {
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(photo.webPath);
      const blob = await response.blob();
      console.log(this.convertBlobToBase64(blob));

      return await this.convertBlobToBase64(blob) as string;
    }
  }
  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  deleteFile(i: number) {

    this.photos.pop();
  }
  // cropImage(fileUrl) {
  //   this.crop.crop(fileUrl, { quality: 50 })
  //     .then(
  //       newPath => {
  //         this.showCroppedImage(newPath.split('?')[0])
  //       },
  //       error => {
  //         alert('Error cropping image' + error);
  //       }
  //     );
  // }

  
  async alertPhoto() {
    let actionSheet = this.actionSheetCtrl.create({
      header: 'Please select option',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take photo',
          icon: 'camera',
          handler: () => {
            this.CameraPhotos();
          }
        },
        {
          text: 'Choose photo from Gallery',
          icon: 'folder-open',
          handler: () => {
            this.addPhotoToGallery();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: 'close',
          data: {
            action: 'cancel',
          },
        },
      ]
    });
    (await actionSheet).present();
  }

  // *
  // * Lightbox
  // *

  lightboxInit(): void {
    Object.assign(this._lightboxConfig, LightboxConfigs);
  }


}