import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ButtonFileUploadComponent } from './button-file-upload.component';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { MaterialDesignModule } from '../material-design';
@NgModule({
  declarations: [ButtonFileUploadComponent],
  imports: [CommonModule,IonicModule,FormsModule,LayoutModule,MaterialDesignModule,FlexLayoutModule,  SharedComponentsModule],
  exports: [ButtonFileUploadComponent]
})
export class ButtonFileUploadModule {}

