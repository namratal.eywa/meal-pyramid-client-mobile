import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.component.html',
  styleUrls: ['./get-started.component.scss']
})
export class GetStartedComponent implements OnInit, AfterViewInit {
  @ViewChild('stepper') stepper: MatStepper;

  constructor(
    private _dialog: MatDialogRef<GetStartedComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { step: number }
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.stepper.steps.forEach((item, i) => {
      item.completed = false;

      if (i < this.data.step) {
        item.completed = true;
      }
    });
  }

  goToNextStep(): void {
    this.stepper.selected.completed = true;
    this.stepper.next();
  }

  closeDialog(): void {
    this._dialog.close();
  }
}
