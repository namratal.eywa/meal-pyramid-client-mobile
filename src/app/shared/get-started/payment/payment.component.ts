import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Router } from '@angular/router';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ProductTypes, RouteIDs } from 'src/app/common/constants';
import { User, APIResponse } from 'src/app/common/models';
import { AuthService, SnackBarService, HttpService, SharedService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';

declare const Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  dispatchView = false;
  loading = false;
  me: User;
  gstFG!: FormGroup;
  requiresGSTFC!: FormControl;

  @Output() closeDialog = new EventEmitter();
  @Output() goToNextStep = new EventEmitter();

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<void> = new Subject();

  constructor(
    private _as: AuthService,
    private _sb: SnackBarService,
    private _hs: HttpService,
    private _router: Router,
    private _fb: FormBuilder,
    private _shs: SharedService
  ) {
    this._router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
  }

  ngOnInit(): void {
    this._me$.pipe(takeUntil(this._notifier$)).subscribe((data: User) => {
      this.me = data;

      this.requiresGSTFC = new FormControl(this.me.flags.requiresGSTInvoice);
      this.gstFG = this._fb.group({
        number: [
          this.me.personalDetails.gst.number,
          Validators.pattern(/^[A-Z0-9]{15}$/)
        ],
        name: [
          this.me.personalDetails.gst.name,
          Validators.pattern(/^[a-zA-Z. ]{2,51}$/)
        ],
        address: [this.me.personalDetails.gst.address, Validators.minLength(5)]
      });

      this.dispatchView = true;
    });
  }

  get gstNumberFC(): FormControl {
    return this.gstFG.get('number') as FormControl;
  }

  get gstNameFC(): FormControl {
    return this.gstFG.get('name') as FormControl;
  }

  get gstAddressFC(): FormControl {
    return this.gstFG.get('address') as FormControl;
  }

  async onCreatePayment(): Promise<void> {
    this.loading = true;

    const paymentObj = {
      userUID: this.me.uid,
      product: ProductTypes.ORIENTATION,
      requiresGST: this.requiresGSTFC.value,
      gstNumber: this.gstNumberFC.value,
      gstName: this.gstNameFC.value,
      gstAddress: this.gstAddressFC.value
    };

    this._hs.createPayment(paymentObj).subscribe(
      (res: APIResponse) => {
        if (!res.status) {
          this._sb.openErrorSnackBar(
            res.error.description ||
              'Sorry, the payment could not be initiated. Please try again.'
          );
          this.loading = false;
          return;
        }

        const { paymentKey } = res.data;
        const paises = res.data.order.amount;

        const razorPayOptions: any = {
          key: environment.payment.razorPay.key,
          amount: paises,
          currency: environment.payment.currency,
          name: this.me.name,
          description: ProductTypes.ORIENTATION,
          order_id: res.data.order.id,
          handler: (response) => {
            const obj = {
              userUID: this.me.uid,
              paymentKey,
              product: ProductTypes.ORIENTATION,
              pgResponse: response
            };

            this._hs.setPayment(obj).subscribe(async (res1: APIResponse) => {
              if (!res1.status) {
                this._shs.setHomeSnackBar({
                  type: 'Error',
                  message:
                    res1.error.description ||
                    'Thank you for the payment. Please contact our Support team for the next steps.'
                });
              } else {
                this._shs.setHomeSnackBar({
                  type: 'Success',
                  message: 'Thank you for the payment.'
                });
              }

              this._router.navigate([RouteIDs.HOME]);
            });
          }
        };
        const rzpInstance = new Razorpay(razorPayOptions);
        rzpInstance.open();

        rzpInstance.on('payment.failed', (response) => {
          // console.log('[error] ', response);
        });

        this.loading = false;
        this.closeSelf();
      },
      () => {
        this._sb.openErrorSnackBar(
          'Sorry, the payment could not be initiated. Please try again.'
        );
        this.loading = false;
      }
    );
  }

  closeSelf(): void {
    this.closeDialog.emit();
  }
}

// *
// * PayU
// *

// const payUObj = {
//   txnid: res.data.paymentKey,
//   hash: res.data.hash,
//   surl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   furl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   productinfo: ProductTypes.ORIENTATION,
//   amount: Orientation.price.amountWithTax.toString(),
//   phone: this.me.personalDetails.mobile,
//   email: this.me.personalDetails.email,
//   firstname: this.me.personalDetails.firstName,
//   key: environment.payU.merchantKey
// };

// bolt.launch(payUObj, {
//   responseHandler: (obj) => {
//     if (obj.response.txnStatus !== 'CANCEL') {
//       this._hs
//         .setPayment(obj.response)
//         .subscribe((res1: APIResponse) => {
//           if (!res1.status) {
//             this._sb.openErrorSnackBar(
//               res1.error.description || ErrorMessages.SYSTEM,
//               4000
//             );
//           }

//           this._router.navigate([RouteIDs.HOME]);
//         });
//     }
//   },
//   catchException: (err) => {
//     this._sb.openErrorSnackBar(
//       err.message || ErrorMessages.SYSTEM,
//       4000
//     );
//   }
// });

// const tempPaymentObj = {
//   txnid: payUObj.txnid,
//   productinfo: payUObj.productinfo
// };
// this._hs.setPayment(tempPaymentObj).subscribe((res1: APIResponse) => {
//   if (!res1.status) {
//     this._sb.openErrorSnackBar(
//       res1.error.description || ErrorMessages.SYSTEM,
//       4000
//     );
//   }

//   this._router.navigate([RouteIDs.HOME]);
// });

// this.loading = false;
// this.closeSelf();
