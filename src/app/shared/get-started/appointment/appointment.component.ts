import { Component, OnInit, Output, EventEmitter } from '@angular/core';


import { Observable, Subject, combineLatest, EMPTY } from 'rxjs';
import { takeUntil, catchError } from 'rxjs/operators';

import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ErrorMessages, SuccessMessages, RouteIDs } from 'src/app/common/constants';
import { MLocationn, User, Appointment, DialogBox } from 'src/app/common/models';
import { StaticDataService, AuthService, HttpService, SnackBarService, SharedService } from 'src/app/core/services';
import { DialogBoxComponent } from '../../dialog-box';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit {
  areSlotsAvailable = false;
  bookingAppt = false;
  dispatchView = false;
  endDate: Date;
  isSearchPristine = true;
  loadingSlots = false;
  locations: MLocationn[] = [];
  me: User;
  searchFG: FormGroup;
  selectedSlot: Appointment;
  slots: Appointment[] = [];
  startDate: Date;

  @Output() closeDialog = new EventEmitter();

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  private _locations$: Observable<MLocationn[]> = this._sds.getMLocations();
  private _h$ = combineLatest([this._me$, this._locations$]);

  constructor(
    private _sds: StaticDataService,
    private _as: AuthService,
    private _fb: FormBuilder,
    private _hs: HttpService,
    private _sb: SnackBarService,
    private _dialog: MatDialog,
    private _shs: SharedService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._h$.pipe(takeUntil(this._notifier$)).subscribe((data: any[]) => {
      [this.me, this.locations] = data;

      this.createSearchFG();
      this.startDate = moment().add(3, 'day').startOf('day').toDate();
      this.endDate = moment().add(365, 'day').startOf('day').toDate();

      this.dispatchView = true;
    });
  }

  createSearchFG(): void {
    this.searchFG = this._fb.group({
      date: ['', Validators.required],
      location: ['', Validators.required]
    });
  }

  get locationFC(): FormControl {
    return this.searchFG.get('location') as FormControl;
  }

  get dateFC(): FormControl {
    return this.searchFG.get('date') as FormControl;
  }

  isSearchFGValid(): boolean {
    return this.searchFG.valid;
  }

  resetFactorySearch(): void {
    this.searchFG.reset();
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
    this.isSearchPristine = true;
  }

  resetLightSearch(): void {
    delete this.slots;
    this.areSlotsAvailable = false;
    delete this.selectedSlot;
  }

  lockSearch(): void {
    this.loadingSlots = true;
    this.searchFG.disable();
  }

  unlockSearch(): void {
    this.loadingSlots = false;
    this.searchFG.enable();
  }

  lockBook(): void {
    this.bookingAppt = true;
    this.searchFG.disable();
  }

  unlockBook(): void {
    this.bookingAppt = false;
    this.searchFG.enable();
  }

  onSearch(): void {
    this.lockSearch();
    this.resetLightSearch();
    this.isSearchPristine = false;

    this._hs
      .getAvailableOrientationSlots(
        this.me.uid,
        moment(this.dateFC.value).format('YYYY-MM-DD'),
        this.locationFC.value.key
      )
      .pipe(
        catchError((err) => {
          this._sb.openErrorSnackBar(
            err.message || ErrorMessages.SYSTEM_TRY_AGAIN
          );
          this.resetFactorySearch();
          this.unlockSearch();
          return EMPTY;
        })
      )
      .subscribe((data: Appointment[]) => {
        this.slots = data;

        if (this.slots.length > 0) {
          this.areSlotsAvailable = true;
        }

        this.unlockSearch();
      });
  }

  onSlotClicked(appt: Appointment): void {
    appt.clicked = true;
    this.slots.map((item) => {
      if (item.key !== appt.key) {
        item.clicked = false;
      }
    });
    this.selectedSlot = appt;
  }

  async onBookAppointment(): Promise<boolean> {
    this.lockBook();

    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to book this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    return new Promise((resolve) => {
      dialogRef.afterClosed().subscribe(async (res: any) => {
        if (!res) {
          resolve(false);
          this.unlockBook();
          return;
        }

        const body = {
          appointmentKey: this.selectedSlot.key,
          appointmentMergeSlotKey: this.selectedSlot.mergeSlotKey,
          userUID: this.me.uid
        };
        this._hs.setOrientationAppointment(body).subscribe((apiRes) => {
          if (!apiRes.status) {
            this._sb.openErrorSnackBar(apiRes.error.description);
            resolve(false);
            this.unlockBook();
            return;
          }

          this._sb.openSuccessSnackBar(SuccessMessages.APPT_BOOKED);

          this.resetFactorySearch();
          this.unlockBook();
          resolve(true);
          this._router.navigate([RouteIDs.HOME]);
          this.closeSelf();
        });
      });
    });
  }

  closeSelf(): void {
    this.closeDialog.emit();
  }
}
