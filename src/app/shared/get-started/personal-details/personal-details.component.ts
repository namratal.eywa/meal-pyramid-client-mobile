import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { combineLatest, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';

import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import * as moment from 'moment';
import { PackageCategories, GenderTypes, GeneralNutritionSubCategories, PerformanceNutritionSubCategories, ErrorMessages } from 'src/app/common/constants';
import { MLocationn, User } from 'src/app/common/models';
import { AuthService, StaticDataService, UserService, SnackBarService, UtilitiesService } from 'src/app/core/services';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {
  categories: string[] = Object.values(PackageCategories);
  dispatchView = false;
  dpChosenFileSize = 0;
  dpChosenInitialFileName = '';
  dpUploadedFileSize = 0;
  dpUploadedInitialFileName = '';
  dob!: Date;
  genderTypes: typeof GenderTypes = GenderTypes;
  file: File;
  locations: MLocationn[] = [];
  maxDate!: Date;
  packageCategories: typeof PackageCategories = PackageCategories;
  me: User;
  step1FormGroup: FormGroup;
  subCategories: string[] = [];
  uploadCollectionName = 'userProfilePictureFiles';
  uploadFileNamePartial = 'MEALpyramid_DP';
  uploadFileNamePrefix = '';
  uploadDirPath = 'displayPictures';

  @Output() closeDialog = new EventEmitter();
  @Output() goToNextStep = new EventEmitter();

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  private _locations$: Observable<MLocationn[]> = this._sds.getMLocations();

  private _h$ = combineLatest([this._me$, this._locations$]);

  constructor(
    private _as: AuthService,
    private _sds: StaticDataService,
    private _fb: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this._h$.pipe(takeUntil(this._notifier$)).subscribe((data: any[]) => {
      [this.me, this.locations] = data;

      this.maxDate = moment().subtract(1, 'day').startOf('day').toDate();
      this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;

      this.createFG();
      this.setSubCategories();

      this.dispatchView = true;
    });
  }

  // *
  // * FormGroup
  // *

  createFG(): void {
    this.step1FormGroup = this._fb.group({
      name: [this.me.name],
      onboardingStep: [this.me.onboardingStep],
      dOBSearchIndex: [this.me.dOBSearchIndex],
      searchArray: [this.me.searchArray],
      personalDetails: this._fb.group(this.me.personalDetails)
    });

    this.firstNameFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z0-9]{2,51}$/)
    ]);
    this.lastNameFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z0-9]{1,51}$/)
    ]);
    this.dateOfBirthFC.setValidators(Validators.required);
    this.genderFC.setValidators(Validators.required);
    this.addressLine1.setValidators([
      Validators.required,
      Validators.minLength(5)
    ]);
    this.cityFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z ]{2,101}$/)
    ]);
    this.stateFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z ]{2,101}$/)
    ]);
    this.countryFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z ]{2,101}$/)
    ]);
    this.pincodeFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z0-9-]{3,11}$/)
    ]);
    this.mobileFC.setValidators([
      Validators.required,
      Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/)
    ]);
    this.categoryFC.setValidators(Validators.required);
    this.serviceTypeFC.setValidators(Validators.required);
    this.target.setValidators([Validators.maxLength(50)]);

    const dob = moment(this.dateOfBirthFC.value).toDate();
    this.dob = this._uts.isValidDate(dob) ? dob : null;
  }

  get firstNameFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.firstName') as FormControl;
  }

  get lastNameFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.lastName') as FormControl;
  }

  get nameFC(): FormControl {
    return this.step1FormGroup.get('name') as FormControl;
  }

  get gender(): string {
    return this.step1FormGroup.get('personalDetails.gender').value;
  }

  get genderFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.gender') as FormControl;
  }

  get dateOfBirthFC(): FormControl {
    return this.step1FormGroup.get(
      'personalDetails.dateOfBirth'
    ) as FormControl;
  }

  get dOBSearchIndex(): FormControl {
    return this.step1FormGroup.get('dOBSearchIndex') as FormControl;
  }

  get searchArrayFC(): FormControl {
    return this.step1FormGroup.get('searchArray') as FormControl;
  }

  get photoURLFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.photoURL') as FormControl;
  }

  get addressLine1(): FormControl {
    return this.step1FormGroup.get(
      'personalDetails.addressLine1'
    ) as FormControl;
  }

  get cityFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.city') as FormControl;
  }

  get stateFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.state') as FormControl;
  }

  get pincodeFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.pincode') as FormControl;
  }

  get countryFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.country') as FormControl;
  }

  get mobileFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.mobile') as FormControl;
  }

  get emailFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.email') as FormControl;
  }

  get preferredLocationFC(): FormControl {
    return this.step1FormGroup.get(
      'personalDetails.preferredLocation'
    ) as FormControl;
  }

  get categoryFC(): FormControl {
    return this.step1FormGroup.get('personalDetails.category') as FormControl;
  }

  get serviceTypeFC(): FormControl {
    return this.step1FormGroup.get(
      'personalDetails.serviceType'
    ) as FormControl;
  }

  get target(): FormControl {
    return this.step1FormGroup.get('personalDetails.target') as FormControl;
  }

  get onboardingStepFC(): FormControl {
    return this.step1FormGroup.get('onboardingStep') as FormControl;
  }

  isStep1FormValid(): boolean {
    return this.step1FormGroup.valid;
  }

  // *
  // * Date of Birth
  // *

  onDOBSet(e: MatDatepickerInputEvent<Date>): void {
    this.dateOfBirthFC.setValue(moment(e.value).format('YYYY-MM-DD'));
    this.dOBSearchIndex.setValue(moment(e.value).format('MM-DD'));
  }

  // *
  // * Sub-categories
  // *

  setSubCategories(): void {
    if (this.categoryFC.value === this.packageCategories.GENERAL) {
      this.subCategories = Object.values(GeneralNutritionSubCategories);
    } else if (this.categoryFC.value === this.packageCategories.PERFORMANCE) {
      this.subCategories = Object.values(PerformanceNutritionSubCategories);
    }
  }

  // *
  // * Preferred Location
  // *

  isPreferredLocationChecked(l: MLocationn): boolean {
    return l.key === this.preferredLocationFC.value.key;
  }

  // *
  // * Display Picture
  // *

  availableEvent(e: any): void {
    if (!e.fileSize) {
      return;
    }

    this.dpChosenInitialFileName = e.initialFileName;
    this.dpChosenFileSize = e.fileSize;
  }

  uploadEvent(e: any): void {
    if (!e.fileURL) {
      return;
    }

    this.dpUploadedInitialFileName = e.initialFileName;
    this.dpUploadedFileSize = e.fileSize;
    this.photoURLFC.setValue(e.fileURL);
  }

  // *
  // * Save
  // *

  setNameSearchArray(): void {
    const sanitizedFirstName = this._uts.sanitize(this.firstNameFC.value);
    const sanitizedLastName = this._uts.sanitize(this.lastNameFC.value);

    const fullName = `${sanitizedFirstName} ${sanitizedLastName}`;
    this.nameFC.setValue(fullName);

    const searchArray = this._uts.getSearchArray(
      sanitizedFirstName,
      sanitizedLastName
    );
    this.searchArrayFC.setValue(searchArray);
  }

  async onSubscribeLater(): Promise<void> {
    if (
      this.dpChosenFileSize !== this.dpUploadedFileSize ||
      this.dpChosenInitialFileName !== this.dpUploadedInitialFileName
    ) {
      this._sb.openErrorSnackBar(
        ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED
      );
      return;
    }

    try {
      this.setNameSearchArray();

      await this._us.setUser(this.me.uid, this.step1FormGroup.value);

      this.closeSelf();
    } catch (error) {
      console.log(error);
    }
  }

  async onNext(): Promise<void> {
    if (!this.isStep1FormValid()) {
      return;
    }

    if (
      this.dpChosenFileSize !== this.dpUploadedFileSize ||
      this.dpChosenInitialFileName !== this.dpUploadedInitialFileName
    ) {
      this._sb.openErrorSnackBar(
        ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED
      );
      return;
    }

    try {
      this.onboardingStepFC.setValue(1);
      this.setNameSearchArray();

      await this._us.setUser(this.me.uid, this.step1FormGroup.value);

      this.goToNextStep.emit();
    } catch (error) {
      console.log(error);
    }
  }

  // *
  // * Close
  // *

  closeSelf(): void {
    this.closeDialog.emit();
  }
}
