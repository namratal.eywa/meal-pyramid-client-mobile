import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import {
  FormArray,
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';

import { MatCheckboxChange } from '@angular/material/checkbox';
import { ExerciseTypes, GeneralNutritionMedicalConditions, MealCategories, WeightLossTypes, UserCategories, GenderTypes, ErrorMessages } from 'src/app/common/constants';
import { UserHealthHistory, User, Exercise } from 'src/app/common/models';
import { UserService, AuthService, SnackBarService, StaticDataService } from 'src/app/core/services';

@Component({
  selector: 'app-health-history',
  templateUrl: './health-history.component.html',
  styleUrls: ['./health-history.component.scss']
})
export class HealthHistoryComponent implements OnInit {
  dispatchView = false;
  exerciseTypes: string[] = Object.values(ExerciseTypes);
  generalNutritionMedConds: string[] = Object.values(
    GeneralNutritionMedicalConditions
  );

  initialHealthHistory: UserHealthHistory;
  initialMedicalConditions: any = {};
  initialWeightLossAttempts: any = {};
  isFemale = false;
  isNew = false;
  isSportsPerson = false;
  me: User;
  mealCategories: string[] = Object.values(MealCategories);
  otherMedCondFC: FormControl = new FormControl({ value: '', disabled: true });
  step2FormGroup: FormGroup;
  weightLossTypes: string[] = Object.values(WeightLossTypes);

  @Output() closeDialog = new EventEmitter();
  @Output() goToNextStep = new EventEmitter();

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<void> = new Subject();

  constructor(
    private _fb: FormBuilder,
    private _us: UserService,
    private _as: AuthService,
    private _sb: SnackBarService,
    private _sts: StaticDataService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;

          switch (this.me.personalDetails.category) {
            case UserCategories.GENERAL_NUTRITION:
              this.isSportsPerson = false;
              break;

            case UserCategories.SPORTS_NUTRITION:
              this.isSportsPerson = true;
              break;

            default:
              this.isSportsPerson = false;
              break;
          }

          this.isFemale = this.me.personalDetails.gender === GenderTypes.FEMALE;

          return this._us.getUserHealthHistory(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((data: UserHealthHistory[]) => {
        if (data.length === 0) {
          this.initialHealthHistory = new UserHealthHistory();
          this.initialHealthHistory.key = this._sts.getNewKey(
            'userHealthHistory',
            true,
            this.me.uid
          );
          this.initialHealthHistory.uid = this.me.uid;
          this.initialHealthHistory.membershipKey = this.me.membershipKey;
          this.initialHealthHistory.isActive = true;
          this.initialHealthHistory.recStatus = true;

          this.isNew = true;
        } else if (data.length === 1) {
          [this.initialHealthHistory] = data;

          this.isNew = false;
        }

        this.createFormGroup(this.initialHealthHistory);

        this.onOnMedicationChkChange();
        this.onIsMCycleRegularChkChange();
        this.onDoesAlcoholChkChange();
        this.onDoesSmokingChkChange();
        this.onDoesTobaccoChkChange();
        this.onDoesExerciseChkChange();
        this.onTriedLosingWeightChkChange();
        this.onOnSupplementsChkChange();
        this.onHasOtherConcernsChkChange();

        this.dispatchView = true;
      });
  }

  createFormGroup(obj: any): void {
    this.step2FormGroup = this._fb.group({});

    for (const key in obj) {
      if (!Array.isArray(obj[key])) {
        this.step2FormGroup.addControl(key, this._fb.control(obj[key]));
      }
    }

    // * Medical Conditions

    this.step2FormGroup.addControl('medicalConditions', this._fb.array([]));
    this.initialHealthHistory.medicalConditions.map((i) =>
      this.addMedicalCondition(i)
    );

    this.medicalConditionsFA.controls.map((item) => {
      this.initialMedicalConditions[item.value] = true;
    });

    // * Exercise

    this.step2FormGroup.addControl('exerciseDesc', this._fb.array([]));
    this.exerciseTypes.map((str) => {
      const e = this.initialHealthHistory.exerciseDesc.filter(
        (i) =>
          i.name === str &&
          (Boolean(i.daysPerWeek) ||
            Boolean(i.duration) ||
            Boolean(i.intensity))
      )[0];

      const exercise = new Exercise();
      exercise.name = str;
      if (e && e.name) {
        exercise.daysPerWeek = e.daysPerWeek;
        exercise.duration = e.duration;
        exercise.intensity = e.intensity;
      }
      this.addExercise(exercise);
    });

    // * Weight Loss

    this.step2FormGroup.addControl('weightLossTypes', this._fb.array([]));
    this.initialHealthHistory.weightLossTypes.map((i) =>
      this.addWeightLossType(i)
    );

    this.weightLossTypesFA.controls.map((item) => {
      this.initialWeightLossAttempts[item.value] = true;
    });

    // * Misc

    if (!this.onMedicationFC.value) {
      this.medicationDescFC.disable();
    }

    if (this.isMCycleRegularFC.value) {
      this.mCycleDescFC.disable();
    }

    if (!this.doesAlcoholFC.value) {
      this.alcoholDescFC.disable();
    }

    if (!this.doesSmokingFC.value) {
      this.smokingDescFC.disable();
    }

    if (!this.doesTobaccoFC.value) {
      this.tobaccoDescFC.disable();
    }

    if (!this.onSupplementsFC.value) {
      this.supplementsDescFC.disable();
    }

    if (!this.hasOtherConcernsFC.value) {
      this.otherConcernsDescFC.disable();
    }
  }

  // *
  // * Medical Conditions
  // *

  get medicalConditionsFA(): FormArray {
    return this.step2FormGroup.get('medicalConditions') as FormArray;
  }

  addMedicalCondition(val: string): void {
    this.medicalConditionsFA.push(this._fb.control(val));
  }

  removeMedicalCondition(val: string): void {
    let i = 0;
    let remove: number;
    this.medicalConditionsFA.controls.map((c: FormControl) => {
      if (c.value === val) {
        remove = i;
      }
      i++;
    });
    this.medicalConditionsFA.removeAt(remove);
  }

  onMedCondChkChange(e: MatCheckboxChange) {
    if (e.checked) {
      this.addMedicalCondition(e.source.value);
    } else {
      this.removeMedicalCondition(e.source.value);
    }
  }

  // * -----

  get otherMedicalConditionsFC(): FormControl {
    return this.step2FormGroup.get('otherMedicalConditions') as FormControl;
  }

  addOtherMedCond(val: string) {
    if (val) {
      this.addMedicalCondition(val);
    }
  }

  onOtherMedCondChkChange(e: MatCheckboxChange) {
    if (e.checked) {
      this.otherMedicalConditionsFC.enable();
    } else {
      this.otherMedicalConditionsFC.reset('');
      this.otherMedicalConditionsFC.disable();
    }
  }

  // *
  // * Medication
  // *

  get onMedicationFC(): FormControl {
    return this.step2FormGroup.get('onMedication') as FormControl;
  }

  get medicationDescFC(): FormControl {
    return this.step2FormGroup.get('medicationDesc') as FormControl;
  }

  onOnMedicationChkChange(): void {
    this.onMedicationFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (val) {
          this.medicationDescFC.enable();
        } else {
          this.medicationDescFC.reset('');
          this.medicationDescFC.disable();
        }
      });
  }

  // *
  // * Menstrual Cycle
  // *

  get isMCycleRegularFC(): FormControl {
    return this.step2FormGroup.get('isMCycleRegular') as FormControl;
  }

  get mCycleDescFC(): FormControl {
    return this.step2FormGroup.get('mCycleDesc') as FormControl;
  }

  onIsMCycleRegularChkChange(): void {
    this.isMCycleRegularFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (!val) {
          this.mCycleDescFC.enable();
        } else {
          this.mCycleDescFC.reset('');
          this.mCycleDescFC.disable();
        }
      });
  }

  // *
  // * Alcohol
  // *

  get doesAlcoholFC(): FormControl {
    return this.step2FormGroup.get('doesAlcohol') as FormControl;
  }

  get alcoholDescFC(): FormControl {
    return this.step2FormGroup.get('alcoholDesc') as FormControl;
  }

  onDoesAlcoholChkChange(): void {
    this.doesAlcoholFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (val) {
          this.alcoholDescFC.enable();
        } else {
          this.alcoholDescFC.reset('');
          this.alcoholDescFC.disable();
        }
      });
  }

  // *
  // * Smoking
  // *

  get doesSmokingFC(): FormControl {
    return this.step2FormGroup.get('doesSmoking') as FormControl;
  }

  get smokingDescFC(): FormControl {
    return this.step2FormGroup.get('smokingDesc') as FormControl;
  }

  onDoesSmokingChkChange(): void {
    this.doesSmokingFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (val) {
          this.smokingDescFC.enable();
        } else {
          this.smokingDescFC.reset('');
          this.smokingDescFC.disable();
        }
      });
  }

  // *
  // * Tobacco
  // *

  get doesTobaccoFC(): FormControl {
    return this.step2FormGroup.get('doesTobacco') as FormControl;
  }

  get tobaccoDescFC(): FormControl {
    return this.step2FormGroup.get('tobaccoDesc') as FormControl;
  }

  onDoesTobaccoChkChange(): void {
    this.doesTobaccoFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (val) {
          this.tobaccoDescFC.enable();
        } else {
          this.tobaccoDescFC.reset('');
          this.tobaccoDescFC.disable();
        }
      });
  }

  // *
  // * Exercise
  // *

  get doesExerciseFC(): FormControl {
    return this.step2FormGroup.get('doesExercise') as FormControl;
  }

  onDoesExerciseChkChange(): void {
    this.doesExerciseFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (!val) {
          this.exerciseDescFA.controls.map((c: FormControl) => {
            c.get('daysPerWeek').reset();
            c.get('duration').reset();
            c.get('intensity').reset('');
          });
        }
      });
  }

  // * ---

  get exerciseDescFA(): FormArray {
    return this.step2FormGroup.get('exerciseDesc') as FormArray;
  }

  addExercise(e: Exercise): void {
    const fg = this._fb.group(e);
    fg.get('daysPerWeek').setValidators([Validators.pattern(/^[1-7]$/)]);
    fg.get('duration').setValidators([
      Validators.pattern(/^([1-9][0-9][0-9]|[1-9][0-9]|[1-9])$/)
    ]);
    this.exerciseDescFA.push(fg);
  }

  // *
  // * Weight
  // *

  get triedLosingWeightFC(): FormControl {
    return this.step2FormGroup.get('triedLosingWeight') as FormControl;
  }

  onTriedLosingWeightChkChange(): void {
    this.triedLosingWeightFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (!val) {
          this.weightLossTypesFA.clear();
        }
      });
  }

  get weightLossTypesFA(): FormArray {
    return this.step2FormGroup.get('weightLossTypes') as FormArray;
  }

  addWeightLossType(val: string): void {
    this.weightLossTypesFA.push(this._fb.control(val));
  }

  removeWeightLossType(val: string): void {
    let i = 0;
    let remove: number;
    this.weightLossTypesFA.controls.map((c: FormControl) => {
      if (c.value === val) {
        remove = i;
      }
      i++;
    });
    this.weightLossTypesFA.removeAt(remove);
  }

  onWeightLossTypeChkChange(e: MatCheckboxChange): void {
    if (e.checked) {
      this.addWeightLossType(e.source.value);
    } else {
      this.removeWeightLossType(e.source.value);
    }
  }

  // *
  // * Meal
  // *

  get mealCategoryFC(): FormControl {
    return this.step2FormGroup.get('mealCategory') as FormControl;
  }

  // *
  // * Supplements
  // *

  get onSupplementsFC(): FormControl {
    return this.step2FormGroup.get('onSupplements') as FormControl;
  }

  onOnSupplementsChkChange(): void {
    this.onSupplementsFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (val) {
          this.supplementsDescFC.enable();
        } else {
          this.supplementsDescFC.reset('');
          this.supplementsDescFC.disable();
        }
      });
  }

  get supplementsDescFC(): FormControl {
    return this.step2FormGroup.get('supplementsDesc') as FormControl;
  }

  // *
  // * Other Concerns
  // *

  get hasOtherConcernsFC(): FormControl {
    return this.step2FormGroup.get('hasOtherConcerns') as FormControl;
  }

  onHasOtherConcernsChkChange(): void {
    this.hasOtherConcernsFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val: boolean) => {
        if (val) {
          this.otherConcernsDescFC.enable();
        } else {
          this.otherConcernsDescFC.reset('');
          this.otherConcernsDescFC.disable();
        }
      });
  }

  get otherConcernsDescFC(): FormControl {
    return this.step2FormGroup.get('otherConcernsDesc') as FormControl;
  }

  // *
  // *  General
  // *

  get uidFC(): FormControl {
    return this.step2FormGroup.get('uid') as FormControl;
  }

  get keyFC(): FormControl {
    return this.step2FormGroup.get('key') as FormControl;
  }

  enableAllDesc(): void {
    this.otherMedicalConditionsFC.enable();
    this.medicationDescFC.enable();
    this.mCycleDescFC.enable();
    this.alcoholDescFC.enable();
    this.smokingDescFC.enable();
    this.tobaccoDescFC.enable();
    this.supplementsDescFC.enable();
    this.otherConcernsDescFC.enable();
  }

  // *
  // *  Save
  // *

  isStep2FormValid(): boolean {
    return this.step2FormGroup.valid;
  }

  async onSubscribeLater(): Promise<void> {
    if (!this.isStep2FormValid()) {
      this._sb.openErrorSnackBar(ErrorMessages.INVALID_DATA);
      return;
    }

    try {
      this.enableAllDesc();

      await this._sts.setData(
        this.me.uid,
        'userHealthHistory',
        this.step2FormGroup.value,
        this.isNew,
        [],
        true,
        this.keyFC.value
      );

      this.closeSelf();
    } catch (error) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM_TRY_AGAIN);
    }
  }

  async onNext(): Promise<void> {
    if (!this.isStep2FormValid()) {
      this._sb.openErrorSnackBar(ErrorMessages.INVALID_DATA);
      return;
    }

    try {
      this.enableAllDesc();

      await this._sts.setData(
        this.me.uid,
        'userHealthHistory',
        this.step2FormGroup.value,
        this.isNew,
        [],
        true,
        this.keyFC.value
      );

      this.me.onboardingStep = 2;
      const obj = {
        onboardingStep: 2
      };
      await this._us.setUser(this.me.uid, obj);

      this.goToNextStep.emit();
    } catch (error) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM_TRY_AGAIN);
    }
  }

  // *
  // *  Close
  // *

  closeSelf(): void {
    this.closeDialog.emit();
  }
}
