import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GetStartedComponent } from './get-started.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { HealthHistoryComponent } from './health-history/health-history.component';
import { PaymentComponent } from './payment/payment.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { InputFilePreviewModule } from '../input-file-preview';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogBoxModule } from '../dialog-box';
import { MaterialDesignModule } from '../material-design';
import { UploadFileModule } from '../upload-file';

@NgModule({
  declarations: [
    GetStartedComponent,
    PersonalDetailsComponent,
    HealthHistoryComponent,
    PaymentComponent,
    AppointmentComponent
  ],
  imports: [
    CommonModule,
    MatStepperModule,
    FlexLayoutModule,
    MaterialDesignModule,
    ReactiveFormsModule,
    UploadFileModule,
    DialogBoxModule,
    InputFilePreviewModule
  ],
  exports: [GetStartedComponent]
})
export class GetStartedModule {}
