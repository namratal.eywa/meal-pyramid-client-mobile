import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable, Subject, EMPTY } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';

import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import { Router } from '@angular/router';
import { AdvancedTestPackages, ErrorMessages, ProductTypes, RouteIDs } from 'src/app/common/constants';
import { MATPackage, AdvancedTest, UserAdvancedTest, User, APIResponse } from 'src/app/common/models';
import { AuthService, StaticDataService, UserService, HttpService, SnackBarService, SharedService, UtilitiesService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';
declare const Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  advancedTestPackageInitial: MATPackage;
  advancedTestPackagesFC: FormControl = new FormControl();
  advancedTestPackages: MATPackage[] = MATPackage.FromData(
    AdvancedTestPackages,
    this._uts
  );

  advancedTestsFA: FormArray = new FormArray([]);
  allAdvancedTests: AdvancedTest[] = [];
  cost = 0;
  costWithTax = 0;
  currentUserAdvancedTest: UserAdvancedTest;
  dispatchView = false;
  gstFG!: FormGroup;
  initialAdvancedTests: AdvancedTest[] = [];
  isNew = false;
  loading = false;
  me: User;
  needGSTFC: FormControl;
  requiresGSTFC!: FormControl;

  @Output() closeDialog = new EventEmitter();
  @Output() goToNextStep = new EventEmitter();

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _sds: StaticDataService,
    private _fb: FormBuilder,
    private _us: UserService,
    private _hs: HttpService,
    private _sb: SnackBarService,
    private _router: Router,
    private _shs: SharedService,
    private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        takeUntil(this._notifier$),
        switchMap((me: User) => {
          this.me = me;

          this.requiresGSTFC = new FormControl(
            this.me.flags.requiresGSTInvoice
          );
          this.gstFG = this._fb.group({
            number: [
              this.me.personalDetails.gst.number,
              Validators.pattern(/^[A-Z0-9]{15}$/)
            ],
            name: [
              this.me.personalDetails.gst.name,
              Validators.pattern(/^[a-zA-Z. ]{2,51}$/)
            ],
            address: [
              this.me.personalDetails.gst.address,
              Validators.minLength(5)
            ]
          });

          return this._sds.getAllAdvancedTests();
        }),
        switchMap((tests: AdvancedTest[]) => {
          this.allAdvancedTests = tests;

          return this._us.getActiveUserAdvancedTests(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        catchError(() => EMPTY)
      )
      .subscribe((userAdvancedTests: UserAdvancedTest[]) => {
        this.cost = 0;
        this.costWithTax = 0;

        if (userAdvancedTests.length === 0) {
          this.currentUserAdvancedTest = new UserAdvancedTest();

          this.isNew = true;
        } else if (userAdvancedTests.length === 1) {
          [this.currentUserAdvancedTest] = userAdvancedTests;

          this.isNew = false;
        }

        this.setInitialAdvancedTests();
        this.setInitialAdvancedTestPackage();
        this.setAdvancedTestsFA(this.currentUserAdvancedTest.recommendedTests);

        // if (userAdvancedTests.length) {
        //   if (userAdvancedTests.length !== 1) {
        //     throw new Error();
        //   }

        //   [this.currentUserAdvancedTest] = userAdvancedTests;
        //   this.currentUserAdvancedTest.recommendedTests.map((item) => {
        //     this.addAdvancedTest(item);
        //   });
        // } else {
        //   this.currentUserAdvancedTest = new UserAdvancedTest();
        //   this.currentUserAdvancedTest.uid = this.me.uid;
        //   this.currentUserAdvancedTest.membershipKey = this.me.membershipKey;
        // }

        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  // *
  // * GST
  // *

  get gstNumberFC(): FormControl {
    return this.gstFG.get('number') as FormControl;
  }

  get gstNameFC(): FormControl {
    return this.gstFG.get('name') as FormControl;
  }

  get gstAddressFC(): FormControl {
    return this.gstFG.get('address') as FormControl;
  }

  // *
  // * Initial
  // *

  setInitialAdvancedTests(): void {
    this.initialAdvancedTests.length = 0;

    if (this.currentUserAdvancedTest.recommendedTests.length > 0) {
      this.initialAdvancedTests = this.currentUserAdvancedTest.recommendedTests;
    }
  }

  setInitialAdvancedTestPackage(): void {
    const [pkg] = this.advancedTestPackages.filter(
      (pkg) => pkg.key === this.currentUserAdvancedTest.recommendedPackage.key
    );

    if (pkg) {
      this.advancedTestPackageInitial = pkg;
      this.advancedTestPackagesFC.setValue(pkg);
    } else {
      const newPkg = new MATPackage();
      this.advancedTestPackageInitial = newPkg;
      this.advancedTestPackagesFC.setValue(newPkg);
    }
  }

  // *
  // * Individual Advanced Tests Form Array
  // *

  addAdvancedTest(t: AdvancedTest): void {
    this.advancedTestsFA.push(this._fb.group(t));

    this.cost += t.price.amount;
    this.costWithTax += t.price.amountWithTax;
  }

  removeAdvancedTest(t: AdvancedTest): void {
    let i = 0;
    let remove: number;
    this.advancedTestsFA.controls.map((test) => {
      if (test.value.name === t.name) {
        remove = i;
      }
      i++;
    });
    this.advancedTestsFA.removeAt(remove);

    this.cost -= t.price.amount;
    this.costWithTax -= t.price.amountWithTax;
  }

  onCheckChange(e: any): void {
    if (e.checked) {
      this.addAdvancedTest(e.source.value);
    } else {
      this.removeAdvancedTest(e.source.value);
    }

    const testKeys = this.advancedTestsFA.value.map((test) => test.key);
    this.advancedTestPackagesFC.reset();
    this.advancedTestPackages.map((pkg) => {
      if (pkg.testKeys.sort().join() === testKeys.sort().join()) {
        this.advancedTestPackagesFC.setValue(pkg);
      }
    });
  }

  setAdvancedTestsFA(arr: AdvancedTest[] = []): void {
    this.advancedTestsFA.clear();

    if (arr.length > 0) {
      arr.map((item) => {
        this.addAdvancedTest(item);
      });
    }
  }

  // *
  // * Advanced Tests Packages
  // *

  onPackageCheck(p: any): void {
    const tests = [];

    p.testKeys.map((k) => {
      const t = this.allAdvancedTests.filter((i) => i.key === k)[0];
      tests.push(t);
    });

    this.cost = 0;
    this.costWithTax = 0;

    this.setAdvancedTestsFA(tests);
    // this.advancedTestsFA.reset();
    // this.allAdvancedTests.map((t) => this.removeAdvancedTest(t));
    // tests.map((t) => {
    //   this.addAdvancedTest(t);
    // });
  }

  onPackageCheckChange(e: any): void {
    this.onPackageCheck(e.value);
  }

  // *
  // * Misc
  // *

  isChecked(t: AdvancedTest): boolean {
    let flag = false;
    this.advancedTestsFA.controls.map((test) => {
      if (test.value.name === t.name) {
        flag = true;
      }
    });
    return flag;
  }

  resetPackages(): void {
    this.advancedTestPackagesFC.setValue(false);

    this.cost = 0;
    this.costWithTax = 0;

    this.setAdvancedTestsFA(this.initialAdvancedTests);
    this.advancedTestPackagesFC.reset(this.advancedTestPackageInitial);

    // this.advancedTestsFA.reset();
    // this.allAdvancedTests.map((t) => this.removeAdvancedTest(t));
    // this.cost = 0;
    // this.costWithTax = 0;
    // this.advancedTestPackagesFC.setValue(false);
    // this.currentUserAdvancedTest.recommendedTests.map((item) => {
    //   this.addAdvancedTest(item);
    // });
  }

  // *
  // * Payment
  // *

  async onPayment(): Promise<void> {
    if (!this.cost) {
      return;
    }

    try {
      this.loading = true;

      if (this.isNew) {
        this.currentUserAdvancedTest.key = this._sds.getNewKey(
          'userAdvancedTests',
          true,
          this.me.uid
        );
        this.currentUserAdvancedTest.uid = this.me.uid;
        this.currentUserAdvancedTest.membershipKey = this.me.membershipKey;
        this.currentUserAdvancedTest.isActive = true;
        this.currentUserAdvancedTest.recStatus = true;
      }

      this.currentUserAdvancedTest.chosenTests = this.advancedTestsFA.value;
      this.currentUserAdvancedTest.chosenPackage =
        this.advancedTestPackagesFC.value || new MATPackage();

      await this._sds.setData(
        this.me.uid,
        'userAdvancedTests',
        this.currentUserAdvancedTest,
        this.isNew,
        [],
        true,
        this.currentUserAdvancedTest.key
      );

      // if (
      //   Boolean(this.currentUserAdvancedTest.recommendedTests.length > 0) !==
      //   this.me.flags.hasAdvancedTests
      // ) {
      //   await this._sds.setData(this.me.uid, 'users', {
      //     flags: {
      //       hasAdvancedTests: Boolean(
      //         this.currentUserAdvancedTest.recommendedTests.length > 0
      //       )
      //     }
      //   });
      // }
    } catch (error) {
      this._sb.openErrorSnackBar(error.message || ErrorMessages.SYSTEM);
    }

    // if (!this.currentUserAdvancedTest.key) {
    //   this.currentUserAdvancedTest.key = this._sds.getNewKey(
    //     'userAdvancedTests',
    //     true
    //   );
    // }
    // this.currentUserAdvancedTest.recommendedTests =
    //   this.advancedTestsFA.value;
    // await this._sds.setData(
    //   this.me.uid,
    //   'userAdvancedTests',
    //   this.currentUserAdvancedTest,
    //   false,
    //   [],
    //   true,
    //   this.currentUserAdvancedTest.key
    // );

    // await this._us.setUser(this.me.uid, {
    //   flags: {
    //     requiresGSTInvoice: this.needGSTFC.value
    //   },
    //   personalDetails: {
    //     gst: {
    //       number: this.gstNumberFC.value
    //     }
    //   }
    // });

    const paymentObj = {
      userUID: this.me.uid,
      product: ProductTypes.ADVANCED_TESTING,
      requiresGST: this.requiresGSTFC.value,
      gstNumber: this.gstNumberFC.value,
      gstName: this.gstNameFC.value,
      gstAddress: this.gstAddressFC.value
    };

    this._hs.createPayment(paymentObj).subscribe(
      (res: APIResponse) => {
        if (!res.status) {
          this._sb.openErrorSnackBar(
            res.error.description ||
              'Sorry, the payment could not be initiated. Please try again.'
          );
          this.loading = false;
          return;
        }

        const { paymentKey } = res.data;
        const paises = res.data.order.amount;

        const razorPayOptions: any = {
          key: environment.payment.razorPay.key,
          amount: paises,
          currency: environment.payment.currency,
          name: this.me.name,
          description: ProductTypes.ADVANCED_TESTING,
          order_id: res.data.order.id,
          handler: (response) => {
            const obj = {
              userUID: this.me.uid,
              paymentKey,
              product: ProductTypes.ADVANCED_TESTING,
              pgResponse: response
            };

            this._hs.setPayment(obj).subscribe(async (res1: APIResponse) => {
              if (!res1.status) {
                this._shs.setHomeSnackBar({
                  type: 'Error',
                  message:
                    res1.error.description ||
                    'Thank you for the payment. Please contact our Support team for the next steps.'
                });
              } else {
                this._shs.setHomeSnackBar({
                  type: 'Success',
                  message: 'Thank you for the payment.'
                });
              }

              this._router.navigate([RouteIDs.HOME]);
            });
          }
        };
        const rzpInstance = new Razorpay(razorPayOptions);
        rzpInstance.open();

        rzpInstance.on('payment.failed', (response) => {
          // console.log('[error] ', response);
        });

        this.loading = false;
        this.closeSelf();
      },
      () => {
        this._sb.openErrorSnackBar(
          'Sorry, the payment could not be initiated. Please try again.'
        );
        this.loading = false;
      }
    );
  }

  closeSelf(): void {
    this.closeDialog.emit();
  }
}

// *
// * PayU
// *

// const payUObj = {
//   txnid: res.data.paymentKey,
//   hash: res.data.hash,
//   surl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   furl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   productinfo: ProductTypes.ADVANCED_TESTING,
//   amount: this.costWithTax.toString(),
//   phone: this.me.personalDetails.mobile,
//   email: this.me.personalDetails.email,
//   firstname: this.me.personalDetails.firstName,
//   key: environment.payU.merchantKey
// };

// bolt.launch(payUObj, {
//   responseHandler: (obj) => {
//     if (obj.response.txnStatus !== 'CANCEL') {
//       this._hs
//         .setPayment(obj.response)
//         .subscribe(async (res1: APIResponse) => {
//           if (!res1.status) {
//             this._sb.openErrorSnackBar(
//               res1.error.description || ErrorMessages.SYSTEM,
//               4000
//             );
//           }

//           const obj = {
//             buyAdvancedTestingStep: 1,
//             flags: {
//               requiresGSTInvoice: this.needGSTFC.value
//             },
//             personalDetails: {
//               gstNumber: this.gstNumberFC.value
//             }
//           };
//           await this._us.setUser(this.me.uid, obj);

//           if (
//             this.currentUserAdvancedTest &&
//             this.currentUserAdvancedTest.uid
//           ) {
//             this.currentUserAdvancedTest.purchasedTests =
//               this.advancedTestsFA.value;
//             this.currentUserAdvancedTest.paymentKey =
//               this.paymentKey;
//             this.currentUserAdvancedTest.isLocked = true;

//             await this._us.setUserAdvancedTest(
//               this.currentUserAdvancedTest,
//               false
//             );
//           } else {
//             const test = new UserAdvancedTest();
//             test.purchasedTests = this.advancedTestsFA.value;
//             test.uid = this.me.uid;
//             test.paymentKey = this.paymentKey;
//             test.isLocked = true;

//             await this._us.setUserAdvancedTest(test, true);
//           }

//           // this.goToNextStep.emit();
//           this._router.navigate([RouteIDs.HOME]);
//         });
//     }
//   },
//   catchException: (err) => {
//     this._sb.openErrorSnackBar(
//       err.message || ErrorMessages.SYSTEM,
//       4000
//     );
//   }
// });

// this.loading = false;
// this.closeSelf();
