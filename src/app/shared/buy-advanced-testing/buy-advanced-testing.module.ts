import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatStepperModule } from '@angular/material/stepper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BuyAdvancedTestingComponent } from './buy-advanced-testing.component';
import { PaymentComponent } from './payment/payment.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogBoxModule } from '../dialog-box';
import { MaterialDesignModule } from '../material-design';

@NgModule({
  declarations: [
    BuyAdvancedTestingComponent,
    PaymentComponent,
    AppointmentComponent
  ],
  imports: [
    CommonModule,
    MaterialDesignModule,
    MatStepperModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    DialogBoxModule
  ],
  exports: [BuyAdvancedTestingComponent]
})
export class BuyAdvancedTestingModule {}
