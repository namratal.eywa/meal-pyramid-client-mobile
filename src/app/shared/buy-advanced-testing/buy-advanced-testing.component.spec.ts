import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyAdvancedTestingComponent } from './buy-advanced-testing.component';

describe('BuyAdvancedTestingComponent', () => {
  let component: BuyAdvancedTestingComponent;
  let fixture: ComponentFixture<BuyAdvancedTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyAdvancedTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyAdvancedTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
