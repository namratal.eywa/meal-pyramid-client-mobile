import {
  Component,
  OnInit,
  Inject,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-buy-advanced-testing',
  templateUrl: './buy-advanced-testing.component.html',
  styleUrls: ['./buy-advanced-testing.component.scss']
})
export class BuyAdvancedTestingComponent implements OnInit, AfterViewInit {
  @ViewChild('stepper') stepper: MatStepper;

  constructor(
    private _dialog: MatDialogRef<BuyAdvancedTestingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { step: number }
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.stepper.steps.forEach((item, i) => {
      item.completed = false;

      if (i < this.data.step) {
        item.completed = true;
      }
    });
  }

  goToNextStep(): void {
    this.stepper.selected.completed = true;
    this.stepper.next();
  }

  closeDialog(): void {
    this._dialog.close();
  }
}
