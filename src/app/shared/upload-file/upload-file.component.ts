import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { Filee2 } from 'src/app/common/models';
import { StaticDataService, SnackBarService, StorageService } from 'src/app/core/services';


@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit, OnChanges {
  deleting = false;
  fileKey = '';
  filePath = '';
  fileURL = '';
  uploading = false;
  uploadPercentage$: Observable<number>;
  uploadState$: Observable<any>;

  @Input() file: File;
  @Input() fileName = '';
  @Input() isVisible = false;
  @Input() uploadByName = '';
  @Input() uploadByUID = '';
  @Input() uploadCollectionName = '';
  @Input() uploadDir = '';
  @Input() userMembershipKey = '';
  @Input() userName = '';
  @Input() userUID = '';
  @Output() onUploadComplete = new EventEmitter<any>();
  @Output() onDelete = new EventEmitter<void>();

  constructor(
    private _sds: StaticDataService,
    private _sb: SnackBarService,
    private _sts: StorageService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.reset();
  }


  reset(): void {
    this.fileKey = '';
    this.filePath = '';
    this.fileURL = '';
    delete this.uploadPercentage$;
    delete this.uploadState$;
  }
}
