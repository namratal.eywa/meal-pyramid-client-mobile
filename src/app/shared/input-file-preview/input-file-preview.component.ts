import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Filee2 } from 'src/app/common/models';
import { StorageService, StaticDataService } from 'src/app/core/services';

@Component({
  selector: 'app-input-file-preview',
  templateUrl: './input-file-preview.component.html',
  styleUrls: ['./input-file-preview.component.scss']
})
export class InputFilePreviewComponent implements OnInit {
  acceptedFileTypes = '.jpg, .jpeg, .png';
  files: File[] = [];
  imgURL = '';
  isUploaded = false;
  uploading = false;

  @Input() disabled = false;
  @Input() isReset = false;
  @Input() uploadByName = '';
  @Input() uploadByUID = '';
  @Input() uploadCollectionName = '';
  @Input() uploadDirPath = '';
  @Input() uploadFileNamePrefix = '';
  @Input() userMembershipKey = '';
  @Input() userName = '';
  @Input() userUID = '';
  @Output() availableEvent = new EventEmitter<any>();
  @Output() uploadEvent = new EventEmitter<any>();

  constructor(private _sts: StorageService, private _sds: StaticDataService) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    if (this.isReset) {
      this.reset();
    }
  }

  reset(): void {
    this.files.length = 0;
    this.imgURL = '';
    this.isUploaded = false;
    this.isReset = false;
  }

  onFileAvailable(e: any) {
    if (this.uploading) {
      return;
    }

    this.reset();

    if (!e || !e.target || !e.target.files) {
      return;
    }

    const fileList: File[] = e.target.files;
    if (fileList.length !== 1) {
      return;
    }
    this.files.push(fileList[0]);

    const reader = new FileReader();

    reader.onload = (event: any) => {
      this.imgURL = event.target.result;

      this.availableEvent.emit({
        fileSize: this.files[0].size,
        initialFileName: this.files[0].name
      });
    };

    reader.onerror = (event: any) => {
      console.log(`CANNOT_READ_FILE: ${event.target.error.code}`);
    };

    reader.readAsDataURL(this.files[0]);
  }

  onFileUpload(): void {
    if (!this.files.length || !this.imgURL) {
      return;
    }

    this.uploading = true;

    const { fName, fPath, uploadRef, snapshotChanges$ } = this._sts.upload(
      this.files[0],
      this.uploadDirPath,
      this.uploadFileNamePrefix,
      this.userUID
    );

    snapshotChanges$
      .pipe(
        finalize(async () => {
          const fKey = this.userUID
            ? this._sds.getNewKey(this.uploadCollectionName, true, this.userUID)
            : this._sds.getNewKey(this.uploadCollectionName);
          const fURL = await uploadRef.getDownloadURL().toPromise();

          const fileObj = new Filee2();
          fileObj.key = fKey;
          fileObj.fileName = fName;
          fileObj.filePath = fPath;
          fileObj.fileURL = fURL;
          fileObj.fileType = this.files[0].type;
          fileObj.fileSize = this.files[0].size;
          fileObj.uploadedBy.uid = this.uploadByUID;
          fileObj.uploadedBy.name = this.uploadByName;
          fileObj.uploadedAt = new Date();
          fileObj.isActive = true;
          fileObj.recStatus = true;

          if (this.userUID) {
            fileObj.uid = this.userUID;
            fileObj.membershipKey = this.userMembershipKey;

            await this._sds.setData(
              this.userUID,
              this.uploadCollectionName,
              fileObj,
              true,
              ['uploadedAt'],
              true,
              fKey
            );
          } else {
            await this._sds.setData(
              fKey,
              this.uploadCollectionName,
              fileObj,
              true,
              ['uploadedAt']
            );
          }

          this.uploadEvent.emit({
            fileKey: fKey,
            fileName: fName,
            fileType: this.files[0].type,
            filePath: fPath,
            fileURL: fURL,
            fileSize: this.files[0].size,
            initialFileName: this.files[0].name
          });

          this.uploading = false;
          this.isUploaded = true;
        })
      )
      .subscribe();
  }
}
