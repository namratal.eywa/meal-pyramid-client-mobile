import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InputFilePreviewComponent } from './input-file-preview.component';
import { SharedComponentsModule } from '../shared-components';

@NgModule({
  declarations: [InputFilePreviewComponent],
  imports: [CommonModule, SharedComponentsModule, FlexLayoutModule],
  exports: [InputFilePreviewComponent]
})
export class InputFilePreviewModule {}
