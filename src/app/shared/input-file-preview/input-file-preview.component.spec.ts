import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFilePreviewComponent } from './input-file-preview.component';

describe('InputFilePreviewComponent', () => {
  let component: InputFilePreviewComponent;
  let fixture: ComponentFixture<InputFilePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputFilePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputFilePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
