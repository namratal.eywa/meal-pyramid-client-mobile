import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RouteIDs, ErrorMessages } from 'src/app/common/constants';
import { User } from 'src/app/common/models';
import { AuthService, SnackBarService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  chatURL = '';
  dispatchView = false;
  isPackageSubscribed = false;
  loading = false;
  me: User;
  path: string;
  routeIDs: typeof RouteIDs = RouteIDs;
  subscribedPackage: any;

  private _notifier$: Subject<void> = new Subject();
  private _me$: Observable<User> = this._as.getMe();

  constructor(
    private _router: Router,
    private _as: AuthService,
    private _sb: SnackBarService
  ) {}

  ngOnInit(): void {
    this.path = this.getSubPath(this._router.url);
    this.chatURL = environment.support.chatURL;

    this._me$.pipe(takeUntil(this._notifier$)).subscribe((user: User) => {
      this.me = user;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;

      this.dispatchView = true;
    });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  getSubPath(data): string {
    return data.split('/')[1];
  }

  goto(path): void {
    this.path = path;
    this._router.navigate([path]);
  }

  async onLogout(): Promise<void> {
    try {
      this.loading = true;

      setTimeout(async () => {
        await this._as.logout();
        this.loading = false;
        this._router.navigate([RouteIDs.LOGIN]);
      }, 750);
    } catch (error) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
    }
  }
}
