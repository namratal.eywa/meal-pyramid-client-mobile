import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';

import { LayoutComponent } from './layout.component';
import { MaterialDesignModule } from '../material-design';
import { SharedComponentsModule } from '../shared-components';

@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    MaterialDesignModule,
    FlexLayoutModule,
    SharedComponentsModule
  ],
  exports: [LayoutComponent]
})
export class LayoutModule {}
