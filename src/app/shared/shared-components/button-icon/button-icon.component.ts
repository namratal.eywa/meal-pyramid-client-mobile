import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input
} from '@angular/core';

@Component({
  selector: 'app-button-icon',
  templateUrl: './button-icon.component.html',
  styleUrls: ['./button-icon.component.scss']
})
export class ButtonIconComponent implements OnInit, OnChanges {
  iconCSS: any;
  showSpinner = false;

  @Input() disabled = false;
  @Input() icon = '';
  @Input() iconStyle = '';
  @Input() loading = false;
  @Input() toolTip = '';
  @Output() onButtonClick = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.showSpinner = this.loading;

    this.iconCSS = {
      [this.iconStyle || 'e-mat-icon--3']: true
    };
  }

  onClick($event): void {
    if (this.disabled) {
      return;
    }

    this.onButtonClick.emit($event);
  }
}
