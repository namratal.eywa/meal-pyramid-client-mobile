import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { RouteIDs } from 'src/app/common/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit , OnDestroy {

  @Input() public title: string = "";
  @Input() public backButtonURL: string = "/tab/home";
  show_footer: boolean=true;
  subcribption: any;
  
  constructor(private _router: Router) { }


  ngOnInit() {
    this.subcribption=this._router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          if (this._router.url === "/tab/home") {
            this.show_footer = false;
          }
          else{
            this.show_footer = true;
          }
        }
      })
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subcribption.unsubscribe();
  }

}
