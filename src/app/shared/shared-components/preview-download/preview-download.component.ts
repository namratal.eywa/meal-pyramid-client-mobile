import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';

import { finalize } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { Filee } from 'src/app/common/models';
import { StorageService, StaticDataService, SnackBarService } from 'src/app/core/services';


@Component({
  selector: 'app-preview-download',
  templateUrl: './preview-download.component.html',
  styleUrls: ['./preview-download.component.scss']
})
export class PreviewDownloadComponent implements OnInit, OnChanges {
  acceptedFileTypes = '';
  dispatchFileView = false;
  deleting = false;
  downloading = false;
  isFileAvailable = false;
  uploading = false;

  @Input() allowDelete = false;
  @Input() allowReplace = false;
  @Input() allowUpload = false;
  @Input() downloadFileName = '';
  @Input() fileKey = '';
  @Input() fileName = '';
  @Input() filePath = '';
  @Input() fileURL = '';
  @Input() isImg = false;
  @Input() isImgAllowed = false;
  @Input() placeholder = '';
  @Input() uploadByName = '';
  @Input() uploadByUID = '';
  @Input() uploadCollectionName = '';
  @Input() uploadDir = '';
  @Input() userMembershipKey = '';
  @Input() userName = '';
  @Input() userUID = '';
  @Input() title = '';
  @Output() onUploadComplete = new EventEmitter<any>();
  @Output() onDelete = new EventEmitter<any>();

  constructor(
    private _sts: StorageService,
    private _sds: StaticDataService,
    private _sb: SnackBarService
  ) {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.isFileAvailable = Boolean(this.fileURL);
    this.acceptedFileTypes = this.isImgAllowed
      ? '.jpg, .jpeg, .png, .pdf'
      : '.pdf';

    setTimeout(() => {
      this.dispatchFileView = true;
    }, 250);
  }

  onDownloadFile(): void {
    this.downloading = true;
    this._sts.download(this.fileURL, this.downloadFileName);

    setTimeout(() => {
      this.downloading = false;
    }, 2000);
  }

  async onUploadFile(e: any, replaceFlag = false): Promise<void> {
    if (!e || !e.target || !e.target.files) {
      return;
    }

    const fileList = e.target.files;
    if (fileList.length < 1) {
      return;
    }

    if (replaceFlag) {
      await this.onDeleteFile(true);
    }

    const file = fileList.item(0);
    this.isImg = !file.type.includes('pdf');
    // this.startUpload(file);
  }

  // startUpload(file: File): void {
  //   if (!file) {
  //     return;
  //   }

  //   this.uploading = true;

  //   const { fName, fPath, uploadRef, snapshotChanges$ } = this._sts.upload(
  //     file,
  //     this.uploadDir,
  //     this.fileName,
  //     this.userUID
  //   );

  //   snapshotChanges$
  //     .pipe(
  //       finalize(async () => {
  //         const fKey = this.userUID
  //           ? this._sds.getNewKey(this.uploadCollectionName, true, this.userUID)
  //           : this._sds.getNewKey(this.uploadCollectionName);
  //         const fURL = await uploadRef.getDownloadURL().toPromise();

  //         const fileObj = new Filee();
  //         fileObj.key = fKey;
  //         fileObj.fileName = fName;
  //         fileObj.filePath = fPath;
  //         fileObj.fileURL = fURL;
  //         fileObj.fileType = file.type;
  //         fileObj.isActive = true;
  //         fileObj.membershipKey = this.userMembershipKey;
  //         fileObj.recStatus = true;
  //         fileObj.uploadedAt = new Date();
  //         fileObj.uploadedBy.uid = this.uploadByUID;
  //         fileObj.uploadedBy.name = this.uploadByName;
  //         fileObj.user.uid = this.userUID;
  //         fileObj.user.name = this.userName;

  //         if (this.userUID) {
  //           await this._sds.setData(
  //             this.userUID,
  //             this.uploadCollectionName,
  //             fileObj,
  //             true,
  //             ['uploadedAt'],
  //             true,
  //             fKey
  //           );
  //         } else {
  //           await this._sds.setData(
  //             fKey,
  //             this.uploadCollectionName,
  //             fileObj,
  //             true,
  //             ['uploadedAt']
  //           );
  //         }

  //         this.fileKey = fKey;
  //         this.fileName = this.fileName ? this.fileName : fName;
  //         this.filePath = fPath;
  //         this.fileURL = fURL;
  //         this.isFileAvailable = true;

  //         this.onUploadComplete.emit({
  //           fileKey: fKey,
  //           fileName: fName,
  //           filePath: fPath,
  //           fileURL: fURL
  //         });

  //         this.uploading = false;
  //       })
  //     )
  //     .subscribe();
  // }

  async onDeleteFile(replaceFlag = false): Promise<void> {
    try {
      this.deleting = true;

      if (this.userUID) {
        await this._sds.setData(
          this.userUID,
          this.uploadCollectionName,
          {
            recStatus: false
          },
          false,
          ['uploadedAt'],
          true,
          this.fileKey
        );
      } else {
        await this._sds.setData(
          this.fileKey,
          this.uploadCollectionName,
          {
            recStatus: false
          },
          false,
          ['uploadedAt']
        );
      }

      this._sts.delete(this.filePath).then().catch();
      if (!replaceFlag) {
        this.reset();
      }

      this.onDelete.emit();
      this.deleting = false;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
      this.deleting = false;
    }
  }

  reset(): void {
    this.fileKey = '';
    this.fileName = '';
    this.filePath = '';
    this.fileURL = '';
    this.isFileAvailable = false;
  }
}
