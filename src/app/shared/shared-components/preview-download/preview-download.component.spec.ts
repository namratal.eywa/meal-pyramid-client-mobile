import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewDownloadComponent } from './preview-download.component';

describe('PreviewDownloadComponent', () => {
  let component: PreviewDownloadComponent;
  let fixture: ComponentFixture<PreviewDownloadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewDownloadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewDownloadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
