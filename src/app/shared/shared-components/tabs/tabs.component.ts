import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subject, Observable, EMPTY } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { RouteIDs } from 'src/app/common/constants';
import { User } from 'src/app/common/models';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  chatURL = '';
  dispatchView = false;
  isPackageSubscribed = false;
  loading = false;
  me: User;
  path: string;
  routeIDs: typeof RouteIDs = RouteIDs;
  subscribedPackage: any;
  closed$ = new Subject<any>();
  showTabs = true; // <-- show tabs by default

  private _notifier$: Subject<void> = new Subject();
  private _me$: Observable<User> = this._as.getMe();
  constructor(   private _router: Router, private navCtrl: NavController,  private _as: AuthService) { }

  ngOnInit() {
    this._me$.pipe(takeUntil(this._notifier$)).subscribe((user: User) => {
      this.me = user;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;
      this.dispatchView = true;
    });

  }

  goto(path): void {
    this.path = path;
    this._router.navigate([path]);
    console.log(this.path)
  }
  ngOnDestroy() {
    this.closed$.next(); // <-- close subscription when component is destroyed
  }

}
