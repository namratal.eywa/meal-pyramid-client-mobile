import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd, Router ,UrlSegment  } from '@angular/router';
import { MenuController, AlertController } from '@ionic/angular';
import { Observable, Subject, combineLatest, async, EMPTY } from 'rxjs';
import { catchError, switchMap, takeUntil, filter } from 'rxjs/operators';
import { ErrorMessages, RouteIDs } from 'src/app/common/constants';
import { DialogBox, Session, User, UserAppointment } from 'src/app/common/models';
import { AuthService, SharedService, SnackBarService, HttpService, SessionnService, UserService, UtilitiesService } from 'src/app/core/services';
import { DialogBoxComponent } from '../../dialog-box/dialog-box.component';
@Component({
  selector: 'app-profile-avatar',
  templateUrl: './profile-avatar.component.html',
  styleUrls: ['./profile-avatar.component.scss'],
})
export class ProfileAvatarComponent implements OnInit {
  me: User;
  showHasRecommendedAdvancedTests: boolean;
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  userHasSubcribedPackage: boolean;
  path: any;
  routeIDs: typeof RouteIDs = RouteIDs;
  hideNotificationIcon: boolean=false;
  subscription: any;
  dispatchView = false;
  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    private _shs: SharedService,
    private _sb: SnackBarService,
    private _hs: HttpService,
    private _router: Router,
    private _ss: SessionnService,
    private _us: UserService,
    private _uts: UtilitiesService,
    public menuCtrl: MenuController,
    private alertController: AlertController,
    private _activatedRoute: ActivatedRoute
  ) {
    this.menuCtrl.swipeGesture(true);
  }

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.dispatchView=true;
          console.log(this.me);
          this.userHasSubcribedPackage = this.me.flags.isPackageSubscribed;
          return combineLatest([
            this._us.getActiveUserAppointments(
              this.me.uid,
              this.me.membershipKey
            ),
            this._ss.getSessionsByUser(this.me.uid, this.me.membershipKey)
          ]);
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe(async (arr: [UserAppointment[], Session[]]) => { });
    //   this.subscription= this._router.events.subscribe(
    //   (event: any) => {
    //     if (event instanceof NavigationEnd) {
    //       console.log('this.router.url', this._router.url);
    //       if(this._router.url==="/p/notification")
    //       this.hideNotificationIcon=true;
    //       console.log("sbjsbjb")
    //     }
    //     else
    //     {
    //       this.hideNotificationIcon=false;
    //     }
    //   }
    // );  
    // console.log(this.hideNotificationIcon)
  
  }

  goto(path): void {
    this.path = path;
    this._router.navigate([path]);
  }

  onLogout() {
    // const dialogBox = new DialogBox();
    // dialogBox.actionFalseBtnTxt = 'No';
    // dialogBox.actionTrueBtnTxt = 'Yes';
    // dialogBox.icon = 'logout';
    // dialogBox.title = 'Logout';
    // dialogBox.message = '  Are you sure you want to log out ? ';
    // dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    // dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    // this._shs.setDialogBox(dialogBox);
    // const dialogRef = this._dialog.open(DialogBoxComponent);
    // dialogRef.afterClosed().subscribe(async (res: any) => {
    //   if (!res) {
    //     return;
    //   }
    //   try {
    //     setTimeout(async () => {
    //                     await this._as.logout();
    //                     this._router.navigate([RouteIDs.LOGIN]);
    //                   }, 750);
    //   } catch (err) {
    //     this._sb.openErrorSnackBar(ErrorMessages.DELETE_FAILED);
    //   }
    // });

  }


}
