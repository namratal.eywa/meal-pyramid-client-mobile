import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendedPackageComponent } from './recommended-package.component';

describe('RecommendedPackageComponent', () => {
  let component: RecommendedPackageComponent;
  let fixture: ComponentFixture<RecommendedPackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecommendedPackageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendedPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
