import { Component, OnDestroy, OnInit } from '@angular/core';

import { EMPTY, Observable, Subject } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UserCategories, ErrorMessages, ProductTypes, RouteIDs } from 'src/app/common/constants';
import { Package, User, MPackage, APIResponse } from 'src/app/common/models';
import { AuthService, HttpService, SharedService, SnackBarService, StaticDataService } from 'src/app/core/services';

declare const Razorpay: any;

@Component({
  selector: 'app-recommended-package',
  templateUrl: './recommended-package.component.html',
  styleUrls: ['./recommended-package.component.scss']
})
export class RecommendedPackageComponent implements OnInit, OnDestroy {
  allPackages: Package[] = [];
  defaultSelection: number;
  dispatchView = false;
  loading = false;
  generalNumberOfSessionsArr = [6, 10, 20, 40];
  gstFG!: FormGroup;
  gstMultiplier = environment.gst.multiplier;
  me: User;
  numberOfSessionsArr: any[] = [];
  numberOfSessionsFC: FormControl = new FormControl('');
  recommendedPackage: MPackage;
  relevantPackages: Package[] = [];
  requiresGSTFC!: FormControl;
  shownPackages: Package[] = [];
  sportsNumberOfSessionsArr = [10, 20];

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _sts: StaticDataService,
    private _as: AuthService,
    private _sb: SnackBarService,
    private _hs: HttpService,
    private _dialog: MatDialogRef<RecommendedPackageComponent>,
    private _router: Router,
    private _fb: FormBuilder,
    private _shs: SharedService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.recommendedPackage = this.me.recommendedPackage;

          this.requiresGSTFC = new FormControl(
            this.me.flags.requiresGSTInvoice
          );
          this.gstFG = this._fb.group({
            number: [
              this.me.personalDetails.gst.number,
              Validators.pattern(/^[A-Z0-9]{15}$/)
            ],
            name: [
              this.me.personalDetails.gst.name,
              Validators.pattern(/^[a-zA-Z. ]{2,51}$/)
            ],
            address: [
              this.me.personalDetails.gst.address,
              Validators.minLength(5)
            ]
          });

          this.numberOfSessionsArr =
            this.me.personalDetails.category ===
            UserCategories.GENERAL_NUTRITION
              ? this.generalNumberOfSessionsArr
              : this.sportsNumberOfSessionsArr;

          this.defaultSelection = this.numberOfSessionsArr.indexOf(
            this.recommendedPackage.numberOfSessions
          );
          this.numberOfSessionsFC.setValue(
            this.recommendedPackage.numberOfSessions
          );

          return this._sts.getPackages();
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((data: Package[]) => {
        this.allPackages = data;
        if (this.me.personalDetails.category) {
          this.relevantPackages = this.allPackages.filter(
            (item) => item.category === this.me.personalDetails.category
          );
        } else {
          this.relevantPackages = this.allPackages;
        }

        this.onNumberOfSessionsSelect(
          this.me.recommendedPackage.numberOfSessions
        );

        this.numberOfSessionsArr = this.allPackages.map((i) => {
          if (i.category === this.me.personalDetails.category) {
            return {
              name: i.name,
              sessions: i.numberOfSessions
            };
          }
          return '';
        });
        this.numberOfSessionsArr = this.numberOfSessionsArr
          .filter((i) => i.name)
          .sort((a, b) => a.sessions - b.sessions);
        this.numberOfSessionsArr = [
          ...new Map(
            this.numberOfSessionsArr.map((item) => [item.name, item])
          ).values()
        ];

        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  get gstNumberFC(): FormControl {
    return this.gstFG.get('number') as FormControl;
  }

  get gstNameFC(): FormControl {
    return this.gstFG.get('name') as FormControl;
  }

  get gstAddressFC(): FormControl {
    return this.gstFG.get('address') as FormControl;
  }

  onNumberOfSessionsSelect(n: number) {
    this.shownPackages.length = 0;
    this.shownPackages = this.relevantPackages.filter(
      (i) => i.numberOfSessions === n
    );
  }

  async onCreatePayment(pkg: Package): Promise<void> {
    this.loading = true;
    pkg.clicked = true;

    const paymentObj = {
      userUID: this.me.uid,
      name: this.me.name,
      product: ProductTypes.SUBSCRIPTION,
      packageKey: pkg.key,
      requiresGST: this.requiresGSTFC.value,
      gstNumber: this.gstNumberFC.value,
      gstName: this.gstNameFC.value,
      gstAddress: this.gstAddressFC.value
    };

    this._hs.createPayment(paymentObj).subscribe(
      (res: APIResponse) => {
        if (!res.status) {
          this._sb.openErrorSnackBar(
            res.error.description ||
              'Sorry, the payment could not be initiated. Please try again.'
          );
          this.loading = false;
          pkg.clicked = false;
          return;
        }

        const { paymentKey } = res.data;
        const paises = res.data.order.amount;

        const razorPayOptions: any = {
          key: environment.payment.razorPay.key,
          amount: paises,
          currency: environment.payment.currency,
          name: this.me.name,
          description: ProductTypes.SUBSCRIPTION,
          order_id: res.data.order.id,
          handler: (response) => {
            const obj = {
              userUID: this.me.uid,
              paymentKey,
              product: ProductTypes.SUBSCRIPTION,
              packageKey: pkg.key,
              pgResponse: response
            };

            this._hs.setPayment(obj).subscribe(async (res1: APIResponse) => {
              if (!res1.status) {
                this._shs.setHomeSnackBar({
                  type: 'Error',
                  message:
                    res1.error.description ||
                    'Thank you for the payment. Please contact our Support team for the next steps.'
                });
              } else {
                this._shs.setHomeSnackBar({
                  type: 'Success',
                  message: 'Thank you for the payment.'
                });
              }

              this._router.navigate([RouteIDs.HOME]);
            });
          }
        };
        const rzpInstance = new Razorpay(razorPayOptions);
        rzpInstance.open();

        rzpInstance.on('payment.failed', (response) => {
          // console.log('[error] ', response);
        });

        this.loading = false;
        pkg.clicked = false;
        this.closeSelf();
      },
      () => {
        this._sb.openErrorSnackBar(
          'Sorry, the payment could not be initiated. Please try again.'
        );
        this.loading = false;
        pkg.clicked = false;
      }
    );
  }

  closeSelf(): void {
    this._dialog.close();
  }
}

// *
// * PayU
// *

// const payUObj = {
//   txnid: res.data.paymentKey,
//   hash: res.data.hash,
//   surl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   furl: `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
//   productinfo: ProductTypes.SUBSCRIPTION,
//   amount: pkg.price.amountWithTax.toString(),
//   phone: this.me.personalDetails.mobile,
//   email: this.me.personalDetails.email,
//   firstname: this.me.personalDetails.firstName,
//   key: environment.payU.merchantKey
// };

// bolt.launch(payUObj, {
//   responseHandler: (obj) => {
//     if (obj.response.txnStatus !== 'CANCEL') {
//       this._hs
//         .setPayment(obj.response)
//         .subscribe(async (res1: APIResponse) => {
//           if (!res1.status) {
//             this._sb.openErrorSnackBar(
//               res1.error.description || ErrorMessages.SYSTEM
//             );
//           }

//           await this._us.setUser(this.me.uid, {
//             flags: {
//               requiresGSTInvoice: this.needGSTFC.value
//             },
//             personalDetails: {
//               gstNumber: this.gstNumberFC.value
//             }
//           });

//           this.closeSelf();
//           this._router.navigate([RouteIDs.HOME]);
//         });
//     }
//   },
//   catchException: (err) => {
//     this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
//   }
// });

// this.loading = false;
// this.closeSelf();
