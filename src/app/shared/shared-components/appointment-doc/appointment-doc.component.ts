import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { AppointmentStatuses, AppointmentTypes } from 'src/app/common/constants';
import { Appointment } from 'src/app/common/models';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-appointment-doc',
  templateUrl: './appointment-doc.component.html',
  styleUrls: ['./appointment-doc.component.scss']
})
export class AppointmentDocComponent implements OnInit, OnChanges {
  appointmentStatuses: typeof AppointmentStatuses = AppointmentStatuses;
  appointmentTypes: typeof AppointmentTypes = AppointmentTypes;
  nutritionistStr = '';
  photoURL = '';
  showCancel = false;

  @Input() appointment: Appointment;
  @Output() onCancel = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.appointment) {
      this.showCancel = this.appointment.sessionStart > new Date();
      this.nutritionistStr = `uid_${this.appointment.staff.uid}`;
      this.photoURL = environment.staff[this.nutritionistStr]
        ? environment.staff[this.nutritionistStr].photoURL
        : '';
    }
  }

  onCancelClick(): void {
    this.onCancel.emit(this.appointment);
  }
}
