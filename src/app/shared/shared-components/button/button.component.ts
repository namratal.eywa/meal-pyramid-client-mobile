// Verified -
/**
 * * Width is needed for the spinner to center-align while loading.
 * * Height is needed in Users App, although not needed for the same component in Staff App.
 */

import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, OnChanges {
  buttonCSS: any;
  showSpinner = false;

  @Input() buttonStyle = '';
  @Input() disabled = false;
  @Input() loading = false;
  @Input() title = '';
  @Output() onButtonClick = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.showSpinner = this.loading;

    this.buttonCSS = {
      'e-mat-button eh-px-12': true,
      [this.buttonStyle]: true,
      'eh-csr-dsb': this.disabled
    };
  }

  onClick($event): void {
    if (this.disabled) {
      return;
    }

    this.onButtonClick.emit($event);
  }
}
