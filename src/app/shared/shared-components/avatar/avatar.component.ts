import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserRoles } from 'src/app/common/constants';
import { User } from 'src/app/common/models';
import { UserService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit, OnChanges {
  avatarSize = '';
  photoURL = '';
  user: User;

  @Input() border = false;
  @Input() userUID = '';
  @Input() size = '';
  @Input() zIndex = 0;

  private _notifier$ = new Subject<any>();

  constructor(private _us: UserService) {}

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.userUID) {
      this._us
        .getPersonByUID(this.userUID)
        .pipe(takeUntil(this._notifier$))
        .subscribe((user: User) => {
          this.user = user;

          const staffStr = `uid_${this.user.uid}`;

          if (this.user.personalDetails.photoURL) {
            this.photoURL = this.user.personalDetails.photoURL;
          } else if (!this.user.roles.includes(UserRoles.USER)) {
            this.photoURL = environment.staff[staffStr]
              ? environment.staff[staffStr].photoURL
              : '';
          } else if (this.user.personalDetails.gender === 'Male') {
            this.photoURL = environment.avatar.userMale;
          } else if (this.user.personalDetails.gender === 'Female') {
            this.photoURL = environment.avatar.userFemale;
          }
        });
    }

    if (this.size) {
      this.avatarSize = `e-avatar--${this.size}`;
    }
  }
}
