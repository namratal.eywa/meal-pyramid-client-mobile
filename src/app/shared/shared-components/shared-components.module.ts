import { HeaderComponent } from './header/header.component';
import { ProfileAvatarComponent } from './profile-avatar/profile-avatar.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PreviewDownloadComponent } from './preview-download';
import { AppointmentDocComponent } from './appointment-doc/appointment-doc.component';
import { AvatarComponent } from './avatar/avatar.component';
import { RecommendedPackageComponent } from './recommended-package/recommended-package.component';
import { ButtonComponent } from './button/button.component';
import { ButtonIconComponent } from './button-icon/button-icon.component';
import { ButtonIconTitleComponent } from './button-icon-title/button-icon-title.component';
import { MaterialDesignModule } from '../material-design';
import { TabsComponent } from './tabs/tabs.component';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppointmentDocComponent,
    PreviewDownloadComponent,
    AvatarComponent,
    RecommendedPackageComponent,
    ButtonComponent,
    ButtonIconComponent,
    ButtonIconTitleComponent,
    TabsComponent,
    BookAppointmentComponent,
    ProfileAvatarComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatTooltipModule,
    MatIconModule,
    MatButtonModule,
    PdfViewerModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatCheckboxModule,
    MaterialDesignModule ,
    IonicModule
  ],
  exports: [
    AppointmentDocComponent,
    PreviewDownloadComponent,
    AvatarComponent,
    ButtonComponent,
    ButtonIconComponent,
    ButtonIconTitleComponent,
    TabsComponent,
    BookAppointmentComponent,
    ProfileAvatarComponent,
    HeaderComponent,
    FooterComponent
  ]
})
export class SharedComponentsModule {}
