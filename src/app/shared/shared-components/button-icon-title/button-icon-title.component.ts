import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  Input
} from '@angular/core';

@Component({
  selector: 'app-button-icon-title',
  templateUrl: './button-icon-title.component.html',
  styleUrls: ['./button-icon-title.component.scss']
})
export class ButtonIconTitleComponent implements OnInit, OnChanges {
  buttonCSS: any;
  iconCSS: any;
  showSpinner = false;
  titleCSS: any;

  @Input() buttonStyle = '';
  @Input() disabled = false;
  @Input() icon = '';
  @Input() iconStyle = '';
  @Input() loading = false;
  @Input() title = '';
  @Input() titleStyle = '';
  @Input() toolTip = '';
  @Output() onButtonClick = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(): void {
    this.showSpinner = this.loading;

    this.buttonCSS = {
      ...(this.buttonStyle && { [this.buttonStyle]: true }),
      'eh-csr-dsb': this.disabled
    };

    this.iconCSS = {
      [this.iconStyle || 'e-mat-icon--3']: true
    };

    this.titleCSS = {
      [this.titleStyle]: true
    };
  }

  onClick($event): void {
    if (this.disabled) {
      return;
    }

    this.onButtonClick.emit($event);
  }
}
