import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonIconTitleComponent } from './button-icon-title.component';

describe('ButtonIconTitleComponent', () => {
  let component: ButtonIconTitleComponent;
  let fixture: ComponentFixture<ButtonIconTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonIconTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonIconTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
