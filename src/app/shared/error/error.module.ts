import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialDesignModule } from '@shared/material-design';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ErrorRoutingModule } from './error-routing.module';
import { ErrorComponent } from './error.component';

@NgModule({
  declarations: [ErrorComponent],
  imports: [
    CommonModule,
    ErrorRoutingModule,
    MaterialDesignModule,
    FlexLayoutModule
  ]
})
export class ErrorModule {}
