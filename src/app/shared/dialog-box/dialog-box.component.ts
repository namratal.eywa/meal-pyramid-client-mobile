import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DialogBox } from 'src/app/common/models';
import { SharedService } from 'src/app/core/services';

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent implements OnInit, OnDestroy {
  private _notifier$: Subject<any> = new Subject();

  dialog: DialogBox;

  constructor(private _ss: SharedService) {}

  ngOnInit(): void {
    this._ss.dialogBox$
      .pipe(takeUntil(this._notifier$))
      .subscribe((data: DialogBox) => {
        this.dialog = data;
      });
  }

  ngOnDestroy(): void {
    this.dialog.reset();
    this._ss.setDialogBox(this.dialog);
    this._notifier$.next(true);
    this._notifier$.complete();
  }
}
