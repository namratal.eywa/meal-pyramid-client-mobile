import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressWeightComponent } from './progress-weight.component';

describe('ProgressWeightComponent', () => {
  let component: ProgressWeightComponent;
  let fixture: ComponentFixture<ProgressWeightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressWeightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressWeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
