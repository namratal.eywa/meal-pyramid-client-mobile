import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { EMPTY, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { ErrorMessages, SuccessMessages } from 'src/app/common/constants';
import { User, UserWeightLog } from 'src/app/common/models';
import { UtilitiesService, UserService, SnackBarService, AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-progress-weight',
  templateUrl: './progress-weight.component.html',
  styleUrls: ['./progress-weight.component.scss']
})
export class ProgressWeightComponent implements OnInit {
  delta = '';
  dispatchView = false;
  hasChanged = false;
  latestWeight: number = null;
  loggedDates: Date[] = [];
  logsFA: FormArray = new FormArray([]);
  maxDate!: Date;
  me!: User;
  progressFG!: FormGroup;
  progressFGInitial!: any;
  startWeight: number = null;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _fb: FormBuilder,
    private _uts: UtilitiesService,
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          this.initProgressFG();
          this.trackProgressFG();

          return this._us.getUserWeightLogs(
            this.me.uid,
            this.me.allMembershipKeys
          );
        }),
        catchError((err) => {
          this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((logs: UserWeightLog[]) => {
        this.setInitialLogsFA(logs);
        this.trackLatestLog();

        this.dispatchView = true;
      });

    this.maxDate = moment().endOf('day').toDate();
  }

  // *
  // * Summary
  // *

  trackLatestLog(): void {
    this._us
      .getUserWeightLogLatest(this.me.uid, this.me.membershipKey)
      .pipe(
        catchError((err) => {
          this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((logs: UserWeightLog[]) => {
        this.latestWeight = logs.length > 0 ? logs[0].weight : null;

        this.computeDelta();
      });
  }

  computeDelta(): void {
    this.delta = this._uts.computeDifference(
      this.latestWeight,
      this.startWeight
    );
  }

  // *
  // * Progress
  // *

  initProgressFG(): void {
    this.progressFG = this._fb.group({
      startDate: [this.me.weightProgress.startDate],
      startWeight: [
        this.me.weightProgress.startWeight,
        [
          Validators.pattern(
            /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/
          )
        ]
      ],
      targetDate: [this.me.weightProgress.targetDate],
      targetWeight: [
        this.me.weightProgress.targetWeight,
        [
          Validators.pattern(
            /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/
          )
        ]
      ]
    });

    this.progressFGInitial = this.progressFG.value;
    this.startWeight = this.startWeightFC.value;
  }

  get startWeightFC(): FormControl {
    return this.progressFG.get('startWeight') as FormControl;
  }

  get startDateFC(): FormControl {
    return this.progressFG.get('startDate') as FormControl;
  }

  get targetWeightFC(): FormControl {
    return this.progressFG.get('targetWeight') as FormControl;
  }

  get targetDateFC(): FormControl {
    return this.progressFG.get('targetDate') as FormControl;
  }

  trackProgressFG(): void {
    this.progressFG.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        this.hasChanged = !this._uts.shallowEqual(this.progressFGInitial, val);
      });
  }

  onReset(): void {
    if (!this.hasChanged) {
      return;
    }

    this.progressFG.reset(this.progressFGInitial);
    this.progressFG.markAsPristine();
  }

  onSave(): void {
    if (
      !this.hasChanged ||
      !this.progressFG.valid ||
      (this.startDateFC.value &&
        !this._uts.isValidDate(this.startDateFC.value)) ||
      (this.targetDateFC.value &&
        !this._uts.isValidDate(this.targetDateFC.value))
    ) {
      return;
    }

    this._us.setUser(this.me.uid, {
      weightProgress: this.progressFG.value
    });
    this.hasChanged = false;
    this.progressFGInitial = this.progressFG.value;
    this.startWeight = this.startWeightFC.value;
    this.computeDelta();

  }

  // *
  // * Weight Log - Date
  // *

  onDailyDateSelection(event: MatDatepickerInputEvent<Date>): void {
    const date = event.value;
    this.addNewLog(date);
  }

  // Needs to be an arrow function as per Angular Material since it's a property
  dateFilter = (date: Date): boolean => {
    const res = this.loggedDates.filter((i) => i.valueOf() === Date.valueOf());

    return res.length === 0;
  };

  // *
  // * Weight Log - FormArray
  // *

  setInitialLogsFA(logs: UserWeightLog[]) {
    this.logsFA.clear();
    logs.map((log: UserWeightLog) => {
      this.addExistingLog(log);
    });
  }

  addExistingLog(log: UserWeightLog): void {
    const fg = this._fb.group({ ...log });
    fg.get('weight').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/
      )
    ]);
    this.logsFA.push(fg);

    this.loggedDates.push(log.date);
  }

  addNewLog(date: Date): void {
    const log = new UserWeightLog();
    log.date = date;
    log.uid = this.me.uid;
    log.membershipKey = this.me.membershipKey;
    log.isActive = true;
    log.recStatus = true;

    const fg = this._fb.group({ ...log });
    fg.get('weight').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,3}(\.[0-9]{1,2})?$)/
      )
    ]);
    this.logsFA.insert(0, fg);

    this.loggedDates.push(date);
  }

  removeLog(e: { date: Date }, i: number) {
    this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
    this.logsFA.removeAt(i);
  }
}
