// ToDo:
// Firestore issue with userMeasurements collection due to bi-directional indices.
// This has caused some glitch in the collection and giving erratic behavior.
// Thus, Summary section is incomplete.

import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import * as moment from 'moment';
import { Observable, Subject, EMPTY } from 'rxjs';
import { switchMap, catchError, takeUntil } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { UserMeasurementLog, User } from 'src/app/common/models';
import { UtilitiesService, UserService, SnackBarService, AuthService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-progress-measurement',
  templateUrl: './progress-measurement.component.html',
  styleUrls: ['./progress-measurement.component.scss']
})
export class ProgressMeasurementComponent implements OnInit {
  delta = '';
  dispatchView = false;
  hasChanged = false;
  imgURL = environment.bodyMeasurementGuide.imgURL;
  latestMeasurement: number = null;
  loggedDates: Date[] = [];
  logs: UserMeasurementLog[] = [];
  logsFA: FormArray = new FormArray([]);
  maxDate!: Date;
  me!: User;
  startMeasurement: number = null;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _fb: FormBuilder,
    private _uts: UtilitiesService,
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          return this._us.getUserMeasurementLogs(
            this.me.uid,
            this.me.allMembershipKeys
          );
        }),
        catchError((err) => {
          this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((logs: UserMeasurementLog[]) => {
        this.logs = logs;
        this.setInitialLogsFA(logs);

        if (logs.length > 0) {
          this.setStartMeasurement();
        }
        this.trackSummaryLogs();

        this.dispatchView = true;
      });

    this.maxDate = moment().endOf('day').toDate();
  }

  // *
  // * Summary
  // *

  trackSummaryLogs(): void {
    this._us
      .getUserMeasurementLogLatest(this.me.uid, this.me.membershipKey)
      .pipe(
        catchError((err) => {
          this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((arr) => {
        if (arr.length > 0) {
          this.setLatestMeasurement(arr[0]);
        }

        this.computeDelta();
      });
  }

  setStartMeasurement(): void {
    this.startMeasurement =
      this.logs[this.logs.length - 1].abs +
      this.logs[this.logs.length - 1].chest +
      this.logs[this.logs.length - 1].hips +
      this.logs[this.logs.length - 1].midArm +
      this.logs[this.logs.length - 1].thighs;
  }

  setLatestMeasurement(log: UserMeasurementLog): void {
    this.latestMeasurement =
      log.abs + log.chest + log.hips + log.midArm + log.thighs;
  }

  computeDelta(): void {
    this.delta = this._uts.computeDifference(
      this.latestMeasurement,
      this.startMeasurement
    );
  }

  // *
  // * Measurement Log - Date
  // *

  onDailyDateSelection(event: MatDatepickerInputEvent<Date>): void {
    const date = event.value;
    this.addNewLog(date);
  }

  // Needs to be an arrow function as per Angular Material since it's a property
  dateFilter = (date: Date): boolean => {
    const res = this.loggedDates.filter((i) => i.valueOf() === date.valueOf());

    return res.length === 0;
  };

  // *
  // * Measurement Log - FormArray
  // *

  setInitialLogsFA(logs: UserMeasurementLog[]) {
    this.logsFA.clear();
    logs.map((log: UserMeasurementLog) => {
      this.addExistingLog(log);
    });
  }

  addExistingLog(log: UserMeasurementLog): void {
    const fg = this._fb.group({ ...log });
    fg.get('abs').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('chest').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('hips').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('midArm').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('thighs').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    this.logsFA.push(fg);

    this.loggedDates.push(log.date);
  }

  addNewLog(date: Date): void {
    const log = new UserMeasurementLog();
    log.date = date;
    log.uid = this.me.uid;
    log.membershipKey = this.me.membershipKey;
    log.isActive = true;
    log.recStatus = true;

    const fg = this._fb.group({ ...log });
    fg.get('abs').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('chest').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('hips').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('midArm').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);
    fg.get('thighs').setValidators([
      Validators.required,
      Validators.pattern(
        /(^0(\.[0-9]{1,2})?$)|(^(?!(0))[0-9]{1,2}(\.[0-9]{1,2})?$)/
      )
    ]);

    this.logsFA.insert(0, fg);

    this.loggedDates.push(date);
  }

  removeLog(e: { date: Date }, i: number) {
    this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
    this.logsFA.removeAt(i);
    this.logs.splice(i, 1);
    if (this.logs.length > 0) {
      this.setStartMeasurement();
    }
  }
}
