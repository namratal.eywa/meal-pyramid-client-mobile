import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressFoodLogComponent } from './progress-food-log.component';

describe('ProgressFoodLogComponent', () => {
  let component: ProgressFoodLogComponent;
  let fixture: ComponentFixture<ProgressFoodLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressFoodLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressFoodLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
