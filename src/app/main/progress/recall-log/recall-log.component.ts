import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Subject, Observable, EMPTY } from 'rxjs';
import { takeUntil, catchError, switchMap } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { User } from 'src/app/common/models/user.model';
import { UserService, SnackBarService, UtilitiesService, AuthService, StaticDataService } from 'src/app/core/services';


export interface Units {
  name: string;
}

@Component({
  selector: 'app-recall-log',
  templateUrl: './recall-log.component.html',
  styleUrls: ['./recall-log.component.scss']
})
export class RecallLogComponent implements OnInit {
  dispatchView = false;
  heightUnits: string[] = ['cm', 'in'];
  loading = false;
  me: User;
  recallFormGroup: FormGroup;
  recallFormGroupInitial: any;
  recallLog: any;
  weightUnits: string[] = ['kg', 'lb'];

  @Input('recallLog') recallLogInitial: any;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _formBuilder: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _uts: UtilitiesService,
    private _as: AuthService,
    private _sts: StaticDataService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;
          return this._us.getUserRecallLog(
            this.me.uid,
            this.recallLogInitial.key
          );
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((res) => {
        this.recallLog = res;

        this.recallFormGroup = this._formBuilder.group({
          isHydrationFollowed: [this.recallLog.isHydrationFollowed || false],
          hydrationIntake: [this.recallLog.hydrationIntake || ''],
          isOutsideMeal: [this.recallLog.isOutsideMeal || false],
          outsideMealsCount: [this.recallLog.outsideMealsCount || ''],
          isMealSkipped: [this.recallLog.isMealSkipped || false],
          skippedMealsCount: [this.recallLog.skippedMealsCount || ''],
          isRoutineChanged: [this.recallLog.isRoutineChanged || false],
          routineChangeDetails: [this.recallLog.routineChangeDetails || ''],
          isHungryBetweenMeals: [this.recallLog.isHungryBetweenMeals || false],
          isAlcoholConsumed: [this.recallLog.isAlcoholConsumed || false],
          alcoholConsumedDetails: [this.recallLog.alcoholConsumedDetails || ''],
          overallFeedback: [this.recallLog.overallFeedback || ''],
          hadCompetition: [this.recallLog.hadCompetition || false],
          competitionPerformance: [this.recallLog.competitionPerformance || ''],
          competitionRecovery: [this.recallLog.competitionRecovery || ''],
          upcomingCompetition: [this.recallLog.upcomingCompetition || false],
          upcomingCompetitionDetails: [
            this.recallLog.upcomingCompetitionDetails || ''
          ]
        });

        this.setInputFields();
        this.recallFormGroupInitial = this.recallFormGroup.getRawValue();

        this.dispatchView = true;

        this.observeRadioButtons();
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  get isOutsideMealFC(): FormControl {
    return this.recallFormGroup.get('isOutsideMeal') as FormControl;
  }

  get outsideMealsCountFC(): FormControl {
    return this.recallFormGroup.get('outsideMealsCount') as FormControl;
  }

  get isMealSkippedFC(): FormControl {
    return this.recallFormGroup.get('isMealSkipped') as FormControl;
  }

  get skippedMealsCountFC(): FormControl {
    return this.recallFormGroup.get('skippedMealsCount') as FormControl;
  }

  get isRoutineChangedFC(): FormControl {
    return this.recallFormGroup.get('isRoutineChanged') as FormControl;
  }

  get routineChangeDetailsFC(): FormControl {
    return this.recallFormGroup.get('routineChangeDetails') as FormControl;
  }

  get isHungryBetweenMealsFC(): FormControl {
    return this.recallFormGroup.get('isHungryBetweenMeals') as FormControl;
  }

  get isAlcoholConsumedFC(): FormControl {
    return this.recallFormGroup.get('isAlcoholConsumed') as FormControl;
  }

  get alcoholConsumedDetailsFC(): FormControl {
    return this.recallFormGroup.get('alcoholConsumedDetails') as FormControl;
  }

  get isHydrationFollowedFC(): FormControl {
    return this.recallFormGroup.get('isHydrationFollowed') as FormControl;
  }

  get hydrationIntakeFC(): FormControl {
    return this.recallFormGroup.get('hydrationIntake') as FormControl;
  }

  get hadCompetitionFC(): FormControl {
    return this.recallFormGroup.get('hadCompetition') as FormControl;
  }

  get competitionPerformanceFC(): FormControl {
    return this.recallFormGroup.get('competitionPerformance') as FormControl;
  }

  get competitionRecoveryFC(): FormControl {
    return this.recallFormGroup.get('competitionRecovery') as FormControl;
  }

  get upcomingCompetitionFC(): FormControl {
    return this.recallFormGroup.get('upcomingCompetition') as FormControl;
  }

  get upcomingCompetitionDetailsFC(): FormControl {
    return this.recallFormGroup.get(
      'upcomingCompetitionDetails'
    ) as FormControl;
  }

  setInputFields(): void {
    if (!this.isOutsideMealFC.value) {
      this.outsideMealsCountFC.disable();
    }

    if (!this.isMealSkippedFC.value) {
      this.skippedMealsCountFC.disable();
    }

    if (!this.isRoutineChangedFC.value) {
      this.routineChangeDetailsFC.disable();
    }

    if (!this.isAlcoholConsumedFC.value) {
      this.alcoholConsumedDetailsFC.disable();
    }

    if (!this.isHydrationFollowedFC.value) {
      this.hydrationIntakeFC.disable();
    }

    if (!this.hadCompetitionFC.value) {
      this.competitionPerformanceFC.disable();
      this.competitionRecoveryFC.disable();
    }

    if (!this.upcomingCompetitionFC.value) {
      this.upcomingCompetitionDetailsFC.disable();
    }
  }

  observeRadioButtons(): void {
    this.isOutsideMealFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.outsideMealsCountFC.enable();
        } else {
          this.outsideMealsCountFC.reset('');
          this.outsideMealsCountFC.disable();
        }
      });

    this.isMealSkippedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.skippedMealsCountFC.enable();
        } else {
          this.skippedMealsCountFC.reset('');
          this.skippedMealsCountFC.disable();
        }
      });

    this.isRoutineChangedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.routineChangeDetailsFC.enable();
        } else {
          this.routineChangeDetailsFC.reset('');
          this.routineChangeDetailsFC.disable();
        }
      });

    this.isAlcoholConsumedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.alcoholConsumedDetailsFC.enable();
        } else {
          this.alcoholConsumedDetailsFC.reset('');
          this.alcoholConsumedDetailsFC.disable();
        }
      });

    this.isHydrationFollowedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.hydrationIntakeFC.enable();
        } else {
          this.hydrationIntakeFC.reset('');
          this.hydrationIntakeFC.disable();
        }
      });

    this.hadCompetitionFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.competitionPerformanceFC.enable();
          this.competitionRecoveryFC.enable();
        } else {
          this.competitionPerformanceFC.reset('');
          this.competitionPerformanceFC.disable();
          this.competitionRecoveryFC.reset('');
          this.competitionRecoveryFC.disable();
        }
      });

    this.upcomingCompetitionFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.upcomingCompetitionDetailsFC.enable();
        } else {
          this.upcomingCompetitionDetailsFC.reset('');
          this.upcomingCompetitionDetailsFC.disable();
        }
      });
  }

  onReset(): void {
    this.recallFormGroup.reset(this.recallFormGroupInitial);
    this.recallFormGroup.markAsPristine();
  }

  hasRecallFormGroupChanged(): boolean {
    return !this._uts.shallowEqual(
      this.recallFormGroup.getRawValue(),
      this.recallFormGroupInitial
    );
  }

  async onSave(): Promise<void> {
    if (!this.hasRecallFormGroupChanged()) {
      return;
    }

    this.loading = true;

    try {
      const obj = {
        ...this.recallFormGroup.getRawValue(),
        key: this.recallLog.key,
        uid: this.me.uid,
        membershipKey: this.me.membershipKey
      };
      await this._sts.setData(
        this.me.uid,
        'userProgressRecall',
        obj,
        false,
        [],
        true,
        this.recallLog.key
      );
      this.recallLogInitial.isNew = false;
      this.loading = false;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
    }
  }
}
