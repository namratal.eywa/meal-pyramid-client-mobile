import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecallLogComponent } from './recall-log.component';

describe('RecallLogComponent', () => {
  let component: RecallLogComponent;
  let fixture: ComponentFixture<RecallLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecallLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecallLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
