import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressPhotoLogComponent } from './progress-photo-log.component';

describe('ProgressPhotoLogComponent', () => {
  let component: ProgressPhotoLogComponent;
  let fixture: ComponentFixture<ProgressPhotoLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressPhotoLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressPhotoLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
