import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { Subject, EMPTY } from 'rxjs';
import { takeUntil, catchError } from 'rxjs/operators';

import { Lightbox, LightboxConfig } from 'ngx-lightbox';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogBox, MFilee2, User } from 'src/app/common/models';
import { ErrorMessages, LightboxConfigs } from 'src/app/common/constants';
import { SnackBarService, StorageService, GeneralService, AuthService, UtilitiesService, SharedService } from 'src/app/core/services';
import { DialogBoxComponent } from 'src/app/shared/dialog-box';


@Component({
  selector: 'app-progress-photo-log',
  templateUrl: './progress-photo-log.component.html',
  styleUrls: ['./progress-photo-log.component.scss']
})
export class ProgressPhotoLogComponent implements OnInit, OnDestroy {
  acceptedFileTypes = '.png, .jpeg, .jpg';
  backImagesArr: MFilee2[] = [];
  chosenUser: User;
  deleting = false;
  deletingFile = false;
  dispatchView = false;
  frontalImagesArr: any[] = [];
  isNew = false;
  lateralImagesArr: MFilee2[] = [];
  loading = false;
  lightboxAlbum = [];
  me!: User;
  uploadCollectionName = 'userPhotoLogFiles';
  uploadDirPath = 'photoLogs';
  uploadFileNamePartial = 'MEALpyramid_PL';
  uploadFileNamePrefix = '';
  uploadingBack = false;
  uploadingFrontal = false;
  uploadingLateral = false;

  @Input() logFG: FormGroup;
  @Output() deleteEvent = new EventEmitter();

  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _sb: SnackBarService,
    private _lightbox: Lightbox,
    private _lightboxConfig: LightboxConfig,
    private _sts: StorageService,
    private _gs: GeneralService,
    private _as: AuthService,
    private _uts: UtilitiesService,
    private _fb: FormBuilder,
    private _shs: SharedService,
    private _dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isNew = !this.keyFC.value;

    this._as
      .getMe()
      .pipe(
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((user: User) => {
        this.me = user;

        this.uploadFileNamePrefix = `${this.uploadFileNamePartial}`;

        this.dispatchView = true;
      });

    if (this.frontalImagesFA.controls.length > 0) {
      this.frontalImagesArr = this.frontalImagesFA.value;
    }

    if (this.lateralImagesFA.controls.length > 0) {
      this.lateralImagesArr = this.lateralImagesFA.value;
    }

    if (this.backImagesFA.controls.length > 0) {
      this.backImagesArr = this.backImagesFA.value;
    }

    this.lightboxInit();
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  get frontalImagesFA(): FormArray {
    return this.logFG.get('frontalImages') as FormArray;
  }

  get lateralImagesFA(): FormArray {
    return this.logFG.get('lateralImages') as FormArray;
  }

  get backImagesFA(): FormArray {
    return this.logFG.get('backImages') as FormArray;
  }

  get dateFC(): FormControl {
    return this.logFG.get('date') as FormControl;
  }

  get keyFC(): FormControl {
    return this.logFG.get('key') as FormControl;
  }

  get uidFC(): FormControl {
    return this.logFG.get('uid') as FormControl;
  }

  get membershipKeyFC(): FormControl {
    return this.logFG.get('membershipKey') as FormControl;
  }

  async delete(): Promise<void> {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to delete?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    dialogRef.afterClosed().subscribe(async (res: any) => {
      if (!res) {
        return;
      }
      try {
        this.deleting = true;

        if (!this.isNew) {
          await this._gs.setData(
            this.uidFC.value,
            'userPhotoLogs',
            { recStatus: false },
            this.isNew,
            ['date'],
            true,
            this.keyFC.value
          );
        }

        // ToDo: Delete all images

        this.deleteEvent.emit({ date: this.dateFC.value });
      } catch (err) {
        this._sb.openErrorSnackBar(ErrorMessages.DELETE_FAILED);
        this.deleting = false;
      }
    });
  }

  // *
  // * Images
  // *

  isAnyLoaderActive(): boolean {
    return (
      this.deleting ||
      this.uploadingFrontal ||
      this.uploadingLateral ||
      this.uploadingBack ||
      this.deletingFile
    );
  }

  onAvailable(type: string): void {
    switch (type) {
      case 'Frontal':
        this.uploadingFrontal = true;
        break;

      case 'Lateral':
        this.uploadingLateral = true;
        break;

      case 'Back':
        this.uploadingBack = true;
        break;

      default:
        break;
    }
  }

  async onUploadComplete(
    obj: {
      fileKey: string;
      fileName: string;
      fileType: string;
      filePath: string;
      fileURL: string;
    },
    type: string
  ): Promise<void> {
    const mFileObj = new MFilee2(obj, this._uts);
    switch (type) {
      case 'Frontal':
        this.frontalImagesArr.push(mFileObj);
        this.frontalImagesFA.push(this._fb.control(mFileObj));
        break;

      case 'Lateral':
        this.lateralImagesArr.push(mFileObj);
        this.lateralImagesFA.push(this._fb.control(mFileObj));
        break;

      case 'Back':
        this.backImagesArr.push(mFileObj);
        this.backImagesFA.push(this._fb.control(mFileObj));
        break;

      default:
        break;
    }

    try {
      if (this.isNew) {
        const key = this._gs.getNewKey('userPhotoLogs', true, this.uidFC.value);
        this.keyFC.setValue(key);
      }

      await this._gs.setData(
        this.uidFC.value,
        'userPhotoLogs',
        this.logFG.value,
        this.isNew,
        ['date'],
        true,
        this.keyFC.value
      );
      this.isNew = false;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.FILE_NOT_UPLOADED);
    }

    switch (type) {
      case 'Frontal':
        this.uploadingFrontal = false;
        break;

      case 'Lateral':
        this.uploadingLateral = false;
        break;

      case 'Back':
        this.uploadingBack = false;
        break;

      default:
        break;
    }
  }

  async deleteFile(type: string, i: number): Promise<void> {
    this.deletingFile = true;

    let fileObj: MFilee2;
    try {
      switch (type) {
        case 'Frontal':
          fileObj = this.frontalImagesFA.at(i).value;
          this.frontalImagesFA.removeAt(i);
          break;

        case 'Lateral':
          fileObj = this.lateralImagesFA.at(i).value;
          this.lateralImagesFA.removeAt(i);
          break;

        case 'Back':
          fileObj = this.backImagesFA.at(i).value;
          this.backImagesFA.removeAt(i);
          break;

        default:
          break;
      }

      await this._gs.setData(
        this.uidFC.value,
        'userPhotoLogs',
        this.logFG.value,
        false,
        ['date'],
        true,
        this.keyFC.value
      );

      // ToDo: Mark recStatus = false in 'photoLogFiles' collection

      this._sts.delete(fileObj.filePath).then().catch();

      this.deletingFile = false;
    } catch (error) {
      this._sb.openErrorSnackBar(ErrorMessages.CANNOT_DELETE_FILE);
      this.deletingFile = false;
    }
  }

  // *
  // * Lightbox
  // *

  lightboxInit(): void {
    Object.assign(this._lightboxConfig, LightboxConfigs);
  }

  openLightbox(src: string): void {
    this.lightboxAlbum.length = 0;
    this.lightboxAlbum.push({
      src
    });
    this._lightbox.open(this.lightboxAlbum, 0);
  }
}
