import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Subject, Observable, EMPTY } from 'rxjs';
import { takeUntil, catchError, switchMap } from 'rxjs/operators';


import * as moment from 'moment';
import { ErrorMessages } from 'src/app/common/constants';
import { UserService, SnackBarService, AuthService, UtilitiesService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';
import { User, UserMeasurementLog } from 'src/app/common/models';

@Component({
  selector: 'app-measumement-log',
  templateUrl: './measumement-log.component.html',
  styleUrls: ['./measumement-log.component.scss']
})
export class MeasumementLogComponent implements OnInit {
  dispatchView = false;
  firstMeasurement = 0;
  hasMeasurementFGChanged = false;
  imgURL = environment.bodyMeasurementGuide.imgURL;
  latestMeasurement = 0;
  loading = false;
  maxDate: Date;
  me: User;
  measurementFG: FormGroup;
  measurementFGInitial: any;
  measurementLogs: UserMeasurementLog[];
  sanitizedMeasurementLogs: UserMeasurementLog[] = [];
  userMeasurementLog: UserMeasurementLog;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _formBuilder: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService,
    private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this.maxDate = moment().endOf('day').toDate();

    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;
          return this._us.getUserMeasurementLogs(this.me.uid);
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((res) => {
        if (!res || res.length === 0) {
          this.userMeasurementLog = new UserMeasurementLog();
        } else {
          [this.userMeasurementLog] = res;
        }
        this.measurementLogs = this.userMeasurementLog.logs;
        this.initFG(this.userMeasurementLog);

        this.measurementFG.valueChanges.subscribe((val) => {
          this.hasMeasurementFGChanged = !this._uts.deepEqual(
            {
              ...this.measurementFG
            },
            val
          );
        });

        this.dispatchView = true;
      });
  }

  initFG(measurementLog: any): void {
    this.measurementFG = this._formBuilder.group({});
    for (const key in this.userMeasurementLog) {
      if (Array.isArray(this.userMeasurementLog[key])) {
        this.measurementFG.addControl(key, this._formBuilder.array([]));
      } else {
        this.measurementFG.addControl(
          key,
          this._formBuilder.control(this.userMeasurementLog[key])
        );
      }
    }

    if (measurementLog && measurementLog.logs && measurementLog.logs.length) {
      this.sanitizedMeasurementLogs = measurementLog.logs
        .filter((i) => i.date)
        .sort((a, b) => a.date - b.date);
      this.addMeasurementLogs();

      this.firstMeasurement =
        this.sanitizedMeasurementLogs[0].abs +
        this.sanitizedMeasurementLogs[0].chest +
        this.sanitizedMeasurementLogs[0].hips +
        this.sanitizedMeasurementLogs[0].midArm +
        this.sanitizedMeasurementLogs[0].thighs;

      this.latestMeasurement =
        this.sanitizedMeasurementLogs[this.sanitizedMeasurementLogs.length - 1]
          .abs +
        this.sanitizedMeasurementLogs[this.sanitizedMeasurementLogs.length - 1]
          .chest +
        this.sanitizedMeasurementLogs[this.sanitizedMeasurementLogs.length - 1]
          .hips +
        this.sanitizedMeasurementLogs[this.sanitizedMeasurementLogs.length - 1]
          .midArm +
        this.sanitizedMeasurementLogs[this.sanitizedMeasurementLogs.length - 1]
          .thighs;
    }

    this.measurementFGInitial = this.measurementFG.value;
    this.disableMeasurementLogsDates();
  }

  get logsFA(): FormArray {
    return this.measurementFG.get('logs') as FormArray;
  }

  disableMeasurementLogsDates(): void {
    this.logsFA.controls.map((c) => c.get('date').disable());
  }

  enableMeasurementLogsDates(): void {
    this.logsFA.controls.map((c) => c.get('date').enable());
  }

  newLog(log?: any): FormGroup {
    const MeasurementLog = log
      ? new UserMeasurementLog(log, this._uts)
      : new UserMeasurementLog();
    const formGroup = this._formBuilder.group(UserMeasurementLog);

    formGroup.get('date').setValidators([Validators.required]);
    formGroup
      .get('abs')
      .setValidators([
        Validators.required,
        Validators.min(0.01),
        Validators.max(100),
        Validators.pattern(/^[0-9]{1,3}?(\.[0-9]{1,2})?$/)
      ]);
    formGroup
      .get('chest')
      .setValidators([
        Validators.required,
        Validators.min(0.01),
        Validators.max(100),
        Validators.pattern(/^[0-9]{1,3}?(\.[0-9]{1,2})?$/)
      ]);
    formGroup
      .get('hips')
      .setValidators([
        Validators.required,
        Validators.min(0.01),
        Validators.max(100),
        Validators.pattern(/^[0-9]{1,3}?(\.[0-9]{1,2})?$/)
      ]);
    formGroup
      .get('midArm')
      .setValidators([
        Validators.required,
        Validators.min(0.01),
        Validators.max(100),
        Validators.pattern(/^[0-9]{1,3}?(\.[0-9]{1,2})?$/)
      ]);
    formGroup
      .get('thighs')
      .setValidators([
        Validators.required,
        Validators.min(0.01),
        Validators.max(100),
        Validators.pattern(/^[0-9]{1,3}?(\.[0-9]{1,2})?$/)
      ]);

    return formGroup;
  }

  addLog(log?: any): void {
    this.logsFA.insert(0, this.newLog(log));
  }

  removeLog(i: number) {
    this.logsFA.removeAt(i);
  }

  addMeasurementLogs(): void {
    this.sanitizedMeasurementLogs.map((i) => {
      this.addLog(i);
    });
  }

  async onSave(): Promise<void> {
    if (!this.hasMeasurementFGChanged) {
      return;
    }

    if (!this.measurementFG.valid) {
      this._sb.openErrorSnackBar(ErrorMessages.INVALID_DATA);
      return;
    }

    try {
      this.loading = true;
      this.enableMeasurementLogsDates();

      const obj = {
        logs: this.logsFA.value,
        uid: this.me.uid,
        membershipKey: this.me.membershipKey
      };
      await this._us.setUserMeasurementLog(obj);

      this.hasMeasurementFGChanged = false;
      this.loading = false;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
      this.loading = false;
    }
  }

  computeMeasurementDifference(
    val1: number,
    val2: number,
    showUnit = false
  ): string {
    let res = '-';

    if (val1 && val2) {
      const diff = Math.round((val1 - val2) * 100) / 100;

      if (diff > 0) {
        res = showUnit ? `+${diff} inch(es)` : `+${diff}`;
      } else {
        res = showUnit ? `${diff} inch(es)` : `${diff}`;
      }
    }

    return res;
  }

  // Needs to be an arrow function as per Angular Material since it's a property
  dateFilter = (date: Date): boolean => {
    if (this.sanitizedMeasurementLogs && this.sanitizedMeasurementLogs.length) {
      const res = this.sanitizedMeasurementLogs.filter(
        (i) => i.date.valueOf() === date.valueOf()
      );
      if (res.length > 0) {
        return false;
      }
    }
    return true;
  };

  onReset(): void {
    this.measurementFG.reset(this.measurementFGInitial);
    this.logsFA.clear();
    this.addMeasurementLogs();
    this.disableMeasurementLogsDates();
    this.hasMeasurementFGChanged = false;
    this.measurementFG.markAsPristine();
  }
}
