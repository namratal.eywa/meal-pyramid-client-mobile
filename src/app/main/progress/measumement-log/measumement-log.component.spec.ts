import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasumementLogComponent } from './measumement-log.component';

describe('MeasumementLogComponent', () => {
  let component: MeasumementLogComponent;
  let fixture: ComponentFixture<MeasumementLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasumementLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasumementLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
