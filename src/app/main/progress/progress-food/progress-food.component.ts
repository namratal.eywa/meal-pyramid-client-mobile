import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

import { EMPTY, Observable } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import * as moment from 'moment';
import { ErrorMessages } from 'src/app/common/constants';
import { UserFoodLog, User } from 'src/app/common/models';
import { UserService, SnackBarService, AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-progress-food',
  templateUrl: './progress-food.component.html',
  styleUrls: ['./progress-food.component.scss']
})
export class ProgressFoodComponent implements OnInit {
  dispatchView = false;
  loggedDates: Date[] = [];
  logs: UserFoodLog[] = [];
  logsFA: FormArray = new FormArray([]);
  maxDate!: Date;
  me!: User;

  private _me$: Observable<User> = this._as.getMe();

  constructor(
    private _fb: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService
  ) {}

  ngOnInit(): void {
    
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          return this._us.getUserFoodLogs(
            this.me.uid,
            this.me.allMembershipKeys
          );
        }),
        catchError((err) => {
          console.log(err);
          this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        take(1)
      )
      .subscribe((logs: UserFoodLog[]) => {
        console.log(logs);
        this.setInitialLogsFA(logs);

        this.dispatchView = true;
      });

    this.maxDate = moment().endOf('day').toDate();
  }

  // *
  // * Food Log - Date
  // *

  onDailyDateSelection(event: MatDatepickerInputEvent<Date>): void {
    const date = event.value;
    this.addNewLog(date);
  }

  // Needs to be an arrow function as per Angular Material since it's a property
  dateFilter = (date: Date): boolean => {
    const res = this.loggedDates.filter((i) => i.valueOf() === date.valueOf());

    return res.length === 0;
  };

  // *
  // * Food Log - FormArray
  // *

  setInitialLogsFA(logs: UserFoodLog[]) {
    this.logsFA.clear();
    logs.map((log: UserFoodLog) => {
      this.addExistingLog(log);
    });
  }

  addExistingLog(log: UserFoodLog): void {
    const fg = this._fb.group({ ...log });
    this.logsFA.push(fg);

    this.loggedDates.push(log.date);
  }

  addNewLog(date: Date): void {
    const log = new UserFoodLog();
    log.date = date;
    log.uid = this.me.uid;
    log.membershipKey = this.me.membershipKey;
    log.isActive = true;
    log.recStatus = true;

    const fg = this._fb.group({ ...log });
    this.logsFA.insert(0, fg);

    this.loggedDates.push(date);
  }

  removeLog(e: { date: Date }, i: number) {
    this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
    this.logsFA.removeAt(i);
  }
}
