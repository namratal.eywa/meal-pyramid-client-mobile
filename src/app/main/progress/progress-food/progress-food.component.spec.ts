import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressFoodComponent } from './progress-food.component';

describe('ProgressFoodComponent', () => {
  let component: ProgressFoodComponent;
  let fixture: ComponentFixture<ProgressFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
