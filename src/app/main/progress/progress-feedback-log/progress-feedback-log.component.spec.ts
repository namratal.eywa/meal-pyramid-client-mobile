import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressFeedbackLogComponent } from './progress-feedback-log.component';

describe('ProgressFeedbackLogComponent', () => {
  let component: ProgressFeedbackLogComponent;
  let fixture: ComponentFixture<ProgressFeedbackLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressFeedbackLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressFeedbackLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
