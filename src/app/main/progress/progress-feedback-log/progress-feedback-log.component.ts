import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ErrorMessages, SuccessMessages } from 'src/app/common/constants';
import { DialogBox } from 'src/app/common/models';
import { UtilitiesService, GeneralService, SnackBarService, SharedService } from 'src/app/core/services';
import { DialogBoxComponent } from 'src/app/shared/dialog-box';


@Component({
  selector: 'app-progress-feedback-log',
  templateUrl: './progress-feedback-log.component.html',
  styleUrls: ['./progress-feedback-log.component.scss']
})
export class ProgressFeedbackLogComponent implements OnInit, OnDestroy {
  deleting = false;
  dispatchView = false;
  hasChanged = false;
  isNew = false;
  logFGInitial: any;
  saving = false;

  @Input() logFG: FormGroup;
  @Input() startWeight: number;
  @Output() deleteEvent = new EventEmitter();

  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _uts: UtilitiesService,
    private _gs: GeneralService,
    private _sb: SnackBarService,
    private _shs: SharedService,
    private _dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();

    this.dispatchView = true;

    this.logFG.valueChanges.pipe(takeUntil(this._notifier$)).subscribe(() => {
      this.hasChanged = !this._uts.shallowEqual(
        this.logFGInitial,
        this.logFG.getRawValue()
      );
    });

    this.observeRadioButtons();
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  get dateFC(): FormControl {
    return this.logFG.get('date') as FormControl;
  }

  get keyFC(): FormControl {
    return this.logFG.get('key') as FormControl;
  }

  get uidFC(): FormControl {
    return this.logFG.get('uid') as FormControl;
  }

  setInitialFG(): void {
    this.setInputFields();
    this.logFGInitial = this.logFG.getRawValue();
  }

  get isOutsideMealFC(): FormControl {
    return this.logFG.get('isOutsideMeal') as FormControl;
  }

  get outsideMealsCountFC(): FormControl {
    return this.logFG.get('outsideMealsCount') as FormControl;
  }

  get isMealSkippedFC(): FormControl {
    return this.logFG.get('isMealSkipped') as FormControl;
  }

  get skippedMealsCountFC(): FormControl {
    return this.logFG.get('skippedMealsCount') as FormControl;
  }

  get isRoutineChangedFC(): FormControl {
    return this.logFG.get('isRoutineChanged') as FormControl;
  }

  get routineChangeDetailsFC(): FormControl {
    return this.logFG.get('routineChangeDetails') as FormControl;
  }

  get isHungryBetweenMealsFC(): FormControl {
    return this.logFG.get('isHungryBetweenMeals') as FormControl;
  }

  get isAlcoholConsumedFC(): FormControl {
    return this.logFG.get('isAlcoholConsumed') as FormControl;
  }

  get alcoholConsumedDetailsFC(): FormControl {
    return this.logFG.get('alcoholConsumedDetails') as FormControl;
  }

  get isHydrationFollowedFC(): FormControl {
    return this.logFG.get('isHydrationFollowed') as FormControl;
  }

  get hydrationIntakeFC(): FormControl {
    return this.logFG.get('hydrationIntake') as FormControl;
  }

  get hadCompetitionFC(): FormControl {
    return this.logFG.get('hadCompetition') as FormControl;
  }

  get competitionPerformanceFC(): FormControl {
    return this.logFG.get('competitionPerformance') as FormControl;
  }

  get competitionRecoveryFC(): FormControl {
    return this.logFG.get('competitionRecovery') as FormControl;
  }

  get upcomingCompetitionFC(): FormControl {
    return this.logFG.get('upcomingCompetition') as FormControl;
  }

  get upcomingCompetitionDetailsFC(): FormControl {
    return this.logFG.get('upcomingCompetitionDetails') as FormControl;
  }

  setInputFields(): void {
    if (!this.isOutsideMealFC.value) {
      this.outsideMealsCountFC.disable();
    }

    if (!this.isMealSkippedFC.value) {
      this.skippedMealsCountFC.disable();
    }

    if (!this.isRoutineChangedFC.value) {
      this.routineChangeDetailsFC.disable();
    }

    if (!this.isAlcoholConsumedFC.value) {
      this.alcoholConsumedDetailsFC.disable();
    }

    if (!this.isHydrationFollowedFC.value) {
      this.hydrationIntakeFC.disable();
    }

    if (!this.hadCompetitionFC.value) {
      this.competitionPerformanceFC.disable();
      this.competitionRecoveryFC.disable();
    }

    if (!this.upcomingCompetitionFC.value) {
      this.upcomingCompetitionDetailsFC.disable();
    }
  }

  observeRadioButtons(): void {
    this.isOutsideMealFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.outsideMealsCountFC.enable();
        } else {
          this.outsideMealsCountFC.reset('');
          this.outsideMealsCountFC.disable();
        }
      });

    this.isMealSkippedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.skippedMealsCountFC.enable();
        } else {
          this.skippedMealsCountFC.reset('');
          this.skippedMealsCountFC.disable();
        }
      });

    this.isRoutineChangedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.routineChangeDetailsFC.enable();
        } else {
          this.routineChangeDetailsFC.reset('');
          this.routineChangeDetailsFC.disable();
        }
      });

    this.isAlcoholConsumedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.alcoholConsumedDetailsFC.enable();
        } else {
          this.alcoholConsumedDetailsFC.reset('');
          this.alcoholConsumedDetailsFC.disable();
        }
      });

    this.isHydrationFollowedFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.hydrationIntakeFC.enable();
        } else {
          this.hydrationIntakeFC.reset('');
          this.hydrationIntakeFC.disable();
        }
      });

    this.hadCompetitionFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.competitionPerformanceFC.enable();
          this.competitionRecoveryFC.enable();
        } else {
          this.competitionPerformanceFC.reset('');
          this.competitionPerformanceFC.disable();
          this.competitionRecoveryFC.reset('');
          this.competitionRecoveryFC.disable();
        }
      });

    this.upcomingCompetitionFC.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        if (val) {
          this.upcomingCompetitionDetailsFC.enable();
        } else {
          this.upcomingCompetitionDetailsFC.reset('');
          this.upcomingCompetitionDetailsFC.disable();
        }
      });
  }

  reset(): void {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  async save(): Promise<void> {
    if (!this.hasChanged || !this.logFG.valid || this.deleting) {
      return;
    }

    try {
      this.saving = true;

      if (this.isNew) {
        const key = this._gs.getNewKey(
          'userFeedbackLogs',
          true,
          this.uidFC.value
        );
        this.keyFC.setValue(key);
      }

      await this._gs.setData(
        this.uidFC.value,
        'userFeedbackLogs',
        this.logFG.getRawValue(),
        this.isNew,
        ['date'],
        true,
        this.keyFC.value
      );

      this.saving = false;
      this.isNew = false;
      this.setInitialFG();
      this.hasChanged = false;
      this._sb.openSuccessSnackBar(SuccessMessages.SAVE_OK, 2000);
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SAVE_FAILED);
      this.saving = false;
    }
  }

  async delete(): Promise<void> {
    if (this.saving) {
      return;
    }

    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to delete the log?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    dialogRef.afterClosed().subscribe(async (res: any) => {
      if (!res) {
        return;
      }
      try {
        this.deleting = true;

        if (!this.isNew) {
          await this._gs.setData(
            this.uidFC.value,
            'userFeedbackLogs',
            { recStatus: false },
            this.isNew,
            ['date'],
            true,
            this.keyFC.value
          );
        }

        this.deleteEvent.emit({ date: this.dateFC.value });
      } catch (err) {
        this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
        this.deleting = false;
      }
    });
  }
}
