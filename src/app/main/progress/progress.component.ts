import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { User } from 'src/app/common/models';
import { AuthService, SnackBarService, SharedService } from 'src/app/core/services';
import { GetStartedComponent } from 'src/app/shared/get-started';

// import * as moment from 'moment';
// import { MatDatepickerInputEvent } from '@angular/material/datepicker';
// import { environment } from '@envs/environment';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class ProgressComponent implements OnInit, OnDestroy {
  dispatchView = false;
  // foodLogs: any[];
  // imgURL = environment.photoLogGuide.imgURL;
  isPackageSubscribed = false;
  loading = false;
  // maxDate: Date;
  me: User;
  // photoLogs: any[];
  // recallLogs: any[];
  tabSelected = 0;
  public segment='food';
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    // private _us: UserService,
    private _sb: SnackBarService, // private _sts: StaticDataService,
    private _shs: SharedService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.isPackageSubscribed = this.me.flags.isPackageSubscribed;

          return this._shs.getProgressTabSelected();
        }),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((tab: number) => {
        this.tabSelected = tab;

        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  // isFoodLogAvailable(d: Date): boolean {
  //   const arr = this.foodLogs.filter((item) =>
  //     item.date.seconds
  //       ? item.date.toDate().valueOf() === d.valueOf()
  //       : item.date.valueOf() === d.valueOf()
  //   );
  //   return arr.length > 0;
  // }

  // isRecallLogAvailable(d: Date): boolean {
  //   const arr = this.recallLogs.filter((item) =>
  //     item.date.seconds
  //       ? item.date.toDate().valueOf() === d.valueOf()
  //       : item.date.valueOf() === d.valueOf()
  //   );
  //   return arr.length > 0;
  // }

  // isPhotoLogAvailable(d: Date): boolean {
  //   const arr = this.photoLogs.filter((item) =>
  //     item.date.seconds
  //       ? item.date.toDate().valueOf() === d.valueOf()
  //       : item.date.valueOf() === d.valueOf()
  //   );
  //   return arr.length > 0;
  // }

  // async onDailyDateSelection(
  //   event: MatDatepickerInputEvent<Date>
  // ): Promise<void> {
  //   if (!this.isFoodLogAvailable(event.value)) {
  //     this.loading = true;
  //     const ref = this._sts.getNewKey('userProgressDaily', true, this.me.uid);
  //     await this._sts.setData(
  //       this.me.uid,
  //       'userProgressDaily',
  //       {
  //         date: event.value,
  //         uid: this.me.uid,
  //         membershipKey: this.me.membershipKey,
  //         key: ref
  //       },
  //       true,
  //       ['date'],
  //       true,
  //       ref
  //     );
  //     this.foodLogs.unshift({ date: event.value, key: ref, isNew: true });
  //     this.loading = false;
  //   }
  // }

  // async onRecallDateSelection(
  //   event: MatDatepickerInputEvent<Date>
  // ): Promise<void> {
  //   if (!this.isRecallLogAvailable(event.value)) {
  //     this.loading = true;
  //     const ref = this._sts.getNewKey('userProgressRecall', true, this.me.uid);
  //     await this._sts.setData(
  //       this.me.uid,
  //       'userProgressRecall',
  //       {
  //         date: event.value,
  //         uid: this.me.uid,
  //         membershipKey: this.me.membershipKey,
  //         key: ref
  //       },
  //       true,
  //       ['date'],
  //       true,
  //       ref
  //     );
  //     this.recallLogs.unshift({ date: event.value, key: ref, isNew: true });
  //     this.loading = false;
  //   }
  // }

  // async onPhotoDateSelection(
  //   event: MatDatepickerInputEvent<Date>
  // ): Promise<void> {
  //   if (!this.isPhotoLogAvailable(event.value)) {
  //     this.loading = true;
  //     const ref = this._sts.getNewKey('userPhotoLogs', true, this.me.uid);
  //     await this._sts.setData(
  //       this.me.uid,
  //       'userPhotoLogs',
  //       {
  //         frontalImages: [],
  //         lateralImages: [],
  //         backImages: [],
  //         date: event.value,
  //         uid: this.me.uid,
  //         membershipKey: this.me.membershipKey,
  //         key: ref
  //       },
  //       true,
  //       ['date'],
  //       true,
  //       ref
  //     );
  //     this.photoLogs.unshift({ date: event.value, key: ref, isNew: true });
  //     this.loading = false;
  //   }
  // }

  showDialog(): void {
    this._dialog.open(GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: { step: this.me.onboardingStep || 0 }
    });
  }
}
