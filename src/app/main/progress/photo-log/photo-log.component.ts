import { Component, OnInit, Input } from '@angular/core';
import { Subject, Observable, EMPTY } from 'rxjs';
import { takeUntil, catchError, switchMap } from 'rxjs/operators';

import { Lightbox, LightboxConfig } from 'ngx-lightbox';
import { AuthService, SnackBarService, StaticDataService, StorageService, UserService, UtilitiesService } from 'src/app/core/services';
import { ErrorMessages, LightboxConfigs } from 'src/app/common/constants';
import { User } from 'src/app/common/models/user.model';


@Component({
  selector: 'app-photo-log',
  templateUrl: './photo-log.component.html',
  styleUrls: ['./photo-log.component.scss']
})
export class PhotoLogComponent implements OnInit {
  acceptedFileTypes = '.png, .jpeg, .jpg';
  backImagesArr = [];
  deleting = false;
  dispatchView = false;
  file: File;
  fileName = '';
  frontalImagesArr = [];
  lateralImagesArr = [];
  loading = false;
  lightboxAlbum = [];
  me: User;
  photoLog: any;
  uploadCollectionName = 'userPhotoLogFiles';
  uploadDir = 'photoLogs';
  uploadingBack = false;
  uploadingFrontal = false;
  uploadingLateral = false;

  @Input('photoLog') photoLogInitial: any;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService,
    private _lightbox: Lightbox,
    private _lightboxConfig: LightboxConfig,
    private _sts: StorageService,
    private _sds: StaticDataService,
    private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;
          return this._us.getUserPhotoLog(
            this.me.uid,
            this.photoLogInitial.key
          );
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((res : any) => {
        this.photoLog = res;

        if (res && res.frontalImages && res.frontalImages.length) {
          this.frontalImagesArr = res.frontalImages;
        }

        if (res && res.lateralImages && res.lateralImages.length) {
          this.lateralImagesArr = res.lateralImages;
        }

        if (res && res.backImages && res.backImages.length) {
          this.backImagesArr = res.backImages;
        }

        this.lightboxInit();

        this.dispatchView = true;
      });
  }

  lightboxInit(): void {
    Object.assign(this._lightboxConfig, LightboxConfigs);
  }

  openLightbox(src: string): void {
    this.lightboxAlbum = [];
    this.lightboxAlbum.push({
      src
    });
    this._lightbox.open(this.lightboxAlbum, 0);
  }

  onUploadFile(e: any, str: string): Promise<void> {
    if (!e || !e.target || !e.target.files) {
      return;
    }

    const fileList = e.target.files;
    if (fileList.length < 1) {
      return;
    }

    this.uploadingFrontal = Boolean(str === 'Frontal');
    this.uploadingLateral = Boolean(str === 'Lateral');
    this.uploadingBack = Boolean(str === 'Back');

    this.fileName = `MP__${str}-Photo__${this._uts.sanitize(
      this.me.name
    )}__${Math.floor(Date.now() / 1000)}`;
    this.file = fileList.item(0);
  }

  async onUploadComplete(obj: {
    fileKey: string;
    fileName: string;
    filePath: string;
    fileURL: string;
  }): Promise<void> {
    try {
      const data: any = {};

      if (obj.fileName.includes('Frontal')) {
        this.frontalImagesArr.push(obj);
        data.frontalImages = this.frontalImagesArr;
      } else if (obj.fileName.includes('Lateral')) {
        this.lateralImagesArr.push(obj);
        data.lateralImages = this.lateralImagesArr;
      } else if (obj.fileName.includes('Back')) {
        this.backImagesArr.push(obj);
        data.backImages = this.backImagesArr;
      }

      await this._sds.setData(
        this.me.uid,
        'userPhotoLogs',
        data,
        false,
        ['date'],
        true,
        this.photoLog.key
      );

      this.resetLoaders();
    } catch (error) {
      this.resetLoaders();
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
    }
  }

  resetLoaders(): void {
    this.deleting = false;
    this.uploadingFrontal = false;
    this.uploadingLateral = false;
    this.uploadingBack = false;
  }

  isAnyLoaderActive(): boolean {
    return (
      this.deleting ||
      this.uploadingFrontal ||
      this.uploadingLateral ||
      this.uploadingBack
    );
  }

  async onDeleteFile(img, str): Promise<void> {
    try {
      this.deleting = true;
      let filteredArr = [];
      const data: any = {};

      if (str === 'Frontal') {
        filteredArr = this.frontalImagesArr.filter(
          (i) => i.filePath === img.filePath
        );
        this.frontalImagesArr = this.frontalImagesArr.filter(
          (i) => i.filePath !== img.filePath
        );
        data.frontalImages = this.frontalImagesArr;
      } else if (str === 'Lateral') {
        filteredArr = this.lateralImagesArr.filter(
          (i) => i.filePath === img.filePath
        );
        this.lateralImagesArr = this.lateralImagesArr.filter(
          (i) => i.filePath !== img.filePath
        );
        data.lateralImages = this.lateralImagesArr;
      } else if (str === 'Back') {
        filteredArr = this.backImagesArr.filter(
          (i) => i.filePath === img.filePath
        );
        this.backImagesArr = this.backImagesArr.filter(
          (i) => i.filePath !== img.filePath
        );
        data.backImages = this.backImagesArr;
      }

      await this._sds.setData(
        this.me.uid,
        'userPhotoLogs',
        data,
        false,
        ['date'],
        true,
        this.photoLog.key
      );

      if (filteredArr && filteredArr.length && filteredArr.length === 1) {
        this._sds
          .setData(
            this.me.uid,
            'userPhotoLogFiles',
            {
              recStatus: false
            },
            false,
            ['uploadedAt'],
            true,
            filteredArr[0].fileKey
          )
          .then()
          .catch();
      }

      this._sts.delete(img.filePath).then().catch();
      this.deleting = false;
    } catch (err) {
      this.deleting = false;
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
    }
  }
}
