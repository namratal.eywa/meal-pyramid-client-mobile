import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotoLogComponent } from './photo-log.component';

describe('PhotoLogComponent', () => {
  let component: PhotoLogComponent;
  let fixture: ComponentFixture<PhotoLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotoLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
