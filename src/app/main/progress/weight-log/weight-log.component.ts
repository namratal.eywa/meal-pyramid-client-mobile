import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  FormArray,
  Validators
} from '@angular/forms';
import { Subject, Observable, EMPTY } from 'rxjs';
import { takeUntil, catchError, switchMap } from 'rxjs/operators';

import * as moment from 'moment';
import { ErrorMessages } from 'src/app/common/constants';
import { UserService, SnackBarService, AuthService, UtilitiesService } from 'src/app/core/services';
import { UserWeightLog, WeightLog ,  } from 'src/app/common/models/weight-log.model';
import { User } from 'src/app/common/models';

@Component({
  selector: 'app-weight-log',
  templateUrl: './weight-log.component.html',
  styleUrls: ['./weight-log.component.scss']
})
export class WeightLogComponent implements OnInit {
  dispatchView = false;
  hasWeightFGChanged = false;
  latestWeight = 0;
  loading = false;
  maxDate: Date;
  me: User;
  sanitizedWeightLogs: WeightLog[] = [];
  startDate: Date = null;
  startWeight: number;
  targetDate: Date = null;
  targetWeight: number;
  userWeightLog: UserWeightLog;
  weightFG: FormGroup;
  weightFGInitial: any;
  weightLogs: WeightLog[];

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject<any>();

  constructor(
    private _formBuilder: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService,
    private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this.maxDate = moment().endOf('day').toDate();

    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;
          return this._us.getUserWeightLogs(this.me.uid);
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((res) => {
        if (!res || res.length === 0) {
          this.userWeightLog = new UserWeightLog();
        } else {
          [this.userWeightLog] = res;
        }
        this.weightLogs = this.userWeightLog.logs;
        this.initFG(this.userWeightLog);

        this.weightFG.valueChanges.subscribe((val) => {
          this.hasWeightFGChanged = !this._uts.deepEqual(
            {
              ...this.weightFGInitial
            },
            val
          );
        });

        this.dispatchView = true;
      });
  }

  initFG(weightLog: any): void {
    this.weightFG = this._formBuilder.group({});
    for (const key in this.userWeightLog) {
      if (Array.isArray(this.userWeightLog[key])) {
        this.weightFG.addControl(key, this._formBuilder.array([]));
      } else {
        this.weightFG.addControl(
          key,
          this._formBuilder.control(this.userWeightLog[key])
        );
      }
    }

    if (weightLog && weightLog.logs && weightLog.logs.length) {
      this.sanitizedWeightLogs = weightLog.logs
        .filter((i) => i.date)
        .sort((a, b) => a.date - b.date);
      this.addWeightLogs();

      this.latestWeight =
        this.sanitizedWeightLogs[this.sanitizedWeightLogs.length - 1].weight;
    }

    this.weightFGInitial = this.weightFG.value;
    this.disableWeightLogsDates();
  }

  get startWeightFC(): FormControl {
    return this.weightFG.get('startWeight') as FormControl;
  }

  get startDateFC(): FormControl {
    return this.weightFG.get('startDate') as FormControl;
  }

  get targetWeightFC(): FormControl {
    return this.weightFG.get('targetWeight') as FormControl;
  }

  get targetDateFC(): FormControl {
    return this.weightFG.get('targetDate') as FormControl;
  }

  get logsFA(): FormArray {
    return this.weightFG.get('logs') as FormArray;
  }

  disableWeightLogsDates(): void {
    this.logsFA.controls.map((c) => c.get('date').disable());
  }

  enableWeightLogsDates(): void {
    this.logsFA.controls.map((c) => c.get('date').enable());
  }

  newLog(log?: any): FormGroup {
    const weightLogObj = log ? new WeightLog(log, this._uts) : new WeightLog();
    const formGroup = this._formBuilder.group(weightLogObj);

    formGroup.get('date').setValidators([Validators.required]);
    formGroup
      .get('weight')
      .setValidators([
        Validators.required,
        Validators.min(0.01),
        Validators.max(500),
        Validators.pattern(/^[0-9]{1,3}?(\.[0-9]{1,2})?$/)
      ]);

    return formGroup;
  }

  addLog(log?: any): void {
    this.logsFA.insert(0, this.newLog(log));
  }

  removeLog(i: number) {
    this.logsFA.removeAt(i);
  }

  addWeightLogs(): void {
    this.sanitizedWeightLogs.map((i) => {
      this.addLog(i);
    });
  }

  async onSave(): Promise<void> {
    if (!this.hasWeightFGChanged) {
      return;
    }

    if (!this.weightFG.valid) {
      this._sb.openErrorSnackBar(ErrorMessages.INVALID_DATA);
      return;
    }

    try {
      this.loading = true;
      this.enableWeightLogsDates();

      const obj = {
        startWeight: this.startWeightFC.value,
        startDate: this.startDateFC.value,
        targetWeight: this.targetWeightFC.value,
        targetDate: this.targetDateFC.value,
        logs: this.logsFA.value,
        uid: this.me.uid,
        membershipKey: this.me.membershipKey
      };
      await this._us.setUserWeightLog(obj);

      this.hasWeightFGChanged = false;
      this.loading = false;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
      this.loading = false;
    }
  }

  computeWeightDifference(
    val1: number,
    val2: number,
    showUnit = false
  ): string {
    let res = '-';

    if (val1 && val2) {
      const diff = Math.round((val1 - val2) * 100) / 100;

      if (diff > 0) {
        res = showUnit ? `+${diff} kg` : `+${diff}`;
      } else {
        res = showUnit ? `${diff} kg` : `${diff}`;
      }
    }

    return res;
  }

  // Needs to be an arrow function as per Angular Material since it's a property
  dateFilter = (date: Date): boolean => {
    if (this.sanitizedWeightLogs && this.sanitizedWeightLogs.length) {
      const res = this.sanitizedWeightLogs.filter(
        (i) => i.date.valueOf() === date.valueOf()
      );
      if (res.length > 0) {
        return false;
      }
    }
    return true;
  };

  onReset(): void {
    this.weightFG.reset(this.weightFGInitial);
    this.logsFA.clear();
    this.addWeightLogs();
    this.disableWeightLogsDates();
    this.hasWeightFGChanged = false;
    this.weightFG.markAsPristine();
  }
}
