import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject, Observable, EMPTY } from 'rxjs';
import { takeUntil, catchError, switchMap } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { User } from 'src/app/common/models';
import { UserService, SnackBarService, UtilitiesService, AuthService, StaticDataService } from 'src/app/core/services';


@Component({
  selector: 'app-food-log',
  templateUrl: './food-log.component.html',
  styleUrls: ['./food-log.component.scss']
})
export class FoodLogComponent implements OnInit, OnDestroy {
  dispatchView = false;
  foodFormGroup: FormGroup;
  foodFormGroupInitial: any;
  foodLog: any;
  loading = false;
  me: User;

  @Input('foodLog') foodLogInitial: any;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _formBuilder: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _uts: UtilitiesService,
    private _as: AuthService,
    private _sts: StaticDataService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;
          return this._us.getUserFoodLog(this.me.uid, this.foodLogInitial.key);
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((res) => {
        this.foodLog = res;

        this.foodFormGroup = this._formBuilder.group({
          breakfastAte: [this.foodLog.breakfastAte || ''],
          breakfastDrank: [this.foodLog.breakfastDrank || ''],
          breakfastTime: [this.foodLog.breakfastTime || ''],
          midMorningAte: [this.foodLog.midMorningAte || ''],
          midMorningDrank: [this.foodLog.midMorningDrank || ''],
          midMorningTime: [this.foodLog.midMorningTime || ''],
          lunchAte: [this.foodLog.lunchAte || ''],
          lunchDrank: [this.foodLog.lunchDrank || ''],
          lunchTime: [this.foodLog.lunchTime || ''],
          midAfternoonAte: [this.foodLog.midAfternoonAte || ''],
          midAfternoonDrank: [this.foodLog.midAfternoonDrank || ''],
          midAfternoonTime: [this.foodLog.midAfternoonTime || ''],
          eveningAte: [this.foodLog.eveningAte || ''],
          eveningDrank: [this.foodLog.eveningDrank || ''],
          eveningTime: [this.foodLog.eveningTime || ''],
          dinnerAte: [this.foodLog.dinnerAte || ''],
          dinnerDrank: [this.foodLog.dinnerDrank || ''],
          dinnerTime: [this.foodLog.dinnerTime || ''],
          bedTimeAte: [this.foodLog.bedTimeAte || ''],
          bedTimeDrank: [this.foodLog.bedTimeDrank || ''],
          bedTimeTime: [this.foodLog.bedTimeTime || ''],
          preWorkoutAte: [this.foodLog.preWorkoutAte || ''],
          preWorkoutDrank: [this.foodLog.preWorkoutDrank || ''],
          preWorkoutTime: [this.foodLog.preWorkoutTime || ''],
          postWorkoutAte: [this.foodLog.postWorkoutAte || ''],
          postWorkoutDrank: [this.foodLog.postWorkoutDrank || ''],
          postWorkoutTime: [this.foodLog.postWorkoutTime || ''],
          otherAte: [this.foodLog.postWorkoutAte || ''],
          otherDrank: [this.foodLog.postWorkoutDrank || ''],
          otherTime: [this.foodLog.postWorkoutTime || ''],
          exerciseType: [this.foodLog.exerciseType || ''],
          exerciseDuration: [this.foodLog.exerciseDuration || ''],
          exerciseIntensity: [this.foodLog.exerciseIntensity || '']
        });
        this.foodFormGroupInitial = this.foodFormGroup.value;
        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  onReset(): void {
    this.foodFormGroup.reset(this.foodFormGroupInitial);
    this.foodFormGroup.markAsPristine();
  }

  hasFoodFormGroupChanged(): boolean {
    return !this._uts.shallowEqual(
      this.foodFormGroup.value,
      this.foodFormGroupInitial
    );
  }

  async onSave(): Promise<void> {
    if (!this.hasFoodFormGroupChanged()) {
      return;
    }

    this.loading = true;

    try {
      const obj = {
        ...this.foodFormGroup.value,
        key: this.foodLog.key,
        uid: this.me.uid,
        membershipKey: this.me.membershipKey
      };
      await this._sts.setData(
        this.me.uid,
        'userProgressDaily',
        obj,
        false,
        [],
        true,
        this.foodLog.key
      );
      this.foodLogInitial.isNew = false;
      this.loading = false;
    } catch (err) {
      this.loading = false;
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
    }
  }
}
