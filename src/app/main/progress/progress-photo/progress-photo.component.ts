import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

import { EMPTY, Observable } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';

import * as moment from 'moment';
import { ErrorMessages } from 'src/app/common/constants';
import { UserPhotoLog, User } from 'src/app/common/models';
import { UserService, SnackBarService, AuthService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-progress-photo',
  templateUrl: './progress-photo.component.html',
  styleUrls: ['./progress-photo.component.scss']
})
export class ProgressPhotoComponent implements OnInit {
  dispatchView = false;
  imgURL = environment.photoLogGuide.imgURL;
  loggedDates: Date[] = [];
  logs: UserPhotoLog[] = [];
  logsFA: FormArray = new FormArray([]);
  maxDate!: Date;
  me!: User;

  private _me$: Observable<User> = this._as.getMe();

  constructor(
    private _fb: FormBuilder,
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;

          return this._us.getUserPhotoLogs(
            this.me.uid,
            this.me.allMembershipKeys
          );
        }),
        catchError((err) => {
          this._sb.openErrorSnackBar(err.message || ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        take(1)
      )
      .subscribe((logs: UserPhotoLog[]) => {
        this.setInitialLogsFA(logs);

        this.dispatchView = true;
      });

    this.maxDate = moment().endOf('day').toDate();
  }

  // *
  // * Photo Log - Date
  // *

  onDailyDateSelection(event: MatDatepickerInputEvent<Date>): void {
    const date = event.value;
    this.addNewLog(date);
  }

  // Needs to be an arrow function as per Angular Material since it's a property
  dateFilter = (date: Date): boolean => {
    const res = this.loggedDates.filter((i : any) => i.valueOf() === date.valueOf());

    return res.length === 0;
  };

  // *
  // * Photo Log - FormArray
  // *

  setInitialLogsFA(logs: UserPhotoLog[]) {
    this.logsFA.clear();
    logs.map((log: UserPhotoLog) => {
      this.addExistingLog(log);
    });
  }

  addExistingLog(log: UserPhotoLog): void {
    const fg = this._fb.group({});

    for (const key in log) {
      if (Array.isArray(log[key])) {
        if (log[key].length > 0) {
          const fa = new FormArray([]);
          log[key].map((i) => {
            const fileFG = this._fb.group(i);
            fa.push(fileFG);
          });
          fg.addControl(key, fa);
        } else {
          fg.addControl(key, this._fb.array([]));
        }
      } else {
        fg.addControl(key, this._fb.control(log[key]));
      }
    }

    this.logsFA.push(fg);

    this.loggedDates.push(log.date);
  }

  addNewLog(date: Date): void {
    const log = new UserPhotoLog();
    log.date = date;
    log.uid = this.me.uid;
    log.membershipKey = this.me.membershipKey;
    log.isActive = true;
    log.recStatus = true;

    const fg = this._fb.group({});

    for (const key in log) {
      if (Array.isArray(log[key])) {
        fg.addControl(key, this._fb.array([]));
      } else {
        fg.addControl(key, this._fb.control(log[key]));
      }
    }

    this.logsFA.insert(0, fg);

    this.loggedDates.push(date);
  }

  removeLog(e: { date: Date }, i: number) {
    this.loggedDates = this.loggedDates.filter((i) => i !== e.date);
    this.logsFA.removeAt(i);
  }
}
