import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressPhotoComponent } from './progress-photo.component';

describe('ProgressPhotoComponent', () => {
  let component: ProgressPhotoComponent;
  let fixture: ComponentFixture<ProgressPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressPhotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
