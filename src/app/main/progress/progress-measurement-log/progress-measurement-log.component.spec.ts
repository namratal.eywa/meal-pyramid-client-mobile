import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressMeasurementLogComponent } from './progress-measurement-log.component';

describe('ProgressMeasurementLogComponent', () => {
  let component: ProgressMeasurementLogComponent;
  let fixture: ComponentFixture<ProgressMeasurementLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressMeasurementLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressMeasurementLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
