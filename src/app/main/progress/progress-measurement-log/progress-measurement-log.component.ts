import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ErrorMessages, SuccessMessages } from 'src/app/common/constants';
import { DialogBox } from 'src/app/common/models';
import { UtilitiesService, GeneralService, SnackBarService, SharedService } from 'src/app/core/services';
import { DialogBoxComponent } from 'src/app/shared/dialog-box';


@Component({
  selector: 'app-progress-measurement-log',
  templateUrl: './progress-measurement-log.component.html',
  styleUrls: ['./progress-measurement-log.component.scss']
})
export class ProgressMeasurementLogComponent implements OnInit, OnDestroy {
  deleting = false;
  dispatchView = false;
  hasChanged = false;
  isNew = false;
  isUpdated = false;
  logFGInitial: any;
  saving = false;
  startMeasurement: number = null;

  @Input() logFG: FormGroup;
  @Input() startWeight: number;
  @Output() deleteEvent = new EventEmitter();

  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _uts: UtilitiesService,
    private _gs: GeneralService,
    private _sb: SnackBarService,
    private _shs: SharedService,
    private _dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();

    this.dispatchView = true;

    this.logFG.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        this.hasChanged = !this._uts.shallowEqual(this.logFGInitial, val);
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  get dateFC(): FormControl {
    return this.logFG.get('date') as FormControl;
  }

  get absFC(): FormControl {
    return this.logFG.get('abs') as FormControl;
  }

  get chestFC(): FormControl {
    return this.logFG.get('chest') as FormControl;
  }

  get hipsFC(): FormControl {
    return this.logFG.get('hips') as FormControl;
  }

  get midArmFC(): FormControl {
    return this.logFG.get('midArm') as FormControl;
  }

  get thighsFC(): FormControl {
    return this.logFG.get('thighs') as FormControl;
  }

  get keyFC(): FormControl {
    return this.logFG.get('key') as FormControl;
  }

  get uidFC(): FormControl {
    return this.logFG.get('uid') as FormControl;
  }

  setInitialFG(): void {
    this.logFGInitial = this.logFG.value;
  }

  reset(): void {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  async save(): Promise<void> {
    if (!this.hasChanged || !this.logFG.valid || this.deleting) {
      return;
    }

    try {
      this.saving = true;

      if (this.isNew) {
        const key = this._gs.getNewKey(
          'userMeasurementLogs',
          true,
          this.uidFC.value
        );
        this.keyFC.setValue(key);
      }

      await this._gs.setData(
        this.uidFC.value,
        'userMeasurementLogs',
        this.logFG.value,
        this.isNew,
        ['date'],
        true,
        this.keyFC.value
      );

      this.saving = false;
      this.isUpdated = true;
      this.setInitialFG();
      this.hasChanged = false;
      this._sb.openSuccessSnackBar(SuccessMessages.SAVE_OK, 2000);
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SAVE_FAILED);
      this.saving = false;
    }
  }

  async delete(): Promise<void> {
    if (this.saving) {
      return;
    }

    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to delete the log?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    dialogRef.afterClosed().subscribe(async (res: any) => {
      if (!res) {
        return;
      }
      try {
        this.deleting = true;

        if (!this.isNew || this.isUpdated) {
          await this._gs.setData(
            this.uidFC.value,
            'userMeasurementLogs',
            { recStatus: false },
            this.isNew,
            ['date'],
            true,
            this.keyFC.value
          );
        }

        this.deleteEvent.emit({ date: this.dateFC.value });
      } catch (err) {
        this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
        this.deleting = false;
      }
    });
  }
}
