import { PhotoLogComponent } from './photo-log/photo-log.component';
import { WeightLogComponent } from './weight-log/weight-log.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressRoutingModule } from './progress-routing.module';
import { ProgressComponent } from './progress.component';
import { ProgressFoodComponent } from './progress-food/progress-food.component';
import { ProgressFoodLogComponent } from './progress-food-log/progress-food-log.component';
import { ProgressWeightComponent } from './progress-weight/progress-weight.component';
import { ProgressWeightLogComponent } from './progress-weight-log/progress-weight-log.component';
import { ProgressMeasurementComponent } from './progress-measurement/progress-measurement.component';
import { ProgressMeasurementLogComponent } from './progress-measurement-log/progress-measurement-log.component';
import { ProgressFeedbackComponent } from './progress-feedback/progress-feedback.component';
import { ProgressFeedbackLogComponent } from './progress-feedback-log/progress-feedback-log.component';
import { ProgressPhotoComponent } from './progress-photo/progress-photo.component';
import { ProgressPhotoLogComponent } from './progress-photo-log/progress-photo-log.component';
import { LayoutModule } from 'src/app/shared/layout/layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';
import { ButtonFileUploadModule } from 'src/app/shared/button-file-upload';
import { GetStartedModule } from 'src/app/shared/get-started';
import { MaterialDesignModule } from 'src/app/shared/material-design';
import { SharedComponentsModule } from 'src/app/shared/shared-components';
import { UploadFileModule } from 'src/app/shared/upload-file';
import {MatTabsModule} from '@angular/material/tabs';


@NgModule({
  declarations: [
    ProgressComponent,
    ProgressFoodComponent,
    ProgressFoodLogComponent,
    ProgressWeightComponent,
    ProgressWeightLogComponent,
    ProgressMeasurementComponent,
    ProgressMeasurementLogComponent,
    ProgressFeedbackComponent,
    ProgressFeedbackLogComponent,
    ProgressPhotoComponent,
    ProgressPhotoLogComponent,
    // FoodLogComponent,
    // WeightLogComponent,
    // MeasumementLogComponent,
    // PhotoLogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProgressRoutingModule,
    LayoutModule,
    GetStartedModule,
    MaterialDesignModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    LightboxModule,
    SharedComponentsModule,
    UploadFileModule,
    ButtonFileUploadModule,
    MatTabsModule
  ]
})
export class ProgressModule {}
