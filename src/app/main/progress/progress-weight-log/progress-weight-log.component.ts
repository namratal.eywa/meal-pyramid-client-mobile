import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatDialog } from '@angular/material/dialog';
import { ErrorMessages, SuccessMessages } from 'src/app/common/constants';
import { DialogBox } from 'src/app/common/models';
import { UtilitiesService, GeneralService, SharedService, SnackBarService } from 'src/app/core/services';
import { DialogBoxComponent } from 'src/app/shared/dialog-box';
@Component({
  selector: 'app-progress-weight-log',
  templateUrl: './progress-weight-log.component.html',
  styleUrls: ['./progress-weight-log.component.scss']
})
export class ProgressWeightLogComponent
  implements OnInit, OnChanges, OnDestroy
{
  deleting = false;
  delta = '';
  dispatchView = false;
  hasChanged = false;
  isNew = false;
  isUpdated = false;
  logFGInitial: any;
  saving = false;

  @Input() logFG: FormGroup;
  @Input() startWeight: number;
  @Output() deleteEvent = new EventEmitter();

  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _uts: UtilitiesService,
    private _gs: GeneralService,
    private _shs: SharedService,
    private _sb: SnackBarService,
    private _dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isNew = !this.keyFC.value;
    this.setInitialFG();

    this.dispatchView = true;

    this.logFG.valueChanges
      .pipe(takeUntil(this._notifier$))
      .subscribe((val) => {
        this.hasChanged = !this._uts.shallowEqual(this.logFGInitial, val);
        this.delta = this._uts.computeDifference(
          this.weightFC.value,
          this.startWeight
        );
      });
  }

  ngOnChanges(changes: SimpleChanges) {
    const { startWeight } = changes;

    if (startWeight) {
      this.delta = this._uts.computeDifference(
        this.weightFC.value,
        this.startWeight
      );
    }
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  get dateFC(): FormControl {
    return this.logFG.get('date') as FormControl;
  }

  get weightFC(): FormControl {
    return this.logFG.get('weight') as FormControl;
  }

  get keyFC(): FormControl {
    return this.logFG.get('key') as FormControl;
  }

  get uidFC(): FormControl {
    return this.logFG.get('uid') as FormControl;
  }

  setInitialFG(): void {
    this.logFGInitial = this.logFG.value;
  }

  reset(): void {
    if (!this.hasChanged || this.saving) {
      return;
    }

    this.logFG.reset(this.logFGInitial);
  }

  async save(): Promise<void> {
    if (!this.hasChanged || !this.logFG.valid || this.deleting) {
      return;
    }

    try {
      this.saving = true;

      if (this.isNew) {
        const key = this._gs.getNewKey(
          'userWeightLogs',
          true,
          this.uidFC.value
        );
        this.keyFC.setValue(key);
      }

      await this._gs.setData(
        this.uidFC.value,
        'userWeightLogs',
        this.logFG.value,
        this.isNew,
        ['date'],
        true,
        this.keyFC.value
      );

      this.saving = false;
      this.isUpdated = true;
      this.setInitialFG();
      this.hasChanged = false;
      this._sb.openSuccessSnackBar(SuccessMessages.SAVE_OK, 2000);
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SAVE_FAILED);
      this.saving = false;
    }
  }

  async delete(): Promise<void> {
    if (this.saving) {
      return;
    }
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to delete the log?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    dialogRef.afterClosed().subscribe(async (res: any) => {
      if (!res) {
        return;
      }

      try {
        this.deleting = true;

        if (!this.isNew || this.isUpdated) {
          await this._gs.setData(
            this.uidFC.value,
            'userWeightLogs',
            { recStatus: false },
            this.isNew,
            ['date'],
            true,
            this.keyFC.value
          );
        }

        this.deleteEvent.emit({ date: this.dateFC.value });
      } catch (err) {
        this._sb.openErrorSnackBar(ErrorMessages.DELETE_FAILED);
        this.deleting = false;
      }
    });
  }
}
