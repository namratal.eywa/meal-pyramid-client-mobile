import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressWeightLogComponent } from './progress-weight-log.component';

describe('ProgressWeightLogComponent', () => {
  let component: ProgressWeightLogComponent;
  let fixture: ComponentFixture<ProgressWeightLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressWeightLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressWeightLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
