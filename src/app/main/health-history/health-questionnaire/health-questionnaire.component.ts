import { Component, OnInit } from '@angular/core';
import { Subject, EMPTY, Observable } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';
import { UserCategories, ErrorMessages } from 'src/app/common/constants';
import { UserHealthHistory, User } from 'src/app/common/models';
import { AuthService, UserService, SnackBarService } from 'src/app/core/services';


@Component({
  selector: 'app-health-questionnaire',
  templateUrl: './health-questionnaire.component.html',
  styleUrls: ['./health-questionnaire.component.scss']
})
export class HealthQuestionnaireComponent implements OnInit {
  dispatchView = false;
  healthHistory: UserHealthHistory;
  me: User;
  userCategories: typeof UserCategories = UserCategories;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$ = new Subject<any>();

  constructor(
    private _as: AuthService,
    private _us: UserService,
    private _sb: SnackBarService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((user: User) => {
          this.me = user;

          return this._us.getUserHealthHistory(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((res: UserHealthHistory[]) => {
        if (res.length === 0) {
          this.healthHistory = new UserHealthHistory();
        } else if (res.length === 1) {
          [this.healthHistory] = res;
        } else {
          this.healthHistory = new UserHealthHistory();
        }

        this.dispatchView = true;
      });
  }
}
