import { UploadService } from './../../../core/services/upload.service';
import { Component, OnInit } from '@angular/core';
import { Observable, Subject, EMPTY } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';
import { AssessmentCategories, ErrorMessages, SuccessMessages, SuccessTypes } from 'src/app/common/constants';
import { Assessment, UserAssessment } from 'src/app/common/models/assessment.model';
import { Filee, Filee2 } from 'src/app/common/models/filee.model';
import { User } from 'src/app/common/models/user.model';
import { AuthService, UserService, SnackBarService, StaticDataService, StorageService } from 'src/app/core/services';


@Component({
  selector: 'app-clinical-assessment-file',
  templateUrl: './clinical-assessment-file.component.html',
  styleUrls: ['./clinical-assessment-file.component.scss']
})
export class ClinicalAssessmentFileComponent implements OnInit {
  acceptedFileTypes = '.jpg, .jpeg, .png, .pdf';
  areClinicalAssessmentFilesAvailable = false;
  assessments: Assessment[] = [];
  clinicalAssessmentFiles: Filee2[] = [];
  diabetesAssessments: Assessment[] = [];
  dispatchView = false;
  downloadFileName = '';
  file: File;
  fileKey = '';
  fileName = '';
  filePath = '';
  fileType = '';
  fileURL = '';
  generalAssessments: Assessment[] = [];
  hasAssessments = false;
  hormonesAssessments: Assessment[] = [];
  isFileImg = false;
  kidneyAssessments: Assessment[] = [];
  me: User;
  placeholder = 'Select a file to load details';
  otherAssessments: Assessment[] = [];
  otherBloodAssessments: Assessment[] = [];
  // showFile = false;
  sportsAssessments: Assessment[] = [];
  title = '';
  uploadCollectionName = 'userAssessmentFiles';
  uploadDir = 'assessments';
  uploadDirPath = 'assessments';
  uploadFileNamePartial = 'MEALpyramid_CA';
  uploadFileNamePrefix = '';
  uploading = false;
  userAssessment: UserAssessment;
  vitaminsAssessments: Assessment[] = [];

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  downloading: boolean=false;

  constructor(
    private _as: AuthService,
    private _us: UserService,
    private _sb: SnackBarService,
    private _sts: StorageService,
    private _sds: StaticDataService,
    // private _demo: UploadService
    // private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;

          return this._us.getActiveUserAssessments(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        switchMap((arr: UserAssessment[]) => {
          if (arr.length) {
            if (arr.length !== 1) {
              throw new Error();
            }

            this.hasAssessments = Boolean(arr[0].tests && arr[0].tests.length);

            this.assessments = arr[0].tests;

            this.generalAssessments = this.assessments.filter(
              (item) => item.category === AssessmentCategories.GENERAL
            );

            // this.kidneyAssessments = this.assessments.filter(
            //   (item) => item.category === AssessmentCategories.KIDNEY_PROFILE
            // );

            this.diabetesAssessments = this.assessments.filter(
              (item) => item.category === AssessmentCategories.DIABETIC_PROFILE
            );

            this.vitaminsAssessments = this.assessments.filter(
              (item) => item.category === AssessmentCategories.VITAMIN_PROFILE
            );

            this.hormonesAssessments = this.assessments.filter(
              (item) => item.category === AssessmentCategories.HORMONES
            );

            this.sportsAssessments = this.assessments.filter(
              (item) =>
                item.category === AssessmentCategories.SPORTS_SPECIFIC_TESTS
            );

            // this.otherBloodAssessments = this.assessments.filter(
            //   (item) => item.category === AssessmentCategories.OTHER_BLOOD_TESTS
            // );

            this.otherAssessments = this.assessments.filter(
              (item) => item.category === AssessmentCategories.OTHER_TESTS
            );
          } else {
            this.hasAssessments = false;
          }

          return this._us.getUserAssessmentFiles(
            this.me.uid,
            // this.me.membershipKey
            this.me.allMembershipKeys
          );
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((files: Filee2[]) => {
        this.clinicalAssessmentFiles = files.sort(
          (a, b) => b.uploadedAt.valueOf() - a.uploadedAt.valueOf()
        );
        this.areClinicalAssessmentFilesAvailable = Boolean(
          this.clinicalAssessmentFiles.length > 0
        );

        this.dispatchView = true;
      });
  }
  onDownloadFile(file: Filee2): void {
    this.downloading = true;
    console.log(file.fileURL);
    console.log( file.fileName);
    this._sts.download(file.fileURL, file.fileName);
    // this._demo.downloadFile(file.fileURL, file.fileName);
    setTimeout(() => {
      this.downloading = false;
      this._sb.openSnackBar(SuccessTypes.FILE_DOWNLAOD);
    }, 2000);
  }

  public onShowClick(file: Filee2) {
    // this.showFile = false;
    this.title = file.fileName;
    this.fileKey = file.key;
    this.fileType = file.fileType;
    this.downloadFileName = file.fileName;
    this.filePath = file.filePath;
    this.fileURL = file.fileURL;

    this.isFileImg = file.fileType.includes('image');

    // setTimeout(() => {
    //   this.showFile = true;
    // }, 250);
  }

  // onUploadFile(e: any): Promise<void> {
  //   if (!e || !e.target || !e.target.files) {
  //     return;
  //   }

  //   const fileList = e.target.files;
  //   if (fileList.length < 1) {
  //     return;
  //   }

  //   this.uploading = true;
  //   this.areClinicalAssessmentFilesAvailable = false;
  //   this.fileName = `MEALpyramid_CA_${this._uts.sanitize(
  //     this.me.name
  //   )}_${Math.floor(Date.now() / 100)}`;
  //   this.file = fileList.item(0);
  // }

  // onUploadComplete(): void {
  //   this.uploading = false;
  // }

  onUploadComplete(): void {
    this._sb.openSuccessSnackBar(SuccessMessages.FILE_UPLOADED);
  }
}
