import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalAssessmentFileComponent } from './clinical-assessment-file.component';

describe('ClinicalAssessmentFileComponent', () => {
  let component: ClinicalAssessmentFileComponent;
  let fixture: ComponentFixture<ClinicalAssessmentFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicalAssessmentFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalAssessmentFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
