import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { User } from 'src/app/common/models';
import { AuthService, SharedService } from 'src/app/core/services';
import { GetStartedComponent } from 'src/app/shared/get-started';


@Component({
  selector: 'app-health-history',
  templateUrl: './health-history.component.html',
  styleUrls: ['./health-history.component.scss']
})
export class HealthHistoryComponent implements OnInit {
  dispatchView = false;
  isPackageSubscribed = false;
  me: User;
  tabSelected = 0;
  public segment = 'address';

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    private _ss: SharedService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.isPackageSubscribed = this.me.flags.isPackageSubscribed;

          return this._ss.getHealthHistoryTabSelected();
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((res: any) => {
        this.tabSelected = res;

        this.dispatchView = true;
      });
  }

  showDialog(): void {
    this._dialog.open(GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: { step: this.me.onboardingStep || 0 }
    });
  }
}
