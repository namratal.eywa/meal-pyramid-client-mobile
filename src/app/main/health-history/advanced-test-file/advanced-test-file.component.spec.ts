import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedTestFileComponent } from './advanced-test-file.component';

describe('AdvancedTestFileComponent', () => {
  let component: AdvancedTestFileComponent;
  let fixture: ComponentFixture<AdvancedTestFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedTestFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedTestFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
