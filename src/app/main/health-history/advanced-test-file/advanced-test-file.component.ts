import { Component, OnInit } from '@angular/core';
import { Observable, Subject, EMPTY } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';
import { ErrorMessages, SuccessTypes } from 'src/app/common/constants';
import {  User, } from 'src/app/common/models';
import { AuthService, UserService, SnackBarService, StorageService } from 'src/app/core/services';
import { Filee, Filee2 } from 'src/app/common/models/filee.model';


@Component({
  selector: 'app-advanced-test-file',
  templateUrl: './advanced-test-file.component.html',
  styleUrls: ['./advanced-test-file.component.scss']
})
export class AdvancedTestFileComponent implements OnInit {
  acceptedFileTypes = '.jpg, .jpeg, .png, .pdf';
  advancedTestFiles: Filee2[] = [];
  areAdvancedTestFilesAvailable = false;
  dispatchView = false;
  downloadFileName = '';
  fileURL = '';
  isFileImg = false;
  me: User;
  placeholder = 'Select a file to load details';
  showFile = false;
  title = '';
  downloading: boolean=false;
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _us: UserService,
    private _sb: SnackBarService,
    private _sts: StorageService,
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          return this._us.getUserAdvancedTestFiles(
            this.me.uid,
            // this.me.membershipKey
            this.me.allMembershipKeys
          );
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((files: Filee2[]) => {
        this.advancedTestFiles = files;
        this.areAdvancedTestFilesAvailable = Boolean(
          this.advancedTestFiles.length > 0
        );

        this.dispatchView = true;
      });
  }


  
  public onDownloadFile(file: Filee2): void {
    this.downloading = true;
    console.log(file)
    this._sts.download(file.fileURL, file.fileName);
    setTimeout(() => {
      this.downloading = false;
      this._sb.openSnackBar(SuccessTypes.FILE_DOWNLAOD);
    }, 2000);
  }
}
