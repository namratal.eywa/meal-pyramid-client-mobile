import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HealthHistoryComponent } from './health-history.component';
import { HealthHistoryRoutingModule } from './health-history-routing.module';
import { AdvancedTestFileComponent } from './advanced-test-file/advanced-test-file.component';
import { ClinicalAssessmentFileComponent } from './clinical-assessment-file/clinical-assessment-file.component';
import { HealthQuestionnaireComponent } from './health-questionnaire/health-questionnaire.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ButtonFileUploadModule } from 'src/app/shared/button-file-upload';
import { GetStartedModule } from 'src/app/shared/get-started';
import { MaterialDesignModule } from 'src/app/shared/material-design';
import { SharedComponentsModule } from 'src/app/shared/shared-components';
import { UploadFileModule } from 'src/app/shared/upload-file';
import { LayoutModule } from 'src/app/shared/layout';
import { HomeModule } from 'src/app/home';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HealthHistoryComponent,
    AdvancedTestFileComponent,
    ClinicalAssessmentFileComponent,
    HealthQuestionnaireComponent
  ],
  imports: [
    CommonModule,
    LayoutModule,
    IonicModule,
    GetStartedModule,
    MaterialDesignModule,
    FlexLayoutModule,
    HealthHistoryRoutingModule,
    SharedComponentsModule,
    UploadFileModule,
    ButtonFileUploadModule,
    FormsModule
  ]
})
export class HealthHistoryModule {}
