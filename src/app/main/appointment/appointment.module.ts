import { ViewHistoryComponent } from './view-history/view-history.component';
import { AppointComponent } from './appoint/appoint.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { AppointmentRoutingModule } from './appointment-routing.module';
import { AppointmentComponent } from './appointment.component';
import { LayoutModule } from 'src/app/shared/layout';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GetStartedModule } from 'src/app/shared/get-started';
import { MaterialDesignModule } from 'src/app/shared/material-design';
import { SharedComponentsModule } from 'src/app/shared/shared-components';
import { PreviousAppointmentComponent } from './previous-appointment/previous-appointment.component';
import { UpcomingAppointmentComponent } from './upcoming-appointment/upcoming-appointment.component';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [AppointmentComponent,
    PreviousAppointmentComponent ,
    AppointComponent,
    UpcomingAppointmentComponent,
  ViewHistoryComponent],
  imports: [
    CommonModule,
    IonicModule,
    AppointmentRoutingModule,
    LayoutModule,
    MaterialDesignModule,
    FlexLayoutModule,
    GetStartedModule,
    SharedComponentsModule,
    ReactiveFormsModule,

  ],
})
export class AppointmentModule {}
