import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject, EMPTY } from 'rxjs';
import { switchMap, takeUntil, catchError } from 'rxjs/operators';
import { AppointmentTypes, ErrorTypes, SuccessTypes } from 'src/app/common/constants';
import { Appointment, DialogBox, User } from 'src/app/common/models';
import { AuthService, AppointmentService, SharedService, HttpService, SnackBarService } from 'src/app/core/services';
import { DialogBoxComponent } from 'src/app/shared/dialog-box';
import { GetStartedComponent } from 'src/app/shared/get-started';

@Component({
  selector: 'app-upcoming-appointment',
  templateUrl: './upcoming-appointment.component.html',
  styleUrls: ['./upcoming-appointment.component.scss'],
})
export class UpcomingAppointmentComponent implements OnInit {
  allAppointments: Appointment[] = [];
  dispatchView = false;
  isPackageSubscribed = false;
  me: User;
  nextAppointment: Appointment;
  previousAppointments: Appointment[] = [];
  upcomingAppointments: Appointment[] = [];

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    private _aps: AppointmentService,
    private _shs: SharedService,
    private _hs: HttpService,
    private _sb: SnackBarService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.isPackageSubscribed = this.me.flags.isPackageSubscribed;

          return this._aps.getAppointmentsByUser(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        takeUntil(this._notifier$),
        catchError(() => EMPTY)
      )
      .subscribe((appts: Appointment[]) => {
        this.allAppointments = appts;

        this.setPreviousAppointments();
        this.setUpcomingAppointments();

        if (this.upcomingAppointments.length > 0) {
          [this.nextAppointment] = this.upcomingAppointments;
        } else {
          this.nextAppointment = null;
        }

        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  setPreviousAppointments(): void {
    this.previousAppointments.length = 0;
    this.previousAppointments = this.allAppointments
      .filter((item) => new Date(item.sessionStart) < new Date())
      .sort((a, b) => {
        return a.sessionStart < b.sessionStart ? 1 : -1;
      });
  }

  setUpcomingAppointments(): void {
    this.upcomingAppointments.length = 0;
    this.upcomingAppointments = this.allAppointments
      .filter((item) => new Date(item.sessionStart) > new Date())
      .sort((a, b) => {
        return a.sessionStart > b.sessionStart ? 1 : -1;
      });
  }

  showDialog(): void {
    this._dialog.open(GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: { step: this.me.onboardingStep || 0 }
    });
  }

  onCancelClick(appt: Appointment): Promise<boolean> {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message = 'Are you sure you want to cancel this appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    return new Promise((resolve) => {
      dialogRef.afterClosed().subscribe(async (res: any) => {
        if (!res) {
          resolve(false);
          return;
        }

        const body = {
          userUID: this.me.uid,
          appointmentKey: appt.key
        };

        let h$;
        if (appt.type === AppointmentTypes.RECALL) {
          h$ = this._hs.cancelRecallAppointment(body);
        } else {
          h$ = this._hs.cancelAdvancedTestingAppointment(body);
        }

        h$.subscribe(
          (apiRes) => {
            if (!apiRes.status) {
              this._sb.openErrorSnackBar(
                res.error.description || ErrorTypes.SYSTEM
              );
              resolve(false);
              return;
            }

            this._sb.openSuccessSnackBar(SuccessTypes.APPT_CANCELED);
            resolve(true);
          },
          () => {
            this._sb.openErrorSnackBar(ErrorTypes.SYSTEM);
            resolve(false);
          }
        );
      });
    });
  }

}
