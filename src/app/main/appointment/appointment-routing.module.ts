import { ViewHistoryComponent } from './view-history/view-history.component';
import { AppointComponent } from './appoint/appoint.component';
import { PreviousAppointmentComponent } from './previous-appointment/previous-appointment.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppointmentComponent } from './appointment.component';
import { UpcomingAppointmentComponent } from './upcoming-appointment/upcoming-appointment.component';

const routes: Routes = [
  {
    path: '',
    component: AppointmentComponent,
    children: [
      {
        path:'previous',
        component:PreviousAppointmentComponent
      },
      {
        path: 'upcoming',
        component:UpcomingAppointmentComponent
      },
      {
        path: '',
        component:AppointComponent
      },
      {
        path: 'history',
        component:ViewHistoryComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppointmentRoutingModule {}
