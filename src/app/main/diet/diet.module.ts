import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DietComponent } from './diet.component';
import { DietRoutingModule } from './diet-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GetStartedModule } from 'src/app/shared/get-started';
import { MaterialDesignModule } from 'src/app/shared/material-design';
import { SharedComponentsModule } from 'src/app/shared/shared-components';
import { UploadFileModule } from 'src/app/shared/upload-file';
import { LayoutModule } from 'src/app/shared/layout/layout.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [DietComponent],
  imports: [
    CommonModule,
    IonicModule,
    DietRoutingModule,
    LayoutModule,
    GetStartedModule,
    MaterialDesignModule,
    FlexLayoutModule,
    UploadFileModule,
    PdfViewerModule,
    SharedComponentsModule
  ],
})
export class DietModule {}
