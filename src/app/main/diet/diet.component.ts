import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable, Subject, EMPTY } from 'rxjs';
import { takeUntil, switchMap, catchError } from 'rxjs/operators';
import * as moment from 'moment';
import {  Session, User, UserAssessment } from 'src/app/common/models';
import { AuthService, SessionnService, SnackBarService, StaticDataService, StorageService, UserService, UtilitiesService } from 'src/app/core/services';
import { MatDialog } from '@angular/material/dialog';
import { ErrorMessages, SuccessTypes } from 'src/app/common/constants';
import { GetStartedComponent } from 'src/app/shared/get-started/get-started.component';


@Component({
  selector: 'app-diet',
  templateUrl: './diet.component.html',
  styleUrls: ['./diet.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class DietComponent implements OnInit, OnDestroy {
  currentSession: Session;
  dispatchDietFileView = true;
  dispatchView = false;
  downloadFileName = '';
  fileURL = '';
  futureSessions: Session[];
  hasClinicalAssessments = false;
  isBehindSchedule = false;
  isDietFileAvailable = false;
  isImg = false;
  isPackageSubscribed = false;
  isWaitingForFirstDiet = false;
  me: User;
  previewDownloadPlaceholder = 'Select a diet to load details';
  previewDownloadTitle = '';
  previousSessions: Session[];
  showDiet = false;
  weeksLeft = 0;
  downloading = false;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  isFirstTabVisible: boolean;
  isSecondTabVisible: boolean;
  isThirdTabVisible: boolean;

  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    private _sss: SessionnService,
    private _sb: SnackBarService,
    private _us: UserService,
    private _uts: UtilitiesService,
    private _sts: StorageService,
    private _sds: StaticDataService,
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.isPackageSubscribed = this.me.flags.isPackageSubscribed;

          return this._us.getActiveUserAssessments(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        switchMap((res: UserAssessment[]) => {
          this.hasClinicalAssessments = Boolean(
            res.length > 0 && res[0].tests.length > 0
          );

          return this._sss.getAllSessionsByUser(this.me.uid);
        }),
        switchMap((allSessions: Session[]) => {
          this.setPreviousSessions(allSessions);

          return this._sss.getSessionsByUser(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe((sessions: Session[]) => {
        this.setCurrentSession(sessions);
        this.setFutureSessions(sessions);

        const now = moment();
        const then = moment(this.me.subscriptionDuration.end);
        const weeks = moment.duration(then.diff(now)).asWeeks();
        this.weeksLeft = Math.floor(weeks);

        this.isWaitingForFirstDiet = !this.currentSession;

        if (this.hasClinicalAssessments) {
          this._sb.openInfoSnackBar(
            'Your Clinical Assessments reports are due. Kindly upload all your Clinical Assessments reports at the earliest.'
          );
        }

        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  onTabPanelClick(event, tab) {
    this.isFirstTabVisible= (event.index === 0) ? true : true;
    this.isSecondTabVisible= (event.index === 1) ? true : false;
    this.isThirdTabVisible= (event.index === 2) ? true : false;
 }

  DownloadFile(URL:any, Name:any): void {
    this.downloading = true;
    console.log(URL, Name);
    this._sts.download(URL, Name);
    setTimeout(() => {
      this.downloading = false;
      this._sb.openSnackBar(SuccessTypes.FILE_DOWNLAOD);
    }, 2000);
  }

  // onDownloadFile(file: Filee2): void {
  //   console.log(file)
  //   this.downloading = true;
  //   this._sts.download(file.fileURL, file.fileName);
  //   // this._demo.downloadFile(file.fileURL, file.fileName);
  //   setTimeout(() => {
  //     this.downloading = false;
  //     this._sb.openSnackBar(SuccessTypes.FILE_DOWNLAOD);
  //   }, 2000);
  // }
  
  setCurrentSession(sessionsArr: Session[]): void {
    const a = sessionsArr.filter(
      (item) => item.isCurrent && item.sessionNumber
    );

    if (a.length === 0) {
      return;
    }

    if (a.length !== 1) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
      return;
    }

    [this.currentSession] = a;

    this.isBehindSchedule = false;
    if (
      this.currentSession.sessionNumber > 0 &&
      this.currentSession.startDate
    ) {
      const number = this.currentSession.sessionNumber + 1;

      if (number < this.me.subscribedPackage.numberOfSessions) {
        const [session] = sessionsArr.filter(
          (item) => item.sessionNumber === number
        );

        this.isBehindSchedule =
          !session.fileURL &&
          session.idealDueDate &&
          session.idealDueDate.valueOf() < moment.now();
      }
    }
  }

  setPreviousSessions(sessionsArr: Session[]): void {
    const sessions = sessionsArr.filter(
      (item) => item.isCompleted && item.sessionNumber > 0
    );
    this.previousSessions = sessions;
  }

  setFutureSessions(sessionsArr: Session[]): void {
    const sessions = sessionsArr
      .filter(
        (item) => !item.isCurrent && !item.isCompleted && item.sessionNumber > 0
      )
      .sort((a, b) => {
        return a.sessionNumber > b.sessionNumber ? 1 : -1;
      });
    this.futureSessions = sessions;
  }

  showDialog(): void {
    this._dialog.open(GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: { step: this.me.onboardingStep || 0 }
    });
  }

  onShowDiet(session: Session): void {
    this.dispatchDietFileView = false;

    this.previewDownloadTitle = `Diet # ${session.sessionNumber}`;
    this.fileURL = session.fileURL;
    this.isImg = session.fileType.includes('image');
    // this.downloadFileName = session.fileURL
    //   ? `MEALpyramid__Diet-${session.sessionNumber}__${this._uts.sanitize(
    //       this.me.name
    //     )}`
    //   : '';
    this.downloadFileName = session.fileName;

    if (!session.fileURL) {
      this.previewDownloadPlaceholder = 'The Diet file is not yet uploaded';
    }
console.log("file")
   
    setTimeout(() => {
      this.dispatchDietFileView = true;
     
    }, 250);
  
  }
}
