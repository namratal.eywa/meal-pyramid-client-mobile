
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { UserRecommendations } from 'src/app/common/constants';
import { User, UserGeneralRecommendation } from 'src/app/common/models';
import { AuthService, UserService } from 'src/app/core/services';
import { GetStartedComponent } from 'src/app/shared/get-started';
import { DownloadFileService } from './download.services';


@Component({
  selector: 'app-recommendation',
  templateUrl: './recommendation.component.html',
  styleUrls: ['./recommendation.component.scss']
})
export class RecommendationComponent implements OnInit {
  dispatchView = false;
  isPackageSubscribed = false;
  me: User;
  userGeneralRecommendation: UserGeneralRecommendation;
  userRecommendations: typeof UserRecommendations = UserRecommendations;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    private _us: UserService,
    private _download:DownloadFileService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          return this._us.getUserGeneralRecommendations(
            this.me.uid,
            this.me.membershipKey
          );
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((recom: UserGeneralRecommendation[]) => {
        if (recom.length === 0) {
          this.userGeneralRecommendation = new UserGeneralRecommendation();
        } else if (recom.length === 1) {
          [this.userGeneralRecommendation] = recom;
        } else {
          [this.userGeneralRecommendation] = recom;
        }

        this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
        this.dispatchView = true;
      });
  }

  showDialog(): void {
    this._dialog.open(GetStartedComponent, {
      width: '60vw',
      height: '90vh',
      data: { step: this.me.onboardingStep || 0 }
    });
  }

  Demofile()
  {
  this._download.downloadFile();
  }
}
