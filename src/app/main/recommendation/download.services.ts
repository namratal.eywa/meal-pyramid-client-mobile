import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { MenuController, LoadingController } from '@ionic/angular';
import { AuthService } from '../../core/services/auth.service';
import { SessionnService } from '../../core/services/sessionn.service';
import { SharedService } from '../../core/services/shared.service';
import { SnackBarService } from '../../core/services/snack-bar.service';
import { UtilitiesService } from '../../core/services/utilities.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Injectable({
  providedIn: 'root'
})
export class DownloadFileService {
  public file:any = {};
  fileName: any="demo.jpeg";
  constructor(
    private _afStore: AngularFirestore,
    private _uts: UtilitiesService,
    private _as: AuthService,
    private _router: Router,
    private _dialog: MatDialog,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private _ss: SessionnService,
    private _shs: SharedService,
    private _sb: SnackBarService,
    private transfer: FileTransfer,
    private androidPermissions: AndroidPermissions,
    
  ){ }
   FileTransfer: FileTransferObject = this.transfer.create();

  // case1()
  // { }

  photoviewer()
  {
    
  }
  async downloadFile() {
    // const fileTransfer: FileTransferObject = this.transfer.create();
    await this.FileTransfer.download("https://cdn.pixabay.com/photo/2017/01/06/23/21/soap-bubble-1959327_960_720.jpg", this.file.externalRootDirectory + 
    '/Download/' + "soap-bubble-1959327_960_720.jpg");
  }
  
  getPermission() {
    this.androidPermissions.hasPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      .then(status => {
        if (status.hasPermission) {
          this.downloadFile();
        } 
        else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
            .then(status => {
              if(status.hasPermission) {
                this.downloadFile();
              }
            });
        }
      });
  }
}