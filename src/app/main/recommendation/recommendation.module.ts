import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { RecommendationComponent } from './recommendation.component';
import { RecommendationRoutingModule } from './recommendation-routing.module';
import { LayoutModule } from 'src/app/shared/layout';
import { GetStartedModule } from 'src/app/shared/get-started';
import { MaterialDesignModule } from 'src/app/shared/material-design';
import { SharedComponentsModule } from 'src/app/shared/shared-components';
// import { DownloadFileService } from './download.services';

@NgModule({
  declarations: [RecommendationComponent],
  imports: [
    CommonModule,
    IonicModule,
    LayoutModule,
    GetStartedModule,
    MaterialDesignModule,
    SharedComponentsModule,
    FlexLayoutModule,
    RecommendationRoutingModule
  ],
  // providers:[
  //   DownloadFileService
  // ]
})
export class RecommendationModule {}
