import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject,throwError } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { ErrorMessages } from 'src/app/common/constants';
import { User , Notification} from 'src/app/common/models';
import { AuthService, UserService, SnackBarService, StaticDataService } from 'src/app/core/services';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  areNotificationsAvailable = false;
  dispatchView = false;
  me: User;
  notifications: Notification[] = [];
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _us: UserService,
    private _sb: SnackBarService,
    private _sts: StaticDataService,
    private router:Router
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        takeUntil(this._notifier$),
        switchMap((me: User) => {
          this.me = me;

          return this._us.getUserNotifications(this.me.uid);
        })
      )
      .subscribe((notifs: Notification[]) => {
        this.notifications = notifs;

        if (this.notifications.length > 0) {
          this.areNotificationsAvailable = true;
        } else {
          this.areNotificationsAvailable = false;
        }

        this.dispatchView = true;
        return throwError(Error)
      });

  
  }

  async onClick(notif: Notification): Promise<void> {
    notif.isRead = true;
    notif.isShown = true;
    notif.isNew = false;

    try {
      await this._sts.setData(
        this.me.uid,
        'userNotifications',
        notif,
        false,
        ['notifDate'],
        true,
        notif.key
      );
    } catch (error) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
    }
  }
}
