import { HomeModule } from 'src/app/home';
import { PagesComponent } from './pages.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { NotificationComponent } from './notification/notification.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { IonicModule } from '@ionic/angular';
import { LightboxModule } from 'ngx-lightbox';
import { BuyAdvancedTestingModule } from '../shared/buy-advanced-testing';
import { GetStartedModule } from '../shared/get-started';
import { MaterialDesignModule } from '../shared/material-design';
import { SharedComponentsModule } from '../shared/shared-components';
import { LayoutModule } from '../shared/layout/layout.module';


@NgModule({
  declarations: [PagesComponent, NotificationComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    IonicModule,
    LayoutModule,
    HomeModule,
    GetStartedModule,
    MaterialDesignModule,
    FlexLayoutModule,
    BuyAdvancedTestingModule,
    SharedComponentsModule,
    LightboxModule
  ]
})
export class PagesModule { }
