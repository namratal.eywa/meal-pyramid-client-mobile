import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertController, MenuController } from '@ionic/angular';
import { combineLatest, EMPTY, Observable, Subject } from 'rxjs';
import { takeUntil, take, switchMap, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ErrorMessages, AppointmentTypes, RouteIDs, SuccessMessages } from '../common/constants';
import { UserAppointment, User, Appointment, DialogBox, Session } from '../common/models';
import { AuthService, SharedService, SnackBarService, HttpService, SessionnService, UserService, UtilitiesService } from '../core/services';
import { BuyAdvancedTestingComponent } from '../shared/buy-advanced-testing';
import { DialogBoxComponent } from '../shared/dialog-box';
import { GetStartedComponent } from '../shared/get-started';



// ToDo: Verify Component & Template

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  advancedRecommendedPackage = '';
  advancedTestingAppt: UserAppointment;
  dispatchView = false;
  hasIntroVideoEnded = false;
  hasWelcomeVideoEnded = false;
  heroImage = environment.heroSection.kinita;
  introVideoURL = environment.video.intro;
  me: User;
  userHasSubcribedPackage: boolean;
  path: string;
  loading: boolean;
  nextAppointment: Appointment;
  nextAppointmentDueDate: Date;
  orientationAppointment: UserAppointment;
  showAdvancedTesting = false;
  showClinicalAssessmentUpload = false;
  showDidNotAttendOrientation = false;
  showFirstDiet = false;
  showHasActiveAdvancedTestingAppt = false;
  showHasPurchasedAdvancedTests = false;
  showHasRecommendedAdvancedTests = false;
  showHasSubscribed = false;
  showLastDiet = false;
  showNewSubscriber = false;
  showNextAppointmentAvailable = false;
  showOnboardingIncomplete = false;
  showRecommendations = false;
  showScheduleNextAppointment = false;
  showVideo = true;
  showWaitingOnATReports = false;
  showWaitingOnOrientation = false;
  showWaitingOnRecommendations = false;
  userAppointments: UserAppointment[] = [];
  // videoJSConfigObj = {
  //   preload: 'metadata',
  //   controls: false,
  //   autoplay: 'any',
  //   overrideNative: true,
  //   techOrder: ['html5', 'flash'],
  //   html5: {
  //     nativeVideoTracks: false,
  //     nativeAudioTracks: false,
  //     nativeTextTracks: false,
  //     hls: {
  //       withCredentials: false,
  //       overrideNative: true,
  //       debug: true
  //     }
  //   }
  // };

  welcomeVideoURL = environment.video.welcome;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  isPackageSubscribed: boolean;
  subscribedPackage: any;


  constructor(
    private _as: AuthService,
    private _dialog: MatDialog,
    private _shs: SharedService,
    private _sb: SnackBarService,
    private _hs: HttpService,
    private _router: Router,
    private _ss: SessionnService,
    private _us: UserService,
    private _uts: UtilitiesService,
    public menuCtrl: MenuController,
    private alertController: AlertController,
  ) {
    this.menuCtrl.swipeGesture(true);
  }

  ngOnInit(): void {
    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;
          this.userHasSubcribedPackage = this.me.flags.isPackageSubscribed;
          this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
          this.subscribedPackage = this.me.subscribedPackage;
          this.dispatchView = true;
          console.log(this.me); 
          return combineLatest([
            this._us.getActiveUserAppointments(
              this.me.uid,
              this.me.membershipKey
            ),
            this._ss.getSessionsByUser(this.me.uid, this.me.membershipKey)
          ]);
        }),
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        })
      )
      .subscribe(async (arr: [UserAppointment[], Session[]]) => {
        this.setAllViewFlagsFalse();
        await this._uts.sleep(500);

        const [appointments, sessions] = arr;
        this.userAppointments = appointments;

        if (this.me.flags.isPackageSubscribed) {
          this.showHasSubscribed = true;

          const recallAppts = appointments
            .filter((i) => i.appointment.type === AppointmentTypes.RECALL)
            .sort((a, b) => {
              return a.appointment.sessionStart > b.appointment.sessionStart
                ? 1
                : -1;
            });

          const [currentSess] = sessions.filter((i) => i.isCurrent);
          const [nextSession] = currentSess.nextSessionKey
            ? sessions.filter((i) => i.key === currentSess.nextSessionKey)
            : [];
          const [lastSession] = sessions.filter(
            (i) => i.sessionNumber === sessions.length - 1
          );

          if (recallAppts.length === 0) {
            this.showNewSubscriber = currentSess.sessionNumber === 0;

            if (nextSession) {
              this.showScheduleNextAppointment = true;
              this.nextAppointmentDueDate = nextSession.dueDate;
            }
          } else {
            this.showNextAppointmentAvailable = true;
            this.nextAppointment = recallAppts[0].appointment;
          }

          if (currentSess.sessionNumber === 1 && currentSess.fileURL) {
            this.showFirstDiet = true;
          } else if (currentSess.sessionNumber === lastSession.sessionNumber) {
            this.showLastDiet = true;
          }

          this.dispatchView = true;
          this.checkForSnackBar();
        } else if (this.me.flags.isPackageRecommended) {
          this.showRecommendations = true;
          this.dispatchView = true;
          this.checkForSnackBar();
        } else if (this.me.flags.isOrientationCompleted) {
          this.showWaitingOnRecommendations = true;
          this.dispatchView = true;
          this.checkForSnackBar();
        } else if (this.me.flags.isOrientationScheduled) {
          this.showWaitingOnOrientation = true;

          const appts = appointments.filter(
            (i) => i.appointment.type === AppointmentTypes.ORIENTATION
          );
          if (appts.length !== 1) {
            this._router.navigate([RouteIDs.ERROR]);
            return;
          }
          [this.orientationAppointment] = appts;

          this.showDidNotAttendOrientation = Boolean(
            this.orientationAppointment.appointment.sessionStart < new Date()
          );

          this.dispatchView = true;
          this.checkForSnackBar();


        } else {
          this.showOnboardingIncomplete = true;

          this.dispatchView = true;
          this.checkForSnackBar();
        }

        this._us
          .getActiveUserAssessments(this.me.uid, this.me.membershipKey)
          .pipe(
            takeUntil(this._notifier$),
            catchError(() => {
              this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
              this.dispatchView = true;
              return EMPTY;
            })
          )
          .subscribe((res) => {
            if (res.length) {
              if (res.length !== 1) {
                this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
                this.dispatchView = true;
                return;
              }

              this.showClinicalAssessmentUpload = Boolean(
                res[0].tests && res[0].tests.length
              );
            } else {
              this.showClinicalAssessmentUpload = false;
            }
          });

        this._us
          .getActiveUserAdvancedTests(this.me.uid, this.me.membershipKey)
          .pipe(
            takeUntil(this._notifier$),
            catchError(() => {
              this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
              return EMPTY;
            })
          )
          .subscribe((res) => {
            if (res.length === 0) {
              this.showAdvancedTesting = false;
            } else if (res.length === 1) {
              const [userAdvancedTest] = res;

              this.showAdvancedTesting =
                Boolean(userAdvancedTest.recommendedTests.length > 0) ||
                Boolean(userAdvancedTest.purchasedTests.length > 0);

              if (!this.showAdvancedTesting) {
                return;
              }

              if (this.me.buyAdvancedTestingStep === 0) {
                this.advancedRecommendedPackage =
                  userAdvancedTest.recommendedPackage.name;
                this.showHasRecommendedAdvancedTests = true;
              } else if (this.me.buyAdvancedTestingStep === 1) {
                this.showHasPurchasedAdvancedTests = true;
              } else if (this.me.buyAdvancedTestingStep === 2) {
                [this.advancedTestingAppt] = this.userAppointments.filter(
                  (appt) =>
                    appt.appointment.type === AppointmentTypes.ADVANCED_TESTING
                );

                this.showHasActiveAdvancedTestingAppt = Boolean(
                  this.advancedTestingAppt
                );
                this.showWaitingOnATReports = !this.advancedTestingAppt;
              }
            }
          });
      });
  }

  showDialog(comp: any, step = 0): void {
    this._dialog.open(comp, {
      width: '60vw',
      height: '90vh',
      data: { step }
    });
  }

  onIntroVideoEnd(): void {
    this.hasIntroVideoEnded = true;
  }

  onWelcomeVideoEnd(): void {
    this.hasWelcomeVideoEnded = true;
  }

  onCancelOrientationClick(): Promise<boolean> {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message =
      'Are you sure you want to cancel your Orientation appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe(async (res: any) => {
        if (!res) {
          resolve(false);
          return;
        }

        this.dispatchView = false;

        this._us
          .getActiveUserAppointmentsByType(
            this.me.uid,
            this.me.membershipKey,
            AppointmentTypes.ORIENTATION
          )
          .pipe(
            take(1),
            switchMap((appts: UserAppointment[]) => {
              if (appts.length !== 1) {
                throw new Error();
              }

              const [appt] = appts;

              const body = {
                appointmentKey: appt.appointment.key,
                userUID: this.me.uid
              };
              return this._hs.cancelOrientationAppointment(body);
            }),
            catchError(() => {
              this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
              this.dispatchView = true;
              reject();
              return EMPTY;
            })
          )
          .subscribe((res: any) => {
            if (!res.status) {
              this._sb.openErrorSnackBar(
                res.error.description || ErrorMessages.SYSTEM
              );
              this.dispatchView = true;
              reject();
              return;
            }

            this._sb.openSuccessSnackBar(SuccessMessages.APPT_CANCELED);
            this.dispatchView = true;
            resolve(true);
          });
      });
    });
  }

  onCancelATApptClick(): Promise<boolean> {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'help_center';
    dialogBox.title = 'Confirmation';
    dialogBox.message =
      'Are you sure you want to cancel your Advanced Testing appointment?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);

    return new Promise((resolve, reject) => {
      dialogRef.afterClosed().subscribe(async (res: any) => {
        if (!res) {
          resolve(false);
          return;
        }

        this.dispatchView = false;

        this._us
          .getActiveUserAppointmentsByType(
            this.me.uid,
            this.me.membershipKey,
            AppointmentTypes.ADVANCED_TESTING
          )
          .pipe(
            take(1),
            switchMap((appts: UserAppointment[]) => {
              if (appts.length !== 1) {
                throw new Error();
              }

              const [appt] = appts;

              const body = {
                appointmentKey: appt.appointment.key,
                userUID: this.me.uid
              };
              return this._hs.cancelAdvancedTestingAppointment(body);
            }),
            catchError(() => {
              this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
              this.dispatchView = true;
              reject();
              return EMPTY;
            })
          )
          .subscribe((res: any) => {
            if (!res.status) {
              this._sb.openErrorSnackBar(
                res.error.description || ErrorMessages.SYSTEM
              );
              this.dispatchView = true;
              reject();
              return;
            }

            this._sb.openSuccessSnackBar(SuccessMessages.APPT_CANCELED);
            this.dispatchView = true;
            resolve(true);
          });
      });
    });
  }

  setAllViewFlagsFalse(): void {
    this.showDidNotAttendOrientation = false;
    this.showOnboardingIncomplete = false;
    this.showRecommendations = false;
    this.showVideo = false;
    this.showWaitingOnOrientation = false;
    this.showWaitingOnRecommendations = false;
    this.showAdvancedTesting = false;
    this.showHasSubscribed = false;
    this.showNewSubscriber = false;
    this.showFirstDiet = false;
    this.showLastDiet = false;
    this.showNextAppointmentAvailable = false;
    this.showScheduleNextAppointment = false;
    this.showHasRecommendedAdvancedTests = false;
    this.showHasPurchasedAdvancedTests = false;
    this.showHasActiveAdvancedTestingAppt = false;
    this.showWaitingOnATReports = false;
    delete this.nextAppointment;
    delete this.nextAppointmentDueDate;
    delete this.advancedTestingAppt;
    this.advancedRecommendedPackage = '';
  }
  goto(path): void {
    this.path = path;
    this._router.navigate([path]);
  }
  onResumeOnboardingClick(): void {
    this.showDialog(GetStartedComponent, this.me.onboardingStep);
  }

  onBuyAdvancedTestingClick(): void {
    this.showDialog(
      BuyAdvancedTestingComponent,
      this.me.buyAdvancedTestingStep
    );
  }

  onUploadClick(): void {
    this._shs.setHealthHistoryTabSelected(1);
    this._router.navigate([RouteIDs.HEALTH_HISTORY]);
  }

  onGoToProgress(n: number): void {
    this._shs.setProgressTabSelected(n);
    switch (n) {
      case 1:
        this._router.navigate(['/tab/'+RouteIDs.PROGRESS]);
        break;
      case 2:
        this._router.navigate(['/tab/'+RouteIDs.PROGRESS]);
        break;
      case 3:
        this._router.navigate(['/tab/'+RouteIDs.PROGRESS]);
        break;
      case 4:
        this._router.navigate([RouteIDs.APPOINTMENTS]);
        break;
        default:
          break;
    }
    }

    checkForSnackBar(): void {
      this._shs
        .getHomeSnackBar()
        .pipe(take(1))
        .subscribe(async (obj: any) => {
          if (obj && obj.type && obj.message) {
            switch (obj.type) {
              case 'Success':
                this._sb.openSuccessSnackBar(obj.message);
                break;

              case 'Error':
                this._sb.openErrorSnackBar(obj.message);
                break;

              default:
                break;
            }

            this._shs.setHomeSnackBar(null);
          }
        });
    }
}
