import { FooterComponent } from '../shared/shared-components/footer/footer.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FeedComponent } from './feed/feed.component';
import { LayoutModule } from '@angular/cdk/layout';
import { BuyAdvancedTestingModule } from '../shared/buy-advanced-testing';
import { GetStartedModule } from '../shared/get-started';
import { MaterialDesignModule } from '../shared/material-design';
import { SharedComponentsModule } from '../shared/shared-components';
import { LightboxModule } from 'ngx-lightbox';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    HomeComponent,
    FeedComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    HomeRoutingModule,
    LayoutModule,
    GetStartedModule,
    MaterialDesignModule,
    FlexLayoutModule,
    BuyAdvancedTestingModule,
    SharedComponentsModule,
    LightboxModule
  ],
  exports: [
    FooterComponent,
    HomeComponent
  ]
})
export class HomeModule { }
