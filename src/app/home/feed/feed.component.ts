import { Component, OnDestroy, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { Lightbox, LightboxConfig } from 'ngx-lightbox';
import { LightboxConfigs } from 'src/app/common/constants';
import { CommunityFeed, User } from 'src/app/common/models';
import { AuthService, StaticDataService } from 'src/app/core/services';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit, OnDestroy {
  areFeedsAvailable = false;
  feeds: CommunityFeed[];
  lightboxAlbum = [];
  me: User;
  now: moment.Moment;

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();

  constructor(
    private _as: AuthService,
    private _sds: StaticDataService,
    private _lightbox: Lightbox,
    private _lightboxConfig: LightboxConfig
  ) {}

  ngOnInit(): void {
    this.now = moment();

    this._me$
      .pipe(
        switchMap((me: User) => {
          this.me = me;

          return this._sds.getCommunityFeed();
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((data: CommunityFeed[]) => {
        this.feeds = data;

        if (this.feeds.length > 0) {
          this.areFeedsAvailable = true;
        }
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  lightboxInit(): void {
    Object.assign(this._lightboxConfig, LightboxConfigs);
  }

  openLightbox(src: string): void {
    this.lightboxAlbum = [];
    this.lightboxAlbum.push({
      src
    });
    this._lightbox.open(this.lightboxAlbum, 0);
  }
}
