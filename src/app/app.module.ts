import { FooterBarPageModule } from './footer-bar/footer-bar.module';
import { HomeModule } from './home/home.module';
import { PagesModule } from './pages/pages.module';
import { AuthModule } from './auth/auth.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedComponentsModule } from './shared/shared-components/shared-components.module';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './shared/layout';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MaterialDesignModule } from './shared/material-design';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Network } from '@ionic-native/network/ngx'; //ngx
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer, } from '@ionic-native/file-transfer/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { Downloader } from '@ionic-native/downloader/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, 
    IonicModule.forRoot(),
    AppRoutingModule,
    CoreModule,
    LayoutModule,
    AuthModule,
    PagesModule,
    HttpClientModule,
    MaterialDesignModule,
    SharedComponentsModule,
    HomeModule,
    BrowserAnimationsModule],
  providers: [SplashScreen,
    StatusBar, 
    File,
    FileTransfer,
    FileOpener,
    Network,
    Camera,
    Crop,
    Base64,
    HTTP,
    Downloader,
    AndroidPermissions,
   { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },],
  bootstrap: [AppComponent],
})
export class AppModule { }
