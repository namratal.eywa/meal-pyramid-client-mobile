import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteIDs } from '../common/constants';
import { AuthGuard } from '../core/guards';
import { FooterBarPage } from './footer-bar.page';

const routes: Routes = [
  {
    path: '',
    component: FooterBarPage,
    children: [
      {
        path: '',
        redirectTo: RouteIDs.HOME,
        pathMatch: 'full'
      },
      {
        path: RouteIDs.HOME,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        loadChildren: () => import('../home/home.module').then(m => m.HomeModule)
      },
   {
    path: 'p',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () => import('../pages/pages.module').then( m => m.PagesModule)
  },
  {
    path: RouteIDs.APPOINTMENTS,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () =>
      import('../main/appointment/appointment.module').then((m) => m.AppointmentModule)
  },
  {
    path: RouteIDs.DIETS,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () => import('../main/diet/diet.module').then((m) => m.DietModule)
  },
  {
    path: RouteIDs.PROFILE,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () => import('../profile/profile.module').then((m) => m.ProfileModule)
  },
  {
    path: RouteIDs.HEALTH_HISTORY,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () =>
      import('../main/health-history/health-history.module').then((m) => m.HealthHistoryModule)
  },
  {
    path: RouteIDs.PROGRESS,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () => import('../main/progress/progress.module').then((m) => m.ProgressModule)
  },
  {
    path: RouteIDs.RECOMMENDATIONS,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () =>
      import('../main/recommendation/recommendation.module').then((m) => m.RecommendationModule)
  },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FooterBarPageRoutingModule { }
