import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Subject, EMPTY } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';
import { RouteIDs } from '../common/constants';
import { User } from '../common/models';
import { AuthService } from '../core/services';

@Component({
  selector: 'app-footer-bar',
  templateUrl: './footer-bar.page.html',
  styleUrls: ['./footer-bar.page.scss'],
})
export class FooterBarPage implements OnInit , OnDestroy {
  chatURL = '';
  dispatchView = false;
  isPackageSubscribed = false;
  loading = false;
  me: User;
  path: string;
  routeIDs: typeof RouteIDs = RouteIDs;
  subscribedPackage: any;
  closed$ = new Subject<any>();
  showTabs = true; // <-- show tabs by default
  private _notifier$: Subject<void> = new Subject();
  // private _me$: Observable<User> = this._as.getMe();
  constructor(   private _router: Router, private navCtrl: NavController,  private _as: AuthService) { }

  ngOnInit() {
    
    this._as.getMe().pipe(catchError((error) => {
      return EMPTY;
    }),
    takeUntil(this._notifier$)).subscribe((user: User) => {
      this.me = user;
      console.log(this.me );
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;
      this.dispatchView = true;
    });

  }

  goto(path): void {
    this.path = path;
    this._router.navigate(['/tab/'+path]);
    console.log(this.path)
  }
  ngOnDestroy() {
    this.closed$.next();
    this._notifier$.next(); 
    this._notifier$.complete(); // <-- close subscription when component is destroyed
  }

}
