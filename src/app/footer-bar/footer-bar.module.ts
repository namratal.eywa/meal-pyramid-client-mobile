import { PagesModule } from './../pages/pages.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FooterBarPageRoutingModule } from './footer-bar-routing.module';

import { FooterBarPage } from './footer-bar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FooterBarPageRoutingModule,
    PagesModule
  ],
  declarations: [FooterBarPage],
  exports:[FooterBarPage]
})
export class FooterBarPageModule {}
