import { HealthHistoryModule } from './main/health-history/health-history.module';
import { HomeModule } from './home/home.module';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { RouteIDs } from './common/constants';
import { AuthGuard } from './core/guards';

const routes: Routes = [
  {
    path: '',
    redirectTo: "tab",
    pathMatch: 'full'
  },
  // {
  //   path: '',
  //   loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule)
  // },
  // {
  //   path: 'p',
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () => import('./pages/pages.module').then( m => m.PagesModule)
  // },
  // {
  //   path: RouteIDs.APPOINTMENTS,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () =>
  //     import('../app/main/appointment/appointment.module').then((m) => m.AppointmentModule)
  // },
  // {
  //   path: RouteIDs.DIETS,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () => import('../app/main/diet/diet.module').then((m) => m.DietModule)
  // },
  // {
  //   path: RouteIDs.PROFILE,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () => import('./profile/profile.module').then((m) => m.ProfileModule)
  // },
  // {
  //   path: RouteIDs.HEALTH_HISTORY,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () =>
  //     import('../app/main/health-history/health-history.module').then((m) => m.HealthHistoryModule)
  // },
  // {
  //   path: RouteIDs.PROGRESS,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () => import('../app/main/progress/progress.module').then((m) => m.ProgressModule)
  // },
  // {
  //   path: RouteIDs.RECOMMENDATIONS,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () =>
  //     import('../app/main/recommendation/recommendation.module').then((m) => m.RecommendationModule)
  // },
  // {
  //   path: RouteIDs.HOME,
  //   canActivate: [AuthGuard],
  //   canActivateChild: [AuthGuard],
  //   loadChildren: () => import('./home/home.module').then( m => m.HomeModule)
  // },
  {
    path: "tab",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    loadChildren: () => import('./footer-bar/footer-bar.module').then( m => m.FooterBarPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
