import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import * as moment from 'moment';
import { Observable, Subject, EMPTY } from 'rxjs';
import { takeUntil, catchError } from 'rxjs/operators';
import { GenderTypes, ErrorMessages, SuccessMessages } from 'src/app/common/constants';
import { User } from 'src/app/common/models';
import { UserService, SnackBarService, AuthService, UtilitiesService } from 'src/app/core/services';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {

  dispatchView = false;
  dpChosenFileSize = 0;
  dpChosenInitialFileName = '';
  dpUploadedFileSize = 0;
  dpUploadedInitialFileName = '';
  dob!: Date;
  genderTypes: typeof GenderTypes = GenderTypes;
  isPackageSubscribed = false;
  loading = false;
  maxDate!: Date;
  me: User;
  personalDetailsFG: FormGroup;
  personalDetailsFGInitial: any;
  uploadCollectionName = 'userProfilePictureFiles';
  uploadFileNamePartial = 'MEALpyramid_DP';
  uploadFileNamePrefix = '';
  uploadDirPath = 'displayPictures';

  private _me$: Observable<User> = this._as.getMe();
  private _notifier$ = new Subject<any>();

  constructor(
    private _us: UserService,
    private _sb: SnackBarService,
    private _as: AuthService,
    private _fb: FormBuilder,
    private _uts: UtilitiesService
  ) {}

  ngOnInit(): void {
    this._me$
      .pipe(
        takeUntil(this._notifier$),
        catchError(() => {
          this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
          return EMPTY;
        }),
        takeUntil(this._notifier$)
      )
      .subscribe((me: User) => {
        this.me = me;
        this.isPackageSubscribed = this.me.flags.isPackageSubscribed;

        this.maxDate = moment().subtract(1, 'day').startOf('day').toDate();
        this.uploadFileNamePrefix = `${this.uploadFileNamePartial}_${this.me.name}`;

        this.createFG();
        this.personalDetailsFGInitial =
          this.personalDetailsFG.get('personalDetails').value;

        this.dispatchView = true;
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  // *
  // * FormGroup
  // *

  createFG(): void {
    this.personalDetailsFG = this._fb.group({
      name: [this.me.name],
      dOBSearchIndex: [this.me.dOBSearchIndex],
      searchArray: [this.me.searchArray],
      personalDetails: this._fb.group(this.me.personalDetails)
    });

    this.firstNameFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z0-9]{2,51}$/)
    ]);
    this.lastNameFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z0-9]{1,51}$/)
    ]);
    this.dateOfBirthFC.setValidators(Validators.required);
    this.genderFC.setValidators(Validators.required);
    this.addressLine1FC.setValidators([
      Validators.required,
      Validators.minLength(5)
    ]);
    this.cityFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z ]{2,101}$/)
    ]);
    this.stateFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z ]{2,101}$/)
    ]);
    this.countryFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z ]{2,101}$/)
    ]);
    this.pincodeFC.setValidators([
      Validators.required,
      Validators.pattern(/^[A-Za-z0-9-]{3,11}$/)
    ]);
    this.mobileFC.setValidators([
      Validators.required,
      Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/)
    ]);
    const dob = moment(this.dateOfBirthFC.value).toDate();
    this.dob = this._uts.isValidDate(dob) ? dob : null;
  }

  get firstNameFC(): FormControl {
    return this.personalDetailsFG.get(
      'personalDetails.firstName'
    ) as FormControl;
  }

  get lastNameFC(): FormControl {
    return this.personalDetailsFG.get(
      'personalDetails.lastName'
    ) as FormControl;
  }

  get nameFC(): FormControl {
    return this.personalDetailsFG.get('name') as FormControl;
  }

  get gender(): string {
    return this.personalDetailsFG.get('personalDetails.gender').value;
  }

  get genderFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.gender') as FormControl;
  }

  get dateOfBirthFC(): FormControl {
    return this.personalDetailsFG.get(
      'personalDetails.dateOfBirth'
    ) as FormControl;
  }

  get dOBSearchIndex(): FormControl {
    return this.personalDetailsFG.get('dOBSearchIndex') as FormControl;
  }

  get searchArrayFC(): FormControl {
    return this.personalDetailsFG.get('searchArray') as FormControl;
  }

  get photoURLFC(): FormControl {
    return this.personalDetailsFG.get(
      'personalDetails.photoURL'
    ) as FormControl;
  }

  get addressLine1FC(): FormControl {
    return this.personalDetailsFG.get(
      'personalDetails.addressLine1'
    ) as FormControl;
  }

  get cityFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.city') as FormControl;
  }

  get stateFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.state') as FormControl;
  }

  get pincodeFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.pincode') as FormControl;
  }

  get countryFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.country') as FormControl;
  }

  get mobileFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.mobile') as FormControl;
  }

  get emailFC(): FormControl {
    return this.personalDetailsFG.get('personalDetails.email') as FormControl;
  }

  isPersonalDetailsFGValid(): boolean {
    return this.personalDetailsFG.valid;
  }

  hasChanged(): boolean {
    return !this._uts.shallowEqual(
      this.personalDetailsFG.get('personalDetails').value,
      this.personalDetailsFGInitial
    );
  }

  onReset(): void {
    if (this.hasChanged()) {
      this.personalDetailsFG
        .get('personalDetails')
        .reset(this.personalDetailsFGInitial);
    }
  }

  // *
  // * Date of Birth
  // *

  onDOBSet(e: MatDatepickerInputEvent<Date>): void {
    this.dateOfBirthFC.setValue(moment(e.value).format('YYYY-MM-DD'));
    this.dOBSearchIndex.setValue(moment(e.value).format('MM-DD'));
  }

  // *
  // * Display Picture
  // *

  availableEvent(e: any): void {
    if (!e.fileSize) {
      return;
    }

    this.dpChosenInitialFileName = e.initialFileName;
    this.dpChosenFileSize = e.fileSize;
  }

  uploadEvent(e: any): void {
    if (!e.fileURL) {
      return;
    }

    this.dpUploadedInitialFileName = e.initialFileName;
    this.dpUploadedFileSize = e.fileSize;
    this.photoURLFC.setValue(e.fileURL);
  }

  // async uploadEvent(e: any): Promise<void> {
  //   try {
  //     if (!e.fileURL) {
  //       return;
  //     }

  //     this.photoURLFC.setValue(e.fileURL);
  //     await this._us.setUser(this.me.uid, {
  //       personalDetails: {
  //         photoURL: e.fileURL
  //       }
  //     });
  //   } catch (err) {
  //     this._sb.openErrorSnackBar(ErrorMessages.DISPLAY_PICTURE_NOT_SAVED);
  //   }
  // }

  // *
  // * Save
  // *

  setNameSearchArray(): void {
    const sanitizedFirstName = this._uts.sanitize(this.firstNameFC.value);
    const sanitizedLastName = this._uts.sanitize(this.lastNameFC.value);

    const fullName = `${sanitizedFirstName} ${sanitizedLastName}`;
    this.nameFC.setValue(fullName);

    const searchArray = this._uts.getSearchArray(
      sanitizedFirstName,
      sanitizedLastName
    );
    this.searchArrayFC.setValue(searchArray);
  }

  async onSave(): Promise<void> {
    if (!this.hasChanged() || !this.personalDetailsFG.valid) {
      return;
    }

    if (
      this.dpChosenFileSize !== this.dpUploadedFileSize ||
      this.dpChosenInitialFileName !== this.dpUploadedInitialFileName
    ) {
      this._sb.openErrorSnackBar(
        ErrorMessages.DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED
      );
      return;
    }
    this.loading = true;

    try {
      this.setNameSearchArray();
      // const searchArray = this._uts.getSearchArray(
      //   this._uts.sanitize(this.firstNameFC.value),
      //   this._uts.sanitize(this.lastNameFC.value)
      // );

      // const userObj: any = {
      //   name: `${this._uts.sanitize(
      //     this.firstNameFC.value
      //   )} ${this._uts.sanitize(this.lastNameFC.value)}`,
      //   searchArray,
      //   dOBSearchIndex: moment(this.dateOfBirthFC.value).format('MM-DD'),
      //   personalDetails: this.personalDetailsFG.value
      // };
      await this._us.setUser(this.me.uid, this.personalDetailsFG.value);

      // await this._us.setUser(this.me.uid, userObj);

      this.loading = false;
      this._sb.openSuccessSnackBar(SuccessMessages.SAVE_OK, 2000);
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
      this.loading = false;
    }
  }

}
