import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { DialogBoxModule } from '../shared/dialog-box';
import { GetStartedModule } from '../shared/get-started';
import { InputFilePreviewModule } from '../shared/input-file-preview';
import { MaterialDesignModule } from '../shared/material-design';
import { SharedComponentsModule } from '../shared/shared-components';
import { UploadFileModule } from '../shared/upload-file';
import { LayoutModule } from '../shared/layout/layout.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [ProfileComponent, EditProfileComponent, UserProfileComponent],
  imports: [
    CommonModule,
    IonicModule,
    LayoutModule,
    GetStartedModule,
    MaterialDesignModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    ProfileRoutingModule,
    UploadFileModule,
    DialogBoxModule,
    SharedComponentsModule,
    InputFilePreviewModule
  ]
})
export class ProfileModule {}
