import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Router } from '@angular/router';
import { MenuController, AlertController, LoadingController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ErrorMessages, RouteIDs } from 'src/app/common/constants';
import { UserAppointment, User, DialogBox } from 'src/app/common/models';
import { AuthService, GeneralService, SessionnService, SharedService, SnackBarService, UserService, UtilitiesService } from 'src/app/core/services';
import { environment } from 'src/environments/environment';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { DialogBoxComponent } from 'src/app/shared/dialog-box/dialog-box.component';
import { MatDialog } from '@angular/material/dialog';


export interface UserPhoto {
  filepath: string;
  webviewPath: string;
}
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  advancedRecommendedPackage = '';
  advancedTestingAppt: UserAppointment;
  dispatchView = false;
  hasIntroVideoEnded = false;
  hasWelcomeVideoEnded = false;
  heroImage = environment.heroSection.kinita;
  introVideoURL = environment.video.intro;
  me: User;
  routeIDs: typeof RouteIDs = RouteIDs;
  isPackageSubscribed: boolean;
  subscribedPackage: any;
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  path: any;
  base64Img: any;
  userImg: any;
  public photos: UserPhoto[] = [];
  constructor(
    private _as: AuthService,
    private _router: Router,
    private _dialog: MatDialog,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private _ss: SessionnService,
    private _shs: SharedService,
    private _sb: SnackBarService,
    private _general: GeneralService

  ) {    
    this.menuCtrl.swipeGesture(true);
    
  }

  ngOnInit() {
    this._me$.pipe(takeUntil(this._notifier$)).subscribe((user: User) => {
      this.me = user;
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;
      this.subscribedPackage = this.me.subscribedPackage;
      this.dispatchView = true;
      console.log(this.me)
    });
  }
  goto(path): void {
    this.path = path;
    this._router.navigate(['/tab/'+path]);
  }

  async onLogout()
  {
    await this._general.onLogout();
  }

  public async addPhotoToGallery() {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });
      // Save the picture and add it to photo collection
      this.photos.unshift({
        filepath: "soon...",
        webviewPath: capturedPhoto.webPath
      });
  const savedImageFile = await this.savePicture(capturedPhoto);
  }

  private async savePicture(photo: Photo) {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(photo);
  
    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });
  
    // Use webPath to display the new image instead of base64 since it's
    // already loaded into memory
    return {
      filepath: fileName,
      webviewPath: photo.webPath
    };
  }

  private async readAsBase64(photo: Photo) {
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();
  
    return await this.convertBlobToBase64(blob) as string;
  }
  
  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

}
