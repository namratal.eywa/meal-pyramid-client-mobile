import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from './../shared/layout/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthPageRoutingModule } from './auth-routing.module';

import { AuthPage } from './auth.page';
import { MaterialDesignModule } from '../shared/material-design';
import { LoginComponent } from './login/login.component';
import { SharedComponentsModule } from '../shared/shared-components';
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialDesignModule,
    LayoutModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    AuthPageRoutingModule,
    SharedComponentsModule
  ],
  declarations: [AuthPage, LoginComponent , RegisterComponent]
})
export class AuthModule {}
