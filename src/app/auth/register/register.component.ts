import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { RouteIDs } from 'src/app/common/constants';
import { AuthService, SnackBarService, UserService, HttpService } from 'src/app/core/services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  isPasswordVisible = false;
  loading = false;
  passwordFormControlIcon = 'visibility_off';
  passwordFormControlType = 'password';
  registerFormGroup: FormGroup;

  @Output() toggleView = new EventEmitter<boolean>();
  
  constructor(   
    private _formBuilder: FormBuilder,
    private _as: AuthService,
    private _sbs: SnackBarService,
    private _us: UserService,
    private _router: Router,
    private _hs: HttpService,
    public menuCtrl: MenuController,
    ) {
      this.menuCtrl.swipeGesture(false);
    }

  ngOnInit() {    this.registerFormGroup = this._formBuilder.group({
    firstName: [
      '',
      [Validators.required, Validators.pattern(/^[A-Za-z0-9]{2,51}$/)]
    ],
    lastName: [
      '',
      [Validators.required, Validators.pattern(/^[A-Za-z0-9]{1,51}$/)]
    ],
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
    mobile: [
      '',
      [
        Validators.required,
        Validators.pattern(/^\+(?:[0-9]-?){6,14}[0-9]$/)
        // Validators.minLength(10),
        // Validators.maxLength(10),
        // Validators.pattern('^[0-9]*$')
      ]
    ]
  });}

  get email(): string {
    return this.registerFormGroup.get('email').value;
  }

  get password(): string {
    return this.registerFormGroup.get('password').value;
  }

  get firstName(): string {
    return this.registerFormGroup.get('firstName').value;
  }

  get lastName(): string {
    return this.registerFormGroup.get('lastName').value;
  }

  get mobile(): string {
    return this.registerFormGroup.get('mobile').value;
  }

  async onRegister(): Promise<void> {
    if (this.registerFormGroup.valid) {
      this.loading = true;

      try {
        const cred = await this._as.setFirebaseMe({
          email: this.email,
          password: this.password
        });

        const data = {
          ...this.registerFormGroup.value,
          uid: cred.user.uid
        };
        await this._us.setUserFromAuth(data);

        this._hs
          .notifyAllAdmins({
            message: `A new user, ${this.firstName} ${this.lastName} just registered!`,
            isSystemGenerated: true
          })
          .then()
          .catch();

        this._hs
          .sendWelcomeEmail({
            name: this.firstName,
            email: this.email
          })
          .then()
          .catch();

        this._hs
          .sendHelloEmail({
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            mobile: this.mobile
          })
          .then()
          .catch();

        this._router.navigate([RouteIDs.HOME]);
      } catch (err) {
        this.registerFormGroup.reset();
        this.loading = false;
        this._sbs.openErrorSnackBar(err.message);
      }
    }
  }
  onShowLogin(): void {
    this.toggleView.emit(true);
    this._router.navigate([RouteIDs.LOGIN]);
    this.registerFormGroup.reset();
    console.log("login")
  }

  onTogglePasswordView(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
    this.passwordFormControlType =
      this.passwordFormControlType === 'password' ? 'text' : 'password';
    this.passwordFormControlIcon =
      this.passwordFormControlIcon === 'visibility_off'
        ? 'visibility'
        : 'visibility_off';
  }
}
