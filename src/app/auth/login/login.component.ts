import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorMessages, RouteIDs, SuccessMessages } from 'src/app/common/constants';
import { AuthService, SharedService, SnackBarService } from 'src/app/core/services';
import { Location } from '@angular/common';
import { MenuController } from '@ionic/angular';
import { Observable, Subject } from 'rxjs';
import { DialogBox, User } from 'src/app/common/models';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from 'src/app/shared/dialog-box/dialog-box.component';
import { Browser } from '@capacitor/browser';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  forgotPassFG!: FormGroup;
  isPasswordVisible = false;
  loading = false;
  loginFormGroup: FormGroup;
  passwordFormControlIcon = 'visibility_off';
  passwordFormControlType = 'password';
  sendingForgotPassEmail = false;
  showLogin = true;
  me: User;
  userHasSubcribedPackage: boolean;
  @Output() toggleView = new EventEmitter<boolean>();
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<any> = new Subject();
  isPackageSubscribed: boolean;
  subscribedPackage: any;
  dispatchView: boolean;
  subscription;
  constructor(
    private _formBuilder: FormBuilder,
    private _as: AuthService,
    private _sbs: SnackBarService,
    private _router: Router,
    private _snackBarService: SnackBarService,
    private _location: Location,
    public menuCtrl: MenuController,
    private _dialog: MatDialog,
    private _shs: SharedService,
  ) {
    this.menuCtrl.swipeGesture(false);

  }

  ngOnInit() {
    this.loginFormGroup = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });

    this.forgotPassFG = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }
  async onLogin(): Promise<void> {
    if (this.loginFormGroup.valid) {
      this.loading = true;
      try {
        await this._as.signInWithEmail(this.loginFormGroup.value);
        this.loginFormGroup.reset();
        this.checkUserFlag();
        // this._router.navigate([RouteIDs.HOME]);
        this.loading = false;
      } catch (err) {
        this.loginFormGroup.reset();
        this.loading = false;
        this._sbs.openErrorSnackBar("The password is invalid or the user does not have a password.");
      }
    }
  }

  public subcribeDialogue()
  {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'Close';
    dialogBox.actionTrueBtnTxt = 'Continue';
    dialogBox.icon = 'grading';
    dialogBox.title = 'Subcribe Package';
    dialogBox.message = ' Please Subcribe Package to continue using the application. ';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);
    dialogRef.beforeClosed().subscribe(async (res: any) => {
      if (!res) {
        return ;
      }
      try {
        await Browser.open({ url: "https://dev-meal-pyramid-users.web.app" });

    //  await window.open("https://dev-meal-pyramid-users.web.app");
    //  navigator['app'].exitApp();      
      } catch (err) {
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  
  checkUserFlag()
  {
    this.subscription=  this._me$.pipe(takeUntil(this._notifier$)).subscribe(async (user: User) => {
    this.me = await user;
    console.log( this.me)
    if( this.me.flags.isPackageSubscribed) 
    {
     await this._router.navigate(['/']);
      console.log( "home")
    }
    else if (this.me.membershipKey.length === 0 && !this.me.flags.isPackageSubscribed ) {
      await this._router.navigate(['/']);
      console.log( "2", this.me.membershipKey.length)
    }   
    else if(!this.me.flags.isPackageSubscribed)
      {
        console.log( "3")
        await this.subcribeDialogue();
    }
    console.log( this.me.membershipKey.length,this.me.membershipKey.length )
   });
  
  }



  async onForgotPass(): Promise<void> {
    if (!this.forgotPassFG.valid) {
      return;
    }

    try {
      this.sendingForgotPassEmail = true;
      await this._as.sendPasswordResetEmail(
        this.forgotPassFG.get('email').value
      );
      this.forgotPassFG.reset();
      this.sendingForgotPassEmail = false;
      this._snackBarService.openSuccessSnackBar(
        SuccessMessages.FORGOT_PASSWORD_EMAIL_SENT,
        5000
      );
    } catch (err) {
      this.forgotPassFG.reset();
      this.sendingForgotPassEmail = false;
      this._snackBarService.openErrorSnackBar(err.message);
    }
  }

  toggleViewPassword(): void {
    this.showLogin = !this.showLogin;
    const path = this._location.isCurrentPathEqualTo(`/${RouteIDs.LOGIN}`)
      ? RouteIDs.FORGOT_PASSWORD
      : RouteIDs.LOGIN;
    this._location.replaceState(path);
  }

  onShowRegister(): void {
    this.toggleView.emit(true);
    this._router.navigate([RouteIDs.REGISTER]);
    this.loginFormGroup.reset();
  }

  onTogglePasswordView(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
    this.passwordFormControlType =
      this.passwordFormControlType === 'password' ? 'text' : 'password';
    this.passwordFormControlIcon =
      this.passwordFormControlIcon === 'visibility_off'
        ? 'visibility'
        : 'visibility_off';
  }
}
