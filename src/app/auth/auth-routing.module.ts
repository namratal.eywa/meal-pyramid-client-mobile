import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteIDs } from '../common/constants';

import { AuthPage } from './auth.page';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: '',
    component: AuthPage,
    children: [ 
      {
        path:'register',
        component:RegisterComponent
      },
      {
        path: RouteIDs.LOGIN,
        component:LoginComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthPageRoutingModule {}
