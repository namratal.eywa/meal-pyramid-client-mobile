import { Component, OnInit, OnDestroy } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AlertController, MenuController, Platform } from '@ionic/angular';
import { NavigationEnd, Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Location, } from "@angular/common";
import { ErrorMessages, RouteIDs, } from './common/constants';
import { AuthService, GeneralService, SnackBarService } from './core/services';
import { environment } from 'src/environments/environment';
import { User } from './common/models';
import { Subject, EMPTY, Observable } from 'rxjs';
import { catchError, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

  chatURL = '';
  dispatchView = false;
  isPackageSubscribed = false;
  loading = false;
  me: User;
  path: string;
  routeIDs: typeof RouteIDs = RouteIDs;
  subscribedPackage: any;
  closed$ = new Subject<any>();
  showTabs = true; // 
  private _me$: Observable<User> = this._as.getMe();
  private _notifier$: Subject<void> = new Subject();
  public show_footer: boolean;


  constructor(private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _router: Router,
    private _location: Location,
    private alertController: AlertController,
    private _as: AuthService,
    private _general: GeneralService,
    private _sb: SnackBarService,
  ) {
    this.initializeApp();
    this.chatURL = environment.support.chatURL;
    // App.addListener('backButton', () => {
    //   if (this._location.isCurrentPathEqualTo('/tab/home')) {
    //     this._general.closeApp();
    //   }
    //   else {
    //     this._location.back();
    //   }
    // });
  }


  ngOnInit(): void {
    this.chatURL = environment.support.chatURL;
    this._me$.pipe(
      takeUntil(this._notifier$),
      catchError(() => {
        this._sb.openErrorSnackBar(ErrorMessages.SYSTEM);
        return EMPTY;
      }),
      takeUntil(this._notifier$)
    )
    .subscribe((me: User) => {
      this.me = me;
      console.log(this.me);
      
      this.isPackageSubscribed = this.me.flags.isPackageSubscribed;

      this.dispatchView = true;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this._router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          if (this._router.url === "/" + RouteIDs.LOGIN) {
            this.show_footer = false;
          }
        }
      })
      this.backButtonEvent();
  }

  backButtonEvent() {
    this.platform.backButton.subscribeWithPriority(10, (processNextHandler) => {
      console.log('Back press handler!');
      if (this._location.isCurrentPathEqualTo("/tab/home")) {
        // Show Exit Alert!
        navigator['app'].exitApp();
        // this._general.closeApp();
        processNextHandler();
      } else {
        // Navigate to back page
        console.log('Navigate to back page');
        this._location.back();

      }

    });
  }


  // async onLogoutClick(): Promise<void> {
  //   const confirm = this.alertController.create({
  //     header: "exit",
  //     message: "Do you really want to exit from the application?",
  //     animated: true,
  //     buttons: [
  //       {
  //         text: "Yes",
  //         handler: () => {
  //             setTimeout(async () => {
  //               navigator['app'].exitApp();
  //             }, 750);
  //         }
  //       },
  //       {
  //         text: "No",
  //         handler: async () => {

  //         },
  //       },
  //     ],
  //   });
  //   (await confirm).present();
  // }


  ngOnDestroy() {
    this.closed$.next();
    this._notifier$.next();
    this._notifier$.complete(); // <-- close subscription when component is destroyed
  }
}
