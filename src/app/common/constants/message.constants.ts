export enum SuccessMessages {
  APPT_BOOKED = 'Your appointment has been scheduled!',
  APPT_CANCELED = 'Your appointment has been canceled!',
  FILE_UPLOADED = 'Thank you! Your file has been uploaded.',
  SAVE_OK = 'The changes have been saved successfully.',
  FORGOT_PASSWORD_EMAIL_SENT = 'If you have an account with us, you should receive an email to reset your password shortly.',
  FILE_DELETED = 'The file has been deleted.'
}

export enum SuccessTypes {
  APPT_BOOKED = 'Your appointment has been scheduled!',
  APPT_CANCELED = 'Your appointment has been canceled!',
  FILE_UPLOADED = 'Thank you! Your file has been uploaded.',
  SAVE_OK = 'The changes have been saved successfully.',
  FILE_DOWNLAOD = 'The file has been downloaded!',
  FORGOT_PASSWORD_EMAIL_SENT = 'We just sent you an email to reset your password.'
}
