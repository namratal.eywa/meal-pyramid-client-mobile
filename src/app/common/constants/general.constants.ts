export enum RouteIDs {
  APPOINTMENTS = 'appointments',
  DIETS = 'diets',
  HEALTH_HISTORY = 'health-history',
  HOME = 'home',
  LANDING = '',
  LOGIN = 'login',
  PROGRESS = 'progress',
  RECOMMENDATIONS = 'recommendations',
  REGISTER =  'register',
  ERROR = 'error',
  PROFILE = 'profile',
  FORGOT_PASSWORD = 'forgot-password',
  NOTIFICATION="notification",
  BILLING="billings"
}

export enum ErrorMessages {
  SYSTEM = 'Aw snap! Something went wrong.',
  SYSTEM_TRY_AGAIN = 'Sorry, the system encountered an error. Please try again.',
  ACTION_NOT_ALLOWED = 'This action is not allowed.',
  SAVE_FAILED = 'Sorry, the changes could not be saved.',
  DELETE_FAILED = 'Sorry, the item could not be deleted.',
  CANNOT_RECALL = 'This user cannot have a Recall appointment.',
  CANNOT_ORIENTATION = 'This user cannot have an Orientation appointment.',
  INVALID_DATE = 'The selected date is invalid.',
  USER_NOT_LOGGED_IN = 'Please login to continue using the application.',
  USER_NOT_AUTHORIZED = "You're not authorized to access this application.",
  INVALID_DATA = 'The provided data does not pass validation.',
  DISPLAY_PICTURE_NOT_SAVED = 'Your profile picture could not be set.',
  DISPLAY_PICTURE_CHOSEN_NOT_UPLOADED = 'You have chosen a display picture, but not uploaded yet. Please upload or reset prior to saving the data.',
  CANNOT_DELETE_FILE = 'The file could not be deleted.',
  FILE_NOT_UPLOADED = 'The file could not be uploaded.'
}

export enum ErrorTypes {
  SYSTEM = 'Aw snap! Something went wrong.',
  SYSTEM_TRY_AGAIN = 'Sorry, the system encountered an error. Please try again.',
  ACTION_NOT_ALLOWED = 'This action is not allowed.',
  CANNOT_RECALL = 'This user cannot have a Recall appointment.',
  CANNOT_ORIENTATION = 'This user cannot have an Orientation appointment.',
  INVALID_DATE = 'The selected date is invalid.',
  USER_NOT_LOGGED_IN = 'Please login to continue using the application.',
  USER_NOT_AUTHORIZED = "You're not authorized to access this application.",
  INVALID_DATA = 'The provided data does not pass validation.',
  DISPLAY_PICTURE_NOT_SAVED = 'Your profile picture could not be set.',
  CANNOT_DELETE_FILE = 'The file could not be deleted.'
}

// Verified -
export enum UserRoles {
  USER = 'user',
  NUTRITIONIST = 'nutritionist',
  ADVANCED_TESTING_STAFF = 'advancedTestingStaff',
  LEAD_NUTRITIONIST = 'leadNutritionist',
  ADMIN = 'admin',
  SUPER_ADMIN = 'superAdmin'
}

// export enum UserTypes {
//   PRE_ORIENTATION_USER = 'preOrientationUser',
//   ORIENTATION_USER = 'orientationUser',
//   SUBSCRIBER = 'subscriber'
// }

// Verified -
export enum GenderTypes {
  MALE = 'Male',
  FEMALE = 'Female',
  UNDISCLOSED = 'Undisclosed'
}

// Verified -
export enum Locations {
  JUHU = 'Juhu',
  KEMPS_CORNER = 'Kemps Corner',
  ONLINE = 'Online'
}

// Verified -
export enum ProductTypes {
  ORIENTATION = 'Orientation',
  SUBSCRIPTION = 'Subscription',
  ADVANCED_TESTING = 'Advanced Testing'
}

// Verified -
export enum PackageTypes {
  STANDARD = 'Standard',
  PREMIUM = 'Premium',
  PRESTIGE = 'Prestige'
}

// Verified -
export enum PackageCategories {
  GENERAL = 'General Health Nutrition',
  PERFORMANCE = 'Sports & Performance Nutrition'
}

// Verified -
export enum GeneralNutritionSubCategories {
  FAT_LOSS = 'Fat Loss',
  THERAPEUTIC = 'Therapeutic',
  GROWING_KIDS = 'Growing Kids',
  PREGNANCY = 'Pregnancy',
  HORMONAL_BALANCE = 'Hormonal Balance',
  MAINTENANCE = 'Maintenance',
  BALANCED_DIETS = 'Balanced Diets'
}

// Verified -
export enum GeneralNutritionMedicalConditions {
  DIABETES = 'Diabetes',
  PRE_DIABETES = 'Pre-diabetes',
  PCOD = 'PCOD',
  HIGH_CHOLESTROL = 'High Cholestrol',
  HIGH_TRYGLYCERIDES = 'High Triglycerides',
  LOW_IRON = 'Low Iron OR Anemia',
  B12_DEFICIENCY = 'B12 Deficiency',
  D3_DEFICIENCY = 'D3 Deficiency',
  HEART_DISEASE = 'Heart Disease',
  HIGH_URIC_ACID = 'High Uric Acid',
  THYROID = 'Thyroid',
  HIGH_BLOOD_PRESSURE = 'High Blood Pressure',
  GERD = 'GERD',
  CHRONIC_ACIDITY = 'Chronic Acidity'
}

// Verified -
export enum PerformanceNutritionSubCategories {
  ELITE_ATHLETE = 'Elite Athlete',
  JUNIOR_ATHLETE = 'Junior Athlete',
  ADVENTURE_SPORTS = 'Adventure Sports',
  FITNESS_ENTHUSIAST = 'Fitness Enthusiast',
  RECREATIONAL_ATHLETE = 'Recreational Athlete',
  MUSCLE_GAIN = 'Muscle Gain & Fat Loss'
}

// Verified -
export enum ExerciseTypes {
  WALKING = 'Walking',
  JOGGING = 'Jogging',
  SWIMMING = 'Swimming',
  PILATES = 'Pilates',
  ZUMBA = 'Zumba/Dance',
  YOGA = 'Yoga',
  WEIGHTS = 'Weight Training',
  HIIT = 'HIIT',
  MISC = 'Any Sports'
}

// Verified -
export enum WeightLossTypes {
  COMMERCIAL = 'Commercial weight loss centers',
  GYM = 'Joined the gym',
  SELF = 'Self-dieting/training',
  EXPERT = 'Visited a nutritionist/fitness-expert',
  OTHER = 'Other'
}

// Verified -
export enum MealCategories {
  VEG = 'Vegetarian',
  JAIN = 'Jain',
  NON_VEG = 'Non-vegetarian',
  VEGAN = 'Vegan',
  EGG = 'Eggetarian',
  OTHER = 'Other'
}

// export enum Frequency {
//   WEEKLY = 7,
//   FORTNIGHTLY = 14,
//   MONTHLY = 30
// }

// Verified -
export enum AppointmentTypes {
  ORIENTATION = 'Orientation',
  RECALL = 'Recall',
  ADVANCED_TESTING = 'Advanced Testing'
}

// Verified -
export enum AppointmentStatuses {
  ATTENDED = 'Attended',
  NO_SHOW = 'No-show'
}

// Verified -
// // ToDo: Delete KIDNEY_PROFILE, OTHER_BLOOD_TESTS
export enum AssessmentCategories {
  GENERAL = 'General',
  // KIDNEY_PROFILE = 'Kidney Profile',
  DIABETIC_PROFILE = 'Diabetic Profile',
  VITAMIN_PROFILE = 'Vitamin Profile',
  HORMONES = 'Hormones',
  SPORTS_SPECIFIC_TESTS = 'Sports Specific Tests',
  // OTHER_BLOOD_TESTS = 'Other Blood Tests',
  OTHER_TESTS = 'Other Tests'
}

// Verified -
// export const AdvancedTestPackages = [
//   {
//     key: 'wellnessTestBasic',
//     name: 'Wellness Test - Basic',
//     price: {
//       amount: 2300,
//       amountWithTax: 2714
//     },
//     testKeys: ['weight', 'bloodSugar', 'bodyCompositionAnalysis'],
//     isActive: true
//   },
//   {
//     key: 'wellnessTestBasicPlus',
//     name: 'Wellness Test - Basic+',
//     price: {
//       amount: 7800,
//       amountWithTax: 9204
//     },
//     testKeys: [
//       'weight',
//       'bloodSugar',
//       'bodyCompositionAnalysis',
//       'metabolicRateTesting'
//     ],
//     isActive: true
//   },
//   {
//     key: 'wellnessTestPro',
//     name: 'Wellness Test - Pro',
//     price: {
//       amount: 17300,
//       amountWithTax: 20414
//     },
//     isActive: false
//   },
//   {
//     key: 'performanceTestBasic',
//     name: 'Performance Test - Basic',
//     price: {
//       amount: 3500,
//       amountWithTax: 4130
//     },
//     testKeys: [
//       'weight',
//       'bloodSugar',
//       'bodyCompositionAnalysis',
//       'lungCapacityTesting'
//     ],
//     isActive: true
//   },
//   {
//     key: 'performanceTestBasicPlus',
//     name: 'Performance Test - Basic+',
//     price: {
//       amount: 9000,
//       amountWithTax: 10620
//     },
//     testKeys: [
//       'weight',
//       'bloodSugar',
//       'bodyCompositionAnalysis',
//       'lungCapacityTesting',
//       'metabolicRateTesting'
//     ],
//     isActive: true
//   },
//   {
//     key: 'performanceTestPro',
//     name: 'Performance Test - Pro',
//     price: {
//       amount: 22000,
//       amountWithTax: 25690
//     },
//     isActive: false
//   }
// ];
export const AdvancedTestPackages = [
  {
    key: 'wellnessTestBasic',
    name: 'Wellness Test - Basic',
    price: {
      amount: 2650,
      amountWithTax: 3127
    },
    testKeys: [
      'heightWeight',
      'bloodSugar',
      'handGripStrength',
      'bodyCompositionAnalysis'
    ],
    isActive: true
  },
  {
    key: 'wellnessTestBasicPlus',
    name: 'Wellness Test - Basic+',
    price: {
      amount: 8150,
      amountWithTax: 9617
    },
    testKeys: [
      'heightWeight',
      'bloodSugar',
      'handGripStrength',
      'bodyCompositionAnalysis',
      'restingMetabolicRate'
    ],
    isActive: true
  },
  {
    key: 'wellnessTestPro',
    name: 'Wellness Test - Pro',
    price: {
      amount: 12650,
      amountWithTax: 14927
    },
    testKeys: [
      'heightWeight',
      'bloodSugar',
      'handGripStrength',
      'bodyCompositionAnalysis',
      'restingMetabolicRate',
      'vitaminScan'
    ],
    isActive: true
  },
  {
    key: 'performanceTestBasic',
    name: 'Performance Test - Basic',
    price: {
      amount: 8650,
      amountWithTax: 10207
    },
    testKeys: [
      'heightWeight',
      'bloodSugar',
      'handGripStrength',
      'bodyCompositionAnalysis',
      'pulmonaryFunction',
      'vitaminScan'
    ],
    isActive: true
  },
  {
    key: 'performanceTestBasicPlus',
    name: 'Performance Test - Basic+',
    price: {
      amount: 14150,
      amountWithTax: 16697
    },
    testKeys: [
      'heightWeight',
      'bloodSugar',
      'handGripStrength',
      'bodyCompositionAnalysis',
      'pulmonaryFunction',
      'vitaminScan',
      'restingMetabolicRate'
    ],
    isActive: true
  },
  {
    key: 'performanceTestPro',
    name: 'Performance Test - Pro',
    price: {
      amount: 24150,
      amountWithTax: 28497
    },
    testKeys: [
      'heightWeight',
      'bloodSugar',
      'handGripStrength',
      'bodyCompositionAnalysis',
      'pulmonaryFunction',
      'vitaminScan',
      'restingMetabolicRate',
      'lactateThreshold',
      'sweatAnalysis'
    ],
    isActive: false
  }
];

// Verified -
export enum UserRecommendations {
  WATER = 'Daily Water Intake',
  ACTIVITY = 'Physical Activity or Exercise',
  SUPPLEMENTATION = 'Supplementation',
  OTHER = 'Other Recommendations',
  AVOID = 'Things to Avoid',
  DOS = "Do's & Don'ts",
  GENERAL = 'General Remarks'
}

// Verified -
export enum UserCategories {
  GENERAL_NUTRITION = 'General Health Nutrition',
  SPORTS_NUTRITION = 'Sports & Performance Nutrition'
}

// Verified -
export const LightboxConfigs = {
  fadeDuration: 0.5,
  disableScrolling: true,
  centerVertically: true,
  albumLabel: '',
  enableTransition: false
};

// export const Orientation = {
//   price: {
//     amount: 2000,
//     amountWithTax: 2360
//   }
// };
