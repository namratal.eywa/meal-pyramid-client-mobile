// Verified -
export enum APIFunctions {
  CREATE_PAYMENT = '/payment/auto/create',
  SET_PAYMENT = '/payment/auto',

  GET_AVAILABLE_ORIENTATION_SLOTS = '/slots/orientation/available/user',
  SET_ORIENTATION_APPOINTMENT = '/appointment/orientation/book',
  CANCEL_ORIENTATION_APPOINTMENT = '/appointment/orientation/cancel',

  GET_AVAILABLE_RECALL_SLOTS = '/slots/recall/available/user',
  SET_RECALL_APPOINTMENT = '/appointment/recall/book',
  CANCEL_RECALL_APPOINTMENT = '/appointment/recall/cancel',

  GET_AVAILABLE_ADVANCED_TESTING_SLOTS = '/slots/advanced-testing/available/user',
  SET_ADVANCED_TESTING_APPOINTMENT = '/appointment/advanced-testing/book',
  CANCEL_ADVANCED_TESTING_APPOINTMENT = '/appointment/advanced-testing/cancel',

  // SUBSCRIBE_PACKAGE = '/user/subscribe',

  EMAIL_WELCOME = '/email/welcome',
  EMAIL_REGISTERED = '/email/hello',

  NOTIFY_ADMINS = '/notify/admins'
}
