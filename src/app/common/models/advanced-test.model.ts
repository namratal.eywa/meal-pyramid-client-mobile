import { UtilitiesService } from 'src/app/core/services';
import { Price } from './price.model';

// Used & Verified -
export class UserAdvancedTest {
  key = '';
  isCompleted = false;
  isLocked = false;
  chosenTests: AdvancedTest[] = [];
  recommendedTests: AdvancedTest[] = [];
  purchasedTests: AdvancedTest[] = [];
  recommendedPackage: MATPackage = new MATPackage();
  chosenPackage: MATPackage = new MATPackage();
  purchasedPackage: MATPackage = new MATPackage();
  uid = '';
  membershipKey = '';
  paymentKey = '';
  userAppointmentKey = '';
  isActive = false;
  recStatus = false;
  isForcedReset = false;
  systemLog: string[] = [];

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);

      // if (data.recommendedTests) {
      //   this.recommendedTests = AdvancedTest.FromData(
      //     data.recommendedTests,
      //     uts
      //   );
      // }

      // if (data.purchasedTests) {
      //   this.purchasedTests = AdvancedTest.FromData(data.purchasedTests, uts);
      // }
    }
  }

  static FromData(data: any, uts: UtilitiesService): UserAdvancedTest[] {
    let arr: UserAdvancedTest[] = [];

    if (data && data.length) {
      arr = data.map((item) => new UserAdvancedTest(item, uts));
    }

    return arr;
  }
}

export class AdvancedTest {
  key = '';
  name = '';
  price: Price = new Price();
  // sort = 0;
  isActive = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): AdvancedTest[] {
    let tests: AdvancedTest[] = [];

    if (data && data.length) {
      tests = data.map((item) => new AdvancedTest(item, uts));
    }

    return tests;
  }

  // static FromDBDataTemp(data: any, uts: UtilitiesService): AdvancedTest[] {
  //   let tests: AdvancedTest[] = [];

  //   if (data.recommendedTests && data.recommendedTests.length > 0) {
  //     tests = data.recommendedTests.map((item) => new AdvancedTest(item, uts));
  //   }

  //   return tests;
  // }
}

export class MATPackage {
  key = '';
  name = '';
  price: Price = new Price();
  testKeys: string[] = [];
  isActive = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): MATPackage[] {
    let arr: MATPackage[] = [];

    if (data && data.length) {
      arr = data.map((item) => new MATPackage(item, uts));
    }

    return arr;
  }
}
