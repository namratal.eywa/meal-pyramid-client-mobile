// Used & Verified -
// ToDo: Verify why is Object.getOwnPropertyNames being used?
export class DialogBox {
  actionFalseBtnStyle = '';
  actionFalseBtnTxt = '';
  actionTrueBtnStyle = '';
  actionTrueBtnTxt = '';
  icon = '';
  title = '';
  message = '';

  reset(): void {
    Object.getOwnPropertyNames(this).forEach((key) => {
      this[key] = '';
    });
  }
}
