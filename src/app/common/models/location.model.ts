import { UtilitiesService } from "src/app/core/services";

// Verified -
// ToDo: Verify & Delete, use getMLocations instead
export class Locationn {
  city = '';
  country = '';
  key = '';
  name = '';
  sort = 0;
  isActive = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): Locationn[] {
    let arr: Locationn[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Locationn(item, uts));
    }

    return arr;
  }
}
