import { UtilitiesService } from "src/app/core/services";


// Used & Verified -
export class UserHealthHistory {
  key = '';
  alcoholDesc = '';
  doesAlcohol = false;
  doesExercise = false;
  doesSmoking = false;
  doesTobacco = false;
  eatingHabits = '';
  exerciseDesc: Exercise[] = [];
  hasOtherConcerns = false;
  isMCycleRegular: boolean = null;
  mCycleDesc = '';
  mealCategory = '';
  medicalConditions: string[] = [];
  medicationDesc = '';
  onMedication = false;
  onSupplements = false;
  otherConcernsDesc = '';
  otherMedicalConditions = '';
  smokingDesc = '';
  sportConcerns = '';
  sportInjury = '';
  sportName = '';
  supplementsDesc = '';
  tobaccoDesc = '';
  triedLosingWeight = false;
  typicalDayRoutine = '';
  weightLossTypes: string[] = [];
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): UserHealthHistory[] {
    let arr: UserHealthHistory[] = [];

    if (data && data.length) {
      arr = data.map((item) => new UserHealthHistory(item, uts));
    }

    return arr;
  }
}

export class Exercise {
  daysPerWeek: number = null;
  duration: number = null;
  intensity = '';
  name = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): Exercise[] {
    let arr: Exercise[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Exercise(item, uts));
    }

    return arr;
  }
}
