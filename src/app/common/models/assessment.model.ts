import { UtilitiesService } from "src/app/core/services";

// Used & Verified -
export class UserAssessment {
  key = '';
  isCompleted = false;
  tests: Assessment[] = [];
  uid = '';
  membershipKey = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);

      // if (data.tests && data.tests.length) {
      //   this.tests = data.tests;
      // }
    }
  }

  static FromData(data: any, uts: UtilitiesService): UserAssessment[] {
    let arr: UserAssessment[] = [];

    if (data && data.length) {
      arr = data.map((item) => new UserAssessment(item, uts));
    }

    return arr;
  }
}

export class Assessment {
  category = '';
  key = '';
  name = '';
  description = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): Assessment[] {
    let arr: Assessment[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Assessment(item, uts));
    }

    return arr;
  }
}
