import { UtilitiesService } from "src/app/core/services";
import { MUser } from './mini-user.model';

// Used & Verified -
export class Notification {
  key = '';
  isNew = false;
  isRead = false;
  isShown = false;
  isSystemGenerated = false;
  message = '';
  messageHTML = '';
  category = '';
  notifDate: Date = null;
  staff = new MUser();
  uid = '';
  // recStatus = true;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): Notification[] {
    let arr: Notification[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Notification(item, uts));
    }

    return arr;
  }
}
