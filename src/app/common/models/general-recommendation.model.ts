import { UtilitiesService } from "src/app/core/services";

// Used & Verified -
export class UserGeneralRecommendation {
  key = '';
  water = '';
  activity = '';
  supplementation = '';
  other = '';
  avoid = '';
  dos = '';
  general = '';
  uid = '';
  membershipKey = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(
    data: any,
    uts: UtilitiesService
  ): UserGeneralRecommendation[] {
    let arr: UserGeneralRecommendation[] = [];

    if (data && data.length) {
      arr = data.map((item) => new UserGeneralRecommendation(item, uts));
    }

    return arr;
  }
}
