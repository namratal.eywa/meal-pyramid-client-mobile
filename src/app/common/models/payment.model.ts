
import { UtilitiesService } from "src/app/core/services";

// ToDo: Delete
export class Payment {
  firstname: string;
  hash: string;
  key: string;
  lastname: string;
  email: string;
  phone: number;
  amount: number;
  productinfo: string;
  txnid: number;
  surl: string;
  furl: string;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      // uts.shallowAssignTruthy(data, this);
    }

    this.txnid = this.getRandomInt();
  }

  getRandomInt() {
    return Math.floor(100000 + Math.random() * 900000);
  }
}
