import { UtilitiesService } from "src/app/core/services";

// Used & Verified -
export class MLocationn {
  key = '';
  name = '';
  // sort = 0;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): MLocationn[] {
    let arr: MLocationn[] = [];

    if (data && data.length) {
      arr = data.map((item) => new MLocationn(item, uts));
    }

    return arr;
  }
}
