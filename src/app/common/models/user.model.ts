
import { MPackage } from './mini-package.model';
import { MAppointment } from './mini-appointment.model';
import { MUser } from './mini-user.model';
import { MLocationn } from './mini-location.model';
import { GST } from './gst.model';
import { UtilitiesService } from 'src/app/core/services';
import { UserRoles } from '../constants';

// Used & Verified -
export class User {
  uid = '';
  legacyUid: '';
  name = '';
  personalDetails: PersonalDetails = new PersonalDetails();

  onboardingStep = 0;
  orientationAppt: MAppointment = new MAppointment();
  orientationNotes = '';

  recommendedPackage: MPackage = new MPackage();

  assignedNutritionist: MUser = new MUser();
  assignedSecondaryNutritionist: MUser = new MUser();

  subscribedPackage: MPackage = new MPackage();
  subscriptionDuration: SubscriptionDuration = new SubscriptionDuration();

  buyAdvancedTestingStep = 0;

  weightProgress: WeightProgress = new WeightProgress();

  birthdayGreetings: BirthdayGreetings = new BirthdayGreetings();

  roles: string[] = [];
  flags: Flag = new Flag();

  dOBSearchIndex = '';
  searchArray: string[] = [];

  membershipKey = '';
  allMembershipKeys: string[] = [];

  isActive = false;
  recStatus = false;
  systemLog = [];
  createdAt: Date = null;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromDBUser(data: any[], uts: UtilitiesService): User {
    if (data.length && data.length === 1) {
      return new User(data[0], uts);
    }

    if (!data.length) {
      throw new Error('USER_NOT_FOUND');
    }

    throw new Error('MULTIPLE_USERS_FOUND');
  }

  static FromAuth(data: any, uts: UtilitiesService): User {
    const user = new User();

    if (!data || !data.email || !data.uid) {
      throw new Error('INVALID_DATA');
    }

    user.personalDetails.firstName = data.firstName || '';
    user.personalDetails.lastName = data.lastName || '';
    user.personalDetails.email = data.email || '';
    user.personalDetails.mobile = data.mobile || '';
    user.roles = [UserRoles.USER];
    user.name = data.firstName
      ? `${uts.sanitize(data.firstName)} ${uts.sanitize(data.lastName)}`
      : '';
    user.uid = data.uid;
    user.isActive = true;
    user.recStatus = true;

    user.setSearchArray(uts);
    return user;
  }

  static FromData(data: any[], uts: UtilitiesService): User[] {
    let arr: User[] = [];

    if (data && data.length) {
      arr = data.map((item) => new User(item, uts));
    }

    return arr;
  }

  setSearchArray(uts: UtilitiesService): void {
    const arr = uts.getSearchArray(
      uts.sanitize(this.personalDetails.firstName),
      uts.sanitize(this.personalDetails.lastName)
    );
    this.searchArray = arr;

    // const arr = [];
    // let previousString1 = '';
    // let previousString2 = '';

    // uts
    //   .sanitize(this.personalDetails.firstName)
    //   .split('')
    //   .map((item) => {
    //     previousString1 += item.toLowerCase();
    //     arr.push(previousString1);
    //   });

    // previousString1 += ' ';
    // uts
    //   .sanitize(this.personalDetails.lastName)
    //   .split('')
    //   .map((item) => {
    //     previousString1 += item.toLowerCase();
    //     arr.push(previousString1);

    //     previousString2 += item.toLowerCase();
    //     arr.push(previousString2);
    //   });

    // this.searchArray = arr;
  }
}

export class PersonalDetails {
  addressLine1 = '';
  addressLine2 = '';
  category = 'General Health Nutrition';
  city = '';
  country = '';
  dateOfBirth = '';
  email = '';
  firstName = '';
  gender = '';
  gst: GST = new GST();
  lastName = '';
  mobile = '';
  photoURL = '';
  pincode = '';
  preferredLocation: MLocationn = new MLocationn();
  referredBy = '';
  serviceType = '';
  state = '';
  target = '';
}

export class Flag {
  isOrientationPaid = false;
  isOrientationScheduled = false;
  isOrientationCompleted = false;
  isPackageRecommended = false;
  isPackageSubscribed = false;

  hasAdvancedTests = false;
  hasClinicalAssessments = false;

  requiresGSTInvoice = false;
}

export class SubscriptionDuration {
  start: Date = null;
  end: Date = null;
}

export class BirthdayGreetings {
  sent: number[] = [];
}

export class WeightProgress {
  startDate: Date = null;
  startWeight: number = null;
  targetDate: Date = null;
  targetWeight: number = null;
}
