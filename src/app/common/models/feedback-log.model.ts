import { UtilitiesService } from '../../core/services';

// Verified -
export class UserFeedbackLog {
  key = '';
  date: Date | null = null;
  isHydrationFollowed: boolean | null = null;
  hydrationIntake = '';
  isOutsideMeal: boolean | null = null;
  outsideMealsCount = '';
  isMealSkipped: boolean | null = null;
  skippedMealsCount = '';
  isRoutineChanged: boolean | null = null;
  routineChangeDetails = '';
  isHungryBetweenMeals: boolean | null = null;
  isAlcoholConsumed: boolean | null = null;
  alcoholConsumedDetails = '';
  overallFeedback = '';
  hadCompetition: boolean | null = null;
  competitionPerformance = '';
  competitionRecovery = '';
  upcomingCompetition: boolean | null = null;
  upcomingCompetitionDetails = '';
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): UserFeedbackLog[] {
    let userFeedbackLogs: UserFeedbackLog[] = [];

    if (data && data.length) {
      userFeedbackLogs = data.map((item) => new UserFeedbackLog(item, uts));
    }

    return userFeedbackLogs;
  }
}
