// Used & Verified
export class APIResponse {
  status = false;
  success: Meta = new Meta();
  data: any = {};
  error: Meta = new Meta();

  constructor(resData?: any) {
    if (resData) {
      this.status = resData.status || false;

      if (resData.success) {
        this.success.code = resData.success.code || '';
        this.success.message = resData.success.message || '';
        this.success.description = resData.success.description || '';
      }

      if (resData.error) {
        this.error.code = resData.error.code || '';
        this.error.message = resData.error.message || '';
        this.error.description = resData.error.description || '';
      }

      this.data = resData.data || {};
    }
  }
}

export class Meta {
  code = '';
  message = '';
  description = '';
}
