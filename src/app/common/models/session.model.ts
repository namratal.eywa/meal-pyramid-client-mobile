import { MPackage } from './mini-package.model';
import { MUser } from './mini-user.model';
import { UtilitiesService } from '../../core/services';

// Used & Verified
export class Session {
  key = '';
  previousSessionKey = '';
  nextSessionKey = '';
  sessionNumber = 0;
  // file = '';
  fileKey = '';
  fileType = '';
  fileName = '';
  filePath = '';
  fileURL = '';
  isCurrent = false;
  isCompleted = false;
  startDate: Date = null;
  completedAt: Date = null;
  dueDate: Date = null;
  idealDueDate: Date = null;
  notes = '';
  package: MPackage = new MPackage();
  staff: MUser = new MUser();
  user: MUser = new MUser();
  membershipKey: '';
  // isActive = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): Session[] {
    let diets: Session[] = [];

    if (data && data.length) {
      diets = data.map((item) => new Session(item, uts));
    }

    return diets;
  }
}
