import { UtilitiesService } from "src/app/core/services";
import { Price } from './price.model';

// Used & Verified -
export class Package {
  key = '';
  name = '';
  category = '';
  type = '';
  description = '';
  numberOfMonths: number = null;
  numberOfWeeks: number = null;
  frequency: number = null;
  numberOfSessions: number = null;
  price: Price = new Price();

  clicked = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): Package[] {
    let arr: Package[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Package(item, uts));
    }

    return arr;
  }
}
