import { UtilitiesService } from "src/app/core/services";

// Verified -
export class UserMeasurementLog {
  key = '';
  date: Date = null;
  abs: number = null;
  chest: number = null;
  hips: number = null;
  midArm: number = null;
  thighs: number = null;
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;
  logs: UserMeasurementLog[];

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): UserMeasurementLog[] {
    let userMeasurementLogs: UserMeasurementLog[] = [];

    if (data && data.length > 0) {
      userMeasurementLogs = data.map(
        (item) => new UserMeasurementLog(item, uts)
      );
    }

    return userMeasurementLogs;
  }
}

// export class UserMeasurementLog {
//   uid = '';
//   membershipKey = '';
//   logs: MeasurementLog[] = [];

//   constructor(data?: any, uts?: UtilitiesService) {
//     if (data && uts) {
//       uts.deepAssignTruthyFirestore(this, data);

//       if (data.logs) {
//         this.logs = MeasurementLog.FromData(data.logs, uts);
//       }
//     }
//   }

//   static FromData(data: any, uts: UtilitiesService): UserMeasurementLog[] {
//     let userWeightLogs: UserMeasurementLog[] = [];

//     if (data && data.length > 0) {
//       userWeightLogs = data.map((item) => new UserMeasurementLog(item, uts));
//     }

//     return userWeightLogs;
//   }
// }

// export class MeasurementLog {
//   abs: number = null;
//   chest: number = null;
//   date: Date = null;
//   hips: number = null;
//   midArm: number = null;
//   thighs: number = null;

//   constructor(data?: any, uts?: UtilitiesService) {
//     if (data && uts) {
//       uts.deepAssignTruthyFirestore(this, data);
//     }
//   }

//   static FromData(data: any, uts: UtilitiesService): MeasurementLog[] {
//     let logs: MeasurementLog[] = [];

//     if (data && data.length) {
//       logs = data.map((item) => new MeasurementLog(item, uts));
//     }

//     return logs;
//   }
// }
