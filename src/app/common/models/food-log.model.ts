import { UtilitiesService } from '../../core/services';

// Verified -
export class UserFoodLog {
  key = '';
  date: Date | null = null;
  breakfastAte = '';
  breakfastDrank = '';
  breakfastTime = '';
  midMorningAte = '';
  midMorningDrank = '';
  midMorningTime = '';
  lunchAte = '';
  lunchDrank = '';
  lunchTime = '';
  midAfternoonAte = '';
  midAfternoonDrank = '';
  midAfternoonTime = '';
  eveningAte = '';
  eveningDrank = '';
  eveningTime = '';
  dinnerAte = '';
  dinnerDrank = '';
  dinnerTime = '';
  bedTimeAte = '';
  bedTimeDrank = '';
  bedTimeTime = '';
  preWorkoutAte = '';
  preWorkoutDrank = '';
  preWorkoutTime = '';
  postWorkoutAte = '';
  postWorkoutDrank = '';
  postWorkoutTime = '';
  otherAte = '';
  otherDrank = '';
  otherTime = '';
  exerciseType = '';
  exerciseDuration = '';
  exerciseIntensity = '';
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): UserFoodLog[] {
    let userFoodLogs: UserFoodLog[] = [];

    if (data && data.length) {
      userFoodLogs = data.map((item) => new UserFoodLog(item, uts));
    }

    return userFoodLogs;
  }
}
