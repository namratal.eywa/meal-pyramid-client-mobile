import { UtilitiesService } from "src/app/core/services";
import { MUser } from './mini-user.model';
import { MLocationn } from './mini-location.model';

// Used & Verified -
export class Appointment {
  key = '';
  mergeSlotKey: string | string[] = '';
  isOOO = false;
  isBlocked = false;
  isPerCalendar = false;
  isHoliday = false;
  isAvailable = false;
  isCompleted = false;
  status = '';
  description = '';
  type = '';
  location: MLocationn = new MLocationn();
  sessionStart: Date = null;
  sessionEnd: Date = null;
  user: MUser = new MUser();
  staff: MUser = new MUser();
  // isActive = false;

  clicked = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): Appointment[] {
    let arr: Appointment[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Appointment(item, uts));
    }

    return arr;
  }

  static FromBackend(res: any, uts: UtilitiesService): Appointment[] {
    let arr: Appointment[] = [];

    if (res && !res.status) {
      throw new Error(res.error.description);
    }

    if (res && res.data && res.data.length) {
      arr = res.data.map((item) => new Appointment(item, uts));
    }

    return arr;
  }
}

export class UserAppointment {
  key = '';
  bookedAt: Date = null;
  bookedBy: MUser = new MUser();
  isCanceled = false;
  canceledAt: Date = null;
  canceledBy: MUser = new MUser();
  appointment: Appointment = new Appointment();
  uid = '';
  membershipKey = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): UserAppointment[] {
    let arr: UserAppointment[] = [];

    if (data && data.length) {
      arr = data.map((item) => new UserAppointment(item, uts));
    }

    return arr;
  }
}
