import { UtilitiesService } from 'src/app/core/services';
import { MUser } from './mini-user.model';

// Used & Verified -
export class CommunityFeed {
  key = '';
  description = '';
  // title = '';
  mediaKey = '';
  mediaName = '';
  mediaType = '';
  mediaPath = '';
  mediaURL = '';
  publishedAt: Date = null;
  staff: MUser = new MUser();

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): CommunityFeed[] {
    let feeds: CommunityFeed[] = [];

    if (data && data.length) {
      feeds = data.map((item) => new CommunityFeed(item, uts));
    }

    return feeds;
  }
}
