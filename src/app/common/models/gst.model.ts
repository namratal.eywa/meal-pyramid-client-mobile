import { UtilitiesService } from '../../core/services';

// Used & Verified -
export class GST {
  address = '';
  name = '';
  number = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }
}
