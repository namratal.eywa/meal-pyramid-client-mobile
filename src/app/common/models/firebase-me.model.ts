import { UtilitiesService } from '../../core/services/utilities.service';

// Verified & Used
export class FirebaseMe {
  authState = false;
  email = '';
  emailVerified = false;
  name = '';
  photoURL = '';
  uid = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      this.authState = true;
      uts.deepAssignTruthyFirestore(this, data);
    }
  }
}
