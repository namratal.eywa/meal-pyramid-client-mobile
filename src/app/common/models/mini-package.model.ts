import { UtilitiesService } from "src/app/core/services";

// Used & Verified -
export class MPackage {
  key = '';
  name = '';
  category = '';
  type = '';
  description = '';
  numberOfMonths: number = null;
  numberOfWeeks: number = null;
  frequency: number = null;
  numberOfSessions: number = null;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): MPackage[] {
    let arr: MPackage[] = [];

    if (data && data.length) {
      arr = data.map((item) => new MPackage(item, uts));
    }

    return arr;
  }
}
