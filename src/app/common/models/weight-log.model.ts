import { UtilitiesService } from "src/app/core/services";


// Verified -
export class UserWeightLog {
  key = '';
  date: Date = null;
  weight: number = null;
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;
  logs: WeightLog[];

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): UserWeightLog[] {
    let userWeightLogs: UserWeightLog[] = [];

    if (data && data.length) {
      userWeightLogs = data.map((item) => new UserWeightLog(item, uts));
    }

    return userWeightLogs;
  }
}

// Verified
// export class UserWeightLog {
//   uid = '';
//   membershipKey = '';
//   startWeight: number = null;
//   startDate: Date = null;
//   targetWeight: number = null;
//   targetDate: Date = null;
//   logs: WeightLog[] = [];

//   constructor(data?: any, uts?: UtilitiesService) {
//     if (data && uts) {
//       uts.deepAssignTruthyFirestore(this, data);

//       if (data.logs) {
//         this.logs = WeightLog.FromData(data.logs, uts);
//       }
//     }
//   }

//   static FromData(data: any, uts: UtilitiesService): UserWeightLog[] {
//     let userWeightLogs: UserWeightLog[] = [];

//     if (data && data.length > 0) {
//       userWeightLogs = data.map((item) => new UserWeightLog(item, uts));
//     }

//     return userWeightLogs;
//   }
// }

export class WeightLog {
  date: Date = null;
  weight: number = null;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): WeightLog[] {
    let logs: WeightLog[] = [];

    if (data && data.length) {
      logs = data.map((item) => new WeightLog(item, uts));
    }

    return logs;
  }
}
