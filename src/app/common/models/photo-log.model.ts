import { UtilitiesService } from '../../core/services';
import { MFilee2 } from './filee.model';

// Used & Verified -
export class UserPhotoLog {
  key = '';
  date: Date | null = null;
  frontalImages: MFilee2[] = [];
  lateralImages: MFilee2[] = [];
  backImages: MFilee2[] = [];
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any[], uts: UtilitiesService): UserPhotoLog[] {
    let userPhotoLogs: UserPhotoLog[] = [];

    if (data && data.length) {
      userPhotoLogs = data.map((item) => new UserPhotoLog(item, uts));
    }

    return userPhotoLogs;
  }
}
