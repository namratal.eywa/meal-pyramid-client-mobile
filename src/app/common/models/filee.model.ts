import { UtilitiesService } from "src/app/core/services";
import { MUser } from './mini-user.model';

// Used & Verified
export class Filee {
  key = '';
  fileName = '';
  filePath = '';
  fileURL = '';
  fileType = '';
  uploadedAt: Date = null;
  uploadedBy: MUser = new MUser();
  user: MUser = new MUser();
  membershipKey = '';
  isActive = false;
  recStatus = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): Filee[] {
    let arr: Filee[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Filee(item, uts));
    }

    return arr;
  }
}

// Used & Verified -
export class Filee2 {
  key = '';
  fileName = '';
  fileType = '';
  filePath = '';
  fileURL = '';
  fileSize: number = null;
  uploadedAt: Date = null;
  uploadedBy: MUser = new MUser();
  uid = '';
  membershipKey = '';
  isActive = false;
  recStatus = false;

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): Filee2[] {
    let arr: Filee2[] = [];

    if (data && data.length) {
      arr = data.map((item) => new Filee2(item, uts));
    }

    return arr;
  }
}

// Used & Verified -
export class MFilee2 {
  fileKey = '';
  fileName = '';
  fileType = '';
  filePath = '';
  fileURL = '';

  constructor(data?: any, uts?: UtilitiesService) {
    if (data && uts) {
      uts.deepAssignTruthyFirestore(this, data);
    }
  }

  static FromData(data: any, uts: UtilitiesService): MFilee2[] {
    let arr: MFilee2[] = [];

    if (data && data.length) {
      arr = data.map((item) => new MFilee2(item, uts));
    }

    return arr;
  }
}
