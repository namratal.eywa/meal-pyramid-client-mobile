import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {
  constructor(private _fb: FormBuilder) {}

  // ! Verify & Delete
  formGroupBuilder(obj: any): FormGroup {
    const keys = Object.keys(obj);
    const formControlsObj: any = {};

    const arr = keys.filter((i: any) => !Array.isArray(obj[i]));

    arr.map((i: string) => {
      formControlsObj[i] = new FormControl(obj[i]);
    });

    return this._fb.group(formControlsObj);
  }

  formArrayBuilder(): FormArray {
    return this._fb.array([]);
  }
}
