import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask
} from '@angular/fire/compat/storage';
import { ListResult } from '@angular/fire/compat/storage/interfaces';
import { async, Observable } from 'rxjs';
import { HTTP } from '@ionic-native/http/ngx';
import { File } from '@ionic-native/file/ngx';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  downloadedFile: Blob;
  bbname: any;

  
  constructor(
    private _aFStorage: AngularFireStorage,
    private http: HttpClient,
    private file: File,
    private Nativehttp: HTTP
  ) { }

  // Verified -
  list(path: string): Observable<ListResult> {
    return this._aFStorage.ref(path).listAll();
  }



  downloadFile(url: string, name: string) {
    this.bbname=name;
    this.Nativehttp.sendRequest(url, { method: "get", responseType: "arraybuffer" }).then(
      httpResponse => {
        console.log("File dowloaded successfully")
        this.downloadedFile = new Blob([httpResponse.data], { type:  'image/jpeg' });
      }
    ).catch(err => {
      console.error(err);
    })
  }

  async writeFile() {
    if (this.downloadedFile == null) return;
    var filename = this.bbname;
    await this.createFile(filename);
    await this.writeToFile(filename);
  }

  async createFile(filename) {
    return this.file.createFile(this.file.externalRootDirectory, filename, false).catch(err => {
      console.error(err);
    })
  }

  writeToFile(filename) {
    return this.file.writeFile(this.file.externalRootDirectory, filename, this.downloadedFile, { replace: true, append: false }).then(createdFile => {
      console.log('File written successfully.');
      console.log(createdFile)
    }).catch(err => {
      console.error(err);
    });
  }
}
