import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable, of } from 'rxjs';
import { switchMap, map, debounceTime } from 'rxjs/operators';

import * as fb from 'firebase/compat/app';
import 'firebase/firestore';

import { UtilitiesService } from './utilities.service';
import { Filee, Filee2, User, UserAdvancedTest, UserAppointment,
   UserAssessment, UserFeedbackLog, UserFoodLog, UserGeneralRecommendation,
    UserHealthHistory, UserMeasurementLog, UserPhotoLog, UserWeightLog ,Notification } from 'src/app/common/models';
import { UserRoles } from 'src/app/common/constants';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  [x: string]: any;
  // Verified -
  private _debounceMillis = 500;

  constructor(
    private _afStore: AngularFirestore,
    private _uts: UtilitiesService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------

  // *
  // * Persons
  // *

  // Verified -
  getPersonByUID(uid: string): Observable<User> {
    return this._afStore
      .collection('users', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(switchMap((data) => of(User.FromDBUser(data, this._uts))));
  }

  // Verified -
  getUserByUID(uid: string): Observable<User> {
    return this._afStore
      .collection('users', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
          .where('roles', 'array-contains-any', [UserRoles.USER])
      )
      .valueChanges()
      .pipe(
        debounceTime(this._debounceMillis),
        // switchMap((data) => of(User.FromDBUser(data, this._uts)))
        map((data) => User.FromDBUser(data, this._uts))
      );
  }

  // Verified -
  // ToDo: AngularFire toPromise() is not emitting, hence used firebase directly. Fix issue & use AngularFire .
  getUserByUIDP(uid: string): Promise<any> {
    return fb.default
      .firestore()
      .collection('users')
      .where('uid', '==', uid)
      .where('isActive', '==', true)
      .where('recStatus', '==', true)
      .where('roles', 'array-contains-any', [UserRoles.USER])
      .get();
  }

  // *
  // * User Sub-collections
  // *

  // Verified -
  getActiveUserAppointments(
    uid: string,
    membershipKey: string
  ): Observable<UserAppointment[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAppointments', (ref) =>
        ref

          .where('isCanceled', '==', false)
          .where('appointment.isCompleted', '==', false)
          .where('membershipKey', '==', membershipKey)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        debounceTime(this._debounceMillis),
        map((arr) =>
          arr.sort((a: any, b: any) =>
            a.sessionStart < b.sessionStart ? 1 : -1
          )
        ),
        map((data) => UserAppointment.FromData(data, this._uts))
      );
  }

  // Verified & not used -
  getUserAppointments(
    uid: string,
    membershipKey: string
  ): Observable<UserAppointment[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAppointments', (ref) =>
        ref.where('membershipKey', '==', membershipKey)
      )
      .valueChanges()
      .pipe(map((data) => UserAppointment.FromData(data, this._uts)));
  }

  // Verified -
  getActiveUserAppointmentsByType(
    uid: string,
    membershipKey: string,
    type: string
  ): Observable<UserAppointment[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAppointments', (ref) =>
        ref
          .where('appointment.type', '==', type)
          .where('isCanceled', '==', false)
          .where('appointment.isCompleted', '==', false)
          .where('membershipKey', '==', membershipKey)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(map((data) => UserAppointment.FromData(data, this._uts)));
  }

  // Verified -
  getActiveUserAssessments(
    uid: string,
    membershipKey: string
  ): Observable<UserAssessment[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAssessments', (ref) =>
        ref
          .where('isCompleted', '==', false)
          .where('uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(map((data) => UserAssessment.FromData(data, this._uts)));
  }

  // Verified -
  getActiveUserAdvancedTests(
    uid: string,
    membershipKey: string
  ): Observable<UserAdvancedTest[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAdvancedTests', (ref) =>
        ref
          .where('isCompleted', '==', false)
          .where('uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(map((data) => UserAdvancedTest.FromData(data, this._uts)));
  }

  // Verified -
  getUserHealthHistory(
    uid: string,
    membershipKey: string
  ): Observable<UserHealthHistory[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userHealthHistory', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(map((data) => UserHealthHistory.FromData(data, this._uts)));
  }

  // Verified -
  getUserAssessmentFiles(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<Filee2[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAssessmentFiles', (ref) =>
        ref
          .where('uid', '==', uid)
          // .where('membershipKey', '==', membershipKey)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) =>
          arr.sort((a: any, b: any) => (a.uploadedAt < b.uploadedAt ? 1 : -1))
        ),
        map((data) => Filee2.FromData(data, this._uts))
      );
  }

  // Verified -
  getUserAdvancedTestFiles(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<Filee2[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userAdvancedTestFiles', (ref) =>
        ref
          .where('uid', '==', uid)
          // .where('membershipKey', '==', membershipKey)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) =>
          arr.sort((a: any, b: any) => (a.uploadedAt < b.uploadedAt ? 1 : -1))
        ),
        map((data) => Filee2.FromData(data, this._uts))
      );
  }

  // Verified -
  // getUserFoodLogs(uid: string): Observable<any> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userProgressDaily', (ref) => ref.orderBy('date', 'desc'))
  //     .valueChanges();
  // }
  getUserFoodLogs(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<UserFoodLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userFoodLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          // .where('membershipKey', '==', membershipKey)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) => arr.sort((a: any, b: any) => (a.date < b.date ? 1 : -1))),
        map((data) => UserFoodLog.FromData(data, this._uts))
      );
  }

  // Verified
  getUserFoodLog(uid: string, key: string): Observable<any> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userProgressDaily')
      .doc(key)
      .valueChanges();
  }

  // Verified
  getUserRecallLogs(uid: string): Observable<any> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userProgressRecall', (ref) => ref.orderBy('date', 'desc'))
      .valueChanges();
  }

  // Verified
  getUserRecallLog(uid: string, key: string): Observable<any> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userProgressRecall')
      .doc(key)
      .valueChanges();
  }

  // Verified -
  // getUserWeightLogs(uid: string): Observable<UserWeightLog[]> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userWeightLogs', (ref) => ref.where('uid', '==', uid))
  //     .valueChanges()
  //     .pipe(map((data) => UserWeightLog.FromData(data, this._uts)));
  // }
  getUserWeightLogs(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<UserWeightLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userWeightLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) => arr.sort((a: any, b: any) => (a.date < b.date ? 1 : -1))),
        map((data) => UserWeightLog.FromData(data, this._uts))
      );
  }

  // Verified -
  getUserWeightLogLatest(
    uid: string,
    membershipKey: string
  ): Observable<UserWeightLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userWeightLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
          .orderBy('date', 'desc')
          .limit(1)
      )
      .valueChanges()
      .pipe(map((data) => UserWeightLog.FromData(data, this._uts)));
  }

  // Verified
  // getUserMeasurementLogs(uid: string): Observable<UserMeasurementLog[]> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userMeasurementLogs', (ref) => ref.where('uid', '==', uid))
  //     .valueChanges()
  //     .pipe(map((data) => UserMeasurementLog.FromData(data, this._uts)));
  // }

  // Verified -
  getUserMeasurementLogs(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<UserMeasurementLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userMeasurementLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) => arr.sort((a: any, b: any) => (a.date < b.date ? 1 : -1))),
        map((data) => UserMeasurementLog.FromData(data, this._uts))
      );
  }

  // Verified -
  getUserMeasurementLogLatest(
    uid: string,
    membershipKey: string
  ): Observable<UserMeasurementLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userMeasurementLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
          .orderBy('date', 'desc')
          .limit(1)
      )
      .valueChanges()
      .pipe(map((data) => UserMeasurementLog.FromData(data, this._uts)));
  }

  // Verified -
  getUserFeedbackLogs(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<UserFeedbackLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userFeedbackLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) => arr.sort((a: any, b: any) => (a.date < b.date ? 1 : -1))),
        map((data) => UserFeedbackLog.FromData(data, this._uts))
      );
  }

  // Verified
  // getUserPhotoLogs(uid: string): Observable<any> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userPhotoLogs', (ref) => ref.orderBy('date', 'desc'))
  //     .valueChanges();
  // }

  // Verified
  // getUserPhotoLog(uid: string, key: string): Observable<any> {
  //   return this._afStore
  //     .collection('users')
  //     .doc(uid)
  //     .collection('userPhotoLogs')
  //     .doc(key)
  //     .valueChanges();
  // }

  // Verified -
  getUserPhotoLogs(
    uid: string,
    // membershipKey: string
    allMembershipKeys: string[] = []
  ): Observable<UserPhotoLog[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userPhotoLogs', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', 'in', allMembershipKeys)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) => arr.sort((a: any, b: any) => (a.date < b.date ? 1 : -1))),
        map((data) => UserPhotoLog.FromData(data, this._uts))
      );
  }

  // Verified -
  getUserGeneralRecommendations(
    uid: string,
    membershipKey: string
  ): Observable<UserGeneralRecommendation[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userGeneralRecommendations', (ref) =>
        ref
          .where('uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(map((data) => UserGeneralRecommendation.FromData(data, this._uts)));
  }

  // Verified -
  getUserNotifications(uid: string): Observable<Notification[]> {
    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userNotifications', (ref) =>
        ref
          .where('isRead', '==', false)
          .where('uid', '==', uid)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) =>
          arr.sort((a: any, b: any) => (a.notifDate < b.notifDate ? 1 : -1))
        ),
        map((data) => Notification.FromData(data, this._uts))
      );
  }

  // -----------------------------------------------------------------------------------------------------
  // Setters
  // -----------------------------------------------------------------------------------------------------

  // Verified
  async setUserFromAuth(data: any): Promise<User> {
    const user = User.FromAuth(data, this._uts);

    const obj = {
      ...this._uts.toDB(user),
      createdAt: fb.default.firestore.FieldValue.serverTimestamp(),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    try {
      await this._afStore
        .collection('users')
        .doc(user.uid)
        .set(obj, { merge: true });

      return user;
    } catch (error) {
      throw new Error('CouldNotSetUser');
    }

    /* ***** Reference ***** */
    // const [err] = await this._uts.promiseHandler(
    //   this._afStore.collection('users').doc(user.uid).set(obj, { merge: true }),
    //   'ERROR_SET_USER'
    // );
    // if (err) {
    //   throw err;
    // }
    /* ********** */
  }

  // Verified
  async setUser(uid: string, user: User | any): Promise<User> {
    const obj = {
      ...this._uts.toDB(user, [
        'orientationAppt.sessionStart',
        'orientationAppt.sessionEnd',
        'subscriptionDuration.start',
        'subscriptionDuration.end',
        'weightProgress.startDate',
        'weightProgress.targetDate'
      ]),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    try {
      await this._afStore
        .collection('users')
        .doc(uid)
        .set(obj, { merge: true });

      return user;
    } catch (error) {
      throw new Error('CouldNotSetUser');
    }
  }

  // ToDo: Verify & Delete
  async setNewUserNotification(
    uid: string,
    key: string,
    data: any
  ): Promise<void> {
    const obj = {
      ...this._uts.toDB(data, ['notifDate']),
      createdAt: fb.default.firestore.FieldValue.serverTimestamp(),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };
    obj.notifDate = fb.default.firestore.FieldValue.serverTimestamp();

    return this._afStore
      .collection('users')
      .doc(uid)
      .collection('userNotifications')
      .doc(key)
      .set(obj, { merge: true });
  }

  // ToDo: Verify & Delete
  async setUserAdvancedTest(
    test: UserAdvancedTest,
    isNew: boolean
  ): Promise<boolean> {
    const obj = {
      ...this._uts.toDB(test),
      ...(isNew && {
        key: this._afStore.collection('userAdvancedTests').doc().ref.id
      }),
      ...(isNew && {
        createdAt: fb.default.firestore.FieldValue.serverTimestamp()
      }),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    const [err] = await this._uts.promiseHandler(
      this._afStore
        .collection('users')
        .doc(test.uid)
        .collection('userAdvancedTests')
        .doc(obj.key)
        .set(obj, { merge: true }),
      'ERROR_SET_USER'
    );
    if (err) {
      throw err;
    }

    return true;
  }

  // ToDo: Verify & Delete
  async setUserAssessmentFile(data: Filee, isNew: boolean): Promise<boolean> {
    const obj = {
      ...this._uts.toDB(data, ['uploadedAt']),
      ...(isNew && {
        key: this._afStore.collection('userAssessmentFiles').doc().ref.id
      }),
      ...(isNew && {
        createdAt: fb.default.firestore.FieldValue.serverTimestamp()
      }),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    const [err] = await this._uts.promiseHandler(
      this._afStore
        .collection('users')
        .doc(obj.user.uid)
        .collection('userAssessmentFiles')
        .doc(obj.key)
        .set(obj, { merge: true }),
      'ERROR_SET_USER'
    );
    if (err) {
      throw err;
    }

    return true;
  }

  // Verified
  setUserWeightLog(data: any): Promise<any> {
    const obj = {
      ...data,
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    return this._afStore
      .collection('users')
      .doc(data.uid)
      .collection('userWeightLogs')
      .doc(data.uid)
      .set(obj, { merge: true });
  }

  // Verified
  setUserMeasurementLog(data: any): Promise<any> {
    const obj = {
      ...data,
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    return this._afStore
      .collection('users')
      .doc(data.uid)
      .collection('userMeasurementLogs')
      .doc(data.uid)
      .set(obj, { merge: true });
  }
}
