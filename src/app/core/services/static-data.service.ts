import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import * as fb from 'firebase/compat/app';
import 'firebase/firestore';
import { UtilitiesService } from './utilities.service';
import { AdvancedTest, CommunityFeed, MLocationn, Package } from 'src/app/common/models';

@Injectable({
  providedIn: 'root'
})
export class StaticDataService {
  constructor(
    private _afStore: AngularFirestore,
    private _uts: UtilitiesService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------

  // Verified -
  getPackages(): Observable<Package[]> {
    return this._afStore
      .collection('packages', (ref) =>
        ref
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
          .orderBy('sort')
      )
      .valueChanges()
      .pipe(map((data) => Package.FromData(data, this._uts)));
  }

  // // ToDo: Verify & Delete, use getMLocations instead
  // getLocations(): Observable<Locationn[]> {
  //   return this._afStore
  //     .collection('locations', (ref) =>
  //       ref
  //         .where('isActive', '==', true)
  //         .where('recStatus', '==', true)
  //         .orderBy('sort')
  //     )
  //     .valueChanges()
  //     .pipe(map((data) => Locationn.FromData(data, this._uts)));
  // }

  // Verified -
  getMLocations(): Observable<MLocationn[]> {
    return this._afStore
      .collection('locations', (ref) =>
        ref
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
          .orderBy('sort')
      )
      .valueChanges()
      .pipe(map((data) => MLocationn.FromData(data, this._uts)));
  }

  // Verified -
  getAllAdvancedTests(): Observable<AdvancedTest[]> {
    return this._afStore
      .collection('advancedTests', (ref) =>
        ref.where('recStatus', '==', true).orderBy('sort')
      )
      .valueChanges()
      .pipe(map((data) => AdvancedTest.FromData(data, this._uts)));
  }

  // Verified -
  getCommunityFeed(): Observable<CommunityFeed[]> {
    return this._afStore
      .collection('communityFeed', (ref) =>
        ref.where('isActive', '==', true).where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        map((arr) =>
          arr.sort((a: any, b: any) => (a.publishedAt < b.publishedAt ? 1 : -1))
        ),
        map((data) => CommunityFeed.FromData(data, this._uts))
      );
  }

  // Verified
  getNewKey(collectionName: string, userSubColl = false, userUID = ''): string {
    if (userSubColl) {
      return this._afStore
        .collection('users')
        .doc(userUID)
        .collection(collectionName)
        .doc().ref.id;
    }

    return this._afStore.collection(collectionName).doc().ref.id;
  }

  // -----------------------------------------------------------------------------------------------------
  // Setters
  // -----------------------------------------------------------------------------------------------------

  // Verified
  setData(
    key: string,
    collectionName: string,
    data: any,
    isNew = false,
    dateFields: string[] = [],
    userSubColl = false,
    subKey = ''
  ): Promise<any> {
    const obj = {
      ...this._uts.toDB(data, dateFields),
      ...(isNew && {
        createdAt: fb.default.firestore.FieldValue.serverTimestamp()
      }),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    if (userSubColl) {
      return this._afStore
        .collection('users')
        .doc(key)
        .collection(collectionName)
        .doc(subKey)
        .set(obj, { merge: true });
    }

    return this._afStore
      .collection(collectionName)
      .doc(key)
      .set(obj, { merge: true });
  }
}
