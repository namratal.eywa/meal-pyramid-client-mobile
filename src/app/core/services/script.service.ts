import { Injectable } from '@angular/core';

interface Scripts {
  name: string;
  src: string;
}

export const ScriptStore: Scripts[] = [
  {
    name: 'payUBolt',
    src: 'https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js'
  }
];

declare const document: any;

// Verified
@Injectable({
  providedIn: 'root'
})
export class ScriptService {
  private scripts: any = {};

  constructor() {
    ScriptStore.forEach((script: any) => {
      this.scripts[script.name] = {
        loaded: false,
        src: script.src
      };
    });
  }

  private loadScript(name: string): Promise<any> {
    return new Promise((resolve) => {
      if (this.scripts[name].loaded) {
        resolve({ script: name, loaded: true, status: 'Already Loaded' });
      } else {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.scripts[name].src;

        if (script.readyState) {
          // IE
          script.onreadystatechange = () => {
            if (
              script.readyState === 'loaded' ||
              script.readyState === 'complete'
            ) {
              script.onreadystatechange = null;
              this.scripts[name].loaded = true;
              resolve({ script: name, loaded: true, status: 'Loaded' });
            }
          };
        } else {
          // Others
          script.onload = () => {
            this.scripts[name].loaded = true;
            resolve({ script: name, loaded: true, status: 'Loaded' });
          };
        }

        script.onerror = () =>
          resolve({ script: name, loaded: false, status: 'Loaded' });

        document.getElementsByTagName('head')[0].appendChild(script);
      }
    });
  }

  load(...scripts: string[]): Promise<any[]> {
    const promises: any[] = [];
    scripts.forEach((script) => promises.push(this.loadScript(script)));

    return Promise.all(promises);
  }

  loadPayUBolt(): Promise<any> {
    return new Promise((resolve) => {
      if (this.scripts.payUBolt.loaded) {
        resolve({ script: 'payUBolt', loaded: true, status: 'Already Loaded' });
      } else {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.id = 'bolt';
        script.setAttribute('bolt-color', 'FF4081');
        script.setAttribute(
          'bolt-logo',
          'https://firebasestorage.googleapis.com/v0/b/stunning-prism-221707.appspot.com/o/assets%2Fmp-logo.png?alt=media&token=613e9e80-f283-4368-8394-5a578261cee0'
        );
        script.src = this.scripts.payUBolt.src;

        script.onload = () => {
          this.scripts.payUBolt.loaded = true;
          resolve({ script: 'payUBolt', loaded: true, status: 'Loaded' });
        };

        script.onerror = () =>
          resolve({ script: 'payUBolt', loaded: false, status: 'Loaded' });

        document.getElementsByTagName('head')[0].appendChild(script);
      }
    });
  }
}
