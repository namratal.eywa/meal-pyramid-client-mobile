import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Session } from 'src/app/common/models/session.model';

import { UtilitiesService } from './utilities.service';

@Injectable({
  providedIn: 'root'
})
export class SessionnService {
  // Verified -
  private _debounceMillis = 500;

  constructor(
    private _afStore: AngularFirestore,
    private _uts: UtilitiesService
  ) {}

  // Verified -
  getSessionsByUser(uid: string, membershipKey: string): Observable<Session[]> {
    return this._afStore
      .collection('sessions', (ref) =>
        ref
          .where('user.uid', '==', uid)
          .where('membershipKey', '==', membershipKey)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        debounceTime(this._debounceMillis),
        map((arr) =>
          arr.sort((a: any, b: any) =>
            a.sessionNumber < b.sessionNumber ? 1 : -1
          )
        ),
        map((data) => Session.FromData(data, this._uts))
      );
  }

  getAllSessionsByUser(uid: string): Observable<Session[]> {
    return this._afStore
      .collection('sessions', (ref) =>
        ref
          .where('user.uid', '==', uid)
          .where('isActive', '==', true)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(
        debounceTime(this._debounceMillis),
        map((arr) =>
          arr.sort((a: any, b: any) => (a.updatedAt < b.updatedAt ? 1 : -1))
        ),
        map((data) => Session.FromData(data, this._uts))
      );
  }
}
