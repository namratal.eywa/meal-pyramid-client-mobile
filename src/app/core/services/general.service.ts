import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MenuController, LoadingController } from '@ionic/angular';
import * as fb from 'firebase/compat/app';
import { ErrorMessages, RouteIDs } from 'src/app/common/constants/general.constants';
import { DialogBox } from 'src/app/common/models';
import { DialogBoxComponent } from 'src/app/shared/dialog-box';
import { AuthService } from './auth.service';
import { SessionnService } from './sessionn.service';
import { SharedService } from './shared.service';
import { SnackBarService } from './snack-bar.service';
import { UtilitiesService } from './utilities.service';


@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  db = fb.default.firestore();

  constructor(
    private _afStore: AngularFirestore,
    private _uts: UtilitiesService,
    private _as: AuthService,
    private _router: Router,
    private _dialog: MatDialog,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private _ss: SessionnService,
    private _shs: SharedService,
    private _sb: SnackBarService,
  ) { }

  // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------

  // Verified -
  getNewKey(collectionName: string, userSubColl = false, userUID = ''): string {
    if (userSubColl) {
      return this.db
        .collection('users')
        .doc(userUID)
        .collection(collectionName)
        .doc().id;
    }

    return this.db.collection(collectionName).doc().id;
  }

  // -----------------------------------------------------------------------------------------------------
  // Setters
  // -----------------------------------------------------------------------------------------------------

  // Verified -
  setData(
    key: string,
    collectionName: string,
    data: any,
    isNew = false,
    dateFields: string[] = [],
    userSubColl = false,
    subKey = ''
  ): Promise<any> {
    const obj = {
      ...this._uts.toDB(data, dateFields),
      ...(isNew && {
        createdAt: fb.default.firestore.FieldValue.serverTimestamp()
      }),
      updatedAt: fb.default.firestore.FieldValue.serverTimestamp()
    };

    if (userSubColl) {
      return this._afStore
        .collection('users')
        .doc(key)
        .collection(collectionName)
        .doc(subKey)
        .set(obj, { merge: true });
    }

    return this._afStore
      .collection(collectionName)
      .doc(key)
      .set(obj, { merge: true });
  }


  async onLogout() {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'logout';
    dialogBox.title = 'Logout';
    dialogBox.message = '  Are you sure you want to log out ? ';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);
    dialogRef.afterClosed().subscribe(async (res: any) => {
      if (!res) {
        return;
      }
      try {
        setTimeout(async () => {
          await this._as.logout();
          this._router.navigate([RouteIDs.LOGIN]);
        }, 750);
      } catch (err) {
        this._sb.openErrorSnackBar(ErrorMessages.DELETE_FAILED);
      }
    });

  }

  async closeApp() {
    const dialogBox = new DialogBox();
    dialogBox.actionFalseBtnTxt = 'No';
    dialogBox.actionTrueBtnTxt = 'Yes';
    dialogBox.icon = 'logout';
    dialogBox.title = 'Exist';
    dialogBox.message = 'Do you want to close the app?';
    dialogBox.actionFalseBtnStyle = 'e-mat-button--matgrey500';
    dialogBox.actionTrueBtnStyle = 'e-mat-button--eblue400';
    this._shs.setDialogBox(dialogBox);
    const dialogRef = this._dialog.open(DialogBoxComponent);
    dialogRef.beforeClosed().subscribe(async (res: any) => {
      if (!res) {
        return;
      }
      try {
          navigator['app'].exitApp();
      } catch (err) {
        this._sb.openErrorSnackBar(ErrorMessages.DELETE_FAILED);
      }
    });
  }
}
