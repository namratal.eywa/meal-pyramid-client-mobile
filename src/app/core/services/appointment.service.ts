import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Appointment } from 'src/app/common/models';
import { UtilitiesService } from './utilities.service';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  constructor(
    private _afStore: AngularFirestore,
    private _uts: UtilitiesService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------

  // Verified -
  // ToDo: Fetch from UserAppointments instead of Appointmemnts so that
  // * older membership appointments can be filtered
  getAppointmentsByUser(
    uid: string,
    membershipKey: string
  ): Observable<Appointment[]> {
    return this._afStore
      .collection('appointments', (ref) =>
        ref
          .where('user.uid', '==', uid)
          // .where('membershipKey', '==', membershipKey)
          .where('recStatus', '==', true)
      )
      .valueChanges()
      .pipe(map((data) => Appointment.FromData(data, this._uts)));
  }
}
