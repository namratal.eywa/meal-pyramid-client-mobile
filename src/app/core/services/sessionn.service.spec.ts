import { TestBed } from '@angular/core/testing';

import { SessionnService } from './sessionn.service';

describe('SessionnService', () => {
  let service: SessionnService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SessionnService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
