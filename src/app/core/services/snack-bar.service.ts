import { Injectable } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition
} from '@angular/material/snack-bar';

// Verified -
@Injectable({
  providedIn: 'root'
})
export class SnackBarService {
  constructor(private _sb: MatSnackBar) {}

  openSnackBar(
    message = '',
    panelClass: string[] = ['e-snackbar', 'e-snackbar--info'],
    action = 'X',
    duration = 3000,
    horizontalPosition: MatSnackBarHorizontalPosition = 'center',
    verticalPosition: MatSnackBarVerticalPosition = 'bottom'
  ) {
    this._sb.open(message, action, {
      duration,
      panelClass,
      horizontalPosition,
      verticalPosition
    });
  }

  openSuccessSnackBar(message = '', duration = 4000) {
    this._sb.open(message, 'X', {
      duration,
      panelClass: ['e-snackbar', 'e-snackbar--success']
    });
  }

  openWarnSnackBar(message = '', duration = 4000) {
    this._sb.open(message, 'X', {
      duration,
      panelClass: ['e-snackbar', 'e-snackbar--warn']
    });
  }

  openErrorSnackBar(message = '', duration = 4000) {
    this._sb.open(message, 'X', {
      duration,
      panelClass: ['e-snackbar', 'e-snackbar--error']
    });
  }

  openInfoSnackBar(message = '', duration = 4000) {
    this._sb.open(message, 'X', {
      duration,
      panelClass: ['e-snackbar', 'e-snackbar--info']
    });
  }
}
