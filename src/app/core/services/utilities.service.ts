import { Injectable, Optional, SkipSelf } from '@angular/core';
import * as isEqual from 'lodash.isequal';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {
  constructor(@Optional() @SkipSelf() utilitiesService: UtilitiesService) {
    if (utilitiesService) {
      throw new Error('UtilitiesService is already loaded!');
    }
  }

  // Verified -
  // // ToDo: Replace target[key] === null condition
  deepAssignTruthyFirestore(target: any, source: any): any {
    for (const key of Object.keys(target)) {
      if (typeof source[key] !== 'object' && typeof target[key] !== 'object') {
        if (source[key]) {
          target[key] = source[key];
        }
      } else if (Array.isArray(source[key]) && source[key].length > 0) {
        target[key] = source[key];
      } else if (source[key] && Object.keys(source[key]).includes('seconds')) {
        target[key] = new Date(source[key].seconds * 1000);
      } else if (
        (typeof source[key] === 'boolean' || typeof source[key] === 'number') &&
        target[key] === null
      ) {
        target[key] = source[key];
      } else if (source[key]) {
        this.deepAssignTruthyFirestore(target[key], source[key]);
      }
    }

    return target;
  }

  // Verified -
  sanitize(str: string): string {
    return str.trim().replace(/[^A-Za-z0-9]+/g, '');
  }

  // Verified -
  // ToDo: Delete
  async promiseHandler(promise, improved) {
    return promise
      .then((data) => [null, data || true])
      .catch((err) => {
        if (improved) {
          Object.assign(err, { initial: err.message });
          Object.assign(err, { message: improved });
        }
        return [err];
      });
  }

  // Verified -
  dashify(str: string): string {
    return str
      .trim()
      .replace(/[^A-Za-z0-9]+/g, '-')
      .toLowerCase();
  }

  // Verified -
  dashifyFileName(str: string): string {
    return str
      .trim()
      .split('.')
      .slice(0, -1)
      .toString()
      .replace(/[^A-Za-z0-9]+/g, '-');
  }

  // Verified -
  isValidDate(value: any): boolean {
    return Boolean(value instanceof Date && value.getTime());
  }

  // Verified -
  // * Works only for first level date-fields in a map.
  toDB(data: any, dateFields?: string[]): any {
    const obj = JSON.parse(JSON.stringify(data));

    if (dateFields && dateFields.length) {
      dateFields.map((item: string) => {
        const a = item.split('.');

        if (Object.prototype.hasOwnProperty.call(data, a[0])) {
          obj[a[0]] = data[a[0]];
        }
      });
    }

    return obj;
  }

  // Verified -
  shallowEqual(object1: any, object2: any): boolean {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (const key of keys1) {
      if (object1[key] !== object2[key]) {
        return false;
      }
    }

    return true;
  }

  // Verified -
  deepEqual(object1: any, object2: any): boolean {
    return isEqual(object1, object2);
  }

  // Verified -
  getSearchArray(str1: string, str2: string): string[] {
    const arr = [];
    let previousString1 = '';
    let previousString2 = '';

    str1.split('').map((item) => {
      previousString1 += item.toLowerCase();
      arr.push(previousString1);
    });

    previousString1 += ' ';
    str2.split('').map((item) => {
      previousString1 += item.toLowerCase();
      arr.push(previousString1);

      previousString2 += item.toLowerCase();
      arr.push(previousString2);
    });

    return arr;
  }

  // Verified -
  sleep(milliSecs: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, milliSecs));
  }

  // Verified -
  computeDifference(val1: number, val2: number): string {
    let res = null;

    if (val1 && val2 && !Number.isNaN(val1) && !Number.isNaN(val2)) {
      const diff = val1 - val2;

      const m = Number((Math.abs(diff) * 100).toPrecision(15));
      const roundedDiff = (Math.round(m) / 100) * Math.sign(diff);

      res = roundedDiff > 0 ? `+${roundedDiff}` : roundedDiff.toString();
    }

    return res;
  }
}
