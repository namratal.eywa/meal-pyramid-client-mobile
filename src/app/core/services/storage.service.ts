import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AngularFireStorage,
  AngularFireStorageReference,
  AngularFireUploadTask
} from '@angular/fire/compat/storage';
import { ListResult } from '@angular/fire/compat/storage/interfaces';
import { Observable } from 'rxjs';
import { UtilitiesService } from './utilities.service';
import { FileDownload } from "capacitor-plugin-filedownload";
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { Downloader } from '@ionic-native/downloader/ngx';
import { Capacitor } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  downloadedFile: Blob;
  constructor(
    private _aFStorage: AngularFireStorage,
    private _uts: UtilitiesService,
    // private http: HttpClient,
    // private fileOpener: FileOpener,
    // private httpnative: HTTP,
    // private downloader: Downloader
  ) { }

  // Verified -
  list(path: string): Observable<ListResult> {
    return this._aFStorage.ref(path).listAll();
  }


  upload(
    file: File,
    dirPath?: string,
    fileNamePrefix?: string,
    userUID?: string
  ): {
    fName: string;
    fPath: string;
    uploadRef: AngularFireStorageReference;
    uploadProgress$: Observable<number>;
    snapshotChanges$: Observable<firebase.default.storage.UploadTaskSnapshot>;
  } {
    const fileType = file.type.split('/').slice(-1).toString();
    const fileName = fileNamePrefix
      ? `${fileNamePrefix}_${Math.floor(Date.now() / 100)}.${fileType}`
      : `MEALpyramid_${this._uts.dashifyFileName(file.name)}_${Math.floor(
          Date.now() / 100
        )}.${fileType}`;

    let filePath;
    if (userUID) {
      filePath = dirPath
        ? `${dirPath}/${userUID}/${fileName}`
        : `files/${userUID}/${fileName}`;
    } else {
      filePath = dirPath ? `${dirPath}/${fileName}` : `files/${fileName}`;
    }

    const ref: AngularFireStorageReference = this._aFStorage.ref(filePath);
    const task: AngularFireUploadTask = this._aFStorage.upload(filePath, file);

    return {
      fName: fileName,
      fPath: filePath,
      uploadRef: ref,
      uploadProgress$: task.percentageChanges(),
      snapshotChanges$: task.snapshotChanges()
    };
  }
  
async statApplicationDirectory() {
  const info = await Filesystem.stat({path: '/', directory: Directory.External});
  console.log('Stat Info: ', info);
}
  // Verified -
  async download(url: string, name: string): Promise<void> {
    console.log(url, name, 'downloading.');
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = () => {
      const blob = xhr.response;
      const link = document.createElement('a');
      if (link.download !== undefined) {
        const url = URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', name);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    };
    xhr.open('GET', url);
    xhr.send();
    FileDownload.download({
      uri: url,
      fileName: name,
    }).then((res) => {
      console.log(res.path);
    }).catch(err => {
      console.log(err);
    }).then((savedFile: void) =>{
      console.log('File saved to:', savedFile)
    });
  try {
    Filesystem.writeFile({
      path: 'secrets/text.txt',
      data: "This is a test",
      directory: Directory.Documents,
      encoding: Encoding.UTF8
    });
  } catch(e) {
    console.error('Unable to write file', e);
  }

  const response = await fetch(url);
  // convert to a Blob
  const blob = await response.blob();
  // convert to base64 data, which the Filesystem plugin requires
  const base64Data = await this.convertBlobToBase64(blob) as string;
  const savedFile= async () => { await Filesystem.writeFile({
    path: 'Download/images' ,
    data: base64Data,
    directory: Directory.Data
  });
};
  
  // helper function

    // const writeSecretFile = async () => {
    //   await Filesystem.writeFile({
    //       path: name,
    //       data: url,
    //       directory: Directory.External,
    //       encoding: Encoding.UTF8,
    //       recursive: true
    //   });
    //   };
    //   const readSecretFile = async () => {
    //   const contents = await Filesystem.readFile({
    //       path: name,
    //       directory: Directory.External,
    //       encoding: Encoding.UTF8,    
    //   });      };
    //   const readFilePath = async () => {
    //     const contents = await Filesystem.readFile({
    //       path: name
    //     });
    //     console.log('secrets:', JSON.stringify(contents));
    //   }


      
      
    //   readFilePath();
    //   writeSecretFile();
    //   readSecretFile();   
  //   let fileExtn=name.split('.').reverse()[0];
  //   let fileMIMEType=this.getMIMEtype(fileExtn);
  //            this.fileOpener.open("file:///storage/emulated/0/download/"+ name+"", fileMIMEType)
  //                   .then(() => console.log('File is opened'))
  //                   .catch(e => console.log('Error openening file', e));
  // }

  // getMIMEtype(extn){
  //   let ext=extn.toLowerCase();
  //   let MIMETypes={
  //     'txt' :'text/plain',
  //     'docx':'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  //     'doc' : 'application/msword',
  //     'pdf' : 'application/pdf',
  //     'jpg' : 'image/jpeg',
  //     'bmp' : 'image/bmp',
  //     'png' : 'image/png',
  //     'xls' : 'application/vnd.ms-excel',
  //     'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  //     'rtf' : 'application/rtf',
  //     'ppt' : 'application/vnd.ms-powerpoint',
  //     'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
  //   }
  //   return MIMETypes[ext];
  savedFile();
  }

  // Verified -
  delete(path: string): Promise<void> {
    return this._aFStorage.ref(path).delete().toPromise();
  }

   convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader;
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
   reader.readAsDataURL(blob);
  });

}
