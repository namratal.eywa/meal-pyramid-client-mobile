import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { BehaviorSubject, Observable, EMPTY, Subject } from 'rxjs';
import {
  catchError,
  takeUntil,
  first,
  switchMap,
  debounceTime
} from 'rxjs/operators';
import { User } from 'src/app/common/models';
import { UserService } from './user.service';

// Verified
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _meBS$: BehaviorSubject<Observable<User>> = new BehaviorSubject(null);
  private _notifier$: Subject<void> = new Subject();
  private readonly _me$: Observable<User> = this._meBS$.asObservable().pipe(
    debounceTime(500),
    switchMap((data) => data)
  );

  constructor(private _afAuth: AngularFireAuth, private _us: UserService) {
    this._afAuth.authState
      .pipe(
        catchError(() => EMPTY),
        takeUntil(this._notifier$)
      )
      .subscribe(async (fbUser) => {
        if (fbUser) {
          const user$ = this._us.getUserByUID(fbUser.uid);
          this._meBS$.next(user$);
        }
      });
  }

  ngOnDestroy(): void {
    this._notifier$.next();
    this._notifier$.complete();
  }

  // Verified -
  signInWithEmail({ email, password }): Promise<any> {
    return this._afAuth.signInWithEmailAndPassword(email, password);
  }

  // Verified -
  logout(): Promise<void> {
    return this._afAuth.signOut();
  }

  // Verified -
  getFirebaseMeP(): Promise<any> {
    return this._afAuth.authState.pipe(first()).toPromise();
  }

  // Verified -
  getMe(): Observable<User> {
    return this._me$;
  }

  setFirebaseMe({ email, password }): Promise<any> {
    return this._afAuth.createUserWithEmailAndPassword(email, password);
  }

  sendPasswordResetEmail(email: string): Promise<void> {
    return this._afAuth.sendPasswordResetEmail(email);
  }
}
