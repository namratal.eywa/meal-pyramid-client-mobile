import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UtilitiesService } from './utilities.service';
import { APIFunctions } from 'src/app/common/constants';
import { Appointment, APIResponse } from 'src/app/common/models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  private _headers = {
    'Content-Type': 'application/json'
  };

  constructor(private _http: HttpClient, private _uts: UtilitiesService) {}

  // -----------------------------------------------------------------------------------------------------
  // Getters
  // -----------------------------------------------------------------------------------------------------

  // Verified -
  getAvailableOrientationSlots(
    userUID: string,
    dateString: string,
    locationKey: string
  ): Observable<Appointment[]> {
    let params = new HttpParams();
    params = params.append('userUID', userUID);
    params = params.append('dateString', dateString);
    params = params.append('locationKey', locationKey);

    return this._http
      .get(
        `${environment.api.baseURL}${APIFunctions.GET_AVAILABLE_ORIENTATION_SLOTS}`,
        { params }
      )
      .pipe(map((res) => Appointment.FromBackend(res, this._uts)));
  }

  // Verified -
  getAvailableAdvancedTestingSlots(
    userUID: string,
    dateString: string,
    locationKey: string
  ): Observable<Appointment[]> {
    let params = new HttpParams();
    params = params.append('userUID', userUID);
    params = params.append('dateString', dateString);
    params = params.append('locationKey', locationKey);

    return this._http
      .get(
        `${environment.api.baseURL}${APIFunctions.GET_AVAILABLE_ADVANCED_TESTING_SLOTS}`,
        { params }
      )
      .pipe(map((res) => Appointment.FromBackend(res, this._uts)));
  }

  // Verified -
  getAvailableRecallSlots(
    userUID: string,
    dateString: string,
    locationKey: string
  ): Observable<Appointment[]> {
    let params = new HttpParams();
    params = params.append('userUID', userUID);
    params = params.append('dateString', dateString);
    params = params.append('locationKey', locationKey);

    return this._http
      .get(
        `${environment.api.baseURL}${APIFunctions.GET_AVAILABLE_RECALL_SLOTS}`,
        { params }
      )
      .pipe(map((res) => Appointment.FromBackend(res, this._uts)));
  }

  // -----------------------------------------------------------------------------------------------------
  // Setters
  // -----------------------------------------------------------------------------------------------------

  // Verified -
  createPayment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http
      .post(
        `${environment.api.baseURL}${APIFunctions.CREATE_PAYMENT}`,
        body,
        options
      )
      .pipe(map((res) => new APIResponse(res)));
  }

  // Verified -
  setPayment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http
      .post(
        `${environment.api.baseURL}${APIFunctions.SET_PAYMENT}`,
        body,
        options
      )
      .pipe(map((res) => new APIResponse(res)));
  }

  // Verified -
  setOrientationAppointment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http.post(
      `${environment.api.baseURL}${APIFunctions.SET_ORIENTATION_APPOINTMENT}`,
      body,
      options
    );
  }

  // Verified -
  cancelOrientationAppointment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http.post(
      `${environment.api.baseURL}${APIFunctions.CANCEL_ORIENTATION_APPOINTMENT}`,
      body,
      options
    );
  }

  // Verified -
  setRecallAppointment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http.post(
      `${environment.api.baseURL}${APIFunctions.SET_RECALL_APPOINTMENT}`,
      body,
      options
    );
  }

  // Verified -
  cancelRecallAppointment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http.post(
      `${environment.api.baseURL}${APIFunctions.CANCEL_RECALL_APPOINTMENT}`,
      body,
      options
    );
  }

  // Verified -
  setAdvancedTestingAppointment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http.post(
      `${environment.api.baseURL}${APIFunctions.SET_ADVANCED_TESTING_APPOINTMENT}`,
      body,
      options
    );
  }

  // Verified -
  cancelAdvancedTestingAppointment(body: any): Observable<any> {
    const options = {
      headers: this._headers
    };
    return this._http.post(
      `${environment.api.baseURL}${APIFunctions.CANCEL_ADVANCED_TESTING_APPOINTMENT}`,
      body,
      options
    );
  }

  // Verified
  // subscribeToPackage(body: {
  //   userUID: string;
  //   packageKey: string;
  //   packageStartDateStr: string;
  // }): Observable<any> {
  //   const options = {
  //     headers: this._headers
  //   };
  //   return this._http.post(
  //     `${environment.api.baseURL}${APIFunctions.SUBSCRIBE_PACKAGE}`,
  //     body,
  //     options
  //   );
  // }

  // Verified -
  sendWelcomeEmail(body: { name: string; email: string }): Promise<any> {
    const options = {
      headers: this._headers
    };

    return this._http
      .post(
        `${environment.api.baseURL}${APIFunctions.EMAIL_WELCOME}`,
        body,
        options
      )
      .toPromise();
  }

  // Verified -
  sendHelloEmail(body: {
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
  }): Promise<any> {
    const options = {
      headers: this._headers
    };

    return this._http
      .post(
        `${environment.api.baseURL}${APIFunctions.EMAIL_REGISTERED}`,
        body,
        options
      )
      .toPromise();
  }

  // Verified -
  notifyAllAdmins(body: {
    message: string;
    isSystemGenerated: boolean;
  }): Promise<any> {
    const options = {
      headers: this._headers
    };

    return this._http
      .post(
        `${environment.api.baseURL}${APIFunctions.NOTIFY_ADMINS}`,
        body,
        options
      )
      .toPromise();
  }
}
