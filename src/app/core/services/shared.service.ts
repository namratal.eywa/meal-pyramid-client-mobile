import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DialogBox } from 'src/app/common/models/dialog-box.model';


// Verified
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private _dialogBox$: BehaviorSubject<DialogBox> =
    new BehaviorSubject<DialogBox>(new DialogBox());

  private _progressTab$: BehaviorSubject<number> = new BehaviorSubject(1);
  private _healthHistoryTab$: BehaviorSubject<number> = new BehaviorSubject(0);
  private _homeSnackBar$: BehaviorSubject<{
    type: string;
    message: string;
  } | null> = new BehaviorSubject(null);

  dialogBox$: Observable<DialogBox> = this._dialogBox$.asObservable();

  constructor() {}

  getProgressTabSelected(): Observable<number> {
    return this._progressTab$.asObservable();
  }

  getHealthHistoryTabSelected(): Observable<number> {
    return this._healthHistoryTab$.asObservable();
  }

  getHomeSnackBar(): Observable<{ type: string; message: string } | null> {
    return this._homeSnackBar$.asObservable();
  }

  setDialogBox(data: DialogBox): void {
    this._dialogBox$.next(data);
  }

  setProgressTabSelected(n = 0): void {
    this._progressTab$.next(n);
  }

  setHealthHistoryTabSelected(n = 0): void {
    this._healthHistoryTab$.next(n);
  }

  setHomeSnackBar(obj: { type: string; message: string } | null): void {
    this._homeSnackBar$.next(obj);
  }
}
