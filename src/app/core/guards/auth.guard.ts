import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { ErrorMessages, RouteIDs } from 'src/app/common/constants';
import { AuthService, SnackBarService, UserService } from '../services';


// Verified -
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private _as: AuthService,
    private _router: Router,
    private _sb: SnackBarService,
    private _us: UserService
  ) {}

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    try {
      const fbUser = await this._as.getFirebaseMeP();
      if (!fbUser) {
        // this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_LOGGED_IN);
        this._router.navigate([RouteIDs.LOGIN]);
        return false;
      }

      const querySnapshot = await this._us.getUserByUIDP(fbUser.uid);
      if (querySnapshot.empty) {
        this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_AUTHORIZED);
        this._router.navigate([RouteIDs.LOGIN]);
        return false;
      }

      return true;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_AUTHORIZED);
      this._router.navigate([RouteIDs.LOGIN]);
      return false;
    }
  }

  async canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    try {
      const fbUser = await this._as.getFirebaseMeP();
      if (!fbUser) {
        this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_LOGGED_IN);
        this._router.navigate([RouteIDs.LOGIN]);
        return false;
      }

      const querySnapshot = await this._us.getUserByUIDP(fbUser.uid);
      if (querySnapshot.empty) {
        this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_AUTHORIZED);
        this._router.navigate([RouteIDs.LOGIN]);
        return false;
      }

      return true;
    } catch (err) {
      this._sb.openErrorSnackBar(ErrorMessages.USER_NOT_AUTHORIZED);
      this._router.navigate([RouteIDs.LOGIN]);
      return false;
    }
  }
}
