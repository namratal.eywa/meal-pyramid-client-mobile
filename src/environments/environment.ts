export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAxZm9cK9I3M0P31Iy7llWZE0dJkIg0PlU',
    authDomain: 'meal-pyramid-dev.firebaseapp.com',
    projectId: 'meal-pyramid-dev',
    storageBucket: 'meal-pyramid-dev.appspot.com',
    messagingSenderId: '294748298017',
    appId: '1:294748298017:web:0819fb1bdbbd87ee1ebc60'
  },
  api: {
    baseURL: 'https://us-central1-meal-pyramid-dev.cloudfunctions.net/app'
  },
  staff: {
    uid_nm2RJCW6lnPLJuNjFJWdn3B5cXx1: {
      name: 'Kinita Kadakia Patel',
      photoURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/displayPictures%2Fnm2RJCW6lnPLJuNjFJWdn3B5cXx1%2Fdp-kinita.png?alt=media&token=8f0ccbf9-ffbe-4ec0-a899-ec20051fa7fd'
    },
    uid_TVUvv16dBZhP4tSS4bPLCwVGnci2: {
      name: 'Nutritionist1',
      photoURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/displayPictures%2FTVUvv16dBZhP4tSS4bPLCwVGnci2%2Fdp-nutritionist1.png?alt=media&token=1e37fc42-1c8e-4889-9321-5a511892ee4d'
    },
    uid_mocIhriAWSOoo8gmwmsXwi6CJpT2: {
      name: 'Lincia Creado',
      photoURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/displayPictures%2FmocIhriAWSOoo8gmwmsXwi6CJpT2%2Fdp-lincia.png?alt=media&token=e9706a57-ab32-4072-b2a6-fd76b3f5a009'
    }
  },
  avatar: {
    directory: 'displayPictures',
    userMale: 'assets/images/avatar-user-male.svg',
    userFemale: 'assets/images/avatar-user-female.svg'
  },
  // payU: {
  //   baseURL: 'https://sandboxsecure.payu.in/_payment',
  //   merchantKey: 'rWYGsF4x',
  //   successURL:
  //     'https://us-central1-stunning-prism-221707.cloudfunctions.net/app/payment/success',
  //   failureURL:
  //     'https://us-central1-stunning-prism-221707.cloudfunctions.net/app/payment/failure'
  // },
  payment: {
    currency: 'INR',
    razorPay: {
      key: 'rzp_test_UEpkoReBkIhjZr'
    }
  },
  bodyMeasurementGuide: {
    imgURL:
      'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fbody-measurement-guide.jpg?alt=media&token=b3ef3785-6e70-4f59-9193-87a10656fd25'
  },
  photoLogGuide: {
    imgURL:
      'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fbody-photo.jpg?alt=media&token=06811871-9209-497a-a571-39409a41a4f6'
  },
  heroSection: {
    kinita:
      'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fkinita-hero-section.jpeg?alt=media&token=8edd946f-4b00-439d-92bd-a2e6f4e8a6ee'
  },
  support: {
    chatURL: 'https://wa.me/919892686118'
  },
  login: {
    bg1: {
      imgURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-1.jpeg?alt=media&token=e0a52c2f-d679-470b-b860-384cbb87cedd'
    },
    bg2: {
      imgURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-2.jpeg?alt=media&token=f8fceb39-12af-4ea3-9d08-54b05a907038'
    },
    bg3: {
      imgURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-3.jpeg?alt=media&token=b51c339d-2cad-4fd9-9d68-1332540ff2fc'
    },
    bg4: {
      imgURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-4.jpeg?alt=media&token=3b7e3ea2-cc61-4113-b5df-42dffaca3ec6'
    },
    bg5: {
      imgURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-5.jpeg?alt=media&token=f5b680ef-d737-442f-824c-36b61c759f0c'
    },
    bg6: {
      imgURL:
        'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Flogin-6.jpeg?alt=media&token=40a3b13c-6a85-406f-99b9-743d925669b9'
    }
  },
  video: {
    intro:
      'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fintro-video.mp4?alt=media&token=4171c35d-d049-41a5-a3c3-f1e542c68481',
    welcome:
      'https://firebasestorage.googleapis.com/v0/b/meal-pyramid-dev.appspot.com/o/assets%2Fwelcome-video.mp4?alt=media&token=42e2431b-8249-4773-b1dc-fc9098f2e71a'
  },
  gst: {
    percentage: 18,
    multiplier: 1.18
  }
};

// baseURL: 'https://us-central1-stunning-prism-221707.cloudfunctions.net/app'
// baseURL: 'http://localhost:5000/stunning-prism-221707/us-central1/app'
// baseURL: 'https://us-central1-meal-pyramid-dev.cloudfunctions.net/app'
// baseURL: 'http://localhost:5000/meal-pyramid-dev/us-central1/app'
